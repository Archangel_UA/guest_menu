local translations =
{
	["Language"] =
	{
	    ["en"]  = "Language",
	    ["es"]  = "Idioma",
	    ["ru"]  = "Язык",
	}, 
    ["Languages"] =
    {
        ["en"]  = "English",
        ["es"]  = "Español",
        ["ru"]  = "Русский",
    }, 
    ["Months"] =
    {
        ["en"]  = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" },
        ["es"]  = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" },
        ["ru"]  = { "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"},
    },     
    ["Item_Currency"] =
    {
        ["en"]  = "uah",
        ["es"]  = "Soles",
        ["ru"]  = "грн",
    },        
    ["Item_Gram"] =
    {
        ["en"]  = "gr",
        ["es"]  = "gr",
        ["ru"]  = "гр",
    },
    ["YesNo"] =
    {
        ["en"]  = { "Yes", "No" },
        ["es"]  = { "Si", "No" },
        ["ru"]  = { "Да", "Нет" },
    },
    ["ok"] = 
  {
        ["en"]  ="OK",
        ["es"]  ="Ok",
        ["ru"]  ="OК",
    },    
    ["Cancel"] =
    {
        ["en"]  = "Cancel",
        ["es"]  = "Cancelar",
        ["ru"]  = "Отмена",
    },         
    ["Modify"] =
    {
        ["en"]  = "Modify",
        ["es"]  = "Modificar ",
        ["ru"]  = "Изменить",
    },         
    ["CloseBtn"] =
    {
        ["en"]  = "Close",
        ["es"]  = "Cerrar",
        ["ru"]  = "Закрыть",
    },
    ["ClosePaymentDlgBtn"] =
    {
        ["en"]  = "Close payment dialog",
        ["es"]  = "Cerrar Diálogo de Pagos",
        ["ru"]  = "Закрыть диалог оплаты",
    },    
    ["EnterModuleTxt_Tabs"] =
    {
        ["en"]  = {"Log in", "Sign up"},
        ["es"]  = {"Iniciar sesión", "Registración"},
        ["ru"]  = { "Bxoд", "Pегистрация" }
    },
    ["OrderTxt_NoOrders"] =
    {
        ["en"]  = {
          "Establishment is not selected",
          "Select establishment",
          "You have no orders. Place an order",
          "Select products",
        },
        ["es"]  = {
          "Establecimiento no seleccionado",
          "Ir a la elección de establecimientos",
          "Sin órdenes. Realizar orden.",
          "Ir a la elección de platos",
        },
        ["ru"]  = { 
            "Заведение не выбрано",
            "Перейти к выбору заведения",
            "Заказов нет. Сделайте заказ.",
            "Перейти к выбору блюд",
        },
    },
    ["OrderTxt_ReservDate"] =
    {
        ["en"]  = { "Reservation Date", "Date" },
        ["es"]  = { "Fecha de Reserva", "Fecha" },
        ["ru"]  = { "Дата брони", "Дата" },
    },
    ["OrderTxt_ReservDateToday"] =
    {
        ["en"]  = "Today",
        ["es"]  = "Hoy",
        ["ru"]  = "Сегодня",
    },
    ["OrderTxt_ReservTime"] =
    {
        ["en"]  = {"Reservation Time", "Time"},
        ["es"]  = {"Hora de Reserva", "Hora"},
        ["ru"]  = {"Время брони", "Время"},
    },
    ["OrderTxt_ReservHours"] =
    {
        ["en"]  = { "hour", "hours", "hours", "hours", "hours" },
        ["es"]  = { "hora", "horas", "horas", "horas", "horas" },
        ["ru"]  = { "час", "часа", "часа", "часа", "часов" },
    }, 
    ["OrderTxt_ReservTimeNow"] =
    {
        ["en"]  = "Now",
        ["es"]  = "Ahora",
        ["ru"]  = "Сейчас",
    },
    ["OrderTxt_ReservComment"] =
    {
        ["en"]  = {"Reservation comment", "Reservation comment"},
        ["es"]  = {"Comentarios sobre la reserva ", "Comentarios sobre la reserva "},
        ["ru"]  = {"Комментарий брони", "Ваш комментарий"},
    },
    ["OrderTxt_ReservCommentClean"] =
    {
        ["en"]  = "Clear",
        ["es"]  = "Limpiar",
        ["ru"]  = "Очистить",
    },
    ["OrderTxt_Reserv"] =
    {
        ["en"]  = "Order reservation",
        ["es"]  = "Reserva",
        ["ru"]  = "Предзаказ",
    },
    ["OrderTxt_ReservTakeaway"] =
    {
        ["en"]  = "Is takeaway order?",
        ["es"]  = "¿Orden para llevar?",
        ["ru"]  = "Заказ на вынос?",
    },
    ["OrderTxt_ReservPersons"] =
    {
        ["en"]  = "Persons",
        ["es"]  = "En persona",
        ["ru"]  = "Персон",
    },
    ["OrderTxt_Delivery"] =
    {
        ["en"]  = "Delivery",
        ["es"]  = "Entrega",
        ["ru"]  = "Доставка",
    },
    ["OrderTxt_DeliveryAdress"] =
    {
        ["en"]  = {"Delivery address", "Address", "" },
        ["es"]  = {"Dirección de entrega", "Dirección", "" },
        ["ru"]  = {"Адрес доставки", "Адрес"},
    },
    ["OrderTxt_DeliveryComment"] =
    {
        ["en"]  = {"Delivery comment", "Your comment"},
        ["es"]  = {"Comentarios sobre la entrega ", "Sus comentarios"},
        ["ru"]  = {"Комментарий доставки", "Ваш комментарий"},
    },
    ["OrderTxt_DeliveryDate"] =
    {
        ["en"]  = { "Delivery Date", "Date" },
        ["es"]  = { "Fecha de entrega", "Fecha" },
        ["ru"]  = { "Дата доставки", "Дата" },
    },
    ["OrderTxt_DeliveryTime"] =
    {
        ["en"]  = {"Delivery Time", "Time"},
        ["es"]  = {"Hora de entrega", "Hora"},
        ["ru"]  = {"Время доставки", "Время"},
    }, 
    ["OrderTxt_Type"] =
    {
        ["en"]  = { 
            {"select order type"},
            {"Order inside establishment"},
            {"Make reservation"},
            {"Order delivery"},                      
        },
        ["es"]  = { 
            {"Elija el tipo de orden"},
            {"Orden en el local"},
            {"Reserva"},
            {"Delivery"},                      
        },
        ["ru"]  = { 
             { "выберите тип заказа" }, 
            { "Заказ в заведении" },
            { "Забронировать" },
             { "Заказать доставку" },  
        },
    }, 
    ["OrderTxt_MainButtons"] =
    {
        ["en"]  = {
          { "Reservation", "Send order to the outlet", "Close", "Repeat order","Pay" }, 
            { "Delete", "Cancel reservation", "Call waiter"  },
        },
        ["es"]  = {
          { "Reservar", "Enviar Orden", "Cerrar", "Repetir orden","Pagar" }, 
            { "Eliminar", "Anular la reservación", "Llamar al mozo"  },
        },
        ["ru"]  = { 
            { "Забронировать", "Отправить заказ в заведение", "Закрыть", "Повторить заказ" ,"Оплатить"}, 
            { "Удалить", "Отменить бронь", "Позвать официанта" },
        },
    },
    ["OrderTxt_SendingDialog"] =
    {
        ["en"]  = {
            {
                "Confirm action",
                "Do you reall want to send the order to the outlet?",
                "Send", 
                "Cancel"
            },
            {
                "Confirm action",
                "Do you really want to make preorder on !t!?",
                "Send", 
                "Cancel"               
            },
            {
                "Confirm action",
                "Do you really want to order delivery at !d! on !t! by address !a!?",
                "Order", 
                "Cancel"
            },            
        },
        ["es"]  = {
            {
                "Confirmar acciones",
                "¿Está seguro que quiere enviar la orden al establecimiento?",
                "Enviar", 
                "Cancelar"
            },
            {
                "Confirmar acciones",
                "¿Seguro que quiere hacer una reservación en !t!?",
                "Enviar", 
                "Cancelar"               
            },
            {
                "Confirmar acciones",
                "¿Seguro que desea pedir Delivery para el !d! a las !t! a esta dirección !a!?",
                "Ordenar", 
                "Cancelar"
            },            
        },
        ["ru"]  = {
            {
                "Подтверждение действия",
                "Вы действительно хотите отправить заказ в заведение?",
                "Отправить", 
                "Отмена"
            },
            {
                "Подтверждение действия",
                "Вы действительно хотите сделать предзаказ на !t!?",
                "Заказать", 
                "Отмена"               
            },
            {
                "Подтверждение действия",
                "Вы действительно хотите заказать доставку на !d! в !t! по адресу !a!?",
                "Заказать", 
                "Отмена"
            },
        },
    },
    ["OrderTxt_PayingDialog"] =
    {
        ["en"]  = {
            "Confirm action",
            "Do you really want to close an order?",
            "Close", 
            "Cancel"
        },
        ["es"]  = {
            "Confirmar acciones",
            "¿Seguro que desea cerrar el pedido?",
            "Cerrar", 
            "Cancelar"
        },
        ["ru"]  = {
            "Подтверждение действия",
            "Вы действительно хотите закрыть заказ?",
            "Закрыть", 
            "Отмена"
        },
    },
    ["OrderTxt_RepeatingDialog"] =
    {
        ["en"]  = {
            "Confirm action",
            "do you really want to create an order with the same products?",
            "Create", 
            "Cancel"
        },
        ["es"]  = {
            "Confirmar acciones",
            "¿Seguro quiere crear la orden con la misma lista de los platos?",
            "Realizar", 
            "Cancelar"
        },
        ["ru"]  = {
            "Подтверждение действия",
            "Вы действительно хотите создать заказ с таким же перечнем блюд?",
            "Создать", 
            "Отмена"
        },
    }, 
    ["loading_errors"] =
    {
        ["en"]  = {"We can't load data from server.\nTry again.",
                "We can't load data from server.\nTry again.",
                "There is no internet connection"},
        ["es"]  = {"No se pudo cargar datos desde el servidor.\I ntentelo nuevamente.",
                "No se pudo cargar datos desde el servidor.\I ntentelo nuevamente.",
                "Sin conexión a Internet.\nCompruebe la conección a internet e intentelo de nuevo."},
        ["ru"]  = {"Не удалось загрузить данные с сервера.\nПопробуйте еще раз.",
                "Не удалось загрузить данные с сервера.\nПопробуйте еще раз.",
                "Отсутствует подключение к интернету.\nПроверьте подключение к интернету и попробуйте еще раз."},
    },    
    ["OrderTxt_DeletingDialog"] =
    {
        ["en"]  = {
            {
                "Confirm action",
                "Do you really want to delete the order?",
                "Delete", 
                "Cancel"
            },
            {
                "Confirm action",
                "Do you really want to delete all data about the order?",
                "Delete", 
                "Cancel"               
            },
            {
                "Confirm action",
                "Do you really want to cancel reservation?",
                "Cancel reservation", 
                "Close window"               
            },            
        },
        ["es"]  = {
            {
                "Confirmar acciones",
                "¿Seguro que desea cancelar la orden y borrar todos los datos en él?",
                "Eliminar", 
                "Cancelar"
            },
            {
                "Confirmar acciones",
                "¿Seguro que desea eliminar todos los datos sobre el pedido?",
                "Eliminar", 
                "Cancelar"               
            },
            {
                "Confirmar acciones",
                "¿Seguro que desea cancelar la reservación?",
                "Cancelar Reserva", 
                "Cerrar ventana"               
            },            
        },
        ["ru"]  = {
            {
                "Подтверждение действия",
                "Вы действительно хотите отменить заказ и удалить все данные о нем?",
                "Удалить", 
                "Отмена"
            },
            {
                "Подтверждение действия",
                "Вы действительно хотите удалить все данные о заказе?",
                "Удалить", 
                "Отмена"               
            },
            {
                "Подтверждение действия",
                "Вы действительно хотите отменить бронь?",
                "Отменить бронь", 
                "Закрыть окно"               
            },

        },
    },
    ["OrderTxt_CallWaiter"] =
    {
        ["en"]  = {
            "Confirm action",
            "Do you really want to call waiter?",
            "Call", 
            "Cancel"
        },
        ["es"]  = {
            "Confirmar acciones",
            "¿Seguro que desea llamar al Mozo?",
            "Llamar", 
            "Cancelar"
        },
        ["ru"]  = {
            "Подтверждение действия",
            "Вы действительно хотите позвать официанта?",
            "Позвать", 
            "Отмена"
        },
    },       
    ["OrderTxt_DeleteItem"] =
    {
        ["en"]  = "Delete product",
        ["es"]  = "Eliminar ",
        ["ru"]  = "Удалить блюдо",
    },
    ["OrderTxt_PanelMenu"] =
    {
        ["en"]  = {
            { "Delete all products", "Delete selected products" },
            { "Change q-ty for all items", "Change q-ty for several items" }
        },
        ["es"]  = {
            { "Eliminar todos", "Eliminar  seleccionados" },
            { "Modificar cant.  en todos", "Modificar cant. en algunos " }
        },
        ["ru"]  = {
            { "Удалить все блюда", "Удалить выбранные блюда" },
            { "Изменить кол-во у всех блюд", "Изменить кол-во у нескольких блюд" }
        }
    },
    ["OrderTxt_Items"] =
    {
        ["en"]  = "Products",
        ["es"]  = "A ordenar",
        ["ru"]  = "Позиции",
    },
    ["OrderTxt_InfoTitles"] =
    {
        ["en"]  = { "Created", "Closed", "Served by", "Message from establishment" },
        ["es"]  = { "Creado", "Cerrado", "Lo atiende", "Mensaje del establecimiento" },
        ["ru"]  = { "Создан", "Закрыт", "Вас обслуживает", "Сообщение от заведения" },
    },
    ["OrderTxt_Tips"] =
    {
        ["en"]  = "Tips",
        ["es"]  = "Propina ",
        ["ru"]  = "Чаевые официанту",
    },        
    ["OrderTxt_Name"] =
    {
        ["en"]  = "Name",
        ["es"]  = "Nombre",
        ["ru"]  = "Название",
    },
    ["OrderTxt_Weight"] =
    {
        ["en"]  = "Weight",
        ["es"]  = "Peso",
        ["ru"]  = "Вес",
    },
    ["OrderTxt_Quantity"] =
    {
        ["en"]  = "Quantity",
        ["es"]  = "Cantidad",
        ["ru"]  = "Количество",
    },
    ["OrderTxt_Price"] =
    {
        ["en"]  = "Price",
        ["es"]  = "Precio",
        ["ru"]  = "Цена",
    },
    ["OrderTxt_Sum"] =
    {
        ["en"]  = "Sum",
        ["es"]  = "Suma",
        ["ru"]  = "Сумма",
    },
    ["OrderTxt_Totals"] =
    {
        ["en"]  = "Totals",
        ["es"]  = "Total",
        ["ru"]  = "Всего",
    },
    ["OrderN"] =
    {
        ["en"]  = "Order №",
        ["es"]  = "Orden №",
        ["ru"]  = "Заказ №",
    },
    ["OrderTxt_States"] =
    {
        ["en"]  = {
            "new",
            "sent",
            "approved",
            "payed",
            "canceled by you",
            "in production",
            "order is ready",
		"canceled by establishment",
		"closed"          
        },

        ["es"]  = {
            "nuevo",
            "enviado al establecimiento",
            "Confirmado por el establecimiento",
            "Pagado",
            "Anulado por usted",
            "comenzaron a preparar",
            "orden lista",
            "Cancelado por el establecimiento",          
		"closed"          
	},

        ["ru"]  = {
            "новый",
            "отправлен в заведение",
            "подтвержден заведением",
            "оплачен",
            "отменен вами",
            "начали готовить",
            "заказ приготовлен",
		"отклонен заведением",
		"закрыт"
        },
    },
    ["no_photo_mes"] =
    {
        ["en"]  = "No photos yet",
        ["es"]  = "El establecimiento aun no tiene fotos",
        ["ru"]  = "У заведения пока нет фотографий",
    },    
    ["OrderTxt_Errors"] =
    {
        ["en"]  = {
            "We can't send order to the outlet. Try again.",
            "There are no internet connection",
            "We can't delete the order. Try again.",
            "Restaurant have changed order state.\nYou can't delete.",
            "You need to set delivery address."            
        },
        ["es"]  = {
            "No se pudo enviar la orden al restaurante. Inténtelo de nuevo.",
            "Sin conexión a Internet",
            "No se pudo eliminar la orden. Inténtelo de nuev.",
            "Estado de la orden cambiado por el  restaurante.\nImposible eliminar.",
            "Debe rellenar la dirección de envío."            
        },
        ["ru"]  = {
            "Не удалось отправить заказ в ресторан. Попробуйте еще раз.",
            "Отсутсвует подключение к интернету",
            "Не удалось удaлить заказ. Попробуйте еще раз.",
            "Статус заказа изменен рестораном.\nУдаление невозможно.",
            "Необходимо заполнить адрес доставки."
        },
    },
    ["OrderTxt_CallMe"] =
    {
        ["en"]  = {
            "My phone: "
        },
        ["es"]  = {
            "Mi número"
        },
        ["ru"]  = {
            "Мой номер: ",
        },
    },     
    ["Rate_Caption"] =
    {
        ["en"]  = "Rate our outlet!",
        ["es"]  = "Califique nuestro establecimiento",
        ["ru"]  = "Оцените наше заведение!",
    },    
    ["Rate_RateBtn"] =
    {
        ["en"]  = "Rate",
        ["es"]  = "Calificar",
        ["ru"]  = "Оценить",
    }, 
    ["Rate_Strings"] =
    {
        ["en"]  = {"Interior", "Service", "Cuisine","Review", "Items"},
        ["es"]  = {"Interior", "Servicio", "Cocina","Calificación", "Pedidos"},
        ["ru"]  = {"Интерьер", "Обслуживание", "Кухня","Отзыв", "Заказанное"},
    }, 
    ["Rate_CancelBtn"] =
    {
        ["en"]  = "Cancel",
        ["es"]  = "Cancelar",
        ["ru"]  = "Отмена",
    },    
    ["Rate_Cuisine"] =
    {
        ["en"]  = "Cuisine",
        ["es"]  = "Cocina",
        ["ru"]  = "Кухня",
    },
    ["Rate_Service"] =
    {
        ["en"]  = "Service",
        ["es"]  = "Servicio",
        ["ru"]  = "Обслуживание",
    },
    ["Rate_Interior"] =
    {
        ["en"]  = "Interior",
        ["es"]  = "Interior",
        ["ru"]  = "Интерьер",
    },
    ["Start_Menu"] =
    {
        ["en"]  = "Price list",
        ["es"]  = "La carta del Local",
        ["ru"]  = "Меню Заведения",
    },
    ["AppPausedTitle"] =
    {
        ["en"]  = "App is paused",
        ["es"]  = "La app se detuvo",
        ["ru"]  = "Работа приостановлена",
    },        
    ["AppPausedText"] =
    {
        ["en"]  = "Do you want to exit?",
        ["es"]  = "¿Seguro que desea salir de la app?",
        ["ru"]  = "Вы действительно хотите выйти из приложения?",
    },        
    ["AppPausedContinue"] =
    {
        ["en"]  = "Continue",
        ["es"]  = "Continuar",
        ["ru"]  = "Продолжить",
    },        
    ["AppPausedExit"] =
    {
        ["en"]  = "Exit",
        ["es"]  = "Salir",
        ["ru"]  = "Выйти",
    },        
    ["RestList_Menu"] =
    {
        ["en"]  = "Menu",
        ["es"]  = "Menú   ",
        ["ru"]  = "Меню",
    },        
    ["RestList_Reservation"] =
    {
        ["en"]  = "Table reserv.",
        ["es"]  = "Reservar mesa",
        ["ru"]  = "Бронь стола",
    },
    ["Sidebar_main"] =
    {
        ["en"]  = "Main",
        ["es"]  = "Principal",
        ["ru"]  = "Главная",
    },
    ["Sidebar_list_main"] =
    {
        ["en"]  = "Select outlet",
        ["es"]  = "Establecimientos",
        ["ru"]  = "Выбор заведения",
    },    
    ["Sidebar_actions"] =
    {
        ["en"]  = "News and discounts",
        ["es"]  = "Novedades",
        ["ru"]  = "Новости заведений",
    },    
    ["Sidebar_cabinet"] =
    {
        ["en"]  = "My account",
        ["es"]  = "Mi cuenta",
        ["ru"]  = "Мой кабинет",
    },    
    ["Sidebar_orders"] =
    {
        ["en"]  = "My orders",
        ["es"]  = "Mis ordenes",
        ["ru"]  = "Мои заказы",
    },    
    ["Sidebar_menu"] =
    {
        ["en"]  = "Price list",
        ["es"]  = "La carta",
        ["ru"]  = "Меню заведения",
    },
    ["Sidebar_about"] =
    {
        ["en"]  = "About",
        ["es"]  = "Sobre el establ.",
        ["ru"]  = "О заведении",
    },
    ["Sidebar_settings"] =
    {
        ["en"]  = "Settings",
        ["es"]  = "Configuración",
        ["ru"]  = "Настройки",
    },
    ["Filters"] =
    {
        ["en"]  = "Filters",
        ["es"]  = "Filtros",
        ["ru"]  = "Фильтры",
    },
    ["history_orders"] =
    {
        ["en"]  = "History of orders",
        ["es"]  = "Historia de pedidos",
        ["ru"]  = "История заказов",
    },
    ["CabinetTxt_History"] =
    {
        ["en"]  = { "History",
            "You have no orders.\nTry again"
        },
        ["es"]  = { "Historial",
            "Usted aun no tiene ordenes."
        },
        ["ru"]  = { 
            "История",
            "У вас еще нет заказов."
        },
    },
    ["CabinetTxt_Balance"] =
    {
        ["en"]  = { "Balance","You don't have any balance yet" },
        ["es"]  = { "Balance","Usted aun no tiene un balance por establecimientos." },
        ["ru"]  = { 
            "Баланс", 
            "У вас пока нет баланса по заведениям." 
        },
    },
    ["qr_code"] =
    {
        ["en"]  = "QR Code" ,
        ["es"]  = "QR Cod" ,
        ["ru"]  = "QR Код",
    },
    ["balance"] =
    {
        ["en"]  = "Your balance (bonuses)" ,
        ["es"]  = "Balance de cuenta (bonos)" ,
        ["ru"]  = "Баланс счета (бонусы)",
    },
    ["too_much_bonuses"] =
    {
        ["en"]  = "You can't use more bonuses than you have" ,
        ["es"]  = "Usted no puede usar mas bonos de los que tiene" ,
        ["ru"]  = "Вы не можете использовать больше бонусов чем у вас есть",
    },    
    ["pay_by_bonuses"] =
    {
        ["en"]  = "Pay by bonuses" ,
        ["es"]  = "Pagar con bonos" ,
        ["ru"]  = "Оплатить бонусами",
    },    
    ["qr_info"] =
    {
        ["en"]  = "Show this QR code in the establishment and you will get discount or bonuses" ,
        ["es"]  = "Para recibir descuentos y bonos muestre este cód. al trabajador. El cód. es general para todos los establecimientos BonusMe." ,
        ["ru"]  = "Для получения скидки или бонусов покажите данный код сотруднику заведения. Код является общим для всех заведений BonusMe.",
    },  
    ["tel_info"] =
    {
        ["en"]  = "Or tell you phone number" ,
        ["es"]  = "O puede dictar su № de telefono " ,
        ["ru"]  = "Либо можете назвать ваш номер телефона",
    },      
    ["select_city"] =
    {
        ["en"]  = "Select city" ,
        ["es"]  = "Selecciones la ciudad" ,
        ["ru"]  = "Выберите город",
    },  
    ["no_description"] =
    {
        ["en"]  = "No description" ,
        ["es"]  = "Sin descripción disponible" ,
        ["ru"]  = "Описание отсутствует",
    },
    ["no_address"] =
    {
        ["en"]  = "No address" ,
        ["es"]  = "Sin dirección disponible" ,
        ["ru"]  = "Адрес отсутствует",
    },
    ["no_name"] =
    {
        ["en"]  = "No name" ,
        ["es"]  = "Sin titulo disponible" ,
        ["ru"]  = "Заголовок отсутствует",
    },
    ["password"] =
    {
        ["en"]  = "password",
        ["es"]  = "Contraseña",
        ["ru"]  = "пароль",
    }, 
    ["password_repeat"] =
    {
        ["en"]  = "repeat password",
        ["es"]  = "Repita contraseña",
        ["ru"]  = "пароль еще раз",
    }, 
    ["register"] =
    {
        ["en"]  = "Register",
        ["es"]  = "Registrarse",
        ["ru"]  = "Зарегистрироваться",
    }, 
    ["remind_password"] =
    {
        ["en"]  = "Remind password",
        ["es"]  = "Recordar contraseña",
        ["ru"]  = "Напомнить пароль",
    },
    ["name"] =
    {
        ["en"]  = "name",
        ["es"]  = "Nombre",
        ["ru"]  = "имя",
    },
    ["need_login"] =
    {
        ["en"]  = "You need to log in",
        ["es"]  = "Necesita Ingresar o Registrarse",
        ["ru"]  = "Вход не выполнен",
    },
    ["show"] =
    {
        ["en"]  = "Show",
        ["es"]  = "Mostrar",
        ["ru"]  = "Показать",
    }, 
    ["on_the_map"] =
    {
        ["en"]  = "On the map",
        ["es"]  = "En el Mapa",
        ["ru"]  = "На карте",
    }, 
    ["list"] =
    {
        ["en"]  = "List",
        ["es"]  = "Lista  ",
        ["ru"]  = "Списком",
    }, 
    ["bricks"] =
    {
        ["en"]  = "Bricks",
        ["es"]  = "Cuadro",
        ["ru"]  = "Плиткой",
    },    
    ["section_names"] =
    {
        ["en"]  = {
          "Cuisine",
          "Type",
      --     "Average bill",
          "Options",          
        },
        ["es"]  = {
          "Cocina",
          "Tipo de establecimiento",
      --     "Ticket promedio",
          "Opciones extras",          
        },
        ["ru"]  = {
          "Кухня",
          "Тип заведения",
      --     "Средний чек",
          "Дополнительные опции",
        },
    },
    ["select_cuisine"] =
    {
        ["en"]  = "Press to select cuisine",
        ["es"]  = "Presione, para eleguir ",
        ["ru"]  = "Нажмите, чтобы выбрать кухню",
    },  
    ["new_order"] =
    {
        ["en"]  = "New order",
        ["es"]  = "Nueva orden",
        ["ru"]  = "Новый заказ",
    },  
    ["price_list"] =
    {
        ["en"]  = "Price list",
        ["es"]  = "Lista de precios",
        ["ru"]  = "Прайс лист",
    },
    ["address_open"] =
    {
        ["en"]  = "Address and open hours",
        ["es"]  = "Ubicación y horarios de trabajo ",
        ["ru"]  = "Адрес и Время работы",
    },
    ["description"] =
    {
        ["en"]  = "Description",
        ["es"]  = "Descripción",
        ["ru"]  = "Описание",
    },
    ["photo"] =
    {
        ["en"]  = "Photo",
        ["es"]  = "Foto",
        ["ru"]  = "Фото",
    },
    ["restore_password_subj"] =
    {
        ["en"]  = "BonusMe Remind password",
        ["es"]  = "BonusMe  recordatorio de su clave",
        ["ru"]  = "BonusMe Напоминание пароля",
    },
    ["restore_password_txt"] =
    {
        ["en"]  = "Hello!<br><br>Your password:",
        ["es"]  = "Hola!<br><br>Su contraseña:",
        ["ru"]  = "Добрый день!<br><br>Ваш пароль:",
    },
    ["order_in_the_rest"] =
    {
        ["en"]  = "Order in the outlet",
        ["es"]  = "Onden en el local",
        ["ru"]  = "3аказ в заведении",
    },
    ["order_with_delivery"] =
    {
        ["en"]  = "Order delivery",
        ["es"]  = "Delivery",
        ["ru"]  = "Заказ c доставкой",
    },
    ["order_with_reservation"] =
    {
        ["en"]  = "Order with the table reserv.",
        ["es"]  = "Reservación",
        ["ru"]  = "Предзаказ",
    },  
    ["incl_discount"] =
    {
        ["en"]  = "incl. discount",
        ["es"]  = "Desc.",
        ["ru"]  = "в т.ч. скидка",
    },           
    ["bonuses_txt"] =
    {
        ["en"]  = "points",
        ["es"]  = "Puntos",
        ["ru"]  = "баллов",
    },           
    ["cups_txt"] =
    {
        ["en"]  = "cups",
        ["es"]  = "Porción(s)",
        ["ru"]  = "порции(й)",
    },
    ["bonuses_push"] =
    {
        ["en"]  = "Bonuses",
        ["es"]  = "Bonos ",
        ["ru"]  = "Бонусы",
    },               
    ["enter_messages"] =
    {
        ["en"]  = {
            "Еntered passwords do not match",
            "The member with this e-mail or phone already exist",
            "Fields are not filled correctly",
            "You are successfully registered",
            "Network error!",
            "All fields should be filled",
		"No internet connection",
		"There is no email address. Check the settings of your facebook account."            
        },
        ["es"]  = {
            "Contraseñas ingresadas no coinsiden",
            "Ya existe un usuario con tal email",
            "Campo rellenado incorrectamente",
            "Se registro correctamente",
            "error al conectar!",
            "Todos los campos deben ser llenados",
            "Sin conexión a Internet",            
		"There is no email address. Check the settings of your facebook account."            
	},
        ["ru"]  = {
            "Введенные пароли не совпадают",
            "Пользователь с данным емейлом или телефоном уже зарегистрирован",
            "Поля заполнены некорректно",
            "Вы успешно зарегистрированы",
            "Ошибка соединения!",
            "Все поля должны быть заполнены",
		"Нет соединения с интернетом",
		"Отсутствует email адрес. Проверить настройки вашего facebook аккаунта."
        },
    },    
    ["enter_messages_type"] =
    {
        ["en"]  = {
            "Incorrect e-mail",
            "Incorrect user name",
            "Incorrect user surname",
            "Incorrect phone number"            
        },
        ["es"]  = {
            "e-mail incorrecto",
            "Nombre incorrecto",
            "Apellidos incorrectos",
            "Teléfono incorrecto"            
        },
        ["ru"]  = {
            "Некорректный е-мейл",
            "Некорректное имя",
            "Некорректная фамилия",
            "Некорректный телефон"
        },
    },    
    ["enter_messages_min"] =
    {
        ["en"]  = {
            "E-mail should be filled",
            "User name should be filled",
            "User surname should be filled",
            "Phone number should be filled",
            "The password should be 5 and more symbols",
            "The password should be 5 and more symbols"            
        },
        ["es"]  = {
            "Es necesario rellenar un e-mail",
            "Debe rellenar el nombre",
            "Debe rellenar sus apellidos",
            "Debe rellenar el teléfono",
            "La contraseña debe tener al menos 5 caracteres",
            "La contraseña debe tener al menos 5 caracteres"            
        },
        ["ru"]  = {
            "Необходимо заполнить е-мейл",
            "Необходимо заполнить имя",
            "Необходимо заполнить фамилию",
            "Необходимо заполнить телефон",
            "Пароль должен быть не меньше 5 символов",
            "Пароль должен быть не меньше 5 символов"
        },
    },    
    ["enter_messages_login"] =
    {
        ["en"]  = {
            "All fields should be filled",
            "Wrong member e-mail or password",
            "Message with password has been sent to your email",
            "Wrong email"
        },
        ["es"]  = {
            "Todos los campos deben ser llenados",
            "e-mail o contraseña incorrecta",
            "La contraseña fue enviada a su e-mail",
            "E-mail incorrecto"
        },
        ["ru"]  = {
            "Все поля должны быть заполнены",
            "Неверный е-мейл или пароль",
            "Письмо с паролем отправлено на ваш е-мейл",
            "Неверный е-мейл"
        },
    }, 
    ["rating"] =
    {
        ["en"]  = "Rating",
        ["es"]  = "Rating",
        ["ru"]  = "Рейтинг",
    }, 
    ["no_rating"] =
    {
        ["en"]  = "nothing yet",
        ["es"]  = "Sin calificaciones",
        ["ru"]  = "нет оценок",
    },        
    ["rest_of_the_day"] =
    {
        ["en"]  = "Best outlet",
        ["es"]  = "Establecimiento del Día",
        ["ru"]  = "РЕСТОРАН ДНЯ",
    },   
    ["open_outlet"] =
    {
        ["en"]  = "Go to the outlet",
        ["es"]  = "En el establecimiento",
        ["ru"]  = "В заведение",
    },
    ["cumulat_discount"] =
    {
        ["en"]  = "Accumulation discounts",
        ["es"]  = "Aplica el sistema de descuentos acumulativos",
        ["ru"]  = "Действует система накопительных скидок",
    }, 
    ["cumulat_bonus"] =
    {
        ["en"]  = "Bonus system",
        ["es"]  = "Aplica el programa de acumulación de bonos",
        ["ru"]  = "Действует накопительная бонусная программа",
    },          
    ["scan_outlet_qr"] =
    {
        ["en"]  = "Scan outlet QR",
        ["es"]  = "Ubicar establecimiento por cód QR",
        ["ru"]  = "Найти заведение по QR",
    },     
    ["wrong_outlet_qr"] =
    {
        ["en"]  = "We can't find outlet with such code",
        ["es"]  = "No se encontro el establecimiento con tal código",
        ["ru"]  = "Заведение с таким кодом не найдено",
    },  
    ["reviews"] =
    {
        ["en"]  = "Reviews",
        ["es"]  = "Calificaciones",
        ["ru"]  = "Отзывы",
    },  
    ["no_reviews"] =
    {
        ["en"]  = "No reviews yet",
        ["es"]  = "Sin calificaciones",
        ["ru"]  = "Отзывов пока нет",
    },  
    ["last_name"] =
    {
        ["en"]  = "last name",
        ["es"]  = "Apellidos",
        ["ru"]  = "фамилия",
    },  
    ["first_name"] =
    {
        ["en"]  = "first name",
        ["es"]  = "Nombre",
        ["ru"]  = "имя",
    },  
    ["phone"] =
    {
        ["en"]  = "phone",
        ["es"]  = "Teléfono",
        ["ru"]  = "телефон",
    }, 
    ["your_location"] =
    {
        ["en"]  = "Your location",
        ["es"]  = "Su estado",
        ["ru"]  = "Ваше положение",
    },  
    ["you_want_delivery_to"] =
    {
        ["en"]  = "Do you want delivery to ",
        ["es"]  = "Usted quiere el delivery en ",
        ["ru"]  = "Вы хотите доставку на ",
    }, 
    ["select_address"] =
    {
        ["en"]  = "Choose address",
        ["es"]  = "Selecione la dirección",
        ["ru"]  = "Выберете адрес",
    },                                             
    ["order_done"] =
    {
        ["en"]  = "Your order #!p! is done!",
        ["es"]  = "Su orden #!p! está lista!",
        ["ru"]  = "Ваш заказ #!p! готов!",
    }, 
    ["order_confirmed"] =
    {
        ["en"]  = "Your order #!p! has been comfirmed!",
        ["es"]  = "Su orden #!p! fue confirmada!",
        ["ru"]  = "Ваш заказ #!p! подтвержден!",
    },     
    ["modifiers"] =
    {
        ["en"]  = "Modifiers",
        ["es"]  = "Modificadores",
        ["ru"]  = "Модификаторы",
    }, 
    ["no_news"] =
    {
        ["en"]  = "No news",
        ["es"]  = "No hay novedades",
        ["ru"]  = "Нет новостей",
    },                                                               
    ["new_card"] =
    {
        ["en"]  = "New card",
        ["es"]  = "Nueva tarjeta",
        ["ru"]  = "Новая карта",
    },                                                               
    ["facebook"] =
    {
        ["en"]  = "Facebook login",
        ["es"]  = "Con Facebook",
        ["ru"]  = "Facebook вход",
    },                                                               
    ["new_password"] =
    {
        ["en"]  = "new password",
        ["es"]  = "Nueva Contraseña ",
        ["ru"]  = "новый пароль",
    },
    ["new_password_1_more"] =
    {
        ["en"]  = "repeat new password",
        ["es"]  = "Repita contraseña ",
        ["ru"]  = "новый пароль еще раз",
    },    
    ["Days"] = {
        ["en"]  ={"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"},
        ["es"]  ={"Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"},
        ["ru"]  ={"Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"},
    },
    ["birthday"] = {
		["en"]  = "Birthday",
		["es"]  = "Cumpleaños",
		["ru"]  = "День рождения",
    },
    ["cancel"] = {
	["en"]  = "Cancel",
	["es"]  = "Cancelar",
	["ru"]  = "Отмена",
   },
   ["send_sms"] = {
	["en"]  = "Send SMS",
	["es"]  = "Send SMS",
	["ru"]  = "Отправить SMS",
   },   
   ["phone_verification"] = {
	["en"]  = "Phone verification",
	["es"]  = "Phone verification",
	["ru"]  = "Подтверждение номера",
   }, 
   ["will_send_sms"] = {
	["en"]  = "will_send_sms",
	["es"]  = "will_send_sms",
	["ru"]  = "На номер # будет отправлено SMS с кодом.\nНажмите кнопку \"Отправить\"\nи дождитесь получения сообщения.",
   },  
   ["sent_sms"] = {
	["en"]  = "sent_sms",
	["es"]  = "sent_sms",
	["ru"]  = "На номер #\nотправлено SMS с кодом.\nВведите полученный код в поле ниже.\nЕсли вы не получили сообщение, \n повторите отправку через # секунд.",
   },   
   ["sms_error_provider"] = {
	["en"]  = "sms_error_provider",
	["es"]  = "sms_error_provider",
	["ru"]  = "Не удалось отправить SMS на номер #.\nПроверьте номер телефона и повторите попытку\nлибо свяжитесь со службой поддеркжи.",
   },    
   ["sms_error_network"] = {
	["en"]  = "sms_error_network",
	["es"]  = "sms_error_network",
	["ru"]  = "Не удалось отправить SMS.\nПроверьте подключение к сети интернет и повторите попытку\nлибо свяжитесь со службой поддеркжи.",
    },    
    ["sms_wrong_code"] = {
	   ["en"]  = "Incorrect code",
	   ["es"]  = "Incorrect code",
	   ["ru"]  = "Неверный код из SMS сообщения",
   },
   ["select_country"] =
    {
        ["en"]  = "Select country" ,
        ["es"]  = "Selecciones la país" ,
        ["ru"]  = "Выберите страну",
    },
    ["km"] =
    {
        ["en"]  = "km" ,
        ["es"]  = "km" ,
        ["ru"]  = "км",
    },
    ["r_comment"] = {
        ["en"]  ="Comment",
        ["ru"]  ="Комментарий",
        ["tr"]  ="Yorum",
        ["ge"]  ="კომენტარები",
        ["uk"]  ="Коментар",
        ["es"]  ="Comentarios",
        ["cn"]  ="评论",
        ["ar"]  ="تعليق",
        ["pl"]  ="Komentarz",
        ["th"]  ="คิดเห็น",
        ["az"]  ="Şərh",
    },
    ["w_no_imp__pay_type"] =
    {
        ["en"]  = "This type of payment is not implemented in the current version of the application" ,
        ["es"]  = "Este tipo de pago no está implementado en la versión actual de la aplicación" ,
        ["ru"]  = "Этот тип оплаты не реализован в текущей версии приложения",
    },["w_no_pay_type"] =
    {
        ["en"]  = "there are no payment methods available here" ,
        ["es"]  = "no hay métodos de pago disponibles aquí" ,
        ["ru"]  = "здесь нет доступных способов оплаты",
    },
}

 trans = function( key,n,langCode)
    langCode = langCode or _G.Lang
    if n then
        if translations[key] and translations[key][langCode] and translations[key][langCode][n]  then
            return translations[key][langCode][n]
        else
            return key.." ?"
        end
    else
        if translations[key] and translations[key][langCode] then
            return translations[key][langCode]
        else
            return key.." ?"
        end 
    end
end

return translations