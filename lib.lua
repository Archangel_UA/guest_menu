function comma_value(amount)
  local formatted = amount
  while true do  
    formatted, k = string.gsub(formatted, "^(-?%d+)(%d%d%d)", '%1,%2')
    if (k==0) then
      break
    end
  end
  return formatted
end

function round(val, decimal)
  if (decimal) then
    return math.floor( (val * 10^decimal) + 0.5) / (10^decimal)
  else
    return math.floor(val+0.5)
  end
end

local random = math.random
local function uuid()
    local template ='xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'
    return string.gsub(template, '[xy]', function (c)
        local v = (c == 'x') and random(0, 0xf) or random(8, 0xb)
        return string.format('%x', v)
    end)
end


function format_num(amount, decimal, prefix, neg_prefix)
  local str_amount,  formatted, famount, remain

  decimal = decimal or 2  -- default 2 decimal places
  neg_prefix = neg_prefix or "-" -- default negative sign

  famount = math.abs(round(amount,decimal))
  famount = math.floor(famount)

  remain = round(math.abs(amount) - famount, decimal)

        -- comma to separate the thousands
  formatted = comma_value(famount)

        -- attach the decimal portion
  if (decimal > 0) then
    remain = string.sub(tostring(remain),3)
    formatted = formatted .. "." .. remain ..
                string.rep("0", decimal - string.len(remain))
  end

        -- attach prefix string e.g '$' 
  formatted = (prefix or "") .. formatted 

        -- if value is negative then format accordingly
  if (amount<0) then
    if (neg_prefix=="()") then
      formatted = "("..formatted ..")"
    else
      formatted = neg_prefix .. formatted 
    end
  end

  return formatted
end

function chsize(char)
    if not char then
        return 0
    elseif char > 240 then
        return 4
    elseif char > 225 then
        return 3
    elseif char > 192 then
        return 2
    else
        return 1
    end
end

function utf8sub(str, startChar, numChars)
  if str==nil then
    return ""
  end
  local startIndex = 1
  while startChar > 1 do
      local char = string.byte(str, startIndex)
      startIndex = startIndex + chsize(char)
      startChar = startChar - 1
  end
 
  local currentIndex = startIndex
 
  while numChars > 0 and currentIndex <= #str do
    local char = string.byte(str, currentIndex)
    currentIndex = currentIndex + chsize(char)
    numChars = numChars -1
  end
  return str:sub(startIndex, currentIndex - 1)
end

function getTimeStamp(dateString)
  local pattern = "(%d+)%-(%d+)%-(%d+).(%d+)%:(%d+)%:([%d%.]+)"
  local year, month, day, hour, minute, seconds = dateString:match(pattern)
  local timestamp = os.time({year=year, month=month, day=day, hour=hour, min=minute, sec=seconds})
  local offset = 0
  if ( tzoffset ) then
    if ( tzoffset == "+" or tzoffset == "-" ) then  -- we have a timezone!
      offset = offsethour * 60 + offsetmin
      if ( tzoffset == "-" ) then
        offset = offset * -1
      end
      timestamp = timestamp + offset
    end
  end
  return timestamp
end

-- replace UTF-8 characters based on a mapping table
function utf8replace (s, mapping)
  -- argument checking
  if type(s) ~= "string" then
    error("bad argument #1 to 'utf8replace' (string expected, got ".. type(s).. ")")
  end
  if type(mapping) ~= "table" then
    error("bad argument #2 to 'utf8replace' (table expected, got ".. type(mapping).. ")")
  end

  local pos = 1
  local bytes = s:len()
  local charbytes
  local newstr = ""

  while pos <= bytes do
    --charbytes = utf8charbytes(s, pos)
    local c = utf8sub(s,pos, 1)
    newstr = newstr .. (mapping[c] or c)

    pos = pos + 1
  end

  return newstr
end

function createGrid( design, data )
  -- shortcut for creating text - x position calculated outside
  function genTxt(txt, yp,header, wl, align)
    align = align or "center"
    local header=header or false
    local fontSize
    if header then
      fontSize=css.font_size.rowCaptionFontSize
    else
      fontSize=css.font_size.rowCaptionFontSize
    end
    local txtL = display.newText({text=txt, x=0, y=yp, font=native.systemFont, width = wl,fontSize=fontSize, align=align})
    txtL:setFillColor(0)
    return txtL
  end
  local poslib = require( "lib.pos_lib" )
  local bg
  local grid = display.newGroup()
  local headerGrid = display.newGroup()
  local columns = {}
  local xp = 0
  local yp = 0

  local aligns -- настройки выравнивания  по умолчанию по центру 
  if not design.aligns then
    aligns = {}
    for i=1, #design.cols do
      aligns[i] = "center"
    end
  else
    aligns = design.aligns
    for i=1, #aligns do
      if aligns[i] == 0 then
        aligns[i] = "center"
      elseif aligns[i] == 1 then
        aligns[i] = "left"
      elseif aligns[i] == 2 then
        aligns[i] = "right"
      else
        aligns[i] = "center"
      end
    end
  end

  local realW =  _Xa-(#design.cols+1)*design.cellfontfix
  for i=1, #design.cols do
 	 design.cols[i]=realW*design.cols[i]/100
  end
  local rect = display.newRect(0, 0, _Xa, 60)
  rect.x = _Xa/2; 
  rect:setFillColor(css.colorDialogDackground[1],css.colorDialogDackground[2],css.colorDialogDackground[3] )

  local line = poslib:newBitmappedLine(0,30,_Xa,30,1,css.standartLine)
  headerGrid:insert(rect)
  headerGrid:insert(line)

  local tx = 0
  for k, v in pairs(design.cols) do -- определяем поозицию стоплбца (по Х)
    tx = tx+design.cellfontfix+v
    columns[k] = {tx-v/2, v}
  end

  for c, wid in ipairs( design.cols ) do -- оглавление столбцов
    txt = genTxt( data[1][c], 0 ,true, wid )
    txt.x = columns[c][1]
    headerGrid:insert( txt, false )
    xp = xp + wid
  end

  yp = yp + design.header.height
  -- start from row 2 since 1st row is headers
  for r = 2, table.getn(data)-1 do -- данные
    row = data[r]
    local maxH = design.row.height
    local tempR = {}
    local line = poslib:newBitmappedLine(0,yp + design.row.height/2,_Xa,yp + design.row.height/2,1,css.standartLine)
    grid:insert( line, false )
    for c,col in ipairs( row ) do
      txt = genTxt(col, yp,nil, columns[c][2],aligns[c]);
      txt.x = columns[c][1]
      if maxH < txt.height then
        maxH = txt.height
      end
      grid:insert( txt, false )
      table.insert(tempR,txt)
    end
    if maxH > design.row.height then -- ровняем всю строку 
      maxH= maxH+10
      yp = yp-design.row.height/2+maxH/2
      for k, v in pairs(tempR) do        
        v.y=yp        
      end
      line.y = yp+maxH/2
      yp = yp + maxH/2 + design.row.height/2 
    else 
      yp = yp + design.row.height
    end
  end
  
  xp=0
  if #data>2 then -- итоги
    for c, wid in ipairs( design.cols ) do
      txt = genTxt( data[table.getn(data)][c], yp + design.footer.height / 2 + design.toprowfontfix - 2  )
      txt.x = columns[c][1]
      grid:insert( txt, false, nil, wid )
      xp = xp + wid
    end
  end
  local res={}
  res[0]=headerGrid
  res[1]=grid
  return res
end

