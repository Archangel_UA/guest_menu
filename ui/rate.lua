local uilib = require "ui.uilib"
local rdb = require "lib.db_remote"

local rate = {}

local function createTitle( width, text )
    local pad = 0
    local text_pad = 5
    local height = 20
    local line_height = 0.7
    local line_color = 0.8
    local font = "HelveticaNeueCyr-Light"
    local font_size = 13

    local title = display.newGroup()
    title.anchorChildren = true

    local bg = display.newRect( title, 0, 0, width, height )
    bg:setFillColor( 0.5, 0 )

    local ref_left = -bg.width*0.5 + pad

    local left_line = display.newRect( title, ref_left, 0, 25, line_height )
    left_line:setFillColor( line_color )
    left_line.anchorX = 0

    local title_text = display.newText({
        parent = title,
        text = text,
        font = font,
        fontSize = font_size,
    })
    title_text:setFillColor( 0 )
    title_text.anchorX = 0
    title_text.x = left_line.x + left_line.width + text_pad

    local rlwidth = width - ( left_line.width + title_text.width + pad*2 + text_pad*2 )
    local right_line = display.newRect( title, 0, 0, rlwidth, line_height )
    right_line:setFillColor( line_color )
    right_line.anchorX = 0
    right_line.x = title_text.x + title_text.width + text_pad

    return title
end

local function createStarsRow( params )
    params = params or {}
    local row_id = params.row_id or ""
    local pad = params.pad or 0
    local scale = params.scale or 1
    local listener = params.listener or function() end

    local stars = display.newGroup()
    stars.anchorChildren = true

    local options = {
        width = 57,
        height = 54,
        numFrames = 2,
        sheetContentWidth = 114,
        sheetContentHeight = 54
    }
    local sheet =graphics.newImageSheet( "assets/rateStar.png", options )
    
    local star
    for i=1, 5 do
        star = display.newGroup()
        star.id = i
        star.anchorChildren = true
        star:insert( display.newImageRect( sheet, 1, 57*0.6, 54*0.6 ) )
        star:insert( display.newImageRect( sheet, 2, 57*0.6, 54*0.6 ) )
        stars:insert( star )
        star:scale( scale, scale )    
        star.anchorX = 0
        star.x = (star.width + pad) * (i-1)
        star[1].isVisible = false
        if i == 1 and params.init then
            star[1].isVisible = true
            star[2].isVisible = false
        end
        star:addEventListener( "touch", function( event )
            local star = event.target
            if event.phase == "ended" or event.phase == "cancelled" then
                if not params.init then
                    if star.id == 1 then
                        if not stars[2][1].isVisible then
                            stars[1][1].isVisible, stars[1][2].isVisible = stars[1][2].isVisible, stars[i][1].isVisible
                        end
                    elseif star.id > 1 then
                        stars[1][1].isVisible = true
                        stars[1][2].isVisible = false  
                    end
                end
                for i=2, stars.numChildren do
                    if i <= star.id then
                        stars[i][1].isVisible = true
                        stars[i][2].isVisible = false                            
                    else
                        stars[i][2].isVisible = true
                        stars[i][1].isVisible = false   
                    end                    
                end
                listener( row_id, star.id )
            end
            --return true
        end )
    end

    return stars
end

local function createDishRate( dishes_data, listener )
    local photo_size = "80x40"
    local pad = 10

    local dr = display.newGroup()
    dr.anchorChildren = true

    local mask = graphics.newMask( "assets/itemRateMask.png" )

    local row, frame, title, stars
    local photo_index = {}
    local ypos = 0
    for i=1, #dishes_data do
        row = display.newGroup()
        row.anchorChildren = true
        row.anchorY = 0
        dr:insert( row )

        frame = display.newImageRect( "assets/itemRateFrame.png", 50, 50 )
        row:insert( frame )
        
        photo_index[dishes_data[i].mi_id] = i
        rdb:getDishData( dishes_data[i].com_id, dishes_data[i].mi_id, "ru", function( state, didata )
            if state == "success" then
                rdb:getDishPhoto( didata.com_id, didata.photo_id, photo_size, function( state, phdata )
                    if state == "success" then
                        local mi_id = tonumber( didata.mg_id )
                        local index = photo_index[mi_id]
                        local photo = phdata.image
                        
                        if dr[index] then
                            dr[index]:insert( 1, photo )
                            photo:setMask( mask )
                        else photo:removeSelf()
                            photo = nil
                        end
                    end
                end )
            end
        end )

        title = display.newText({
            parent = row,
            text = dishes_data[i].mi_name,
            font = "HelveticaNeueCyr-Thin",
            fontSize = 9,
            width = 70,
            --height = 10,
            align = "center"
        })
        title:setFillColor( 0 )
        title.anchorY = 0
        title.y = frame.height*0.5 + 4

        stars = createStarsRow({
            row_id = dishes_data[i].mi_id,
            pad = 3,
            scale = 0.9, 
            listener = listener
        })
        stars.anchorX = 0
        stars.x = frame.width*0.5 + 10
        row:insert( stars )
        
        row.y = ypos
        ypos = ypos + row.height + pad    
    end

    return dr
end

local function sendDishesRate( data, listener )
    local count = 0
    local dishes_count = 0
    for k,v in pairs(data) do
        if v.rate > 0 then
            dishes_count = dishes_count + 1
            rdb:sendDishRate( v, function( state )
                count = count + 1
                if dishes_count == count then 
                    listener()
                end
            end )
        end
    end
    if dishes_count == 0 then listener() end
end

local function newRateBox( params )
    params.onClose = params.onClose or function() end

    local data = {
        com_id = params.com_id,
        ord_id = params.ord_cid,
        guest_id = params.guest_id,
        pos_id = params.pos_id,        
        datetime = os.date('%Y-%m-%d %H:%M:%S', os.time(os.date( "*t" ))),--JENDATECHANGE Date():fmt('${iso}'),
        interior = 1,
        service = 1,
        cuisine = 1,
        ordrat_text = "",        
    }
    local dishes_data = {}
    local mi_id = 0
    for i=1, #params.dishes do
        mi_id = params.dishes[i].mi_id
        dishes_data[mi_id] = {
            com_id = params.com_id,
            ord_id = params.ord_cid,
            mi_id = params.dishes[i].mi_id,
            datetime = os.date('%Y-%m-%d %H:%M:%S', os.time(os.date( "*t" ))),--JENDATECHANGE Date():fmt('${iso}'),
            rate = 0
        } 
    end

    local max_width, max_height = _Xa-40, _Ya-50
    
    ---------------------------

    local rb = uilib.newBox( max_width, max_height, params.onClose )
    local top = rb.bg.y - rb.bg.height*0.5
    local left = rb.bg.x - rb.bg.width*0.5
    local right = rb.bg.x + rb.bg.width*0.5
    local xcenter = rb.bg.x

    --------------------------

    local title = display.newText({
        text = translations["Rate_Caption"][_G.Lang],
        width = max_width*0.8,
        font = "HelveticaNeueCyr-Medium", 
        fontSize = 15,
        align = "center"
    })
    title:setFillColor( 1,0,0 )
    title.x = xcenter
    title.anchorY = 0
    title.y = top + 17
    rb:insert( title )

    ---------------------------

    local sv_content = display.newGroup()
    sv_content.anchorChildren = true
   
    local subtitles = {}
    local st_names = translations.Rate_Strings[_G.Lang]
    local stars = {}
    local stars_id = { "interior", "service", "cuisine" }

    local ypos = 0
    for i=1, #st_names do
        subtitles[i] = createTitle( max_width, st_names[i] )
        subtitles[i].anchorY = 0
        subtitles[i].y = ypos
        sv_content:insert( subtitles[i] )

        if i < #st_names then
            if i==4 then --review
                local function inputListener( event )
                    if event.phase == "editing" then
                        -- do something with textBox text
                        data.ordrat_text= event.text

                    end
                end                
                local review = native.newTextBox( 
                left, -200,                    -- x, y
                max_width-80, 50         -- w, h
                )
                review:setTextColor( 0 )
                review.isEditable = true
                review.isFontSizeScaled = true  -- make the text box use the same font units as the text object
                review.size = 12               
                --review.size = 200
                review.anchorX = 0
                review.anchorY = 0
                review.x = -(max_width-80)/2
                review.y = ypos + subtitles[i].height + 10
                review:addEventListener( "userInput", inputListener )
                 sv_content:insert(review )
                ypos = review.y + review.height + 13
            else
                stars[i] = createStarsRow({
                    row_id = stars_id[i],
                    init = true,
                    pad = 10, 
                    listener = function( id, value )
                        data[id] = value
                    end
                })
                stars[i].anchorY = 0
                stars[i].y = ypos + subtitles[i].height + 5
                sv_content:insert( stars[i] )
                
                ypos = stars[i].y + stars[i].height + 13
            end
        else
            local drate = createDishRate( params.dishes, function( id, value )
                dishes_data[id].rate = value
            end )
            drate.anchorY = 0
            drate.y = ypos + subtitles[i].height + 5
            sv_content:insert( drate )
        end
    end

    local scroll_view = widget.newScrollView
    {
        width = max_width,
        height = max_height - 125,
        bottomPadding = 0,
        horizontalScrollDisabled = true,
        hideBackground = true,
        listener = function( event )
            display.getCurrentStage():setFocus( nil )
        end
    }   
    sv_content.x = scroll_view.width*0.5
    sv_content.anchorY = 0
    sv_content.y = 0
    scroll_view:insert( sv_content )

    rb:insert( scroll_view )
    scroll_view.x = xcenter
    scroll_view.anchorY = 0
    scroll_view.y = title.y + title.height + 15  
    
    ----------------------

    local sepr = display.newLine( 
        right + 0, 
        scroll_view.y + scroll_view.height + 5,  
        left - 0, 
        scroll_view.y + scroll_view.height + 5
    )
    sepr:setStrokeColor( 0,0,0, 0.1 )
    sepr.strokeWidth = 1.5
    rb:insert( sepr )  

    local b_rate = widget.newButton({
        label = translations.Rate_RateBtn[_G.Lang],
        labelColor = { default={ 1, 0, 0, 1 }, over={ 0, 0.5 } },
        fontSize = 14,
        font = "HelveticaNeueCyr-Medium",
        --properties for a rounded rectangle button...
        shape="roundedRect",
        width = max_width * 0.45,
        height = 37,
        cornerRadius = 5,
        fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
        strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
        strokeWidth = 2,
        onRelease = function( event )
            Spinner:start()
            rdb:sendRestRate( data, function( state )
                sendDishesRate( dishes_data, function()
                    Spinner:stop()
                    rb:removeSelf()
                    rb = nil
                    params.onClose() 
                end )
            end )
        end     
    })
    rb:insert( b_rate, true )
    b_rate.anchorX = 0
    b_rate.x = left + max_width*0.2/3
    b_rate.anchorY = 0
    b_rate.y = sepr.y + sepr.height*0.5 + 15

    local b_cancel = widget.newButton({
        label = translations.Rate_CancelBtn[_G.Lang],
        labelColor = { default={ 0, 1 }, over={ 0, 0, 0, 0.5 } },
        fontSize = 14,
        font = "HelveticaNeueCyr-Medium",
        --properties for a rounded rectangle button...
        shape="roundedRect",
        width = max_width * 0.35,
        height = 37,
        cornerRadius = 5,
        fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
        strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
        strokeWidth = 2,
        onRelease = function( event )
            rb:removeSelf()
            rb = nil
            params.onClose() 
        end     
    })
    rb:insert( b_cancel, true )
    b_cancel.anchorX = 1
    b_cancel.x = right - max_width*0.2/3
    b_cancel.anchorY = 0
    b_cancel.y = sepr.y + sepr.height*0.5 + 15


end


------------------------------------
rate.newRateBox = newRateBox

return rate