--[[

-- Options Example --

options = { 
    -- general
    width = display.contentWidth * 0.8,
    height = display.contentHeight * 0.8,
    bg = displayObject,                     -- is not scrollable
    speed = 250,
    inEasing = easing.outBack,
    outEasing = easing.outCubic,
    isScrollable = true,
    topScrollPad = 0,
    bottomScrollPad = 0,
    friction = 1,   
    isHold = false,
    onPress = listener,
    onStart = listener,
    onComplete = listener,

    --header
    headerHeight = 20,
    headerColor = {1,1,1}
}

-- Object Properties --

object.header                          - header group of the sliding panel
object.content
object.completeState
object.openHeight
object.isHold

-- Object Methods --

object:show()
object:hide()

]]

local widget = require "widget"

function widget.newSlidingPanel( options )
    local opt = options or {}
    
    opt.width = opt.width or display.actualContentWidth
    opt.height = opt.height or display.actualContentHeight
    opt.speed = opt.speed or 500
    opt.inEasing = opt.inEasing or easing.linear
    opt.outEasing = opt.outEasing or easing.linear
    opt.isScrollable = opt.isScrollable == nil and true or opt.isScrollable
    opt.topScrollPad = opt.topScrollPad or 0
    opt.bottomScrollPad = opt.bottomScrollPad or 0
    opt.friction = opt.friction or 1

    local panel = display.newGroup()
    panel.anchorChildren = true
    panel.x = display.contentCenterX
    panel.y = display.contentCenterY

    panel.id = opt.id
    panel.isHold = opt.isHold

    local container = display.newContainer( panel, opt.width, opt.height )       

    if opt.headerHeight > 0 then
        local y = -container.height*0.5 + opt.headerHeight*0.5
        local x = container.x - container.width*(container.anchorY-0.5)

        local bg = display.newRect( 0, 0, container.width, opt.headerHeight )
        if not opt.headerColor then 
            bg.alpha = 0
            bg.isHitTestable = true
        else bg:setFillColor( unpack( opt.headerColor ) )
            bg.isHitTestable = true
        end
        
        panel.header = display.newGroup()
        panel.header.y = y
        panel.header:insert( bg )
        container:insert( panel.header )

        panel.header.left = -bg.width*0.5
        panel.header.right = bg.width*0.5
        panel.header.top = -bg.height*0.5
        panel.header.bottom = bg.height*0.5    
    end
    
    panel.openHeight = opt.height - (opt.headerHeight or 0)

    panel.content = display.newGroup()
    panel.content.anchorChildren = true
    panel.content.anchorY = 0
    panel.content.y = -container.height*0.5 + opt.headerHeight --or 0--container.header.height*0.5
    container:insert( panel.content )
    panel.content:toBack()

    panel.content.bg = display.newRect( 
        panel.content, 
        0, 0, 
        opt.width, panel.openHeight
    )
    panel.content.bg:setFillColor( 0.5 )
    panel.content.bg.isVisible = false

    if opt.bg then
    	panel.bg = opt.bg
    	panel:insert( panel.bg )
    	panel.bg:toBack()
    end

	panel.scrollpad = display.newRect( 
	    container, 
	    0, 0, 
	    opt.width, panel.openHeight
	)
    panel.scrollpad.anchorY = 0
    panel.scrollpad.y = panel.header.y + panel.header.height*0.5
    panel.scrollpad.isVisible = false
    panel.scrollpad.isHitTestable = true
    panel.scrollpad._topLimit = 0
    panel.scrollpad._bottomLimit = opt.isLocked or true    
    
    panel.scrollpad:addEventListener( "touch", function( event )
        local target = event.target
        local global_delta = event.y - event.yStart
        local content_bounds = panel.content.contentBounds
        local container_bounds = container.contentBounds
        local tpad = opt.topScrollPad
        local bpad = opt.bottomScrollPad
        local up_dist = content_bounds.yMax - container_bounds.yMax + bpad
        local down_dist = container_bounds.yMin + opt.headerHeight - content_bounds.yMin + tpad
        
        if event.phase == "began" then
           if target.trans then transition.cancel( target.trans ) end
           target.last_delta = 0
           target.yprev = 0
        elseif event.phase == "moved" then
            if target.isFocus then 
                local delta = event.y - target.yprev
                if delta > 0 and down_dist < delta then
                    delta = down_dist
                elseif delta < 0 and up_dist < delta*(-1) then
                    delta = -up_dist
                end
                panel.content.y = panel.content.y + delta
                target.yprev = event.y
                target.last_delta = delta
                return true
            elseif math.abs( global_delta ) >= 5 then
                if up_dist > 0 or down_dist > 0 then
                    target.isFocus = true
                    target.yprev = event.y
                    display.getCurrentStage():setFocus( target )  
                    return true
                end
            end
        elseif event.phase == "ended" or event.phase == "cancelled" then
            target.isFocus = false
            display.getCurrentStage():setFocus( nil )
           
            local up_offset = container_bounds.yMin + opt.headerHeight - content_bounds.yMin
            local down_offset = container_bounds.yMax - content_bounds.yMax  
            if up_offset < 0 then
                target.trans = transition.to( panel.content, {
                    time=300, delta=true, y=up_offset, transition = easing.outBack
                })    
            elseif down_offset > 0 then
                target.trans = transition.to( panel.content, {
                    time=300, delta=true, y=down_offset, transition = easing.outBack
                })  
            elseif math.abs( target.last_delta ) > 2 then
                local y = target.last_delta * 100 * opt.friction
                if y < 0 and down_offset - y > 0 then y = down_offset end
                if y > 0 and up_offset - y < 0 then y = up_offset end                    
                local time = math.abs( y * 10 )
                target.trans = transition.to( panel.content, {
                    time=time, delta=true, y=y, transition = easing.outExpo
                })
            end
        end
        return false
    end )
        
    panel.header:addEventListener( "touch", function ( event )
        if event.phase == "began" then
            display.getCurrentStage():setFocus( event.target )
        elseif event.phase == "moved" then
            display.getCurrentStage():setFocus( nil )
        elseif event.phase == "ended" or event.phase == "cancelled" then
            if panel.completeState == "shown" then
                if opt.onPress then 
                    event.target = panel
                    opt.onPress( event ) 
                end
                if not panel.isHold then panel:hide() end
            elseif panel.completeState == "hidden" then
                if opt.onPress then
                    event.target = panel 
                    opt.onPress( event ) 
                end
                if not panel.isHold then panel:show() end
            end
            display.getCurrentStage():setFocus( nil )
        end
        return true
    end )
    
    panel.completeState = "shown"
    panel.lastAction = "show"

    local function moveBg( action, time )
    	if not panel.bg then return end
    	local target = panel.bg
        local moveto = target.height-- + panel.header[1].height
        if action == "hide" then moveto = -target.height end
        local options = {
            time = time or opt.speed,
            transition = opt.inEasing,
            delta = true,
            y = moveto,
            onStart = function( obj )
                if action == "hide" then 
                    container:insert( target )
                    target:toBack()
                end
            end,
            onComplete = function( obj )
                if action == "show" then 
                	panel:insert( target )
                	target:toBack()
                end
            end,
             onCancel = function( obj )
                 if action == "hide" then 
                     target.y = moveto
                 end
             end
        }
        transition.to( target, options )
    end
    
    function panel:show( time )
        if self.lastAction == "show" then return end
        self.lastAction = "show"

        if self.content then transition.cancel( self.content ) end
        self.scrollpad.isHitTestable = true
        self.content.y = -self.content.height -self.height*0.5
        local options = {
            time = time or opt.speed,
            transition = opt.inEasing,
            delta = true,
            y = self.content.height + self.header[1].height,
            onStart = function( obj )
                if opt.onStart then opt.onStart( self ) end
            end,
            onComplete = function( obj )
                self.completeState = "shown"
                if opt.onComplete then 
                    opt.onComplete( self )
                end
            end,
            onCancel = function( obj )
                self.completeState = "shown"
                obj.y = -self.height*0.5 + opt.headerHeight
            end
            
        }
        transition.to( self.content, options )
        moveBg( "show", time or opt.speed )
    end
    function panel:hide( time )
        if self.lastAction == "hide" then return end
        self.lastAction = "hide"

        if self.content then transition.cancel( self.content ) end
        self.scrollpad.isHitTestable = false
        self.content.y = -self.height*0.5 + opt.headerHeight
        local options = {
            time = time or opt.speed,
            transition = opt.outEasing,
            delta = true,
            y = -self.content.height-3,
            onStart = function( obj )
                if opt.onStart then opt.onStart( self )  end
            end,
            onComplete = function( obj )
                self.completeState = "hidden"
                if opt.onComplete then
                    opt.onComplete( self )
                end 
            end,
            onCancel = function( obj )
                self.completeState = "hidden"
                obj.y = -self.content.height -self.height*0.5
            end
        }
        transition.to( self.content, options )
        moveBg( "hide", time or opt.speed )
    end

    local function realpos( dispobj )
        local bounds = dispobj.contentBounds
        local realx = bounds.xMin + dispobj.width*dispobj.anchorX
        local realy = bounds.yMin + dispobj.height*dispobj.anchorY
        return realx, realy
    end

    function panel:place( obj, options  )
    	local top = -self.content.bg.height*0.5
    	local bottom = self.content.bg.height*0.5
    	local right = self.content.bg.width*0.5
    	local left = -self.content.bg.width*0.5
        
        options = options or {}
        obj.anchorChildren = true
        self.content:insert( obj, true )
        obj.anchorY = 0
        obj.y = top
        local _, contenty = realpos( self.content ) 
        if options.top then
            obj.y = top + options.top --options.top > 0 and options.top or 0
        elseif options.bottom then
            obj.anchorY = 1
            obj.y = bottom - options.bottom-- self.openHeight - (options.bottom > 0 and options.bottom or 0)
        end
        if options.left then
            obj.anchorX = 0
            if obj.width + options.left > self.width then options.left = 0  end
            obj.x =  left + options.left -- - self.width*0.5 + (options.left > 0 and options.left or 0)
        elseif options.right then
            obj.anchorX = 1
            if obj.width + options.right > self.width then options.right = 0  end
            obj.x =  right - options.right --self.width*0.5 - (options.right > 0 and options.right or 0)
        end
    end

    return panel
end