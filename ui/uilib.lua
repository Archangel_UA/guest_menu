local widget = require "widget"

local uilib = {}

function uilib.newToggleButton( params )
    local bs = display.newGroup()
    bs.anchorChildren = true

    params.listener = params.listener or function() end

    function switching( self, state )
        if state == "on" then
            self.offbutton.isVisible = false
            self.offbutton:setEnabled( false )
            self.onbutton.isVisible = true
            self.onbutton:setEnabled( true )
            self.state = "on"
        elseif state == "off" then
            self.onbutton.isVisible = false
            self.onbutton:setEnabled( false )
            self.offbutton.isVisible = true
            self.offbutton:setEnabled( true )
            self.state = "off"
        end
    end

    bs.onbutton = widget.newButton({
        label = params.label.on or "",
        labelColor = { default=params.labelColor.on, over=params.labelColor.on },
        fontSize = params.fontSize,
        font = params.font,
        labelXOffset = params.labelXOffset or 0, 
        labelYOffset = params.labelYOffset or 0,
        onEvent = function( event )
            event.name = "turnOff"
            event.state = "on"
            if event.phase == "began" then
                display.getCurrentStage():setFocus( event.target )
            elseif event.phase == "ended" or event.phase == "cancelled" then
                switching( bs, "off" )
                event.state = "off"
            end
            params.listener( event )
        end,
        sheet = params.sheet,
        defaultFrame = params.frameOn,
        overFrame = params.frameOff,               
    })
    bs:insert( bs.onbutton, true )

    bs.offbutton = widget.newButton({
        label = params.label.off or "",
        labelColor = { default=params.labelColor.off, over=params.labelColor.off },
        fontSize = params.fontSize,
        font = params.font,
        labelXOffset = params.labelXOffset or 0, 
        labelYOffset = params.labelYOffset or 0,
        onEvent = function( event )
            event.name = "turnOn"
            event.state = "off"
            if event.phase == "began" then
                display.getCurrentStage():setFocus( event.target )            
            elseif event.phase == "ended" or event.phase == "cancelled" then
                switching( bs, "on" )
                event.state = "on"
            end
            params.listener( event )
        end,
        sheet = params.sheet,
        defaultFrame = params.frameOff,
        overFrame = params.frameOn,               
    })
    bs:insert( bs.offbutton, true )

    function bs:turnOn()
        switching( self, "on" )
    end

    function bs:turnOff()
        switching( self, "off" )
    end

    bs.state = "off"
    switching( bs, "off" )
    if params.initState == "on" then switching( bs, "on" ) end

    return bs
end

function uilib.newBox( width, height, onClose, onEmptySpacePress, isModal )
    print("uilib.newBox isModal", tostring(isModal))
    local isModal=isModal
    if isModal==nil then
        isModal=false
    end
    onClose = onClose or function() end

    local box = display.newGroup( )
        
    box.substrate = display.newRect( _mX, _mY, _Xa, display.actualContentHeight )
    box.substrate:setFillColor( 0,0,0, 0.5 )
    box:insert( box.substrate )
    box.substrate:addEventListener( "touch", function( event )
        if (event.phase == "ended" or event.phase == "cancelled") then 
            if onEmptySpacePress then
                onEmptySpacePress()
            end       
		if isModal==false then
			if box.shouldRecycle then box.isVisible = false
			else box:removeSelf(); box = nil 
			end
                onClose()
                display.getCurrentStage():setFocus( nil )
            end
        end
        return true
    end )

    width = width or _Xa-60
    height = height or _Ya*0.4

    box.bg = display.newRoundedRect( _mX, _mY, width, height, 10 )
    box:insert( box.bg )
    box.bg:addEventListener( "touch", function( event )
        return true
    end )
    box.bg:addEventListener( "tap", function( event )
	return true
  end )    
    
    local prev_keyback = Keyback.focuson
    Keyback:setFocus( nil )
    function box:keyback( event )
        self:removeSelf(); box = nil
        onClose()
        Keyback:setFocus( prev_keyback )
    end
    Keyback:setFocus( box )
 --   print(box.bg.y,box.bg.height)
    return box
end

function uilib.createReloadScreen( reason, onReload )
    local mess = {
        ["error"] = translations["loading_errors"][_G.Lang][1],
        ["denied"] = translations["loading_errors"][_G.Lang][2],
        ["disconnection"] = translations["loading_errors"][_G.Lang][3] 
    }

    local reload = display.newGroup()
    reload.anchorChildren = true
            
    local label = display.newText( { 
        width = 250,
        parent = reload,
        text = mess[reason],
        font = "HelveticaNeueCyr-Light",
        fontSize = 12,
        align = "center"
    } )
    label:setFillColor( 1,0,0, 0.6 )
    local reloadbutn = widget.newButton
    {
        width = 65,
        height = 65,
        defaultFile = "icons/default/reload.png",
        overFile = "icons/over/reload.png",
        onRelease = function( event )
            reload:removeSelf()
            reload = nil
            onReload()
        end
    }
    reload:insert( reloadbutn, true )
    reloadbutn.anchorY = 0
    reloadbutn.y = label.height*0.5 + 10

    reload.x = _mX
    reload.y = _mY

    return reload
end

function uilib.newDateBox( params )
	local w = params.width
	local h = params.height
	local title = params.title or "Title"
	local initdate = params.initDate
	local listener = params.listener or function() end
	local rdbox = uilib.newBox( width, 330, function()	end )

	local pad_regular = 10
	local fsize_item = 15
	
	rdbox.name = display.newText({
		parent = rdbox,
		text = title or "",
		width = rdbox.bg.width*0.8,
		font = "HelveticaNeueCyr-Medium", 
		fontSize = fsize_item,
		align = "center"
	})
	rdbox.name:setFillColor( 0,0,0 )
	rdbox.name.x = rdbox.bg.x
	rdbox.name.anchorY = 0
	rdbox.name.y = rdbox.bg.y - rdbox.bg.height*0.5 + 10
	
	rdbox.separ1 = display.newLine( rdbox, 
		rdbox.bg.x - rdbox.bg.width*0.5, rdbox.name.y + rdbox.name.height + 10,  
		rdbox.bg.x + rdbox.bg.width*0.5, rdbox.name.y + rdbox.name.height + 10
	)
	rdbox.separ1:setStrokeColor( 0,0,0, 0.1 )
	rdbox.separ1.strokeWidth = 1.5			

	local options = {
			frames = 
			{
					{ x=0, y=0, width=320, height=222 },
					{ x=320, y=0, width=320, height=222 },
					{ x=640, y=0, width=8, height=222 }
			},
			sheetContentWidth = 648,
			sheetContentHeight = 222
	}
	local pickerWheelSheet = graphics.newImageSheet( "assets/orders/pickerWheelDate.png", options )

	local function createDatePickerWheel( initdate )
		local days = {}
		local years = {}
		for i = 1,31 do days[i] = i end
		for j = 0,110 do years[j] = 1900 + j end
		local months = translations.Months[_G.Lang]
		for i=1, #years do
			if years[i] == initdate.year then initdate.year = i end
		end
		local columnData = {         -- all columns should add up to 280
			{
				align = "center",
				width = 280*0.125,
				startIndex = 1,
				labels = {""}
			},					
			{
				align = "center",
				width = 280*0.15,
				startIndex = initdate.day,
				labels = days
			},
			{
				align = "center",
				width = 280*0.4,
				startIndex = initdate.month,
				labels = months
			},			
			{
				align = "center",
				width = 280*0.2,
				startIndex = initdate.year,
				labels = years
			},
			{
				align = "center",
				width = 280*0.125,
				startIndex = 1,
				labels = {""}
			},					
		}
			
		local pickerWheel = widget.newPickerWheel {
			x = 0,
			y = 0,
			font = "HelveticaNeueCyr-Light", 
			fontSize = fsize_item,
			columns = columnData,
			columnColor = { 0, 0, 0, 0 },
				sheet = pickerWheelSheet,
				overlayFrame = 1,
				overlayFrameWidth = 320,
				overlayFrameHeight = 222,
				backgroundFrame = 1,
				backgroundFrameWidth = 320,
				backgroundFrameHeight = 222,
				separatorFrame = 3,
				separatorFrameWidth = 8,
				separatorFrameHeight = 222,
		}
		pickerWheel.group = display.newGroup( )
		pickerWheel.group:insert( pickerWheel )
		pickerWheel.group.anchorChildren = true
		pickerWheel.group.x = rdbox.bg.x
		pickerWheel.group.anchorY = 0
		pickerWheel.group.y = rdbox.name.y + rdbox.name.height + 15
		rdbox:insert( pickerWheel.group )
		pickerWheel.months = months

		return pickerWheel
	end
	rdbox.pickerwheel = createDatePickerWheel( initdate )

	rdbox.separ2 = display.newLine( rdbox, 
		rdbox.bg.x - rdbox.bg.width*0.5 + pad_regular, 
		rdbox.pickerwheel.group.y + rdbox.pickerwheel.group.height + 5,  
		rdbox.bg.x + rdbox.bg.width*0.5 - pad_regular, 
		rdbox.pickerwheel.group.y + rdbox.pickerwheel.group.height + 5
	)
	rdbox.separ2:setStrokeColor( 0,0,0, 0.1 )
	rdbox.separ2.strokeWidth = 1.5				

	rdbox.lbut = widget.newButton({
		label = "OK",
		labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
		fontSize = 13.5,
		font = "HelveticaNeueCyr-Medium",
		--properties for a rounded rectangle button...
		shape="roundedRect",
		width = (rdbox.bg.width - pad_regular*3) * 0.4,
		height = 37,
		cornerRadius = 5,
		fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
		strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
		strokeWidth = 2,
		onRelease = function( event )
			local values = rdbox.pickerwheel:getValues()
			local resdate = {}
			resdate.day = values[2].value
			local int, fract = math.modf(values[2].value/10)
			if int == 0 then resdate.day = "0"..values[2].value end
			local months = rdbox.pickerwheel.months
			for i=1, #months do
				if months[i] == values[3].value then
					values[3].value = i
					break
				end
			end
			resdate.month = values[3].value 	
			int, fract = math.modf( values[3].value/10 )
			if int == 0 then resdate.month = "0"..values[3].value end
			resdate.year = values[4].value
			listener({ date = resdate })
			rdbox:removeSelf()
			rdbox = nil
		end	    
	})
	rdbox.lbut.anchorX = 0
	rdbox.lbut.x = rdbox.bg.x - rdbox.bg.width*0.5 + pad_regular
	rdbox.lbut.anchorY = 1
	rdbox.lbut.y = rdbox.bg.y + rdbox.bg.height*0.5 - pad_regular
	rdbox:insert( rdbox.lbut )

	rdbox.rbut = widget.newButton({
			label = translations.cancel[_G.Lang],
			labelColor = { default={ 1, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
			fontSize = 13.5,
			font = "HelveticaNeueCyr-Medium",
			--properties for a rounded rectangle button...
			shape="roundedRect",
			width = (rdbox.bg.width - pad_regular*3) * 0.6,
			height = 37,
			cornerRadius = 5,
			fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
			strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
			strokeWidth = 2,
			onRelease = function( event )
				rdbox:removeSelf()
				rdbox = nil
			end	    
	})
	rdbox.rbut.anchorX = 1
	rdbox.rbut.x = rdbox.bg.x + rdbox.bg.width*0.5 - pad_regular
	rdbox.rbut.anchorY = 1
	rdbox.rbut.y = rdbox.bg.y + rdbox.bg.height*0.5 - pad_regular
	rdbox:insert( rdbox.rbut )		
end

uilib.VerificationPhoneBoxClass = function( params )
	local o = display.newGroup()       -- new object (public members)
	local c = {             				    -- private members
		field_size = params.fieldSize or 10,
		delay = params.delay or 5,
		phone = params.phone or "",
		listener = params.listener or function() end, -- events: sendSms, inputCode

		box = nil,
		label = nil
	}

	local function init()
		c.box = uilib.newBox( _Xa * 0.8, 300 )
		c.box.shouldRecycle = true
		o:insert( c.box )

		local title = display.newText({
			parent = c.box,
			text = translations.phone_verification[userProfile.lang],
			width = c.box.bg.width*0.8,
			font = "HelveticaNeueCyr-Medium", 
			fontSize = 15,
			align = "center"
		})
		title:setFillColor( 0.1 )
		title.x = c.box.bg.x
		title.anchorY = 0
		title.y = c.box.bg.y - c.box.bg.height*0.5 + 11

		local sepr1 = display.newLine( c.box, 
			c.box.bg.x - c.box.bg.width*0.5, title.y + title.height + 10,  
			c.box.bg.x + c.box.bg.width*0.5, title.y + title.height + 10
		)
		sepr1:setStrokeColor( 0,0,0, 0.1 )
		sepr1.strokeWidth = 1.5
		
		local text = translations.will_send_sms[userProfile.lang]
		c.label = display.newText({
			parent = c.box,
			text = string.gsub( text, "#", "+"..c.phone ),
			width = c.box.bg.width*0.82,
			font = "HelveticaNeueCyr-Light", 
			fontSize = 13,
			align = "center"
		})
		c.label:setFillColor( 0.2 )
		c.label.x = c.box.bg.x
		c.label.anchorY = 0
		c.label.y = sepr1.y + 15

		c.btn_send = widget.newButton({
			label = translations.send_sms[userProfile.lang],
			labelColor = { default={ 1, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
			fontSize = 14,
			font = "HelveticaNeueCyr-Medium",
			--properties for a rounded rectangle button...
			shape="roundedRect",
			width = c.box.bg.width * 0.6,
			height = 37,
			cornerRadius = 5,
			fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
			strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
			strokeWidth = 2,
			onRelease = function( event )
				local text = translations.sent_sms[userProfile.lang]
				c.label.text = string.gsub( text, "#", "+"..c.phone, 1 )
				c.label.text = string.gsub( c.label.text, "#", c.delay, 1 )
				c.createTimer()
				c.listener({ name = "sendSms", phone = c.phone, target = o })
			end     
		})
		c.box:insert( c.btn_send, true )
		c.btn_send.x = c.box.bg.x
		c.btn_send.anchorY = 1
		c.btn_send.y = c.box.bg.y + 45
		c.box:insert( c.btn_send )
	
		local sepr2 = display.newLine( c.box, 
			c.box.bg.x - c.box.bg.width*0.5 + 10, 
			c.btn_send.y + c.btn_send.height - 15,  
			c.box.bg.x + c.box.bg.width*0.5 - 10, 
			c.btn_send.y + c.btn_send.height - 15
		)
		sepr2:setStrokeColor( 0,0,0, 0.1 )
		sepr2.strokeWidth = 1.5  	
		
		c.createField()

		o:addEventListener( "finalize", c.finalize )
		o.alpha = 0
		return o
	end

	--- Public Methods ---

	o.show = function( self, phone )
		if self ~= o then return end
		c.phone = phone
		local text = translations.will_send_sms[userProfile.lang]
		c.label.text = string.gsub( text, "#", "+"..c.phone )
		o.alpha = 0
		c.box.isVisible = true
		transition.to( o, { alpha = 1, time = 100 } )
	end

	o.hide = function( self, phone )
		if self ~= o then return end
		transition.to( o, { alpha = 0, time = 100 } )
	end	

	o.update = function( self, text )
		if self ~= o then return end
		c.label.text = string.gsub( text, "#", "+"..c.phone )
	end	

	--- Private Methods ---

	c.createField = function()
		c.field = display.newGroup()
		c.field.anchorChildren = true
		c.field.x = c.box.bg.x
		c.field.y = c.box.bg.y + c.box.bg.height*0.5 - 40
		c.box:insert( c.field )

		local btn = widget.newButton({
			label = translations.ok[userProfile.lang],
			labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
			fontSize = 14,
			font = "HelveticaNeueCyr-Medium",
			--properties for a rounded rectangle button...
			shape="roundedRect",
			width = 40,
			height = 37,
			cornerRadius = 5,
			fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
			strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
			strokeWidth = 2,
			onRelease = function( event )
				c.listener({ name = "inputCode", code = tonumber( c.field.input:getText() ), target = o })
			end     
		})
		c.field:insert( btn, true )
		btn.anchorY = 1
		btn.y = 0
		btn:setEnabled( false ); btn.alpha = 0.5
		c.field:insert( btn )		

		local input = widget.newInputField({
			id = "sms",
			x = 0, y = 0,
			width = c.box.bg.width*0.5,
			height = 35,
			font = "HelveticaNeueCyr-Thin",
			fontSize = 16,
			padLeft = 5, 
			padRight = 5,
			offsetY = 0,
			placeholder = "sms code",
			inputType = "number",--"email",
			typeControl = false,
			symbolsMin = 3,
			symbolsMax = 4,
	
			inputFont = "HelveticaNeueCyr-Light",
			inputOffsetX = -1, -- -0.035,
			inputOffsetY = -2, -- 0.02,

			native_field_xscale = 0.5,
	
			listener = function( event )
				if event.phase == "began" then
					c.field.input.field.y = c.field.input.field.y - _Ya*0.25
					c.box.y = -_Ya*0.25
					c.box.substrate.y = c.box.substrate.y + _Ya*0.25
				elseif event.phase == "editing" then
					btn:setEnabled( #event.text == c.field_size )
					btn.alpha = #event.text == c.field_size and 1 or 0.5 
				elseif event.phase == "submitted" then
					c.listener({ name = "inputCode", code = tonumber( event.text ), target = o })
					c.box.y = 0
					c.box.substrate.y = c.box.substrate.y - _Ya*0.25
				elseif event.phase == "ended" then
					c.box.y = 0
					c.box.substrate.y = c.box.substrate.y - _Ya*0.25
				end
			end
		})
		input.x = btn.x - btn.width - input.width*0.5
		input.y = -btn.height*0.5
		c.field:insert( input )
		c.field.input = input

		input:setDecor({
			allBorders = false,
			--bgColor = bgcolor,		
			bordersWidth = 1,
			bordersColor = {0,0,0, 0.2},
			bottomBorder = { 0, 1 },
			-- leftBorder = { 0, 1 },
			-- rightBorder = { 0, 1 }
		})
	end

	c.createTimer = function()
		if c.timerid then return end

		local label = display.newText({
			parent = c.box,
			text = translations.send_sms[userProfile.lang].." ("..c.delay..")",
			font = "HelveticaNeueCyr-Medium", 
			fontSize = 14,
			align = "center"
		})
		label:setFillColor( 0.5 )
		label.x = c.btn_send.x
		label.y = c.btn_send.y - c.btn_send.height*0.5

		timer.performWithDelay( 5, function( event )
			c.btn_send:setEnabled( false )
			c.btn_send:setLabel( "" ) 
			c.btn_send:setFillColor( 211/255, 211/255, 211/255 ) 
			c.btn_send:setStrokeColor( 0,0,0, 0.13 ) 
			c.timerid = timer.performWithDelay( 1000, function( event )
				if not o then return end
				if c.delay - event.count == 0 then
					label:removeSelf(); label = nil
					c.btn_send:setLabel( translations.send_sms[userProfile.lang] ) 
					c.btn_send:setFillColor( 1,1,1 ) 
					c.btn_send:setStrokeColor( 0,0,0, 0.13 ) 
					c.btn_send:setEnabled( true )
					c.timerid = nil
				else
					label.text = translations.send_sms[userProfile.lang].." ("..( c.delay - event.count )..")"
				end
			end, c.delay )
		end )
	end

	c.finalize = function()
		transition.cancel( o )
		o = nil
	end

	return init()
end

return uilib