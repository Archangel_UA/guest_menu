-- last changes
--		15/07/2014 description of interface
--		15/07/2014 default background of tabs aunder active background
--		16/07/2014 added slide.actualWidth and slide.actualHeight
--			       in the table slide in listeners
--		18/07/2014 smooth movement when user moves slide
--		05/09/2014 add public method setTabLabel;
--				   all listeners get as argument the slideview and current slide
--      08/09/2014 fixed bag slideview with one slide
--      17/09/2014 added separator between tabs
--      17/09/2014 added color label in setTabLabel method
--      27/10/2014 added parametr reversTouch	


--[[                 --- INTERFACE ---

widget.newSlideView( params )  -- creating new slide view and return it
	params {
		x       - X position in content coordinates, 
				  if not specifed will be display.contentWidth*0.5
		y 	    - Y position in content coordinates, 
			      if not specifed will be display.contentHeight*0.5
		width   - the width of slide, 
				   if not specifed then will be display.actualContentWidth
		height   - the height of slide, 
				   if not specifed then will be display.actualContentHeight
		numSlides  - numbers of slide in slide view
		initSlide  		- number of slide? whiche will be showed first 
						  after creatind slide view
		leftPadding     - the distance that user can pull the first slide to the left
		rightPadding    - the distance that user can pull the last slide to the right
		timeMove = 400, - time for which the slide will move on a new position,
						  the lower the value the faster moves ( 1000 - 1 second )
		recycle         - if is true, the slide will be removed from memory 
						  automatically when it lived the queue of slides: prev, curr, next;
						  default is true
		reversTouch     - "down" - touch move up to down by Y-axis will be recognized as forwad motion
	                      "up" - touch move down to up by Y-axis will be recognized as forwad motion
	                      "left" - touch move left to right by X-axis will be recognized as forwad motion
						  "right" - touch move right to left by X-axis will be recognized as forwad motion
						  "right" is default	

		-LISTENES-
			
		loadSlide       
		unloadSlide     
		showSlide 
		hideSlide 
		reachedFirst 
		reachedLast 

		- in each listener will be passed table "slide" - display group of slide content
          also slide have fields:	    
	    	slide.index_number    - number slide, for which was called listeners
	    	slide.params  {       - settings of slide
				autoscale         - if true the content of slide will scalled 
									to fit size of slide view, default is true
				hasBg             - if true, backgroud (rectangle shape ) of the slide
									will be showed, default is false
				bgColor           - color of backgroud, if hasBg is false, will be ignored
				bgStroke {  
					width         - width of the stroke of the background slide
					color         - color of the stroke of the background slide
				}
 			}
 			slide.actualWidth     - actual slide width, equal slide view width
	    	slide.actualHeight    - actual slide height, equal slide view height
	    }
	}

slideView.removeSelf()       - destroying slide view as usual display object

slideView:getSlide( index )  - returns display group of slide content by index (number)
							   of slide if that slide exist, otherwise returns nil

slideView:attachTabBar( general, tabs ) -- attache tab bar to slide view
	- settings for all tab bar
	general {
		y,          -- if not specified - on top of the SlideView
		height,     -- if not specified - height of the SlideView
		bgDefColor,
		bgActColor
	}

	- settings for each tab
	tabs {
		-- first tab, corresponds to first slide
		{
			label,		 -- object display.newText
			labelColor,	 -- { 1,1,1 }, default black
			bgDefault    -- display object (image or shape),
			bgDefColor,  -- if not specified, will be used general bgDefColor, 
							default white
			bgActive     -- display object (image or shape),
			bgActColor   -- if not specified, will be used general bgActColor
							default white
			separetor    -- display object (image or shape),
			separColor   
		}
		-- second tab, corresponds to second slide
		-- etc. 
	}

slideView:setTabLabel( numtab, label, color ) -- set name tab of the slide with specified number

]]-------------------------------------------------------------------------------------- 

local widget = require "widget"

-- DEFAULT LISTENERS --

local function loadSlide( slide )
	local num = slide.index_number
	slide.params = {
		autoscale = false,
		hasBg = true,
		bgColor = { 0.2, 0.3, 0.1 },
		bgStroke = { width = 1.5, color = {0.5, 0, 0} },
	}
	
	local name, descrip
	if num == 1 then
		name = display.newText( "SLIDE 1", 0, -100, native.systemFont, 25 )
		name:setFillColor( 0, 0, 0 )
		descrip = display.newText( "", 0, 0, native.systemFont, 14 )
		descrip.text = " it's demo slide, \n custom slides weren't added"
		descrip:setFillColor( 0, 0, 0 )
		slide:insert( name )		
		slide:insert( descrip )
		slide.params = {
			autoscale = false,
			hasBg = true,
			bgColor = { 0.5, 1, 0.2 },
			bgStroke = { width = 3, color = {0, 0, 0} },
		}
	elseif num == 2 then
		name = display.newText( "SLIDE 2", 0, -100, native.systemFont, 25 )
		descrip = display.newText( "", 0, 0, native.systemFont, 14 )
		descrip.text = " it's demo slide, \n custom slides weren't added"
		slide:insert( name )		
		slide:insert( descrip )		
	elseif num == 3 then
		name = display.newText( "SLIDE 3", 0, -100, native.systemFont, 25 )
		descrip = display.newText( "", 0, 0, native.systemFont, 14 )
		descrip.text = " it's demo slide, \n custom slides weren't added"
		slide:insert( name )		
		slide:insert( descrip )		
	end
end

local function unloadSlide( slide )
end

local function showSlide( slide )
	if not slide.showed_times then slide.showed_times = 1 end
	local times = {}
	if slide.showed_times == 1 then
		times = display.newText( "", 0, 50, native.systemFont, 14 )
		times.text = slide.showed_times.." times showed "
		times:setFillColor( 0,0,0 )		
		slide:insert( times )	
	else slide[4].text = slide.showed_times.." times showed "
	end
	slide.showed_times = slide.showed_times + 1
end

local function hideSlide( slide )
	if slide.index_number > 1 then
		slide.params = {
			bgColor = { 0.8, 0, 0.1 },
			bgStroke = { width = 10, color = {1, 1, 1} },
		}
	end	
end

local function reachedFirst( slide, sView )
	local sb = sView.contentBounds 
	local line = display.newLine( 
		sb.xMin, sb.yMin, 
		sb.xMin, sb.yMax )
	line.strokeWidth = 20
	line.alpha = 0
	
	transition.to( line, { time=1500, alpha=0.4 })
	timer.performWithDelay( 200, function( event )
		line:removeSelf()
	end )
end

local function reachedLast( slide, sView )
	local sb = sView.contentBounds 
	local line = display.newLine( 
		sb.xMax, sb.yMin, 
		sb.xMax, sb.yMax )
	line.strokeWidth = 20
	line.alpha = 0
	transition.to( line, { time=1500, alpha=0.4 })
	timer.performWithDelay( 200, function( event )
		line:removeSelf()
	end )
end


------- WIDGET SlideView --------------------------------------------------

local function newSlideView( params )
	if not params then
		error( " to create slideview you should pass parameters" )
	end

	params.width = params.width or display.actualContentWidth
	params.height = params.height or display.actualContentHeight
	params.x = params.x or display.contentWidth*0.5
	params.y = params.y or display.contentHeight*0.5

	sv = display.newGroup()
	sv.x = params.x
	sv.y = params.y
	
	sv.currindex = params.initSlide or 1
	sv.initslide = params.initSlide or 1
	sv.numslides = params.numSlides or 1

	sv.leftpad = params.leftPadding or 0
	sv.rightpad = params.rightPadding or 0

	sv.timemove = params.timeMove or 400
	
	sv.reversTouch = params.reversTouch or "right"

	if params.recycle == false then sv.recycle = false
	else sv.recycle = true
	end

	sv.container = display.newContainer( params.width, params.height )
	sv:insert( sv.container )
	
	sv.queue = display.newGroup()
	sv:insert( sv.queue )
	sv.queue.x = 0
	sv.queue.y = 0

	sv.touchpad = display.newRect( 0,0, params.width, params.height )
	sv.touchpad.isVisible = false
	sv.touchpad.isHitTestable = true
	sv:insert( sv.touchpad )

	sv.loadSlide = params.loadSlide or loadSlide
	sv.unloadSlide = params.unloadSlide or unloadSlide
	sv.showSlide = params.showSlide or showSlide
	sv.hideSlide = params.hideSlide or hideSlide
	sv.reachedFirst = params.reachedFirst or reachedFirst
	sv.reachedLast = params.reachedLast or reachedLast

	-------- PUBLIC METHODS -------------------------------------------------------	

	function sv:getSlide( index )
		for i=1, self.queue.numChildren do
			if self.queue[i].content.index_number == index then
				return self.queue[i].content
			end
		end
		for i=1, self.container.numChildren do
			if self.container[i].content and 
			self.container[i].content.index_number == index then
				return self.container[i].content
			end
		end

		return nil
	end

	function sv:attachTabBar( general, tabs )
		local gp = general
		local tp = tabs

		if self.numslides > #tp or self.numslides < #tp	then
			print( "WARNING: the number of tabs doesn't correspond to the number of slides" )
		end	
		
		self.tabs_container = display.newContainer( gp.width or self.width, gp.height )
		if gp.width then
			self.tabs_container.x = (gp.width-self.width)*0.5
		end


		self:insert( self.tabs_container )
		
		self.tabs = display.newGroup()
		for i=1, #tp do
			self.tabs:insert( i, display.newGroup() )

			self.tabs[i].id = i
			
			self.tabs[i].bgDefault = tp[i].bgDefault
			local bgDefault = self.tabs[i].bgDefault			
			if bgDefault then
				self.tabs[i]:insert( bgDefault )
				if tp[i].bgDefColor then 
					bgDefault:setFillColor( unpack( tp[i].bgDefColor ) )
				elseif gp.bgDefColor then
					bgDefault:setFillColor( unpack( gp.bgDefColor ) )
				end
			else error( "All tabs should have default background!" )
			end

			self.tabs[i].bgActive = tp[i].bgActive
			local bgActive = self.tabs[i].bgActive
			if bgActive then
				self.tabs[i]:insert( bgActive )
				if tp[i].bgActColor then 
					bgActive:setFillColor( unpack( tp[i].bgActColor ) )
				elseif gp.bgActColor then
					bgActive:setFillColor( unpack( gp.bgActColor ) )			
				end
				bgActive.isVisible = false
			end
			
			self.tabs[i].label = tp[i].label 
			local label = self.tabs[i].label
			if label then
				if  tp[i].labelColor then
					label:setFillColor( unpack( tp[i].labelColor or {1,1,1} ) )
				else label:setFillColor( 0, 0, 0, 1 )
				end
				self.tabs[i]:insert( label )
			end

			self.tabs[i].separator = tp[i].separator
			local sepr = self.tabs[i].separator
			if sepr then
				if tp[i].seprColor then
					sepr:setFillColor( unpack( tp[i].seprColor or {1,1,1,} ) )
				end
				self.tabs[i]:insert( sepr )
			end
			
			if i > 1 then
				bgDefault.anchorX = 0
				bgDefault.x = self.tabs[i-1].bgDefault.x + self.tabs[i-1].bgDefault.width				

				if bgActive then
					bgActive.anchorX = 0
					bgActive.x = bgDefault.x
				end

				if sepr then sepr.x = bgDefault.x end
			else bgDefault.anchorX = 0
				if bgActive then bgActive.anchorX = 0 end
			end
			
			if label then
				label.x = label.x + bgDefault.x + bgDefault.width*0.5
			end
			
			self.tabs[i]:addEventListener( "touch", function( event )
				if event.phase ~= "began" then return end

				local delta = event.target.id - self.currindex
				local timemove = self.timemove
				self.timemove = timemove * 0.3
				if delta > 0 then
					for i=1, delta do
						if i == delta then self.timemove = timemove end
						self:nextSlide()
					end
				elseif delta < 0 then
					for i=1, delta*(-1) do
						if i == delta*(-1) then self.timemove = timemove end					
						self:prevSlide()
					end
				end
				self.timemove = timemove
			end )
		end

		self.tabs_container:insert( self.tabs )
		self.tabs.x = self:placeTabs( self.currindex )
		self.tabs_container.y = gp.y or -self.container.height*0.5 - gp.height*0.5

		if self.tabs[self.currindex].bgActive then
			self.tabs[self.currindex].bgActive.isVisible = true
		end
		self.tabs.currtab = self.currindex
		self:setTabTags()
	end

	function sv:setTabLabel( numtab, label, color )
		if not self.tabs or not self.tabs[numtab] then return end
		self.tabs[numtab].label.text = label or self.tabs[numtab].label.text
		if color then 
			self.tabs[numtab].label:setFillColor( unpack( color ) )
		end
	end

	-----------------------------------------------------------------------------------

	function sv:createSlide( index )
		for i=1, self.container.numChildren do
			if self.container[i].content and 
			self.container[i].content.index_number == index then
				return self.container[i]
			end
		end

		local slide = display.newGroup()
		slide.content = display.newGroup()
		local content = slide.content
		content.index_number = index

		content.actualHeight = self.container.height
		content.actualWidth =  self.container.width
		
		self.loadSlide( content, self )

		local params = content.params or {}

		if params.autoscale == nil then params.autoscale = true end
		if params.autoscale then
			local iw = self.container.width/content.width
			local ih = self.container.height/content.height
			if iw < ih then content:scale( iw, iw )
			else content:scale( ih, ih )
			end
		end
		if params.hasBg then
			params.bgColor = params.bgColor or { 1, 1, 1 }
			slide.bg = display.newRect( 0, 0, self.container.width, self.container.height )
			slide.bg:setFillColor( unpack( params.bgColor ) )
			if params.bgStroke then
				params.bgStroke.width = params.bgStroke.width or 1
				slide.bg.strokeWidth = params.bgStroke.width
				if params.bgStroke.color then 
					slide.bg:setStrokeColor( unpack( params.bgStroke.color ) )
				end
			end
			slide:insert( 1, slide.bg )
		end

		for i=1, content.numChildren do
			if content[i].numChildren then content[i].anchorChildren = true end
		end

		slide:insert( content )
		content.x = 0
		content.y = 0
		self.container:insert( slide )
		
		return slide
	end

	function sv:adjustSlide( slide, mode )
--print( "sv:adjustSlide..." )		
		local content = slide.content
		if mode == "hide" then self.hideSlide( content, self )
		elseif mode == "show" then self.showSlide( content, self )
		end

		local params = content.params
		if not params then return end

		if not slide.bg then
			if params.hasBg	== true then
				slide.bg = display.newRect( 0, 0, self.container.width, self.container.height )
				slide:insert( 1, slide.bg )
			end
		else
			if params.hasBg == false then slide.bg.isVisible = false
			elseif params.hasBg	== true then slide.bg.isVisible = true
			end
		end

		if slide.bg then
			params.bgColor = params.bgColor or { 1, 1, 1 }
			slide.bg:setFillColor( unpack( params.bgColor ) )
			if params.bgStroke then
				params.bgStroke.width = params.bgStroke.width or 1
				slide.bg.strokeWidth = params.bgStroke.width
				if params.bgStroke.color then 
					slide.bg:setStrokeColor( unpack( params.bgStroke.color ) )
				end
			end
		end
	end

	function sv:touch( event )
	--print( "touch...", self, event.target, event.phase )
		local phase = event.phase
		local target = event.target

		local revers = {
			["left"] = display.actualContentWidth - event.x,
			["right"] = event.x,
			["up"] = event.y,
			["down"] = display.actualContentHeight - event.y
		}
		event.x = revers[sv.reversTouch]
				
		if phase == "began" then
	        target.isFocus = true
			if self.tween then transition.cancel( self.tween ) end
			if self.tabs and self.tabs.tween then transition.cancel( self.tabs.tween ) end
			self.startpos = event.x
			self.prevpos = event.x
			self.startmove = false
			return false
	    elseif target.isFocus then
	    	local drag_distance = event.x - self.startpos
			if phase == "moved" then
				local bounds = target.contentBounds
				if event.x < bounds.xMin or event.x > bounds.xMax then
					if drag_distance < -60 and self.nextslide.is then
						self:nextSlide()
					elseif drag_distance > 60 and self.prevslide.is then
						self:prevSlide()
					else
						self:cancelMove()
					end					
					display.getCurrentStage():setFocus( nil )
					target.isFocus = false
					return true
				end

				local delta = event.x - self.prevpos
				if not self.startmove then
					if math.abs( delta ) > 5 then
						self.startmove = true
					end
				end
				
				if self.startmove then
					display.getCurrentStage():setFocus( target )
					if self.numslides == 1 then
						if event.x > self.startpos then
							if event.x - self.startpos <= self.leftpad then
								self.prevpos = event.x
								self.queue.x = self.queue.x + delta
								self:moveTabs()
							end
							if event.x - self.startpos >= self.leftpad then
								self.reachedFirst( self.currslide, self )
							end
						else
							if self.startpos - event.x <= self.rightpad then
								self.prevpos = event.x
								self.queue.x = self.queue.x + delta
								self:moveTabs()
							end
							if self.startpos - event.x >= self.rightpad then
								self.reachedLast( self.currslide, self )
							end	
						end
					elseif not self.nextslide.is then 
						if self.startpos - event.x <= self.rightpad then
							self.prevpos = event.x
							self.queue.x = self.queue.x + delta
							self:moveTabs()
						end
						if self.startpos - event.x >= self.rightpad then
							self.reachedLast( self.currslide, self )
						end
					elseif not self.prevslide.is then
						if event.x - self.startpos <= self.leftpad then
							self.prevpos = event.x
							self.queue.x = self.queue.x + delta
							self:moveTabs()
						end
						if event.x - self.startpos >= self.leftpad then
							self.reachedFirst( self.currslide, self )
						end					
					else
						self.prevpos = event.x
						self.queue.x = self.queue.x + delta
						self:moveTabs()
					end
					native.setKeyboardFocus( nil )
				else return false
				end
			elseif phase == "ended" or phase == "cancelled" then
				if drag_distance < -60 and self.nextslide.is then
					self:nextSlide()
				elseif drag_distance > 60 and self.prevslide.is then
					self:prevSlide()
				else
					self:cancelMove()
				end
				if phase == "cancelled" then		
					self:cancelMove()
				end
	            display.getCurrentStage():setFocus( nil )
	            target.isFocus = false
	   
	            if math.abs( drag_distance ) < 10 then return false end
			end
		end
		return true
	end

	function sv:onComplete( obj )
	--print( "onComplete..." )
		if self.now == "next" then
			self:adjustSlide( self.nextslide, "show" )
			self:adjustSlide( self.currslide, "hide" )
			self:nextQueue( obj )
		elseif self.now == "prev" then
			self:adjustSlide( self.prevslide, "show" )
			self:adjustSlide( self.currslide, "hide" )
			self:prevQueue( obj )
		end	
	end

	function sv:onCancel( obj )
	--print( "onCancel..." )
		if self.now == "next" then
			self:adjustSlide( self.nextslide, "show" )
			self:adjustSlide( self.currslide, "hide" )
			self:nextQueue( obj )
		elseif self.now == "prev" then
			self:adjustSlide( self.prevslide, "show" )
			self:adjustSlide( self.currslide, "hide" )
			self:prevQueue( obj )
		end	
	end

	function sv:firstQueue()
	--print( "firstQueue..." )
		if self.queue.numChildren == 0 then
			self:updateSlidesInQueue()
			if self.currslide.is then 
				self.queue:insert( self.currslide )
				self.currslide.x = 0
				self:adjustSlide( self.currslide, "show" )
			end
			if self.prevslide.is then
				self.queue:insert( self.prevslide )
				self.prevslide.x = -self.container.width  
			end
			if self.nextslide.is then 
				self.nextslide.x = self.container.width 
				self.queue:insert( self.nextslide ) 
			end
			self.container:insert( self.queue )
		end	
	end

	function sv:nextQueue( obj )
	--print( "nextQueue..." )
		local prevslide = self.prevslide
		local currslide = self.currslide
		local nextslide = self.nextslide

		if prevslide.is then
			self.container:insert( prevslide )
			prevslide.x = -self.container.width
		end
		self.container:insert( currslide )
		currslide.x = -self.container.width
		self.container:insert( nextslide )
		nextslide.x = 0
		self.queue.x = self.container.width + obj.x
		
		if self.numslides > self.currindex then
			self.currindex = self.currindex + 1
		end

		self:updateSlidesInQueue()
		self.queue:insert( self.prevslide )
		self.queue:insert( self.currslide )
		if self.nextslide.is then 
			self.queue:insert( self.nextslide )
			self.nextslide.x = self.container.width 
		end
	end

	function sv:prevQueue( obj )
	--print( "prevQueue..." )
		local prevslide = self.prevslide
		local currslide = self.currslide
		local nextslide = self.nextslide	

		if nextslide.is then
			self.container:insert( nextslide )
			nextslide.x = self.container.width
		end
		self.container:insert( currslide )
		currslide.x = self.container.width
		self.container:insert( prevslide )
		prevslide.x = 0
		self.queue.x = self.container.width - obj.x
		
		if self.currindex > 1 then
			self.currindex = self.currindex - 1
		end

		self:updateSlidesInQueue()
		self.queue:insert( self.nextslide )
		self.queue:insert( self.currslide )
		if self.prevslide.is then 
			self.queue:insert( self.prevslide )
			self.prevslide.x = -self.container.width 
		end
	end

	function sv:nextSlide()
	--print( "nextSlide..." )
		self.now = "next"
		self.tween = transition.to( self.queue, {
			time=self.timemove, x=-self.container.width, transition=easing.outExpo, 
			onComplete = self,
	 		onCancel = self
		})

		if self.tabs then
			if self.tabs.currtab < self.numslides then
				self.tabs.currtab = self.tabs.currtab + 1
			end
			local currtab = self.tabs.currtab
			
			if self.tabs and self.tabs.tween then transition.cancel( self.tabs.tween ) end
			local target = self:placeTabs( currtab )
			self.tabs.tween = transition.to( self.tabs, {
				time=self.timemove*0.5, x=target, transition=easing.outExpo,
				onComplete = function( obj )
					self:activateTab( currtab )
					self:setTabTags()
				end,
				onCancel = function( obj )
					self:activateTab( currtab )
					self:setTabTags()
				end  
			})
		end

	end
		
	function sv:prevSlide()
	--print( "prevSlide... " )
		self.now = "prev"
		self.tween = transition.to( self.queue, {
			time=self.timemove, x=self.container.width, transition=easing.outExpo, 
			onComplete = self,
	 		onCancel = self
		})

		if self.tabs then
			if self.tabs.currtab > 1 then
				self.tabs.currtab = self.tabs.currtab - 1
			end
			local currtab = self.tabs.currtab

			if self.tabs and self.tabs.tween then transition.cancel( self.tabs.tween ) end
			local target = self:placeTabs( currtab )
			self.tabs.tween = transition.to( self.tabs, {
				time=self.timemove*0.5, x=target, transition=easing.outExpo,
				onComplete = function( obj )
					self:activateTab( currtab )
					self:setTabTags()
				end,
				onCancel = function( obj )
					self:activateTab( currtab )
					self:setTabTags()
				end  
			})
		end
	end
		
	function sv:cancelMove()
	--print( "cancelMove..." )
	    if self.tween then transition.cancel( self.tween ) end
		self.tween = transition.to( self.queue, {
			time=400, x=0, transition=easing.outExpo 
		})

		if self.tabs then
			if self.tabs and self.tabs.tween then transition.cancel( self.tabs.tween ) end
			local target = self:placeTabs( self.tabs.currtab )
			self.tabs.tween = transition.to( self.tabs, {
				time=400, x=target, transition=easing.outExpo 
			})
			self:activateTab( self.tabs.currtab )			
		end
	end

	function sv:updateSlidesInQueue()
	--print( "updateSlidesInQueue..." )
		local slide = {}
		
		if self.now == "next" then
			if self.prevslide.is then 
				self.unloadSlide( self.prevslide.content, self )
				if not self.prevslide.content then self.prevslide:removeSelf( ) end
				if self.recycle then self.prevslide:removeSelf( ) end 
			end
			self.prevslide = self.currslide
			self.currslide = self.nextslide
			if self.numslides > self.currindex then
				slide = self:createSlide( self.currindex + 1 )	
				self.nextslide = slide
				self.nextslide.is = true
			else self.nextslide = { is = false }
			end		
		elseif self.now == "prev" then
			if self.nextslide.is then 
				self.unloadSlide( self.nextslide.content, self )
				if not self.nextslide.content then self.nextslide:removeSelf( ) end
				if self.recycle then self.nextslide:removeSelf( ) end 
			end				
			self.nextslide = self.currslide
			self.currslide = self.prevslide
			if self.currindex > 1 then
				slide = self:createSlide(self.currindex - 1)
				self.prevslide = slide
				self.prevslide.is = true
			else self.prevslide = { is = false }
			end
		else
			-- prevslide
			if self.currindex > 1 then
				slide = self:createSlide(self.currindex - 1)
				self.prevslide = slide
				self.prevslide.is = true
			else self.prevslide = { is = false }
			end
			
			-- currslide	
			if self.currindex > 0 then 
				slide = self:createSlide( self.currindex )
				self.currslide = slide
				self.currslide.is = true
			else self.currslide.is = false
			end

			-- nextslide
			if self.numslides > self.currindex then
				slide = self:createSlide(self.currindex + 1)		
				self.nextslide = slide
				self.nextslide.is = true
			else self.nextslide = { is = false }
			end
		end
	end

	function sv:placeTabs( index )
		if index < 1 or index > self.numslides then
			return nil
		end

		local x = 0
		if self.numslides == 1 then
			x = - self.tabs[1].width*0.5
			return x
		end 
		if index == 1 then 
			if self.tabs_container.width ~= self.width then 
				x = -self.tabs_container.width*0.5
			else
				x = -self.width*0.5
			end
			return x
		elseif index == self.numslides then 
			if self.tabs_container.width ~= self.width then 
				x = self.tabs_container.width*0.5 - self.tabs.width
			else
				x = self.width*0.5 - self.tabs.width
			end
			return x
		end
		for i=1, index do
			x = x - self.tabs[i].width*0.5
			if i > 1 then x = x - self.tabs[i-1].width*0.5
			end
		end

		return x
	end

	function sv:activateTab( acttab )
		for i=1, self.tabs.numChildren do
			if self.tabs[i].bgActive then
				self.tabs[i].bgActive.isVisible = false
			end
		end

		if acttab > 0 and acttab <= self.numslides then 
			if self.tabs[acttab].bgActive then
				self.tabs[acttab].bgActive.isVisible = true
			end 
		end
	end

	function sv:setTabTags()
--print( "sv:setTabTags..." )		
		if not self.tabs then return end

		self.tags = {}
		self.tags.left = {}
		self.tags.right = {}
		local left = self.tags.left
		local right = self.tags.right
		local bounds_tab = {}
		local bounds_sv = self.contentBounds
		local currtab = self.tabs.currtab

		-- set tags for moving slide view to right
		left.init = bounds_sv.xMin
		if currtab == 1 then
			left.pad = bounds_sv.xMin + self.leftpad
		elseif currtab == 2 then
			bounds_tab = self.tabs[currtab].contentBounds
			left.push = bounds_tab.xMin
			bounds_tab = self.tabs[currtab-1].contentBounds
			left.target = left.push + bounds_sv.xMin - bounds_tab.xMin
		else bounds_tab = self.tabs[currtab].contentBounds
			left.push = bounds_tab.xMin
			left.target = bounds_sv.xMin + (self.width*0.5 + self.tabs[currtab-1].width*0.5)
		end

		-- set tags for moving slide view to left
		right.init = bounds_sv.xMax	
		if currtab == self.numslides then
			right.pad = bounds_sv.xMax - self.rightpad
		elseif currtab == self.numslides - 1 then
			bounds_tab = self.tabs[currtab].contentBounds 
			right.push = bounds_tab.xMax
			bounds_tab = self.tabs[currtab+1].contentBounds
			right.target = right.push - ( bounds_tab.xMax - bounds_sv.xMax )
		else bounds_tab = self.tabs[currtab].contentBounds
			right.push = bounds_tab.xMax
			right.target = bounds_sv.xMax - (self.width*0.5 + self.tabs[currtab+1].width*0.5)
		end
	end

	function sv:catch( pointer, direct )
		if direct == "left" then
			local tags = self.tags.left

			if tags.pad then
				if pointer >= tags.init and pointer <= tags.pad then return true
				else return false
				end
			end		
			if pointer >= tags.push and pointer <= tags.target then
				self:activateTab( self.tabs.currtab )
				return true
			end
			if pointer > tags.target then
				self:activateTab( self.tabs.currtab-1 )
			end
			
			return false

		elseif direct == "right" then
			local tags = self.tags.right

			if tags.pad then
				if pointer >= tags.pad and pointer <= tags.init then return true
				else return false
				end				
			end		
			if pointer <= tags.push and pointer >= tags.target then
				self:activateTab( self.tabs.currtab )
				return true
			end
			if pointer < tags.target then
				self:activateTab( self.tabs.currtab+1 )
			end

			return false			
		end
	end

	function sv:moveTabs()
		if not self.tabs or self.tabs.numChildren == 1 then return end

		function converter( index, side, value )
			local x = self:placeTabs( 1 )
			for i=2, index do
				x = x - self.tabs[i-1].width
			end
			if side == "right" then x = x - self.tabs[index].width end
			return x + value - self.tags.left.init
		end

		local currtab = self.tabs.currtab		
		local q = self.queue
		local bounds = q.contentBounds
		local pleft = bounds.xMin + self.width
		if currtab == 1 then pleft = bounds.xMin end
		local pright = bounds.xMax - self.width
		if currtab == self.numslides then pright = bounds.xMax end

		local catchedl = self:catch( pleft, "left" )
		local catchedr = self:catch( pright, "right" )

		if catchedl then
			self.tabs.x = converter( currtab, "left", pleft )
		elseif catchedr then
			self.tabs.x = converter( currtab, "right", pright )
		end
	end

	-------------------------------------------------------------------------------
	
	sv.touchpad:addEventListener( "touch", sv )
	sv:firstQueue()
	
	return sv
end

widget.newSlideView = newSlideView