local function newDateView(params)
	params = params or {}
	-- local  isIgnoreEnded = true
	local  isIgnoreTap = true
    local coef=0.4    
    --local width = params.width or mW*0.9
	local width = params.width or 650*coef
    --local height = params.height or width*1100/650
    local height = params.height or 1100*coef
    local x = params.x or cX
	local y = params.y or cY

	local colorLItDef = params.colorLItDef or {0,0,0,0.8}
	local colorLItAct = params.colorLItAct or css.color_font_enter_auth

	local gr_root = display.newGroup()
	local grSub = display.newGroup()
	grSub.alpha = 0
	local shield = display.newRect( gr_root,cX, cY, mW, mH )
    shield:setFillColor(0,0,0,0.3)
    -- shield.alpha=0
    -- shield.isHitTestable = true
    -- shield:setFillColor(1,0.5,0.5,0.1)

    local isMove = false
    shield:addEventListener( "touch", function(e)
    	if e.phase=="began" then
    		isMove = false
    		if grSub then
                if params.fHide then
                    params.fHide()
                else
    			     grSub:removeSelf()
    			     grSub=nil
                end
    		end
    	elseif e.phase=="moved"then
    		isMove = true
    	elseif e.phase=="ended" then
		    if  isMove then
    			if params.fHide then
                    params.fHide()
                else
                  gr_root:removeSelf( )
                  gr_root=nil
                end
            else
                isIgnoreTap = false
    		end
	    	isMove = false
    	end 
     return true end )
    shield:addEventListener( "tap", function(e)
    	if isIgnoreTap then
    		isIgnoreTap = false
    	else
            if params.fHide then
                params.fHide()
            else
	    	  gr_root:removeSelf( )
	    	  gr_root=nil
            end
	    end
    	return true
    end)
    gr_root:insert(grSub)

	local bg = display.newRect( grSub,x,y,width, height) 
	bg:addEventListener( "touch", function() return true end )
	bg:addEventListener( "tap", function()  return true end )
	bg:setFillColor(unpack(css.color_green))

    local h_top_b = 80*coef
    local bg_top = display.newRect(grSub,x,y-height*0.5,width, h_top_b)
    bg_top.anchorY=0
    bg_top:setFillColor(unpack(css.color_green_2))

    local bg_date = display.newRect(grSub,x,y+bg.height*0.5-(bg.height*0.65)*0.5, bg.width, bg.height*0.65)
    bg_date:setFillColor(unpack(css.light_gray))
    ui_elm:drawShadow(grSub,bg)
    

    local grCon = display.newContainer(grSub,bg_date.width, bg_date.height )
    grCon.x, grCon.y=bg_date.x, bg_date.y
    local grMonths = display.newGroup()
    grMonths.x, grMonths.y = 0,0
    grCon:insert(grMonths)

    local dt = os.date("*t") -- начальная инициализация 
    if params.dateStr then
        local y,m,d = string.match(params.dateStr, '(%d+)%-(%d+)%-(%d+)')
        y = tonumber(y) 
        m = tonumber(m) 
        d = tonumber(d)
        dt.year, dt.month, dt.day = y,m,d
        dt=os.date("*t",os.time(dt)) 

    end
    -- print( "WWWWWWWWWWWWWWWWWWWWWWWWWWWW" )
    -- for k, v in pairs(dt) do
    --     print( k,v )
        
    -- end
    -- print( "WWWWWWWWWWWWWWWWWWWWWWWWWWWW" )

    local function getCountDayMonth(y,m) -- количество дней в месяце
        local n = 0
        if m~=2 then
            if m > 7 then
                m = m+1
            end
            if (m%2) == 1 then
                n = 31
            else
                n = 30
            end          
        else
            if (not (y%400>0 ) or not (y%4>0)) and y%25>0 then
                n=29
            else
                n=28
            end
        end
        return n
    end

    local step = width/8 -- шаг сетки календаря
    local l_top_day_w = display.newText(grSub, "", bg_top.x, bg_top.y+bg_top.height*0.5, native.systemFont, css.size_font_3*coef)
    local l_top_day_m = display.newText(grSub, "", bg_top.x, bg.y-height*0.5+height*0.15+bg_top.height-15, native.systemFont, 160*coef)
    local l_top_m = display.newText(grSub, "", bg_top.x, bg_top.y+bg_top.height, native.systemFont, 50*coef)
    l_top_m.anchorY=0
    local l_top_y = display.newText(grSub, "", bg_top.x, l_top_day_m.y+l_top_day_m.height*0.5+25, native.systemFont, 50*coef)

    grMonths.act_id=0
    grMonths.curActObj = nil  
    grMonths.lAct = nil  
    grSub.f_move = false
    local function createMonthView(dt)
        local grM = display.newGroup()
        grM.dt=dt
        local yr,m = dt.year, dt.month
        local countDay = getCountDayMonth(yr,m)

        local xp, yp = 0, 0
        local l_m = display.newText(grM, trans("Months",m).."  "..yr, xp+width*0.5-step,-step, native.systemFontBold, 40*coef)
        l_m:setFillColor(0.5)

        for i=1,7 do
            local l =  display.newText( grM,string.utf8sub(trans("Days",i),0,1), xp+(i-1)*step, yp,native.systemFont, 30*coef)
            l:setFillColor(unpack(colorLItDef))
        end
        yp = yp+step
        local options = {
            width = 48,
            height = 48,
            numFrames = 2,
            sheetContentWidth = 96,
            sheetContentHeight = 48
        }
        local radioButtonSheet = graphics.newImageSheet( "images/b_cl.png", options )
        local labelsDays = {}

        local d_temp = {}
        for k, v in pairs(dt) do
            d_temp[k]=v
        end
        d_temp.year=yr
        d_temp.month=m
        d_temp.min=0
        d_temp.sec=0
        d_temp.hour=0
        d_temp.day=1
        d_temp = os.date("*t",os.time(d_temp))
        xp =xp+(d_temp.wday-1)*step
        local function onSwitchPressDay( e )
            local phase = e.phase
            if "began" == phase then
                e.target:setState({isOn=false})
                if grMonths.curActObj then
                   grMonths.curActObj:setState({isOn=true})
                end
            elseif "moved" == phase then
                 -- grSub.f_move = true
            elseif "ended" == phase or "cancelled" == phase then
                if  grSub.f_move  then
                    grSub.f_move = false
                    return
                end
                local switch = e.target
                switch:setState({isOn=true})
                if grMonths.act_id > 0  then
                    grMonths.act_id=switch.id
                    local d = os.date("*t",switch.id )
                    l_top_day_w.text = trans("Days",d.wday)
                    l_top_day_m.text = d.day
                    l_top_m.text = trans("Months",d.month)
                    l_top_y.text = d.year
                    if  grMonths.curActObj then
                        grMonths.curActObj:setState({isOn=false})
                        if grMonths.curActObj.l then
                            grMonths.curActObj.l:setFillColor(unpack(colorLItDef))
                        end
                    end

                    grMonths.curActObj = switch
                    grMonths.curActObj:setState({isOn=true})
                    if grMonths.curActObj.l then
                        grMonths.curActObj.l:setFillColor(unpack(colorLItAct))
                    end
                end
            end
        end

        for i=1,countDay do
            d_temp.day=i
            local l =  display.newText( {parent=grM,text=i,x=xp,y=yp,font=native.systemFont,fontSize=26*coef, align="center"})
            l:setFillColor(unpack(colorLItDef))
            local id = os.time(d_temp)
            labelsDays[id]=l
            local bDay = widget.newSwitch(
                {
                    x = xp,
                    y = yp,
                    style = "radio",
                    id =id,
                    width = 60*coef,
                    height = 60*coef,
                    onEvent = onSwitchPressDay,
                    -- onPress = function() end,
                    -- onRelease = onSwitchPressDay,
                    sheet = radioButtonSheet,
                    frameOff = 1,
                    frameOn = 2
                }
            )
            grM:insert( bDay )
            bDay.l=l
            if grMonths.act_id == 0 and dt.year == d_temp.year and dt.month == d_temp.month and dt.day == d_temp.day  then
                grMonths.act_id=id
                bDay:setState({isOn=true})
                l_top_day_w.text = trans("Days",dt.wday)
                l_top_day_m.text = dt.day
                l_top_m.text = trans("Months",dt.month)
                l_top_y.text = dt.year
                grMonths.curActObj = bDay
                -- grMonths.lAct=l
            end 
            xp = xp+step
            if  xp >= width-step  then
                yp=yp+step
                xp=0
            end
        end
        return grM
    end
    -- start init ======================================================

    local g = createMonthView(dt)
    if g then
        grMonths:insert(g)
        g.x, g.y = grMonths.x-bg_date.width*0.5+step,-bg_date.height*0.5+1.8*step
    end

    local temp_dt = {}
    for k, v in pairs(dt) do
        temp_dt[k]=v
    end
    for i=1,6 do
        temp_dt.month=temp_dt.month-1
        temp_dt=os.date("*t",os.time(temp_dt))
        local g = createMonthView(temp_dt)
        if g then
            grMonths:insert(g)
            g.x, g.y = grMonths.x-bg_date.width*0.5-bg_date.width*i+step,-bg_date.height*0.5+1.8*step
        end
    end
    temp_dt = {}
    for k, v in pairs(dt) do
        temp_dt[k]=v
    end
    for i=1,6 do
        temp_dt.month=temp_dt.month+1
        temp_dt=os.date("*t",os.time(temp_dt))
        local g = createMonthView(temp_dt)
        if g then
            grMonths:insert(g)
            g.x, g.y=grMonths.x+bg_date.width*0.5+step+bg_date.width*(i-1),-bg_date.height*0.5+1.8*step
        end
    end
    -- start init ======================================================

    local swipeLayer = display.newRect(grCon, 0, 0, bg_date.width, bg_date.height)
    swipeLayer.alpha=0 --make it transparent
    -- swipeLayer:setFillColor(1,0,0,0.2)
    swipeLayer.isHitTestable = true
    
    local function startDrag(event)
        local swipeLength = math.abs(event.x - event.xStart) 
        local t = event.target
        local phase = event.phase
        if "began" == phase then
            grSub.f_move= false
            -- return true
        elseif "moved" == phase then
             if swipeLength > 10 then
                grSub.f_move = true
            end
        elseif "ended" == phase or "cancelled" == phase then
            if event.xStart > event.x and swipeLength > 50 then 
                print("Swiped Left")
                transition.to( grMonths, {time=400, x=grMonths.x-bg_date.width})
            elseif event.xStart < event.x and swipeLength > 50 then 
                transition.to( grMonths, {time=400, x=grMonths.x+bg_date.width})
                print( "Swiped Right" )
            end 
        end
    end
    swipeLayer:addEventListener("touch", startDrag)

    local btnOk = widget.newButton(
        {
            x = bg_date.x+bg_date.width*0.5-40*coef,
            y = bg_date.y+bg_date.height*0.5-40*coef,
            label = trans("ok"),
            labelColor = { default=css.color_b1_def, over=css.color_b1_act },
            fontSize = 36*coef,
            fillColor = { default={ 0, 0, 0, 0 }, over={ 0, 0, 0, 0 } },
            shape = "rect",
            width=40,
            onRelease = function(e)
                 if params.onSet then
                    params.onSet(grMonths.act_id)
                end
                if params.fHide then
                    params.fHide()
                else
                  gr_root:removeSelf( )
                  gr_root=nil
                end
            end
        })
    btnOk.anchorX = 1
    btnOk.anchorY = 1
    grSub:insert( btnOk )

    local btnCancel = widget.newButton(
        {
            x = btnOk.x-btnOk.width-20*coef,
            y = bg_date.y+bg_date.height*0.5-40*coef,
            label = trans("Cancel"),
            fillColor = { default={ 0, 0, 0, 0 }, over={ 0, 0, 0, 0 } },
            shape = "rect",
            labelColor = { default=css.color_b1_def, over=css.color_b1_act },
            fontSize = 36*coef,
            onRelease = function(e)
                if params.fHide then
                    params.fHide()
                else
                  gr_root:removeSelf( )
                  gr_root=nil
                end
            end
        })
    btnCancel.anchorX = 1
    btnCancel.anchorY = 1
    grSub:insert(btnCancel)

    transition.to( grSub, { time=200, alpha=1.0 } )
    grSub.height=bg.height
    grSub.width=bg.width
	return grSub, gr_root
end
widget.newDateView = newDateView