--[[         -- ORDER VIEW LIBRARY --

-- METHODS --

findLocalActiveOrder( com_id )
newOrdersViewManager( orders_status )    - orders_status: "active", "inactive", "all"
newOrderView( id )
newOrderViewGUI( order_view )

]] -------------------------------------------------------------

widget.setTheme( "widget_theme_ios7" )
-- 
local rdb = require "lib.db_remote"
local rdb_common = require "lib.db_remote_common"
local rate = require "ui.rate"
local uilib = require "ui.uilib"
local json = require("json")
require "ui.SlidingPanel"

local ov = {}

---------------- General Methods ----------------------------

local function collectData( self )
	-- print("!!!!!!!!!!!!!! collectData",self.lid,userProfile.activeOrderID)
	self.lid=self.lid or userProfile.activeOrderID
	-- print( "collectData", self.lid)	
	local id = self.lid
	local orders = ldb:getRowsData( "orders","id="..id )
	local items = ldb:getRowsData( "order_items","local_ord_id="..id )
	-- printResponse(items)
	if orders==nil then
		orders={}
	end	
	if items==nil then
		items={}
	end	

		for i=1, #orders do 
			if orders[i].id == id then
				self.odata = orders[i]
				self.cid=self.odata.ord_id
	--			print(self.cid)
				self.oidata = {}
				for j=1, #items do
					if items[j].local_ord_id == id and items[j].ordi_count>0 then
						self.oidata[#self.oidata+1] = items[j]
					end 
				end
				
				break
			end
		end

	--printResponse(self.oidata)
end

function ov:findLocalActiveOrder( com_id, pos_id )
	local guest_id = ldb:getSettValue( "guest_id" )
		if not guest_id then return 0 end
		local local_orders = ldb:getRowsData( "orders", "com_id="..com_id..[[ AND (ord_state<>3 and ord_reserv_state<>3) and pos_id=]]..pos_id..[[ AND guest_id=]]..guest_id..[[ ORDER BY id DESC ]], "id, ord_state, ord_reserv_state" )

		if local_orders then
				for i=1, #local_orders do
						if ( local_orders[i].ord_state == 0 or local_orders[i].ord_state > 3 ) 
							and local_orders[i].ord_reserv_state ~= 1 then
							 return local_orders[i].id
						end
				end
		end

	 return 0
end

---------------- Orders View Manager ----------------------------

local function initCreatingOrdersViews( self )
print( "call" )
	self.oviews = {}
	local oviews = self.oviews

	local orders 
	if self.show_closed==true then
		print("SHOW WITH CLOSED ORDERS")
		orders = ldb:getRowsData( "orders", "guest_id="..self.guest_id..[[ ORDER BY id DESC ]] ) --ord_state<>3 and 
	else
		orders = ldb:getRowsData( "orders", "(ord_reserv_state<>3) and guest_id="..self.guest_id..[[ ORDER BY id DESC ]] ) --ord_state<>3 and 
	end
	local ids = {}
	local limits = self.limits[self.ostatus]
	local date_limit = {}
	local today = Date( false )
	local flag = true

	if not orders then
		self.onCreateDone( self ) 
		return 
	end

	for i=1, #orders do 
		flag = true		
		if limits.state_exclude then
			for j=1, #limits.state_exclude do
				if orders[i].ord_state == limits.state_exclude[j] then
					flag = false
					break
				end
			end
		end
		if flag and limits.term_hours ~= 0 then
			if orders[i].ord_closed then
				date_limit = Date( orders[i].ord_closed )
				date_limit:addhours( math.abs( limits.term_hours ) )				
				if limits.term_hours < 0 then
					if today > date_limit then
						flag = false
					end
				elseif limits.term_hours > 0 then
					if today < date_limit then
						flag = false
					end
				end	
			end
		end

		if self.pos_id then
			if  orders[i].pos_id ~= self.pos_id then flag = false end
		end

		if flag or ( self.show_closed and orders[i].pos_id == self.pos_id ) then
			oviews[#oviews+1] = ov:newOrderView( orders[i].id )
			oviews[#oviews].onOrderDeleted = function( ov )
				self.onOrderDeleted( self, ov )
				self:refresh()
			end
			oviews[#oviews].onOrderSent = function( ov )
				self.onOrderSent( self, ov )
			end
			oviews[#oviews].onCopied = self.onOrderCopied
		end
	end

	self.onCreateDone( self )
end

local function updateLocalOrdersItems( self )
	print( "updateLocalOrdersItems...", system.getTimer() )
	local res = false
	local starttime = system.getTimer()	
	local cl_oitems = self.cloud_orders
	local loc_oitems = ldb:getColumnsValues( "order_items", { "id", "ord_id", "ordi_id", "order_item_version" } )
	local fields_oi = ldb:getFieldsNames( "order_items" )
	local prepdata, rows, were = {}, {}, {}
	local for_save = {}
	if #cl_oitems == 0 and #loc_oitems < 2 then return end -- если таблицы пустые
	for i=1, #cl_oitems do
		if cl_oitems[i].ordi_id and cl_oitems[i].ordi_id ~= ""  -- не пустышка
		and were[cl_oitems[i].ordi_id] == nil 	-- еще не проверяли 
		and cl_oitems[i].lang_code == userProfile.lang then -- соответствует локализации
			for_save[cl_oitems[i].ordi_id] = true 	-- помечаем что он актуален
			for j=1, #loc_oitems do
				if loc_oitems[j].ord_id == cl_oitems[i].ord_id 
				and loc_oitems[j].ordi_id == cl_oitems[i].ordi_id then
					-- если версии не совпадают, значит было обновление данных
					if tonumber( cl_oitems[i].order_item_version or 0 ) ~= tonumber( loc_oitems[j].order_item_version ) then 
						-- print(tonumber(loc_oitems[j].order_item_version),tonumber( cl_oitems[i].order_item_version ))							
						res = true
						local field, value
						local row_id = loc_oitems[j].id				
						prepdata = {}
						for z=1, #fields_oi do
							field = fields_oi[z]
							value = cl_oitems[i][field]
							prepdata[field] = value
						end
						ldb:updateRowData( "order_items", row_id, prepdata )
					end
					were[cl_oitems[i].ordi_id] = true
					break
				end
			end
			-- возможно новый
			if not were[cl_oitems[i].ordi_id] then 
				-- проверяем существует ли заказ к которому привязан item
				rows = ldb:getRowsData( "orders", "ord_id="..cl_oitems[i].ord_id, "id" ) 
				if rows then
					prepdata = {}
					for k,v in pairs( cl_oitems[i] ) do
						for q=1, #fields_oi do
							if k == fields_oi[q] then prepdata[k] = v end
						end
					end
					prepdata.local_ord_id = rows[1].id
					ldb:addNewRowData( "order_items", prepdata )
					res = true
				end 
			end
		end
	end
	-- удаляем не актуальные
	local for_del = nil
	for i=1, #loc_oitems do
		if not for_save[loc_oitems[i].ordi_id] and loc_oitems[i].ordi_id > 0 then 
			for_del = ( not for_del and "" or for_del..", ")..loc_oitems[i].ordi_id
		end
	end
	if for_del then 
		ldb:delRows("order_items"," (ordi_id in ("..for_del..")) and ordi_id<>0 ")
		res = true 
	end
	-- print( "[OrderView]: end func", system.getTimer()-starttime, for_del )
	return res
end

local function getCloudOrderItems( self, ord_id )
	self.guest_id = ldb:getSettValue( "guest_id" )
	local oivers = ldb:getColumnsValues( "order_items", { "order_item_version" } )
	
	local function netListener( mess, rows )
		if mess == "error" then
		elseif mess == "success" then
			self.cloud_items = rows
			updateLocalOrdersItems( self )
		elseif mess == "denied" then
			initCreatingOrdersViews( self )
		end
	end

	local filter = {
		group = "and",
		conditions = {
			{ 
				left = "guest_id",
				oper = "=",
				right = self.guest_id
			}	
		}
	}
	for i=1, #oivers do
		if oivers[i].order_item_version and oivers[i].order_item_version > 0 then
			filter.conditions[#filter.conditions+1] = {
				left = "order_item_version",
				oper = "<>",
				right = oivers[i].order_item_version
			}
		end
	end

	rdb.sign = 0	
	rdb:getData( "vw_orders_full", netListener, filter )
end

local function updateLocalOrders( self )
print( "updateLocalOrders...", system.getTimer() )
	local starttime = system.getTimer()	
	
	local cl_orders = self.cloud_orders
	local keyfields = ldb:getColumnsValues( "orders", { "id", "ord_id", "order_version" } )
	local fields_o = ldb:getFieldsNames( "orders" )

	local flag = false
	local prepdata = {}
	local were = {}
	local updated, removed, added = {}, {}, {}
	if #cl_orders ~= 0 then 
		for i=1, #cl_orders  do
			flag = false
			if not were[cl_orders[i].ord_id] then			
				for j=1, #keyfields do
					if keyfields[j].ord_id == cl_orders[i].ord_id then
						if keyfields[j].order_version ~= tonumber( cl_orders[i].order_version )  then
							print("ORDER UPDATE ",cl_orders[i].ord_state,keyfields[j].ord_id)
							if cl_orders[i].ord_state == 4 then
							-- or ( cl_orders[i].ord_state == 3 and cl_orders[i].ord_reserv_state == 3 ) then
								ldb:delRows( "order_items", "local_ord_id="..keyfields[j].id )
								ldb:delRow( "orders", keyfields[j].id )
								removed[cl_orders[i].ord_id] = cl_orders[i]
							else
								-- if cl_orders[i].ord_state == 2 then
								-- 	ldb:delRows( "order_items", "local_ord_id="..keyfields[j].id )
								-- end
								local field, value
								local row_id = keyfields[j].id				
								prepdata = {}						
								for z=1, #fields_o do
								--	print(field,value)
									field = fields_o[z]
									value = cl_orders[i][field]
									prepdata[field] = value
								end
								prepdata["ord_create"] = cl_orders[i]["ord_create_txt"]
								prepdata["ord_reserv_datetime"] = cl_orders[i]["ord_reserv_datetime_txt"]
								prepdata["ord_closed"] = cl_orders[i]["ord_closed_txt"]
								prepdata["ord_reserv_state"] = cl_orders[i]["ord_reserv_state"]
								ldb:updateRowData( "orders", row_id, prepdata )
								updated[cl_orders[i].ord_id] = prepdata
							end
						end
						flag = true
						break
					end
				end
				-- print(cl_orders[i].ord_state,cl_orders[i].ord_reserv_state,flag)
				if not flag and cl_orders[i].ord_state ~= 4 
				and not ( cl_orders[i].ord_state == 3 and cl_orders[i].ord_reserv_state == 3 ) then
					prepdata = {}
					for k,v in pairs( cl_orders[i] ) do
						for q=1, #fields_o do
							if k == fields_o[q] then prepdata[k] = v end
						end
					end
					prepdata.id = ldb:addNewRowData( "orders", prepdata ) 
					added[cl_orders[i].ord_id] = prepdata
				end
				
				were[cl_orders[i].ord_id] = true
			end	
		end
	end
	return { removed = removed, updated = updated, added = added }
	-- collectData( self )	
	-- print( "[OrderView - 209]: end func", system.getTimer()-starttime )
end

local function getCloudOrders( self, listener )
print( "getCloudOrders..." )	
	self.guest_id = ldb:getSettValue( "guest_id" )
	local overs = ldb:getColumnsValues( "orders", { "order_version" } )
	
	local function netListener( mess, rows )
		if mess == "success" then
			self.cloud_orders = rows
			updateLocalOrders( self )
			updateLocalOrdersItems( self )
			initCreatingOrdersViews( self )
		--	collectData( self )
		elseif mess == "denied" then
			updateLocalOrders( self )
			updateLocalOrdersItems( self )
			initCreatingOrdersViews( self )
		else self.onDataLoadError( self, mess )
		end
	end
	netListener = listener or netListener

	local filter = {
		group = "and",
		conditions = {
			{ 
				left = "guest_id",
				oper = "=",
				right = self.guest_id
			},
			{left= "ord_create",oper= ">=",right= "'"..os.date("%Y.%m.%d",os.time( os.date( '*t' ))-60*60*24*5  ).."'"}
		}
	}
	if self.pos_id then
		table.insert( filter.conditions, {
			left = "pos_id",
			oper = "=",
			right = self.pos_id
		})
	end

	rdb.sign = 0	
	rdb:getData( "vw_orders_full", netListener, filter )

	--jen adding load of deleted orders

	 local function getDeletedOrders(mess, rows)
	 	
	 	if mess == "success" then
	 		print("ERROR LOADING ORDER !!!")
	 	

	-- 		-- print( 'OOOOOOOOOOOOOOOOOOOO ammount', #data.rows)
			local m=0
	 		for i,item in pairs(rows) do
	 			-- print("deleted order ",item.ord_id)
	 			if item.version==nil or item.version=="null" then
	 				item.version=0
	 			end
	 			m=math.max( m, tonumber(item.version) )
	 			ldb:updateRowDataWithCondition("orders","ord_id="..item.ord_id,{ord_state=7})
	 		end
	 		if m~=0 then
	 			initCreatingOrdersViews( self )
	 		end
	 		ldb:setSettValue("orders_deleted_version",m)
	 	end
	 end
	 local filter=""

	 	local maxVer = tonumber(ldb:getSettValue( "orders_deleted_version" ) or 0)
	-- 	--print("!!!!!!!!!!!!!!!!!!!!!!!!!!! "..maxVer)
		

	 		filter = {group= "and", conditions= {{left= "pos_id",oper= "=",right= self.pos_id},
	 	          								{left= "version",oper= ">",right= maxVer},}}
	 
	 	rdb:getData("orders_deleted",getDeletedOrders,filter)
	

end

------------------

function ov:newOrdersViewManager( params )   -- "active", "inactive", "all"
print( "init" )
	local new_ovm = {
		ostatus = params.ordersType,
		pos_id = params.posID,
		show_closed=false,
		guest_id = nil,
		cloud_orders = {},
		cloud_items = {},
		oviews = {},
		limits = {},
		onCreateWill = params.onCreateWill,
		onCreateDone = params.onCreateDone,
		onUpdateStart = params.onUpdateStart,
		onUpdateEnded = params.onUpdateEnded,
		onOrderSent = params.onOrderSent,
		onOrderDeleted = params.onOrderDeleted,
		onOrderSelected = params.onOrderSelected,
		onOrderCopied = params.onOrderCopied,
		onDataLoadError = params.onDataLoadError
	}

	new_ovm.limits = {
		["active"] = {
			term_hours = -24,               -- less than 24 hours
			state_exclude = { 4 }
		},
		["inactive"] = {
			term_hours = 24,                -- more than 24 hours 
			state_exclude = { 0,1,2,4,5 }
		},
		["all"] = {
			term_hours = 0,
			state_exclude = nil
		}
	}

	function new_ovm:createOrdersViews()   
print( "call" )
		getCloudOrders( new_ovm )
	end

	function new_ovm:deleteAllOrdersViews( )
		for i=1, #self.oviews do
			self.oviews[i]:remove()
		end
		self.oviews = {}
	end

	function new_ovm:refresh()
		self:deleteAllOrdersViews()
		initCreatingOrdersViews( self )
	end

	function new_ovm:backgroundUpdate(showSpinner)
		if showSpinner == nil then showSpinner = false end
		print( log( "new cycle orders background update" ) )
		if showSpinner then
			Spinner:start()
		end
		getCloudOrders( self, function( mess, rows )
			if showSpinner then
				Spinner:stop()
			end
			if mess == "success" then
				print("on success getCloudOrders backgroundUpdate")
				local prev_orders_data = self.cloud_orders
				self.cloud_orders = rows
				local ochanged = updateLocalOrders( self )
				local ichanged = updateLocalOrdersItems( self )
				if ichanged then print( log("changed orders items") ) end
				-- если изменились позиции заказа или
				-- если хоть один заказ был удален или добавлен обновляем все вью
				if ichanged or next( ochanged.removed ) or next( ochanged.added ) then 
					self:refresh()
				-- если изменились только состояния заказа то обновляем каждое вью отдельно
				elseif next( ochanged.updated ) then
					local updated = nil
					for i=1, #self.oviews do
						updated = ochanged.updated[tonumber( self.oviews[i].cid )]
						if updated then
							print( log( "order "..self.oviews[i].cid.." state changed --"..updated.ord_state ) )
							self.oviews[i].odata.ord_state = updated.ord_state
							self.oviews[i].odata.ord_is_completed = updated.ord_is_completed
							self.oviews[i].odata.ord_reserv_state = updated.ord_reserv_state
							if self.oviews[i].gui then  -- если вью сейчас отображается на экране
								self.oviews[i].gui:onChangeOrderState( self.oviews[i] )
							end							
						end
					end

						-- for j=1, #self.oviews do
						-- 	if tonumber( rows[i].ord_id ) ==  tonumber( self.oviews[j].cid )
						-- 	and ( tonumber( rows[i].ord_state ) ~= tonumber( self.oviews[j].odata.ord_state )
						-- 	or tostring( rows[i].ord_is_completed ) ~= tostring( self.oviews[j].odata.ord_is_completed ) ) then
						-- 		if rows[i].ord_state == 4 then
						-- 			return self:refresh()
						-- 		else 
						-- 			self.oviews[j].odata.ord_state = rows[i].ord_state
						-- 			self.oviews[j].odata.ord_is_completed = rows[i].ord_is_completed
						-- 			if self.oviews[j].gui then  -- если вью сейчас отображается на экране
						-- 				self.oviews[j].gui:onChangeOrderState( self.oviews[j] )
						-- 			end
						-- 		end
						-- 	end
						-- end
				end
			end
			print( log("background updating completed") )
		end )
	end
	
	return new_ovm
end

----------------- Order View ----------------------------------------------------

local function prepareToSend( data, table_name, isnew )
	local prepdata = {}
	
	local expt = { "id", "ord_id","ord_sum", "ord_prev_reject_comment","ord_reserv_comment","ord_guest_comment","ord_comment" }
	if not isnew then expt = { "id", "ord_prev_reject_comment" } end
	if table_name == "order_items" then
		expt = { "id", "ordi_id", "local_ord_id" }
		if not isnew then expt = { "id", "local_ord_id" } end
	end

	local flag = true
	for k,v in pairs( data ) do
		flag = true
		for i=1, #expt do
			if k == expt[i] then 
				flag = false
				break 
			end
		end
		if flag then prepdata[k] = v end
		if k=="id" then prepdata["local_id"]=v end
 	end

	return prepdata
end

local function changeOrderState( self, state )
	print( "changeOrderState..." )	
	local function netListener( mess )
		print("onChangeOrderState",mess)
		if mess == "error" then
		
		elseif mess == "success" then
			self.odata.ord_state = state
			if self.odata.ord_type == 2 then --or self.odata.ord_type == 3 
				self.odata.ord_reserv_state = state
			else self.odata.ord_reserv_state = 0
			end			
			ldb:updateRowData( "orders", self.lid, {
				ord_state = state,
				ord_reserv_state = self.odata.ord_reserv_state
			} )
			
			self.onChangeOrderState( self )
		elseif mess == "denied" then

		end
	end

	if self.odata.ord_state ~= state then
		local data = { 
			["ord_id"] = self.cid,
			["ord_state"] = state,
		}
		if self.odata.ord_type == 2 or self.odata.ord_type == 3 then
			data["ord_reserv_state"] = state
		else data["ord_reserv_state"] = 0
		end
		rdb.sign = self.odata.com_id
		rdb:updateRowData( "orders", data, netListener )		
	end
end

local function onItemSent( self )
	print(self.saved_order_items,#self.oidata)
	if self.saved_order_items == #self.oidata then
		changeOrderState( self, 1 )
		print("onItemSent")
		self:onOrderSent()
	elseif self.sended_order_items == #self.oidata then
		self.onThrowError( self, "not sent" )
	end
end

local function sendItem( self, datas,i,listener )
	-- printResponse(datas)
	if i==#datas+1 then listener(); return end
	local data = datas[i]
	local function netListener( mess, id )
		if mess == "disconnection" then
		elseif mess == "error" then
			self.sended_order_items = self.sended_order_items + 1
		elseif mess == "success" then
			if id then
				data.ordi_id = id
				ldb:setFieldValue( "order_items", data.id, "ordi_id", id )
				ldb:updateRowDataWithCondition( "order_items","ordi_modifier_owner_row_id="..data.id, {ordi_modifier_owner_row_id=id} )
				for k=i, #datas do
					if datas[k].ordi_modifier_owner_row_id==data.id then
						datas[k].ordi_modifier_owner_row_id=id 
					end
				end
			end
			if self.sended_order_items==nil then
				self.sended_order_items=0
			end
			if self.saved_order_items==nil then
				self.saved_order_items=0
			end			
			self.sended_order_items = self.sended_order_items + 1
			self.saved_order_items = self.saved_order_items + 1
			sendItem( self, datas,i+1,listener )
		elseif mess == "denied" then
			self.sended_order_items = self.sended_order_items + 1
		end
		onItemSent( self )
	end
	
	if data.ordi_id == 0 then
		data.ord_id = self.cid
		ldb:setFieldValue( "order_items", data.id, "ord_id", self.cid )
		local datatosend = prepareToSend( data, "order_items", true )
		rdb.sign = self.odata.com_id
		rdb:addNewRowData( "order_items", datatosend, netListener )
	else 
		local datatosend = prepareToSend( data, "order_items", false )
		rdb.sign = self.odata.com_id
		rdb:updateRowData( "order_items", datatosend, netListener )
	end
end

function ov:newOrderView( id, listeners )
	listeners = listeners or {}
	local new_ov = {
		lid = 0,
		cid = 0,
		odata = {},
		oidata = {},
		gui = {},
		onOrderSent = listeners.onOrderSent or function() end,
		onOrderDeleted = listeners.onOrderDeleted or function() end,
		onChangeOrderState = listeners.onChangeOrderState or function() end,
		onThrowError = listeners.onThrowError or function() end,
	}

	function new_ov:updateOrder( listener )
		print( "updateOrder..." )
		local function netListener( mess, rows )
			if mess == "disconnection" then
				self.onThrowError( self, "no internet")
			elseif mess == "error" then
				self.onThrowError( self, "not updated")
			elseif mess == "success" then
				local prev_ordstate = self.odata.ord_state
				local new_ordstate = nil
				if rows[1].order_version ~= self.odata.order_version then
					local fields_o = ldb:getFieldsNames( "orders" )
					local fields_oi = ldb:getFieldsNames( "order_items" )		
					local field, value
					local prepdata = {}						
					
					for i=1, #fields_o do
						field = fields_o[i]
						value = rows[1][field]
						prepdata[field] = value
						self.odata[field] = value
					end
					ldb:updateRowData( "orders", self.lid, prepdata )
					new_ordstate = self.odata.ord_state

					local id, num
					for j=1, #rows do
						prepdata = {}
						id, num = nil, nil						
						for z=1, #self.oidata do
							if self.oidata[z].ordi_id == rows[j].ordi_id then
								id = self.oidata[z].id 
								num = z
								break
							end
						end
						if not num then
							self.oidata[#self.oidata+1] = {}
							num = #self.oidata
						end 
						for i=1, #fields_oi do
							field = fields_oi[i]
							value = rows[j][field]
							prepdata[field] = value
							self.oidata[num][field] = value
						end
						if id then
							ldb:updateRowData( "order_items", id, prepdata )
						else ldb:addNewRowData( "order_items", prepdata )
						end
					end
				end
				if new_ordstate and new_ordstate ~= prev_ordstate then
					self.onChangeOrderState( self )
				end
				listener( new_ordstate )
			elseif mess == "denied" then
				self.onThrowError( self, "not updated")
			end
		end		
		
		if self.cid ~= 0 then
			local filter = {
				group = "and",
				conditions = {
					{ 
						left = "ord_id",
						oper = "=",
						right = self.cid
					}	
				}
			}

			rdb.sign = self.odata.com_id	
			rdb:getData( "vw_orders_full_tablet", netListener, filter )
		end
	end

	function new_ov:sendOrder()
		print( "sendOrder..." )
		local function netListenerExtra( mess, id )
			if mess == "disconnection" then
				self.onThrowError( self, "no internet" )
			elseif mess == "error" then
				self.onThrowError( self, "not sent" )
			elseif mess == "success" then
				--print("?????????????????????")
			elseif mess == "denied" then
				self.onThrowError( self, "not sent" )
			end
		end		
		local function netListener( mess, id )
			if mess == "disconnection" then
				self.onThrowError( self, "no internet")
			elseif mess == "error" then
				self.onThrowError( self, "not sent")
			elseif mess == "success" then
				-- print("%%%%%%%%%%%%%%%%%%%%%%%%%%%")
				if id then
					self.cid = id
					self.odata.ord_id = id
					ldb:setFieldValue( "orders", self.lid, "ord_id", id )
					--Jen sending extra data
			        local tA = {street=self.odata.ord_comment,house='',entrance='',floor='',apartment=''}
			        local jsonAddress = json.encode(tA)					
					rdb:addNewRowData( "order_extra_data", {['ord_id'] = id, ['pos_id'] = self.odata.pos_id,['ord_longitude'] = self.odata.ord_longitude, ['ord_address'] = jsonAddress, ['ord_latitude'] = self.odata.ord_latitude}, netListenerExtra )					
				end
				self.sended_order_items = 0
				self.saved_order_items = 0
				--for i=1, #self.oidata do
					--print("!!!!!!!!!!!!!!!sendItems!!!!!!!!!!!!!!!!!2")
					sendItem( self, self.oidata,1,function() self.gui.spinner:stop() end )
				--end
				--self.gui.spinner:stop()
			elseif mess == "denied" then
				self.onThrowError( self, "not sent")
			end
		end		
		
		if self.cid == 0 then
			ldb:setOrderSum( self.odata.id )
			local ord_sum=0
			for i=1, #self.oidata do
				ord_sum=ord_sum+self.oidata[i].ordi_sum
			end

			local datatosend = prepareToSend( self.odata, "orders", true )
			datatosend.ord_sum=ord_sum
			local pos_id = userProfile.SelectedRestaurantData.com_id 
			datatosend.ord_discount_holder=userProfile.balance[pos_id]
			datatosend.ord_comment=self.odata.ord_comment
			datatosend.ord_reserv_comment=self.odata.ord_reserv_comment
			datatosend.ord_guest_comment=self.odata.ord_guest_comment
			rdb.sign = datatosend.com_id
			--JEN get_enable_guest
			rdb:addNewRowData( "orders", datatosend, netListener )
			rdb_common:getDataFromView{listener=function() end,func_call=1,table_name="enable_guest",filter=self.odata.guest_id}
		else 
			local datatosend = prepareToSend( self.odata, "orders", false )
			rdb.sign = datatosend.com_id
			rdb:updateRowData( "orders", datatosend, netListener )
		end

		if self:type() == 1 then 
			ldb:saveAddress( self.odata.ord_comment )
		end
	end

	function new_ov:sendReserv()
	-- print( "sendReserv..." )

		local function netListener( mess, id )
			if mess == "disconnection" then
				self.onThrowError( self, "no internet" )
			elseif mess == "error" then
				self.onThrowError( self, "not sent" )
			elseif mess == "success" then
				if id then
					self.cid = id
					self.odata.ord_id = id
					ldb:setFieldValue( "orders", self.lid, "ord_id", id )

				end
				self.odata.ord_state = 0
				self.odata.ord_reserv_state = 1
				ldb:updateRowData( "orders", self.lid, {
					ord_state = 1,
					ord_reserv_state = 1
				} )
				--for i=1, #self.oidata do
					print("!!!!!!!!!!!!!!!sendItem!!!!!!!!!!!!!!!!!1")
					sendItem( self, self.oidata,1,function() 
						self.onChangeOrderState( self )
						self.onOrderSent( self ) 
					end  )
			--	end				
				
			elseif mess == "denied" then
				self.onThrowError( self, "not sent" )
			end
		end		
		
		if self.cid == 0 then
			local datatosend = prepareToSend( self.odata, "orders", true )
			datatosend.ord_state = 1
			datatosend.ord_reserv_state = 0
			datatosend.ord_reserv_comment=self.odata.ord_reserv_comment
			datatosend.ord_guest_comment=self.odata.ord_guest_comment
			rdb.sign = datatosend.com_id
			rdb:addNewRowData( "orders", datatosend, netListener )
		else 
			local datatosend = prepareToSend( self.odata, "orders", false )
			datatosend.ord_state = 1
			datatosend.ord_reserv_state = 0
			rdb.sign = datatosend.com_id
			rdb:updateRowData( "orders", datatosend, netListener )
		end
	end	

	function new_ov:closeOrder( onClosed )
		print("new_ov:closeOrder")
		local function netListener( mess, id )
			if mess == "disconnection" then
				self.onThrowError( self, "no internet" )
			elseif mess == "error" then
				self.onThrowError( self, "not deleted" )
			elseif mess == "success" then
				self.gui.spinner:stop()
				self.odata.ord_reserv_state = 3
				ldb:updateRowData( "orders", self.lid, {
					ord_reserv_state = 3
				} )
				self.onChangeOrderState( self )
				onClosed()
			elseif mess == "denied" then
				self.onThrowError( self, "not deleted" )
			end
		end

		local datatosend = {
			["ord_id"] = self.odata.ord_id,
			["ord_reserv_state"] = 3,
			["ord_emenu_state"] = 3
		}
		rdb.sign = self.odata.com_id
		rdb:updateRowData( "orders", datatosend, netListener )
	end	

	function new_ov:deleteOrder()
		local function netListener( mess, id )
			if mess == "disconnection" then
				self.onThrowError( self, "no internet" )
			elseif mess == "error" then
				self.onThrowError( self, "not deleted" )
			elseif mess == "success" then
				ldb:delRows( "order_items", "local_ord_id="..self.lid )
				ldb:delRow( "orders", self.lid )
				if self.gui then 
					self.gui:delete()
					self.gui = nil 
				end
				self:remove()
				self.onOrderDeleted( self )
			elseif mess == "denied" then
				self.onThrowError( self, "not deleted" )
			end
		end		
		
		if self.cid ~= 0 then
			local datatosend = prepareToSend( self.odata, "orders", false )
			datatosend.ord_state = 4
			datatosend.ord_reserv_state = 4
			rdb.sign = datatosend.com_id
			rdb:updateRowData( "orders", datatosend, netListener )
		else 
			ldb:delRows( "order_items", "local_ord_id="..self.lid )
			ldb:delRow( "orders", self.lid )
			self:remove()
			self.onOrderDeleted( self )
		end		
		
	end

	function new_ov:cancelReserv()
		local function netListener( mess, id )
			if mess == "disconnection" then
				self.onThrowError( self, "no internet" )
			elseif mess == "error" then
				self.onThrowError( self, "not deleted" )
			elseif mess == "success" then
				self.odata.ord_reserv_state = 4
				ldb:updateRowData( "orders", self.lid, { ["ord_reserv_state"] = 4 } )
				self.onChangeOrderState( self )
			elseif mess == "denied" then
				self.onThrowError( self, "not deleted" )
			end
		end		
		
		if self.cid ~= 0 then
			local datatosend = prepareToSend( self.odata, "orders", false )
			datatosend.ord_reserv_state = 4
			rdb.sign = datatosend.com_id
			rdb:updateRowData( "orders", datatosend, netListener )
		end	
	end

	function new_ov:setItemCount( id, count )
		local sum, price
		for i=1, #self.oidata do
			if self.oidata[i].id == id then
				price = self.oidata[i].ordi_price
				self.oidata[i].ordi_count = count
				self.oidata[i].ordi_sum = tonumber( price ) * tonumber( count )
				sum = self.oidata[i].ordi_sum
			end
		end		
		ldb:updateRowData( "order_items", id, {
			ordi_count = count,
			ordi_sum = sum
		})
	end

	function new_ov:deleteItem( id, item_only )
		local newoidata = {}
		local item_only=item_only
		if item_only==nil then
			item_only=false
		end
		local gi_id
		for i=1, #self.oidata do
			if self.oidata[i].id ~= id then
				newoidata[#newoidata+1] = self.oidata[i]
			else
				gi_id = self.oidata[i].gi_id
			end
		end
		self.oidata = newoidata	
		ldb:delRow( "order_items", id )
		if item_only==false then
		--del modifiers
			local to_del={}
			print("del modifiers",gi_id)
			for i=1, #self.oidata do
		--		print(self.oidata[i].ordi_base_gi_id)			
				if self.oidata[i].ordi_base_gi_id == gi_id then
					to_del[#to_del+1]=self.oidata[i].id					
				end
			end

			for i=1, #to_del do
				new_ov:deleteItem(to_del[i])
			end
		end	
	end	

	function new_ov:changeOrderType( type_num )
		-- print("new_ov:changeOrderType ",type_num,self.lid)
		self.odata.ord_type = type_num
		ldb:setFieldValue( "orders", self.lid, "ord_type", type_num )
	end

	function new_ov:sortItems()
		local sorted = {}
		local oidata = self.oidata
		local item_id = 0
		local grpname, itmname
		local inext = 0

		local function getNames( id )
			local item = ldb:getRowsData( "menu", "mi_id="..id, "mi_name, parent_id" )
			if not item then return "No group", nil end
			local group = ldb:getRowsData( "menu", "mi_id="..item[1].parent_id, "mi_name" )
			if not group then return "No group", item[1].mi_name end
			return group[1].mi_name, item[1].mi_name
		end

		for i=1, #oidata do
			item_id = oidata[i].mi_id
			grpname, itmname = getNames( item_id )
			if not itmname then itmname = oidata[i].mi_name end
			if itmname then
				if not sorted[grpname] then sorted[grpname] = {} end
				inext = #sorted[grpname]+1
				sorted[grpname][inext] = oidata[i]
				oidata[i].mi_name = itmname
			end
		end

		return sorted
	end	

	function new_ov:getOrderState()
		-- print("new_ov:getOrderState")
		local stateOrd = self.odata.ord_state
		local resstate = self.odata.ord_reserv_state
		local state = stateOrd
		if stateOrd == 0 and resstate > 0 then
			state = resstate
			if resstate == 2 then state = 6 end
		end 
		if resstate==3 and stateOrd ~= 3 then
			state = 3 -- оплачен
		end
		if tostring(self.odata.ord_is_completed)=="true" then --jen
			state = 3
		end 
		if resstate==3 and stateOrd == 3 then
			state = 2 -- подтвержден
		end
		
		return state
	end

	function new_ov:getRestName()
		local restid = self.odata.com_id
		local posid = self.odata.pos_id
		for i=1, #RestoransList do
			if RestoransList[i].com_id == restid and RestoransList[i].pos_id == posid then 
				return RestoransList[i].title
			end
		end

		return "No name"
	end

	function new_ov:getRestLogoParams()
		local restid = self.odata.com_id
		local posid = self.odata.pos_id
		local params = {}
		for i=1, #RestoransList do
			if RestoransList[i].com_id == restid and RestoransList[i].pos_id == posid then 
				params.file = RestoransList[i].logo
				params.width = RestoransList[i].logo_width
				params.height = RestoransList[i].logo_height
				return params
			end
		end

		return nil
	end

	function new_ov:getOrderSum()
		local sum = 0
		for i=1, #self.oidata do
			sum = sum + tonumber( self.oidata[i].ordi_sum )
		end
		-- print(self.odata.ord_discount_sum)
		if self.odata.ord_discount_sum~=nil then
			sum=sum-self.odata.ord_discount_sum
		end
		return sum
	end

	function new_ov:getReservParams( format )
		local resparams = {}
		local datetime = self.odata.ord_reserv_datetime
		if datetime then
			local date = Date( datetime )
			if format then
				resparams.date = date:fmt( "%d.%m.%Y" )
				resparams.time = date:fmt( "%H:%M" )
			else
				resparams.date = {}
				resparams.date.day = date:getday()
				resparams.date.month = date:getmonth()
				resparams.date.year = date:getyear()
				resparams.time = {}
				resparams.time.min = date:getminutes()
				resparams.time.hour = date:gethours()
			end
		end
		resparams.persons = self.odata.ord_reserv_number
		resparams.comment = self.odata.ord_guest_comment --28.10.16
		resparams.address = self.odata.ord_comment
		return resparams
	end

	function new_ov:getRestMess( current )
		current = current == nil and true or current
		if current then return self.odata.ord_reject_comment
		else return self.odata.ord_prev_reject_comment
		end
	end

	function new_ov:getOrderDates( format )
		local dates = {}
		dates.create = Date( self.odata.ord_create )
		dates.create = dates.create and dates.create:fmt( format )
		dates.closed = self.odata.ord_closed and Date( self.odata.ord_closed ) or nil
		dates.closed = dates.closed and dates.closed:fmt( format ) or nil
		return dates
	end

	function new_ov:setRestMessIsRead()
		local current = self.odata.ord_reject_comment
		ldb:setFieldValue( "orders", self.lid, "ord_prev_reject_comment", current )
	end

	function new_ov:setReservParams( params )
		local fields = { "ord_reserv_number", "ord_reserv_datetime", "ord_guest_comment" }
		local data = {}
		for k,v in pairs( params ) do
			if k == "persons" then  
				data.ord_reserv_number = v
				self.odata.ord_reserv_number = v 
			elseif k == "date" and self.odata.ord_type>=1 then 
				if self.odata.ord_reserv_datetime then
					local date = Date( self.odata.ord_reserv_datetime )
					v[#v+1] = date:getminutes()
					v[#v+1] = date:gethours()
				end
				data.ord_reserv_datetime = Date( unpack(v) ):fmt("%Y-%m-%d %H:%M:%S")--JENDATECHANGE Date( unpack(v) ):fmt("${iso}.%\fZ")
				self.odata.ord_reserv_datetime = data.ord_reserv_datetime
			elseif k == "time" and self.odata.ord_type>=1 then
				if self.odata.ord_reserv_datetime then
					local date = Date( self.odata.ord_reserv_datetime )
					local h, m = v[1], v[2]
					v = {}
					v[#v+1] = date:getyear()
					v[#v+1] = date:getmonth()
					v[#v+1] = date:getday()
					v[#v+1] = h
					v[#v+1] = m
				end
				data.ord_reserv_datetime =Date( unpack(v) ):fmt("%Y-%m-%d %H:%M:%S")--JENDATECHANGE  Date( unpack(v) ):fmt("${iso}.%\fZ")
				self.odata.ord_reserv_datetime = data.ord_reserv_datetime
			elseif k == "comment" then
				data.ord_guest_comment = v
				self.odata.ord_guest_comment = v
			elseif k == "address" then
				data.ord_comment = v
				self.odata.ord_comment = v
			elseif k == "long" then
				data.ord_longitude = v
				self.odata.ord_longitude = v
			elseif k == "lat" then
				data.ord_latitude = v
				self.odata.ord_latitude = v								
			end
		end
		ldb:updateRowData( "orders", self.lid, data )
	end

	function new_ov:setTipsPerc( v )
		self.odata.ord_fee_percent  = v
		ldb:setFieldValue( "orders", self.lid, "ord_fee_percent", v )
		-- print("new_ov:setTipsPerc",v)
	end

	function new_ov:getTipsPerc( n )
		return self.odata.ord_fee_percent 
	end			

	function new_ov:copyOrder()
			local neword_id = ldb:createNewOrder( self.odata.ord_type )
			local copy_data = {}
			copy_data["ordi_create"] = {}
			for i=1, #self.oidata do
				for k,v in pairs( self.oidata[i] ) do
					if not copy_data[k] then copy_data[k] = {} end
					copy_data[k][i] = v
				end
 				
				copy_data["local_ord_id"][i] = neword_id
				copy_data["ordi_create"][i] = os.date('%Y-%m-%d %H:%M:%S', os.time(os.date( "*t" )))--JENDATECHANGE Date():fmt('${iso}')
			end
			copy_data["id"] = nil
			copy_data["ord_id"] = nil
			copy_data["ordi_id"] = nil
			copy_data["order_item_version"] = nil

			local index = ldb:addNewRowData( "order_items", copy_data, #self.oidata )
			self.onCopied()
	end
	
	function new_ov:callWaiter( ... )
		-- print( "callWaiter..." )
	end

	function new_ov:type()
		return tonumber( self.odata.ord_type )
	end

	function new_ov:remove()
		if self.gui then 
			pcall( function()
				self.gui:delete()
			end )
			self.gui = nil
		end
		self = nil
	end

	new_ov.lid = id
	if not ldb:checkExistRow( "orders", id ) then return end
	new_ov.cid = ldb:getFieldValue( "orders", id, "ord_id" )


	collectData( new_ov )
	new_ov.cid = new_ov.odata.ord_id

	return new_ov
end		

----------------- Order View GUI ----------------------------------------------------

local _Xa = display.actualContentWidth
local _Ya = display.actualContentHeight - display.topStatusBarContentHeight
local _X = display.contentWidth
local _Y = display.contentHeight - display.topStatusBarContentHeight
local _mX = display.contentWidth*0.5 -- display.screenOriginX*0.5
local _mY = display.contentHeight*0.5 -- display.screenOriginY*0.5


function ov:newOrderViewGUI( ordview, lang )
	print( "newOrderViewGUI..."	)	
	local new_ovg = display.newGroup()
	new_ovg.ordview = ordview
	if not ordview then 
		return
	end
	ordview.gui = new_ovg
	new_ovg.lang = lang
	new_ovg.trash = {}
	new_ovg.trash.count = 0

	local currstate = ordview:getOrderState()

	new_ovg:addEventListener( "finalize", function( event )
		ordview.gui = nil
		new_ovg = nil
	end )

	function new_ovg:panelsController( panel, action )
		-- print(panel.id,action)		
		if self.panelInFocus ~= panel then return end
		if action == "show" then
			if panel.id == "info" then
				if self.rspanel then
					self.rspanel:hide(0)
					self.rspanel.y = self.rspanel.down_pos+1000
				elseif self.delivpanel then
					self.delivpanel:hide(0)
					self.delivpanel.y = self.delivpanel.down_pos+1000
				end
				self.itmpanel:hide(0)
				self.itmpanel.y = self.itmpanel.down_pos+1000
			elseif panel.id == "res" then
				self.infopanel:hide(0)
				self.rspanel.y = self.rspanel.up_pos
				self.itmpanel:hide(0)
				self.itmpanel.y = self.itmpanel.down_pos+1000
			elseif panel.id == "deliv" then
				self.infopanel:hide(0)
				self.delivpanel.y = self.delivpanel.up_pos
				self.itmpanel:hide(0)
				self.itmpanel.y = self.itmpanel.down_pos+1000
			elseif panel.id == "itm" then
				self.infopanel:hide(0)
				if self.rspanel then
					self.rspanel:hide(0)
					self.rspanel.y = self.rspanel.up_pos
				elseif self.delivpanel then
					self.delivpanel:hide(0)
					self.delivpanel.y = self.delivpanel.up_pos						 
				end
				self.itmpanel.y = self.itmpanel.up_pos
			end
		elseif action == "hide" then
			if panel.id == "info" then
				if self.rspanel then self.rspanel.y = self.rspanel.up_pos end
				if self.delivpanel then self.delivpanel.y = self.delivpanel.up_pos end
				self.itmpanel.y = self.itmpanel.up_pos
				self.itmpanel:show()
			elseif panel.id == "res" then
				self.itmpanel.y = self.itmpanel.up_pos
				self.itmpanel:show()
			elseif panel.id == "deliv" then
				self.itmpanel.y = self.itmpanel.up_pos
				self.itmpanel:show()
			elseif panel.id == "itm" then
				self.itmpanel.y = self.itmpanel.down_pos+1000
				if self.rspanel then self.rspanel:show()
				elseif self.delivpanel then self.delivpanel:show()
				else self.infopanel:show()
				end
			end
		end
		panel.isFocus = false
	end	

	function new_ovg:createBox( width, height, onClose )
		-- local box = display.newGroup( )
		
		-- box.substrate = display.newRect( _mX, _mY, _Xa, _Ya )
		-- box.substrate:setFillColor( 0,0,0, 0.5 )
		-- box:insert( box.substrate )
		-- box.substrate:addEventListener( "touch", function( event )
		-- 	if event.phase == "ended" or event.phase == "cancelled" then
		-- 		onClose()
		-- 		box:removeSelf()
		-- 		box = nil
		-- 		display.getCurrentStage():setFocus( nil )
		-- 	end
		-- 	return true
		-- end )

		-- width = width or _Xa-60
		-- height = height or _Ya*0.4
		-- box.bg = display.newRoundedRect( _mX, _mY, width, height, 10 )
		-- box:insert( box.bg )
		-- box.bg:addEventListener( "touch", function( event )
		-- 	return true
		-- end )

		return uilib.newBox( width, height, onClose )
	end

	function new_ovg:createDateBox( panel, title, w, h )
		local rdbox = self:createBox( w, 330, function()	end)	
		
		rdbox.name = display.newText({
			parent = rdbox,
			text = title or "",
			width = rdbox.bg.width*0.8,
			font = self.font_bold, 
			fontSize = self.fsize_item,
			align = "center"
		})
		rdbox.name:setFillColor( 0,0,0 )
		rdbox.name.x = rdbox.bg.x
		rdbox.name.anchorY = 0
		rdbox.name.y = rdbox.bg.y - rdbox.bg.height*0.5 + 10
		
		rdbox.separ1 = display.newLine( rdbox, 
			rdbox.bg.x - rdbox.bg.width*0.5, rdbox.name.y + rdbox.name.height + 10,  
			rdbox.bg.x + rdbox.bg.width*0.5, rdbox.name.y + rdbox.name.height + 10
		)
		rdbox.separ1:setStrokeColor( 0,0,0, 0.1 )
		rdbox.separ1.strokeWidth = 1.5			

		local options = {
				frames = 
				{
						{ x=0, y=0, width=320, height=222 },
						{ x=320, y=0, width=320, height=222 },
						{ x=640, y=0, width=8, height=222 }
				},
				sheetContentWidth = 648,
				sheetContentHeight = 222
		}
		local pickerWheelSheet = graphics.newImageSheet( "assets/orders/pickerWheelDate.png", options )

		local function createDatePickerWheel( initdate )
			local days = {}
			local years = {}
			for i = 1,31 do days[i] = i end
			for j = 1,10 do years[j] = 2013+j end
			local months = translations.Months[self.lang]
			for i=1, #years do
				if years[i] == initdate.year then initdate.year = i end
			end
			local columnData = {         -- all columns should add up to 280
				{
					align = "center",
					width = 280*0.125,
					startIndex = 1,
					labels = {""}
				},					
				{
					align = "center",
					width = 280*0.15,
					startIndex = initdate.day,
					labels = days
				},
				{
					align = "center",
					width = 280*0.4,
					startIndex = initdate.month,
					labels = months
				},			
				{
					align = "center",
					width = 280*0.2,
					startIndex = initdate.year,
					labels = years
				},
				{
					align = "center",
					width = 280*0.125,
					startIndex = 1,
					labels = {""}
				},					
			}
				
			local pickerWheel = widget.newPickerWheel {
				x = 0,
				y = 0,
				font = self.font_title, 
				fontSize = self.fsize_item,
				columns = columnData,
				columnColor = { 0, 0, 0, 0 },
					sheet = pickerWheelSheet,
					overlayFrame = 1,
					overlayFrameWidth = 320,
					overlayFrameHeight = 222,
					backgroundFrame = 1,
					backgroundFrameWidth = 320,
					backgroundFrameHeight = 222,
					separatorFrame = 3,
					separatorFrameWidth = 8,
					separatorFrameHeight = 222,
			}
			pickerWheel.group = display.newGroup( )
			pickerWheel.group:insert( pickerWheel )
			pickerWheel.group.anchorChildren = true
			pickerWheel.group.x = rdbox.bg.x
			pickerWheel.group.anchorY = 0
			pickerWheel.group.y = rdbox.name.y + rdbox.name.height + 15
			rdbox:insert( pickerWheel.group )
			pickerWheel.months = months

			return pickerWheel
		end
		rdbox.pickerwheel = createDatePickerWheel( self.ordview:getReservParams().date )

		rdbox.separ2 = display.newLine( rdbox, 
			rdbox.bg.x - rdbox.bg.width*0.5 + self.pad_regular, 
			rdbox.pickerwheel.group.y + rdbox.pickerwheel.group.height + 5,  
			rdbox.bg.x + rdbox.bg.width*0.5 - self.pad_regular, 
			rdbox.pickerwheel.group.y + rdbox.pickerwheel.group.height + 5
		)
		rdbox.separ2:setStrokeColor( 0,0,0, 0.1 )
		rdbox.separ2.strokeWidth = 1.5				

		rdbox.lbut = widget.newButton({
				label = "OK",
				labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
				fontSize = 13.5,
				font = self.font_bold,
				--properties for a rounded rectangle button...
				shape="roundedRect",
				width = (rdbox.bg.width - self.pad_regular*3) * 0.4,
				height = 37,
				cornerRadius = 5,
				fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
				strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
				strokeWidth = 2,
				onRelease = function( event )
					local values = rdbox.pickerwheel:getValues()
					local resdate = values[2].value
					local int, fract = math.modf(values[2].value/10)
					if int == 0 then resdate = "0"..values[2].value end
					local months = rdbox.pickerwheel.months
					for i=1, #months do
					if months[i] == values[3].value then
						values[3].value = i
						break
					end
				end
				int, fract = math.modf( values[3].value/10 )
				if int == 0 then resdate = resdate..".0"..values[3].value 
				else resdate = resdate.."."..values[3].value 	
				end
				resdate = resdate.."."..values[4].value
				panel.date:setText( resdate )
				panel.informer.text = string.match( panel.informer.text, "%s+(.+)" )
				panel.informer.text = resdate.." "..panel.informer.text
				self.ordview:setReservParams({ date = {
					values[4].value, values[3].value, values[2].value
				}})		  		
				rdbox:removeSelf()
				rdbox = nil
				end	    
		})
		rdbox.lbut.anchorX = 0
		rdbox.lbut.x = rdbox.bg.x - rdbox.bg.width*0.5 + self.pad_regular
		rdbox.lbut.anchorY = 1
		rdbox.lbut.y = rdbox.bg.y + rdbox.bg.height*0.5 - self.pad_regular
		rdbox:insert( rdbox.lbut )

		rdbox.rbut = widget.newButton({
				label = translations.OrderTxt_ReservDateToday[self.lang],
				labelColor = { default={ 1, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
				fontSize = 13.5,
				font = self.font_bold,
				--properties for a rounded rectangle button...
				shape="roundedRect",
				width = (rdbox.bg.width - self.pad_regular*3) * 0.6,
				height = 37,
				cornerRadius = 5,
				fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
				strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
				strokeWidth = 2,
				onRelease = function( event )
					local initdate = os.date("*t")
					rdbox.pickerwheel.group:removeSelf()
					rdbox.pickerwheel.group = nil
					rdbox.pickerwheel = createDatePickerWheel( initdate )
				end	    
		})
		rdbox.rbut.anchorX = 1
		rdbox.rbut.x = rdbox.bg.x + rdbox.bg.width*0.5 - self.pad_regular
		rdbox.rbut.anchorY = 1
		rdbox.rbut.y = rdbox.bg.y + rdbox.bg.height*0.5 - self.pad_regular
		rdbox:insert( rdbox.rbut )		
	end

	function new_ovg:createTimeBox( panel, title, w, h )
		local rtbox = self:createBox( w, 375, function() end )	

		local init_time = string.match( panel.informer.text, "%s+(.+)$" )
	
		rtbox.name = display.newText({
			parent = rtbox,
			text = title,
			width = rtbox.bg.width*0.8,
			font = self.font_bold, 
			fontSize = self.fsize_item,
			align = "center"
		})
		rtbox.name:setFillColor( 0,0,0, 1 )
		rtbox.name.x = rtbox.bg.x
		rtbox.name.anchorY = 0
		rtbox.name.y = rtbox.bg.y - rtbox.bg.height*0.5 + 10

		rtbox.separ1 = display.newLine( rtbox, 
			rtbox.bg.x - rtbox.bg.width*0.5, rtbox.name.y + rtbox.name.height + 10,  
			rtbox.bg.x + rtbox.bg.width*0.5, rtbox.name.y + rtbox.name.height + 10
		)
		rtbox.separ1:setStrokeColor( 0,0,0, 0.1 )
		rtbox.separ1.strokeWidth = 1.5		

		local options = {
				frames = 
				{
						{ x=0, y=0, width=320, height=222 },
						{ x=320, y=0, width=320, height=222 },
						{ x=640, y=0, width=8, height=222 }
				},
				sheetContentWidth = 648,
				sheetContentHeight = 222
		}
		local pickerWheelSheet = graphics.newImageSheet( "assets/orders/pickerWheelTime.png", options )

		local function createTimePickerWheel( inittime )
			local minutes = {}
			local hours = {}
			hours[1] = 0
			for i = 2,24 do hours[i] = i-1 end
			inittime.hour = inittime.hour % 24 + 1 
			minutes[1] = "00"
			for j = 2,6 do 	minutes[j] = (j-1)*10 end
			local int, fract = math.modf(inittime.min/10)
			inittime.min = int + 1

			local columnData = {           -- all columns should add up to 280
				{
					align = "center",
					width = 50,
					startIndex = 1,
					labels = {""}
				},				
				{
					align = "center",
					width = 90,
					startIndex = inittime.hour,
					labels = hours
				},
				{
					align = "center",
					width = 90,
					startIndex = inittime.min,
					labels = minutes
				},
				{
					align = "center",
					width = 50,
					startIndex = 1,
					labels = {""}
				}
			}
				
			local pickerWheel = widget.newPickerWheel {
				x = 0,
				y = 0,
				font = self.font_title, 
				fontSize = self.fsize_regular + 13,
				columns = columnData,
				columnColor = { 0, 0, 0, 0 },
					sheet = pickerWheelSheet,
					overlayFrame = 1,
					overlayFrameWidth = 320,
					overlayFrameHeight = 222,
					backgroundFrame = 1,
					backgroundFrameWidth = 320,
					backgroundFrameHeight = 222,
					separatorFrame = 3,
					separatorFrameWidth = 8,
					separatorFrameHeight = 222,
			}
			pickerWheel.group = display.newGroup( )
			pickerWheel.group:insert( pickerWheel )
			pickerWheel.group.anchorChildren = true
			pickerWheel.group.x = rtbox.bg.x
			pickerWheel.group.anchorY = 0
			pickerWheel.group.y = rtbox.name.y + rtbox.name.height + 15
			rtbox:insert( pickerWheel.group )

			local pwsensor = display.newRect( 
				pickerWheel.group, 0, 0, rtbox.width*0.4, pickerWheel.height 
			)
			pwsensor.isVisible = false
			pwsensor.isHitTestable = true
			pwsensor:addEventListener( "touch", function( event )
				if event.phase == "began" and not rtbox.addhours.moulage then
					rtbox.addhours:removeSelf()
					rtbox.createSegmentedControl()
				end
			end )

			return pickerWheel
		end
		rtbox.pickerwheel = createTimePickerWheel( self.ordview:getReservParams().time )

		local options = {
				frames = 
				{
						{ x=0, y=0, width=40, height=40 },
						{ x=40, y=0, width=40, height=40 },
						{ x=80, y=0, width=40, height=40 },
						{ x=120, y=0, width=40, height=40 },
						{ x=160, y=0, width=40, height=40 },
						{ x=200, y=0, width=40, height=40 },
						{ x=241, y=0, width=1, height=40 },
				},		    
				sheetContentWidth = 242,
				sheetContentHeight = 40
		}
		local segmentSheet = graphics.newImageSheet( "assets/orders/segmentHours.png", options )	
		
		local segm_text = translations.OrderTxt_ReservHours[self.lang]
		local segments = {}
		for i=1,5 do segments[i] = "+ "..i.."\n"..segm_text[i] end

		function rtbox.createSegmentedControl( label_color,  select_frames )
			label_color = label_color or { default = { 0 }, over = { 0 } }
			select_frames = select_frames or { 1, 2, 3 }	

			rtbox.addhours = widget.newSegmentedControl({
				x = 0,
				y = 0,
				segmentWidth = rtbox.bg.width/5,
				segments = segments,
				--defaultSegment = 1,
				labelFont = self.font_title,
				labelSize = self.fsize_regular-1,
				labelColor = label_color,
				sheet = segmentSheet,
				leftSegmentFrame = 1,
				middleSegmentFrame = 2,
				rightSegmentFrame = 3,
				leftSegmentSelectedFrame = select_frames[1],
				middleSegmentSelectedFrame = select_frames[2],
				rightSegmentSelectedFrame = select_frames[3],
				dividerFrame = 7,
				dividerFrameWidth = 1,
				dividerFrameHeight = 40,
				segmentFrameWidth = 40,
				segmentFrameHeight = 40,
				onPress = function( event )
					local target = event.target
					local num = target.segmentNumber
						
					local inittime = os.date("*t")
					inittime.hour = inittime.hour + num
					rtbox.pickerwheel.group:removeSelf()
					rtbox.pickerwheel.group = nil
					rtbox.pickerwheel = createTimePickerWheel( inittime )
					
					if select_frames[1] == 1 then
						rtbox.addhours:removeSelf()
						rtbox.createSegmentedControl( { default = { 0 }, over = { 1 } }, { 4, 5, 6 } )
						rtbox.addhours.moulage = false
						rtbox.addhours:setActiveSegment( num )
					end
				end
			})
			rtbox.addhours.anchorChildren = true
			rtbox.addhours.x = rtbox.bg.x
			rtbox.addhours.anchorY = 0
			rtbox.addhours.y = rtbox.pickerwheel.group.y + rtbox.pickerwheel.group.height + 10
			rtbox.addhours.moulage = true
			rtbox:insert( rtbox.addhours )		
		end

		rtbox.createSegmentedControl()
		
		local line1 = display.newRect( rtbox, 0, 0, rtbox.addhours.width, 1.3 )
		line1:setFillColor( 0,0,0, 0.1 )
		line1.x = rtbox.bg.x
		line1.y = rtbox.addhours.y
		--line1:toBack()

		local line2 = display.newRect( rtbox, 0, 0, rtbox.addhours.width, 1.3 )
		line2:setFillColor( 0,0,0, 0.1 )
		line2.x = rtbox.bg.x
		line2.y = rtbox.addhours.y + rtbox.addhours.height
		--line2:toBack()		

		rtbox.lbut = widget.newButton({
				label = "OK",
				labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
				fontSize = 13.5,
				font = self.font_bold,
				--properties for a rounded rectangle button...
				shape="roundedRect",
				width = (rtbox.bg.width - self.pad_regular*3) * 0.4,
				height = 37,
				cornerRadius = 5,
				fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
				strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
				strokeWidth = 2,
				onRelease = function( event )
					local values = rtbox.pickerwheel:getValues()
					local restime = values[2].value
					local int, fract = math.modf(values[2].value/10)
					if int == 0 then restime = "0"..values[2].value end
					if values[3].value == "00" then values[3].value = 0 end
					int, fract = math.modf( values[3].value/10 )
					if int == 0 then restime = restime..":0"..values[3].value 
					else restime = restime..":"..values[3].value 	
					end
					panel.time:setText( restime )
					panel.informer.text = string.match( panel.informer.text, "(.+)%s+" )
					panel.informer.text = panel.informer.text.." "..restime
					self.ordview:setReservParams({ time = {
						values[2].value, values[3].value
					}})		  		
					rtbox:removeSelf()
				rtbox = nil
				end	    
		})
		rtbox.lbut.anchorX = 0
		rtbox.lbut.x = rtbox.bg.x - rtbox.bg.width*0.5 + self.pad_regular
		rtbox.lbut.anchorY = 1
		rtbox.lbut.y = rtbox.bg.y + rtbox.bg.height*0.5 - self.pad_regular
		rtbox:insert( rtbox.lbut )

		rtbox.rbut = widget.newButton({
				label = translations.Cancel[self.lang],
				labelColor = { default={ 1, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
				fontSize = 13.5,
				font = self.font_bold,
				--properties for a rounded rectangle button...
				shape="roundedRect",
				width = (rtbox.bg.width - self.pad_regular*3) * 0.6,
				height = 37,
				cornerRadius = 5,
				fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
				strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
				strokeWidth = 2,
				onRelease = function( event )
					rtbox:removeSelf()
				end	    
		})
		rtbox.rbut.addon = 1
		rtbox.rbut.anchorX = 1
		rtbox.rbut.x = rtbox.bg.x + rtbox.bg.width*0.5 - self.pad_regular
		rtbox.rbut.anchorY = 1
		rtbox.rbut.y = rtbox.bg.y + rtbox.bg.height*0.5 - self.pad_regular
		rtbox:insert( rtbox.rbut )
	end

	function new_ovg:createInputBox( name, init_text, onComplete )
		local inbox = self:createBox( _Xa*0.8, _Ya*0.4, function()
			native.setKeyboardFocus( nil )
		end)
		inbox.bg.y = inbox.bg.y*0.6

		inbox.name = display.newText({
			parent = inbox,
			width = inbox.bg.width*0.8,
			text = name,
			font = self.font_bold, 
			fontSize = self.fsize_item,
			align = "center"
		})
		inbox.name:setFillColor( 0,0,0, 1 )
		inbox.name.x = inbox.bg.x
		inbox.name.anchorY = 0
		inbox.name.y = inbox.bg.y - inbox.bg.height*0.5 + 10

		inbox.separ1 = display.newLine( inbox, 
			inbox.bg.x - inbox.bg.width*0.5, inbox.name.y + inbox.name.height + 10,  
			inbox.bg.x + inbox.bg.width*0.5, inbox.name.y + inbox.name.height + 10
		)
		inbox.separ1:setStrokeColor( 0,0,0, 0.1 )
		inbox.separ1.strokeWidth = 1.5		

		local textBox = native.newTextBox( 
			inbox.bg.x, inbox.separ1.y + 10, 
			inbox.bg.width - self.pad_regular*2, inbox.bg.height*0.45
		)
		textBox.text = init_text or ""
		textBox.font = native.newFont( self.font_title, self.fsize_item ) 
		textBox.align = "left"
		textBox.isEditable = true
		textBox.hasBackground = false
		textBox.anchorY = 0
		inbox:insert( textBox )
		textBox:addEventListener( "userInput", function( event )
				if event.phase == "began" then
				native.setKeyboardFocus( event.target )
				elseif event.phase == "ended" then
					 native.setKeyboardFocus( nil )
				elseif event.phase == "editing" then
						-- print( event.newCharacters )
						-- print( event.oldText )
						-- print( event.startPosition )
						-- print( event.text )
				end
		end )
		native.setKeyboardFocus( textBox )

		inbox.lbut = widget.newButton({
				label = "OK",
				labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
				fontSize = 13.5,
				font = self.font_bold,
				--properties for a rounded rectangle button...
				shape="roundedRect",
				width = (inbox.bg.width - self.pad_regular*3) * 0.5,
				height = 37,
				cornerRadius = 5,
				fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
				strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
				strokeWidth = 2,
				onRelease = function( event )
					native.setKeyboardFocus( nil )
					onComplete( textBox.text )
					inbox:removeSelf()
				inbox = nil
				end	    
		})
		inbox.lbut.anchorX = 0
		inbox.lbut.x = inbox.bg.x - inbox.bg.width*0.5 + self.pad_regular
		inbox.lbut.anchorY = 1
		inbox.lbut.y = inbox.bg.y + inbox.bg.height*0.5 - self.pad_regular
		inbox:insert( inbox.lbut )

		inbox.rbut = widget.newButton({
			label = translations.OrderTxt_ReservCommentClean[self.lang],
			labelColor = { default={ 1, 0, 0 }, over={ 0, 0, 0, 0.5 } },
			fontSize = 13.5,
			font = self.font_bold,
			--properties for a rounded rectangle button...
			shape="roundedRect",
			width = (inbox.bg.width - self.pad_regular*3) * 0.5,
			height = 37,
			cornerRadius = 5,
			fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
			strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
			strokeWidth = 2,
			onRelease = function( event )
			textBox.text = ""
			end	    
		})
		inbox.rbut.anchorX = 1
		inbox.rbut.x = inbox.bg.x + inbox.bg.width*0.5 - self.pad_regular
		inbox.rbut.anchorY = 1
		inbox.rbut.y = inbox.lbut.y
		inbox:insert( inbox.rbut )

		inbox.separ2 = display.newLine( inbox, 
			inbox.bg.x - inbox.bg.width*0.5 + self.pad_regular, 
			inbox.rbut.y - inbox.rbut.height - 15,  
			inbox.bg.x + inbox.bg.width*0.5 - self.pad_regular, 
			inbox.rbut.y - inbox.rbut.height - 15
		)
		inbox.separ2:setStrokeColor( 0,0,0, 0.1 )
		inbox.separ2.strokeWidth = 1
	end

	function new_ovg:newRadioSwitcher( params )
		if not params.sheet then return end
		params.gap = params.gap or 0
		params.leftLabel = params.leftLabel or ""
		params.rightLabel = params.rightLabel or ""
		params.labelFont = params.labelFont or 0
		params.labelSize = params.labelSize or 0
		params.labelColor = params.labelColor or { 0,0,0 }
		params.labelPad = params.labelPad or 0
		params.onPress = params.onPress or function() end
		if params.leftIsOn == nil then params.leftIsOn = true
		else params.leftIsOn = params.leftIsOn
		end
		if params.rightIsOn == nil then params.rightIsOn = false
		else params.rightIsOn = params.rightIsOn
		end

		local switcher = display.newGroup()
		switcher.anchorChildren = true
		
		switcher.leftButton = display.newGroup()
		switcher.leftButton.anchorChildren = true
		switcher:insert( switcher.leftButton )
		switcher.leftButton.radio = display.newGroup()
		switcher.leftButton.radio.anchorChildren = true
		switcher.leftButton.radio.anchorX = 1
		switcher.leftButton:insert( switcher.leftButton.radio )

		switcher.rightButton = display.newGroup()
		switcher.rightButton.anchorChildren = true
		switcher:insert( switcher.rightButton )
		switcher.rightButton.radio = display.newGroup()
		switcher.rightButton.radio.anchorChildren = true
		switcher.rightButton.radio.anchorX = 1
		switcher.rightButton:insert( switcher.rightButton.radio )

		switcher.leftButton.anchorX = 1
		switcher.rightButton.anchorX = 0
		switcher.rightButton.x = params.gap

		switcher.leftButton.id = "left"
		switcher.rightButton.id = "right"

		switcher.leftButton.radio:insert( display.newImage( params.sheet, 2 ) )
		switcher.leftButton.radio:insert( display.newImage( params.sheet, 1 ) )
		switcher.rightButton.radio:insert( display.newImage( params.sheet, 2 ) )
		switcher.rightButton.radio:insert( display.newImage( params.sheet, 1 ) )

		switcher.leftButton.isOn = params.leftIsOn
		switcher.leftButton.radio[2].isVisible = params.leftIsOn
		switcher.rightButton.isOn = params.rightIsOn
		switcher.rightButton.radio[2].isVisible = params.rightIsOn

		switcher.leftLabel = display.newText( 
			switcher.leftButton,
			params.leftLabel,
			0, 0, 
			params.labelFont, params.labelSize
		)
		switcher.leftLabel:setFillColor( unpack( params.labelColor ) )
		switcher.leftLabel.anchorX = 0
		switcher.leftLabel.x = params.labelPad

		switcher.rightLabel = display.newText( 
			switcher.rightButton,
			params.rightLabel,
			0, 0, 
			params.labelFont, params.labelSize
		)
		switcher.rightLabel:setFillColor( unpack( params.labelColor ) )
		switcher.rightLabel.anchorX = 0
		switcher.rightLabel.x = params.labelPad

		function switcher:onPress(event)
			if event.target.isOn == false then
				params.onPress( switcher, event.target )
				event.target.radio[2].isVisible = true
				event.target.isOn = true
				if event.target.id == "left" then 
					self.rightButton.radio[2].isVisible = false
					self.rightButton.isOn = false
				else self.leftButton.radio[2].isVisible = false
					self.leftButton.isOn = false
				end
			end			
		end

		switcher.leftButton:addEventListener( "tap", function( event )
			switcher:onPress( event )
		end )
		switcher.rightButton:addEventListener( "tap", function( event )
			switcher:onPress( event )
		end )
	
		return switcher
	end

	function new_ovg:createReservPanel( ypos, height, header_height )
		if self.ordview:type() < 2 then return end

		local resparams = self.ordview:getReservParams( true )
		
		local resdate = resparams.date
		if not resdate then
			resdate = os.date( "*t" )
			self.ordview:setReservParams({ date = {
				resdate.year, resdate.month, resdate.day
			}})
			resdate = self.ordview:getReservParams( true ).date			
		end

		local restime = resparams.time
		if not restime then
			restime = os.date( "*t" )
			self.ordview:setReservParams({ time = {
				restime.hour, restime.min
			}})
			restime = self.ordview:getReservParams( true ).time			
		end
		
		local function listnOnStart( panel )
			if panel.completeState == "hidden" then
				panel.bplus.isVisible = false
				panel.bminus.isVisible = true
				self:panelsController( panel, "show" )
			elseif panel.completeState == "shown" then
				panel.bplus.isVisible = true
				panel.bminus.isVisible = false
			end
		end
		local function listnOnComplete( panel )
			if panel.completeState == "hidden" then
				self:panelsController( panel, "hide" )
			end
		end		
		
		local options = { 
				-- general
				id = "res",
				width = _Xa,
				height = height,
				speed = 250,
				inEasing = easing.outCubic,
				outEasing = easing.outCubic,
				isScrollable = true,
				topScrollPad = 5,
				bottomScrollPad = 5,		    
				onPress = function( event )
					self.panelInFocus = event.target
				end,
				onStart = listnOnStart,
				onComplete = listnOnComplete,
				--header
				headerHeight = header_height,
				headerColor = { 1,1,1 }
		}
		local slpanel = widget.newSlidingPanel( options )
		slpanel.anchorChildren = true
		slpanel.x = 0
		slpanel.anchorY = 0
		slpanel.y = ypos
		slpanel.up_pos = ypos
		slpanel.down_pos = slpanel.up_pos + slpanel.openHeight

		-- header ------------------------------------------------------------

		local options = {
				width = 30,
				height = 30,
				numFrames = 2,
				sheetContentWidth = 60,
				sheetContentHeight = 30
		}
		local pbsheet =graphics.newImageSheet( "assets/orders/panelButton.png", options )
		slpanel.bplus = display.newImageRect( pbsheet, 1, 22, 22 )
		slpanel.bplus.anchorX = 0
		slpanel.bplus.x = slpanel.header.left + self.pad_regular
		slpanel.bplus.y = 0
		slpanel.header:insert( slpanel.bplus )
		slpanel.bminus = display.newImageRect( pbsheet, 2, 22, 22 )
		slpanel.bminus.anchorX = 0
		slpanel.bminus.x = slpanel.header.left + self.pad_regular
		slpanel.bminus.y = 0
		slpanel.header:insert( slpanel.bminus )
		slpanel.bminus.isVisible = false
		
		local label =  display.newText( 
			translations.OrderTxt_Reserv[self.lang],
			0,0,
			self.font_title, self.fsize_title 
		)
		label:setFillColor( 0,0,0 )
		label.anchorX = 0
		label.x = slpanel.bplus.x + slpanel.bplus.width + 10
		label.y = 0
		slpanel.header:insert( label )

		local greserv = display.newGroup()
		greserv.anchorChildren = true
		greserv.anchorX = 0
		greserv.x = label.x + label.width + 10
		greserv.y = 0
		slpanel.header:insert( greserv )

		slpanel.informer = display.newText( 	
			resdate.."  "..restime,
			0, 0,
			self.font_title, self.fsize_regular-3
		)
		slpanel.informer:setFillColor( 1,1,1 )
		greserv:insert( slpanel.informer )

		local w = self:getTextWidth( slpanel.informer.text, self.font_title, self.fsize_regular-3 )
		local bg_reserv = display.newRoundedRect( greserv, 0, 0, w+5, slpanel.bplus.height*0.7, 3 )
		bg_reserv:setFillColor( 246/255, 133/255, 49/255 )
		bg_reserv:toBack()		

		-- body ---------------------------------------------------------------------------

		local side_pad = self.pad_regular+slpanel.bplus.width*0.5
		local top_pad = 5
		local next_row = 0
		local dist = 10
		
		local takeawaygroup = display.newGroup( )
		
		local takeawaylabel = display.newText( 
			takeawaygroup,
			translations.OrderTxt_ReservTakeaway[self.lang],
			0,0,
			self.font_title, self.fsize_item 
		)
		takeawaylabel:setFillColor( 0,0,0 )
		takeawaylabel.anchorX = 0
		
		local options = {
				width = 22,
				height = 22,
				numFrames = 2,
				sheetContentWidth = 44,
				sheetContentHeight = 22
		}
		local radioButtonSheet = graphics.newImageSheet( "assets/orders/smallRadio.png", options )

		local leftIsOn = false
		local rightIsOn = true
		if self.ordview:type() == 3 then
			leftIsOn = true
			rightIsOn = false
		end		

		local switcher = self:newRadioSwitcher({
			sheet = radioButtonSheet,
			gap = 10,
			leftLabel = translations.YesNo[self.lang][1],
			rightLabel = translations.YesNo[self.lang][2],
			labelFont = self.font_title,
			labelSize = self.fsize_item,
			labelColor = { 0,0,0, 0.8 },
			labelPad = 3,
			leftIsOn = leftIsOn,
			rightIsOn = rightIsOn,
			onPress = function( switcher, button )
				if button.id == "left" then self.ordview:changeOrderType( 3 ) 
				elseif button.id == "right" then self.ordview:changeOrderType( 2 )
				end 
			end
		})
		switcher.anchorX = 0
		switcher.x = takeawaylabel.x + takeawaylabel.width + 15
		takeawaygroup:insert( switcher )

		slpanel:place( takeawaygroup, { top = top_pad, left = side_pad } )
		next_row = top_pad + takeawaygroup.height + dist

		local dategroup = display.newGroup()
		
		local datelabel = display.newText( 
				translations.OrderTxt_ReservDate[self.lang][2]..":",
			0, 0,
			self.font_title, self.fsize_item 
		)
		datelabel:setFillColor( 0,0,0 )
		datelabel.anchorX = 0
		dategroup:insert( datelabel )
		
		slpanel.date = self:createPlate({
			text = resdate,
			font = self.font_title, 
			fsize = self.fsize_tap - 2,
			pad = 2,
			align = "right",
			color = { 0,0,0, 0.8 }
		})
		slpanel.date.anchorX = 0
		slpanel.date.x = -5
		slpanel.date.anchorY = 0
		slpanel.date.y = datelabel.y + datelabel.height*0.5 + 5
		
		dategroup:insert( slpanel.date )
		slpanel:place( dategroup, { top = next_row, left = side_pad-5 } )

		dategroup:addEventListener( "touch", function( event )
			if event.phase == "moved" then return false
			elseif event.phase == "ended" or event.phase == "cancelled" then
				self:createDateBox( 
					slpanel, translations.OrderTxt_ReservDate[self.lang][1], _Xa-60, _Ya*0.5 
				)
			end			
			return true
		end )

		local timegroup = display.newGroup()
		local timelabel = display.newText( 
			translations.OrderTxt_ReservTime[self.lang][2]..":",
			0,0,
			self.font_title, self.fsize_item
		)
		timelabel:setFillColor( 0,0,0 )
		timelabel.anchorX = 0
		timelabel.x = _mX
		timegroup:insert( timelabel )

		slpanel.time = self:createPlate({
			text = restime,
			font = self.font_title, 
			fsize = self.fsize_tap - 2,
			pad = 2,
			align = "right",
			color = { 0,0,0, 0.8 }
		})
		slpanel.time.anchorX = 0
		slpanel.time.x = _mX - 5
		slpanel.time.anchorY = 0
		slpanel.time.y = timelabel.y + timelabel.height*0.5 + 5

		timegroup:insert( slpanel.time )
		slpanel:place( timegroup, { top = next_row, left = side_pad-5 + dategroup.width + 10 } )

		timegroup:addEventListener( "touch", function( event )
			if event.phase == "moved" then return false
			elseif event.phase == "ended" or event.phase == "cancelled" then
				self:createTimeBox( 
					slpanel, translations.OrderTxt_ReservTime[self.lang][1], _Xa-60, _Ya*0.5
				)
			end			
			return true
		end )

		next_row = next_row + timegroup.height + dist

		local steppergroup = display.newGroup()
		local stepperlabel = display.newText( 
			translations.OrderTxt_ReservPersons[self.lang]..":",
			0,0,
			self.font_title, self.fsize_item
		)
		stepperlabel:setFillColor( 0,0,0 )
		stepperlabel.anchorX = 0
		steppergroup:insert( stepperlabel )

		slpanel.persons = self:createPlate({
			text = resparams.persons or 1,
			font = self.font_title, 
			fsize = self.fsize_tap - 2,
			pad = 2,
			align = "right",
			color = { 0,0,0, 0.8 }
		})
		slpanel.persons.anchorX = 0
		slpanel.persons.x = stepperlabel.x + stepperlabel.width + 8
		steppergroup:insert( slpanel.persons )
		
		local options = {
				width = 94,
				height = 22,
				numFrames = 3,
				sheetContentWidth = 282,
				sheetContentHeight = 22
		}
		local stepperSheet = graphics.newImageSheet( "assets/orders/widthStepper.png", options )

		slpanel.stepper = display.newGroup()
		local stepper = widget.newStepper
		{
				id = "persons_stepper",
				initialValue = tonumber( resparams.persons or 1 ),
				minimumValue = 1,
				sheet = stepperSheet,
				defaultFrame = 1,
				noMinusFrame = 2,
				noPlusFrame = 3,
				minusActiveFrame = 2,
				plusActiveFrame = 3,		    
				onPress = function( event )
					if event.phase == "increment" then
							slpanel.persons:setText( event.value )
							self.ordview:setReservParams( { persons = event.value })
					elseif event.phase == "decrement" then
							slpanel.persons:setText( event.value )
							self.ordview:setReservParams( { persons = event.value })
					end
			end
		}
		stepper.slpanel = slpanel
		slpanel.stepper:insert( stepper )
		--slpanel.stepper.width = 85
		--slpanel.stepper.height = 22 
		slpanel.stepper.anchorChildren = true
		slpanel.stepper.anchorX = 0
		slpanel.stepper.x = slpanel.persons.x + slpanel.persons.width + 20
		steppergroup:insert( slpanel.stepper )

		slpanel:place( steppergroup, { top = next_row+5, left = side_pad } )
		next_row = next_row + steppergroup.height + dist + 5		

		local comboxgroup = display.newGroup()
		local comboxlabel = display.newText( 
			translations.OrderTxt_ReservComment[self.lang][2]..":",
			0, 0,
			self.font_title, self.fsize_item
		)
		comboxlabel:setFillColor( 0,0,0 )
		comboxlabel.anchorX = 0
		comboxgroup:insert( comboxlabel )

		local comment = resparams.comment or ""
		if comment == "" 
		and ( self.ordview:getOrderState() == 0 or self.ordview:getOrderState() > 3 ) then
			--comment = "user_name="..ldb:getSettValue( "guest_fullname" ).."\nuser_phone="..ldb:getSettValue( "guest_phone" ).."\nuser_email="..ldb:getSettValue( "guest_email" )--translations.OrderTxt_CallMe[self.lang][1]..ldb:getSettValue( "guest_phone" )
			--self.ordview:setReservParams( { comment = comment } )  --Jen
		end
		slpanel.comment = display.newText({ 	
			text = comment,
			x = 0, y = 0,
			width = _Xa - side_pad*2 - 15,
			font = self.font_regular, fontSize = self.fsize_regular-1, 
			align = "left"
		})
		slpanel.comment:setFillColor( 0,0,0 )

		local freespace = slpanel.openHeight - next_row - comboxlabel.height - dist
		local boxheight = slpanel.comment.height + 10
		boxheight = boxheight < freespace and freespace or boxheight
		boxheight = boxheight < 70 and 70 or boxheight
		local box_comment = display.newRoundedRect( 0, 0, 
			_Xa - side_pad*2, boxheight, 10
		)
		box_comment:setFillColor( 1,1,1 )
		box_comment:setStrokeColor( 0,0,0, 0.1 )
		box_comment.strokeWidth = 1	

		comboxgroup:insert( box_comment )
		box_comment.anchorX = 0
		box_comment.anchorY = 0
		box_comment.y = comboxlabel.height
		
		comboxgroup:insert( slpanel.comment )
		slpanel.comment.anchorX = 0
		slpanel.comment.x = 13
		slpanel.comment.anchorY = 0
		slpanel.comment.y = box_comment.y + 10
		
		slpanel:place( comboxgroup, { top = next_row, left = side_pad } )

		comboxgroup:addEventListener( "touch", function( event )
			if event.phase == "ended" or event.phase == "cancelled" then
				self:createInputBox( 
					translations.OrderTxt_ReservComment[self.lang][1], 
					slpanel.comment.text,
					function( text )
						slpanel.comment.text = text
						self.ordview:setReservParams( { comment = text } )
					end  
				)
			end
			return true
		end )

		function slpanel:setState( ordstate )
			-- print("slpanel:setState ",ordstate)
			if ordstate == 0 then
				if self.overlay then
					self.overlay:removeSelf()
					self.overlay = nil
					self.stepper.isVisible = true
				end
			else
				self.stepper.isVisible = false
				if self.overlay then return end
				self.overlay = display.newRect( 0, 0, self.width, self.content.height )
				self.overlay:setFillColor( 1,1,1, 0.4 )
				self:place( self.overlay )
				self.overlay:addEventListener( "touch", function( event )
					return true
				end )
			end
		end
		slpanel:setState( self.ordview:getOrderState() )

		slpanel:hide( 0 )
		self:insert( slpanel )
		return slpanel
	end

	function new_ovg:createUserAddressList( data, onSelect )
		local listbox = nil
		local hview = display.newGroup()
		hview.anchorChildren = true
			
		local width = _Xa*0.85
		local min_height = 120
		local max_height = _Ya*0.9 - min_height
		local sum_points = 0

		local function row( text )
			local row_width = width - 20
			local row_height = 33
			local row = display.newGroup()
			row.anchorChildren = true

			local bg = display.newRect( row, 0, 0, row_width, row_height )
			bg:setFillColor( 0, 0 )
			local left = -bg.width*0.5
			local right = bg.width*0.5
			local top = -bg.height*0.5
			local bottom = bg.height*0.5

			local addr = display.newText({
				text = text,
				width = row_width,     
				x = 0,
				y = 0,
				font = "HelveticaNeueCyr-Light",   
				fontSize = 14
			})
			addr:setFillColor( 0 )
			addr.anchorX = 0
			addr.x = left
			row:insert( addr )

			row:addEventListener( "touch", function( event )
				if event.phase == "ended" then
					onSelect( text )
					display.remove( listbox ); listbox = nil
				end
			end )

			return row
		end

		local function list()
			local pad = 13
			local ypos = 0
			for i=1, #data do
				hview:insert( row( data[i] ) )
				hview[i].anchorY = 0
				hview[i].y = ypos
				ypos = ypos + hview[i].height + pad   
			end

			if hview.height > max_height then
				local content = hview
				hview = widget.newScrollView
				{
					width = width,
					height = max_height,
					bottomPadding = 0,
					horizontalScrollDisabled = true,
					hideBackground = true,
					listener = function( event )
						display.getCurrentStage():setFocus( nil )
					end
				}   
				content.x = hview.width*0.5
				content.anchorY = 0
				content.y = 0
				hview:insert( content )
			end
		end

		local function box( name, width, height, onClose )
			local hbox = uilib.newBox( width, height, onClose )
			
			local title = display.newText({
				parent = hbox,
				text = name,
				width = hbox.bg.width*0.8,
				font = "HelveticaNeueCyr-Medium", 
				fontSize = 15,
				align = "center"
			})
			title:setFillColor( 0.1 )
			title.x = hbox.bg.x
			title.anchorY = 0
			title.y = hbox.bg.y - hbox.bg.height*0.5 + 10
					
			local sepr1 = display.newLine( hbox, 
				hbox.bg.x - hbox.bg.width*0.5, title.y + title.height + 10,  
				hbox.bg.x + hbox.bg.width*0.5, title.y + title.height + 10
			)
			sepr1:setStrokeColor( 0,0,0, 0.1 )
			sepr1.strokeWidth = 1.5

			local btn_close = widget.newButton({
				label = translations.CloseBtn[userProfile.lang],
				labelColor = { default={ 1, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
				fontSize = 14,
				font = "HelveticaNeueCyr-Medium",
				--properties for a rounded rectangle button...
				shape="roundedRect",
				width = hbox.bg.width * 0.6,
				height = 37,
				cornerRadius = 5,
				fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
				strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
				strokeWidth = 2,
				onRelease = function( event )
					--onClose()
					hbox:removeSelf()
					hbox = nil
				end     
			})
			hbox:insert( btn_close, true )
			btn_close.x = hbox.bg.x
			btn_close.anchorY = 1
			btn_close.y = hbox.bg.y + hbox.bg.height*0.5 - 15
			hbox:insert( btn_close )

			local sepr2 = display.newLine( hbox, 
				hbox.bg.x - hbox.bg.width*0.5 + 10, 
				btn_close.y - btn_close.height - 15,  
				hbox.bg.x + hbox.bg.width*0.5 - 10, 
				btn_close.y - btn_close.height - 15
			)
			sepr2:setStrokeColor( 0,0,0, 0.1 )
			sepr2.strokeWidth = 1.5  

			return hbox
		end		

		if data then 
			list()
			listbox = box( 
				translations.select_address[self.lang], 
				width, hview.height + 120,
				onClose 
			)
			listbox:insert( hview )
			hview.x = listbox.bg.x
			hview.anchorY = 0
			hview.y = listbox.bg.y - listbox.bg.height*0.5 + 45
		end
	end

	function new_ovg:createDeliveryPanel( ypos, height, header_height )
		-- print("new_ovg:createDeliveryPanel self.ordview:type()",self.ordview:type())
		if self.ordview:type() ~= 1 then return end

	 	--     rdb_common:getUserAddress({long=44.4647452,lat=7.3553838,listener=getCoordListener})
		local resparams = self.ordview:getReservParams( true )
		
		local resdate = resparams.date
		if not resdate then
			resdate = os.date( "*t" )
			self.ordview:setReservParams({ date = {
				resdate.year, resdate.month, resdate.day
			}})
			resdate = self.ordview:getReservParams( true ).date			
		end

		local restime = resparams.time
		if not restime then
			restime = os.date( "*t" )
			self.ordview:setReservParams({ time = {
				restime.hour, restime.min
			}})
			restime = self.ordview:getReservParams( true ).time			
		end
		
		local function listnOnStart( panel )
			if panel.completeState == "hidden" then
				panel.bplus.isVisible = false
				panel.bminus.isVisible = true
				self:panelsController( panel, "show" )
			elseif panel.completeState == "shown" then
				panel.bplus.isVisible = true
				panel.bminus.isVisible = false
			end
		end
		local function listnOnComplete( panel )
			if panel.completeState == "hidden" then
				self:panelsController( panel, "hide" )
			end
		end		
		
		local options = { 
				-- general
				id = "deliv",
				width = _Xa,
				height = height,
				speed = 250,
				inEasing = easing.outCubic,
				outEasing = easing.outCubic,
				isScrollable = true,
				topScrollPad = 5,
				bottomScrollPad = 5,		    
				onPress = function( event )
					self.panelInFocus = event.target
				end,
				onStart = listnOnStart,
				onComplete = listnOnComplete,
				--header
				headerHeight = header_height,
				headerColor = { 1,1,1 }
		}
		local slpanel = widget.newSlidingPanel( options )
		slpanel.anchorChildren = true
		slpanel.x = 0
		slpanel.anchorY = 0
		slpanel.y = ypos
		slpanel.up_pos = ypos
		slpanel.down_pos = slpanel.up_pos + slpanel.openHeight

		-- header ------------------------------------------------------------

		local options = {
			width = 30,
			height = 30,
			numFrames = 2,
			sheetContentWidth = 60,
			sheetContentHeight = 30
		}
		local pbsheet =graphics.newImageSheet( "assets/orders/panelButton.png", options )
		slpanel.bplus = display.newImageRect( pbsheet, 1, 22, 22 )
		slpanel.bplus.anchorX = 0
		slpanel.bplus.x = slpanel.header.left + self.pad_regular
		slpanel.bplus.y = 0
		slpanel.header:insert( slpanel.bplus )
		slpanel.bminus = display.newImageRect( pbsheet, 2, 22, 22 )
		slpanel.bminus.anchorX = 0
		slpanel.bminus.x = slpanel.header.left + self.pad_regular
		slpanel.bminus.y = 0
		slpanel.header:insert( slpanel.bminus )
		slpanel.bminus.isVisible = false
		
		local label =  display.newText( 
			translations.OrderTxt_Delivery[self.lang],
			0,0,
			self.font_title, self.fsize_title 
		)
		label:setFillColor( 0,0,0 )
		label.anchorX = 0
		label.x = slpanel.bplus.x + slpanel.bplus.width + 10
		label.y = 0
		slpanel.header:insert( label )

		local greserv = display.newGroup()
		greserv.anchorChildren = true
		greserv.anchorX = 0
		greserv.x = label.x + label.width + 10
		greserv.y = 0
		slpanel.header:insert( greserv )
		if resdate==nil then
			resdate = os.date( "*t" )
			self.ordview:setReservParams({ date = {
				resdate.year, resdate.month, resdate.day
			}})
			resdate = self.ordview:getReservParams( true ).date			
		end

		if restime==nil then
			restime = os.date( "*t" )
			self.ordview:setReservParams({ time = {
				restime.hour, restime.min
			}})
			restime = self.ordview:getReservParams( true ).time			
		end

		slpanel.informer = display.newText( 	
			resdate.."  "..restime,
			0, 0,
			self.font_title, self.fsize_regular-3
		)
		slpanel.informer:setFillColor( 1,1,1 )
		greserv:insert( slpanel.informer )

		local w = self:getTextWidth( slpanel.informer.text, self.font_title, self.fsize_regular-3 )
		local bg_reserv = display.newRoundedRect( greserv, 0, 0, w+5, slpanel.bplus.height*0.7, 3 )
		bg_reserv:setFillColor( 246/255, 133/255, 49/255 )
		bg_reserv:toBack()		

		-- body ---------------------------------------------------------------------------

		local side_pad = self.pad_regular+slpanel.bplus.width*0.5
		local top_pad = 10
		local next_row = top_pad
		local dist = 10
		
		local dategroup = display.newGroup()
		
		local datelabel = display.newText( 
				translations.OrderTxt_ReservDate[self.lang][2]..":",
			0, 0,
			self.font_title, self.fsize_item 
		)
		datelabel:setFillColor( 0,0,0 )
		datelabel.anchorX = 0
		dategroup:insert( datelabel )
		
		slpanel.date = self:createPlate({
			text = resdate,
			font = self.font_title, 
			fsize = self.fsize_tap - 2,
			pad = 2,
			align = "right",
			color = { 0,0,0, 0.8 }
		})
		slpanel.date.anchorX = 0
		slpanel.date.x = -5
		slpanel.date.anchorY = 0
		slpanel.date.y = datelabel.y + datelabel.height*0.5 + 5
		
		dategroup:insert( slpanel.date )
		slpanel:place( dategroup, { top = next_row, left = side_pad-5 } )

		dategroup:addEventListener( "touch", function( event )
			if event.phase == "moved" then return false
			elseif event.phase == "ended" or event.phase == "cancelled" then
				self:createDateBox( 
					slpanel, translations.OrderTxt_DeliveryDate[self.lang][1], _Xa-60, _Ya*0.5
				)
			end			
			return true
		end )

		local timegroup = display.newGroup()
		local timelabel = display.newText( 
			translations.OrderTxt_ReservTime[self.lang][2]..":",
			0,0,
			self.font_title, self.fsize_item
		)
		timelabel:setFillColor( 0,0,0 )
		timelabel.anchorX = 0
		timelabel.x = _mX
		timegroup:insert( timelabel )

		slpanel.time = self:createPlate({
			text = restime,
			font = self.font_title, 
			fsize = self.fsize_tap - 2,
			pad = 2,
			align = "right",
			color = { 0,0,0, 0.8 }
		})
		slpanel.time.anchorX = 0
		slpanel.time.x = _mX - 5
		slpanel.time.anchorY = 0
		slpanel.time.y = timelabel.y + timelabel.height*0.5 + 5

		timegroup:insert( slpanel.time )
		slpanel:place( timegroup, { top = next_row, left = side_pad-5 + dategroup.width + 15 } )

		timegroup:addEventListener( "touch", function( event )
			if event.phase == "moved" then return false
			elseif event.phase == "ended" or event.phase == "cancelled" then
				self:createTimeBox( 
					slpanel, translations.OrderTxt_DeliveryTime[self.lang][1], _Xa-60, _Ya*0.5
				)
			end			
			return true
		end )

		next_row = next_row + timegroup.height + dist + 3

		---

		local addressgroup = display.newGroup()
		local addresslabel = display.newText( 
			translations.OrderTxt_DeliveryAdress[self.lang][2]..":",
			0, 0,
			self.font_title, self.fsize_item
		)
		addresslabel:setFillColor( 0,0,0 )
		addresslabel.anchorX = 0
		addressgroup:insert( addresslabel )

		local addrdata = ldb:getAllAddresses()
		local addr_addbutton = display.newImageRect( 
			addressgroup, "assets/orders/addressAdd.png", 44, 22 
		)
		addr_addbutton.anchorX = 1
		addr_addbutton.x = addresslabel.x + _Xa - side_pad*2
		addr_addbutton.y = addresslabel.y
		addr_addbutton:addEventListener( "touch", function( event )
			if event.phase == "ended" or event.phase == "cancelled" then
				slpanel.box_address.fill = { 1 }
				slpanel.address.fill = { 0 }
				self:createUserAddressList( addrdata, function( address )
					slpanel.address.text = address
					self.ordview:setReservParams( { address = address } )
				end )
			end
			return true
		end )
		if #addrdata == 0 then addr_addbutton.isVisible = false end

		slpanel.address = display.newText({ 	
			text = resparams.address or "",
			x = 0, y = 0,
			width = _Xa - side_pad*2 - 15,
			font = self.font_regular, fontSize = self.fsize_regular-1, 
			align = "left"
		})
		slpanel.address:setFillColor( 0,0,0 )

		local boxheight = 60
		local box_address = display.newRoundedRect( 0, 0, 
			_Xa - side_pad*2, boxheight, 10
		)
		box_address:setFillColor( 1,1,1 )
		box_address:setStrokeColor( 0,0,0, 0.1 )
		box_address.strokeWidth = 1
		slpanel.box_address = box_address	
		
		addressgroup:insert( box_address )
		box_address.anchorX = 0
		box_address.anchorY = 0
		box_address.y = addresslabel.height
		
		addressgroup:insert( slpanel.address )
		slpanel.address.anchorX = 0
		slpanel.address.x = 13
		slpanel.address.anchorY = 0
		slpanel.address.y = box_address.y + 10
		
		slpanel:place( addressgroup, { top = next_row, left = side_pad } )

		addressgroup:addEventListener( "touch", function( event )
			if event.phase == "ended" or event.phase == "cancelled" then
				box_address.fill = { 1 }
				slpanel.address.fill = { 0 }
				self:createInputBox(
					translations.OrderTxt_DeliveryAdress[self.lang][1], slpanel.address.text,
					function( text )
						slpanel.address.text = text
						self.ordview:setReservParams( { address = text } )
					end
				)
			end
			return true
		end )

		next_row = next_row + addressgroup.height + dist + 2

		---

		local comboxgroup = display.newGroup()
		local comboxlabel = display.newText( 
			translations.OrderTxt_ReservComment[self.lang][2]..":",
			0, 0,
			self.font_title, self.fsize_item
		)
		comboxlabel:setFillColor( 0,0,0 )
		comboxlabel.anchorX = 0
		comboxgroup:insert( comboxlabel )

		local comment = resparams.comment or ""
		if comment == "" 
		and ( self.ordview:getOrderState() == 0 or self.ordview:getOrderState() > 3 ) then
			--comment = "user_name="..ldb:getSettValue( "guest_fullname" ).."\nuser_phone="..ldb:getSettValue( "guest_phone" ).."\nuser_email="..ldb:getSettValue( "guest_email" )
			--self.ordview:setReservParams( { comment = comment } )  --Jen
		end
		slpanel.comment = display.newText({ 	
			text = comment,
			x = 0, y = 0,
			width = _Xa - side_pad*2 - 15,
			font = self.font_regular, fontSize = self.fsize_regular-1, 
			align = "left"
		})
		slpanel.comment:setFillColor( 0,0,0 )

		local freespace = slpanel.openHeight - next_row - comboxlabel.height - dist
		local boxheight = slpanel.comment.height + 10
		boxheight = boxheight < freespace and freespace or boxheight
		boxheight = boxheight < 70 and 70 or boxheight
		local box_comment = display.newRoundedRect( 0, 0, 
			_Xa - side_pad*2, boxheight, 10
		)
		box_comment:setFillColor( 1,1,1 )
		box_comment:setStrokeColor( 0,0,0, 0.1 )
		box_comment.strokeWidth = 1	

		comboxgroup:insert( box_comment )
		box_comment.anchorX = 0
		box_comment.anchorY = 0
		box_comment.y = comboxlabel.height
		
		comboxgroup:insert( slpanel.comment )
		slpanel.comment.anchorX = 0
		slpanel.comment.x = 13
		slpanel.comment.anchorY = 0
		slpanel.comment.y = box_comment.y + 10
		
		slpanel:place( comboxgroup, { top = next_row, left = side_pad } )

		comboxgroup:addEventListener( "touch", function( event )
			if event.phase == "ended" or event.phase == "cancelled" then
				self:createInputBox( 
					translations.OrderTxt_ReservComment[self.lang][2], slpanel.comment.text,
					function( text )
						slpanel.comment.text = text
						self.ordview:setReservParams( { comment = text } )
					end
				)
			end
			return true
		end )

		function slpanel:setState( ordstate )
			print("slpanel:setState ",ordstate)
			if ordstate == 0 then
				if self.overlay then
					self.overlay:removeSelf()
					self.overlay = nil
				end
			else
				if self.overlay then return end
				self.overlay = display.newRect( 0, 0, self.width, self.content.height )
				self.overlay:setFillColor( 1,1,1, 0.4 )
				self:place( self.overlay )
				self.overlay:addEventListener( "touch", function( event )
					return true
				end )
				addr_addbutton.isVisible = false
			end
		end
		slpanel:setState( self.ordview:getOrderState() )

	
		function slpanel:highlightAddress()
			box_address.fill = { 244/255, 64/255, 66/255, 0.4 }
			self.address.fill = { 1, 0, 0 }
		end

		slpanel:hide( 0 )
		self:insert( slpanel )
		return slpanel
	end

	function new_ovg:createOrderTypePanel( panel_ypos, names_and_states, onRelease )
		local types = {
			[0] = translations["order_in_the_rest"][self.lang],
			[1] = translations["order_with_delivery"][self.lang], 
			[2] = translations["order_with_reservation"][self.lang],
		}
		local types_combos = {
			[0] = { 0, 2 },
			[1] = { 0, 1, 2 },
			[2] = { 0 },
			[3] = { 1 },
			[4] = { 0, 1 },
			[6] = { 2 },
			[7] = { 1,2 }				
		}
		local rest_otypes_id = userProfile.SelectedRestaurantData.menu_type or 1
		local available_types = types_combos[rest_otypes_id]

		local slpanel = {}

		-- header ---------------------------------------------
			
		local header = display.newGroup()

		local sheet_options = {
				width = 292,
				height = 39,
				numFrames = 2,
				sheetContentWidth = 292,
				sheetContentHeight = 78
		}
		local sheet_file = "assets/filterCuisinesButton.png"

		local header_sheet = graphics.newImageSheet( sheet_file, sheet_options )
		local headerimg1 = display.newImage( header, header_sheet, 1 )
		local headerimg2 = display.newImage( header, header_sheet, 2 )

		local title = display.newText({
				parent = header,
				text = "",
				font = "HelveticaNeueCyr-Light", 
				fontSize = 14,
				align = "center"
		})
		title:setFillColor( 0, 0.4 )
		title.def_text = translations.OrderTxt_Type[self.lang][1][1]
		title.text = title.def_text

		-- body -----------------------------------------------

		local body = display.newGroup()
		local body_width = header.width-40

		local function createOTLine( id, name, state, width, height, onRelease )
			local otline = display.newGroup()
			otline.anchorChildren = true
			otline.anchorY = 0
			otline.state = state and "on" or "off"
			otline.name = name
			otline.id = id

			local bg = display.newRect( otline, 0, 0, width, height )
			bg:setFillColor( 0.95, 0 )
			bg.isHitTestable = true

			local right = -bg.width*0.5

			local options = {
					width = 25,
					height = 25,
					numFrames = 2,
					sheetContentWidth = 50,
					sheetContentHeight = 25
			}
			local rbsheet = graphics.newImageSheet( "assets/orders/smallRadio.png", options )
			local radio_on = display.newImageRect( otline, rbsheet, 1, 20, 20 )
			local radio_off = display.newImageRect( otline, rbsheet, 2, 20, 20 )
			radio_on.anchorX = 0
			radio_off.anchorX = 0
			radio_on.x = right + 15
			radio_off.x = right + 15
			if otline.state == "off" then radio_on.isVisible = false end

			local label = display.newText({
				parent = otline, 
				text = name, 
				font = "HelveticaNeueCyr-Light", 
				fontSize = 14
			})
			label:setFillColor( 0 )
			label.anchorX = 0
			label.x = radio_on.x + radio_on.width + 10

			function otline:turnOn()
				radio_on.isVisible = true
				self.state = "on"                
			end
			function otline:turnOff()
				radio_on.isVisible = false
				self.state = "off"                
			end

			otline:addEventListener( "touch", function( event )
				if event.phase == "began" then
					event.target.isFocus = true
					display.getCurrentStage():setFocus( event.target )
				elseif event.phase == "moved" then
					if math.abs( event.y - event.yStart ) >= 10 then
						event.target.isFocus = false
						display.getCurrentStage():setFocus( nil )
						return false
					end
				elseif event.phase == "ended" or event.phase == "cancelled" then
					if event.target.state == "off" then
						event.target:turnOn()
					elseif event.target.state == "on" then
						event.target:turnOff()
					end
					local pEvent = { 
						target = event.target,
						state = event.target.state,
						newName = event.target.name,
						id = event.target.id
					}
					print("onRelease( pEvent )")
					onRelease( pEvent )
					display.getCurrentStage():setFocus( nil )
				end
				return true
			end )

			return otline
		end

		local lines = {}
		local newl, sepr
		local ypos = 10
		local otype_id = 0
		local otype = ""
		local is_active = false
		-- print("available_types ",#available_types,otype_id,self.ordview:type())
		for i=1, #available_types do
			otype_id = available_types[i]
			otype = types[otype_id]
			--jen !!!
			--if  #available_types == 1 and self.ordview:type()==1 then
			--	print(self.ordview:type())
			--	self.ordview:changeOrderType( 2 )
			
			--else

			if #available_types == 1 and otype_id>=1 then
				self.ordview:changeOrderType( otype_id )
			end
			is_active = false
		
			if otype_id == self.ordview:type() or #available_types == 1 
			or ( otype_id == 2 and self.ordview:type() == 3 ) then
				headerimg2.isVisible = false
				title.text = otype
				title:setFillColor( 1,0,0 )
				is_active = true
			end          

			newl = createOTLine( 
				otype_id, otype, is_active, 
				body_width, 45, 
				function( event )
					for i=1, #lines do
						if lines[i] ~= event.target then lines[i]:turnOff() end
					end
					if event.state == "on" then 
						slpanel:setIsActivate( true, event.newName )

						self.ordview:changeOrderType( event.id )
						self:createPanels( self.header_height )
						self.mainbtns:setState( self.ordview:getOrderState() )
						
					elseif event.state == "off" then slpanel:setIsActivate( false )
						self.mainbtns:setState()
					end
					-- print("self.ordview:type()",self.ordview:type())
					if self.ordview:type()==1 then
						print("starting map coord")				
						--Jen try to get current customer position lat long. We save it and will try to ger address by it
						local function getCoordListener(event_lic)

						  local data = event_lic.data
						  if event_lic.result ~= "completed" 
						  or not data or not data.results or not data.results[1] then
					            print("ERROR LOADING address!!!")
					            return
					        end
					   --     printResponse(data)
					        print(self.ordview.odata.ord_comment)
					        if self.ordview.odata.ord_comment=="" or self.ordview.odata.ord_comment==nil then
							native.showAlert( 
								_G.appTitle, 
								translations.you_want_delivery_to[userProfile.lang]..data.results[1].formatted_address.."?",
								{ translations.YesNo[_G.Lang][1], translations.Modify[_G.Lang], translations.YesNo[_G.Lang][2] }, 
								function( event )
									if "clicked" == event.action then
										if event.index == 1 then 
											self.ordview.odata.ord_comment = data.results[1].formatted_address 
											-- self.onChangeOrderState( self )
											print(event_lic.params.long,event_lic.params.lat)
											self.ordview:setReservParams( {address = self.ordview.odata.ord_comment,long = event_lic.params.long,lat = event_lic.params.lat } )
											print("ee")
											new_ovg:createPanels( new_ovg.header_height )
										elseif event.index ==2 then
											self:createInputBox(
												translations.OrderTxt_DeliveryAdress[self.lang][1], data.results[1].formatted_address ,
												function( text )
													self.ordview.odata.ord_comment  = text
													self.ordview:setReservParams( { address = text } )
													new_ovg:createPanels( new_ovg.header_height )
												end
											)													
										end
									end
								end 
							)	        	
					        end
						end

							if not ov.map then
							    ov.map = native.newMapView( 
							        -_mX, -_mY,
							        1, 1
							    )
							end  
							if ov.map then  
								local attempts=0
								local function locationHandler( event )	
								    local currentLocation = ov.map:getUserLocation()
								    if ( currentLocation.errorCode or ( currentLocation.latitude == 0 and currentLocation.longitude == 0 ) ) then 		
								        attempts = attempts + 1
								        if ( attempts > 10 ) then
								            print( "No GPS Signal Can't sync with GPS.",currentLocation.errorCode )
								        else
								            timer.performWithDelay( 1000, locationHandler )
								        end
								    else			
								        rdb_common:getUserAddress({long=currentLocation.longitude,lat=currentLocation.latitude,listener=getCoordListener})
								        print( currentLocation.latitude, currentLocation.longitude )
								    end
								end
								locationHandler() 
						    end

					    ------------------------------------------------------------------Jen


					end
					slpanel:hide(0)
				end 
			)
			body:insert( newl )
			newl.y = ypos
			ypos = ypos + newl.height

			sepr = display.newRect( body, 0, 0, body_width, 1 )
			sepr:setFillColor( 0.95 )
			sepr.y = newl.y + newl.height

			lines[#lines+1] = newl        
		end

		-- slpanel --------------------------------------------
			
		local sp_height = body.height+header.height
		sp_height = sp_height <= 250 and sp_height or 250

		local slpanel_bg = display.newRoundedRect( 0, 0, body_width, sp_height, 5 )
		slpanel_bg:setFillColor( 1 )
		slpanel_bg:setStrokeColor( 0.9 )
		slpanel_bg.strokeWidth = 1    
			 
		local options = { 
			-- general
			id = "order_types",
			width = header.width,
			height = sp_height+20,
			bg = slpanel_bg,
			speed = 250,
			inEasing = easing.outCubic,
			outEasing = easing.outCubic,
			isScrollable = true,
			topScrollPad = 5,
			bottomScrollPad = 5,            
			onPress = function( event )
			end,
			onStart = function( panel )
				if panel.completeState == "hidden" then
					panel:toFront()
					local substrate = display.newRect( 0, display.screenOriginY, _Xa, display.actualContentHeight )
					substrate.anchorX = 0
					substrate.anchorY = 0
					substrate.isVisible = false
					substrate.isHitTestable = true
					substrate:addEventListener( "touch", function( event )
						if event.phase == "began" then
							slpanel:hide()
							substrate:removeSelf(); substrate = nil
						end
					end )
				elseif panel.completeState == "shown" then
				end
			end,
			headerHeight = header.height-8.5,
			headerColor = { 0.8, 0 }
		}
		slpanel = widget.newSlidingPanel( options )
		slpanel.anchorChildren = true
		slpanel.x = 0
		slpanel.anchorY = 0
		slpanel.y = panel_ypos
		self:insert( slpanel )

		slpanel.header:insert( header )
		slpanel.header.y = slpanel.header.y + 2.5
		slpanel:place( body, { top=10 } )

		function slpanel:setIsActivate( is_active, mess )
			if is_active then
				headerimg2.isVisible = false
				title:setFillColor( 1,0,0 )            
			else headerimg2.isVisible = true                     
				title:setFillColor( 0, 0.4 )
			end
			title.text = mess or title.def_text
		end

		function slpanel:lock()
			local ord_state = self.parent.ordview:getOrderState()
			if ord_state ~= 0 then
				slpanel.isHold = true
				slpanel.alpha = 0.5
				slpanel_bg.isVisible = false
				body.isVisible = false
			end
		end
		slpanel:lock()

		slpanel:hide(0)
		return slpanel
	end

	function new_ovg:createSwitcher( ypos )
		local Switcher = display.newGroup()
		self:insert( Switcher )
		Switcher.anchorChildren = true
		Switcher.x = 0
		Switcher.anchorY = 0
		Switcher.y = ypos
		Switcher.isavailable = true
		local parent = Switcher.parent

		function Switcher:setState( ordstate )
			local alpha_activ = 1
			local alpha_inactiv = 0.4

			if ordstate == 1 or ordstate == 2 or ordstate == 3 or ordstate == 6 then
				if self.rbright.isOn then 
					self.rbleft.label.alpha = alpha_inactiv
				elseif self.rbleft.isOn then  
					self.rbright.label.alpha = alpha_inactiv
				end
				Switcher.isavailable = false	
			else self.rbleft.label.alpha =  alpha_activ
				self.rbright.label.alpha =  alpha_activ
				Switcher.isavailable = true
			end			
		end

		function Switcher:onSwitchPress( event )
				local radiobutn = event.target
			 
				if not self.isavailable then
					if not radiobutn.ison then 
						if radiobutn.id == "rbl" then 
							self.rbright:setState( { isOn = true } )
						elseif radiobutn.id == "rbr" then  
							self.rbleft:setState( { isOn = true } )
						end
						return
					else return
					end
				end 

				if radiobutn.id == "rbl" then
					self.parent.ordview:changeOrderType( 0 )
				parent:createPanels( parent.header_height )
					self.parent.mainbtns:setState( self.parent.ordview:getOrderState() )
					radiobutn.ison = true
					self.rbright.ison = false
				
				elseif self.parent.ordview:type() ~= 3 then
					self.parent.ordview:changeOrderType( 2 )
				parent:createPanels( parent.header_height )
					self.parent.mainbtns:setState( self.parent.ordview:getOrderState() )
					radiobutn.ison = true
					self.rbleft.ison = false
					
				else radiobutn.ison = true
					self.rbleft.ison = false
				end
			
		end

		Switcher.bg = display.newRect( Switcher, 0, 0, _Xa, 50 )
		Switcher.bg:setFillColor( 0,0,0, 0 )
		local lref = -Switcher.bg.width*0.5
		local rref = Switcher.bg.width*0.5
		local tref = -Switcher.bg.height*0.5
		local bref = Switcher.bg.height*0.5

		local radiogroup = display.newGroup()
		Switcher:insert( radiogroup )
		radiogroup:scale( 0.85, 0.85 )

		local options = {
				width = 40,
				height = 40,
				numFrames = 2,
				sheetContentWidth = 80,
				sheetContentHeight = 40
		}
		local radioButtonSheet = graphics.newImageSheet( "assets/orders/bigRadio.png", options )

		Switcher.rbleft = widget.newSwitch
		{
				style = "radio",
				id = "rbl",
				initialSwitchState = true,
				onPress = function( event )
					Switcher:onSwitchPress( event )
				end,
				sheet = radioButtonSheet,
				frameOff = 2,
				frameOn = 1
		}
		Switcher.rbleft.x = lref+15
		Switcher.rbleft.y = -2	
		radiogroup:insert( Switcher.rbleft )
	
		Switcher.rbleft.label = display.newGroup()
		Switcher.rbleft.label.anchorChildren = true
		Switcher.rbleft.label.anchorX = 0
		Switcher.rbleft.label.x = Switcher.rbleft.x + Switcher.rbleft.width + 7
		Switcher.rbleft.label.y = -1
		Switcher:insert( Switcher.rbleft.label )
		local lb_line1 = display.newText( 
			Switcher.rbleft.label,
			translations.OrderTxt_Type[self.lang][1][1],
			0, 0, 
			0, 0,
			parent.font_title, parent.fsize_title 
		)
		lb_line1:setFillColor( 0,0,0 )
		lb_line1.anchorX = 0
		lb_line1.anchorY = 1
		local lb_line2 = display.newText( 
			Switcher.rbleft.label,
			translations.OrderTxt_Type[self.lang][1][2],
			0, -3, 
			0, 0,
			parent.font_title, parent.fsize_title
		)
		lb_line2:setFillColor( 0,0,0 )
		lb_line2.anchorX = 0
		lb_line2.anchorY = 0
		
		local rbright_label = display.newGroup()
		rbright_label.anchorChildren = true
		rbright_label.anchorX = 1
		rbright_label.x = rref - self.pad_title
		rbright_label.y = -1
		Switcher:insert( rbright_label )
		local rb_line1 = display.newText( 
			rbright_label,
			translations.OrderTxt_Type[self.lang][2][1],
			0, 0, 
			0, 0,
			parent.font_title, parent.fsize_title
		)
		rb_line1:setFillColor( 0,0,0 )
		rb_line1.anchorX = 0
		rb_line1.anchorY = 1
		local rb_line2 = display.newText( 
			rbright_label,
			translations.OrderTxt_Type[self.lang][2][2],
			0, -3, 
			0, 0,
			parent.font_title, parent.fsize_title
		)
		rb_line2:setFillColor( 0,0,0 )
		rb_line2.anchorX = 0
		rb_line2.anchorY = 0
		
		Switcher.rbright = widget.newSwitch
		{
				style = "radio",
				id = "rbr",
				onPress = function( event )
					Switcher:onSwitchPress( event )
				end,
				sheet = radioButtonSheet,
				frameOff = 2,
				frameOn = 1
		}
		Switcher.rbright.label = rbright_label
		Switcher.rbright.x = rref - Switcher.rbright.label.width - Switcher.rbright.width
		Switcher.rbright.y = -2
		radiogroup:insert( Switcher.rbright )

		Switcher.sepr = display.newRect( Switcher, 0, bref, _Xa, 1 )
		Switcher.sepr:setFillColor( 0,0,0, 0.1 )

		if parent.ordview:type() == 0 then
			Switcher.rbleft:setState( { isOn = true } )
			Switcher.rbleft.ison = true
		else Switcher.rbright:setState( { isOn = true } )
			Switcher.rbright.ison = true
		end
		Switcher:setState( parent.ordview:getOrderState() )

		return Switcher
	end

	function new_ovg:getTextWidth( text, font, font_size )
		local temp_text = display.newText( text, 0,0, font, font_size )
		local width = temp_text.width
		temp_text:removeSelf()
		temp_text = nil
		return width
	end

	function new_ovg:showAlert( mode, onAccept )
		local translat = ""
		print("showAlert",mode)
		if mode == 1 then   	
			translat = translations.OrderTxt_SendingDialog[self.lang]	
				native.showAlert( 
					translat[1][1], 
					translat[1][2],
					{ translat[1][3], translat[1][4] }, 
					function( event )
							if "clicked" == event.action then
								if event.index == 1 then onAccept() end
						end
					end 
				)
		elseif mode == 2 then   	
			translat = translations.OrderTxt_SendingDialog[self.lang]
			local delivdate = Date( self.ordview.odata.ord_reserv_datetime )
			local mess = string.gsub( translat[2][2], "!d!", delivdate:fmt( "%d/%m/%Y" ) )	
			mess = string.gsub( mess, "!t!", delivdate:fmt( "%H:%M" ) )
				native.showAlert( 
					translat[2][1], 
					mess,
					{ translat[2][3], translat[2][4] }, 
					function( event )
							if "clicked" == event.action then
								if event.index == 1 then onAccept() end
						end
					end 
				)
		elseif mode == 3 then   	
			translat = translations.OrderTxt_PayingDialog[self.lang]	
				native.showAlert( 
					translat[1], 
					translat[2],
					{ translat[3], translat[4] }, 
					function( event )
							if "clicked" == event.action then
								if event.index == 1 then onAccept() end
						end
					end 
				)
		elseif mode == 4 then   	
			translat = translations.OrderTxt_RepeatingDialog[self.lang]	
				native.showAlert( 
					translat[1], 
					translat[2],
					{ translat[3], translat[4] }, 
					function( event )
							if "clicked" == event.action then
								if event.index == 1 then onAccept() end
						end
					end 
				)
		elseif mode == 5 then   	
			translat = translations.OrderTxt_DeletingDialog[self.lang]	
				native.showAlert( 
					translat[1][1], 
					translat[1][2],
					{ translat[1][3], translat[1][4] }, 
					function( event )
							if "clicked" == event.action then
								if event.index == 1 then onAccept() end
						end
					end 
				)
		elseif mode == 6 then   	
			translat = translations.OrderTxt_DeletingDialog[self.lang]	
				native.showAlert( 
					translat[2][1], 
					translat[2][2],
					{ translat[2][3], translat[2][4] }, 
					function( event )
							if "clicked" == event.action then
								if event.index == 1 then onAccept() end
						end
					end 
				)
		elseif mode == 7 then   	
			translat = translations.OrderTxt_DeletingDialog[self.lang]	
				native.showAlert( 
					translat[3][1], 
					translat[3][2],
					{ translat[3][3], translat[3][4] }, 
					function( event )
							if "clicked" == event.action then
								if event.index == 1 then onAccept() end
						end
					end 
				)
		elseif mode == 8 then   	
			translat = translations.OrderTxt_CallWaiter[self.lang]	
				native.showAlert( 
					translat[1], 
					translat[2],
					{ translat[3], translat[4] }, 
					function( event )
							if "clicked" == event.action then
								if event.index == 1 then onAccept() end
						end
					end 
				)
		elseif mode == 9 then   	
			translat = translations.OrderTxt_SendingDialog[self.lang]
			local delivdate = Date( self.ordview.odata.ord_reserv_datetime )
			local mess = string.gsub( translat[3][2], "!d!", delivdate:fmt( "%d/%m/%Y" ) )
			mess = string.gsub( mess, "!t!", delivdate:fmt( "%H:%M" ) )
			mess = string.gsub( mess, "!a!", self.ordview.odata.ord_comment )	
			native.showAlert( 
				translat[3][1], 
				mess,
				{ translat[3][3], translat[3][4] }, 
				function( event )
					if "clicked" == event.action then
						if event.index == 1 then onAccept() end
					end
				end 
			)
		end
	end
	
	function new_ovg:payDialog( ord_id,onClose,use_bonusme_proccessing )
		print("new_ov:payDialog")

		local function getUrlFor_11(p)
			Spinner:start()
			local function listener(e)
				Spinner:stop()
				if e.data and e.data.url then
					local isShowSpinner = true
					Spinner:start()
					local function webListener( e ) -- слушатель только для остановки спиннера 
					    if e.url then
					    	if isShowSpinner then
								Spinner:stop()
					    	end
					    	isShowSpinner = false
					    end
					    -- if e.type then
					    -- end					  
					    if e.errorCode then
							Spinner:stop()
					    end
					end
					-- открываем
					local webView = native.newWebView( 0, ((display.topStatusBarContentHeight)/2)-headerHeight*2,_Xa,display.safeActualContentHeight-(headerHeight*2+display.topStatusBarContentHeight) )
					local rectOverButtons = display.newRect( 0, ((headerHeight + display.topStatusBarContentHeight)/2), _Xa, display.safeActualContentHeight-(headerHeight+display.topStatusBarContentHeight) )
					self:insert(rectOverButtons)	    
					rectOverButtons:setFillColor( 1 )
					local closeBtn = widget.newButton({
							x = rectOverButtons.x,
							y = rectOverButtons.y,
							label = translations.ClosePaymentDlgBtn[self.lang],
							labelColor = { default={ 1, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
							fontSize = 13.5,
							font = self.font_bold,
							--properties for a rounded rectangle button...
								shape="roundedRect",
								width = 170,
								height = 39,
								cornerRadius = 5,
								fillColor = { default={ 1,1,1 }, over={ 211/255,211/255,211/255 } },
								strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
								strokeWidth = 2,	 
								onRelease = function( event )
								webView:removeSelf()
								event.target:removeSelf()
								rectOverButtons:removeSelf( )
								composer.getScene("actions.my_orders_scene").ovm:backgroundUpdate(true)
							end	    
					})
					closeBtn.x = 0
					closeBtn.y = webView.y+webView.height/2+30
					self:insert(webView)
					self:insert(closeBtn)
					webView:request(e.data.url)
					webView:addEventListener( "urlRequest", webListener )
				end
			end
			rdb_common:getDataFromView{listener=listener,func_call=3,table_name="get_pay_url",filter={order_id=ord_id,pays_id=11,type=""},admin_auth=false}
		end

		-- открываем список типами оплат 
		local listSel = {}
		if userProfile.SelectedRestaurantData.typePays and #userProfile.SelectedRestaurantData.typePays > 0 then
			for k, v in pairs(userProfile.SelectedRestaurantData.typePays) do
				listSel[k] = v.pays_name
			end
		else
			native.showAlert(_G.appTitle, userProfile.SelectedRestaurantData.title.."\n"..translations["w_no_pay_type"][_G.Lang],{ "OK" } ) 
			return
		end
		
		local wl,hl = _Xa*0.7,200
        local function onSelect(v)
        	local tp = userProfile.SelectedRestaurantData.typePays[v].pays_id
        	if tp ~= nil then
	        	if tp == 11 then
					-- запрашиваем url
					getUrlFor_11() 
				else
					native.showAlert(_G.appTitle, listSel[v].."\n"..translations["w_no_imp__pay_type"][_G.Lang],{ "OK" } ) 
	        	end
	        end
        end
        local function drawTap()
            local r=display.newRoundedRect(0,0,wl*0.9,60,4)
            r:setFillColor(1)       
            return r
        end 
        local list = ui_elm:newSelectList({width=wl,height=hl,data=listSel,curKey=0,drawTap=drawTap,colorBgItAct=css.color_green,sizeFont=css.size_font_4+6,onSet=onSelect,onShow=onClose,alignLabel="left",isLine=true})
        -- local x,y= e.target:localToContent( 0, 0 )
        list.x=_Xa*0.5
        list.y=_Ya*0.5
		do 
			return
		end 

		-- все что ниже в  этой фнукции старое

		-- local webView
		-- local rectOverButtons
		-- local closeBtn
		-- local webView = native.newWebView( display.contentCenterX, display.contentCenterY+(headerHeight + display.topStatusBarContentHeight)/2, _Xa, 2+display.actualContentHeight-(headerHeight + display.topStatusBarContentHeight) )
		-- local webView = display.newRect( 0, ((headerHeight + display.topStatusBarContentHeight)/2)-55, _Xa-2, -110+display.actualContentHeight-(headerHeight + display.topStatusBarContentHeight) )
		-- webView.strokeWidth = 3
		-- webView:setFillColor( 0.5 )
		-- webView:setStrokeColor( 1, 0, 0 )

	    -- local function networkListener( state, data )
	    -- 	-- print("!!!!!!!!!!!!!!!!!!!!",state)        
	    -- 	printResponse(data)
	    --     if state == "success" then
	    --     	if (data.response.response_status  == "failure") then
					-- webView:removeSelf()
					-- closeBtn:removeSelf()
					-- rectOverButtons:removeSelf( )	        		
	    --     		native.showAlert( "Error!", data.response.error_message, { "OK" } )
	    --     	else
	    --         	print(data.response.checkout_url)
	    --         	webView:request( data.response.checkout_url )
	    --     	end
	    --     else

	    --     end
	    -- end   
	    -- print("makePayment")

		-- local function callPayment(rectoken_active,bonuses_used)	    
		-- 	local bonuses_used=bonuses_used
		-- 	if bonuses_used==nil then
		-- 		bonuses_used=0
		-- 	end
	 --    	webView = native.newWebView( 0, ((headerHeight + display.topStatusBarContentHeight)/2)-55, _Xa-2, -110+display.actualContentHeight-(headerHeight + display.topStatusBarContentHeight) )
	 --    	local a=(display.actualContentHeight-(webView.height))/2
	 --    	rectOverButtons = display.newRect( 0, ((headerHeight + display.topStatusBarContentHeight)/2), _Xa-2, display.actualContentHeight-(headerHeight + display.topStatusBarContentHeight) )
		--    	self:insert(rectOverButtons)	    
		--    	rectOverButtons:setFillColor( 1 )
		-- 	closeBtn = widget.newButton({
		-- 			label = translations.ClosePaymentDlgBtn[self.lang],
		-- 			labelColor = { default={ 1, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
		-- 			fontSize = 13.5,
		-- 			font = self.font_bold,
		-- 			--properties for a rounded rectangle button...
		-- 				shape="roundedRect",
		-- 				width = 170,
		-- 				height = 39,
		-- 				cornerRadius = 5,
		-- 				fillColor = { default={ 1,1,1 }, over={ 211/255,211/255,211/255 } },
		-- 				strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
		-- 				strokeWidth = 2,	 
		-- 			onRelease = function( event )
		-- 				webView:removeSelf()
		-- 				event.target:removeSelf()
		-- 				rectOverButtons:removeSelf( )
	        	
	 --     				--ov.new_ovm:backgroundUpdate()
	 --     				composer.getScene("actions.my_orders_scene"):refresh()


		-- 			end	    
		-- 	})
		-- 	closeBtn.x = 0
		-- 	closeBtn.y = webView.y+webView.height/2+30
		--    	self:insert(webView)

		-- 	self:insert(closeBtn)	   	

		--     print("ord sum "..( self.ordview:getOrderSum() * ( 1+self.ordview:getTipsPerc() )  ))
		--     local sum_to_pay=self.ordview:getOrderSum() * ( 1+self.ordview:getTipsPerc() )-bonuses_used
		--     local orderData = {orderid=tostring(ord_id),
		--     guest_id=self.ordview.odata.guest_id,
		--     pos_id=self.ordview.odata.pos_id,
		--     merchant_data=tostring(ord_id)..";"..tostring(self.ordview:getOrderSum() *self.ordview:getTipsPerc()*100)..";"..tostring(bonuses_used*100),order_desc=userProfile.SelectedRestaurantData.title,merchantid=userProfile.SelectedRestaurantData.pos_bonusme_merchant_id,sum=(sum_to_pay)*100,
		--     rectoken=rectoken_active,
		--     sum=sum_to_pay,
		--     order_desc="Testorder",
		-- 	guest_name="Harigopal",
		-- 	guest_address="Hyderabad",
		-- 	guest_city="Hyderabad",
		-- 	guest_state="Hyderabad",
		-- 	guest_postal_code="500082",
		-- 	guest_email="ppopl@gmail.com",
		-- 	guest_phone="9853766496",		    
		--     bonuses_used=tostring(bonuses_used)}

		-- local orderData = {orderid="A00020",
		-- guest_id="3233",
		-- pos_id="247",
		-- merchant_data="md",
		-- order_desc="Testorder",
		-- merchantid=1007870,
		-- sum="20.00",
		-- guest_name="Harigopal",
		-- guest_address="Hyderabad",
		-- guest_city="Hyderabad",
		-- guest_state="Hyderabad",
		-- guest_postal_code="500082",
		-- guest_email="ppopl@gmail.com",
		-- guest_phone="9853766496",
		-- rectoken=0,
		-- bonuses_used=0}
			    
		--     if use_bonusme_proccessing==1 then
		--     	rdb:makePayment( orderData,networkListener )
		-- 	    local function webListener( event )
		-- 	        if event.url then
		-- 	            print( "You are visiting: " .. event.url )
		-- 	        end

		-- 	        if event.type then
		-- 	            print( "The event.type is " .. event.type ) -- print the type of request
		-- 	        end

		-- 	        if event.errorCode then
		-- 	            native.showAlert( "Error!", event.errorMessage, { "OK" } )
		-- 	        end
		-- 	    end
		-- 		webView:addEventListener( "urlRequest", webListener ) 		    	
		-- 	else
		-- 		local function networkListener( result,data )
		-- 			if  result == "success" then
		-- 				webView:request(  data.redirectURL)
		-- 			end
		-- 		end	
		-- 		rdb:makePayment( orderData,networkListener,"paynear" )				
		-- 	end
		--  --   local orderData = {response_status="success",order_id=3918847}
		--  --   rdb:makePayment( orderData,networkListener)
		-- end
	 --    --lets get user cards
		-- local function showCardList(cardsList)
		
		-- 	local newsFullGroup=display.newGroup( )
				
		-- 	local rect = display.newRect( 0,display.screenOriginY, display.contentWidth-2*display.screenOriginX, display.contentHeight-2*display.screenOriginY )
		-- 	rect.anchorX=0;rect.anchorY=0
		-- 	rect:setFillColor( 0, 0, 0,0.7 ) 
		-- 	newsFullGroup:insert( rect)       
		-- 	local bg = display.newRoundedRect( newsFullGroup, _mX, 80, _Xa-35, _mY-30,4 )
		-- 		local paint = { 0,0, 0, 0.3 }
		-- 		bg.stroke = paint
		-- 		bg.strokeWidth = 1
		-- 		bg.anchorY=0
		-- 		bg.anchorX=0.5
		-- 		bg:setFillColor( 1 )
		-- 		newsFullGroup:insert( bg )  
		-- 	local balanse_bonuses=userProfile.balance[userProfile.SelectedRestaurantData.com_id]
		-- 	local bonus_y=0
		-- 	local bonus_sum
		-- 	if  (balanse_bonuses and balanse_bonuses>0) then   
		-- 			local balance_txt = display.newText({ 
		-- 					text = translations.balance[_G.Lang]..": "..balanse_bonuses, 
		-- 					font = "HelveticaNeueCyr-Light", 
		-- 					fontSize = 14
		-- 				})
		-- 			balance_txt:setFillColor( 0,0,0, 0.8  )
		-- 			balance_txt.anchorX=0;balance_txt.anchorY=0.5		            
		-- 			balance_txt.x=50;balance_txt.y=bg.y+30		            
		-- 			newsFullGroup:insert( balance_txt )  
		-- 			local balance_txt = display.newText({ 
		-- 					text = translations.pay_by_bonuses[_G.Lang]..":", 
		-- 					font = "HelveticaNeueCyr-Light", 
		-- 					fontSize = 14
		-- 				})
		-- 			balance_txt:setFillColor( 0,0,0, 0.8  )
		-- 			balance_txt.anchorX=0;balance_txt.anchorY=0.5		            
		-- 			balance_txt.x=50;balance_txt.y=bg.y+55		            
		-- 			newsFullGroup:insert( balance_txt )  
		-- 			bonus_sum  = native.newTextField( -25, 0, 60, 30)			
		-- 				bonus_sum.anchorX = 0
		-- 				bonus_sum.align = "left";  bonus_sum:setTextColor( 1, 0.5, 0 )
		-- 				bonus_sum.size = 12
		-- 			bonus_sum.anchorX=0;bonus_sum.anchorY=0.5		            
		-- 			bonus_sum.x=200;bonus_sum.y=bg.y+55						
		-- 				bonus_sum.inputType = "decimal"
		-- 				bonus_sum.text = "0"
					
		-- 				newsFullGroup:insert(bonus_sum)
		-- 			bonus_y=50       
		-- 			if #cardsList == 0 then
		-- 				bonus_y=-40
		-- 			end
		-- 	end
		-- 	bg.height=bg.height+bonus_y
		-- 			local options = {
		-- 					width = 31,
		-- 					height = 30,
		-- 					numFrames = 2,
		-- 					sheetContentWidth = 62,
		-- 					sheetContentHeight = 30
		-- 			}    		
		-- 		local radioButtonSheet = graphics.newImageSheet( "assets/orders/itemCheckbox.png", options )
		-- 		local rectoken_active="0"
		-- 		local function onSwitchPress( event )
		-- 				local switch = event.target
		-- 				--if switch.isOn==true then
		-- 				--end
		-- 				rectoken_active = switch.rectoken
		-- 			print( "Switch with ID '"..switch.id.."' is on: "..tostring(switch.isOn) )
		-- 		end

		-- 	for i=math.max(1,#cardsList-2),#cardsList do
		-- 			local card_txt = display.newText({ 
		-- 					text = cardsList[i].guestc_masked_card, 
		-- 					font = "HelveticaNeueCyr-Light", 
		-- 					fontSize = 14
		-- 				})
		-- 				--setFillColor( 1,0,0, 0.8 )
		-- 			card_txt:setFillColor( 0,0,0, 0.8  )
		-- 			card_txt.anchorX=0
		-- 			card_txt.anchorY=0.5
		-- 			card_txt.x=50
		-- 			card_txt.y=bg.y+15+(i)*35+bonus_y
		-- 			newsFullGroup:insert( card_txt ) 

		-- 				local checkbox = widget.newSwitch
		-- 				{
		-- 						id = i,
		-- 						x = 0,
		-- 						y = 0,
		-- 						style = "radio",
		-- 						onPress = onSwitchPress,
		-- 						sheet = radioButtonSheet,
		-- 						initialSwitchState = false,
		-- 						frameOff = 1,
		-- 						frameOn = 2
		-- 				}
		-- 				checkbox.rectoken=cardsList[i].guestc_rectoken
		-- 				checkbox.xScale=0.8;checkbox.yScale=0.8
		-- 				checkbox.x=_Xa*0.5+70
		-- 				checkbox.y=bg.y+15+(i)*35+bonus_y	;checkbox.anchorY  = 0.5;
		-- 				newsFullGroup:insert( checkbox )

		-- 	end
		-- 	local i=#cardsList+1
		-- 	if #cardsList>0 then
		-- 			local card_txt = display.newText({ 
		-- 					text = translations.new_card[_G.Lang], 
		-- 					font = "HelveticaNeueCyr-Light", 
		-- 					fontSize = 14
		-- 				})
		-- 				--setFillColor( 1,0,0, 0.8 )
		-- 			card_txt:setFillColor( 0,0,0, 0.8  )
		-- 			card_txt.anchorX=0
		-- 			card_txt.anchorY=0.5
		-- 			card_txt.x=50
		-- 			card_txt.y=bg.y+15+(i)*35+bonus_y
		-- 			newsFullGroup:insert( card_txt ) 

		-- 				local checkbox = widget.newSwitch
		-- 				{
		-- 						id = i,
		-- 						x = 0,
		-- 						y = 0,
		-- 						style = "radio",
		-- 						onPress = onSwitchPress,
		-- 						sheet = radioButtonSheet,
		-- 						initialSwitchState = false,
		-- 						frameOff = 1,
		-- 						frameOn = 2
		-- 				}
		-- 				checkbox.rectoken="0"
		-- 				checkbox.xScale=0.8;checkbox.yScale=0.8
		-- 				checkbox.x=_Xa*0.5+70
		-- 				checkbox.y=bg.y+15+(i)*35+bonus_y	;checkbox.anchorY  = 0.5;
		-- 				newsFullGroup:insert( checkbox )
		-- 				--print(_G.lang)
		-- 				--print(_G.lang,translations.OrderTxt_MainButtons[_G.lang])
		-- 		end
		-- 	local leftbut = widget.newButton({
		-- 	labelColor = { default={1}, over={0} },
		-- 	font = "HelveticaNeueCyr-Light",
		-- 	fontSize = 16,
		-- 	label = translations.OrderTxt_MainButtons[_G.Lang][1][5],
		-- 	onRelease = function( event )
		-- 		v_res=true
		-- 		bonuses_used=0
		-- 		local balanse_bonuses=userProfile.balance[userProfile.SelectedRestaurantData.com_id]
		-- 		if  (balanse_bonuses and balanse_bonuses>0) then
		-- 			if (bonus_sum.text~=nil and bonus_sum.text~="") then
		-- 					if (tonumber( bonus_sum.text)>balanse_bonuses) then
		-- 						v_res=false
		-- 					else
		-- 						bonuses_used=bonus_sum.text
		-- 					end
		-- 				end 
		-- 			end
		-- 			if v_res then 
		-- 			callPayment(rectoken_active,bonuses_used)
		-- 			newsFullGroup:removeSelf( )
		-- 			newsFullGroup = nil
		-- 		else
		-- 				native.showAlert(_G.appTitle, translations["too_much_bonuses"][_G.Lang], 
		-- 					{ "OK" } )
		-- 		end
		-- 	end,
		-- 	--properties for a rounded rectangle button...
		-- 	shape = "roundedRect",
		-- 	width = 120,
		-- 	height = 30,
		-- 	cornerRadius = 5,
		-- 	fillColor = { default={ 56/255, 214/255, 128/255 }, over={ 211/255,211/255,211/255 } },
		-- 	strokeColor = { default={ 56/255, 214/255, 128/255 }, over={ 0,0,0, 0.13 } },
		-- 	strokeWidth = 2
		-- 	})
		-- 	--leftbut.data=event.target.data
		-- 	leftbut.anchorX = 0.5
		-- 	leftbut.x = _mX   
		-- 	leftbut.y = bg.y+bg.height-40+bonus_y/4
		-- 	newsFullGroup:insert( leftbut)
		-- 	bg:addEventListener( "touch", function() return true end )
		-- 	rect:addEventListener( "tap", function() return true end )
		-- 	rect:addEventListener( "touch", function(event)
		-- 	if event.phase == "ended" then
		-- 	newsFullGroup:removeSelf( )
		-- 	newsFullGroup = nil
		-- 	end
		-- 	return true 
		-- 	end)
		-- end    

		-- local function netCardsListener( state, data )
		-- 	--print("DDDDDDDDDDDDDDDDD")
		-- 	--printResponse(state,data)
		-- 	if state == "success" then
		-- 		local bb=userProfile.balance[userProfile.SelectedRestaurantData.com_id]
		-- 		if (data and #data>0) or (bb and bb>0) then
		-- 			showCardList(data)
		-- 		else
		-- 			callPayment("0")
		-- 		end
		-- 		--callPayment()
		-- 	else
		-- 			if (bb and bb>0) then
		-- 			showCardList({})
		-- 		else
		-- 			callPayment("0")
		-- 		end
		-- 	end
		-- end   

		-- local filter = {
		-- 	group = "and",
		-- 	conditions = {
		-- 		{ 
		-- 			left = "guest_id",
		-- 			oper = "=",
		-- 			right = self.ordview.odata.guest_id
		-- 		}	
		-- 	}
		-- }

		-- rdb:getData( "guests_cards", netCardsListener, filter )
	end	

	function new_ovg:createMainButtons( ypos )
		local MainButtons = display.newGroup()
		MainButtons.anchorChildren = true
		MainButtons.x = 0
		MainButtons.anchorY = 1
		MainButtons.y = ypos
		self:insert( MainButtons )
		local parent = self
		MainButtons.isavailable = true
		MainButtons.prev_ordstate = -1

		MainButtons.bg = display.newRect( MainButtons, 0, 0, _Xa, 70 )
		MainButtons.bg:setFillColor( 0,0,0, 0 )
		local lref = -MainButtons.bg.width*0.5
		local rref = MainButtons.bg.width*0.5
		local tref = -MainButtons.bg.height*0.5
		local bref = MainButtons.bg.height*0.5		

		function MainButtons:setState( ordstate )
			-- ordstate = 6
			if ordstate and prev_ordstate == ordstate then return end
			local order = self.parent.ordview
			local ordtype = order:type()
			local leftparams = {}
			local rightparams = {}
			local translat = translations.OrderTxt_MainButtons[parent.lang]

			if not ordstate then 		-- если тип заказа не выбран пользователем
				ordtype = 0
				ordstate = 1
			end

			if ordtype == 2 or ordtype == 3 then
				if #order.oidata == 0 then
					leftparams.text = translat[1][1]
					leftparams.mode = 2
				else leftparams.text = translat[1][2]
					leftparams.mode = 2				
				end 
			else leftparams.text = translat[1][2]
				leftparams.mode = 1
			end
			if ordtype == 0 and ordstate == 2 then
				rightparams.text = translat[2][3]
				rightparams.mode = 3
				rightparams.labelColor = { 0, 0, 0, 0.8 }
				rightparams.fillColor = { 1,1,1 }
				rightparams.strokeColor = { 0,0,0, 0.13 }
			else rightparams.text = translat[2][1]
				rightparams.mode = 1
			end

			if order.odata.ord_reserv_state == 3 then
				ordstate = 3
			end
			-- print("@@@@@@@@@@@",new_ovg.ordview.cid, ordstate, leftparams.text )
			-- if  ordstate == 6 or ((ordstate == 5 or ordstate == 2) and (userProfile.SelectedRestaurantData.use_bonusme_proccessing==1 or userProfile.SelectedRestaurantData.use_bonusme_proccessing==2)) then --Jen ordstate == 2 or
			if  ordstate == 6 or ordstate == 5 or ordstate == 2 then --Jen ordstate == 2 or
				local topay = order:getOrderSum() * ( 1+parent.ordview:getTipsPerc() )
				-- if userProfile.SelectedRestaurantData.use_bonusme_proccessing==1 or userProfile.SelectedRestaurantData.use_bonusme_proccessing==2 then
					-- оплатить заказ
					leftparams.text = translat[1][5].." "..topay.._G.Currency
				-- else
					-- закрыть заказ
					-- leftparams.text = translat[1][3]
				-- end
				leftparams.mode = 3
				leftparams.labelColor = {1,0,0}-- { 244/255, 64/255, 66/255 }
			elseif ordstate == 3 then
				leftparams.text = translat[1][3]
				leftparams.mode = 5
			elseif ordstate == 7 then
				leftparams.text = translat[1][3]
				leftparams.mode = 7				
			elseif ordstate == 4 then
				leftparams.text = translat[1][4]
				leftparams.mode = 4
			end
			-- print("@@@@@@@@@@@", new_ovg.ordview.cid, ordstate, leftparams.text )

			if order.odata.ord_reserv_state == 3 then
				leftparams.active = true
				rightparams.active = false
			elseif ordstate == 1 then
				leftparams.active = false
				rightparams.active = true
			elseif ordstate == 3 or ordstate == 4 or ordstate == 6 then
				leftparams.active = true
				rightparams.active = false				
			elseif (ordstate == 5 or ordstate == 2) then --jen напечатаны встречки или подтвержден заведением, изменять нельзя
				leftparams.active = true
				rightparams.active = false
			-- elseif (ordstate == 5 or ordstate == 2) and (userProfile.SelectedRestaurantData.use_bonusme_proccessing==1 or userProfile.SelectedRestaurantData.use_bonusme_proccessing==2) then --jen напечатаны встречки или подтвержден заведением, изменять нельзя
				-- leftparams.active = true
				-- rightparams.active = false								
			-- elseif (ordstate == 5 or ordstate == 2) and (userProfile.SelectedRestaurantData.use_bonusme_proccessing~=1 and userProfile.SelectedRestaurantData.use_bonusme_proccessing~=2) then
				-- leftparams.active = false
				-- rightparams.active = false					
			elseif ordstate == 7 then
				if #order.oidata == 0 then
					leftparams.active = false
				else leftparams.active = true
				end
				rightparams.active = true				
			else leftparams.active = true
				rightparams.active = true
			end
			if ordstate == 7 then
				leftparams.active = true
				rightparams.active = false	
			end
			if (ordtype == 0 or ordtype == 1 or ordtype == 2) and #order.oidata == 0 then
				leftparams.active = false
			end
			
			leftparams.width = parent:getTextWidth( leftparams.text, parent.font_bold, 13.5 )+20
			rightparams.width = parent:getTextWidth( rightparams.text, parent.font_bold, 13.5 )+20

			local offside = leftparams.width+rightparams.width - (_Xa-parent.pad_regular*2)
			if offside > 0 then
				leftparams.width = leftparams.width - offside*0.5
				rightparams.width = rightparams.width - offside*0.5
			end
			
			if leftparams.active then
				if rightparams.active then
					leftparams.pad = (_Xa - leftparams.width - rightparams.width)/3					
				else leftparams.width = leftparams.width + 30
					leftparams.pad = (_Xa - leftparams.width)/2
				end 
				if self.leftbutn then self.leftbutn:removeSelf() end	
				self.leftbutn = self:doLeftButn( leftparams )
				self:insert( self.leftbutn )
			elseif self.leftbutn then 
				self.leftbutn:removeSelf() 
				self.leftbutn = nil 
			end
			
			if rightparams.active then
				if leftparams.active then
					rightparams.pad = (_Xa - leftparams.width - rightparams.width)/3
				else rightparams.width = rightparams.width + 30
					rightparams.pad = (_Xa - rightparams.width)/2
				end
				if self.rightbutn then self.rightbutn:removeSelf() end	
				self.rightbutn = self:doRightButn( rightparams )
				self:insert( self.rightbutn )
			elseif self.rightbutn then 
				self.rightbutn:removeSelf() 
				self.rightbutn = nil 
			end			

			self.prev_ordstate = ordstate
		end
		
		function MainButtons:doLeftButn( params )
			params.labelColor = params.labelColor or { 0, 0, 0, 0.8 }
			params.fillColor = params.fillColor or { 1,1,1 }
			params.strokeColor = params.strokeColor or { 0,0,0, 0.13 }
			local leftbutn = widget.newButton({
					label = params.text,
					labelColor = { default=params.labelColor, over={ 0, 0, 0, 1 } },
					fontSize = 13.5,
					font = self.font_bold,
					onRelease = function( event )
						local mode = params.mode
						if mode == 1 then
							--print(parent.ordview.odata.ord_comment)
							if parent.ordview:type() == 1
							and ( not parent.ordview.odata.ord_comment or parent.ordview.odata.ord_comment=="" ) then
								parent.ordview.onThrowError( self, "no address" )
								parent.panelInFocus = parent.delivpanel
								parent.delivpanel:show()
								parent.delivpanel:highlightAddress()
								parent:panelsController( parent.delivpanel, "show" )
							else
								local alert_mode = 1
								if parent.ordview:type() == 1 then alert_mode = 9 end
								parent:showAlert( alert_mode, function()
									parent.spinner:start()
									parent.ordview:sendOrder() 
								end )
							end
						elseif mode == 2 then
							parent:showAlert( 2, function()
								parent.spinner:start()
								parent.ordview:sendReserv() 
							end )
						elseif mode == 3 then  --order is ready for payment

							--jen PayOrderNew
							-- if userProfile.SelectedRestaurantData.use_bonusme_proccessing==1 or userProfile.SelectedRestaurantData.use_bonusme_proccessing==2 then
								
								parent:payDialog( parent.ordview.odata.ord_id,nil,userProfile.SelectedRestaurantData.use_bonusme_proccessing )					
							-- else		
							-- 	parent:showAlert( 3, function()
							-- 		parent.spinner:start()
							-- 		parent.ordview:closeOrder( function()
							-- 			rate.newRateBox({
							-- 				com_id = parent.ordview.odata.com_id,
							-- 				pos_id = parent.ordview.odata.pos_id,
							-- 				guest_id = parent.ordview.odata.guest_id,
							-- 				ord_cid = parent.ordview.odata.ord_id,
							-- 				dishes = parent.ordview.oidata,
							-- 				onClose = function()
							-- 					-- удаляем и создаем все вью заново
							-- 					parent.ordview.onCopied()
							-- 				end										
							-- 			}) 
							-- 		end)
							-- 	end )
							-- end
						elseif mode == 4 then
							parent:showAlert( 4, function()
								Spinner:start()
								parent.ordview:copyOrder()
								--print( "Order Repeat..." )
							end )
						elseif mode == 7 then
							ldb:updateRowDataWithCondition("orders","ord_id="..parent.ordview.odata.ord_id,{ord_reserv_state=3})	
							parent.ordview.onCopied()
						elseif mode == 5 then  --printed check
							rate.newRateBox({
								com_id = parent.ordview.odata.com_id,
								ord_cid = parent.ordview.odata.ord_id,
								pos_id = parent.ordview.odata.pos_id,
								guest_id = parent.ordview.odata.guest_id,								
								dishes = parent.ordview.oidata,
								onClose = function()
									-- удаляем и создаем все вью заново
									parent.ordview.onCopied()
								end
							})

							parent.ordview:closeOrder( function()
							end) 
						end
					end,
					--properties for a rounded rectangle button...
					shape="roundedRect",
					width = params.width,
					height = 37,
					cornerRadius = 5,
					fillColor = { default=params.fillColor, over={ 211/255,211/255,211/255 } },
					strokeColor = { default=params.strokeColor, over={ 0,0,0, 0.13 } },
					strokeWidth = 2	    
			})
			leftbutn.anchorX = 0
			leftbutn.x = lref + params.pad
			leftbutn.y = 0
			return leftbutn
		end
		
		function MainButtons:doRightButn( params )
			params.labelColor = params.labelColor or { 1, 1, 1 }
			params.fillColor = params.fillColor or { 244/255, 64/255, 66/255 }
			params.strokeColor = params.strokeColor or { 244/255, 64/255, 66/255 }
			local rightbutn = widget.newButton({
					label = params.text,
					labelColor = { default=params.labelColor, over={ 0, 0, 0, 1 } },
					fontSize = 13.5,
					font = self.font_bold,
					onRelease = function( event )
						local translat = translations.OrderTxt_DeletingDialog[parent.lang]
						local ordstate = parent.ordview:getOrderState()
						if params.mode == 1 then
							if ordstate == 1 or  ordstate == 2 or  ordstate == 6 or  ordstate == 7  then
								parent:showAlert( 5, function()
									Spinner:start()
										parent.ordview:updateOrder( function( new_ordstate )
											local ordtype = parent.ordview:type()
											if ordtype == 0 and new_ordstate == 2 then 
												parent:catchError( self.ordview, "delete denied" )
												return
											elseif new_ordstate == 3 then
												parent:catchError( self.ordview, "delete denied" )
												return
											end
											parent.ordview:deleteOrder() 
										end)
							end )
							else 
								parent:showAlert( 6, function()
								Spinner:start()
									parent.ordview:deleteOrder()
								end )
							end
						elseif params.mode == 2 then
							parent:showAlert( 7, function()
								parent.spinner:start()						        	
									parent.ordview:cancelReserv() 
							end )
						elseif params.mode == 3 then
							parent:showAlert( 8, function()
								parent.spinner:start()
								parent.ordview:callWaiter()
							end )
						end
					end,
					--properties for a rounded rectangle button...
					shape="roundedRect",
					width = params.width,
					height = 37,
					cornerRadius = 5,
					fillColor = { default=params.fillColor, over={ 211/255,211/255,211/255 } },
					strokeColor = { default=params.strokeColor, over={ 0,0,0, 0.13 } },
					strokeWidth = 2	    
			})
			rightbutn.anchorX = 1
			rightbutn.x = rref - params.pad
			rightbutn.y = 0
			return rightbutn
		end

		MainButtons:setState( parent.ordview:getOrderState() )
		return MainButtons
	end

	function new_ovg:createPlate( params )
		local text = params.text or ""
		local font = params.font or native.systemFont
		local fsize = params.fsize or 10
		local withEditField = params.withEditField
		local pad = params.pad or 0
		local align = params.align or "center"
		local color = params.color or { 0,0,0, 1 }
		if withEditField==nil then
			withEditField=false
		end
		local plate = display.newGroup()
		plate.font = font
		plate.fsize = fsize
		plate.pad = pad
		plate.align = align


		plate.anchorChildren = true
		if withEditField==false then
			plate.label = display.newText({
				parent = plate,
				text = text,
				font = font, 
				fontSize = fsize,
			})
			plate.label:setFillColor( unpack( color ) )
		else
			plate.label  = native.newTextField( -25, 0, 60, 30)


			local function textListener( event )

				if ( event.phase == "editing" ) then
			        -- Output resulting text from "defaultField"
			        local v=event.target.text
			        if v==nil then
			        	v=0
			        end
			        params.listener( tonumber(v) )

			    end
			end			
			plate.label.anchorX = 0
			plate.label.align = "left";  plate.label:setTextColor( 1, 0.5, 0 )
			plate.label.size = fsize
			plate.label.inputType = "decimal"
			plate.label.text = text
			plate.label:addEventListener( "userInput", textListener  )
			plate:insert(plate.label)
			-- inputFd.font = native.newFont( native.systemFont, 20 )
		end
		
		local ul_width = self:getTextWidth( text, font, fsize )
		plate.line = display.newRect( plate, 0, 0, ul_width+10, 1 )
		plate.line:setFillColor( 0,0,0, 0.1 )
		plate.line.y = plate.label.height*0.5 + pad

		function plate:setText( text )
			local prev_wlabel = self.label.width
			self.label.text = text
			local growth = (self.label.width - prev_wlabel)*0.5
			self.line.width = self.label.width + 10
			local align = 0
			
			if self.align == "left" then align = 1
			elseif self.align == "right" then align = -1
			end			
			
			if growth > 0 then 
				self.label.x = self.label.x + growth*align
				self.line.x = self.line.x + growth*align
			elseif growth < 0 then
				self.label.x = self.label.x - growth*align*(-1)
				self.line.x = self.line.x - growth*align*(-1)				
			end
		end

		return plate
	end

	function new_ovg:createCountBox()
		local cbox = self:createBox( _Xa*0.8, _Ya*0.4, function() 
			local actualsum = self.ordview:getOrderSum()
			self.itmpanel.label_sum.text = actualsum.._G.Currency
			self.slideview:setTabLabel( self.numslide, actualsum.._G.Currency )
		end )
		
		local title = ""
		local initnumber = 0
		cbox.currsum = self.ordview:getOrderSum()
		cbox.groupprice = 0
		cbox.targetitems = {}
		local oidata = self.ordview.oidata
			if self.trash.count == 0 then
				for i=1, #oidata do
					cbox.groupprice = cbox.groupprice + oidata[i].ordi_price
					cbox.targetitems[#cbox.targetitems+1] = i
				if initnumber == 0 then
					initnumber = oidata[i].ordi_count
				elseif initnumber ~= oidata[i].ordi_count then
					initnumber = 1
				end
				end
				title = translations.OrderTxt_PanelMenu[self.lang][2][1]
				cbox.currsum = cbox.groupprice * initnumber
			else
				for k,v in pairs( self.trash ) do
					if v == true then 
						for i=1, #oidata do
							if oidata[i].id == k then 
								cbox.groupprice = cbox.groupprice + oidata[i].ordi_price
								cbox.targetitems[#cbox.targetitems+1] = i
								cbox.currsum = cbox.currsum - oidata[i].ordi_price*oidata[i].ordi_count
								if initnumber == 0 then
									initnumber = oidata[i].ordi_count
								elseif initnumber ~= oidata[i].ordi_count then
									initnumber = 1
								end
							end
						end
					end
				end
				local j = 0
				for i=1, #cbox.targetitems do
					j = cbox.targetitems[i]
					cbox.currsum = cbox.currsum + oidata[j].ordi_price*initnumber
				end
				title = translations.OrderTxt_PanelMenu[self.lang][2][2]
			end

		self.itmpanel.label_sum.text = cbox.currsum.._G.Currency
		self.slideview:setTabLabel( self.numslide, cbox.currsum.._G.Currency )	    	
		
		local function onStepperPress( event )
				local target = event.target.cbox
				if event.phase == "increment" then
					target.currsum = target.currsum + target.groupprice
						target.count_plate:setText( event.value )
						target.sum_plate:setText( target.groupprice*event.value.._G.Currency )
						self.itmpanel.label_sum.text = target.currsum.._G.Currency
				self.slideview:setTabLabel( self.numslide, target.currsum.._G.Currency )

				elseif event.phase == "decrement" then
						target.currsum = target.currsum - target.groupprice
				target.count_plate:setText( event.value )
						target.sum_plate:setText( target.groupprice*event.value.._G.Currency )
						self.itmpanel.label_sum.text = target.currsum.._G.Currency
				self.slideview:setTabLabel( self.numslide, target.currsum.._G.Currency )		    	
				end
		end

		cbox.name = display.newText({
			parent = cbox,
			width = cbox.bg.width*0.8,
			text = title,
			font = self.font_bold, 
			fontSize = self.fsize_item,
			align = "center"
		})
		cbox.name:setFillColor( 0,0,0, 1 )
		cbox.name.x = cbox.bg.x
		cbox.name.anchorY = 0
		cbox.name.y = cbox.bg.y - cbox.bg.height*0.5 + 10

		cbox.separ1 = display.newLine( cbox, 
			cbox.bg.x - cbox.bg.width*0.5, cbox.name.y + cbox.name.height + 10,  
			cbox.bg.x + cbox.bg.width*0.5, cbox.name.y + cbox.name.height + 10
		)
		cbox.separ1:setStrokeColor( 0,0,0, 0.1 )
		cbox.separ1.strokeWidth = 1.5

		cbox.lbut = widget.newButton({
				label = "OK",
				labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
				fontSize = 13.5,
				font = self.font_bold,
				--properties for a rounded rectangle button...
				shape="roundedRect",
				width = (cbox.bg.width - self.pad_regular*3) * 0.5,
				height = 37,
				cornerRadius = 5,
				fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
				strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
				strokeWidth = 2,
				onRelease = function( event )
					local j = 0
					local item_id = 0
					local count = tonumber( cbox.count_plate.label.text )
					for i=1, #cbox.targetitems do
						j = cbox.targetitems[i]
						item_id = self.ordview.oidata[j].id
						self.ordview:setItemCount( item_id, count )
					end
				local actualsum = self.ordview:getOrderSum()
				self.itmpanel.label_sum.text = actualsum.._G.Currency
				self.slideview:setTabLabel( self.numslide, actualsum.._G.Currency )
					self.mainbtns:setState( self.ordview:getOrderState() )
					local params = self.itmpanel.params
					self.itmpanel:removeSelf()
					self.itmpanel = self:createItemsPanel( unpack( params ) )
				self.trash = {}
					self.trash.count = 0		    	
					cbox:removeSelf()
				cbox = nil
				end	    
		})
		cbox.lbut.anchorX = 0
		cbox.lbut.x = cbox.bg.x - cbox.bg.width*0.5 + self.pad_regular
		cbox.lbut.anchorY = 1
		cbox.lbut.y = cbox.bg.y + cbox.bg.height*0.5 - self.pad_regular
		cbox:insert( cbox.lbut )

		cbox.rbut = widget.newButton({
				label = translations.Cancel[self.lang],
				labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 1 } },
				fontSize = 13.5,
				font = self.font_bold,
				--properties for a rounded rectangle button...
				shape="roundedRect",
				width = (cbox.bg.width - self.pad_regular*3) * 0.5,
				height = 37,
				cornerRadius = 5,
				fillColor = { default={ 244/255, 64/255, 66/255 }, over={ 211/255,211/255,211/255 } },
				strokeColor = { default={ 244/255, 64/255, 66/255 }, over={ 0,0,0, 0.13 } },
				strokeWidth = 2,
				onRelease = function( event )
				local actualsum = self.ordview:getOrderSum()
				self.itmpanel.label_sum.text = actualsum.._G.Currency
				self.slideview:setTabLabel( self.numslide, actualsum.._G.Currency )
					cbox:removeSelf()
				cbox = nil
				end	    
		})
		cbox.rbut.anchorX = 1
		cbox.rbut.x = cbox.bg.x + cbox.bg.width*0.5 - self.pad_regular
		cbox.rbut.anchorY = 1
		cbox.rbut.y = cbox.lbut.y
		cbox:insert( cbox.rbut )

		cbox.sum_label = display.newText({
			text = translations.OrderTxt_Sum[self.lang]..":",
			font = self.font_regular, 
			fontSize = self.fsize_item,
		})
		cbox.sum_label:setFillColor( 0,0,0, 1 )
		cbox.sum_plate = self:createPlate({
			text = cbox.groupprice*initnumber.._G.Currency,
			font = self.font_regular, 
			fsize = self.fsize_item,
			align = "left"
		})
		cbox.sum_plate.anchorX = 0
		cbox.sum_plate.x = cbox.sum_label.width*0.5

		local sum = display.newGroup()
		sum:insert( cbox.sum_label )
		sum:insert( cbox.sum_plate )
		cbox:insert( sum )
		sum.anchorChildren = true
		sum.anchorX = 0
		sum.x = cbox.bg.x - cbox.bg.width*0.5 + self.pad_regular
		sum.anchorY = 1
		sum.y = cbox.lbut.y - cbox.lbut.height - 15

		cbox.separ2 = display.newLine( cbox, 
			cbox.bg.x - cbox.bg.width*0.5 + self.pad_regular, 
			sum.y - sum.height - 15,  
			cbox.bg.x + cbox.bg.width*0.5 - self.pad_regular, 
			sum.y - sum.height - 15
		)
		cbox.separ2:setStrokeColor( 0,0,0, 0.1 )
		cbox.separ2.strokeWidth = 1

		cbox.price_plate = self:createPlate({
			text = cbox.groupprice.._G.Currency,
			font = self.font_title,
			fsize = self.fsize_item,
			pad = 3
		})

		local multipsymb = display.newText({
			text = "x",
			font = self.font_regular, 
			fontSize = self.fsize_item,
		})
		multipsymb:setFillColor( 0,0,0, 1 )
		multipsymb.anchorX = 0
		multipsymb.x = cbox.price_plate.width*0.5 + self.pad_regular
		
		cbox.count_plate = self:createPlate({
			text = initnumber,
			font = self.font_title,
			fsize = self.fsize_item,
			pad = 3
		})
		cbox.count_plate.anchorX = 0
		cbox.count_plate.x = multipsymb.x + multipsymb.width + self.pad_regular

		local options = {
				width = 80,
				height = 41,
				numFrames = 3,
				sheetContentWidth = 240,
				sheetContentHeight = 41
		}
		local stepperSheet = graphics.newImageSheet( "assets/orders/stepper.png", options )

		cbox.stepper = display.newGroup()
		local stepper = widget.newStepper
		{
				id = "count_stepper",
				initialValue = initnumber,
				minimumValue = 1,
				sheet = stepperSheet,
				defaultFrame = 1,
				noMinusFrame = 2,
				noPlusFrame = 3,
				minusActiveFrame = 2,
				plusActiveFrame = 3,		    
			onPress = onStepperPress
		}
		stepper.cbox = cbox
		cbox.stepper:insert( stepper )
		cbox.stepper.width = 78
		cbox.stepper.height = 40 
		cbox.stepper.anchorChildren = true
		cbox.stepper.anchorX = 0
		cbox.stepper.x = cbox.count_plate.x + cbox.count_plate.width + self.pad_regular

		local bounds_separ1 = cbox.separ1.contentBounds
		local bounds_separ2 = cbox.separ2.contentBounds
		local freespace = bounds_separ2.yMin - bounds_separ1.yMax + 5

		local counter = display.newGroup()
		counter:insert( cbox.price_plate )
		counter:insert( multipsymb )
		counter:insert( cbox.count_plate )
		counter:insert( cbox.stepper )
		cbox:insert( counter )
		counter.anchorChildren = true
		counter.x = cbox.bg.x
		counter.y = cbox.separ1.y + freespace*0.5
	end	

	function new_ovg:createCountItemBox( itmline )
		local cibox = self:createBox( _Xa*0.8, _Ya*0.4, function()
			local actualsum = self.ordview:getOrderSum()
			self.itmpanel.label_sum.text = actualsum.._G.Currency
			self.slideview:setTabLabel( self.numslide, actualsum.._G.Currency )
		end )
		cibox.tempsum = self.ordview:getOrderSum()

		local function recalcSum( ammount )
			local ammount=ammount or 0
				local target=cibox
				local price = tonumber( target.price_plate.label.text) --:match( "%d+" )
						target.tempsum =  price*ammount
				--print(price)
						target.sum_plate:setText( price*ammount.._G.Currency )
						self.itmpanel.label_sum.text = target.tempsum.._G.Currency
				self.slideview:setTabLabel( self.numslide, self.ordview:getOrderSum() )		    	
		end

		local function onStepperPress( event )
				local target = event.target.cibox
				local price = tonumber( target.price_plate.label.text:match( "%d+" ))
				if event.phase == "increment" then
					target.tempsum = target.tempsum + price
						target.count_plate:setText( tostring(event.value) )						
						target.sum_plate:setText( price*event.value.._G.Currency )
						self.itmpanel.label_sum.text = target.tempsum.._G.Currency
				self.slideview:setTabLabel( self.numslide, target.tempsum.._G.Currency )

				elseif event.phase == "decrement" then
						target.tempsum = target.tempsum - price
				target.count_plate:setText( tostring(event.value) )
						target.sum_plate:setText( price*event.value.._G.Currency )
						self.itmpanel.label_sum.text = target.tempsum.._G.Currency
				self.slideview:setTabLabel( self.numslide, target.tempsum.._G.Currency )		    	
				elseif  event.phase == "ended" or event.phase == "submitted" then
						target.tempsum =  price*tonumber(event.target.text)
				--target.count_plate:setText( tostring(event.value) )
						target.sum_plate:setText( price*tonumber(event.target.text).._G.Currency )
						self.itmpanel.label_sum.text = target.tempsum.._G.Currency
						self.slideview:setTabLabel( self.numslide, target.tempsum.._G.Currency )		    	

				end
		end

		cibox.name = display.newText({
			parent = cibox,
			width = cibox.bg.width*0.8,
			text = itmline.name.text,
			font = self.font_bold, 
			fontSize = self.fsize_item,
			align = "center"
		})
		cibox.name:setFillColor( 0,0,0, 1 )
		cibox.name.x = cibox.bg.x
		cibox.name.anchorY = 0
		cibox.name.y = cibox.bg.y - cibox.bg.height*0.5 + 10

		cibox.separ1 = display.newLine( cibox, 
			cibox.bg.x - cibox.bg.width*0.5, cibox.name.y + cibox.name.height + 10,  
			cibox.bg.x + cibox.bg.width*0.5, cibox.name.y + cibox.name.height + 10
		)
		cibox.separ1:setStrokeColor( 0,0,0, 0.1 )
		cibox.separ1.strokeWidth = 1.5

		cibox.lbut = widget.newButton({
				label = "OK",
				labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
				fontSize = 13.5,
				font = self.font_bold,
				--properties for a rounded rectangle button...
				shape="roundedRect",
				width = (cibox.bg.width - self.pad_regular*3) * 0.5,
				height = 37,
				cornerRadius = 5,
				fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
				strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
				strokeWidth = 2,
				onRelease = function( event )
					local count = cibox.count_plate.label.text
					if itmline.count.text ~= count then --:match( "%d+" )
						self.ordview:setItemCount( itmline.id, count )
						itmline.count.text = "  x  "..count
						itmline.sum:setText( cibox.sum_plate.label.text:match( "%d+" ).._G.Currency )
					end
					self.slideview:setTabLabel( self.numslide, self.ordview:getOrderSum().._G.Currency )
					cibox:removeSelf()
				cibox = nil
				end	    
		})
		cibox.lbut.anchorX = 0
		cibox.lbut.x = cibox.bg.x - cibox.bg.width*0.5 + self.pad_regular
		cibox.lbut.anchorY = 1
		cibox.lbut.y = cibox.bg.y + cibox.bg.height*0.5 - self.pad_regular
		cibox:insert( cibox.lbut )

		cibox.rbut = widget.newButton({
				label = translations.OrderTxt_DeleteItem[self.lang],
				labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 1 } },
				fontSize = 13.5,
				font = self.font_bold,
				--properties for a rounded rectangle button...
				shape="roundedRect",
				width = (cibox.bg.width - self.pad_regular*3) * 0.5,
				height = 37,
				cornerRadius = 5,
				fillColor = { default={ 244/255, 64/255, 66/255 }, over={ 211/255,211/255,211/255 } },
				strokeColor = { default={ 244/255, 64/255, 66/255 }, over={ 0,0,0, 0.13 } },
				strokeWidth = 2,
				onRelease = function( event )
					self.ordview:deleteItem( itmline.id )
					self.slideview:setTabLabel( self.numslide, self.ordview:getOrderSum().._G.Currency )
					local ypos = self.itmpanel.y
					local height = self.itmpanel.height
					self.itmpanel:removeSelf()
					self.itmpanel = self:createItemsPanel( ypos, height, self.header_height )
					self.mainbtns:setState( self.ordview:getOrderState() )
					cibox:removeSelf()
				cibox = nil
				end	    
		})
		cibox.rbut.anchorX = 1
		cibox.rbut.x = cibox.bg.x + cibox.bg.width*0.5 - self.pad_regular
		cibox.rbut.anchorY = 1
		cibox.rbut.y = cibox.lbut.y
		cibox:insert( cibox.rbut )

		cibox.sum_label = display.newText({
			text = translations.OrderTxt_Sum[self.lang]..":",
			font = self.font_regular, 
			fontSize = self.fsize_item,
		})
		cibox.sum_label:setFillColor( 0,0,0, 1 )
		cibox.sum_plate = self:createPlate({
			text = itmline.sum.label.text,
			font = self.font_regular, 
			fsize = self.fsize_item,
			align = "left"
		})
		cibox.sum_plate.anchorX = 0
		cibox.sum_plate.x = cibox.sum_label.width*0.5

		local sum = display.newGroup()
		sum:insert( cibox.sum_label )
		sum:insert( cibox.sum_plate )
		cibox:insert( sum )
		sum.anchorChildren = true
		sum.anchorX = 0
		sum.x = cibox.bg.x - cibox.bg.width*0.5 + self.pad_regular
		sum.anchorY = 1
		sum.y = cibox.lbut.y - cibox.lbut.height - 15

		cibox.separ2 = display.newLine( cibox, 
			cibox.bg.x - cibox.bg.width*0.5 + self.pad_regular, 
			sum.y - sum.height - 15,  
			cibox.bg.x + cibox.bg.width*0.5 - self.pad_regular, 
			sum.y - sum.height - 15
		)
		cibox.separ2:setStrokeColor( 0,0,0, 0.1 )
		cibox.separ2.strokeWidth = 1

		cibox.price_plate = self:createPlate({
			text = itmline.price.text,
			font = self.font_title,
			fsize = self.fsize_item,
			pad = 3
		})

		local multipsymb = display.newText({
			text = "x",
			font = self.font_regular, 
			fontSize = self.fsize_item,
		})
		multipsymb:setFillColor( 0,0,0, 1 )
		multipsymb.anchorX = 0
		multipsymb.x = cibox.price_plate.width*0.5 + self.pad_regular
		
		cibox.count_plate = self:createPlate({
			text = itmline.count.text:match( "[%d%p]+" ),
			font = self.font_title,
			withEditField = true,
			listener=recalcSum,
			fsize = self.fsize_item,
			pad = 3
		})
		cibox.count_plate.anchorX = 0
		cibox.count_plate.x = multipsymb.x + multipsymb.width + self.pad_regular

		local options = {
				width = 80,
				height = 41,
				numFrames = 3,
				sheetContentWidth = 240,
				sheetContentHeight = 41
		}
		local stepperSheet = graphics.newImageSheet( "assets/orders/stepper.png", options )

		cibox.stepper = display.newGroup()
		local stepper = widget.newStepper
		{
				id = "count_stepper",
				initialValue = tonumber( itmline.count.text:match( "%d+" ) ),
				minimumValue = 1,
				sheet = stepperSheet,
				defaultFrame = 1,
				noMinusFrame = 2,
				noPlusFrame = 3,
				minusActiveFrame = 2,
				plusActiveFrame = 3,		    
			onPress = onStepperPress
		}
		stepper.cibox = cibox
		cibox.stepper:insert( stepper )
		cibox.stepper.width = 78
		cibox.stepper.height = 40 
		cibox.stepper.anchorChildren = true
		cibox.stepper.anchorX = 0
		cibox.stepper.x = cibox.count_plate.x + cibox.count_plate.width + self.pad_regular

		local bounds_separ1 = cibox.separ1.contentBounds
		local bounds_separ2 = cibox.separ2.contentBounds
		local freespace = bounds_separ2.yMin - bounds_separ1.yMax + 5

		local counter = display.newGroup()
		counter:insert( cibox.price_plate )
		counter:insert( multipsymb )
		counter:insert( cibox.count_plate )
		counter:insert( cibox.stepper )
		cibox:insert( counter )
		counter.anchorChildren = true
		counter.x = cibox.bg.x
		counter.y = cibox.separ1.y + freespace*0.5
	end

	function new_ovg:createItemsLines( data, width, height )
		local lref, rref, tref, bref
		local distance = 7
		local ypos = 0
		local ilgroup = display.newGroup()
		local line, price, count, amount

		local function onSwitchPress( event )
				local switch = event.target
				local id = event.target.id
				if switch.isOn then 
					self.trash[id] = true
					self.trash.count = self.trash.count + 1 
				else self.trash[id] = nil
					self.trash.count = self.trash.count - 1 
				end
		end

		for i=1, #data do
			line = display.newGroup( )
			line.anchorChildren = true
			line.anchorY = 0
			line.id = data[i].id

			line.bg = display.newRect( 0, 0, width, height )	
			line.bg:setFillColor( 1,1,1 )
			line:insert( line.bg )
			lref = -line.bg.width*0.5
			rref = line.bg.width*0.5
			tref = -line.bg.height*0.5
			bref = line.bg.height*0.5

			local options = {
					width = 31,
					height = 30,
					numFrames = 2,
					sheetContentWidth = 62,
					sheetContentHeight = 30
			}
			local radioButtonSheet = graphics.newImageSheet( "assets/orders/itemCheckbox.png", options )
			
			local checkbox = widget.newSwitch
			{
					id = data[i].id,
					x = 0,
					y = 0,
					style = "checkbox",
					onPress = onSwitchPress,
					sheet = radioButtonSheet,
					frameOff = 1,
					frameOn = 2
			}
			line.checkbox = display.newGroup()
			line.checkbox:insert( checkbox )
			line:insert( line.checkbox )
			line.checkbox.width = 22.5
			line.checkbox.height = 22
			line.checkbox.anchorChildren = true
			line.checkbox.anchorX = 0
			line.checkbox.x = lref

			line.tappad = display.newRect( 0, 0, width-line.checkbox.width, height )	
			line.tappad:setFillColor( 1,1,1 )
			line.tappad.anchorX = 0
			line.tappad.x = lref + line.checkbox.width
			line:insert( line.tappad )

			line.tappad.id = data[i].id
			line.tappad:addEventListener( "touch", function( event )
				local tapedline = event.target.parent
				if event.phase == "began" then
					display.getCurrentStage():setFocus( event.target )
				elseif event.phase == "moved" then
					display.getCurrentStage():setFocus( nil )
					return false
				elseif event.phase == "ended" or event.phase == "cancelled" then
					self:createCountItemBox( tapedline )
					display.getCurrentStage():setFocus( nil )
				end
				return true
			end )
			
			line.sum = self:createPlate({ 	
					text = data[i].ordi_sum.._G.Currency,     
					font = self.font_title,   
					fsize = self.fsize_tap,
					color = { 0,0,0, 0.8 },
				pad = 3,
				align = "right"			
			})
			line.sum.anchorX = 1
			line.sum.x = rref
			line:insert( line.sum )

			local itminfo = display.newGroup()
			itminfo.anchorChildren = true
			itminfo.anchorX = 0
			itminfo.x = line.tappad.x
			line:insert( itminfo )	
		--	printResponse(data)		
			data[i].mi_name = data[i].mi_name or ""
			local f_size=self.fsize_item
			if data[i].ordi_base_gi_id and data[i].ordi_base_gi_id>0 then
				f_size=f_size-3
			end
			line.name = display.newText({
				parent = itminfo,
				width = line.tappad.width-line.sum.width - 5,
				text = data[i].mi_name,
				font = self.font_title, 
				fontSize = f_size,
			})
			if data[i].ordi_base_gi_id and data[i].ordi_base_gi_id>0 then
				line.name:setFillColor( 0.4 )
			else
				line.name:setFillColor( 0,0,0, 1 )
			end			
			
			line.name.anchorX = 0

			line.price = display.newText({
				parent = itminfo,
				text = data[i].ordi_price.._G.Currency,
				font = self.font_regular, 
				fontSize = self.fsize_regular,
			})
			line.price:setFillColor( 0,0,0 )
			line.price.anchorX = 0
			line.price.anchorY = 0
			line.price.y = line.name.height*0.5

			line.count = display.newText({
				parent = itminfo,
				text = "  x  "..data[i].ordi_count,
				font = self.font_regular, 
				fontSize = self.fsize_regular,
			})
			line.count:setFillColor( 0,0,0 )
			line.count.anchorX = 0
			line.count.x = line.price.width
			line.count.anchorY = 0
			line.count.y = line.price.y

			if itminfo.height > height then
			line.bg.height = itminfo.height
			end

			line.y = ypos
			ilgroup:insert( line )
			ypos = ypos + line.height + distance
		end

		return ilgroup
	end

	function new_ovg:createItemsPanelMenu( ypos, width, height )
		local ipmenu = display.newGroup()
		ipmenu.anchorChildren = true

		local function showIPMenu()
					local options = {
							time = 300,
							transition = easing.outExpo,
							delta = true,
							x = -ipmenu.width + 10,
							onCancel = function( event )
									--showIPMenu( time )
							end
					}
					transition.to( ipmenu, options )			
		end

		local function hideIPMenu( event )
					local options = {
							time = 300,
							transition = easing.outExpo,
							delta = true,
							x = ipmenu.width - 10,
							onComplete = function( event )
								if ipmenu then
									ipmenu:removeSelf()
									ipmenu.substrate:removeSelf()
									ipmenu.substrate = nil
								end
					ipmenu = nil
							end,
							onCancel = function( event )
								 
							end
					}
					transition.to( ipmenu, options )
		end
		
		ipmenu.substrate = display.newRect( _mX, _mY, _Xa, _Ya )
		ipmenu.substrate.isVisible = false
		ipmenu.substrate.isHitTestable = true
		ipmenu.substrate:addEventListener( "touch", function( event )
			if event.phase == "ended" or event.phase == "cancelled" then
				hideIPMenu()
				display.getCurrentStage():setFocus( nil )
			end
			return true
		end )

		ipmenu.bg = display.newRoundedRect( 0, 0, width, height, 10 )
		ipmenu.bg:setFillColor( 1,1,1 )
		ipmenu.bg:setStrokeColor( 0,0,0, 0.13 )
		ipmenu.bg.strokeWidth = 2
		ipmenu:insert( ipmenu.bg )
		ipmenu.bg:addEventListener( "touch", function( event )
			return true
		end )

		local top = -ipmenu.bg.height*0.5
		local left = -ipmenu.bg.width*0.5

		-- local patch = display.newRect( self.rightref, ypos, width, 10 )
		-- patch:setFillColor( 1,1,0 )
		-- ipmenu:insert( patch )

		local translat = translations.OrderTxt_PanelMenu[self.lang]
		ipmenu.delbutn = display.newText({
				parent = ipmenu,
				text = translat[1][1],
				x = left + self.pad_regular, 
				y = top + self.pad_regular, 
				width = width*0.9-10,
				--height = 14*2+3,
				fontSize = self.fsize_item,
				font = self.font_title,
				align = "left"
		})
		if self.trash.count == 0 then
			ipmenu.delbutn.text = translat[1][1]  
		else ipmenu.delbutn.text = translat[1][2].." ("..self.trash.count..")" 
		end
		ipmenu.delbutn.anchorX = 0
		ipmenu.delbutn.anchorY = 0
		ipmenu.delbutn:setFillColor( 0, 0, 0 )    
		ipmenu.delbutn:addEventListener( "touch", function( event )
				if event.phase == "ended" or event.phase == "cancelled" then
					if self.trash.count == 0 then
						for i=1, #self.ordview.oidata do
							if self.ordview.oidata and self.ordview.oidata[1] then
								self.ordview:deleteItem( self.ordview.oidata[1].id )
							end
						end
					else
						for k,v in pairs( self.trash ) do
							if v == true then 
								self.ordview:deleteItem( k )
							end
						end
						self.trash = {}
						self.trash.count = 0
					end
					self.mainbtns:setState( self.ordview:getOrderState() )
					local params = self.itmpanel.params
					self.itmpanel:removeSelf()
					self.itmpanel = self:createItemsPanel( unpack( params ) )
					self.slideview:setTabLabel( self.numslide, self.ordview:getOrderSum() )	
					hideIPMenu()
				end
				return true		    	
			end )

			local sepr1 = display.newRect( ipmenu, 0, 0, width, 1)
			sepr1:setFillColor( 0,0,0, 0.1 )
			sepr1.anchorX = 0
			sepr1.x = left
			sepr1.y = ipmenu.delbutn.y + ipmenu.delbutn.height + self.pad_regular

		ipmenu.countbutn = display.newText({
				parent = ipmenu,
				text = translat[2][1],
				x = left + self.pad_regular, 
				y = sepr1.y + self.pad_regular, 
				width = width*0.9-10,
				--height = 14*2+3,
				fontSize = self.fsize_item,
				font = self.font_title,
				align = "left"
		})
		if self.trash.count == 0 then
			ipmenu.countbutn.text = translat[2][1]  
		else ipmenu.countbutn.text = translat[2][2].." ("..self.trash.count..")" 
		end
		ipmenu.countbutn.anchorX = 0
		ipmenu.countbutn.anchorY = 0
		ipmenu.countbutn:setFillColor( 0, 0, 0 )
		ipmenu.countbutn:addEventListener( "touch", function( event )
				if event.phase == "ended" or event.phase == "cancelled" then
					hideIPMenu()
					self:createCountBox()
				end
				return true		    	
			end )

			local sepr2 = display.newRect( ipmenu, 0, 0, width, 1 )
			sepr2:setFillColor( 0,0,0, 0.1 )
			sepr2.anchorX = 0
			sepr2.x = left
			sepr2.y = ipmenu.countbutn.y + ipmenu.countbutn.height + self.pad_regular 	    
		
		ipmenu.anchorX = 0
		ipmenu.x = _mX + _Xa*0.5
		ipmenu.anchorY = 0
		ipmenu.y = ypos
		ipmenu:toFront()
			showIPMenu()
	end

	function new_ovg:createItemsPanel( ypos, height, header_height, bygroup  )
		local parent = self
		local function listnOnStart( panel )
			if panel.completeState == "hidden" then
				panel.bplus.isVisible = false
				panel.bminus.isVisible = true
				if self.itmpanel.menubutn then
					self.itmpanel.menubutn.isVisible = true
				end
				self:panelsController( panel, "show" )
			elseif panel.completeState == "shown" then
				panel.bplus.isVisible = true
				panel.bminus.isVisible = false
				if self.itmpanel.menubutn then 
					self.itmpanel.menubutn.isVisible = false
				end
			end
		end
		local function listnOnComplete( panel )
			if panel.completeState == "hidden" then
				self:panelsController( panel, "hide" )
			end
		end
		
		local options = { 
				-- general
				id = "itm",
				width = _Xa,
				height = height,
				speed = 250,
				inEasing = easing.outCubic,
				outEasing = easing.outCubic,
				isScrollable = true,
				topScrollPad = 5,
				bottomScrollPad = 5,		    
				onPress = function( event )
					self.panelInFocus = event.target
				end,
				onStart = listnOnStart,
				onComplete = listnOnComplete,
				--header
				headerHeight = header_height,
				headerColor = { 1,1,1, 1 }
		}
		local slpanel = widget.newSlidingPanel( options )
		slpanel.anchorChildren = true
		slpanel.x = 0
		slpanel.anchorY = 0
		slpanel.y = ypos
		slpanel.up_pos = ypos
		slpanel.down_pos = slpanel.up_pos + slpanel.openHeight
		slpanel.params = { ypos, height, header_height, bygroup }

		-- header ------------------------------------------------------------

		local options = {
				width = 30,
				height = 30,
				numFrames = 2,
				sheetContentWidth = 60,
				sheetContentHeight = 30
		}
		local pbsheet =graphics.newImageSheet( "assets/orders/panelButton.png", options )
		slpanel.bplus = display.newImageRect( pbsheet, 1, 22, 22 )
		slpanel.bplus.anchorX = 0
		slpanel.bplus.x = slpanel.header.left + self.pad_regular
		slpanel.bplus.y = 0
		slpanel.bplus.isVisible = false
		slpanel.header:insert( slpanel.bplus )
		slpanel.bminus = display.newImageRect( pbsheet, 2, 22, 22 )
		slpanel.bminus.anchorX = 0
		slpanel.bminus.x = slpanel.header.left + self.pad_regular
		slpanel.bminus.y = 0
		slpanel.header:insert( slpanel.bminus )
				
		local label =  display.newText( 
			translations.OrderTxt_Items[self.lang],
			0,0,
			self.font_title, self.fsize_title 
		)
		label:setFillColor( 0,0,0 )
		label.y = 0
		label.anchorX = 0
		label.x = slpanel.bplus.x + slpanel.bplus.width + 10
		slpanel.header:insert( label )

		local gsum = display.newGroup()
		gsum.anchorChildren = true
		gsum.anchorX = 0
		gsum.x = label.x + label.width + 10
		gsum.y = 0
		slpanel.header:insert( gsum )

		slpanel.label_sum = display.newText( 	
			"0",
			0,-0.5,
			self.font_title, self.fsize_regular-3
		)
		slpanel.label_sum.text = ordview:getOrderSum()
		local cur=""
		if userProfile.SelectedRestaurantData.cur_short then
			cur=" "..userProfile.SelectedRestaurantData.cur_short
		end		
		slpanel.label_sum.text=slpanel.label_sum.text..cur
		if new_ovg.ordview.odata.ord_discount_sum~=nil then
			slpanel.label_sum.text=slpanel.label_sum.text.."  ("..translations["incl_discount"][self.lang]..": "..new_ovg.ordview.odata.ord_discount_sum..")"
		end
		slpanel.label_sum:setFillColor( 1,1,1 )
		gsum:insert( slpanel.label_sum )

		local w = self:getTextWidth( slpanel.label_sum.text, self.font_title, self.fsize_regular-3 )
		local bg_sum = display.newRoundedRect( gsum, 0, 0, w+5, slpanel.bplus.height*0.7, 3 )
		bg_sum:setFillColor( 246/255, 133/255, 49/255 )
		bg_sum:toBack()
		
		function slpanel:createMenuButn()
			if #parent.ordview.oidata == 0 then return end
			
			self.menubutn = display.newGroup()
			self.menubutn.anchorChildren = true
			self.menubutn.anchorX = 1
			self.menubutn.x = self.header.right
			self.menubutn.y = 0
			self.header:insert( self.menubutn )
			local mb_face = display.newImageRect( 
				"assets/orders/panelMenu.png", 
				bg_sum.height*1.01, bg_sum.height 
			)
			self.menubutn:insert( mb_face )
			local mb_lining = display.newRect( 0, 0, 
				mb_face.width+parent.pad_regular*2, mb_face.height+5 
			)
			self.menubutn:insert( mb_lining )
			mb_lining:toBack()
			
			self.menubutn:addEventListener( "touch", function( event )
				if event.phase == "moved" then
					return false
				elseif event.phase == "ended" or event.phase == "cancelled" then
					if not self.isavailable or #parent.ordview.oidata == 0 then 
						return true 
					end
					local dispobj = parent.itmpanel.header
					local y = dispobj.contentBounds.yMax
					parent:createItemsPanelMenu( y, _Xa*0.5, self.openHeight )
				end
				return true
			end )
		end

		-- body ---------------------------------------------------------------------------

		local bg = display.newRect( 0, 0, _Xa, slpanel.openHeight )
		bg:setFillColor( 232/255,232/255,232/255, 0 )
		--bg:setStrokeColor( 105/255,105/255,105/255 )
		--bg.strokeWidth = 2
		slpanel:place( bg )

		local distance = 10
		local gtitle = {}
		local itmlines = {} 
		local nextpos = 0
		
		if bygroup then
			for group, items in pairs( self.ordview:sortItems() ) do
				gtitle = display.newText(
					slpanel.content,
					group,
					0, 0,
					native.systemFontBold, 12 
				)
				gtitle:setFillColor( 0,0,0 )
				gtitle:setStrokeColor( 0,0,0 )
				gtitle.strokeWidth = 2
				gtitle.x = - slpanel.width*0.5 + gtitle.width*0.5 + 10
				if nextpos == 0 then
					gtitle.y = - slpanel.height*0.5 + slpanel.header.height - gtitle.height*0.5 + 10
				else gtitle.y = nextpos
				end

				itmlines = createItemsLines( self, items )
				itmlines.x = - slpanel.width*0.5 + 20
				itmlines.y = gtitle.y + distance
				slpanel.content:insert( itmlines )

				nextpos = gtitle.y + itmlines.height + distance
			end
		elseif #self.ordview.oidata > 0 then
			local itml_width = _Xa - self.pad_regular*2 - slpanel.bplus.width*0.5
			-- printResponse(self.ordview.oidata)
			-- print("@@@@@@@@@")
			itmlines = self:createItemsLines( self.ordview.oidata, itml_width, 35 )
			slpanel:place( itmlines, { top = 5 } ) 
		end

		function slpanel:createAddButn()
			local options = {
					width = 120,
					height = 121,
					numFrames = 2,
					sheetContentWidth = 240,
					sheetContentHeight = 121
			}
			local buttonSheet =graphics.newImageSheet( "assets/orders/addButton.png", options )
		
			self.addbutn = display.newGroup()
			self.addbutn.default = display.newImage( self.addbutn, buttonSheet, 1 )
			self.addbutn.over = display.newImage( self.addbutn, buttonSheet, 2 )
			self.addbutn.over.isVisible = false
			self:place( self.addbutn, { top = (itmlines.height or 0) + 20 } )
			
			self.addbutn:addEventListener( "touch", function( event )
				if event.phase == "began" then
					event.target.over.isVisible = true
					event.target.isFocus = true
					display.getCurrentStage():setFocus( event.target )
				elseif event.phase == "moved" then
					local delta = math.abs( event.yStart - event.y )
					if event.target.isFocus and delta > 3 then
						event.target.over.isVisible = false
						event.target.isFocus = false
						display.getCurrentStage():setFocus( nil )
						return false					
					end
				elseif event.phase == "ended" or event.phase == "cancelled" then
					if event.target.isFocus then
						event.target.over.isVisible = false
						event.target.isFocus = false
						display.getCurrentStage():setFocus( nil )
						composer.setVariable( "came_from", "order" )
						mySidebar:press("menu")
					end	
				end
				return true
			end )
		end		

		function slpanel:setState( ordstate )
			--print("FFFFFFFFFFFFF"..ordstate)
			if ordstate == 0  then --or ordstate == 4 or ordstate == 5  or or ordstate == 6
				self.isavailable = true
				self:createMenuButn()
				self:createAddButn()
				if self.overlay then
					self.overlay:removeSelf()
					self.overlay = nil
				end
			else
				if self.menubutn then
					self.menubutn:removeSelf()
					self.menubutn = nil
				end
				if self.addbutn then
					self.addbutn:removeSelf()
					self.addbutn = nil
				end
				self.isavailable = false

				if self.overlay then return end
				self.overlay = display.newRect( 0, 0, self.width, self.content.height )
				self.overlay:setFillColor( 1,1,1, 0.4 )
				self:place( self.overlay )
				self.overlay:addEventListener( "touch", function( event )
					return true
				end )
			end
		end
		slpanel:setState( self.ordview:getOrderState() )
		
		self:insert( slpanel )
		return slpanel
	end

	function new_ovg:createMessageAlert( time )
		if self.ordview:getOrderState() < 2 then return end
		local parent = self

		local currmess = self.ordview:getRestMess( true )
		local prevmess = self.ordview:getRestMess( false )
		if not currmess then return end
		if prevmess and prevmess == currmess then return end

		local messalert = display.newImageRect( "assets/orders/messIcon.png", 17, 15 )
		local function fadeOutIn( obj, time )
			transition.to( obj, { 
				time=time, 
				alpha=0, 
				onComplete = function( obj )
					transition.to( obj, { 
						time=time, 
						alpha=1, 
						onComplete = function( obj )
							fadeOutIn( obj, time )
						end
					})
				end,
				onCancel = function( obj )
				end
			})
		end
		fadeOutIn( messalert, time )

		function messalert:delete()
			parent.ordview:setRestMessIsRead()
			transition.cancel( self )
			if self.removeSelf then self:removeSelf() end
			self = nil
		end

		return messalert
	end

	function new_ovg:createInfoPanel( ypos, height, header_height )
		-- print("new_ovg:createInfoPanel",self.cid,userProfile.activeOrderID)
		local function listnOnStart( panel )
			if panel.completeState == "hidden" then
				panel.bplus.isVisible = false
				panel.bminus.isVisible = true
				if panel.messalert then panel.messalert:delete() end
				self:panelsController( panel, "show" )
			elseif panel.completeState == "shown" then
				panel.bplus.isVisible = true
				panel.bminus.isVisible = false
			end
		end
		local function listnOnComplete( panel )
			if panel.completeState == "hidden" then
				self:panelsController( panel, "hide" )
			end
		end

		local options = { 
				-- general
				id = "info",
				width = _Xa,
				height = height,
				speed = 250,
				inEasing = easing.outCubic,
				outEasing = easing.outCubic,
				isScrollable = true,
				topScrollPad = 5,
				bottomScrollPad = 5,
				onPress = function( event )
					self.panelInFocus = event.target
				end,
				onStart = listnOnStart,
				onComplete = listnOnComplete,
				--header
				headerHeight = header_height,
				headerColor = { 1,1,1, 1 }
		}
		local slpanel = widget.newSlidingPanel( options )
		slpanel.anchorChildren = true
		slpanel.x = 0
		slpanel.anchorY = 0
		slpanel.y = ypos
		self:insert( slpanel )

		-- header ------------------------------------------------------------

		local options = {
				width = 30,
				height = 30,
				numFrames = 2,
				sheetContentWidth = 60,
				sheetContentHeight = 30
		}
		local pbsheet =graphics.newImageSheet( "assets/orders/panelButton.png", options )
		slpanel.bplus = display.newImageRect( pbsheet, 1, 22, 22 )
		slpanel.bplus.anchorX = 0
		slpanel.bplus.x = slpanel.header.left + self.pad_regular
		slpanel.bplus.y = 0
		slpanel.header:insert( slpanel.bplus )
		slpanel.bminus = display.newImageRect( pbsheet, 2, 22, 22 )
		slpanel.bminus.anchorX = 0
		slpanel.bminus.x = slpanel.header.left + self.pad_regular
		slpanel.bminus.y = 0
		slpanel.header:insert( slpanel.bminus )
		slpanel.bminus.isVisible = false

		local ordnum = " - "
		-- print("self.cid",self.cid,ordstate)
		if currstate >0 then ordnum = self.cid or ordview.cid or " - " end
		slpanel.label_order = display.newText( 	
			translations.OrderN[self.lang]..ordnum,
			0,0, 
			self.font_title, self.fsize_regular
		)
		slpanel.label_order:setFillColor( 0,0,0 )
		slpanel.label_order.anchorX = 0
		slpanel.label_order.x = slpanel.bplus.x + slpanel.bplus.width + 10
		slpanel.label_order.y = 0
		slpanel.header:insert( slpanel.label_order )

		function slpanel:createStateLabel()
			local parent = self.parent
			if self.label_state then self.label_state:removeSelf() end

			self.label_state = display.newGroup()
			self.label_state.anchorChildren = true
			self.label_state.anchorX = 0
			self.label_state.x = slpanel.label_order.x + slpanel.label_order.width + 10
			self.label_state.y = 0
			self.header:insert( self.label_state )

			local label = display.newText( 	
				"State",
				0,-0.5,
				parent.font_title, parent.fsize_regular-3
			)
			local ordstate = ordview:getOrderState()
			label.text = parent.statemess[ordstate]
			label:setFillColor( 1,1,1 )
			self.label_state:insert( label )

			local w = parent:getTextWidth( label.text, parent.font_regular, parent.fsize_regular-3 )
			local bg_state = display.newRoundedRect( self.label_state, 0, 0, w+5, self.bplus.height*0.7, 3 )
			if ordstate==7 then --refused order
				bg_state:setFillColor( 244/255, 64/255, 66/255 )
			else
				bg_state:setFillColor( 246/255, 133/255, 49/255 )
			end
			bg_state:toBack()
		end
		slpanel:createStateLabel()

		slpanel.messalert = self:createMessageAlert( 1500 )
		if slpanel.messalert then
			slpanel.messalert.anchorX = 0
			slpanel.messalert.x = slpanel.label_state.x + slpanel.label_state.width + 10
			slpanel.messalert.y = 0
			slpanel.header:insert( slpanel.messalert )
		end

		-- body -----------------------------------------------------------------------
		
		local left_pad = self.pad_regular+slpanel.bplus.width*0.5
		local top_pad = 10
		local next_row = 0
		local dates = self.ordview:getOrderDates( "%d.%m.%Y   %H:%M" )
		local translat = translations.OrderTxt_InfoTitles[self.lang]

		local label_datecreate = display.newText( 	
			translat[1]..":",
			0,0,
			self.font_regular, self.fsize_regular
		)
		label_datecreate:setFillColor( 0,0,0 )
		label_datecreate.anchorX = 0
		label_datecreate.anchorY = 0
		slpanel:place( label_datecreate, { top = top_pad, left = left_pad }  )

		local datecreate_group = display.newGroup()
		datecreate_group.anchorChildren = true
		datecreate_group.anchorX = 0
		slpanel:place( datecreate_group, { top = top_pad, left = left_pad+label_datecreate.width } )
		local dclabel = display.newText({ 	
				parent = datecreate_group,
				text = dates.create,     
				x = 0,
				y = 0,
				width = 150,     
				font = self.font_regular,   
				fontSize = self.fsize_regular,
				align = "center"
		}) 
		dclabel:setFillColor( 0,0,0, 1 )
		local  dclline = display.newRect( datecreate_group, 0, 0, 150, 1 )
		dclline:setFillColor( 0,0,0, 0.1 )
		dclline.anchorY = 1
		dclline.y = dclabel.height*0.5

		next_row = top_pad + label_datecreate.height + 10

		local label_dateclosed = display.newText( 	
			translat[2]..":",
			0,0,
			self.font_regular, self.fsize_regular
		)
		label_dateclosed:setFillColor( 0,0,0 )
		label_dateclosed.anchorX = 0
		label_dateclosed.anchorY = 0
		slpanel:place( label_dateclosed, { 
			top = next_row , left = left_pad 
		})

		local dateclosed_group = display.newGroup()
		dateclosed_group.anchorChildren = true
		dateclosed_group.anchorX = 0
		slpanel:place( dateclosed_group, { 
			top = top_pad + label_datecreate.height + 10, 
			left = left_pad+label_dateclosed.width 
		} )
		dates.closed = dates.closed and dates.closed or "-"
		slpanel.dclolabel = display.newText({ 	
				parent = dateclosed_group,
				text = dates.closed,     
				x = 0,
				y = 0,
				width = 150,     
				font = self.font_regular,   
				fontSize = self.fsize_regular,
				align = "center"
		}) 
		slpanel.dclolabel:setFillColor( 0,0,0, 1 )
		local  dcloline = display.newRect( dateclosed_group, 0, 0, 150, 1 )
		dcloline:setFillColor( 0,0,0, 0.1 )
		dcloline.anchorY = 1
		dcloline.y = slpanel.dclolabel.height*0.5

		next_row = next_row + label_dateclosed.height + 10		

		local label_waiter = display.newText( 	
			translat[3]..":",
			0,0,
			self.font_regular, self.fsize_regular
		)
		label_waiter:setFillColor( 0,0,0 )
		label_waiter.anchorX = 0
		label_waiter.anchorY = 0
		slpanel:place( label_waiter, { top = next_row, left = left_pad } )

		local waiter_name_group = display.newGroup()
		waiter_name_group.anchorChildren = true
		waiter_name_group.anchorX = 0
		slpanel:place( waiter_name_group, 
			{ 
				top = top_pad + label_datecreate.height + 10 + label_dateclosed.height + 10, 
				left = left_pad+label_waiter.width 
			} 
		)
		local name_waiter = " - "
		-- print("new_ovg.ordview.odata.user_name",new_ovg.ordview.odata.user_name,currstate)
		if currstate > 1 then name_waiter = new_ovg.ordview.odata.user_name or "No user"end
		local nwlabel = display.newText({ 	
				parent = waiter_name_group,
				text = name_waiter,     
				x = 0,
				y = 0,
				width = 150,     
				font = self.font_regular,   
				fontSize = self.fsize_regular,
				align = "center"
		}) 
		nwlabel:setFillColor( 0,0,0, 1 )
		local  mwlline = display.newRect( waiter_name_group, 0, 0, 150, 1 )
		mwlline:setFillColor( 0,0,0, 0.1 )
		mwlline.anchorY = 1
		mwlline.y = nwlabel.height*0.5

		next_row = next_row + label_waiter.height + 10		

		local message_group = display.newGroup()
		message_group.anchorChildren = true
		message_group.anchorX = 0
		slpanel:place( message_group, { top = next_row, left = left_pad } )

		local label_message = display.newText( 	
			translat[4]..":",
			0,0,
			self.font_regular, self.fsize_regular
		)
		label_message:setFillColor( 0,0,0 )
		label_message.anchorX = 0
		label_message.anchorY = 0
		message_group:insert( label_message )

		next_row = next_row + label_message.height + 10

		local text_message = display.newText({	
			text = self.ordview:getRestMess( true ) or "no messages",
			x = 0, y = 0,
			width = _Xa - left_pad*2 - 15,
			font = self.font_regular, 
			fontSize = self.fsize_regular-1, 
			align = "left"			
		})
		text_message:setFillColor( 0,0,0 )
		
		local freespace = slpanel.openHeight - next_row - 10
		local boxheight = text_message.height + 10
		boxheight = boxheight < freespace and freespace or boxheight
		boxheight = boxheight < 70 and 70 or boxheight
		local box_message = display.newRoundedRect( 0, 0, 
			_Xa - left_pad*2, boxheight, 10
		)
		box_message:setFillColor( 1,1,1 )
		box_message:setStrokeColor( 0,0,0, 0.1 )
		box_message.strokeWidth = 1
		
		box_message.anchorX = 0
		box_message.anchorY = 0
		box_message.y = label_message.height + 10
		message_group:insert( box_message )

		text_message.anchorX = 0
		text_message.x = 13
		text_message.anchorY = 0
		text_message.y = label_message.height + 10 + 10
		message_group:insert( text_message )
		
		function slpanel:setState( ordstate )
			local parent = self.parent
			local translat = translations.OrderN[parent.lang]
			if ordstate > 0 then 
				self.label_order.text = translat..parent.ordview.cid 
			end
			if ordstate == 3 then
				local ord_dates = self.parent.ordview:getOrderDates( "%d.%m.%Y   %H:%M" )
				self.dclolabel.text = ord_dates.closed
			end			
			self:createStateLabel()
		end

		slpanel:hide( 0 )
		return	slpanel	
	end

	function new_ovg:createPanels( header_height )
		local switch_height = 70
		local top = self.topref + switch_height
		local freespace = self.bg.height - switch_height - self.mainbtns.height - 30
		if self.tips then freespace = freespace - self.tips.height end
		local ypos = top + 7
		local panel_height = freespace - header_height*2 + 5
		if self.ordview:type() == 0 then
			panel_height = panel_height + header_height
		end

		if self.rspanel then self.rspanel:removeSelf() end
		if self.itmpanel then self.itmpanel:removeSelf() end
		if self.infopanel then self.infopanel:removeSelf() end
		if self.delivpanel then self.delivpanel:removeSelf() end

		self.infopanel = self:createInfoPanel( ypos, panel_height, header_height )
		ypos = ypos + header_height + 7 
		
		self.rspanel = self:createReservPanel( ypos, panel_height, header_height )
		if self.rspanel then ypos = ypos + header_height + 7
		end

		self.delivpanel = self:createDeliveryPanel( ypos, panel_height, header_height )
		if self.delivpanel then ypos = ypos + header_height + 6
			print("self.delivpanel")
			-- self.delivpanel:show()
		end
		
		self.itmpanel = self:createItemsPanel( ypos, panel_height, header_height )
	end

	function new_ovg:createTipsButtons( ypos )
		local ordstate = self.ordview:getOrderState()
		-- print("VVVVVVVVVVVVVVVVVV",self.ordview:type(),ordstate)
		if self.ordview:type() == 1 
		and (ordstate == 0 or ordstate == 1 or ordstate == 2 or ordstate == 4 or ordstate == 7) then
		-- print("1")
			return
		end
		if self.ordview:type() == 0 
		and (ordstate == 0 or ordstate == 1 or ordstate == 7) then
		-- print("2")
			return
		end
		if self.ordview:type() == 2 and self.ordview:getOrderSum() == 0 then
			-- print("3")
			return
		end
		-- if rest_data.pos_bonusme_merchant_id~=nil and rest_data.pos_bonusme_merchant_id~="" then		
		-- end
		-- print("TIPS SHOING")
		local tips = display.newGroup()
		tips.anchorChildren = true
		tips.x = 0
		tips.anchorY = 1
		tips.y = ypos
		self:insert( tips )

		local ordsum = self.ordview:getOrderSum()
		local currtipsperc = self.ordview:getTipsPerc()
		if currtipsperc == 0 and ordstate ~= 3 then 
			currtipsperc = 0
			self.ordview:setTipsPerc( currtipsperc ) 
		end 

		local height = 75
		if  ordstate ~= 2 and ordstate ~= 6 then height = height - 40 end
		tips.bg = display.newRect( tips, 0, 0, _Xa, height )
		tips.bg:setFillColor( 0,0,0, 0 )
		local lref = -tips.bg.width*0.5
		local rref = tips.bg.width*0.5
		local tref = -tips.bg.height*0.5
		local bref = tips.bg.height*0.5

		local options = {
				-- frames = 
				-- {
				--     { x=0, y=0, width=20, height=40 },
				--     { x=20, y=0, width=20, height=40 },
				--     { x=40, y=0, width=20, height=40 },
				--     { x=60, y=0, width=20, height=40 },
				--     { x=80, y=0, width=3, height=40 },
				-- },		    
				width = 20,
				height = 40,
				numFrames = 4,
				sheetContentWidth = 83,
				sheetContentHeight = 40
		}
		local segmentSheet = graphics.newImageSheet( "assets/orders/segmentTips.png", options )	
		
		local default = math.round(currtipsperc*100)
		-- print("defaultdefaultdefaultdefault",default)
		if default==0 then
			default=1
		elseif  default==5 then
			default=2
		elseif  default==7 then
			default=3
		elseif  default==10 then
			default=4
		elseif  default==15 then
			default=5
		else
			default=1
		end
		-- print("defaultdefaultdefaultdefault 2",default)
		-- print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2",userProfile.SelectedRestaurantData.pos_type)
		if (ordstate == 2 or ordstate == 5 or ordstate == 6) and userProfile.SelectedRestaurantData.pos_type==1 then
			tips.butns = widget.newSegmentedControl({
					x = 0,
					y = bref,
					segmentWidth = math.ceil( _Xa/5 ),
					segments = { "0%", "5%", "7%", "10%", "15%" },
					defaultSegment = default,
					labelSize = 16,
					labelColor = 
					{
							default = { 0, 0, 0, 1 },
							over = { 1, 1, 1, 1 },
					},			    
					sheet = segmentSheet,
				leftSegmentFrame = 4,
					middleSegmentFrame = 4,
					rightSegmentFrame = 4,
					leftSegmentSelectedFrame = 1,
					middleSegmentSelectedFrame = 1,
					rightSegmentSelectedFrame = 1,
				-- segmentFrameWidth = 40,
		-- 			segmentFrameHeight = 20,
				-- dividerFrame = 5,
			 --    dividerFrameWidth = 4,
			 --    dividerFrameHeight = 40,    						    
					onPress = function( event )
						local target = event.target
						local num = target.segmentNumber
						local perctip 
						if num==1 then
							perctip  = 0
						elseif num==2 then
							perctip  = 0.05
						elseif num==3 then
							perctip  = 0.07
						elseif num==4 then
							perctip  = 0.1
						elseif num==5 then
							perctip  = 0.15
						else
							perctip  =0
						end
						local sumtip = self.ordview:getOrderSum()*perctip
						if tips.sumlabel then tips.sumlabel:setText( sumtip.._G.Currency ) end
						self.ordview:setTipsPerc( perctip )
						local topay = self.ordview:getOrderSum() + sumtip
						if self.mainbtns and self.mainbtns.leftbutn then
							local translat = translations.OrderTxt_MainButtons[self.lang]
							-- if userProfile.SelectedRestaurantData.use_bonusme_proccessing==1 or userProfile.SelectedRestaurantData.use_bonusme_proccessing==2 then								
								self.mainbtns.leftbutn:setLabel( translat[1][5].." "..topay.._G.Currency )
							-- else
								-- self.mainbtns.leftbutn:setLabel( translat[1][3].." "..topay.._G.Currency )
							-- end
						end
						self.slideview:setTabLabel( self.numslide, topay.._G.Currency )
				end
			})
			--if currtipsperc == 0 then self.ordview:setTipsPerc( 0.05 ) end
			tips.butns.anchorY = 1
			tips:insert( tips.butns )
			tips.butns.width = tips.butns.width - (tips.butns.width-_Xa)
		
			local line1 = display.newRect( tips, 0, 0, _Xa, 1.3 )
			line1:setFillColor( 0,0,0, 0.1 )
			line1.x = 0
			line1.y = bref - tips.butns.height
			line1:toBack()

			local line2 = display.newRect( tips, 0, 0, _Xa, 1.3 )
			line2:setFillColor( 0,0,0, 0.1 )
			line2.x = 0
			line2.y = bref
			line2:toBack()

			local translat = translations.OrderTxt_MainButtons[self.lang]
			if self.mainbtns.leftbutn~=nil then
				-- if userProfile.SelectedRestaurantData.use_bonusme_proccessing==1 or userProfile.SelectedRestaurantData.use_bonusme_proccessing==2 then								
					self.mainbtns.leftbutn:setLabel( translat[1][5].." "..ordsum*(1+currtipsperc).._G.Currency )
				-- else
					-- self.mainbtns.leftbutn:setLabel( translat[1][3] )
				-- end
			end
		end

		if (ordstate == 2 or ordstate == 3 or ordstate == 6 ) and userProfile.SelectedRestaurantData.pos_type==1 then
			local tipssum = currtipsperc*ordsum
			tips.label = display.newText( 	
				translations.OrderTxt_Tips[self.lang]..":",
				0,0,
				self.font_title, self.fsize_title
			) 
			tips.label:setFillColor( 0,0,0 )
			tips.label.anchorX = 0
			tips.label.x = lref + self.pad_regular
			tips.label.anchorY = 1
			local y = bref -5 
			if tips.butns then y = y - tips.butns.height - 5 end
			tips.label.y = y
			tips:insert( tips.label )

			tips.sumlabel = self:createPlate({ 	
					text = tipssum.._G.Currency,     
					font = self.font_title,   
					fsize = self.fsize_regular,
				pad = 0.5,
				align = "left"
			}) 
			tips.sumlabel.anchorX = 0
			tips.sumlabel.x = tips.label.x + tips.label.width
			tips.sumlabel.y = tips.label.y - tips.label.height*0.5 + 1
			tips:insert( tips.sumlabel )
		end

		return tips
	end

	function new_ovg:onChangeOrderState( orderv )
		self.cid = self.cid or orderv.cid
		print("new_ovg:onChangeOrderState",self.cid,orderv.cid)
		if orderv.lid == self.ordview.lid then
			-- print("222")
			-- collectData( self.ordview )
			local ordstate = orderv:getOrderState()
			currstate = ordstate
			print("onChangeOrderState ordstate",ordstate)
			self.switcher:lock()
			self.infopanel:setState( ordstate )
			if self.rspanel then self.rspanel:setState( ordstate ) end
			if self.delivpanel then self.delivpanel:setState( ordstate ) end
			self.itmpanel:setState( ordstate )
			self.mainbtns:setState( ordstate )

			if self.tips then
				new_ovg.downline.y = new_ovg.downline.y + new_ovg.tips.height
				self.tips:removeSelf(); self.tips = nil
				new_ovg:createPanels( new_ovg.header_height )  
			end
			self.tips = self:createTipsButtons( self.botref - self.mainbtns.height )
			if self.tips then 
				self.downline.y = self.tips.y - self.tips.height
				new_ovg:createPanels( new_ovg.header_height ) 
			end		 	
		end
		-- print("new_ovg:onChangeOrderState - spinner:stop")
		self.spinner:stop()
	end
	new_ovg.ordview.onChangeOrderState = function( ordv )
		new_ovg:onChangeOrderState( ordv )
	end

	function new_ovg:catchError( ordv, err )
		Spinner:stop()
		self.spinner:stop()

		local translat = translations.OrderTxt_Errors[self.lang]
		local mess = ""
		if err == "not sent" then
			mess = translat[1]
		elseif err == "no internet" then
			mess = translat[2]
		elseif err == "not deleted" then
			mess = translat[3]
		elseif err == "no address" then
			mess = translat[5]
		elseif err == "delete denied" then
			mess = translat[4]		
		end

		local messbox = display.newGroup()
		self:insert( messbox )

				local text = display.newText({
						parent = messbox,
						text = mess,     
						x = 0,
						y = 0,
						width = _Xa*0.7,
						font = "HelveticaNeueCyr-Medium",   
						fontSize = 14
				})
				text:setFillColor( 1, 1, 1 )
		
		local box = display.newRoundedRect( messbox, 
			0, 0, text.width + 30, text.height + 30, 15 
		)
		box:setFillColor( 0,0,0, 0.7 )
		text:toFront()

		messbox.timer_handle = timer.performWithDelay( 1500, function()
			messbox:removeSelf()
			messbox = nil
		end )
	end
	new_ovg.ordview.onThrowError = function( ordv, err )
		new_ovg:catchError( ordv, err )
	end
	
	function new_ovg:delete()
		if ov.map then ov.map:removeSelf(); ov.map = nil end
		self:removeSelf()
		self = nil
	end

	local translat = translations.OrderTxt_States[new_ovg.lang]
	new_ovg.statemess = {
		[0] = translat[1],
		[1] = translat[2],
		[2] = translat[3],
		[3] = translat[4],
		[4] = translat[5],
		[5] = translat[6],
		[6] = translat[7],
		[7] = translat[8],
		[8] = translat[9]
	}

	new_ovg.bg = display.newRect( new_ovg, 0, 0, _Xa, _Ya-common_tab_height*2 )
	new_ovg.bg:setFillColor( 1,1,0, 0 )
	--new_ovg.bg:setStrokeColor( 0,0,0 )
	new_ovg.bg.strokeWidth = 2

	new_ovg.topref = - new_ovg.bg.height*0.5
	new_ovg.botref = new_ovg.bg.height*0.5
	new_ovg.leftref = - display.screenOriginX
	new_ovg.rightref = _X + display.screenOriginX
	new_ovg.centerx_ref = new_ovg.leftref + new_ovg.bg.width*0.5
	new_ovg.pad_title = 20
	new_ovg.pad_regular = 10
	new_ovg.fsize_title = 13
	new_ovg.fsize_regular = 13
	new_ovg.fsize_item = 15
	new_ovg.fsize_tap = 18
	new_ovg.font_title = "HelveticaNeueCyr-Light"
	new_ovg.font_regular = "HelveticaNeueCyr-Thin" 
	new_ovg.font_bold = "HelveticaNeueCyr-Medium"
	new_ovg.header_height = 30

	new_ovg.spinner = Spinner:new( {1}, 0.6, new_ovg.bg.width, new_ovg.bg.height)
	new_ovg.spinner.x = new_ovg.bg.x
	new_ovg.spinner.anchorY = 0
	new_ovg.spinner.y = new_ovg.topref
	new_ovg:insert( new_ovg.spinner )

	new_ovg.switcher = new_ovg:createOrderTypePanel( new_ovg.topref + 14 )
	--new_ovg.switcher = new_ovg:createSwitcher( new_ovg.topref )
	new_ovg.topline = display.newRect( new_ovg, 0, 0, _Xa - new_ovg.pad_regular*2, 0.7 )
	new_ovg.topline:setFillColor( 0, 0.1 )
	new_ovg.topline.y = new_ovg.topref + 65
	new_ovg.mainbtns = new_ovg:createMainButtons( new_ovg.botref )
	new_ovg.tips = new_ovg:createTipsButtons( new_ovg.botref - new_ovg.mainbtns.height )
	new_ovg.downline = display.newRect( 0, 0, _Xa - new_ovg.pad_regular*2 , 0.7 )
	new_ovg.downline:setFillColor( 0, 0.1 )
	new_ovg.downline.anchorY = 1
	new_ovg.downline.y = new_ovg.botref - new_ovg.mainbtns.height
	if new_ovg.tips then new_ovg.downline.y = new_ovg.downline.y - new_ovg.tips.height end 
	new_ovg:insert( new_ovg.downline )
	new_ovg.ordview:changeOrderType( new_ovg.ordview.odata.ord_type )
	new_ovg:createPanels( new_ovg.header_height )
--	print(new_ovg.ordview.odata.ord_type)
	new_ovg.topline:toFront()
	new_ovg.anchorChildren = true
	new_ovg.anchorY = 0
	new_ovg.y = -_Ya*0.5 + common_tab_height
	return new_ovg
end


-----------------------------------------------------------------------------------

return ov