local uilib = require "ui.uilib"
ui_elm = {}

function ui_elm:countSetUI(startCount, valuePrice, mode, customLook, onSetCount,onDeleteItem, onAddToOrder,disable) -- елемент задания количества 
    mode = mode or 0
    disable = disable or 0
    local  cLook= customLook or {}
    local g = display.newGroup()
    local baseSize = cLook.height or 40
    local wU= cLook.width or 300
    local colorR1 = cLook.cR1 or {0.913,0.913,0.913}
    local colorRS1 = cLook.cRS1 or {0.5,0.5,0.5}
    local strokeWidthR1 = cLook.sWR1 or 1
    local colorR2 = cLook.cR2 or {1,1,1}
    local colorUR = cLook.cUR or {0.913,0.913,0.913}
    local colorLB = cLook.cLB or { default={0,0,0,1}, over={0,0,0,0.5}}
    local colorLCount = cLook.cLCount or {61/255,78/255,176/255}
    local colorLSum = cLook.cLSum or {0.1,0.1,0.1}

    local sizeFontLCount = math.roundIdp(baseSize*0.6,0) 
    local sizeFontLSum = math.roundIdp(baseSize*0.7,0) 
    local sizeFontLBPlus = math.roundIdp(baseSize*0.8,0) 
    local sizeFontLBMinus = math.roundIdp(baseSize*0.82,0)

    local setCountCallBack = onSetCount or function(c) print("onSetcount:", g.l_count.text)  end 
    local deleteCallBack = onDeleteItem or function(c) print("onDeleteItem:", g.l_count.text)  end 
    local addToOrder = onAddToOrder or function(c) print("onAddToOrder:", g.l_count.text)  end 

    local r = display.newRoundedRect(-wU*0.5,0, baseSize*2.5, baseSize,baseSize*0.35)
    r:setFillColor(unpack(colorR1))
    r.strokeWidth = strokeWidthR1
    r:setStrokeColor(unpack(colorRS1))
    g:insert(r)

    local r2 = display.newRect( r.x, r.y, r.height-strokeWidthR1*2,r.height-strokeWidthR1*2 )
    r2:setFillColor(unpack(colorR2))
    g:insert(r2)
    local r_un = display.newRoundedRect(r.x+r.width+2,0, baseSize*6, baseSize+2,baseSize*0.5)
    r_un:setFillColor(unpack(colorUR))
    g:insert( 1,r_un)

    local labelCount = display.newText({text = startCount, x =r.x, y=r.y, width= r2.width,font=native.systemFont, fontSize=sizeFontLCount, align = "center"})
    labelCount:setFillColor(unpack(colorLCount))
    g:insert(labelCount)
    g.l_count = labelCount

    local labelSum = display.newText({text = tonumber(labelCount.text)*valuePrice,width=200,x=r_un.x+r.width*0.2,y=r.y,font=native.systemFont,fontSize=sizeFontLSum,align="center"})
    labelSum:setFillColor(unpack(colorLSum))
    g:insert(labelSum)

    local bPlus = widget.newButton({
            x = r.x+r.width*0.33,
            y = r.y-mode,
            label = "+",
            fontSize= sizeFontLBPlus,
            labelColor = colorLB,
            onRelease = function(e)
                g.l_count.text=tonumber(g.l_count.text)+1; 
                labelSum.text = math.roundIdp(tonumber(g.l_count.text)*valuePrice)
                setCountCallBack(tonumber(g.l_count.text))
            end,
            textOnly = true
        })
    g:insert(bPlus)
    local bMinus = widget.newButton({
            x = r.x-r.width*0.33,
            y = r.y-2,
            label = "-",
            fontSize= sizeFontLBMinus,
            labelColor = colorLB,
            onRelease = function(e)
                if tonumber(g.l_count.text) >1 then
                    g.l_count.text=tonumber(g.l_count.text)-1
                    labelSum.text = math.roundIdp(tonumber(g.l_count.text)*valuePrice)
                    setCountCallBack(tonumber(g.l_count.text))
                else
                    deleteCallBack()
                end
            end,
            textOnly = true
        })
    g:insert(bMinus)

    if mode == 0 then  
        local bBuy = widget.newButton({
            x = r_un.x+r_un.width*0.5+80,
            y = 0,
            sheet = graphics.newImageSheet( "images/b_buy_it_v.png",{
                width = 84,
                height = 84,
                numFrames = 2,
                sheetContentWidth = 168,
                sheetContentHeight = 84
                } ),
            defaultFrame = 1,
            overFrame = 2,
            onRelease =  function(e)
                if disable == 0 then
                    local count = tonumber(g.l_count.text)
                    addToOrder(count)
                    g.l_count.text = 1
                else
                    native.showAlert( "SmartTouch POS", trans('can_not_edit'), { "OK" })
                end
            end            
            })
        bBuy:scale( 0.6, 0.6 )
        g:insert(bBuy)
    end
    g.wU=wU+r.width
    if disable > 0 then
        if mode ==1 then
            bPlus:setEnabled(false)
            bMinus:setEnabled(false)
            bPlus.alpha=0
            bMinus.alpha=0
        end
    end
    return g    
end

function ui_elm:dottedBorder(width, height, step, space, strokeWidth, colorLine) -- пунктирная рамка
    step = step or  10
    space = space  or 5
    width =width or 500
    height =height or 50
    colorLine = colorLine or  {1,1,1}
    strokeWidth=strokeWidth or 1

    local g = display.newGroup()
    
    local a={x=-width*0.5, y=-height*0.5}
    local b={x=width*0.5, y=-height*0.5}
    local c={x=width*0.5, y=height*0.5}
    local d={x=-width*0.5, y=height*0.5}

    local x = a.x+space+step
    while x < b.x do
        local l = display.newLine(x-step,a.y,x,a.y)
        l:setStrokeColor(unpack(colorLine))
        l.strokeWidth=strokeWidth
        g:insert(l)
        x=x+space+step
    end

    local y = b.y+step+space
    while y < c.y do
        local l = display.newLine(b.x,y-step,b.x,y)
        l:setStrokeColor(unpack(colorLine))
        l.strokeWidth=strokeWidth
        g:insert(l)
        y=y+space+step
    end

    x = c.x-space-step
    while x > d.x do
        local l = display.newLine(x+step,c.y,x,c.y)
        l:setStrokeColor(unpack(colorLine))
        l.strokeWidth=strokeWidth
        g:insert(l)
        x=x-space-step
    end

    local y = d.y-step-space
    while y > a.y do
        local l = display.newLine(a.x,y+step,a.x,y)
        l:setStrokeColor(unpack(colorLine))
        l.strokeWidth=strokeWidth
        g:insert(l)
        y=y-space-step
    end
    return g
end

function ui_elm:drawItem(dItem,x,y, customSettings ,onSetCount, onDeleteItem, customLookCUI, bDelItem,stateOrd) 
    -- dItem данные строки заказа
    -- x,y  координаты 
    -- customSettings настройки отображения (размер цвета)
    -- onSetCount функция для количества
    -- onDeleteItem функция удаления
    -- customLookCUI талица насроек для количества 
    -- bDelItem кнопка удаления если есть добавляем эту 

    local id_item=dItem.id or 0  
    local id_goods=dItem.mi_id or 0
    local v_name=dItem.name or ""
    local v_price=dItem.ordi_price or 0
    local id_photo=dItem.photo_id or 0
    local photo_format = dItem.photo_format  or "jpeg"
    stateOrd = stateOrd or 0

    local wI, hI,mS = 400,120,10
    local colorName = {1,1,1}
    local sizeFontName = 18
    local colorPrice = {0.7,0.7,0.7}
    local sizeFontPrice = 16
    local cofYCUI = 0.25

    if customSettings then
        wI, hI,mS = customSettings.wI, customSettings.hI, customSettings.mS
        if customSettings.colorName then
            colorName = customSettings.colorName
        end 
        if customSettings.sizeFontName then
            sizeFontName = customSettings.sizeFontName
        end 

        if customSettings.colorPrice then
            colorPrice = customSettings.colorPrice
        end 
        if customSettings.sizeFontPrice then
            sizeFontPrice = customSettings.sizeFontPrice
        end 
        if customSettings.cofYCUI then
            cofYCUI = customSettings.cofYCUI
        end 
    end

    local grI = display.newGroup()
    -- local rect = display.newRect( grI, 0, 0, wI, hI )   -- test
    -- rect:setFillColor( 1,0.2, 0.2)
    poslib:getPriceImage(id_photo,grI,false,(-wI+hI)*0.5,0,photo_format, nil, true,1) -- photo
    if  grI.photo then
        local maxS = math.max(grI.photo.width, grI.photo.height)
        local cofSC = (hI-mS*2)/maxS
        cofSC = math.roundIdp(cofSC,4)
        grI.photo:scale( cofSC, cofSC )
    end

    local l_name = display.newText( {parent = grI, text = v_name or "", x=-wI*0.5+hI+mS*2, y=-hI*0.30, width = wI-(hI+mS*2),font=native.systemFont,fontSize=sizeFontName,align ="left"})
    l_name.anchorX=0; l_name:setFillColor(unpack(colorName))
    local v_p = v_price or 0
    local l_price = display.newText( {parent = grI, text = v_p, x=-wI*0.5+hI+mS*2, y=l_name.y+l_name.height*0.5, width = wI-(hI+mS*2),font=native.systemFont,fontSize=sizeFontPrice,align ="left"})
    l_price.anchorX=0; l_price.anchorY=0; l_price:setFillColor(unpack(colorPrice))

    local g = ui_elm:countSetUI(dItem.ordi_count,v_p,1,customLookCUI,onSetCount, onDeleteItem, nil, stateOrd)
    g.x=l_name.x+g.wU*0.5
    g.y=hI*cofYCUI
    grI:insert(g)

    if bDelItem then
        bDelItem.x = wI*0.5-bDelItem.width
        bDelItem.y = 0
        grI:insert(bDelItem)
        if stateOrd > 0 then
            bDelItem:setEnabled(false)
            bDelItem.alpha = 0
        end
    else
        local hb=math.roundIdp(hI*0.13,0)
        local bDel = widget.newButton({
            x = wI*0.5-hb,
            y = -hI*0.5+hb,
            sheet = graphics.newImageSheet( "images/b_d_item_v.png",{
                width = 24,
                height = 24,
                numFrames = 2,
                sheetContentWidth = 48,
                sheetContentHeight = 24
                } ),
            defaultFrame = 1,
            overFrame = 2,
            onRelease =  function(e)
                if onDeleteItem then
                    onDeleteItem()
                end
            end
            })
        local cofSC = math.roundIdp(hb/bDel.height,4)
        bDel.alpha=0.3
        bDel:scale(cofSC, cofSC)
        grI:insert(bDel)
        if stateOrd > 0 then
            bDel:setEnabled( false)
            bDel.alpha=0
        end
    end
    grI.x, grI.y = x,y
    return grI
end

function ui_elm:drawShadow(baseGr, obj) -- отрисовка теней 
    local hh,wv = 6,6
    local color1, color2 = { 0, 0, 0,0.2 }, { 0, 0, 0,0 }
    local anX, anY = 0, 0
    anX = obj.width*obj.anchorX
    anY = obj.height*obj.anchorY
    
    grhd = { type="gradient", color1=color1, color2=color2, direction="down"}
    grhu = { type="gradient", color1=color1, color2=color2, direction="up"}
    grv1 = { type="gradient", color1=color1, color2=color2, direction="right"}
    grv2 = { type="gradient", color1=color1, color2=color2, direction="left"}
    
    local rsw = display.newRect( baseGr, obj.x-hh/4-anX, obj.y+obj.height+hh/4-anY, obj.width+hh/2, hh )
    rsw.anchorX=0;
    rsw:setFillColor(0.9)
    rsw.fill.effect = "filter.linearWipe"
    rsw.fill.effect.direction = { 0, 1 }
    rsw.fill.effect.smoothness = 1
    rsw.fill.effect.progress = 0.5

    rsw = display.newRect( baseGr, obj.x-hh/4-anX, obj.y-hh/4-anY, obj.width+hh/2, hh )
    rsw.anchorX=0;
    rsw:setFillColor(0.88)
    rsw.fill.effect = "filter.linearWipe"
    rsw.fill.effect.direction = { 0, -1 }
    rsw.fill.effect.smoothness = 1
    rsw.fill.effect.progress = 0.5      

    rsw = display.newRect( baseGr, obj.x+obj.width+wv/4-anX, obj.y-wv/4-anY, wv, obj.height+wv/2 ) 
    rsw.anchorY = 0; 
    rsw:setFillColor(0.88)
    rsw.fill.effect = "filter.linearWipe"
    rsw.fill.effect.direction = { 1, 0 }
    rsw.fill.effect.smoothness = 1
    rsw.fill.effect.progress = 0.5      
    
    rsw = display.newRect( baseGr, obj.x-wv/4-anX, obj.y-wv/4-anY, wv, obj.height+wv/2 )
    rsw.anchorY = 0
    rsw:setFillColor(0.9)
    rsw.fill.effect = "filter.linearWipe"
    rsw.fill.effect.direction = { -1, 0 }
    rsw.fill.effect.smoothness = 1
    rsw.fill.effect.progress = 0.5  
end

function ui_elm:barTab(params)
    params = params or {}
    local data = params.data or {}
    params.f_showKey = params.f_showKey or 0  

    local srl
    local onTap = params.onTap or
        function (id)
            print( "curActiveID: "..id )
        end
    local w = params.width or 400
    local h = params.height or 40
    local g = display.newGroup( )

    srl = widget.newScrollView({
        -- x=0, 
        -- y=0,
        width = w,
        height = h,
        scrollWidth = w*2,
        scrollHeight = h ,
        horizontalScrollDisabled =false ,
        verticalScrollDisabled = true,
        -- backgroundColor = { 0.8, 0.0, 0.0, 0.1 }
        hideBackground = true,
        })
    g:insert(srl)


    srl.labels = {}
    srl.curActiveID = 0
    local px, wrb = 76, 152
    for k, v in pairs(data) do
        local bgIm = display.newImage("images/b_im_scoll_ord.png", px, h-1 )
        bgIm.anchorY=1
        bgIm.id=k
        bgIm:setFillColor(224/255,238/255,251/255)

        bgIm:addEventListener( "tap", function(e)
            print( "TAP", e.target.id )
            g:setActiveBntByid(e.target.id)
        end )

        srl:insert(bgIm)
        local str
        if params.f_showKey ~= 0 then
            str = k.." ("..v..")"
        else
            str = v
        end

        local swL = display.newText(str, bgIm.x, bgIm.y-bgIm.height*0.5, native.systemFont,18 )
        swL:setFillColor(47/255,139/255,230/255)

        srl:insert(swL)
        srl.labels[k]= {bg=bgIm,l=swL}
        px=px+wrb+8
    end

    function g:setActiveBntByid(id_ord)
        if  srl.curActiveID ~= id_ord then
            if srl.curActiveID ~= 0 then
                srl.labels[srl.curActiveID].l:setFillColor(47/255,139/255,230/255)                
                srl.labels[srl.curActiveID].bg:setFillColor(224/255,238/255,251/255)                
            end
            srl.curActiveID = id_ord
            srl.labels[srl.curActiveID].l:setFillColor(1)
            srl.labels[srl.curActiveID].bg:setFillColor(47/255,139/255,230/255)

            onTap(srl.curActiveID)

            local pos_x = srl:getContentPosition()
            if (srl.labels[srl.curActiveID].bg.x+srl.labels[srl.curActiveID].bg.width*0.5)> (srl.width-pos_x) then
                srl:scrollToPosition{x = -(srl.labels[srl.curActiveID].bg.x+srl.labels[srl.curActiveID].bg.width*0.5)+srl.width,time=20}
            elseif  (srl.labels[srl.curActiveID].bg.x-srl.labels[srl.curActiveID].bg.width*0.5)< math.abs(pos_x) then
                srl:scrollToPosition{x = -(srl.labels[srl.curActiveID].bg.x-srl.labels[srl.curActiveID].bg.width*0.5),time=20}
            end

        end
    end

    function g:getActiveId()
        return srl.curActiveID
    end
    return g
end

function ui_elm:newBitmappedLine(x1,y1,x2,y2,frame,rgb)
    local strokeSheetSpec = {
        sheetContentWidth = 1,
        sheetContentHeight = 32,
        width = 1,
        height = 8,
        numFrames = 4
    }
    local strokeSheet = graphics.newImageSheet("icons/strokes2345.png", strokeSheetSpec)
    local math_atan2 = math.atan2
    local math_deg = math.deg
    local math_sqrt = math.sqrt
    local dx = x2 - x1
    local dy = y2 - y1
    local len = math_sqrt(dx*dx+dy*dy)
    local theta = math_atan2(dy,dx)
    local obj = display.newImageRect(strokeSheet, frame, len, 5) --08.12.14
    obj.anchorX, obj.x, obj.y = 0, x1, y1 -- or equivalently:  obj.x, obj.y = (x1+x2)/2, (y1+y2)/2
    obj.rotation = math_deg(theta)
    if (rgb) then
        obj:setFillColor(unpack(rgb))
    end
    return obj
end

ui_elm.curGgF = nil
function ui_elm:newFieldEdit(params) -- base text label
    params = params or {}
    if not params.pText then
        print( "error not set parent text!!")
        return nil
    end

    local pText = params.pText

    local function createField()
        ui_elm.curGgF = nil
        params.backgroundColor = params.backgroundColor or {1,1,1}
        params.cornerRadius = params.cornerRadius or 3
        params.strWidthBackground = params.strWidthBackground or 1
        params.strColorBackground = params.strColorBackground or {0,0,0}
        params.colorFont = params.colorFont or {0,0,0}
        params.align = params.align or "left"
        params.typeValue = params.typeValue or "default"

        local cofbg = 1.3
        local grField = display.newGroup()
        if params.parentGR then
            params.parentGR.grF = grField
            params.parentGR:insert(params.parentGR.grF)
        end
        self.curGgF = grField

        local bgProtect = display.newRect(grField, 0, 0, mW, mH )
        bgProtect:setFillColor( 1,0,0,0.005)
        bgProtect:addEventListener( "tap", function( event ) return true end )
        bgProtect:addEventListener( "touch", function( event ) return true end )

        local background = display.newRoundedRect( grField,  0, 0, pText.width+(cofbg-1)*pText.width, pText.height+(cofbg-1)*pText.height, params.cornerRadius )
        background:setFillColor( unpack(params.backgroundColor) )
        background.strokeWidth = params.strWidthBackground 
        background:setStrokeColor(unpack(params.strColorBackground))
        background.anchorX = pText.anchorX
        -- background.x,background.y=pText:localToContent(0,0)

        local tHeight = pText.height
        local tWidth = pText.width+params.strWidthBackground+6
        if "Android" == system.getInfo("platformName") then
            tHeight = tHeight + 10
        end

        local fontSize = math.roundIdp(background.height-params.cornerRadius*2,0)-10   
        grField.textField = native.newTextField( 0, 0, background.width-params.cornerRadius*2, background.height-params.cornerRadius*2)
        grField.textField.x = background.x
        grField.textField.y = background.y
        grField.textField.align = params.align
        grField.textField.inputType = params.typeValue
        grField.textField.hasBackground = false
        grField.textField.text = pText.text
        grField.textField:setTextColor( unpack(params.colorFont) )
        grField.textField.font = native.newFont(native.systemFont)
        grField.textField.size = fontSize
        grField:insert( grField.textField)
        grField.textField.anchorX = pText.anchorX
        if pText.anchorX ==0 then
            grField.textField.x = grField.textField.x+params.cornerRadius
        elseif pText.anchorX == 1 then
            grField.textField.x = grField.textField.x-params.cornerRadius 
        end
        native.setKeyboardFocus(grField.textField)

        local function textListener( event )
            if ( event.phase == "began" ) then
            elseif ( event.phase == "ended" or event.phase == "submitted" ) then
                pText.text = event.target.text
                if params.onFinish then
                    params.onFinish(event.target.text)
                end
                if grField then
                    grField:removeSelf()
                    grField = nil
                end
                if params.parentGR then
                    params.parentGR.grF = nil
                end
                self.curGgF = nil
                native.setKeyboardFocus(nil)
            elseif ( event.phase == "editing" ) then
                pText.text = event.text
            end
        end
        grField.textField:addEventListener( "userInput", textListener )
    end
    pText:addEventListener( "tap",createField)
end


function ui_elm:enterText(defaultText,captionText,fieldNameText,widthFieldVale,widthRatio,listener,typeInput) 
    local gr = display.newGroup()
    local inputField
    defaultText = defaultText or ""
    captionText = captionText or ""
    fieldNameText = fieldNameText or ""
    typeInput = typeInput or 0 
    widthFieldVale = widthFieldVale or 180 
    widthRatio =  widthRatio or 1 -- widthRatio - соотношение ширины названия к полю значения 

    local grS = display.newGroup()

    local handleCancelEvent = function ( event )
        if ( "ended" == event.phase ) then
            display.remove(gr)
            gr=nil
            if listener then  
                listener({result=inputField.text, index=1})
            end
            _G.isSkipTap = true
        end
    end

    local handleConfirmEvent = function ( event )
        if ( "ended" == event.phase ) then
            display.remove(gr)
            gr=nil
            if listener then  
                listener({result=inputField.text, index=2})
            end 
            _G.isSkipTap = true      
        end
    end                         
    
    local bg1 = display.newRect( display.contentWidth / 2+display.screenOriginX, display.contentHeight / 2+display.screenOriginY, display.contentWidth-4*display.screenOriginX, display.contentHeight-4*display.screenOriginY )
    bg1:setFillColor( css.colorInactiveBackground ); bg1.alpha = 0.5
    bg1:addEventListener( "touch", function( event ) return true end )
    bg1:addEventListener( "tap", function( event ) return true end )
    gr:insert( bg1 )

    local bgForm = display.newRect( display.contentWidth/2, display.contentHeight/2-30, 515, 300)
    bgForm:setFillColor(unpack(css.colorDialogBackground2))     
    gr:insert( bgForm )

    local rectTop = display.newRect(bgForm.x,  bgForm.y-bgForm.height/2+36, bgForm.width, 72)
    rectTop:setFillColor(1)
    gr:insert(rectTop)

    local title = display.newText( "", rectTop.x, rectTop.y, native.systemFontBold, css.dialogCaptionSize)
    title:setFillColor(unpack(css.colorActiveTabNew))
    gr:insert(title )

    title.text = captionText
    gr:insert(grS)

    local function createField(typeInput)
            local inputFd = native.newTextField( 5, -100, widthFieldVale, css.orderRowHeight)
            inputFd.anchorX = 0
            inputFd.align = "left"; inputFd.size = 36; inputFd:setTextColor( 1, 0.5, 0 )
            inputFd.size = css.orderRowTitleSize
            inputFd.text = defaultText
            inputFd.font = native.newFont( native.systemFont, 20 )
            return inputFd
    end

    inputField = createField(typeInput)
    grS:insert(inputField)
    inputField.y=0
    local w = math.abs(widthFieldVale+widthRatio*widthFieldVale+30)

    local inputFieldTxt = display.newText({parent = grS,text=fieldNameText,x=-5,y=0,font=native.systemFont,fontSize=css.orderRowTitleSize-2,width=widthFieldVale*widthRatio})
    inputFieldTxt.anchorX = 1;
    inputFieldTxt:setFillColor( 0 )
    grS.x = bgForm.x
    grS.y = bgForm.y

    local wbOld = bgForm.width

    if w > bgForm.width then
        bgForm.width = w
    end

    local  xl = inputFieldTxt:localToContent(0,0)
    local  xr = inputField:localToContent(0,0)
    local  xb = inputField:localToContent(0,0)

    if (xl-inputFieldTxt.width < xb-bgForm.width*0.5+10) or (xr+inputField.width> xb+bgForm.width*0.5-10)  then
        grS.x= (inputFieldTxt.width-inputField.width)*0.5+bgForm.x
    end 

    local confirmBtn = widget.newButton {
        sheet = graphics.newImageSheet( "images/common_b_1.png", { width=234,height=66,numFrames=2,sheetContentWidth=468,sheetContentHeight = 66}),
        defaultFrame = 1,
        overFrame = 2,
        fontSize=22,
        label = trans("yes"),
        labelColor = {default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
        x = bgForm.x+bgForm.width/4,
        y = bgForm.y+bgForm.height/2-50,
        onRelease = handleConfirmEvent 
    }
    gr:insert( confirmBtn )

    local cancelBtn = widget.newButton { 
        sheet = graphics.newImageSheet( "images/common_b_2.png", { width=234,height=66,numFrames=2,sheetContentWidth=468,sheetContentHeight = 66}),
        defaultFrame = 1,
        overFrame = 2,
        fontSize=22,
        label = trans("no"),
        labelColor = {default={1,1,1}, over={ 1,1,1,0.5 } },
        x = bgForm.x-bgForm.width/4,
        y = bgForm.y+bgForm.height/2-50,
        onRelease = handleCancelEvent 
    }   
    gr:insert( cancelBtn )

    return gr
end

function ui_elm:shade_gradient(r_obj, grP, size_sw, reg_size_angle)
    if not (r_obj and grP ) then
        return
    end
    size_sw = size_sw or 15
    reg_size_angle = reg_size_angle or 0

    local c_x, c_y, w, h  = r_obj.x, r_obj.y, r_obj.width, r_obj.height
    local ws,hs, hs2 = w, size_sw, h 
    local color_1 = { 0, 0, 0, 0.1 }
    local color_2 = { 0, 0, 0, 0 }
    local r_a =math.sqrt((hs*hs)/2)+reg_size_angle

    local r_sw = display.newRect( grP, c_x+w*0.5,c_y, hs, hs2 )
    r_sw.anchorX = 0
    r_sw.fill.effect = "generator.linearGradient"
    r_sw.fill.effect.color1 = color_1
    r_sw.fill.effect.position1  = { 0, 0 }
    r_sw.fill.effect.color2 = color_2
    r_sw.fill.effect.position2  = { 1, 0 }

    local l_sw = display.newRect( grP, c_x-w*0.5,c_y, hs, hs2 )
    l_sw.anchorX = 1
    l_sw.fill.effect = "generator.linearGradient"
    l_sw.fill.effect.color1 = color_1
    l_sw.fill.effect.position1  = { 1, 0 }
    l_sw.fill.effect.color2 = color_2
    l_sw.fill.effect.position2  = { 0, 0 }

    local t_sw = display.newRect( grP, c_x,c_y-h*0.5, ws, hs )
    t_sw.anchorY = 1
    t_sw.fill.effect = "generator.linearGradient"
    t_sw.fill.effect.color1 = color_1
    t_sw.fill.effect.position1  = { 0, 1 }
    t_sw.fill.effect.color2 = color_2
    t_sw.fill.effect.position2  = { 0, 0 }

    local b_sw = display.newRect( grP,c_x,c_y+h*0.5,ws, hs )
    b_sw.anchorY = 0
    b_sw.fill.effect = "generator.linearGradient"
    b_sw.fill.effect.color1 = color_1
    b_sw.fill.effect.position1  = { 0, 0 }
    b_sw.fill.effect.color2 = color_2
    b_sw.fill.effect.position2  = { 0, 1 }

    local a_sw_1 = display.newRect( grP, t_sw.x-t_sw.width*0.5, t_sw.y , r_a,r_a)
    a_sw_1.anchorY = 1
    a_sw_1.anchorX = 1
    a_sw_1.fill.effect = "generator.linearGradient"
    a_sw_1.fill.effect.color1 = color_1
    a_sw_1.fill.effect.position1  = { 1, 1}
    a_sw_1.fill.effect.color2 = color_2
    a_sw_1.fill.effect.position2  = { 0, 0 }

    local a_sw_2 = display.newRect( grP, t_sw.x+t_sw.width*0.5, t_sw.y , r_a,r_a)
    a_sw_2.anchorY = 1
    a_sw_2.anchorX = 0
    a_sw_2.fill.effect = "generator.linearGradient"
    a_sw_2.fill.effect.color1 = color_1
    a_sw_2.fill.effect.position1  = { 0, 1}
    a_sw_2.fill.effect.color2 = color_2
    a_sw_2.fill.effect.position2  = { 1, 0 }

    local a_sw_3 = display.newRect( grP, r_sw.x, r_sw.y+r_sw.height*0.5 , r_a,r_a)
    a_sw_3.anchorY = 0
    a_sw_3.anchorX = 0
    a_sw_3.fill.effect = "generator.linearGradient"
    a_sw_3.fill.effect.color1 = color_1
    a_sw_3.fill.effect.position1  = { 0, 0}
    a_sw_3.fill.effect.color2 = color_2
    a_sw_3.fill.effect.position2  = { 1, 1 }

    local a_sw_4 = display.newRect( grP, l_sw.x, l_sw.y+l_sw.height*0.5 , r_a,r_a)
    a_sw_4.anchorY = 0
    a_sw_4.anchorX = 1
    a_sw_4.fill.effect = "generator.linearGradient"
    a_sw_4.fill.effect.color1 = color_1
    a_sw_4.fill.effect.position1  = { 1, 0}
    a_sw_4.fill.effect.color2 = color_2
    a_sw_4.fill.effect.position2  = { 0,1}
end

function ui_elm:createCountryBox( parentGr, onClose )
    onClose = onClose or function() end

    local w, h = _Xa*0.8, _Ya
    local hview = nil
    local cntrbox = uilib.newBox( w, 330, onClose )
    cntrbox.isVisible = false
    cntrbox.shouldRecycle = true    
        
    cntrbox.name = display.newText({
        parent = cntrbox,
        text = translations["select_country"][_G.Lang],--translations.OrderTxt_ReservDate[self.lang][1],
        width = cntrbox.bg.width*0.8,
        font = "HelveticaNeueCyr-Medium", 
        fontSize = 15,
        align = "center"
    })
    cntrbox.name:setFillColor( 1,0,0 )
    cntrbox.name.x = cntrbox.bg.x
    cntrbox.name.anchorY = 0
    cntrbox.name.y = cntrbox.bg.y - cntrbox.bg.height*0.5 + 10
    parentGr.cntrbox =cntrbox
    parentGr:insert( cntrbox )
        
    cntrbox.separ1 = display.newLine( cntrbox, 
        cntrbox.bg.x - cntrbox.bg.width*0.5, cntrbox.name.y + cntrbox.name.height + 10,  
        cntrbox.bg.x + cntrbox.bg.width*0.5, cntrbox.name.y + cntrbox.name.height + 10
    )
    cntrbox.separ1:setStrokeColor( 0,0,0, 0.1 )
    cntrbox.separ1.strokeWidth = 1.5
    
    local function createCityLine( name, state, width, height, onRelease )
        local line = display.newGroup()
        line.anchorChildren = true
        line.anchorY = 0
        line.state = state and "on" or "off"
        line.name = name

        local bg = display.newRect( line, 0, 0, width, height )
        bg:setFillColor( 0.95, 0 )
        bg.isHitTestable = true

        local right = -bg.width*0.5

        local options = {
            width = 25,
            height = 25,
            numFrames = 2,
            sheetContentWidth = 50,
            sheetContentHeight = 25
        }
        local rbsheet = graphics.newImageSheet( "assets/orders/smallRadio.png", options )
        local radio_on = display.newImageRect( line, rbsheet, 1, 20, 20 )
        local radio_off = display.newImageRect( line, rbsheet, 2, 20, 20 )
        radio_on.anchorX = 0
        radio_off.anchorX = 0
        radio_on.x = right + 15
        radio_off.x = right + 15
        if line.state == "off" then radio_on.isVisible = false end

        local label = display.newText({
            parent = line, 
            text = name, 
            font = "HelveticaNeueCyr-Light", 
            fontSize = 17
        })
        label:setFillColor( 0 )
        label.anchorX = 0
        label.x = radio_on.x + radio_on.width + 10

        function line:turnOn()
            radio_on.isVisible = true
            self.state = "on"                
        end
        function line:turnOff()
            radio_on.isVisible = false
            self.state = "off"                
        end

        line:addEventListener( "tap", function( event )
            if event.target.state == "off" then event.target:turnOn() end
            onRelease({ 
                target = event.target,
                state = event.target.state,
                name = event.target.name
            })          
            return true
        end )

        return line
    end

    hview = widget.newScrollView({
        width = cntrbox.bg.width,
        height = cntrbox.bg.height-60,
        bottomPadding = 0,
        --backgroundColor = { 0.8, 0.8, 0.8 },
        horizontalScrollDisabled = true,
        hideBackground = true,
        listener = function( event )
            display.getCurrentStage():setFocus( nil )
            return true
        end
    })  
    hview.x=cntrbox.bg.x
    hview.y=cntrbox.bg.y+10
    cntrbox:insert( hview ) 

    if not _G.current_country then 
        _G.current_country = CountriesList[_G.userCountry][_G.Lang].cntr_name
    end
    
    local newline, sepr, state
    local ypos = 0  --cntrbox.separ1.y + 5
    if _G.countries == nil  or #_G.countries == 0 then
        _G.countries = ldb:getRowsData("countries","lang_code = '".._G.Lang.."' and cntr_code is not null  order by cntr_name ")
        _G.countries = _G.countries or {}
    end 
    for i=1, #_G.countries do
        state = false
        if _G.countries[i].cntr_name == _G.current_country then state = true end 
        newline = createCityLine( 
            _G.countries[i].cntr_name, state, 
            w, 40, 
            function( event )
                _G.current_country = event.name 
                cntrbox.isVisible = false
                display.getCurrentStage():setFocus( nil )
                onClose({ country = _G.countries[event.target.id] })
            end 
        )
        newline.id = i
        hview:insert( newline )
        newline.x = cntrbox.bg.x 
        newline.y = ypos
        ypos = ypos + newline.height

        sepr = display.newRect( hview, 0, 0, w, 1 )
        sepr:setFillColor( 0.95 )
        sepr.x = cntrbox.bg.x-33
        sepr.y = newline.y + newline.height   
        hview:insert( sepr )     
    end

    function cntrbox:select( country )
        local view = hview:getView()[1]
        for i=1, view.numChildren do
            if view[i].id then 
                if _G.countries[view[i].id].cntr_name == country then view[i]:turnOn()
                else 
                    view[i]:turnOff()
                end
            end
        end
    end
    return cntrbox
end

function ui_elm:newSelectList(params) -- список выбора 
    params = params or {}
    -- local  isIgnoreEnded = true
    local  isIgnoreTap = true
    local width = params.width or 200
    local height = params.height or 200
    local maxH = params.maxH or 350 

    local colorBgItDef = params.colorBgItDef or  {0.8,0.8,0.8}
    local colorBgItAct = params.colorBgItAct or {0.1,0.1,0.1}
    local colorLItDef = params.colorLItDef or {0,0,0}
    local colorLItAct = params.colorLItAct or {1,1,1,}
    local lItAlign= params.alignLabel or "left"
    local sizeFontLIt = params.sizeFont or 16
    if  isPhone then
        sizeFontLIt = params.sizeFont or 32 
    end
    if params.skipShadow == nil then
        params.skipShadow = false
    end 

    if params.skipOuterEvent  == nil then
        params.skipShadow = false
    end

    local isLine = false
    if params.isLine then
        isLine = true
    end  
    local f_onCancel = params.onCancel or function() end

    local mode = params.mode or 0 -- тип если 1 будут закладки 
    local dataSelect = params.data or {}
    -- local curValue = params.curValue or ""
    local curKey = params.curKey or -1
    if params.oldShadow == nil then
        params.oldShadow = false
    end 

    local gr_root = display.newGroup()
    local grSub = display.newGroup()
    grSub.alpha = 0

    if params.skipOuterEvent ~= true then
        local shield = display.newRect( gr_root,_mX, _mY, _Xa, _Ya )
        shield:setFillColor(1,1,1,0.01)
        -- shield:setFillColor(1,0,0,0.1)
        local isMove = false
        local isBeing = false
        shield:addEventListener( "touch", function(e)
            if e.phase=="began" then
                isMove = false
                isBeing = true
                if grSub then
                    f_onCancel()
                    grSub:removeSelf()
                    grSub=nil
                end
            elseif e.phase=="moved"then
                isMove = true
            elseif e.phase=="ended" then
                if  isMove and isBeing then
                    f_onCancel()
                    gr_root:removeSelf( )
                    gr_root=nil
                else
                    isIgnoreTap = false
                end
                isMove = false
            end 
         return true end )
        shield:addEventListener( "tap", function(e)
            if isIgnoreTap then
                isIgnoreTap = false
            else
                if isBeing  then
                    f_onCancel()
                    gr_root:removeSelf( )
                    gr_root=nil
                else
                    isBeing = true
                end
            end
            return true
        end)
    end

    gr_root:insert(grSub)
    local py, hIbg = 40,80 

    local h = 0
    local spase = params.spase or 8
    for k, v in pairs(dataSelect) do
        if h+hIbg+spase < maxH then
            h=h+hIbg+spase
        else
            break
        end
    end

    local hBG = height
    if not  params.height then
        if h > 0 then
           hBG=h+spase
        end
    end

    if maxH ~= 350 and hBG > maxH then
        hBG = maxH
    end
    if mode == 1 then
        hBG=hBG+40
    end


    local bg = display.newRoundedRect( grSub,0,0,width, hBG,3)
    bg:addEventListener( "touch", function() return true end )
    bg:addEventListener( "tap", function()  return true end )
    bg:setFillColor(1,1,1)

    local title
    if params.titleWindow then
        title=display.newText({parent=grSub, text=params.titleWindow,x=bg.x,y=bg.y-bg.height*0.5+35,width=bg.width-30,font=native.systemFont,fontSize=sizeFontLIt+2,align="center"})
        title:setFillColor(unpack(colorLItDef))
    end

    local sh=hBG
    if mode == 1 then
        sh=sh-40
    end
    if title then
        sh=sh-70
    end

    if params.skipShadow ~= true then
        if params.oldShadow then
            local line_rgb=css.colorGreySelection
            grSub.borderlines={}
            grSub.borderlines.bottom=self:newBitmappedLine(-bg.width*0.5, bg.height*0.5, bg.width*0.5, bg.height*0.5,1,line_rgb)
            grSub:insert( grSub.borderlines.bottom )
            grSub.borderlines.top=self:newBitmappedLine(-bg.width*0.5, -bg.height*0.5, bg.width*0.5, -bg.height*0.5,1,line_rgb)
            grSub:insert( grSub.borderlines.top )
            grSub.borderlines.left=self:newBitmappedLine(-bg.width*0.5, -bg.height*0.5, -bg.width*0.5, bg.height*0.5,1,line_rgb)
            grSub:insert( grSub.borderlines.left )
            grSub.borderlines.right=self:newBitmappedLine(bg.width*0.5, -bg.height*0.5, bg.width*0.5, bg.height*0.5,1,line_rgb)
            grSub:insert( grSub.borderlines.right )
            ui_elm:drawShadow(grSub,bg)
        else
            local s= ui_elm:shadow(bg.width,bg.height,nil,0.2)
            s.x,s.y = bg.x, bg.y
            grSub:insert(1,s)
        end
    end

    function grSub:initList(dataItems)
        if grSub.scroll then
            display.remove(grSub.scroll)
            grSub.scroll = nil
        end
        local scroll = widget.newScrollView({
                x = 0,
                y = 0,
                width = width-spase,
                height = sh,
                scrollWidth = width*2,
                scrollHeight = height*2,
                horizontalScrollDisabled =true ,
                verticalScrollDisabled = false,
                hideBackground = true,
                -- backgroundColor = { 0.8, 0.0, 0.0, 0.1 }
                -- listener = scrollListener
                })
        grSub.scroll = scroll
        if mode == 1 then
            scroll.y=scroll.y+20
        end
        if title then
            scroll.y=scroll.y+35 
        end
        grSub:insert(scroll)

        scroll.labels = {}
        scroll.curActiveID = -1
        local px = scroll.width*0.5
        py=spase*0.5
        if params.startY then
            py = params.startY
        end
        for k, v in pairs(dataItems) do
            local bgIm 
            if params.drawTap then
                bgIm = params.drawTap()
                if bgIm.custHeight then -- как признак группы
                    bgIm.x,bgIm.y=px,py+bgIm.height*0.5
                else
                    bgIm.x,bgIm.y=px,py
                end            
            else
                bgIm=display.newImage("images/b_im_scoll_ord.png",px, py )
                bgIm.width=scroll.width-8
                bgIm:setFillColor(unpack(colorBgItDef))
            end
            if not bgIm.custHeight then
                bgIm.anchorY=0
            end
            bgIm.id=k

            bgIm:addEventListener( "tap", function(e)
                print( "TAP", e.target.id )
                scroll:setActiveBntByid(e.target.id, true)
            end )

            if curKey ~= -1 then
             if curKey== k then
                 scroll.curActiveID = k
             end
            end

            scroll:insert(bgIm)
            local l
            local wIm = 0
            if params.dataIm  then
                if params.dataIm[k] and params.dataIm[k].obj then
                    -- params.dataIm[k].obj.anchorX=0
                    local im = params.dataIm[k].obj 
                    im.x=bgIm.x-bgIm.width*0.5+im.width*0.5
                    if bgIm.anchorY == 0 then
                        im.y=bgIm.y+bgIm.height*0.5
                    else
                        im.y=bgIm.y
                    end 
                    scroll:insert(im)
                    im.alpha=1
                    wIm = im.width
                end 
            end

            if bgIm.custHeight then 
                l = display.newText({text=v,x=bgIm.x+wIm,y=bgIm.y,width=bgIm.width-30,font=native.systemFont,fontSize=sizeFontLIt,align=lItAlign})
            else
                l = display.newText({text=v,x=bgIm.x+wIm,y=bgIm.y+bgIm.height*0.5,width=bgIm.width-30,font=native.systemFont,fontSize=sizeFontLIt,align=lItAlign})
            end
            l:setFillColor(unpack(colorLItDef))
            scroll:insert(l)
            scroll.labels[k]= {bg=bgIm,l=l}

            if  isLine then
                local ln=ui_elm:newBitmappedLine(0,bgIm.y+bgIm.height,scroll.width,bgIm.y+bgIm.height,1,{ 0.8, 0.8, 0.8})
                ln.anchorX=0
                scroll:insert(ln)
            end
            py=py+bgIm.height+spase
        end

        function scroll:setActiveBntByid(id_l, set)
            if  scroll.curActiveID ~= id_l then
                if scroll.curActiveID ~= -1 then
                    scroll.labels[scroll.curActiveID].l:setFillColor(unpack(colorLItDef))                
                    scroll.labels[scroll.curActiveID].bg:setFillColor(unpack(colorBgItDef))                
                end
                scroll.curActiveID = id_l
                scroll.labels[scroll.curActiveID].l:setFillColor(unpack(colorLItAct))
                scroll.labels[scroll.curActiveID].bg:setFillColor(unpack(colorBgItAct))

                if set then
                    if params.onSet then
                        params.onSet(id_l,dataItems[id_l])
                    end
                    gr_root:removeSelf( )
                    gr_root=nil
                end
            else
                if set then
                    gr_root:removeSelf( )
                    gr_root=nil
                end
            end
        end

        if scroll.curActiveID ~= -1 then
            local cv =scroll.curActiveID
            scroll.curActiveID = -1
            scroll:setActiveBntByid(cv)
        end
    end

    if mode == 0 then
        grSub:initList(dataSelect)
    else
        local dataH = {}
        local actId = 0
        for k, v in pairs(dataSelect) do
            dataH[k]=v.name or ""
            for ki, vi in pairs(v.items) do
                if ki==curKey then
                    actId = k
                end
            end
        end

        local function onTapTopBar(id_H)
            grSub:initList(dataSelect[id_H].items)
        end
        local topBar
        topBar = ui_elm:barTab({width=width-10, data=dataH, onTap = onTapTopBar})
        topBar.y=bg.y-bg.height*0.5+2 
        topBar.x=-(width-10)*0.5
        grSub:insert(topBar)
        if actId ~= 0 then
            topBar:setActiveBntByid(actId)
        end

        local l=ui_elm:newBitmappedLine(bg.x-bg.width*0.5+1,bg.y-bg.height*0.5+43,bg.x+bg.width*0.5-1,bg.y-bg.height*0.5+43,1,{ 0.8, 0.8, 0.8})
        grSub:insert(l)
    end
    if params.onShow then
        params.onShow()
    end

    grSub.heightBg=bg.height
    grSub.widthBg=bg.width
    transition.to( grSub, { time=100, alpha=1.0 } )
    return grSub
end

function ui_elm:shadow(w,h,wsh,opacity,fShow) -- тени на остнове рисунка
    -- wsh ширина тени
    w= w or 100
    h= h or 100
    fShow = fShow or {1,1,1,1,1,1,1,1} --  что рисуем очередность ниже (сперва укглы потом стороны)

    opacity = opacity or 0.05
    local gs = display.newGroup()
    local hs = wsh or 20

    local options={
         frames = {
             {
                x = 0,
                y = 0,
                width = 20,
                height = 20
            },
            {
                x = 20,
                y = 0,
                width = w,
                height = 20
            },
            {
                x = 20,
                y = 0,
                width = h,
                height = 20
            },
         },
        sheetContentWidth = 1200,
        sheetContentHeight = 20
    }
    local s = graphics.newImageSheet( "icons/gr.png",options)
    local r

    if fShow[1] > 0 then
        r = display.newImageRect(gs,s,1,hs,hs) -- верхний левый угол
        r.anchorX=1; r.anchorY=1
        r.x,r.y = -w*0.5,-h*0.5
    end

    if fShow[2] > 0 then
        r = display.newImageRect(gs,s,1,hs,hs) -- верхний правый угол
        r.anchorX=1; r.anchorY=1
        r.x,r.y = w*0.5,-h*0.5
        r:rotate(90)
    end

    if fShow[3] > 0 then
        r = display.newImageRect(gs,s,1,hs,hs) -- нижний правый угол
        r.anchorX=1; r.anchorY=1
        r.x,r.y = w*0.5,h*0.5
        r:rotate(180)
    end

    if fShow[4] > 0 then
        r = display.newImageRect(gs,s,1,hs,hs) -- нижний левый угол
        r.anchorX=1; r.anchorY=1
        r.x,r.y = -w*0.5,h*0.5
        r:rotate(270)
    end

    if fShow[5] > 0 then
        r = display.newImageRect(gs,s,2,w,hs) -- верх
        r.anchorX=0.5; r.anchorY=1
        r.x,r.y = 0,-h*0.5
    end

    if fShow[6] > 0 then
        r = display.newImageRect(gs,s,3,h,hs) -- правая сторона
        r.anchorX=0.5; r.anchorY=1
        r.x,r.y = w*0.5,0
        r:rotate(90)
    end

    if fShow[7] > 0 then
        r = display.newImageRect(gs,s,3,h,hs) -- левая сторона
        r.anchorX=0.5; r.anchorY=1
        r.x,r.y = -w*0.5,0
        r:rotate(-90)
    end

    if fShow[8] > 0 then
        r = display.newImageRect(gs,s,2,w,hs) -- низ
        r.anchorX=0.5; r.anchorY=1
        r.x,r.y = 0, h*0.5
        r:rotate(180)
    end

    gs.alpha = opacity
    return gs
end

function  ui_elm:newRowList(params) -- список, функция отрисовки задать в параметрах
    params = params or {}
    local width = params.width or  _Xact
    local height = params.height or  _Xact
    local left = params.left+_Xo or _Xo
    local top = params.top+_Yo or _Yo
    local colorBackground = params.colorBackground or { 0.79, 0.79, 0.79}
    local space = params.space or 2
    local drawRowItem = params.drawRowItem or function() print("drawRowItem") end
    local drawRowGroup = params.drawRowGroup or function() print("drawRowGroup") end
    local drawSpase = params.drawSpase or nil -- прорисовка при раскритии 
    local fun_listener = params.listener
    -- local getDataForSpaseFromComonParams = params.drawRowGroup or nil -- колбек получения параметров для функции drawSpase  

    local groot = display.newGroup( )

    groot.isShowPart = false
    if params.stepShow ~= nil and params.stepShow > 0 and params.listener == nil  then
        groot.isShowPart = true
        groot.stepShow = params.stepShow or 50
        groot.currStartShow =  1
        groot.currEndShow =  groot.stepShow 
        groot.allInsertData = {}

        function groot:upShow(isDown)
            if isDown == nil then
                isDown = true
            end
            if isDown then -- вниз
                print("update down !!")
                if self.currEndShow == #self.allInsertData then
                    return
                else
                    local oldEnd = self.currEndShow 
                    self.currEndShow = self.currEndShow+self.stepShow --self.numItsOneScreen
                    if  self.currEndShow > #self.allInsertData then
                        self.currEndShow  = #self.allInsertData 
                    end
                    for i=oldEnd+1,self.currEndShow do
                        self:addRow(self.allInsertData[i].dataItem,self.allInsertData[i].params,self.allInsertData[i].isGr,nil,true)                        
                    end
                    self:upHeightScroll()
                    return
                end 
            else -- вверх
                print("update top !!")
                if groot.currStartShow == 1 then
                    return
                else
                    local oldEnd = self.currStartShow 
                    self.currStartShow = self.currStartShow-self.stepShow --self.numItsOneScreen
                    if self.currStartShow < 1 then
                        self.currStartShow = 1
                    end
                    self.scrll:setIsLocked("true","vertical")
                    self.scrll:scrollTo( "top", { time=400, onComplete=function()
                        local hh = 0
                        for i=oldEnd-1,self.currStartShow,-1 do
                            local a1,a2,h = self:addRow(self.allInsertData[i].dataItem,self.allInsertData[i].params,self.allInsertData[i].isGr,1,true)                        
                            hh=hh+h
                            self.scrll:scrollToPosition{y =-hh,time=1}
                        end
                            self:upHeightScroll()
                            self.scrll:setIsLocked("false","vertical")
                        end})
                    return
                end
            end
        end

        fun_listener = function(e)
            if e.limitReached then
                if math.abs(e.target._prevDeltaY) <= 10 then
                    if e.direction == "up" then
                        groot:upShow(true)
                    else
                        groot:upShow(false)
                    end
                end
            end 
        end
    end

    function groot:setShowInterval(p1,p2)
        if p1 and p2 then
            groot.currStartShow = p1
            groot.currEndShow = p2
        end
    end 

    function groot:setShowInterval(p1,p2)
        if p1 and p2 then
            groot.currStartShow = p1
            groot.currEndShow = p2
        end
    end

    local function createScroll()
        if  groot.scrll then
            groot.scrll:removeSelf( )
            groot.scrll = 0
            groot.allInsertData = nil
            groot.allInsertData = {}
            groot.currStartShow =  1
            groot.currEndShow =  groot.stepShow 
        end
        groot.scrll = widget.newScrollView({
                left = left,
                top =  top,
                width = width,
                height = height,
                scrollWidth = width*2,
                scrollHeight = width*2,
                horizontalScrollDisabled = true,
                verticalScrollDisabled = false,
                -- hideBackground = true,
                -- backgroundColor = { 0, 0, 0,0.2 }
                backgroundColor = colorBackground,
                -- hideBackground = true,
                listener = fun_listener
        })
        groot:insert(1,groot.scrll )
        groot.scrll.py = 0
        groot.scrll.currMaxIndex = 0
        groot.scrll.arrRowByIndex={}
        groot.scrll.arrRowByID={}
    end
    createScroll()

    function groot:addRow(dataItem, params, isGr, pos_index, breakInsert)
        isGr=isGr or 0
        pos_index = pos_index or 0 -- индекс вставка в указаное место 
        if breakInsert == nil then
            breakInsert = false
        end

        if self.isShowPart and breakInsert == false then
            table.insert(self.allInsertData,{dataItem=dataItem,params=params,isGr=isGr}) 

            local n = #self.allInsertData
            if n > self.currEndShow or n < self.currStartShow then
                return 0,0
            end
        end

        local row
        if isGr == 0 then
             row = drawRowItem(dataItem, params)
        else
             row = drawRowGroup(dataItem, params)
        end        
        local hi = row.height
        if row.heightSet then
            hi = row.heightSet
        end
        if row then
            if pos_index > 0 and  groot.scrll.arrRowByIndex[pos_index] then -- добавление на позицию 
                local py = groot.scrll.arrRowByIndex[pos_index].y -- сохраняем y 
                for k, v in pairs(groot.scrll.arrRowByIndex) do -- сдвиг
                    if k >= pos_index then
                        v.y = v.y+(hi-space)
                    end
                end

                local ix = groot.scrll.currMaxIndex
                while ix >= pos_index  do -- 
                    if groot.scrll.arrRowByIndex[ix] then
                        groot.scrll.arrRowByIndex[ix+1] =groot.scrll.arrRowByIndex[ix]
                    end
                    ix=ix-1
                end                
                groot.scrll.py=groot.scrll.py+(hi*0.5+space)*2
                row.y = py
                row.x = groot.scrll.width*0.5
                groot.scrll:insert( row )
                groot.scrll.currMaxIndex = groot.scrll.currMaxIndex+1 -- увеличивием индекс
                row.index = pos_index
                groot.scrll.arrRowByIndex[pos_index]=row
                if row.id then
                    groot.scrll.arrRowByID[row.id] = groot.scrll.arrRowByIndex[pos_index]
                else
                    row.id = row.index
                    groot.scrll.arrRowByID[row.id] = groot.scrll.arrRowByIndex[pos_index]
                end
            else -- добавление в конец 
                groot.scrll.py=groot.scrll.py+hi*0.5+space
                row.y = groot.scrll.py
                row.x = groot.scrll.width*0.5
                groot.scrll:insert( row )
                groot.scrll.py=groot.scrll.py+hi*0.5+space
                groot.scrll.currMaxIndex = groot.scrll.currMaxIndex+1
                row.index = groot.scrll.currMaxIndex
                groot.scrll.arrRowByIndex[groot.scrll.currMaxIndex]=row
                if row.id then
                    groot.scrll.arrRowByID[row.id] = groot.scrll.arrRowByIndex[groot.scrll.currMaxIndex]
                else
                    row.id = row.index
                    groot.scrll.arrRowByID[row.id] = groot.scrll.arrRowByIndex[groot.scrll.currMaxIndex]
                end
            end
        end
        return row.index, row.y, row.height 
    end

    function groot:deleteRowByIndex(index, ignoreMove)
        local h=0
        if groot.scrll.arrRowByIndex[index] then
            if  groot.scrll.arrRowByIndex[index].heightSet then
                h = groot.scrll.arrRowByIndex[index].heightSet
            else
                h=groot.scrll.arrRowByIndex[index].height 
            end

            if  groot.scrll.arrRowByID[groot.scrll.arrRowByIndex[index].id] then
                groot.scrll.arrRowByID[groot.scrll.arrRowByIndex[index].id] = nil
            end
            
            groot.scrll.arrRowByIndex[index]:removeSelf( )
            groot.scrll.arrRowByIndex[index] = nil
            for k, v in pairs(groot.scrll.arrRowByIndex) do
                if k > index and v.y then
                    v.y = v.y-(h+space*2)
                end
            end
            groot.scrll.py=groot.scrll.py-(h+space*2)
        end
        return h+space*2
    end

    function groot:clear()
        createScroll()
    end

    function groot:deleteRowByID(Row_id)
        if groot.scrll.arrRowByID[Row_id] and groot.scrll.arrRowByID[Row_id].index then
            self:deleteRowByIndex(groot.scrll.arrRowByID[Row_id].index)
        end
    end

    function groot:upHeightScroll()
        if self.scrll.arrRowByIndex[groot.scrll.currMaxIndex] then
            local hh = 0 
            for k, v in pairs(self.scrll.arrRowByIndex) do
                if v.heightSet then
                    if hh == 0 then
                        hh=hh+v.heightSet*0.5
                    end
                    hh=hh+v.heightSet
                end
                if v.gO then
                    hh=hh+v.gO.height+space*2
                end
            end
   
            if hh > self.scrll.height then
                self.scrll:setScrollHeight(hh)
            else
                self.scrll:setScrollHeight(self.scrll.height)
            end      
        end
    end

    function groot:updateLine(params, f_gr) -- обновление (полная перерисовка строки)
        f_gr = f_gr or 0
        local isAddSpase = false 
        if params.id and groot.scrll and groot.scrll.insert and  groot.scrll.arrRowByID[params.id] and groot.scrll.arrRowByID[params.id].index and groot.scrll.arrRowByIndex[groot.scrll.arrRowByID[params.id].index] then
            local x,y,index,hr = groot.scrll.arrRowByID[params.id].x, groot.scrll.arrRowByID[params.id].y, groot.scrll.arrRowByID[params.id].index,groot.scrll.arrRowByID[params.id].height
            if groot.scrll.arrRowByID[params.id].gO then
                self:deleteSpaceRowByID(params.id)
                isAddSpase = true
            end
            groot.scrll.arrRowByID[params.id]:removeSelf( )
            groot.scrll.arrRowByID[params.id] = nil
            if groot.scrll.arrRowByIndex[index] then
                groot.scrll.arrRowByIndex[index] = nil
            end
            local row
            if f_gr == 0 then -- рисуем новую строку 
              row = drawRowItem(params)
            else
              row = drawRowGroup(params)
            end

            if hr and  hr ~= row.height and not isAddSpase then -- есть изменения по высоте меняем y
                local h  = row.heightSet or row.height
                y = y+(h-hr)*0.5
            end

            row.y = y
            row.x = x
            row.index = index
            row.id = params.id
            groot.scrll.arrRowByIndex[index]=row
            groot.scrll.arrRowByID[params.id] = row 
            groot.scrll:insert( row )
            if hr and  hr ~= row.height and  not isAddSpase then -- есть изменения по высоте сдвиг всех кото ниже 
                for k, v in pairs(groot.scrll.arrRowByIndex) do
                    if k > index then 
                        local h  = row.heightSet or row.height
                        v.y = v.y+(h-hr)--*0.5
                    end
                end
            end

            if isAddSpase and drawSpase  then
                local o = drawSpase(params.id)
                if o then
                    o.x,o.y = 0,row.height*0.5
                    self:addSpaceRowByID(params.id,o)
                else
                    isAddSpase = false         
                end
            end
            return isAddSpase , groot.scrll.arrRowByID[params.id]
        else
            return isAddSpase , nil
        end
    end

    function groot:addSpaceRowByID(id,gO) -- добавляем пространоство под строкой
         -- gO  группа вкладки
         -- группа заносится в масив по ключу gO
         -- если она есть значит окрыта  
        if groot.scrll.arrRowByID[id] and not groot.scrll.arrRowByID[id].gO and gO and gO.height >0  then
            groot.scrll.arrRowByID[id].gO = gO
            local index = groot.scrll.arrRowByID[id].index
            for k, v in pairs(groot.scrll.arrRowByIndex) do
                if k > index then
                    v.y = v.y+(gO.height+space*2)
                end
            end
            groot.scrll.arrRowByID[id]:insert(gO)
            groot.scrll.py=groot.scrll.py-(gO.height+space*2)
            self:upHeightScroll()
        end
    end

    function groot:deleteSpaceRowByID(id) -- удаляем пространоство под строкой также  удаляется rруппа 
        if groot.scrll.arrRowByID[id] and groot.scrll.arrRowByID[id].gO and groot.scrll.arrRowByID[id].index  then
            local h, index = groot.scrll.arrRowByID[id].gO.height, groot.scrll.arrRowByID[id].index
            groot.scrll.arrRowByID[id].gO:removeSelf( )
            groot.scrll.arrRowByID[id].gO = nil
            for k, v in pairs(groot.scrll.arrRowByIndex) do
                if k > index then
                    v.y = v.y-(h+space*2)
                end
            end
            groot.scrll.py=groot.scrll.py-(h+space*2)
            self:upHeightScroll()
        end
    end
    return groot
end