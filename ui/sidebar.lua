--
-- Target devices: simulator, device
--
----------------------------------------------
local M = {}

--Requirements
local widget = require( "widget" )

local clouds = {236,240,241,255}
local silver = {189,195,199,255}
local asphalt = {52,73,94,255}
local darkgrey = {51/255,51/255,51/255}
local lightgrey = {238/255,238/255,238/255}
local orange = {255,102,0,255}

local iconPath = "icons/"
local overPath = iconPath .. "over/"
local defaultPath = iconPath .. "default/"

function M.newSidebar(options)
	local cases = {}

	local options = options or {}
	options.displayGroup = options.displayGroup
	options.top = options.top or display.screenOriginY
	options.left = options.left or display.screenOriginX
	options.width = options.width or 200
	options.height = options.height or display.actualContentHeight
	options.fcolor = options.fcolor or clouds
	options.bcolor = options.bcolor or darkgrey
	options.lcolor = options.lcolor or lightgrey
	options.ocolor = options.ocolor or orange
	options.bgColor = options.bgColor or darkgrey
	options.lockWhenPressed = options.lockWhenPressed or false
	options.items = options.items or { { id = "home", label = "Home", icon = "home", switch = true } }
	options.spacing = options.spacing or 12
	options.iconSize = options.iconSize or 32
	options.itemWidth = options.itemWidth or 30
	options.spacingX = options.spacing --(options.width - options.iconSize) / 2
	options.font = options.font or native.systemFontBold
	options.fontSize = options.fontSize or 14
	options.event = options.event or nil
	options.onOpen =  options.onOpen or function() end
	options.onClose =  options.onClose or function() end
	options.modal = options.modal or false
	options.side = options.side or "left"
	options.time = options.time or 333
	options.delay = options.delay or 0
	local XX

	local sidebar = display.newGroup()
	sidebar.anchorChildren = true
	sidebar.y = display.contentCenterY
	cases = {
		["left"] = function() 
		   sidebar.anchorX = 1
		   sidebar.x = display.screenOriginX + display.actualContentWidth
		end,
		["right"] = function() 
		   sidebar.anchorX = 0
		   sidebar.x = display.screenOriginX 
		end,
	}
	cases[options.side]()

	sidebar.options = options -- store for orientation change
	sidebar.state = "off"
	sidebar.lock = true

	local lining = display.newRect( sidebar, 0, 0, display.actualContentWidth, display.actualContentHeight )
	lining.isVisible = false
	lining.isHitTestable = false
	lining:setFillColor( 0, 0.5 )

	local bg = display.newRect( sidebar, 0, 0, options.width, options.height )
	cases = {
		["left"] = function() 
		   bg.anchorX = 1
		   bg.x = -lining.width*0.5
		end,
		["right"] = function() 
		   bg.anchorX = 0
		   bg.x = lining.width*0.5
		end,        
	}
	cases[options.side]()
	bg:setFillColor( unpack( options.bgColor ) )
	bg.isHitTestable = true
	bg:addEventListener( "touch", function( event )
		return true
	end)
	bg:addEventListener( "tap", function( event )
		return true
	end)

	local top_ref = -bg.height*0.5
	local left_ref = bg.x - bg.width*bg.anchorX
	local right_ref = lining.width*0.5
	
	-- Create the buttons
	sidebar.buttons = {}
	local button, left, top
	for i = 1, #options.items do
		button = display.newGroup()
		button.id = options.items[i].id
		button.options = options.items[i]
		
		if options.items[i].switch then
			button.state = options.items[i].state or "off"

			if not options.items[i].top then
				if i == 1 then top = top_ref 
				else top = sidebar.buttons[i-1].y + sidebar.buttons[i-1].height + options.spacing
				end
			else top = top_ref + options.items[i].top
			end
			if not options.items[i].left then
				if i == 1 then left = 0
				else left = sidebar.buttons[i-1][2].x - sidebar.buttons[i-1][2].width*0.5
				end                
			else left = -options.width*0.5 + options.items[i].left
			end 

			local buttonBg = display.newRect( button, 0, 0, options.width, options.itemWidth )
			buttonBg:setFillColor(0,0)
			buttonBg.isHitTestable = true

			if options.items[i].icon then
				left = left == 0 and 0 or (left + options.iconSize*0.5)

				button.buttonOn = display.newImageRect(button, overPath .. options.items[i].icon .. ".png",
					options.iconSize,
					options.iconSize
				)
				button.buttonOn.x = left
				if options.items[i].iconColorOn then
					button.buttonOn:setFillColor( unpack( options.items[i].iconColorOn ) )
				end
				button.buttonOn.alpha = (button.state == "off") and 0 or 1

				button.buttonOff = display.newImageRect(button, defaultPath .. options.items[i].icon .. ".png",
					options.iconSize,
					options.iconSize
				)
				button.buttonOff.x = left
				if options.items[i].iconColorOff then
					button.buttonOff:setFillColor( unpack( options.items[i].iconColorOff ) )
				end
				button.buttonOff.alpha = (button.state == "off") and 1 or 0

				left = button.buttonOn.x + options.iconSize*0.5
			end

			if options.items[i].label then
				button.label = display.newText( button, 
					options.items[i].label,
					0,0,
					native.systemFontBold,
					options.fontSize 
				)
				button.label.x = left ~= 0 and (left + button.label.width*0.5) or 0
				
				local label_color = options.items[i].labelColorOn
				if button.state == "off" then label_color = options.items[i].labelColorOff end
				button.label:setFillColor( unpack( label_color or {0,0,0, 0.5} ) )
			end

			button.anchorChildren = true
			button.anchorX = 0
			button.x = left_ref
			button.anchorY = 0
			button.y = top

			-- Toggle Listener

			function button:off()
				local opt = self.options
				if opt.icon then
					transition.to( self.buttonOn, { time=133, alpha=0 } )
					transition.to( self.buttonOff, { time=133, alpha=1 } )
				end
				if opt.label then
					self.label:setFillColor( unpack( opt.labelColorOff ) )
				end
				self.state = "off"
			end

			function button:on()
				local opt = self.options
				if opt.icon then
					transition.to( self.buttonOn, { time=133, alpha=1 } )
					transition.to( self.buttonOff, { time=133, alpha=0 } )
				end
				if opt.label then
					self.label:setFillColor( unpack( opt.labelColorOn ) )
				end
				self.state = "on"
			end

			function button:touch( event )
				if self.parent.lock then return end

				if event.phase == "began" then
					event.target.isFocus = true
				elseif event.phase == "moved" then
					if math.abs( event.xStart - event.x ) >= 10 then
						event.target.isFocus = false
					end
				elseif event.phase == "ended" or event.phase == "cancelled" then
					if not event.target.isFocus then return end

					if self.state == "off" then
						if options.modal then
							for j = 1, self.parent.numChildren do --loop through the parents childeren
								if self.id ~= self.parent[j].id and self.parent[j].state == "on" then -- if it's a state button
									local butn = self.parent[j]
									self.parent[j]:off()
									butn.options.onTurnOff( butn )
								end
							end
						end
						self:on()
						self.options.onTurnOn( self )
						self.parent:close()              
					elseif self.state == "on" then
						if not options.modal then
							self:off()
							self.options.onTurnOff( self )
						end
						self.parent:close() 
					end
				end

				return true
			end

			button:addEventListener("touch", button )
			sidebar.buttons[i] = button
		else
			button = widget.newButton	{
				top = options.top + options.spacing +((i-1) * (64 + options.spacing)),
				left = options.left + options.spacingX,
				width = options.iconSize,
				height = options.iconSize,
				defaultFile = defaultPath .. options.items[i].icon .. ".png",
				overFile = overPath .. options.items[i].icon .. ".png",
				id = options.items[i].id,
				label = "",
				onRelease = options.items[i].onRelease,
			}
		end

		if options.seprWidth and options.seprWidth > 0 then
			if i <= options.seprNumber then
				separator = display.newRect( 0,0, options.seprWidth, 0.8 )
				separator.y = button.height*0.5
				local cases = {
					["left"] = function()
						separator.anchorX = 0 
						separator.x = -button.width*0.5 + (options.seprPad or 0) 
					end

				}
				cases[options.seprAlign or ""]()
				button:insert( separator )
			end
		end

		button.parent = sidebar
		sidebar:insert( button )
	end

	sidebar.openX = options.left
	sidebar.closeX = -options.width + options.left
	XX=sidebar.x	

	function sidebar:touch( event )
		if event.phase == "began" then
			event.target.isFocus = true
		elseif event.phase == "moved" and event.target.isFocus then
			if event.xStart - event.x >= 10 then
				event.target:close()
				event.target.isFocus = false
			end
		elseif event.phase == "ended" or event.phase == "cancelled" then
			event.target.isFocus = false
		end
		return true
	end

	-- Manual open and close

	function sidebar:close()
		if self.state ~= "on" then return end
		self.lock = true
		local event = { name = "close", target = self }
		
		event.phase = "will"
		options.onClose( event )        

		transition.to( self, { 
			time=options.timeClose, x=-options.width, 
			delta = true, transition=options.transClose,
			delay = options.delayClose,
			onComplete = function()
				event.phase = "did"
				options.onClose( event )
				self.state = "off"
				self.x=XX
				lining.isHitTestable = false
			end

		} )

		transition.to( options.displayGroup, { 
			time=options.timeClose, x=0, 
			delta = true, transition=options.transClose, delay = options.delayClose 
		} )
	end

	function sidebar:open()
		if self.state ~= "off" then return end
		local event = { name = "open", target = self }
		lining.isHitTestable = true        
		
		event.phase = "will"
		options.onOpen( event )        

		transition.to( self, { 
			time=options.timeOpen, x=options.width, 
			delta = true, transition=options.transOpen,
			delay = options.delayOpen,
			onComplete = function()
				self.lock = false
				event.phase = "did"
				options.onOpen( event )
				self.state = "on"
			end             
		} )
		transition.to( options.displayGroup, { 
			time=options.timeOpen, x=0, 
			delta = true, transition=options.transOpen, delay = options.delayOpen
		} )        
	end

	function sidebar:off()
		for i=1, #self.buttons do
			if self.buttons[i].state == "on" then
				event = {
					name = "touch",
					state = "off",
					target = {id = self.buttons[i].id}
				}
				options.event(event)
			end
		end
		for j = 1, self.numChildren do --loop through the parents childeren
			if self[j].state then -- if it's a state button
				self[j]:off() -- turn it off
			end
		end

	end
	
	function sidebar:press( id )
		for i=1, #self.buttons do
			if self.buttons[i].id == id and self.buttons[i].state == "off" then
				local butn = self.buttons[i]
				butn:on()
				butn.options.onTurnOn( butn )
				for j = 1, #self.buttons do --loop through the parents childeren            
					butn = self.buttons[j]
					if butn.state and butn.id~=id then -- if it's a state button
						if butn.state == "on" then 
							butn:off() -- turn it off
							butn.options.onTurnOff( butn )
						end
					end
				end
			end
		end
	end

	function sidebar:attach( params )
		if not params.content then return end
		local content = params.content

		content.anchorChildren = true
		if params.top then
			content.anchorY = 0
			content.y = top_ref + params.top 
		end
		if params.left then
			content.anchorX = 0
			content.x = left_ref + params.left 
		end

		self:insert( content )        
	end    

	-- Stay on top

	function sidebar:keepOnTop()
		self:toFront()
	end

	function lining:touch( event )
		if event.phase == "began" then
			sidebar:close()
		end
		return true
	end

	function lining:tap( event )
		return true
	end	

	lining:addEventListener( "touch", lining )
	lining:addEventListener( "tap", lining )
	sidebar:addEventListener( "touch", sidebar )
	
	return sidebar

end

return M