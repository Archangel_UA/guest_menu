local widget = require "widget"
-- 
local common_lib = require "lib.common_lib"

--  PARAMETRS
-- 		tiles        - table with display objects
--		width        - width of visible pad
--		height       - height of visible pad
--		tileWidth    - if specified, all tiles will be the same width; 
--                     if not specified will be equivalent to tileHeight
--		tileHeight   - if specified, all tiles will be the same height;
--                     if not specified will be equivalent to tileWidth
--      rowTilesNumber     - number of tiles in one row, default value is 3
--		columnTilesNumber  - number of tiles in one column, default is unlimited
--      verticalPad
--      horizontalPad
--      topPad
--      bottomPad
--      leftPad
--      rightPad
--      fitSize          - if true all tiles will be scaled to fill tileWidth and tileHeight
--						   area with preserve aspect ratio;
-- 						   default is false;
--                         not valid if tileWidth or tileHeight is not specified;
--						   

-- not implemented	
	--                       - align is not valid if fitSize value is false
	--      alignX           - "left", "center", "right"; default is "center"
	--      alignY           - "top", "center", "bottom"; default is "center"

--      horizontalScrollDisabled
--      verticalScrollDisabled
--      bgColor

function widget.newTilesGroup( params )
	params = params or {}
	params.tiles = params.tiles or {}
	params.width = params.width or 1 --display.actualContentWidth
	params.height = params.height or 1 --display.actualContentHeight
	params.tileWidth = params.tileWidth or params.tileHeight
	params.tileHeight = params.tileHeight or params.tileWidth
	params.rowTilesNumber = params.rowTilesNumber == 0 and 1 or params.rowTilesNumber
	params.columnTilesNumber = params.columnTilesNumber == 0 and 1 or params.columnTilesNumber
	params.verticalPad = params.verticalPad or 0
	params.horizontalPad = params.horizontalPad or 0
	params.topPad = params.topPad or 0
	params.bottomPad = params.bottomPad or 0 
	params.leftPad = params.leftPad or 0
	params.rightPad = params.rightPad or 0
	params.scrollListener = params.scrollListener or function() end
	-- print(#params.tiles)
	local tgroup = display.newGroup()
	tgroup.anchorChildren = true
	
	local bg = display.newRect( tgroup, 0, 0, params.width, params.height )
	bg:setFillColor( unpack( params.bgColor or {1,0} ) )

	local ref_top = -bg.height*0.5

	local content = display.newGroup()
	tgroup:insert( content )

	local tiles = nil

	local tile_group, aspect, tile_width, tile_height, scale_factor
	if params.tileWidth or params.tileHeight then
		tiles = {}
		for i=1, #params.tiles do
			tiles[#tiles+1] = display.newGroup()
			tile_group = tiles[#tiles]
			display.newContainer( tile_group, params.tileWidth, params.tileHeight )
			if params.fitSize then
				aspect = params.tiles[i].width / params.tiles[i].height
			    tile_height = params.tileHeight
			    tile_width = tile_height*aspect
			    scale_factor = params.tileWidth / tile_width
			    if scale_factor > 1 then
			    	tile_width = tile_width * scale_factor
			    	tile_height = tile_height * scale_factor
			    end
			    params.tiles[i].width = tile_width
			    params.tiles[i].height = tile_height
			    tile_group[1]:insert( params.tiles[i] )					
			else tile_group[1]:insert( params.tiles[i] )
			end
		end
	end

	if not tiles then tiles = params.tiles end

	local collim = params.columnTilesNumber
	local rowlim = params.rowTilesNumber
	if not collim and rowlim then collim = math.ceil( #tiles / rowlim )
	elseif not rowlim and collim then rowlim = math.ceil( #tiles / collim )
	elseif not collim and not rowlim then
		collim = #tiles
		rowlim = 1
	end

	local rows = {}
	local irow = 0
	for i=1, #tiles do
		if collim == 1 or i % collim == 1 then
			if irow == rowlim then break end			
			irow = irow + 1
			rows[irow] = display.newGroup()
			rows[irow]:insert( tiles[i] )
			tiles[i].anchorChildren = true
			tiles[i].anchorX = 0
			tiles[i].x = 0
			tiles[i].anchorY = 0.5
			tiles[i].y = 0			
		else
			tiles[i].anchorChildren = true
			tiles[i].anchorX = 0
			tiles[i].x = tiles[i-1].x + tiles[i-1].width + params.horizontalPad
			tiles[i].anchorY = 0.5
			tiles[i].y = 0			
			rows[irow]:insert( tiles[i] )
		end
	end
	for i=1, #rows do
		rows[i].anchorChildren = true
		rows[i].anchorX = 0
		rows[i].anchorY = 0
		content:insert( rows[i] )
		if i > 1 then
			rows[i].y = rows[i-1].y + rows[i-1].height + params.verticalPad
		end
	end

	local positions = { [1] = 0 }
	-- print(#tiles)
	if #tiles==0 then return tgroup end
	local tile_width = params.tileWidth or tiles[1].width
	local posnum = math.ceil( #tiles / #rows ) - math.modf( params.width / tile_width )
	for i=1, posnum do positions[i+1] = tile_width*i + params.horizontalPad*i 
		-- print( positions[i] )
	end

	local scrollView = {}
	if not params.verticalScrollDisabled or not params.horizontalScrollDisabled then
		scrollView = widget.newScrollView
		{
		    width = params.width - params.leftPad - params.rightPad,
		    height = params.height - params.topPad - params.bottomPad,
		    horizontalScrollDisabled = params.horizontalScrollDisabled,
		 	verticalScrollDisabled = params.verticalScrollDisabled,
		 	hideBackground = true,
		 	--isBounceEnabled = false,
		 	listener = function( event )
		 		display.getCurrentStage():setFocus( nil )
		 		params.scrollListener( event )
		 	end
		}
		tgroup:insert( scrollView, true )
		scrollView:insert( content )
		scrollView.x = params.leftPad - (bg.width - scrollView.width)*0.5
		scrollView.y = params.topPad - (bg.height - scrollView.height)*0.5

		function tgroup:scrollTo( position, options )
			scrollView:scrollTo( position, options )
		end
		function tgroup:scrollToPosition( options )
			scrollView:scrollToPosition( options )
		end
		function tgroup:scrollToNext( direction, options )
			local x, y = scrollView:getContentPosition()
			local new_x = nil
			local limit_x =  content.width*(-1)+params.width

			if direction == "left" then
				for i=1, #positions do 
					if positions[i] > math.abs( x ) then 
						new_x = -positions[i]
						if i == #positions then options.onComplete( { limitReached = "right" } ) end
						break
					end 
				end
				if new_x then
					options.x = new_x
					scrollView:scrollToPosition( options )
				end 


				-- new_x = x - bg.width*0.5 + params.leftPad
				-- if x > limit_x then
				-- 	options.x = new_x
				-- 	scrollView:scrollToPosition( options )
				-- end
				-- if new_x <= limit_x and options.onComplete then
				-- 	options.onComplete( { limitReached = "right" } )
				-- end
			elseif direction == "right" then
				for i=#positions, 1, -1 do 
					if positions[i] < math.abs( x ) then 
						new_x = -positions[i]
						if i == 1 then options.onComplete( { limitReached = "left" } ) end
						break
					end 
				end
				if new_x then
					options.x = new_x
					scrollView:scrollToPosition( options )
				end 

				-- new_x = x + bg.width*0.5 - params.rightPad
				-- if x < 0 then
				-- 	options.x = new_x
				-- 	scrollView:scrollToPosition( options )
				-- end
				-- if new_x >= 0 and options.onComplete then
				-- 	options.onComplete( { limitReached = "left" } )
				-- end				 
			end
		end
	else
		content.anchorChildren = true
		content.anchorY = 0
		content.y = ref_top
		--content.x = _mX 
	end

	return tgroup
end

------------------------------------------------------------------------------------------

local tiles = {}

function tiles:createRestTile( width, height, rest_data )
	if rest_data==nil then
		return
	end
	local bottom_pad = 20
	local photo_height = height - bottom_pad*1.5

	local tile = display.newGroup()
	tile.anchorChildren = true
	tile.pos_id = rest_data.pos_id

	local bg = display.newRect( tile, 0, 0, width, height )
	bg:setFillColor( 1 )

	local ref_top = -bg.height*0.5
	local ref_bottom = bg.height*0.5

	local container = display.newContainer( tile, width, photo_height )
	container.anchorY = 0
	container.y = ref_top

    local pheight = photo_height
    local asp=1
    if rest_data.main_photo then
    	asp=rest_data.main_photo.aspect
    end
    local pwidth = pheight*asp
    local scale_factor = width / pwidth
    if scale_factor > 1 then
    	pwidth = pwidth * scale_factor
    	pheight = pheight * scale_factor
    end
    local a = rest_data.logo_width
    local b = rest_data.logo_height    
--    print("createRestTile",rest_data.main_photo,rest_data.main_photo.name,base_dir,rest_data.logo_width,rest_data.logo_height)
    if rest_data.main_photo then
	    local photo_file = rest_data.main_photo.name
	    local base_dir = rest_data.main_photo.basedir
	    local photo = display.newImageRect( container,
	        photo_file, base_dir,
	        pwidth, pheight
	    )
	    photo.anchorY = 0
	    photo.y = -container.height*0.5
	else 
		a=1280
		b=902
	end
	    local photo_cover = display.newRect( tile, 0, 0, width, photo_height )
	    photo_cover.anchorY = 0
	    photo_cover.y = ref_top
		photo_cover:setFillColor( 0, 0.45 )

    local logo_bg = display.newImageRect( tile,
        "assets/logoBackground.png",
        50, 50
    )
    logo_bg.x = container.x
    logo_bg.y = ref_top + container.height*0.5
--    print("%%%%%%%",rest_data,rest_data.logo_width,rest_data.logo_height)

    local a = rest_data.logo_width or 0
    local b = rest_data.logo_height or 0

    local radius = math.sqrt( math.pow(a,2) + math.pow(b,2) ) / 2
    local scale_factor = (logo_bg.width*0.5) / radius
    print(rest_data.logo_file, rest_data.logo_dir,
        rest_data.logo_width, rest_data.logo_height)
    local logo = display.newImageRect( tile,
        rest_data.logo_file, rest_data.logo_dir,
        rest_data.logo_width, rest_data.logo_height
    )
    if logo then
	    logo:scale( scale_factor, scale_factor )
	    logo.y = logo_bg.y
	end

	if rest_data.com_loyalty_type == 2 or rest_data.com_loyalty_type == 1 then
		local bonus = display.newImageRect( tile,
	        "assets/bonusIconOpaque.png", 18, 18
	    )
	    bonus.x = logo_bg.x + logo_bg.width*0.5 - 4
	    bonus.y = logo_bg.y - logo_bg.height*0.5 + 4
	end
	local y_add=10
	local flDelivery=false
	local flOrder=false
	local flPreorder=false
	if rest_data.menu_type==0 then
		flOrder=true
		flPreorder=true
	elseif rest_data.menu_type==1 then
		flOrder=true
		flPreorder=true
		flDelivery=true
	elseif rest_data.menu_type==2 then
		flOrder=true
	elseif rest_data.menu_type==3 then
		flDelivery=true
	elseif rest_data.menu_type==4 then
		flOrder=true
		flDelivery=true
	elseif rest_data.menu_type==6 then
		flPreorder=true
	end

	if rest_data.distance then
		local ds = display.newText({
			parent = tile,
        	text =  string.format("%.2f", rest_data.distance)..trans('km'),
        	font = "HelveticaNeueCyr-Medium",
        	fontSize = 10,
        	align = "right",
        	x = photo_cover.x+photo_cover.width/2-4,
			y = photo_cover.y+y_add
    	})
    	ds:setFillColor(244/255,206/255,1/255)
    	ds.anchorX = 1
		y_add=y_add+15
	end

	if rest_data.main_photo then
	if flOrder then
		local fork = display.newImageRect( tile,
		        "assets/fork.png", 16, 16
		    )
		fork.x = photo_cover.x+photo_cover.width/2-12
		fork.y = photo_cover.y+y_add
		y_add=y_add+15			
	end
	if flPreorder then
		local clock = display.newImageRect( tile,
		        "assets/clock.png", 16, 16
		    )
		clock.x = photo_cover.x+photo_cover.width/2-12
		clock.y = photo_cover.y+y_add	
		y_add=y_add+15	
	end
	if flDelivery then	
		local delivery = display.newImageRect( tile,
		        "assets/delivery.png", 16, 16
		    )
		delivery.x = photo_cover.x+photo_cover.width/2-12
		delivery.y = photo_cover.y+y_add
		y_add=y_add+15
	end
	if rest_data.pos_bonusme_merchant_id~=nil and rest_data.pos_bonusme_merchant_id~="" then
		local card = display.newImageRect( tile,
		        "assets/card.png", 16, 16
		    )
		card.x = photo_cover.x+photo_cover.width/2-12
		card.y = photo_cover.y+y_add
	end
	end
    local rest_name = display.newText({
        parent = tile,
        text = rest_data.title,
        font = "HelveticaNeueCyr-Light",
        fontSize = 10,
        width = width - 10,
        height = 10 + 3,
        align = "center"
    })
    rest_name:setFillColor( 0 )
    rest_name.y = ref_bottom - bottom_pad*1-1


    local address = display.newText({
        parent = tile,
        text = rest_data.addr,
        font = "HelveticaNeueCyr-Light",
        fontSize = 9,
        width = width - 10,
        height = 10 + 3,
        align = "center"
    })
    address:setFillColor( 0 )
    address.y = ref_bottom - bottom_pad*0.5+2

    --print("balance",userProfile.balance[rest_data.com_id])
	if userProfile.balance[rest_data.com_id]~=nil and userProfile.bonus_types and userProfile.bonus_types[rest_data.com_id]==0  then
	    local bonuses_label = display.newText({
	        parent = tile,
	        text = "+ "..userProfile.balance[rest_data.com_id],
	        font = "HelveticaNeueCyr-Medium",
	        fontSize = 15,
	        width = width - 10,
	        height = -10,
	        align = "left"
	    })
	    bonuses_label:setFillColor(244/255,210/255,27/255 )
	    bonuses_label.y = ref_bottom - bottom_pad-22
	end

    local ratingGroup = common_lib.createRating(rest_data.rating)
    ratingGroup.anchorX=0
    ratingGroup.x=-logo_bg.width-15
    ratingGroup.y=ref_top+7
    tile:insert(ratingGroup)

    tile:addEventListener( "tap", function( event )
	event.target.alpha = 0.5
	timer.performWithDelay( 100, function()
		event.target.alpha = 1
		userProfile.SelectedRestaurantData = rest_data
		userProfile.activeOrderID = 0
		_G.defineUserCity( rest_data.city,rest_data.city_id )
		ldb:setSettValue( "last_rest", rest_data.pos_id )
		CreateSideBar()
		mySidebar:press("about")
		event.target.isFocus = false
	end )
    end)

    -- метод для обновления логотипа
	function tile:updateLogo()
		if  not (logo_bg and logo_bg.width) then return end
		local a = rest_data.logo_width or 0
		local b = rest_data.logo_height or 0
		local radius = math.sqrt( math.pow(a,2) + math.pow(b,2) ) / 2
		local scale_factor = (logo_bg.width*0.5) / radius		
		if logo then logo:removeSelf() end
		logo = display.newImageRect( tile,
			rest_data.logo_file, rest_data.logo_dir,
			rest_data.logo_width, rest_data.logo_height
		)
		if logo then
			logo:scale( scale_factor, scale_factor )
			logo.y = logo_bg.y
		end
	end

    return tile
end

return tiles