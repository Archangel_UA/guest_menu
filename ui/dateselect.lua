-- Date Picker example

local composer = require("composer")
local scene = composer.newScene()
local widget = require("widget")
widget.setTheme("widget_theme_ios")
require("ui.datePickerWheel")

---------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE
-- unless "composer.removeScene()" is called.
---------------------------------------------------------------------------------

-- local forward references should go here
local loaded

-- Add widgets to this table so they get removed automatically.
local widgets = {}
local displayText, datePicker

local function validateDate(m, d, y)
	--[[
	If you want to validate that the user chose a date in the past.
	
	Parameters:
		m : int | string
			Month number (not name!)
		d : int | string
			Day
		y : int | string
			Year
	]]
	local time = os.time({year=y, month=m, day=d})
	if time > os.time() then
		native.showAlert("Date Picker", "You cannot choose a date in the future.", {"OK"})
		return false
	end
	return true
end

-- local function onAccept()
-- 	-- Make sure the scene is fully loaded before we accept button events.
-- 	if not loaded then return true end
	
-- 	local values = datePicker:getValues()
	
-- 	-- CORONA BUG: Sometimes the values can be nil.
-- 	-- This happens when one of the tables stopped scrolling but hasn't "snapped" to a selected index.
-- 	-- Prompt the user to fix the bad column.
-- 	if not values[1] then
-- 		native.showAlert(_M.appName, "Please make sure a month is selected.", {"OK"})
-- 	elseif not values[2] then
-- 		native.showAlert(_M.appName, "Please make sure a day is selected.", {"OK"})
-- 	elseif not values[3] then
-- 		native.showAlert(_M.appName, "Please make sure a year is selected.", {"OK"})
-- 	else
-- 		local valid = validateDate(values[1].index, values[2].value, values[3].value)
		
-- 		-- Do whatever you want with the date now.
-- 		if valid then
-- 			displayText.text = values[1].value .. " " .. values[2].value .. ", " .. values[3].value
-- 		end
-- 	end
	
-- 	return true -- indicates successful touch
-- end

---------------------------------------------------------------------------------
local function createDecisionButtons( listener )
	local dbuttons = display.newGroup()
	dbuttons.anchorChildren = true

	local confirm_sheet_options = {
		width = 62,
		height = 62,
		numFrames = 2,
		sheetContentWidth = 124,
		sheetContentHeight = 62
	}
	local confirm_sheet_file = "assets/confirmButton.png"
	local confirm_sheet = graphics.newImageSheet( confirm_sheet_file, confirm_sheet_options )	

	local confirm = widget.newButton({
		sheet = confirm_sheet,
		defaultFrame = 1,
		overFrame = 2,	    
		onRelease = function( event )
			event.name = "confirm"
			event.group = dbuttons 
			listener( event )
		end
	})
	dbuttons:insert( confirm, true )

	local cancel_sheet_options = {
		width = 62,
		height = 62,
		numFrames = 2,
		sheetContentWidth = 124,
		sheetContentHeight = 62
	}
	local cancel_sheet_file = "assets/cancelButton.png"
	local cancel_sheet = graphics.newImageSheet( cancel_sheet_file, cancel_sheet_options )	

	local cancel = widget.newButton({
		sheet = cancel_sheet,
		defaultFrame = 1,
		overFrame = 2,	    
		onRelease = function( event )
			event.name = "cancel"
			event.group = dbuttons 
			listener( event )
		end
	})
	dbuttons:insert( cancel, true )
	cancel.anchorX = 0
	cancel.x = confirm.width*0.5 + 50

	return dbuttons
end
-- "scene:create()"
function scene:create(event)
	local sceneGroup = self.view
	
	-- Figure out which date to start with.
	local p = event.params or {}
	if p.year==nil then
		p.year=tonumber(1980)
	end
	if p.month==nil then
		p.month=tonumber(01)
	end	
	if p.day==nil then
		p.day=tonumber(01)
	end		
	local currYear = tonumber(p.year or os.date("%Y"))
	local currMonth = tonumber(p.month or os.date("%m"))
	local currDay = tonumber(p.day or os.date("%d"))
	local date_field = event.params.date_field
	local date_data = event.params.date_data
	local myRectangle = display.newRect( _mX, _mY, _X, display.actualContentHeight +display.topStatusBarContentHeight)
	myRectangle:setFillColor( 0.4 )
	--myRectangle.alpha = 0.5
	sceneGroup:insert( myRectangle )
	-- Create the widget.
	datePicker = widget.newDatePickerWheel(currYear, currMonth, currDay)
	widgets[#widgets+1] = datePicker
	
	-- Position it however you like.
	datePicker.anchorChildren = true
	datePicker.anchorX = 0.5
	datePicker.anchorY = 0
	datePicker.x = display.contentCenterX
	datePicker.y = 60
	


		local decbuttons = createDecisionButtons(
			function( event )
				if event.name == "confirm" then
					local values = datePicker:getValues()
					
					if not values[1] then
						native.showAlert("BonusMe", "Please make sure a month is selected.", {"OK"})
					elseif not values[2] then
						native.showAlert("BonusMe", "Please make sure a day is selected.", {"OK"})
					elseif not values[3] then
						native.showAlert("BonusMe", "Please make sure a year is selected.", {"OK"})
					else
						local valid = validateDate(values[1].index, values[2].value, values[3].value)
						
					end	
					local v2=	values[2].value
					local v1=	values[1].index
					if string.len( v2 )<2 then 
						v2="0"..v2
					end	
					if string.len( v1 )<2 then 
						v1="0"..v1
					end						
					date_field.text=values[3].value.."-"..v1.."-"..v2
					date_data = date_field.text
					composer.hideOverlay( "fade", 200 )
				elseif event.name == "cancel" then
					composer.hideOverlay( "fade", 200 )
				end
			end
		)
		sceneGroup:insert( decbuttons )
		decbuttons.x = _mX
		decbuttons.anchorY = 1
		decbuttons.y = _Y - 20

	--sceneGroup:insert(okBtn)
	sceneGroup:insert(datePicker)
end

-- "scene:show()"
function scene:show(event)
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is still off screen (but is about to come on screen).
	elseif phase == "did" then
		-- Called when the scene is now on screen.
		-- Insert code here to make the scene come alive.
		-- Example: start timers, begin animation, play audio, etc.
		loaded = true
	end
end

-- "scene:hide()"
function scene:hide(event)
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
		-- Called when the scene is on screen (but is about to go off screen).
		-- Insert code here to "pause" the scene.
		-- Example: stop timers, stop animation, stop audio, etc.
		loaded = false
	elseif phase == "did" then
		-- Called immediately after scene goes off screen.
	end
end

-- "scene:destroy()"
function scene:destroy(event)
	local sceneGroup = self.view
	
	-- Called prior to the removal of scene's view ("sceneGroup").
	-- Insert code here to clean up the scene.
	-- Example: remove display objects, save state, etc.
	for i=1, #widgets do
		local w = table.remove(widgets)
		if w then
			w:removeSelf()
			w = nil
		end
	end
	widgets = nil
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

---------------------------------------------------------------------------------

return scene