--[[ -------------- INTERFACE -----------------

EnterModule:checkEntered()   -- cheking the user logged already;
								if true returns user data from local database,
								else return nil

em = EnterModule:new( params )    -- create new enter module and retuns it as display group
	params = {
		width = 0,
		height = 0,
		tabBarHeight = 0,
		tabBarFontSize = 0,
		
		lang = "en", -- "ru"    -- not realised
		
		fieldsHeight = 0,       -- in inches
		fieldsWidth = 0,
		fieldsFontSize = { 0, 0 },  -- in inches and pixels; need adjust      
		fieldsTop = 0,
		
		listener = function( event ) end,    
			table "event" has:
			event.phase: "logged" - when user successfully logged 
						 "regged" - when user successfully registered
						 "back"   - when user pressed back button
			event.data - all data about user; 
						 if event.phase is "back", then event.data is nil
	}

em:remove()             -- delete created enter module from memory

--------------------------------------------------]]

require "ui.SlideView"
require "ui.InputField"
local widget = require "widget"
-- 
local rdb = require "lib.db_remote" 
local rdb_common = require "lib.db_remote_common" 
local XorCipher = require "lib.XorCipher"
local json = require( "json" )
local facebook = require( "plugin.facebook.v4a" )
--FB block
local fbCommand			-- forward reference
local LOGIN = 0
local LOGOUT = 1
local SHOW_REQUEST_DIALOG = 2
local SHARE_LINK_DIALOG = 3
local POST_MSG = 4
local POST_PHOTO = 5
local GET_USER_INFO = 6
local PUBLISH_INSTALL = 7
--

local EnterModule = {}

local function unloadSlide( slide )
end

local function showSlide( slide, slideView )
	local num = slide.index_number
	slideView:setTabLabel( num, nil, {0,0,0, 0.7} )
end

local function hideSlide( slide, slideView )
	local num = slide.index_number
	slideView:setTabLabel( num, nil, {0,0,0, 0.3} )
end

local function reachedFirst( slide )
end
local function reachedLast( slide )
end

function EnterModule:checkEntered()
	local existdata = ldb:getGuestData()
	if existdata then return existdata
	end
	return nil	
end

function EnterModule:new( params )
	local nem = display.newGroup()

	params.listener = params.listener or function() end
	params.width = params.width or display.actualContentWidth
	params.height = params.height or display.actualContentHeight
	params.tabBarHeight = params.tabBarHeight + 7 or 0
	params.tabBarFont = params.tabBarFont or native.systemFont
	params.tabBarFontSize = params.tabBarFontSize or params.tabBarHeight*0.2

	local function loadSlide( slide, slideView )
		local num = slide.index_number
		local sheight = slide.actualHeight
		local pad = params.fieldsTop or 0
		
		slide.params = {
			autoscale = false,
			hasBg = true,
			bgColor = { 228/255, 228/255, 228/255 },
		}
		
		if num == 1 then
			nem.login = newLoginScreen( params )
			nem.login.y = (sheight*0.5 - nem.login.height*0.5 - pad) * (-1)
			slide:insert( nem.login )		
	
		elseif num == 2 then
			nem.registry = newRegistryScreen( params )		
			nem.registry.y = (sheight*0.5 - nem.registry.height*0.5 - pad) * (-1)
			slide:insert( nem.registry )
		end
	end

	nem.slideView = widget.newSlideView({ 
		y = display.contentCenterY + params.tabBarHeight*0.5,
		width = params.width,
		height = params.height - params.tabBarHeight,
		numSlides = 2,
		initSlide = 1,
		timeMove = 400,

		loadSlide = loadSlide,
		unloadSlide = unloadSlide,
		showSlide = showSlide,
		hideSlide = hideSlide,
		reachedFirst = reachedFirst,
		reachedLast = reachedLast
	})
	nem:insert( nem.slideView )

	if params.tabBarHeight ~= 0 then
		local font = params.tabBarFont
		local fs = params.tabBarFontSize
		local tw = params.width*0.5
		local th = params.tabBarHeight
		local translat = translations.EnterModuleTxt_Tabs[userProfile.lang]
	    local bgactive = {}
	    local bgadef = {}
    	local sepr = nil		
		local tabs = {}

		for i=1,2 do
	        bgactive = display.newGroup()
	        bgactive.anchorChildren = true
	        display.newRect( bgactive, 0, 0, tw, th+2 )
	        bgactive[1]:setFillColor( 228/255, 228/255, 228/255 )
	        display.newImageRect( bgactive, "assets/orders/tabPointer.png", tw, 7 )
	        bgactive[2]:setFillColor( 0,0,0, 0.2 )
	        bgactive[2].anchorY = 1
	        bgactive[2].y = th*0.5

	        bgadef = display.newGroup()
	        bgadef.anchorChildren = true
	        display.newRect( bgadef, 0, 0, tw, th+2 )
	        bgadef[1]:setFillColor( 228/255, 228/255, 228/255 )
	        display.newImageRect( bgadef, "assets/orders/tabBottom.png", tw, 7 )
	        bgadef[2]:setFillColor( 0,0,0, 0.2 )
	        bgadef[2].anchorY = 1
	        bgadef[2].y = th*0.5

	        sepr = nil
	        if i > 1 then sepr = display.newRect( 0, -2, 1, th-7 ) end  

			tabs[i] = {
				label = display.newText( translat[i], 0, -3, font, fs ),			
	            labelColor = { 0,0,0, 0.3 },
	            bgDefault = bgadef,        
	            bgActive = bgactive,
	            separator = sepr,
	            seprColor = { 0,0,0, 0.2 }   
			}
		end
		
		nem.slideView:attachTabBar(
			{
				y = -nem.slideView.height*0.5 - th*0.5+1,          
				height = th+2,
				--bgDefColor = { 228/255, 228/255, 228/255 },
				--bgActColor = { 228/255, 228/255, 228/255 }
			},
			tabs
		)
		nem.slideView:setTabLabel( 1, nil, {0,0,0, 0.7} )
	end

	function nem:key( event )
	    if event.keyName == "back" and system.getInfo("platformName") == "Android" then
	        params.listener( { phase = "back" } )
	        native.setKeyboardFocus( nil )
	        return true
	    end
	    return false
	end
	--Runtime:addEventListener( "key", nem )

	function nem:remove()
		Runtime:removeEventListener( "key", self )
		nem:removeSelf()
	end	

	function nem:setCountry( country )
		-- if country.lang_code then
		-- 	nem.registry.f_country.text = _G.CountriesList[country.lang_code][_G.Lang] 
		-- else
		-- 	nem.registry.f_country.text = country.name
		-- end
		nem.registry.f_country.text = country.cntr_name
		nem.registry.f_country.cntr_id =  country.cntr_id 
		country.cntr_ph_code = country.cntr_ph_code or ""
		nem.registry.f_tel:setText(country.cntr_ph_code)
	end	

	function nem:request( request )
		if request == "registration" then nem.registry:sendNetRequest( true ) 
		end
	end

	return nem
end

-------------------------------------------------------------------------------
				          -- LOGIN SCREEN --
-------------------------------------------------------------------------------

function newLoginScreen( params )
	local _Xa = display.actualContentWidth
	local _Ya = display.actualContentHeight
	local border = 1
	local brcolor= {0,0,0, 0.2}
	local bgcolor= {0,0,0, 0.1}
	
	local fh = params.fieldsHeight or 0
	local fw = params.fieldsWidth or 0
	local fs = params.fieldsFontSize or 0
	local font = params.fieldsFont
	local row_height = params.rowHeight or 40

	local ls = display.newGroup()
	
	ls.data = {}
	ls.datavalid = { is = false, count = {}, mess = {} }
	ls.listener = params.listener
	
	ls.rows = {}
	ls.rowtop = 0
	ls.rowbottom = row_height
	ls.rowleft = 0
	ls.rowcenter = row_height*0.5
	
	ls.messbox = display.newGroup()
	ls:insert( ls.messbox )

	ls.messages = {
		email = translations["enter_messages_type"][_G.Lang][1],
		password = translations["enter_messages_min"][_G.Lang][5],
		empty = translations["enter_messages_login"][_G.Lang][1],
		["error"] = translations["enter_messages"][_G.Lang][5],
		denied = translations["enter_messages_login"][_G.Lang][2],
		gotpassword = translations["enter_messages_login"][_G.Lang][3],
		nosuchemail = translations["enter_messages_login"][_G.Lang][4],
		disconnection = translations["enter_messages"][_G.Lang][7],
		fbemail = translations["enter_messages"][_G.Lang][8]
	}

	ls.rows[1] = display.newGroup()
	ls:insert( ls.rows[1] )
	ls.rows[1].anchorChildren = true

	ls.rows[1].icon = display.newImageRect( "assets/emailIcon.png", 20, 18 )
	ls.rows[1]:insert( ls.rows[1].icon )
	ls.rows[1].icon.anchorX = 0
	ls.rows[1].icon.x = ls.rowleft
	ls.rows[1].icon.y = ls.rowcenter - 2 

	ls.f_email = widget.newInputField({
		id = "email",
		x = 0,
		y = 0,
		width = fw,
		height = fh,
		font = font[1],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = 0,
		placeholder = "Email",
		inputType = "alpha",--"email",
		typeControl = false,
		symbolsMin = 1,

		inputFont = font[2],
		inputOffsetX = -1, -- -0.035,
		inputOffsetY = -2, -- 0.02,

		listener = function( event )
			ls:userInput( event )
		end
	})
	ls.f_email:setDecor({
		allBorders = false,
		--bgColor = bgcolor,		
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	ls.f_email.x = ls.rowleft + ls.rows[1].icon.width + ls.f_email.width*0.5 + 10
	ls.f_email.y = ls.rowcenter
	ls.rows[1]:insert( ls.f_email )

	ls.rows[2] = display.newGroup()
	ls:insert( ls.rows[2] )
	ls.rows[2].anchorChildren = true
	ls.rows[2].anchorY = 0
	ls.rows[2].y = ls.rows[1].y + ls.rows[1].height*0.5 + 8

	ls.rows[2].icon = display.newImageRect( "assets/keyIcon.png", 18, 20 )
	ls.rows[2]:insert( ls.rows[2].icon )
	ls.rows[2].icon.anchorX = 0
	ls.rows[2].icon.x = ls.rowleft
	ls.rows[2].icon.y = ls.rowcenter - 2 

	local secureDotSheet = graphics.newImageSheet( "assets/passwordDot.png", {
	    width = 7,
	    height = 7,
	    numFrames = 1,
	    sheetContentWidth = 7,
	    sheetContentHeight = 7
	})	
	
	ls.f_password = widget.newInputField({
		id = "password",
		x = 0,
		y = 0,
		width = fw,
		height = fh,
		font = font[1],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = -0.01,
		placeholder = translations["password"][_G.Lang],
		inputType = "password",
		secureDotSheet = secureDotSheet,
		symbolsMin = 5,

		inputFont = font[2],
		inputOffsetX = -4,
		inputOffsetY = 2,
		
		listener = function( event )
			ls:userInput( event )
		end	
	})
	ls.f_password:setDecor({
		allBorders = false,
		--bgColor = bgcolor,		
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	ls.f_password.x = ls.rowleft + ls.rows[2].icon.width + ls.f_password.width*0.5 + 10
	ls.f_password.y = ls.rowcenter
	ls.rows[2]:insert( ls.f_password )
	
	ls.butnlog = widget.newButton({
	    label = translations["EnterModuleTxt_Tabs"][_G.Lang][1],
        labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0 } },
        fontSize = 16,
        font = "HelveticaNeueCyr-Medium",
	    onPress = function( event )
	    	ls:buttonPress()
	    end,
        --properties for a rounded rectangle button...
        shape="roundedRect",
        width = 200,
        height = 37,
        cornerRadius = 5,
        fillColor = { default={ 99/255, 218/255, 153/255 }, over={ 211/255,211/255,211/255 } },
        strokeColor = { default={ 99/255, 218/255, 153/255 }, over={ 0,0,0, 0.13 } },
        strokeWidth = 2
	})
	ls.butnlog.y = ls.rows[2].y + ls.butnlog.height + 70
	ls.butnlog.x = ls.x
	ls:insert( ls.butnlog )

	ls.butnpass = widget.newButton({
	    label = translations["remind_password"][_G.Lang],
        labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0 } },
        fontSize = 16,
        font = "HelveticaNeueCyr-Medium",
	    onPress = function( event )
	    	ls:buttonPassPress()
	    end,
        --properties for a rounded rectangle button...
        shape="roundedRect",
        width = 200,
        height = 37,
        cornerRadius = 5,
        fillColor = { default={ 99/255, 218/255, 153/255 }, over={ 211/255,211/255,211/255 } },
        strokeColor = { default={ 99/255, 218/255, 153/255 }, over={ 0,0,0, 0.13 } },
        strokeWidth = 2
	})
	ls.butnpass.y = ls.rows[2].y + ls.butnpass.height + 65*2
	ls.butnpass.x = ls.x
	ls:insert( ls.butnpass )

	ls.butnfacebook = widget.newButton({
	    label = translations["facebook"][_G.Lang],
        labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0 } },
        fontSize = 16,
        font = "HelveticaNeueCyr-Medium",
	    onPress = function( event )
	    	ls:buttonFacebookPress()
	    end,
        --properties for a rounded rectangle button...
        shape="roundedRect",
        width = 200,
        height = 37,
        cornerRadius = 5,
        fillColor = { default={ 99/255, 218/255, 153/255 }, over={ 211/255,211/255,211/255 } },
        strokeColor = { default={ 99/255, 218/255, 153/255 }, over={ 0,0,0, 0.13 } },
        strokeWidth = 2
	})
	ls.butnfacebook.y = ls.rows[2].y + ls.butnfacebook.height + 65*3
	ls.butnfacebook.x = ls.x
	ls:insert( ls.butnfacebook )
	ls.fb_icon = display.newImageRect( "icons/fb_icon.png", 26,26 )
	ls.fb_icon.x = ls.butnfacebook.x-77
	ls.fb_icon.y = ls.butnfacebook.y
	ls:insert( ls.fb_icon )

	-- ls.butntmp = widget.newButton({
	--     label = 'send report',
 --        labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0 } },
 --        fontSize = 16,
 --        font = "HelveticaNeueCyr-Medium",
	--     onPress = function( event )
	--     	k=0
	--     	tt=""
	-- 		while k<#_debugStr do
	-- 			k=k+1
	-- 			tt=tt..tostring(_debugStr[k]).."\n"
	-- 			if k/50==math.round( k/50 ) then
	--     			rdb_common:sendEmail{listener=nil,error_flag=1,email="bugs@smarttouchpos.eu",email_text=tt}
	-- 			end	
	-- 		end
	-- 		rdb_common:sendEmail{listener=nil,error_flag=1,email="bugs@smarttouchpos.eu",email_text=tt}	    	
	--     end,

 --        --properties for a rounded rectangle button...
 --        shape="roundedRect",
 --        width = 200,
 --        height = 37,
 --        cornerRadius = 5,
 --        fillColor = { default={ 99/255, 218/255, 153/255 }, over={ 211/255,211/255,211/255 } },
 --        strokeColor = { default={ 99/255, 218/255, 153/255 }, over={ 0,0,0, 0.13 } },
 --        strokeWidth = 2
	-- })
	-- ls.butntmp.y = ls.rows[2].y + ls.butntmp.height + 65*4
	-- ls.butntmp.x = ls.x
	-- ls:insert( ls.butntmp )
	-------------------------------------------------------------------------

	function ls:userInput( event )
		local ierror = event.inputError
		local id = event.target.id
		local text = event.text

		if event.phase == "ended" then
			self:dataValid( ierror, id, text )
		elseif event.phase == "submitted" then
			self:dataValid( ierror, id, text )
			self:sendNetRequest()
		end
	end

	function ls:dataValid( ierror, id, text )
	--_print( "ls:dataValid..." )
		self.datavalid.is = false
		local count = self.datavalid.count
		local mess = self.datavalid.mess

		if ierror == "type" then
			count[id] = 0
			mess[id] = true
		elseif ierror == "min" then
			count[id] = 0
			mess[id] = true		
		elseif not ierror then 
			count[id] = 1
			self.data[id] = text
			mess[id] = nil
		end

		local control = 0
		for k,v in pairs( count ) do control = control + v end
		if control == 2 then 
			self.datavalid.is = true
			self.datavalid.mess = {}
		end	
	end

	function ls:sendNetRequest()
	--_print( "ls:sendNetRequest..." )
		if self.datavalid.is then
			Spinner:start()
			rdb:requestToLogin( self.data, function( status, data )
				Spinner:stop()
				self:handleNetRespond( status, data )
			end )
		else self:sendMessage()
		end
	end

	function ls:buttonPress()
		native.setKeyboardFocus( nil )
		timer.performWithDelay( 300, function()
			self:sendNetRequest()
		end )
	end

	function ls:buttonPassPress()
		native.setKeyboardFocus( nil )
		printResponse(self.data)
		_print(self.data.email)
		if self.data.email ==nil then
			print("!!!!!!!!SSSSSS")
			self:sendMessage( "email" )
		else
			timer.performWithDelay( 300, function()
				Spinner:start()
				rdb:restorePassword( self.data, function( status, data )
					Spinner:stop()
					self:handleNetRespond( status, data )
				end )
			end )
		end
	end

	-- Check for an item inside the provided table
	-- Based on implementation at: https://www.omnimaga.org/other-computer-languages-help/(lua)-check-if-array-contains-given-value/
	local function inTable( t, item )
		for k,v in pairs( t ) do
			if v == item then
				return true
			end
		end
		return false
	end


	-- Table _printing function for Facebook debugging
	local function printTable( t )
		_print("_printing table", debugOutput )
		if type( t ) ~= "table" then
			_print("WARNING: attempt to _print a non-table. Table expected, got " .. type( t ) )
			return
		end
		if ( debugOutput == false ) then 
			return 
		end
		_print("--------------------------------")
		_print( json.prettify( t ) )
	end

	-- Runs the desired facebook command
	local function processFBCommand( )

		local response = {}


		response = facebook.request("me", "GET", {fields = "id,name,email,picture"})

			-- facebook.request( "me/friends" )		-- Alternate request
		if response~=nil then
--			printTable( response )		
		end
	end

	-- New Facebook Connection listener
	local function FBlistener( event )

		-- Debug Event parameters _printout --------------------------------------------------
		-- _prints Events received up to 20 characters. _prints "..." and total count if longer
		_print( "Facebook Listener events:" )
		
		local maxStr = 20		-- set maximum string length
		local endStr
		
		for k,v in pairs( event ) do
			local valueString = tostring(v)
			if string.len(valueString) > maxStr then
				endStr = " ... #" .. tostring(string.len(valueString)) .. ")"
			else
				endStr = ")"
			end
			_print( "   " .. tostring( k ) .. "(" .. tostring( string.sub(valueString, 1, maxStr ) ) .. endStr )
		end
		-- End of debug Event routine -------------------------------------------------------

	    _print( "event.name", event.name ) -- "fbconnect"
	    _print( "event.type:", event.type ) -- type is either "session" or "request" or "dialog"
		_print( "isError: " .. tostring( event.isError ) )
		_print( "didComplete: " .. tostring( event.didComplete ) )
		_print( "response: " .. tostring( event.response ) )
		debug_print("Response ",tostring(event.name).." "..tostring(event.response))
		-----------------------------------------------------------------------------------------
		-- Process the response to the FB command
		-- Note: If the app is already logged in, we will still get a "login" phase
		-----------------------------------------------------------------------------------------
	    if ( "session" == event.type ) then
	    	_print("Session")
		--	statusMessage.text = event.phase  -- "login", "loginFailed", "loginCancelled", or "logout"

			_print( "Session Status: " .. event.phase )
			
			if ( event.phase ~= "login" ) then
				-- Exit if login error
				return
			else
				-- Run the desired command
				processFBCommand()
			end

	    elseif ( "request" == event.type ) then
	    	_print("request")
	        local response = event.response  -- This is a JSON object from the Facebook server

			if ( event.isError==false ) then
				--native.showAlert( "Facebook success login", event.response, { "OK" } )
			  response = json.decode( event.response )
				local user_data={}	
				user_data.fb_login=true
				user_data.email=response.email
				user_data.name = _G.utf8.match( response.name or "", "%S+" ) or ""
				user_data.surname = _G.utf8.match( response.name or "", "%s(%S+)" ) or ""
				user_data.tel=""
				user_data.fullname=response.name
				user_data.password=tostring( math.random(9))..tostring( math.random(0,9))..tostring( math.random(9))..tostring( math.random(9))..tostring( math.random(9))
				--if response.picture and response.picture.data then
				--	display.loadRemoteImage( response.picture.data.url, "GET",function() end,"userAvatarPhoto.jpg", system.DocumentsDirectory)
				--end
				--response.picture.data.url
				--printTable( response )
			--	printTable( user_data )

				_print("Before login send")
				debug_print("Before login send",user_data.email)
				if user_data.email==nil then
					ls:sendMessage( "fbemail" )
					facebook.logout()
				else
					Spinner:start()
					rdb:requestToRegistry( user_data, function( status, data )
						Spinner:stop()
						ls:handleNetRespond( status, data )
					end )	
				end
				--_print( "Facebook Command: " ,fbCommand )

		  --       if ( fbCommand == GET_USER_INFO ) then
				-- 	--statusMessage.text = response.name
				-- 	printTable( response )
				-- else
				-- 	--statusMessage.text = "(unknown)"
				-- end
	        else
			--	printTable( event.response )
				--native.showAlert( "Facebook error", event.response, { "OK" } )
			end

	    end
	end

	
	local function enforceFacebookLogin( )
		if facebook.isActive then
			local accessToken = facebook.getCurrentAccessToken()
			print("accessToken",accessToken)
	--		printResponse(accessToken.grantedPermissions)
			if accessToken == nil then

				_print( "Need to log in" )
				debug_print("0","Need to log in")
				facebook.login({"email", "public_profile" })

			-- Get publish_actions permission only if we're not getting user info or issuing a game request.
			elseif (not inTable( accessToken.grantedPermissions, "email" ))
				and fbCommand ~= GET_USER_INFO
				and fbCommand ~= SHOW_REQUEST_DIALOG then
			--	_print(inTable( accessToken.grantedPermissions, "publiс_profile" ),inTable( accessToken.grantedPermissions, "email" ))
				_print( "Logged in, but need permissions" )
				debug_print("1","Logged in, but need permissions ")
				printTable( accessToken )
				facebook.login( {"email", "publish_actions" } )

			else

				_print( "Already logged in with needed permissions",accessToken.grantedPermissions )
				debug_print("2","Already logged in with needed permissions")
			--	printTable( accessToken )
				--printTable( accessToken )
				--facebook.login( {"email", "publish_actions" } )

				processFBCommand()

			end
		else
			_print( "Please wait for facebook to finish initializing before checking the current access token" );
		end
	end

	local function facebookListener( event )
	 
	    if ( "fbinit" == event.name ) then
	 
	        print( "Facebook initialized" )
	 
	        -- Initialization complete; share a link
	        --shareLink( "https://www.coronalabs.com/" )
	 
	    elseif ( "fbconnect" == event.name ) then
	 
	        if ( "session" == event.type ) then
	            -- Handle login event and try to share the link again if needed
	        elseif ( "dialog" == event.type ) then
	            -- Handle dialog event
	        end
	    end
	end
	 
	-- Set the "fbinit" listener to be triggered when initialization is complete
	facebook.init( facebookListener )

	-- Set the FB Connect listener to use throughout the app.
	facebook.setFBConnectListener( FBlistener )

	function ls:buttonFacebookPress()
		native.setKeyboardFocus( nil )
		enforceFacebookLogin()
	end	

	function ls:removeMessBox()
		self.messbox:removeSelf()
		self.messbox = { numChildren = 0 }
	end

	function ls:sendMessage( tag )
		local ypos = self.butnlog.y - self.butnlog.height
		local mess = ""
	--	_print("2!!!!!!!!!!!!!!!!!!!!!!!")
		if not tag then
			for k,v in pairs( self.datavalid.mess ) do
				if v then mess = self.messages[k]
					break
				end
			end
		else mess = self.messages[tag]
		end

		if mess == "" then mess = self.messages["empty"] end	
		if mess==nil then mess="" end
		if self.messbox.numChildren > 0 then 
			timer.cancel( self.messbox.timer_handle )
			self.messbox:removeSelf() 
		end

		self.messbox = display.newGroup()
		self:insert( self.messbox )
		local messbox = self.messbox 

        local text = display.newText({
            parent = messbox,
            text = mess,     
            x = 0,
            y = 0,
            width = _Xa*0.7,
            font = "HelveticaNeueCyr-Medium",   
            fontSize = 14
        })
        text:setFillColor( 1, 1, 1 )

        local box = display.newRoundedRect( messbox, 
            0, 0, 
            text.width + 30, text.height + 30, 
            15 
        )
        box:setFillColor( 0,0,0, 0.8 )
        box:toBack()

		self.messbox.timer_handle = timer.performWithDelay( 2000, function()
			self:removeMessBox()
		end )
	end

	function ls:handleNetRespond( status, data )
		_print("ls:handleNetRespond")
		local listener = self.listener
		local ypos = self.butnlog.y - self.butnlog.height

		if status == "accepted" then
			--self:sendMessage( "You logged as "..data.guest_fullname, ypos )
			ldb:saveGuestData( data )
			local event = { target = self, phase = "logged", data = data }
			local rdb_bonus = require( "lib.db_remote_bonuses" )
			rdb_bonus:downloadQRCode("qr.jpg")
			userProfile.balance = {}
			listener( event )
		else self:sendMessage( status )
		end
	end

	return ls
end


-------------------------------------------------------------------------------
				          -- REGISTRY SCREEN --
-------------------------------------------------------------------------------

function newRegistryScreen( params  )
	local _Xa = display.actualContentWidth
	local _Ya = display.actualContentHeight
	local border = 1
	local brcolor= {0,0,0, 0.2}
	local bgcolor= {0,0,0, 0.1}
	local step = 8
	local nexti = 1

	local fh = params.fieldsHeight or 0
	local fw = params.fieldsWidth or 0
	local fs = params.fieldsFontSize or 0
	local font = params.fieldsFont
	local row_height = params.rowHeight or 40	

	local rs = display.newGroup()

	rs.listener = params.listener
	rs.data = {}
	rs.datavalid = { is = false, count = {}, mess = {} }

	rs.rows = {}
	rs.rowtop = 0
	rs.rowbottom = row_height
	rs.rowleft = 0
	rs.rowcenter = row_height*0.5

	rs.messbox = display.newGroup()
	rs:insert( rs.messbox )

	rs.messages = {
		notMatch = translations["enter_messages"][_G.Lang][1],
		exist = translations["enter_messages"][_G.Lang][2],
		incorrect = translations["enter_messages"][_G.Lang][3],
		reged = translations["enter_messages"][_G.Lang][4],
		["error"] = translations["enter_messages"][_G.Lang][5],
		empty = translations["enter_messages"][_G.Lang][6],
		disconnection = translations["enter_messages"][_G.Lang][7]		
	}
	rs.messages.type = {
		email = translations["enter_messages_type"][_G.Lang][1],
		name = translations["enter_messages_type"][_G.Lang][2],
		surname = translations["enter_messages_type"][_G.Lang][3],
		tel = translations["enter_messages_type"][_G.Lang][4],
	}
	rs.messages.min = {
		email = translations["enter_messages_min"][_G.Lang][1],
		name = translations["enter_messages_min"][_G.Lang][2],
		surname = translations["enter_messages_min"][_G.Lang][3],
		tel = translations["enter_messages_min"][_G.Lang][4],
		password = translations["enter_messages_min"][_G.Lang][5],
		repassw = translations["enter_messages_min"][_G.Lang][6],
	}	

	rs.rows[nexti] = display.newGroup()
	rs:insert( rs.rows[nexti] )
	rs.rows[nexti].anchorChildren = true

	rs.rows[nexti].icon = display.newImageRect( "assets/emailIcon.png", 20, 18 )
	rs.rows[nexti]:insert( rs.rows[nexti].icon )
	rs.rows[nexti].icon.anchorX = 0
	rs.rows[nexti].icon.x = rs.rowleft
	rs.rows[nexti].icon.y = rs.rowcenter - 2 

	rs.f_email = widget.newInputField({
		id = "email",
		x = 0,
		y = 0,
		width = fw,
		height = fh,
		font = font[1],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = 0,
		placeholder = "email",
		inputType = "email",
		typeControl = true,
		symbolsMin = 1,		
		
		inputFont = font[2],
		inputOffsetX = -1,
		inputOffsetY = -2,		

		listener = function( event )
			rs:userInput( event )
		end		
	})
	rs.f_email:setDecor({
		allBorders = false,
		--bgColor = bgcolor,		
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	rs.f_email.x = rs.rowleft + rs.rows[nexti].icon.width + rs.f_email.width*0.5 + 10
	rs.f_email.y = rs.rowcenter
	rs.rows[nexti]:insert( rs.f_email )

	nexti = nexti + 1
	rs.rows[nexti] = display.newGroup()
	rs:insert( rs.rows[nexti] )
	rs.rows[nexti].anchorChildren = true
	rs.rows[nexti].anchorY = 0
	rs.rows[nexti].y = rs.rows[nexti-1].y + rs.rows[nexti-1].height*0.5 + step

	rs.rows[nexti].icon = display.newImageRect( "icons/default/user.png", 32*0.75, 32*0.75 )
	rs.rows[nexti]:insert( rs.rows[nexti].icon )
	rs.rows[nexti].icon.anchorX = 0
	rs.rows[nexti].icon.x = rs.rowleft
	rs.rows[nexti].icon.y = rs.rowcenter - 2
	
	rs.f_name = widget.newInputField({
		id = "name",
		x = 0,
		y = 0,
		width = fw,
		height = fh,
		font = font[1],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = 0,
		placeholder = translations["first_name"][_G.Lang],
		inputType = "alpha",
		typeControl = true,
		symbolsMin = 1,
		
		inputFont = font[2],
		inputOffsetX = -1,
		inputOffsetY = -2,		

		listener = function( event )
			rs:userInput( event )
		end	
	})
	rs.f_name:setDecor({
		allBorders = false,
		--bgColor = bgcolor,		
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	rs.f_name.x = rs.rowleft + rs.rows[nexti].icon.width + rs.f_name.width*0.5 + 10
	rs.f_name.y = rs.rowcenter
	rs.rows[nexti]:insert( rs.f_name )

	nexti = nexti + 1
	rs.rows[nexti] = display.newGroup()
	rs:insert( rs.rows[nexti] )
	rs.rows[nexti].anchorChildren = true
	rs.rows[nexti].anchorY = 0
	rs.rows[nexti].y = rs.rows[nexti-1].y + rs.rows[nexti-1].height + step

	rs.rows[nexti].icon = display.newImageRect( "icons/default/user.png", 32*0.75, 32*0.75 )
	rs.rows[nexti]:insert( rs.rows[nexti].icon )
	rs.rows[nexti].icon.anchorX = 0
	rs.rows[nexti].icon.x = rs.rowleft
	rs.rows[nexti].icon.y = rs.rowcenter - 2

	rs.f_surname = widget.newInputField({
		id = "surname",
		x = 0,
		y = 0,
		width = fw,
		height = fh,
		font = font[1],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = 0,
		placeholder = translations["last_name"][_G.Lang],
		inputType = "alpha",
		typeControl = true,
		symbolsMin = 1,
		
		inputFont = font[2],
		inputOffsetX = -1,
		inputOffsetY = -2,		

		listener = function( event )
			rs:userInput( event )
		end	
	})
	rs.f_surname:setDecor({
		allBorders = false,
		--bgColor = bgcolor,		
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	rs.f_surname.x = rs.rowleft + rs.rows[nexti].icon.width + rs.f_surname.width*0.5 + 10
	rs.f_surname.y = rs.rowcenter
	rs.rows[nexti]:insert( rs.f_surname )

	
	nexti = nexti + 1
	rs.rows[nexti] = display.newGroup()
	rs:insert( rs.rows[nexti] )
	rs.rows[nexti].anchorChildren = true
	rs.rows[nexti].anchorY = 0
	rs.rows[nexti].y = rs.rows[nexti-1].y + rs.rows[nexti-1].height + step

	rs.rows[nexti].icon = display.newImageRect( "assets/phoneIcon.png", 20, 18 )
	rs.rows[nexti]:insert( rs.rows[nexti].icon )
	rs.rows[nexti].icon.anchorX = 0
	rs.rows[nexti].icon.x = rs.rowleft
	rs.rows[nexti].icon.y = rs.rowcenter - 2 

	rs.f_tel = widget.newInputField({
		id = "tel",
		x = 0,
		y = 0,
		width = fw,
		height = fh,
		font = font[1],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = 0,
		placeholder = translations["phone"][_G.Lang],
		-- inputType = "number",
		typeControl = true,
		symbolsMin = 1,		
		
		inputFont = font[2],
		inputOffsetX = -1,
		inputOffsetY = -2,		

		listener = function( event )
			rs:userInput( event )
		end	
	})
	rs.f_tel:setDecor({
		border = border,
		bColor = bcolor
	})
	rs.f_tel:setDecor({
		allBorders = false,
		--bgColor = bgcolor,		
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	rs.f_tel.x = rs.rowleft + rs.rows[nexti].icon.width + rs.f_tel.width*0.5 + 10
	rs.f_tel.y = rs.rowcenter
	rs.rows[nexti]:insert( rs.f_tel )	

	local secureDotSheet = graphics.newImageSheet( "assets/passwordDot.png", {
	    width = 7,
	    height = 7,
	    numFrames = 1,
	    sheetContentWidth = 7,
	    sheetContentHeight = 7
	})	

	nexti = nexti + 1
	rs.rows[nexti] = display.newGroup()
	rs:insert( rs.rows[nexti] )
	rs.rows[nexti].anchorChildren = true
	rs.rows[nexti].anchorY = 0
	rs.rows[nexti].y = rs.rows[nexti-1].y + rs.rows[nexti-1].height + step

	rs.rows[nexti].icon = display.newImageRect( "assets/keyIcon.png", 18, 20 )
	rs.rows[nexti]:insert( rs.rows[nexti].icon )
	rs.rows[nexti].icon.anchorX = 0
	rs.rows[nexti].icon.x = rs.rowleft
	rs.rows[nexti].icon.y = rs.rowcenter - 2 

	rs.f_password = widget.newInputField({
		id = "password",
		x = 0,
		y = 0,
		width = fw,
		height = fh,
		font = font[1],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = -0.01,
		placeholder = translations["password"][_G.Lang],
		inputType = "password",
		secureDotSheet = secureDotSheet,
		symbolsMin = 5,		
		
		inputFont = font[2],
		inputOffsetX = -0.035,
		inputOffsetY = 0.02,		

		listener = function( event )
			rs:userInput( event )
		end		
	})
	rs.f_password:setDecor({
		allBorders = false,
		--bgColor = bgcolor,		
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	rs.f_password.x = rs.rowleft + rs.rows[nexti].icon.width + rs.f_password.width*0.5 + 10
	rs.f_password.y = rs.rowcenter
	rs.rows[nexti]:insert( rs.f_password)

	nexti = nexti + 1
	rs.rows[nexti] = display.newGroup()
	rs:insert( rs.rows[nexti] )
	rs.rows[nexti].anchorChildren = true
	rs.rows[nexti].anchorY = 0
	rs.rows[nexti].y = rs.rows[nexti-1].y + rs.rows[nexti-1].height + step

	rs.rows[nexti].icon = display.newImageRect( "assets/keyIcon.png", 18, 20 )
	rs.rows[nexti]:insert( rs.rows[nexti].icon )
	rs.rows[nexti].icon.anchorX = 0
	rs.rows[nexti].icon.x = rs.rowleft
	rs.rows[nexti].icon.y = rs.rowcenter - 2 

	rs.f_reppassw = widget.newInputField({
		id = "repassw",
		x = 0,
		y = 0,
		width = fw,
		height = fh,
		font = font[1],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = -0.01,
		placeholder = translations["password_repeat"][_G.Lang],
		inputType = "password",
		secureDotSheet = secureDotSheet,
		symbolsMin = 5,			
		
		inputFont = font[2],
		inputOffsetX = -0.035,
		inputOffsetY = 0.02,		

		listener = function( event )
			rs:userInput( event )
		end		
	})
	rs.f_reppassw:setDecor({
		allBorders = false,
		--bgColor = bgcolor,		
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	rs.f_reppassw.x = rs.rowleft + rs.rows[nexti].icon.width + rs.f_reppassw.width*0.5 + 10
	rs.f_reppassw.y = rs.rowcenter
	rs.rows[nexti]:insert( rs.f_reppassw)

	nexti = nexti + 1
	rs.rows[nexti] = display.newGroup()
	rs:insert( rs.rows[nexti] )
	rs.rows[nexti].anchorChildren = true
	rs.rows[nexti].anchorY = 0
	rs.rows[nexti].y = rs.rows[nexti-1].y + rs.rows[nexti-1].height + step

	rs.rows[nexti].icon = display.newImageRect( "assets/countryIcon.png", 18, 18 )
	rs.rows[nexti]:insert( rs.rows[nexti].icon )
	rs.rows[nexti].icon.anchorX = 0
	rs.rows[nexti].icon.x = rs.rowleft
	rs.rows[nexti].icon.y = rs.rowcenter - 2
	rs.rows[nexti].icon.alpha=1

	local countr=""
	if CountriesList[_G.userCountry]==nil then countr="Unknown"
	else countr=CountriesList[_G.userCountry][_G.Lang].cntr_name
	end
	rs.f_country = display.newText({
		text = countr,
		x = 0, y = 0,
		width = fw,
            font = font[1],
            fontSize = fs,
		align = "left"
	})
	rs.f_country.fill = { 0.15, 0.52, 0.97 }
	rs.rows[nexti]:insert( rs.f_country, true )
	rs.f_country.x = rs.rowleft + rs.rows[nexti].icon.width + rs.f_country.width*0.5 + 11
	rs.f_country.y = rs.rowcenter
	rs.f_country:addEventListener( "tap", function()
		rs.listener({ target = rs, name = "countryRequest" })
		return true
	end )
	
	rs.butnreg = widget.newButton({
	    label = translations["register"][_G.Lang],
        labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0 } },
        fontSize = 16,
        font = "HelveticaNeueCyr-Medium",
	    onPress = function( event )
	    	rs:buttonPress()
	    end,
        --properties for a rounded rectangle button...
        shape="roundedRect",
        width = 200,
        height = 37,
        cornerRadius = 5,
        fillColor = { default={ 242/255, 105/255, 107/255 }, over={ 211/255,211/255,211/255 } },
        strokeColor = { default={ 242/255, 105/255, 107/255 }, over={ 0,0,0, 0.13 } },
        strokeWidth = 2
	})
	rs.butnreg.y = rs.rows[nexti].y + rs.butnreg.height + 70
	rs.butnreg.x = rs.x
	rs:insert( rs.butnreg )

	function rs:createVerticalScroll( limit )
	
		local function scrollTouch( event )
			if event.phase == "began" then
				self.prevtouch = event.y
				if not self.defaultpos then
					self.defaultpos = event.target.y
					self.limitpos = event.target.y - limit					
				end
			elseif event.phase == "moved" then
				self.lastdist = event.y - self.prevtouch
				if math.abs( self.lastdist ) >= 5 then
					display.getCurrentStage():setFocus( event.target )
				end
	
				if self.y + self.lastdist > self.defaultpos then
					self.y = self.defaultpos
				elseif self.y + self.lastdist < self.limitpos then
					self.y = self.limitpos
				else self.y = self.y + self.lastdist
				end
				self.prevtouch = event.y
				
			elseif event.phase == "ended" or event.phase == "cancelled" then
				self.lastdist = self.lastdist or 0
				if math.abs( self.lastdist ) >= 3 then
					local to = self.limitpos
					if self.lastdist > 0 then to = self.defaultpos end

					self.tween = transition.to( self, {
						time=500, y=to, transition=easing.outExpo, 
					})
				end

				display.getCurrentStage():setFocus( nil )	
			end
		end
		
		self:addEventListener( "touch", scrollTouch )
	end

	if display.actualContentHeight*0.5 < rs.height then
		local limit = rs.height - display.actualContentHeight*0.5
		rs:createVerticalScroll( limit+100 )
	end

-------------------------------------------------------------------------

	function rs:userInput( event )
		local ierror = event.inputError
		local id = event.target.id
		local text = event.text

		if event.phase == "ended" then
			self:dataValid( ierror, id, text )
		elseif event.phase == "submitted" then
			self:dataValid( ierror, id, text )
			self:sendNetRequest()
		end
	end

	function rs:dataValid( ierror, id, text )
--_print( "rs:dataValid... ", ierror, " ", id, " ", text )

		local count = self.datavalid.count
		local mess = self.datavalid.mess
		self.datavalid.is = false

		if ierror == "type" then
			count[id] = 0
			mess[id] = "type"
		elseif ierror == "min" then
			count[id] = 0
			mess[id] = "min"	
		elseif not ierror then 
			count[id] = 1
			self.data[id] = text
			mess[id] = nil
		end

		if id == "password" or id == "repassw" then 
			if self.f_password:getText() ~= self.f_reppassw:getText() then
				count["notMatch"] = 0
				mess["notMatch"] = true
			else count["notMatch"] = 1
				mess["notMatch"] = nil
			end
		end

		if id == "tel" then
			if string.match( text, "%a" ) then
				count[id] = 0
				mess[id] = "type"
				self.data[id] = nil				
			end
		end 


		local control = 0
		for k,v in pairs( count ) do control = control + v end
		if control == 7 then 
			print("$%^&*(",self.f_password:getText())
			self.data.fullname = self.data.name.." "..self.data.surname
			self.data.guest_password = XorCipher.crypt(self.f_password:getText())
			self.datavalid.is = true
			self.datavalid.mess = {}
		end
	end

	function rs:sendNetRequest( immediately )
		if immediately then
			Spinner:start()
			if self.f_country and self.f_country.cntr_id then 
				self.data.cntr_id = self.f_country.cntr_id
			end
			rdb:requestToRegistry( self.data, function( status, id )
				Spinner:stop()
				self:handleNetRespond( status, id )
			end )
			return
		end

		if self.datavalid.is then
			rs.listener({ target = rs, phase = "requestRegistration", data = self.data })
		else
			self:sendMessage()
		end
	end

	function rs:buttonPress()
--_print( "rs:buttonPress..." )		
		native.setKeyboardFocus( nil )
		timer.performWithDelay( 300, function()
			self:sendNetRequest()
		end )
	end

	function rs:removeMessBox()
		self.messbox:removeSelf()
		self.messbox = { numChildren = 0 }
	end

	function rs:sendMessage( tag )
		local ypos = self.butnreg.y - self.butnreg.height
		local mess = ""
--		_print("!!!!!!!!!!!!!!!!!!!!!!!")
		_print(tag)
		if not tag then
			for k,v in pairs( self.datavalid.mess ) do
				if v then 
					if type(v) == "string" then mess = self.messages[v][k]
					else mess = self.messages[k]
					end
					break
				end
			end
		else mess = self.messages[tag]
		end

		if mess == "" or mess==nil then mess = self.messages["empty"] end
		if mess==nil then mess="" end
		if self.messbox.numChildren > 0 then 
			timer.cancel( self.messbox.timer_handle )
			self.messbox:removeSelf() 
		end

		self.messbox = display.newGroup()
		self:insert( self.messbox )
		local messbox = self.messbox 

        local text = display.newText({
            parent = messbox,
            text = mess,     
            x = 0,
            y = 0,
            width = _Xa*0.7,
            font = "HelveticaNeueCyr-Medium",   
            fontSize = 14
        })
        text:setFillColor( 1, 1, 1 )

        local box = display.newRoundedRect( messbox, 
            0, 0, 
            text.width + 30, text.height + 30, 
            15 
        )
        box:setFillColor( 0,0,0, 0.8 )
        box:toBack()

		self.messbox.timer_handle = timer.performWithDelay( 2000, function()
			self:removeMessBox()
		end )
	end

	function rs:handleNetRespond( status, id )
		local listener = self.listener

		if status == "accepted" then
			--self:sendMessage( "reged" )
			local data = {	
				guest_id = id,
				guest_email = self.data.email,
				guest_firstname = self.data.name,
				guest_surname = self.data.surname,
				guest_fullname = self.data.fullname,
				guest_phone = self.data.tel,
				guest_password = self.data.password,
				cntr_id = self.f_country.cntr_id
			}
			ldb:saveGuestData( data )
			local event = { target = self, phase = "regged", data = data }
			local rdb_bonus = require( "lib.db_remote_bonuses" )
			rdb_bonus:downloadQRCode("qr.jpg")
			listener( event )
		else -- error, disconnection, denied
			self:sendMessage( status )
		end
	end
	
	return rs
end



---------------------------------------------------------------------------------
return EnterModule