--
-- Target devices: simulator, device
--
----------------------------------------------
local M = {}

--Requirements
local widget = require( "widget" )

local clouds = {236,240,241,255}
local silver = {189,195,199,255}
local asphalt = {52,73,94,255}
local darkgrey = {51,51,51,255}
local lightgrey = {238/255,238/255,238/255,255/255}
local orange = {255,102,0,255}

local iconPath = "icons/"
local overPath = iconPath .. "over/"
local defaultPath = iconPath .. "default/"

function M.newHeader(options)
    local options = options or {}
    options.top = options.top or display.screenOriginY
    options.left = options.left or 0
    options.width = options.width or (display.actualContentHeight < display.actualContentWidth) and display.actualContentHeight or display.actualContentWidth 
    options.height = options.height or 40
    options.isStatusBar = options.isStatusBar or false
    options.bgFile = options.bgFile or false
    options.baseDir = options.baseDir or system.ResourceDirectory
    options.bgColor = options.bgColor or {0,0}
    options.bgWidth = options.bgWidth or display.actualContentWidth
    options.bgHeight = options.bgHeight or options.height
    options.bcolor = options.bcolor or darkgrey
    options.items = options.items
    options.items.params = options.items.params or {}
    options.spacing = options.spacing or 12
    options.iconSize = options.iconSize or 32
    options.size = options.size or 14

    local header = display.newGroup()
    header.anchorChildren = true
    header.x = display.contentCenterX + options.left
    header.anchorY = 0
    header.y = options.top
    header.options = options -- store for orientation change    
    
    local container = display.newContainer( header, options.width, options.height )

    local cover = display.newRect( header, 0,0, options.width,options.height )
    cover.y = (container.height - cover.height)*0.5
    cover:setFillColor( unpack( options.bgColor ) )

    if options.bgFile then
        local bg = display.newImageRect( container, 
            options.bgFile,
            options.baseDir,
            options.bgWidth, options.bgHeight
        )
        bg.anchorY = 0
        bg.y = - container.height*0.5    
    end

    local ref_top = cover.y - cover.height*0.5
    local ref_bottom = cover.y + cover.height*0.5
    local ref_centerY = cover.y
    local ref_centerX = cover.x
    local ref_left = cover.x - cover.width*0.5
    local ref_right = cover.x + cover.width*0.5

    local function EnableTextButtons()
       for i,txt_button in ipairs(header.text_buttons) do
          txt_button:setEnabled(true)
       end
    end

    -- Create the buttons
    header.buttons = {}
    header.text_buttons = {}
    for i = 1, #options.items do
        local button
        if options.items[i].id then

            -- Build a custom Toggle Button
            button = display.newGroup()
            button.id = options.items[i].id
            button.state = options.items[i].state or "off"
            button.switch = options.items[i].switch
            button.params = options.items[i].params
            button.turnOn = options.items[i].turnOn or function() end
            button.turnOff = options.items[i].turnOff or function() end
            button.label_anchor = options.items[i].labelAnchor
            
            local textButtton
            if options.items[i].label and options.items[i].label~="" then
            	-- print ("create label button "..options.items[i].label)
               
                local tw, th = std:gettextsize( 
                    options.items[i].label, "HelveticaNeueCyr-Medium", options.items[i].fontSize
                )
                if tw > _Xa*0.5 then
                    local counter = 0
                    local croped_text = ""
                    for c in string.gmatch( options.items[i].label, ".[\128-\191]*" ) do
                        counter = counter + 1
                        croped_text = croped_text..c
                        if counter == 15 then break end
                    end
                    options.items[i].label = croped_text.."..."
                end

                local color = options.items[i].labelColor or { 0, 0, 0, 0.8 }
                local color_over = { unpack( color ) }
                if #color_over == 1 then color_over[2] = 0.5
                elseif #color_over == 2 then color_over[2] = color_over[2]*0.5
                elseif #color_over == 3 then color_over[4] = 0.5
                elseif #color_over == 4 then color_over[4] = color_over[4]*0.5    
                end

                textButtton = widget.newButton
                {
                    width = 10,
                    --height = 30,
                    labelColor = { default=color, over=color_over },
                    textOnly=true,
                    isEnabled=false,
                    font = options.fontLabel or options.items[i].font or "HelveticaNeueCyr-Medium",
                    fontSize = options.items[i].fontSize,
                    label = options.items[i].label,
                    onPress = function()
                        button:on()
                    end,
                    onRelease = function( event )
                        if not button.switch then return end
                        local pEvent = {}
                        pEvent.name = "touch"
                        pEvent.state = "on"
                        pEvent.target = event.target
                        button.turnOn( pEvent ) -- pass the event along
                    end
                }
                button:insert(textButtton, true)   
                textButtton.y = ( options.items[i].labelY or 0 ) + 1.5
                textButtton.params = options.items[i].params                
                textButtton.id = options.items[i].id
                header.text_buttons[#header.text_buttons+1] = textButtton
            end
            if options.items[i].icon then
            --     print(options.items[i].id)
             --   printResponse(options.items[i].params[1])
                buttonBg = display.newRect(button, 0, 0, 25, options.height )
            --     buttonBg:setFillColor(options.bcolor[1]/255,options.bcolor[2]/255,options.bcolor[3]/255, 0.01)
			buttonBg.isVisible = false
			buttonBg.isHitTestable = true

                button.skinOn = display.newImageRect(button, overPath .. options.items[i].icon .. ".png",
                    options.items[i].iconWidth or options.iconSize,
                    options.items[i].iconHeight or options.iconSize
                )
                
                button.skinOff = display.newImageRect(button, defaultPath .. options.items[i].icon .. ".png",
                    options.items[i].iconWidth or options.iconSize,
                    options.items[i].iconHeight or options.iconSize
                )
                
                button.skinOn.alpha = (button.state == "off") and 0 or 1
                button.skinOff.alpha = (button.state == "off") and 1 or 0

                if options.items[i].iconY then
                    buttonBg.y = options.items[i].iconY
                    button.skinOn.y = options.items[i].iconY
                    button.skinOff.y = options.items[i].iconY
                end
                if options.items[i].params and options.items[i].animate==true  then
                    local function listener2( obj )
  --                      print( "Transition 2 completed on object: " .. tostring( obj ) )
                    end                    
                    local function listener1( obj )
                        transition.to( button.skinOff, { time=200, xScale=1, yScale=1, onComplete=listener2 } )
                    end



                    -- (1) move square to bottom right corner; subtract half side-length
                    transition.to( button.skinOff, { time=200, xScale=1.3, yScale=1.3, onComplete=listener1 } )

                    -- (2) fade square back in after 2.5 seconds
                                        
                end
            end
            
            if textButtton and button.skinOn and button.skinOff then
                if button.label_anchor then
                    local pos = button.label_anchor.pos or "right"
                    local pad = button.label_anchor.pad or 0
                    local cases = {
                        ["right"] = function( tb )
                            tb.anchorX = 0
                            tb.x = button.skinOn.width*0.5 + pad
                        end,
                        ["left"] = function( tb )
                            tb.anchorX = 1
                            tb.x = -button.skinOn.width*0.5 - pad
                        end
                    }      
                    cases[pos]( textButtton )
                end
                button.textButtton = textButtton
            end            
               
            button.anchorChildren = true
            if options.items[i].left then
                button.anchorX = 0
                button.x = ref_left + options.items[i].left
            elseif options.items[i].right then
                button.anchorX = 1
                button.x = ref_right - options.items[i].right 
            elseif options.items[i].center then
                button.x = ref_centerX + options.items[i].center                
            else button.x = options.items[i].x or ref_centerX
            end
            button.y = ref_centerY + options.spacing + ( options.items[i].yShift or 0 )
            

            -- Toggle Listener

            function button:off()
                transition.to( self.skinOn, { time=133, alpha=0 } )
                transition.to( self.skinOff, { time=133, alpha=1 } )
                self.state = "off"
            end

            function button:on()
                transition.to( self.skinOn, { time=133, alpha=1 } )
                transition.to( self.skinOff, { time=133, alpha=0 } )
                if self.textButtton then  transition.to( self.textButtton, { time=133, alpha=0.5 } ) end
                self.state= "on"
            end

            function button:touch( event )
                if event.phase == "began" then
                    self.isFocus = true
                    display.getCurrentStage():setFocus( self, event.id )
                elseif self.isFocus and event.phase == "moved" then
                    local xmove = math.abs( event.xStart - event.x )
                    local ymove = math.abs( event.yStart - event.y )
                    if xmove >= 10 or ymove >= 10 then
                        display.getCurrentStage():setFocus( self, nil )
                        self.isFocus = false                        
                    end
                elseif self.isFocus and ( event.phase == "ended" or event.phase == "cancelled" ) then
                    if self.state == "off" then                    
                        for j = 1, self.parent.numChildren do --loop through the parents childeren to turn off othes buttons
                            if self.id ~= self.parent[j].id and self.parent[j].state == "on" then -- if it's a state button
                                self.parent[j]:off()
                                local pEvent = {}
                                pEvent.parent = self.parent
                                pEvent.target = self.parent[j]
                                self.parent[j].turnOff( pEvent ) -- pass the event along
                            end
                        end
                        self:on()
                        self.turnOn( { target = self, parent = self.parent } )
                    else
                        self:off()
                        self.turnOff( { target = self, parent = self.parent } )
                    end                        
                    display.getCurrentStage():setFocus( self, nil )
                    self.isFocus = false
                end
                return true
            end

            if button.switch then button:addEventListener("touch", button ) end
            header.buttons[i] = button
        end
        button.parent = header
        header:insert( button )
        
      
        if #header.text_buttons>0 then
            timer.performWithDelay( 300, EnableTextButtons )
        end
    end

    return header

end

return M