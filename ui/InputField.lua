-- Input Field v0.4

--[[-------------- INTERFACE ----------------------------

field = widget.newInputField( params )    -- create new input field
	params ={
		x
		y
		width
		height         
		font
		fontSize       
		fontColor
		padLeft
		padRight
		offsetY 	   
		align          : left, right, center
		text
		placeholder
		inputType      : email, number, alpha, password
		secureDotSheet
		typeControl
		symbolsMin
		symbolsMax
		listener
		
		-- input layer settings   - need to adjust to find suitable values
		inputFont
		inputOffsetX     
		inputOffsetY	 
	}

field:removeSelf()     -- delete input field from memory

field:setFont = function( params ) end,  -- not implemented
			-- params.font
			-- params.size
			-- params.allign
			-- params.color

field:setDecor = function( params ) end,
			params = {
				bgColor				
				allBorders
				bordersWidth
				bordersColor
				topBorder
				bottomBorder
				leftBorder
				rightBorder
			}

field:hideNative = function( flag ) end,
field:getText = function() end,
field:clear = function() end,
field:setEditable( true ) = function() end,	


-----------------------------------------------------------------------------]]

local widget = require "widget"

local function newInputField( params )
	local _X = display.contentWidth
	local _Y = display.contentHeight
	local _Xa = display.actualContentWidth
	local _Ya = display.actualContentHeight
	local _mX = display.contentWidth*0.5
	local _mY = display.contentHeight*0.5

	local p = params
	local nif = display.newGroup()  -- new input field
	nif.anchorChildren = true

	p.symbolsMin = p.symbolsMin or 0
	p.symbolsMax = p.symbolsMax or 0
	p.listener = p.listener or function() end
	p.font = p.font or native.systemFont
	p.fontSize = p.fontSize or 0
	p.fontColor = p.fontColor or {0,0,0}
	p.align = p.align or "left"
	p.padLeft = p.padLeft and math.abs( p.padLeft ) or 0
	p.padRight = p.padRight and math.abs( p.padRight ) or 0
	p.offsetY = p.offsetY or 0
	p.text = p.text or ""
	nif.params = p

	function nif:getTextWidth( text )
		if not text then return 0 end
		local font = self.params.font
		local font_size = self.params.fontSize
		local temp_text = display.newText( text, 0,0, font, font_size )
		local width = temp_text.width
		temp_text:removeSelf()
		temp_text = nil
		return width
	end

	function nif:getTextHeight( text )
		text = text or "text"
		local font = self.params.font
		local font_size = self.params.fontSize
		local temp_text = display.newText( text, 0,0, font, font_size )
		local height = temp_text.height
		temp_text:removeSelf()
		temp_text = nil
		return height
	end

	nif.state = "unlocked"
	nif.isfocus = false
	nif.inputdata = p.text
	nif.initial_text_size = p.text ~= "" and nif:getTextWidth( p.text ) or 0
	if nif.params.width_init then
		nif.initial_text_size =nif.params.width_init
	end
	local def_field_width = 35
	nif.initial_field_width = def_field_width + nif.initial_text_size
	nif.prev_text_size = 0
	nif.prev_field_xscale = 1
	nif.placeholder_alpha = 0.5

	nif.native_xoffset = -2.5
	nif.native_yoffset = 2.5

	nif.x = p.x
	nif.y = p.y

	nif.bg = display.newRoundedRect( 0, 0, p.width, p.height, 0 )
	nif.bg:setFillColor( 1,1,0, 0 )
	-- nif.bg:setFillColor( 1,0,0, 0.5 )
	nif:insert( nif.bg )
	nif.bg.isHitTestable = true
	nif.bg.all_borders = true
	nif.bg.borders = display.newGroup()
	nif:insert( nif.bg.borders )
	nif.bg:addEventListener( "touch", nif )

	nif.top = -nif.bg.height*0.5
	nif.bottom = nif.bg.height*0.5
	nif.left = -nif.bg.width*0.5
	nif.right = nif.bg.width*0.5

	nif.text = display.newText({
	    text = p.text,     
	    x = 0,
	    y = 0,
	    width = p.width - p.padLeft - p.padRight,
	    height = nif:getTextHeight()+1,              -- if not specified, then will be multi-line text  
	    font = p.font,   
	    fontSize = p.fontSize,
	    align = p.align
	})
	nif.text:setFillColor( params.fontColor or unpack({0,0,0}) )
	nif:insert( nif.text )
	nif.text.anchorX = 0
	nif.text.x = nif.left + p.padLeft
	if p.align == "center" then
		nif.text.anchorX = 0.5
		nif.text.x = nif.left + nif.bg.width*0.5
	elseif p.align == "right" then
		nif.text.anchorX = 1
		nif.text.x = nif.left + nif.bg.width - p.padRight	
	end
	nif.text.y = nif.top + nif.bg.height*0.5 + p.offsetY
	
	if p.placeholder and p.text == "" then 
		nif.text.text = p.placeholder
		nif.text.alpha = nif.placeholder_alpha
	end

	--------------------- PUBLIC METODS -----------------------------

	function nif:setDecor( params )
		if params.bgColor then self.bg:setFillColor( unpack( params.bgColor ) ) end

		if params.allBorders ~= nil then
			self.bg.all_borders = params.allBorders
			if params.allBorders == true then
				for i=1, self.bg.borders.numChildren do
					self.bg.borders[i]:removeSelf()
					self.bg.borders[i] = nil
				end
			else self.bg.strokeWidth = 0
			end
		end
		
		if params.topBorder and not self.bg.all_borders then
			display.newLine( self.bg.borders, 
				self.left + self.bg.width*params.topBorder[1], 
				self.top, 
				self.left + self.bg.width*params.topBorder[2], 
				self.top
			)
		end
		
		if params.bottomBorder and not self.bg.all_borders then
			display.newLine( self.bg.borders, 
				self.left + self.bg.width*params.bottomBorder[1], 
				self.bottom, 
				self.left + self.bg.width*params.bottomBorder[2], 
				self.bottom
			)
		end
		  
		if params.leftBorder and not self.bg.all_borders then
			display.newLine( self.bg.borders, 
				self.left, 
				self.top + self.bg.height*params.leftBorder[1]-params.bordersWidth*0.5, 
				self.left, 
				self.top + self.bg.height*params.leftBorder[2]+params.bordersWidth*0.5
			)
		end

		if params.rightBorder and not self.bg.all_borders then
			display.newLine( self.bg.borders, 
				self.right, 
				self.top + self.bg.height*params.rightBorder[1]-params.bordersWidth*0.5, 
				self.right, 
				self.top + self.bg.height*params.rightBorder[2]+params.bordersWidth*0.5
			)
		end

		if params.bordersWidth then
			if self.bg.all_borders then
				self.bg.strokeWidth = params.bordersWidth
			else 
				for i=1, self.bg.borders.numChildren do
					self.bg.borders[i].strokeWidth = params.bordersWidth
				end
			end
		end
		
		if params.bordersColor then
			if self.bg.all_borders then
				self.bg:setStrokeColor( unpack( params.bordersColor ) )
			else 
				for i=1, self.bg.borders.numChildren do
					self.bg.borders[i]:setStrokeColor( unpack( params.bordersColor ) )
				end
			end
		end   	
	end

	function nif:hideNative( flag )
		if type( self.field ) ~= "userdata" then return end
		if flag then self.field:toBack()
		else self.field:toFront()
		end
	end

	function nif:getText()
		return self.inputdata
	end

	function nif:setText( text )
		text = text or ""
		self.inputdata = text
		self.initial_text_size = text ~= "" and self:getTextWidth( text ) or 0
		self.initial_field_width = def_field_width + self.initial_text_size
		self.prev_text_size = 0
		self.prev_field_xscale = 1
		self.text.text = text
	end

	function nif:clear( save_focus )
		self.inputdata = ""
		if self.field then self.field.text = "" end
		self.text.text = ""
		self.initial_text_size = 0
		self.initial_field_width = def_field_width
		self.prev_text_size = 0
		self.prev_field_xscale = 1
		self.text.isVisible = true

		if self.params.placeholder then 
			self.text.text = self.params.placeholder
			self.text.alpha = self.placeholder_alpha
		end

		if self.secure then
			for i=1, #self.secure do
				self.secure[i]:removeSelf()
				self.secure[i] = nil
			end
		end

		if save_focus then
			native.setKeyboardFocus( self.field )
				self.isfocus = true
			else native.setKeyboardFocus( nil )
				self.isfocus = false
			end
		end

	function nif:setEditable( param )
		if param == false then
			self.bg:removeEventListener( "touch", self )
			self.state = "locked"
		elseif param == true then
			self.bg:addEventListener( "touch", self )
			self.state = "unlocked"
		end
	end

	-----------------------------------------------------------------

	function nif:createClearButton()
		local radius = self.params.height * 0.1
		local x = self.bg.width*0.5 - radius		
		local button = display.newCircle( x, 0, radius )
		button:setFillColor( 1,0,0 )
		self:insert( button )

		button:addEventListener( "tap", function( event )
			self:clear()	
		end )
	end

	function nif.newScaledTextField( centerX, centerY, width, desiredFontSize )
-- print( "<<<<<", centerX, centerY, width, desiredFontSize )	    
	    -- Corona provides a resizeHeightToFitFont() feature on build #2520 or higher
	    if ( tonumber( system.getInfo("build") ) >= 2014.2520 ) then
	    	-- width=180
	    	if width < 180 then
	    		width = 180
	    	end
	        local textField = native.newTextField(centerX, centerY, width, 30)
	        local isFontSizeScaled = textField.isFontSizeScaled
	        textField.isFontSizeScaled = true
	        textField.size = desiredFontSize
	  --Jen      textField:resizeHeightToFitFont()
	        textField.isFontSizeScaled = isFontSizeScaled
	        return textField
	    end
	    local fontSize = desiredFontSize or 0

	    -- Create a text object, measure its height, and then remove it
	    local textToMeasure = display.newText( "X", 0, 0, native.systemFont, fontSize )
	    local textHeight = textToMeasure.contentHeight
	    textToMeasure:removeSelf()
	    textToMeasure = nil

	    local scaledFontSize = fontSize / display.contentScaleY
	    local textMargin = 20 * display.contentScaleY  -- convert 20 pixels to content coordinates

	    -- Calculate the text input field's font size and vertical margin, per-platform
	    local platformName = system.getInfo( "platform" )
	    if ( platformName == "ios" ) then
	        local modelName = system.getInfo( "model" )
	        if ( modelName == "iPad" ) or ( modelName == "iPad Simulator" ) then
	            scaledFontSize = scaledFontSize / ( display.pixelWidth / 768 )
	            textMargin = textMargin * ( display.pixelWidth / 768 )
	        else
	            scaledFontSize = scaledFontSize / ( display.pixelWidth / 320 )
	            textMargin = textMargin * ( display.pixelWidth / 320 )
	        end
	    elseif ( platformName == "android" ) then
	        scaledFontSize = scaledFontSize / ( system.getInfo( "androidDisplayApproximateDpi" ) / 160 )
	        textMargin = textMargin * ( system.getInfo( "androidDisplayApproximateDpi" ) / 160 )
	    end

	    -- Create a text field that fits the font size from above
	    local textField = native.newTextField(
	        centerX,
	        centerY,
		  width,
	        textHeight + textMargin
	    )
	    textField.size = scaledFontSize
	    return textField, scaledFontSize
	end

	function nif:getScaledPixels( value )
		local new_value = value * display.contentScaleX
	    if platformName == "android" then
	        new_value = new_value * ( system.getInfo( "androidDisplayApproximateDpi" ) / 160 )
	    else new_value = new_value * ( 240 / 160 )
	    end
	    return new_value
	end		

	function nif:createNativeField( params )
		local platform = system.getInfo( "platform" )
		if platform ~= "android" and platform ~= "win32" then 
			print( "WARNING: InpudField maybe display incorrect, because is not tested on iOS and Mac." )
		end	

		local bg_bounds = self.contentBounds
		local font = params.inputFont or native.systemFont
		local fontsize = params.fontSize or 0
		local offsetx = params.inputOffsetX or 0
		local offsety = params.inputOffsetY or 0
		local align =  params.align or "left"
		
		--if params.inputType == "password" then 
		-- 	self.field = native.newTextField( 
		-- 		0, 0,                    -- x, y
		-- 		1, params.height         -- w, h
		-- 	)
		-- 	self.field.isSecure = true
		-- 	self.field:setTextColor( 1,1,1 )
		-- 	self.field.size = 0
		-- 	self.field.anchorX = 0
		-- 	self.field.x = bg_bounds.xMin
		-- 	self.field.y = bg_bounds.yMin + self.height*0.5
		-- 	--self:createClearButton()
		-- else 

			local field, size = self.newScaledTextField( 
				0,0, 
				self.initial_field_width, 
				fontsize
			)
			self.field = field
			if params.inputType == "password" then 
				self.field.isSecure = true
			end
			self.field.font = native.newFont( font, size )
			self.field.align = align
			self.field:setTextColor( params.fontColor or unpack({0,0,0}) )
			self.field.xScale = p.native_field_xscale or self.prev_field_xscale

			local xoff_scaled = self:getScaledPixels( self.native_xoffset + params.padLeft )
			local yoff_scaled = self:getScaledPixels( self.native_yoffset )
			self.field.anchorX = 0
			self.field.x = bg_bounds.xMin + xoff_scaled  + offsetx
			if align == "center" then
				self.field.anchorX = 0.5		 
				self.field.x = bg_bounds.xMin + self.width*0.5 + offsetx
			elseif align == "right" then
				self.field.anchorX = 1
				self.field.x = bg_bounds.xMax + xoff_scaled - params.padRight + offsetx
			end
			self.field.y = bg_bounds.yMin + yoff_scaled + self.height*0.5 + params.offsetY + offsety
		--end
		self.field.id = "field" 
		self.field.hasBackground = false
		self.field.inputType = params.inputType
		
		self.field:addEventListener( "userInput", self )

		self.field.substrate = display.newRect( _mX, _mY, _Xa, _Ya )
		self.field.substrate.isVisible = false
		self.field.substrate.isHitTestable = true
		self.field.substrate:addEventListener( "touch", function( event )
			local field_bounds = self.field.contentBounds
			local bg_bounds = self.bg.contentBounds
			if event.phase == "began" then
				if ( event.x < bg_bounds.xMin or event.x > bg_bounds.xMax )
				or ( event.y < bg_bounds.yMin or event.y > bg_bounds.yMax ) then
					native.setKeyboardFocus( nil )
					return false
				end
			elseif event.phase == "moved" then
				if ( event.x < field_bounds.xMin or event.x > field_bounds.xMax )
				or ( event.y < field_bounds.yMin or event.y > field_bounds.yMax ) then
					native.setKeyboardFocus( nil )
					return false
				end
			end
			return true
		end )
		
		local function handleBackButn( event )
		    if event.keyName == "back" and system.getInfo("platform") == "android" then
			native.setKeyboardFocus( nil )
		    return true
		end
		    return false
		end
		Runtime:addEventListener( "key", handleBackButn )
	end

	function nif:inputValid()
		-- print( "nif:inputValid...",self.params.inputType )
		local symbol = ""
		local input_error = nil
		local data = self.inputdata
		local datatype = self.params.inputType

		if self.params.typeControl then
			if datatype == "email" then
				local at = 0
				local dot = 0

				for i=1, #data do
					symbol = data:sub( i, i )
					if symbol == "@" then
						if at == 0 then 
							if i > 1 then at = i
							else at = -1; break
							end
						else at = -1; break
						end
					elseif symbol == "." then
						if i > #data - 2 then dot = -1; break
						else dot = i
						end 
					end		
				end
				if at <= 0 or dot <= 0 or at >= dot then return "type" end
		
			elseif datatype == "alpha" then
				for i=1, #data do
					symbol = data:sub( i, i )
					if tonumber( symbol ) then return "type" end
				end
		
			elseif datatype == "number" then
				for i=1, #data do
					symbol = data:sub( i, i )
					if not tonumber( symbol ) then return "type" end
				end
				
			elseif datatype == "password" then
				--if data == "" then return "type" end
			end
		end

		local min = self.params.symbolsMin
		local max = self.params.symbolsMax
		if min >= 0 then
			if #data < min then return "min" end
		end
		if max > 0 and max > min then
			if #data > max then return "max" end
		end	

		return nil
	end

	function nif:textSecurity( call )
		local radius = self.params.height * 0.06
		local step = 1.5
		local startx = -self.bg.width*0.5 + self.params.padLeft

		self.secure = self.secure or {}
		if not self.cursor then
			self.cursor = display.newLine( self, 0,0, 0, self.params.fontSize+4 )
			self.cursor.strokeWidth = 1
			self.cursor:setStrokeColor( 0,0,0, 0.5 )
			self.cursor.x = startx
			if #self.secure > 1 then 
				self.cursor.x = self.secure[#self.secure].x + self.secure[#self.secure].width
			end
			self.cursor.y = self.text.y - self.text.height*0.5
			transition.blink( self.cursor, { time=2000 } )
		end

		local i = #self.secure + 1
		if call == "more" then
			if self.params.secureDotSheet then
				self.secure[i] = display.newImage( self.params.secureDotSheet, 1 )
			else self.secure[i] = display.newCircle( 0, 0, radius )
				self.secure[i]:setFillColor( 0,0,0 )
			end
			self.secure[i].anchorX = 0
			self.secure[i].x = startx + (self.secure[i].width + step)*(i-1)
			self:insert( self.secure[i] )
			self.cursor.x = self.secure[i].x + self.secure[i].width
		elseif call == "less" then
			if i > 1 then
				if i == 2 then self.cursor.x = startx
				else self.cursor.x = self.secure[i-1].x - step
				end
				self.secure[i-1]:removeSelf()
				self.secure[i-1] = nil 
			end
		end
	end

	function nif:userInput( event )
		local input_type = self.params.inputType 
	    local custom_event = {
	        target = { 
	        	id = self.params.id,
	        	inputType = input_type 
	        },
	        phase = event.phase,
	        newCharacters = event.newCharacters,
	        oldText = event.oldText,
	        startPosition = event.startPosition,
	        text = self.field.text
		}
		-- print("event.phase",event.phase)
	    if event.phase == "began" then
	    	self.text.isVisible = false
	    	self.text.text = ""
	    	
	    	if self.inputdata ~= "" then
	    		self.field.text = self.inputdata
	    		self.field:setSelection( #self.inputdata, #self.inputdata )
	    	end

	    	if input_type == "password" then 
	    		--Jen self:textSecurity() 
	    	end

	    elseif event.phase == "editing" then
	     	self.text.text = event.text
	     	local text_widt = self:getTextWidth( event.text )
	     	if #event.text == 0 then text_widt = 0 end
	    	if input_type ~= "password" then 
	    	--	self:changeFieldWith( text_widt, 3 - #self.text.text )
	    	end

	    	if input_type == "password" then  --Jen
	    		-- if #event.text > #event.oldText then
	    		-- 	self:textSecurity( "more" )
	    		-- elseif #event.text < #event.oldText then
	    		-- 	self:textSecurity( "less" )
	    		-- end
	    	end

	        -- print( event.newCharacters )
	        -- print( event.oldText )
	        -- print( event.startPosition )
	        -- print( event.text )    	
	    
	    elseif event.phase == "ended" or event.phase == "submitted" then
		if self.field.text ~= "" then
		    	local prevwidth = self.text.width
		    	self.inputdata = self.field.text
		    	self.text.text = self.field.text
		    	self.text.alpha = 1
		    	self.field.text = ""
	   		
	    	else self.inputdata = ""
	    		self.text.text = self.params.placeholder
	    		self.text.alpha = self.placeholder_alpha
	    	end
	    	self.text.isVisible = true
	    	self.isfocus = false
	    	print(input_type,self.inputdata)
	    	if input_type == "password" and self.text.text ~= "" then
	    		--Jen self.text.isVisible = false
	    		local star=""
	    		for i=1,string.len( self.text.text ) do
	    			star=star.."*"
	    		end
	    		self.text.text =star
	    	end
	    	if self.cursor then 
	    		self.cursor:removeSelf()
	    		self.cursor = nil
	    	end

	    	Runtime:removeEventListener( "key", handleBackButn )
	    	self.field.substrate:removeSelf()
		self.field.substrate = nil
	    	self.field:removeSelf()
	    	self.field = nil	    	

	    	custom_event.inputError = self:inputValid()
	    end

	    self.params.listener( custom_event )
	end

	function nif:touch( event )
		if event.target.id == "field" then 
			--print( "Tne native text field was touched..." )
		end

		if event.phase == "began" then

		elseif event.phase == "ended" then
			if not self.isfocus then
				local custom_event = {
					target = { 
						id = self.params.id,
						inputType = self.params.inputType 
					},
					phase = "will",
					text = self.text.text
				}
				self.params.listener( custom_event )
				self:createNativeField( self.params )
				native.setKeyboardFocus( self.field )
				self.isfocus = true
			end
		end

	end

	function nif:changeFieldWith( curr_text_size, symbs )
		local nfsize = 0
		local cfsize = self.field.xScale * self.initial_field_width
		local ctsize = curr_text_size
		local itsize = self.initial_text_size
		local ifsize = self.initial_field_width
		local ptsize = self.prev_text_size
		local lim = 0.25
		local symbwidth = ctsize - ptsize

		if ptsize == 0 then self.prev_text_size = ctsize end

-- print( "nif:changeFieldWith...1:" )
-- print( "  bg_size: ",  self.bg.width )	
-- print( "  curr_text_size: ",  ctsize )
-- print( "  prev_text_size: ",  ptsize )
-- print( "  initial_text_size: ", itsize )
-- print( "  initial_field_width", ifsize )
-- print( "  symb_width: ", symbwidth )
-- print( "  field_xScale: ", self.field.xScale )	
-- print( "  curr_field_size: ",  self.field.xScale*self.initial_field_width )
		
		if itsize == 0 then
			if ctsize / ifsize >= lim then
				self.initial_text_size = ctsize
				self.prev_text_size = ctsize  
				return
			end
		else
			if ctsize / ifsize < lim then
				self.initial_text_size = 0
				self.field.xScale = 1
			else
				if symbwidth > 0 then
					if ctsize < self.bg.width * 0.75 then
						nfsize = symbwidth*1.3 + cfsize
						self.field.xScale = nfsize / ifsize
					end
				elseif symbwidth < 0 then
					if ctsize < self.bg.width * 0.7 then
						nfsize = symbwidth*1.3 + cfsize
						self.field.xScale = nfsize / ifsize
					end				
				end
			end
		end

-- print( "nif:changeFieldWith...2:" )
-- print( "  field_xScale: ", self.field.xScale )	
-- print( "  curr_field_size: ",  self.field.xScale*self.initial_field_width )
-- --print( "  prev_text_size: ",  ptsize )
-- print( "  total_growth: ",  ctsize/ptsize - 1 )
-- print( "  initial_growth: ",  symbwidth/itsize )
-- print( "  abs_growth: ",  nfsize/cfsize-1 )
-- --print( "  initial_field_width", ifsize )
-- --print( "  symb_width", symbwidth )

	    self.prev_text_size = ctsize
	    self.prev_field_xscale = self.field.xScale     
	end
-- nif.anchorChildren = true
-- nif.x = p.x
-- nif.y = p.y
	return nif
end

widget.newInputField = newInputField