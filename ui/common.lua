local common = {}


------ SPINNER ---------------------------------------------------------

local widget = require "widget"

common.Spinner = {
	sheet = graphics.newImageSheet( "icons/spinner1.png", {
	    width = 24,
	    height = 24,
	    numFrames = 8,
	    sheetContentWidth = 192,
	    sheetContentHeight = 24
	})
}

-- Global Spinner - only one object in all app ----------------------------

function common.Spinner:start( coverColor, coverAlpha )
	self._ = display.newGroup()
	
	self._.spinner = widget.newSpinner
	{
	    width = 24,
	    height = 24,
	    sheet = self.sheet,
	    startFrame = 1,
	    count = 8,
	    time = 500
	}
	self._:insert( self._.spinner )
	self._.spinner.x = display.contentCenterX 
	self._.spinner.y = display.contentCenterY 

	self._.cover = display.newRect( self._, 
    	display.contentCenterX, display.contentCenterY,
    	display.actualContentWidth, display.actualContentHeight 
    )
    self._.cover:setFillColor( unpack( coverColor or {1} ) )
    self._.cover.alpha = coverAlpha or 0.6
    self._.cover:addEventListener( "touch", function( event )
        return true
    end )
    self._.cover:addEventListener( "tap", function( event )
        return true
    end )
	self._.cover.x = display.contentCenterX
	self._.cover.y = display.contentCenterY
	self._:insert( self._.cover )
	self._.cover:toBack()

	self._:toFront()
	self._.spinner:start()
end

function common.Spinner:stop()
	if self._ then
		self._.spinner:stop()
		self._:removeSelf()
		self._ = nil
	end
end

-- Local Spinner ------------------------------------------

function common.Spinner:new( coverColor, coverAlpha, coverWidth, coverHeight )
--print( "... Spinner:new" )
	coverColor = coverColor or {0,0,0}
	coverAlpha = coverAlpha or 0.5

	local newspinner = display.newGroup()
	newspinner.anchorChildren = true
	newspinner.x = display.contentCenterX 
	newspinner.y = display.contentCenterY
	newspinner.isVisible = false
	
	newspinner.spinner = widget.newSpinner
	{
	    width = 24,
	    height = 24,
	    sheet = self.sheet,
	    startFrame = 1,
	    count = 8,
	    time = 500
	}
	newspinner:insert( newspinner.spinner, true )

	if coverWidth and coverHeight then
		newspinner.cover = display.newRect( newspinner, 
	    	0, 0,
	    	coverWidth, coverHeight 
	    )
	    newspinner.cover:setFillColor( unpack( coverColor ) )
	    newspinner.cover.alpha = coverAlpha
	    newspinner.cover:addEventListener( "touch", function( event )
	        return true
	    end )
	    newspinner.cover:addEventListener( "tap", function( event )
	        return true
	    end )
		newspinner.cover:toBack()
	end

	function newspinner:start()
		self:toFront()
		self.isVisible = true
		self.spinner:start()
	end
	
	function newspinner:stop()
		self.isVisible = false
		self.spinner:stop()
	end

	return newspinner
end



-------- KEYBACK HANDLER -----------------------------------------------------------------

local function newKeyBackHandler()
	local kbh = {
		timerid = nil,
		focuson = {}
	}

	function kbh:setFocus( table )
        if table == nil then
        	self.focuson = {}
        else 
        	if self.timerid then timer.cancel( self.timerid ); self.timerid = nil end
	        self.timerid = timer.performWithDelay( 300, function()
				if type( table ) ~= "table" then return end
				self.focuson = table
				self.timerid = nil
	        end )
	    end		
	end

	function kbh:remove()
		Runtime:removeEventListener( "key", self )
	end	

	local function key( event )
		if kbh.pauseid then return true end
	   	local platform = system.getInfo("platform")
		   local keyname = event.keyName
		   local iscommand = event.isCommandDown
		   local is_scene = composer.getSceneName( "current" )
		   local is_overlay = composer.getSceneName( "overlay" )

		   -- prevent duplicates events
		   kbh.pauseid = timer.performWithDelay( 500, function( event )
			kbh.pauseid = nil
		   end )

	    -- for simulators
	    if ( platform == "win32" or platform == "macos"
	    and ( keyname == "deleteBack" and iscommand ) )
	    -- for devices 
		or ( platform == "android" and keyname == "back" ) then
			if is_overlay then
				if kbh.focuson.keyback then kbh.focuson:keyback()
				else composer.hideOverlay()
				end	
				_G.overlayIsActive=false
			else
				if kbh.focuson.keyback then kbh.focuson:keyback()
				elseif mySidebar then mySidebar:press( "list_main" )
				end
			end
		    return true
		else 
			return false 
		end
	    
	end

	Runtime:addEventListener( "key", key )
	return kbh
end

-- only one object for one app
common.KeyBackHandler = newKeyBackHandler()

--------------------------------------------------------------------------------------------

return common