local widget = require("widget")

local scene = composer.newScene()
-- 
local rdb_bonus = require "lib.db_remote_bonuses"
local rdb = require "lib.db_remote"
local rdb_common = require "lib.db_remote_common"
local uilib = require "ui.uilib"
local langs = {}
local settings = {}


local function createHeader()
	print( "header settings" )
    local headerItems = {}
    
    headerItems[1] = {
	  id = "settings",
	  label = translations["Sidebar_settings"][_G.Lang],
	  labelColor = {0, 0.7},
	  fontSize = 20,
	  icon = "backBlack",
	  iconWidth = 7 * 1.2,
	  iconHeight = 13 * 1.2,
	  labelAnchor = {pos = "right", pad = 6},
	  center = 0,
	  switch = true,
	  turnOn = function(event)
		local prevscene = composer.getSceneName("previous")
		mySidebar:press( _G.scenes_ids[prevscene] or "list_main" )
	  end
    }

    CreateHeader(headerItems, {color = {0, 0}})
end

local function createTitle(text)
    local pad = 15
    local text_pad = 5
    local height = 20
    local line_height = 0.7
    local line_color = 0.8
    local font = "HelveticaNeueCyr-Light"
    local font_size = 13

    local title = display.newGroup()
    title.anchorChildren = true

    local bg = display.newRect(title, 0, 0, _Xa, height)
    bg:setFillColor(0.5, 0)

    local ref_left = -bg.width * 0.5 + pad

    local left_line = display.newRect(title, ref_left, 0, 30, line_height)
    left_line:setFillColor(line_color)
    left_line.anchorX = 0

    local title_text =
	  display.newText(
	  {
		parent = title,
		text = text,
		font = font,
		fontSize = font_size
	  }
    )
    title_text:setFillColor(0)
    title_text.anchorX = 0
    title_text.x = left_line.x + left_line.width + text_pad

    local rlwidth = _Xa - (left_line.width + title_text.width + pad * 2 + text_pad * 2)
    local right_line = display.newRect(title, 0, 0, rlwidth, line_height)
    right_line:setFillColor(line_color)
    right_line.anchorX = 0
    right_line.x = title_text.x + title_text.width + text_pad

    return title
end

local function createLanguagePanel( names_and_states, onRelease )
	local slpanel = {}

	-- touchpad ---------------------------------------------
	
	local touchpad = display.newRect( scene.view, _mX, _mY + headerHeight, _Xa, _Ya - headerHeight )
	touchpad.isVisible = false
	touchpad:addEventListener( "touch", function( event )
		if event.phase == "began" then slpanel:hide( 200 ) end
		return true
	end )	
  
	-- header ---------------------------------------------
	
	local header = display.newGroup()
  
	local sheet_options = {
	    width = 292,
	    height = 39,
	    numFrames = 2,
	    sheetContentWidth = 292,
	    sheetContentHeight = 78
	}
	local sheet_file = "assets/filterCuisinesButton.png"
  
	local header_sheet = graphics.newImageSheet( sheet_file, sheet_options )
	local headerimg = display.newImage( header, header_sheet, 2 )
	headerimg:setFillColor( 187/255, 187/255, 187/255 )
  
	local title = display.newText({
	    parent = header,
	    text = translations["select_cuisine"][_G.Lang],--translations.OrderTxt_ReservDate[self.lang][1],
	    font = "HelveticaNeueCyr-Light", 
	    fontSize = 13,
	    align = "center"
	})
	title:setFillColor( 0, 0.6 )
  
	-- body -----------------------------------------------
  
	local body = display.newGroup()
	local body_width = header.width-40
  
	local function createCuisineLine( name, state, width, height, onRelease )
	    local cuisline = display.newGroup()
	    cuisline.anchorChildren = true
	    cuisline.anchorY = 0
	    cuisline.state = state and "on" or "off"
	    cuisline.name = name
  
	    local bg = display.newRect( cuisline, 0, 0, width, height )
	    bg:setFillColor( 0.95, 0 )
	    bg.isHitTestable = true
  
	    local right = -bg.width*0.5
  
	    local options = {
		  width = 25,
		  height = 25,
		  numFrames = 2,
		  sheetContentWidth = 50,
		  sheetContentHeight = 25
	    }
	    local rbsheet = graphics.newImageSheet( "assets/orders/smallRadio.png", options )
	    local radio_on = display.newImageRect( cuisline, rbsheet, 1, 20, 20 )
	    local radio_off = display.newImageRect( cuisline, rbsheet, 2, 20, 20 )
	    radio_on.anchorX = 0
	    radio_off.anchorX = 0
	    radio_on.x = right + 15
	    radio_off.x = right + 15
	    if cuisline.state == "off" then radio_on.isVisible = false end
  
	    local label = display.newText({
		  parent = cuisline, 
		  text = name, 
		  font = "HelveticaNeueCyr-Light", 
		  fontSize = 17
	    })
	    label:setFillColor( 0 )
	    label.anchorX = 0
	    label.x = radio_on.x + radio_on.width + 10
  
	    function cuisline:turnOn()
		  radio_on.isVisible = true
		  self.state = "on"                
	    end
	    function cuisline:turnOff()
		  radio_on.isVisible = false
		  self.state = "off"                
	    end
  
	    cuisline:addEventListener( "touch", function( event )
		  if event.phase == "began" then
			event.target.isFocus = true
			display.getCurrentStage():setFocus( event.target )
		  elseif event.phase == "moved" then
			if math.abs( event.y - event.yStart ) >= 10 then
			    event.target.isFocus = false
			    display.getCurrentStage():setFocus( nil )
			    return false
			end
		  elseif event.phase == "ended" or event.phase == "cancelled" then
			if event.target.state == "off" then
			    event.target:turnOn()
			elseif event.target.state == "on" then
			    event.target:turnOff()
			end
			local pEvent = { 
			    target = event.target,
			    state = event.target.state,
			    newName = event.target.name
			}
			onRelease( pEvent )
			display.getCurrentStage():setFocus( nil )
		  end
		  return true
	    end )
  
	    return cuisline
	end
  
	local newcl, sepr
	local ypos = 0
	for k,v in pairs( names_and_states ) do
	    if v then
		  title.text = k
		  scene.currCuisName = k
	    end          
  
	    newcl = createCuisineLine( 
		  k, v, 
		  body_width, 45, 
		  function( event )
			slpanel:select( event.newName )
			event.panel = slpanel
			event.currName = scene.currCuisName
			onRelease( event )
			slpanel:hide( 200 )
		  end 
	    )
	    body:insert( newcl )
	    newcl.y = ypos
	    ypos = ypos + newcl.height
	    
	    sepr = display.newRect( body, 0, 0, body_width, 1 )
	    sepr:setFillColor( 0.75 )
	    sepr.y = newcl.y + newcl.height        
	end
	body[body.numChildren].isVisible = false 	-- убираем последнюю линию
  
	-- slpanel --------------------------------------------
	
	local sp_height = body.height + header.height
	sp_height = sp_height <= 250 and sp_height or 250
  
	local slpanel_bg = display.newRoundedRect( 0, 0, body_width, sp_height, 5 )
	slpanel_bg:setFillColor( 228 / 255, 228 / 255, 228 / 255 )
	slpanel_bg:setStrokeColor( 0.75 )
	slpanel_bg.strokeWidth = 1

	slpanel_bg.isVisible = false    
	body.isVisible = false    
	 
	local options = { 
	    -- general
	    id = "lang",
	    width = header.width,
	    height = sp_height,
	    bg = slpanel_bg,
	    speed = 250,
	    inEasing = easing.outCubic,
	    outEasing = easing.outCubic,
	    isScrollable = true,
	    topScrollPad = 5,
	    bottomScrollPad = 5,            
	    onPress = function( event )
	    end,
	    onStart = function( panel )
		  if panel.completeState == "hidden" then
			slpanel_bg.isVisible = true    
			body.isVisible = true    
			panel:toFront() 
			touchpad.isHitTestable = true
		  elseif panel.completeState == "shown" then
			touchpad.isHitTestable = false
		end
	    end,
	    headerHeight = header.height-8.5,
	    headerColor = { 0.8, 0 }
	}
	slpanel = widget.newSlidingPanel( options )
	slpanel.anchorChildren = true
	slpanel.header:insert( header )
	slpanel.header.y = slpanel.header.y + 2
	slpanel.bg.y = slpanel.bg.y + 2
	slpanel:place( body, { top = 5 } )
  
	function slpanel:select( name )
		for i=1, body.numChildren do
			if body[i].name == name then
				body[i]:turnOn() 
				title.text = name
			elseif body[i].name then
				body[i]:turnOff() 
			end
		end
	end
	
	slpanel:hide( 0 )
	return slpanel
end

function scene:keyback(event)
    local prevscene = composer.getSceneName("previous")
    local options = {
	  effect = "fade",
	  time = 200,
	  params = {}
    }
	  --composer.gotoScene( prevscene, options )
	  --CreateSideBar()
	  mySidebar:press("list_main")
	  Keyback:setFocus(nil)
end

function scene:create(event)
    local sceneGroup = self.view
    createHeader()
    Keyback:setFocus(self)

--- фон

    local bg = display.newRect(sceneGroup, 0, 0, _Xa, _Ya + display.topStatusBarContentHeight)
    bg:setFillColor(228 / 255, 228 / 255, 228 / 255)
    bg.x = _mX
    bg.y = _mY

--- язык

	local ver_label=display.newText({text = "ver.".._G.appVersion, 
			font = "HelveticaNeueCyr-Light", 
			fontSize = 12
		})

	ver_label.x = _mX
	ver_label.anchorY = 0
	ver_label.y = general_top + 10
	ver_label:setFillColor(0)
	sceneGroup:insert(ver_label)

	local lang_title = createTitle(translations["Language"][_G.Lang].." (".._G.userCountry..")")
	lang_title.x = _mX
	lang_title.anchorY = 0
	lang_title.y = general_top + 10 +ver_label.height+10
	sceneGroup:insert( lang_title )

	local lang_names = {}
	for lang, name in pairs( translations.Languages ) do
		langs[lang] = name
		langs[name] = lang
		lang_names[name] = false
	end
	local user_lang = ldb:getSettValue( "guest_lang" ) or _G.Lang
	lang_names[langs[user_lang]] = true

	local lang_panel = createLanguagePanel( lang_names, function( event )
		if event.state == "on" then
			settings.lang = langs[event.newName]
			if settings.lang ~= _G.Lang then 
				ldb:setSettValue( "guest_lang", settings.lang )
				_G.Lang = settings.lang
				composer.removeScene( "actions.settings_scene", false )
				CreateSideBar( "settings" )
				composer.gotoScene( "actions.settings_scene", { effect = "fade", time = 100 } )                
			end 
		elseif event.state == "off" then
		end
	end )
	lang_panel.y = lang_title.y + lang_panel.height*0.5 + _Ya*0.05
	sceneGroup:insert( lang_panel )
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if phase == "will" then
	    Keyback:setFocus( self )
	elseif phase == "did"then
		composer.removeHidden( false )
		composer.removeScene( "actions.rests_list_scene", false )
		
	end
  end

function scene:destroy(event)
    local sceneGroup = self.view
end

scene:addEventListener("create", scene)
scene:addEventListener( "show", scene )
scene:addEventListener("destroy", scene)

return scene
