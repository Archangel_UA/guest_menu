local SlideView = require "ui.SlideView"
local connection = require "lib.connection"
local ovlib = require "ui.OrderView"
-- 
local rdb = require "lib.db_remote"
local uilib = require "ui.uilib"

local scene = composer.newScene()

local last_slide_ordid = nil			-- ordid последнего активного слайд

function scene:createHeader(only_empty)
	local only_empty=only_empty
	if only_empty==nil then
		only_empty=false
	end
	local v_state="off"
	if scene.ovm.show_closed==true then
		v_state="on"
	end

	local headerItems = {
		{                
			id = "rest_title",
			label = utf8sub(userProfile.SelectedRestaurantData.title,1,18),
			fontSize = 15,
			labelColor = { 1,1,1 },
			params = {},
			switch = true,
			turnOn = function( event ) mySidebar:press("about") end
		}    		             
	}
	if only_empty==false then
		headerItems[#headerItems+1] = 
		{                
			id = "new_order",
			icon = "newOrder",
			iconWidth = 27,
			iconHeight = 27,
			params = {},
			switch = true,
			right = 20,
			turnOn = function( event )
				print( event.target )
				ldb:createNewOrder()
				scene.slideView:removeSelf()
				scene.slideView = nil
				userProfile.activeOrderID = 0
				last_slide_ordid = nil
				--userProfile.SelectedRestaurantData = nil
				scene.ovm:refresh()
			end
		}	
	end		
	headerItems[#headerItems+1] = 
		{                
			id = "closed_orders",
			icon = "closed_orders",
			iconWidth = 27,
			iconHeight = 27,
			params = {},
			state = v_state,
			switch = true,
			right = 60,
			turnOn = function( event )
				--print( event.target )
				scene.ovm.show_closed=true
				scene.ovm:refresh()
			end,
			turnOff = function( event )
				--print( event.target )
				scene.ovm.show_closed=false
				scene.ovm:refresh()
			end			
		}    		             	


	CreateHeader( headerItems ) 
end

function scene:createEmptyScreen( mode )
	print("scene:createEmptyScreen")
	local translat = translations.OrderTxt_NoOrders[userProfile.lang]
	userProfile.activeOrderID=0
	local label_text = ""
	local butn_label = ""
	local goto = ""
	if mode == 1 then
		label_text = translat[1]
		butn_label = translat[2]
		goto = "list_main"
	elseif mode == 2 then
		label_text = translat[3]
		if userProfile.SelectedRestaurantData then
			butn_label = translat[4]
			goto = "menu"
		else
			butn_label = translat[2]
			goto = "list_main"
		end
	end

	local label = display.newText( 
		label_text,
		0,0,
		"HelveticaNeueCyr-Light", 15 
	)
	label:setFillColor( 0,0,0 )
	label.x = _mX
	label.y = _mY-100
	self.view:insert( label )    

	local gobutton = widget.newButton({
		label = butn_label,
		labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0 } },
		fontSize = 14,
		font = "HelveticaNeueCyr-Medium",
		onPress = function( event )
			print( "############" )
			userProfile.activeOrderID=0
			ldb:createNewOrder()
			mySidebar:press( goto )
		end,
		--properties for a rounded rectangle button...
		shape="roundedRect",
		width = 230,
		height = 37,
		cornerRadius = 5,
		fillColor = { default={ 244/255, 64/255, 66/255 }, over={ 211/255,211/255,211/255 } },
		strokeColor = { default={ 244/255, 64/255, 66/255 }, over={ 0,0,0, 0.13 } },
		strokeWidth = 2        
	})
	gobutton.x = _mX
	gobutton.anchorY = 0
	gobutton.y = label.y + label.height*0.5 + 20
	self.view:insert( gobutton )
--	scene:createHeader()
end

----- OrderView listeners -----------------------------------------------------

local function onCreateOrdersDone( ovm )
	local initslide = {}
	for i=1, #ovm.oviews do
		if userProfile.activeOrderID and userProfile.activeOrderID > 0
		and ovm.oviews[i].lid == userProfile.activeOrderID then
			initslide[1] = i
		elseif last_slide_ordid and ovm.oviews[i].lid == last_slide_ordid then 
			initslide[2] = i
		end
	end
	scene:createOrdersSlideView( initslide[2] or initslide[1] or 1  )
	
	Spinner:stop()
end

local function onOrderSent( ovm, ov )
--	userProfile.activeOrderID = 0
	scene:createHeader()
end

local function onOrderDeleted( ovm, ov )
	userProfile.activeOrderID = ovlib:findLocalActiveOrder( 
		userProfile.SelectedRestaurantData.com_id,
		userProfile.SelectedRestaurantData.pos_id
	)

	scene.slideView:removeSelf()
	scene.slideView = nil
end

local function onOrderCopied()
	userProfile.activeOrderID = 0
	if scene.slideView then
		scene.slideView:removeSelf()
		scene.slideView = nil
		scene.ovm:refresh()
		Spinner:stop()
	end
end

local function onDataLoadError( self, reason )
	Spinner:stop()
	
	local headerItems = {}
	headerItems[1] = {              
		id = "my_orders",
		label = "Мои заказы",
		labelColor = { 1,1,1 },
		fontSize = 15,
		icon = "back",
		iconWidth = 7,
		iconHeight = 13,
		labelAnchor = { pos = "right", pad = 3 },
		center = -5,        
		params = {},
		switch = true,
		turnOn = function( event ) 
			local prevscene = composer.getSceneName( "previous" )
			local cases = {
				["actions.main_scene"] = "main",
				["actions.rests_list_scene"] = "list_main",
				["actions.my_cabinet_scene"] = "cabinet",
				["actions.rest_menu_scene"] = "menu",
				["actions.my_orders_scene"] = "orders",
				["actions.rest_about_scene"] = "about",
			}
			mySidebar:press( cases[prevscene] )
		end
	}
	CreateHeader( headerItems ) 
	
	local rs = uilib.createReloadScreen( reason, function()
		Spinner:start()
		self:createOrdersViews()
	end )
	scene.view:insert( rs )
end


----- SlideView listeners -----------------------------------------------------

local function loadSlide( slide, slideView )
	local num = slide.index_number
	slide.params = {
		autoscale = false,
		hasBg = true,
		bgColor = { 1, 1, 1 },
	}
	
	local ordview = scene.ovm.oviews[num] 
	if ordview then   
		print("ovlib:newOrderViewGUI")
		slide:insert( ovlib:newOrderViewGUI( ordview, _G.Lang ) )
		slide[slide.numChildren].slideview = slideView
		slide[slide.numChildren].numslide = num
		local ordsum = ordview:getOrderSum()
		local tipsfee = ordview:getTipsPerc() or 0.05
		local sum = ordsum*(1+tipsfee)
		slideView:setTabLabel( num, sum.._G.Currency, {0,0,0,0.3}  )
	end
end

local function unloadSlide( slide, slideView )
--print( "Was unloaded slider number ", slide.index_number )
	slide:removeSelf()
	slide = nil
end

local function showSlide( slide, slideView )
	local num = slide.index_number
	local ordview = scene.ovm.oviews[num]
	if ordview then
		local ordstate = ordview:getOrderState()	
		if ordstate ~= 1 and ordstate ~= 2 and ordstate ~= 3 then
			userProfile.activeOrderID = ordview.lid
		end
		slideView:setTabLabel( num, nil, {0,0,0,1} )
		last_slide_ordid = ordview.lid
	end
end

local function hideSlide( slide, slideView )
	local num = slide.index_number
	slideView:setTabLabel( num, nil, {0,0,0,0.3} )
end

--------------------------------------------------------------------------------

function scene:keyback( event )
	mySidebar:press( "list_main" )
	Keyback:setFocus( nil )
end

function scene:createOrdersSlideView( initslide )
	print("scene:createOrdersSlideView")
	if self.slideView then
		self.slideView:removeSelf( )
	end	
	if not userProfile.SelectedRestaurantData then
		self:createEmptyScreen( 1 )
		if self.header then 
			self.header:removeSelf()
			self.header = nil
		end
		CreateBaseHeader()
		return 
	elseif #self.ovm.oviews == 0 then 
		self:createEmptyScreen( 2 )
		if self.header then 
			self.header:removeSelf()
			self.header = nil
		end
		--CreateBaseHeader()
		scene:createHeader(true)
		return 
	end

	CreateSideBar( "orders" )
	scene:createHeader()
	
	local tabbar_height = common_tab_height+7

	self.slideView = widget.newSlideView({ 
		y = 0,
		x = _mX,
		height = _Ya-headerHeight-tabbar_height,
		numSlides = #self.ovm.oviews,
		initSlide = initslide or 1,
		leftPadding = 20,
		rightPadding = 20,
		timeMove = 400,

		loadSlide = loadSlide,
		unloadSlide = unloadSlide,
		showSlide = showSlide,
		hideSlide = hideSlide,
		-- reachedFirst = reachedFirst,
		-- reachedLast = reachedLast
	})
	self.slideView.anchorChildren = true
	self.slideView.anchorY = 0
	self.slideView.y = general_top - 2
	self.view:insert( self.slideView )        

	local tabs = {}
	local scale_factor = display.pixelWidth / display.actualContentWidth 
	local twidth = scale_factor >= 2.0 and display.actualContentWidth/3 or display.actualContentWidth/2
	twidth = #self.ovm.oviews <= 2 and display.actualContentWidth/2 or twidth
	local bgactive = {}
	local bgadef = {}
	local sepr = nil

	local ordsum = 0
	local tipsfee = 0
	local sum = 0

	for i=1, #self.ovm.oviews do
		bgactive = display.newGroup()
		bgactive.anchorChildren = true
		display.newRect( bgactive, 0, 0, twidth, tabbar_height )
		display.newImageRect( bgactive, "assets/orders/tabPointer.png", twidth+5, 7 )
		bgactive[2].anchorY = 1
		bgactive[2].y = tabbar_height*0.5

		bgadef = display.newGroup()
		bgadef.anchorChildren = true
		display.newRect( bgadef, 0, 0, twidth, tabbar_height )
		display.newImageRect( bgadef, "assets/orders/tabBottom.png", twidth+5, 7 )
		bgadef[2].anchorY = 1
		bgadef[2].y = tabbar_height*0.5        
		
		sepr = nil
		if i > 1 then sepr = display.newRect( 0, -2, 1, tabbar_height-7 ) end

		ordsum = self.ovm.oviews[i]:getOrderSum()
		tipsfee = self.ovm.oviews[i]:getTipsPerc()
		sum = ordsum*(1+tipsfee)        
		tabs[i] = {
			label = display.newText( sum.._G.Currency, 0, -3, "HelveticaNeueCyr-Medium", 18 ),            
			labelColor = { 0,0,0, 0.3 },
			bgDefault = bgadef,        
			bgActive = bgactive,
			separator = sepr,
			seprColor = { 0,0,0, 0.1 }                
		}
	end
	self.slideView:attachTabBar(
		{
			--y = 0,          
			height = tabbar_height,
			--bgDefColor = { 190/255, 190/255, 190/255 },
			--bgActColor = { 1, 1, 1 }
		},
		tabs
	)
	local currslide = self.slideView.currindex
	self.slideView:setTabLabel( currslide, nil, {0,0,0,1} )
end

--------------------------------------------------------------------------------

-- "scene:create()"
function scene:create( event )
print( "ORDERS SCENE CREATE" )
	local sceneGroup = self.view
	Spinner:start()
   
   	local pos_id = userProfile.SelectedRestaurantData and userProfile.SelectedRestaurantData.pos_id
	self.ovm = ovlib:newOrdersViewManager({
		posID = pos_id,
		ordersType = "active",
		onCreateDone = onCreateOrdersDone,
		onOrderDeleted = onOrderDeleted,
		onOrderSent = onOrderSent,
		onOrderCopied = onOrderCopied,
		onDataLoadError = onDataLoadError
	})
	self.ovm:createOrdersViews()

	rdb:getGuestBalance( ldb:getSettValue( "guest_id" ), function( state, balance )
		if state == "success" then
			userProfile.balance = balance
		end
	end )		
end


-- "scene:show()"
function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	Keyback:setFocus( self )

	if phase == "will" then
		display.setDefault( "background",  1 )
		self.timerid = timer.performWithDelay( _ORDERS_UPDATE * 1000, function( event )
			--  если юзер вышел из сцены то отменяем фоновое обновление
			if not self.view then
				timer.cancel( self.timerid ); self.timerid = nil; return 
			-- если есть заказы запускаем фоновое обновление
			elseif self.slideView then 
				self.ovm:backgroundUpdate()
			end		
		end, -1 )
	end
end

function scene:refresh( )
	print("scene:refresh")
	if self.slideView then self.ovm:backgroundUpdate() end			
end

-- "scene:hide()"
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if phase == "will" then
		if self.timerid then timer.cancel( self.timerid ); self.timerid = nil end
		display.setDefault( "background",  228 / 255, 228 / 255, 228 / 255 )
	elseif phase == "did" then
		-- Called immediately after scene goes off screen.
	end
end


-- "scene:destroy()"
function scene:destroy( event )
print( "...DESTROY MY ORDERS" )
	local sceneGroup = self.view
	self.ovm:deleteAllOrdersViews()

	-- Called prior to the removal of scene's view ("sceneGroup").
	-- Insert code here to clean up the scene.
	-- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene