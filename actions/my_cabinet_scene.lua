local scene = composer.newScene()
-- 
local EnterModule = require "ui.EnterModule"
local rdb_bonus = require "lib.db_remote_bonuses"
local rdb = require "lib.db_remote"
local rdb_common=require "lib.db_remote_common"
local uilib = require "ui.uilib"
local mime = require("mime")
local i_w=100
local i_h=100

local widget = require("widget")
require("ui.datePickerWheel")

-- Create a forward reference for the widget.
local scrollview, datePicker

local function createHeader()
	local headerItems = {}

	headerItems[1] = {              
		id = "my_cabinet",
		label = translations["Sidebar_cabinet"][_G.Lang],
		labelColor = { 0,0.7 },
		fontSize = 20,
		icon = "backBlack",
		iconWidth = 7*1.2,
		iconHeight = 13*1.2,
		labelAnchor = { pos = "right", pad = 6 },
		center = 0,        
		switch = true,
		turnOn = function( event ) 
			local prevscene = composer.getSceneName("previous")
			mySidebar:press( _G.scenes_ids[prevscene] or "list_main" )
		end,
	}
  
	CreateHeader( headerItems, { color = {0,0} } ) 
end

local function createUserAvatar(avatar_photo)
	local width, height = 112, 112
	local buttons_pad = 10

	local mask_name = "assets/avatarMask.png"
	local unlframe_name = "assets/avatarFrameUnlocked.png"
	local lframe_name = "assets/avatarFrameLocked.png"

	local gb_sheet_options = {
		width = 52,
		height = 52,
		numFrames = 2,
		sheetContentWidth = 104,
		sheetContentHeight = 52
	}
	local gb_sheet_file = "assets/galleryButton.png"
	local cb_sheet_options = {
		width = 52,
		height = 52,
		numFrames = 2,
		sheetContentWidth = 104,
		sheetContentHeight = 52
	}
	local cb_sheet_file = "assets/cameraButton.png"

	-- initialization -----------------------
	
	local avatar = display.newGroup()
	avatar.anchorChildren = true
	avatar.state = "locked"

	local bg = display.newRect( avatar, 0, 0, _Xa, height+10 )
	bg:setFillColor( 228/255, 228/255, 228/255 )

	avatar.mask = graphics.newMask( mask_name )
	if avatar_photo then
		avatar.photo=avatar_photo
	else
		avatar.photo = display.newImage( avatar, "userAvatarPhoto.jpg", system.DocumentsDirectory, 0, 0 )
	end
	if avatar.photo then
		local scale_factor = height / avatar.photo.height
		avatar.photo.width = avatar.photo.width * scale_factor
		avatar.photo.height = avatar.photo.height * scale_factor 
	elseif not avatar.photo then
		avatar.photo = display.newImageRect( avatar, "assets/noAvatar.jpg", width, height )
	end
	avatar.photo:setMask( avatar.mask )

	avatar.lframe = display.newImageRect( avatar, lframe_name, width+3, height+3 )
	avatar.unlframe = display.newImageRect( avatar, unlframe_name, width+3, height+3 )
	avatar.unlframe.isVisible = false

	-- interfase ---------------------------

	function avatar:setState( state )
		if self.state == state then return end
		
		if state == "unlocked" then
			local bgallery_sheet = graphics.newImageSheet( 
				gb_sheet_file, gb_sheet_options 
			)
			self.bgallery = widget.newButton({
				sheet = bgallery_sheet,
				defaultFrame = 1,
				overFrame = 2,		        
				onRelease = function( event )
					if media.hasSource( media.PhotoLibrary ) then
						media.selectPhoto({ 
							mediaSource = media.PhotoLibrary, 
							listener = self,
							destination = { 
								baseDir=system.DocumentsDirectory, 
								filename="userAvatarPhotoTemp.jpg", 
								type="image"
							} 
						})
					else
--						self:sendPhotoToCloud(120,100)
					   native.showAlert( _G.appTitle, "This device does not have a photo library.", { "OK" } )
					end
				end
			})
			self:insert( self.bgallery, true )
			self.bgallery.anchorX = 1
			self.bgallery.x = -self.lframe.width*0.5 - buttons_pad

			local bcamera_sheet = graphics.newImageSheet( 
				cb_sheet_file, cb_sheet_options 
			)
			self.bcamera = widget.newButton({
				sheet = bcamera_sheet,
				defaultFrame = 1,
				overFrame = 2,		        
				onRelease = function( event )
					local hasAccessToCamera, hasCamera=media.hasSource( media.Camera )
					if hasAccessToCamera then
						media.capturePhoto({ 
							listener = self,
							destination = { 
								baseDir=system.DocumentsDirectory, 
								filename="userAvatarPhotoTemp.jpg", 
								type="image"
							} 
						})
					else
						if hasCamera == true and native.canShowPopup( "requestAppPermission" )  then
							native.showPopup( "requestAppPermission", { appPermission="Camera" } )
						elseif	hasCamera == true then
							native.showAlert( _G.appTitle, "This device does not have access to camera.", { "OK" } )
					    else
					   		native.showAlert( _G.appTitle, "This device does not have a camera.", { "OK" } )
					    end
					end
				end,
			})
			self:insert( self.bcamera, true )
			self.bcamera.anchorX = 0
			self.bcamera.x = self.lframe.width*0.5 + buttons_pad			 

			self.lframe.isVisible = false
			self.unlframe.isVisible = true
			self.state = state
		
		elseif state == "locked" then
			self.bgallery:removeSelf()
			self.bgallery = nil
			self.bcamera:removeSelf()
			self.bcamera = nil

			self.lframe.isVisible = true
			self.unlframe.isVisible = false
			self.state = state
		end
	end

	function avatar:save()
		_print("avatar:save")
		if self.temp_photo then
			local basedir = system.DocumentsDirectory
--			_print("0",self.photo)
			if self.photo then
			--	self.photo:removeSelf()
				self.photo = nil
			end
--			_print("1",self.temp_photo)
			--if self.temp_photo then
			--	self.temp_photo:removeSelf()
			--	self.temp_photo = nil
			--end
			_print("2")



			timer.performWithDelay( 50, function()

				--local results, reason = os.remove( system.pathForFile( "userAvatarPhoto.jpg", basedir  ) )
				-- results, reason = os.rename( 
				-- 	system.pathForFile( "userAvatarPhotoTemp.jpg", basedir ),
				-- 	system.pathForFile( "userAvatarPhoto.jpg", basedir )
				-- )


	           -- os.remove( system.pathForFile( nameFotoRow, system.DocumentsDirectory)) -- удаление временого фото 

				-- self.photo = display.newImage( self, 
				-- 	"userAvatarPhoto.jpg",
				-- 	system.DocumentsDirectory, 0,0 
				-- )
				
				-- self.photo.width = self.photo.width * scale_factor
				-- self.photo.height = self.photo.height * scale_factor 
				-- self.photo:setMask( self.mask )
				-- self:insert( self.numChildren-1, self.photo )

				self:sendPhotoToCloud(math.round( i_w ),math.round( i_h ))
			end )						
		end
	end

	function avatar:restore()
		if self.temp_photo then
			self.photo.isVisible = true
			self.temp_photo:removeSelf()
			self.temp_photo = nil
			timer.performWithDelay( 50, function()
				local basedir = system.DocumentsDirectory
				local results, reason = os.remove( system.pathForFile( "userAvatarPhotoTemp.jpg", basedir ) )
			end )    	
		end
	end

	----------------------------------------

	function avatar:completion ( event )     -- listener for media.selectPhoto
		if event.completed then
			self.photo.isVisible = false
			if self.temp_photo then
				self.temp_photo:removeSelf()
				self.temp_photo=nil
			end

				local nameFotoRow = "userAvatarPhotoTemp.jpg"
      			local rowHeight = 80 -- высота строки в таблице гостей 
            -- // привидение фото к нужному размеру
	            local image = display.newImage(nameFotoRow, system.DocumentsDirectory)
	       --     image.alpha=0
	            local w,h  =  image.width, image.height


	            local scale_factor = height / image.height
	            i_w = image.width * scale_factor
	            i_h = image.height * scale_factor
	            image.width = image.width * scale_factor  -- save small photo
	            image.height = image.height  * scale_factor
           
	            display.save( image, {  filename= "userAvatarPhoto.jpg",
	                                    baseDir=system.DocumentsDirectory,
	                                    isFullResolution=true,
	            })	            
	            image:removeSelf( )
	            image=nil

			self.temp_photo = display.newImage( self, 
				"userAvatarPhotoTemp.jpg",
				system.DocumentsDirectory, 0,0 
			)
			local scale_factor = height / self.temp_photo.height
			self.temp_photo.width = self.temp_photo.width * scale_factor
			self.temp_photo.height = self.temp_photo.height * scale_factor 
			self.temp_photo:setMask( self.mask )
		end
	end
	
	function avatar:downloadActualPhoto( guest_id )
		-- chech version of the photo
		-- download actual photo from cloud if it was changed
		-- replace current photo
		local function networkListenerLoadSmallPhoto(event)
            if event.phase == "ended" then
                if event.bytesTransferred > 0 then	
 --               	event.target.rotation = -90
                	self:insert(event.target)
                	userProfile.guestHasPhoto=true
					self.photo = event.target
					self.photo.x=0
					self.photo.y=0

					local scale_factor = height / self.photo.height
					self.photo.width = self.photo.width * scale_factor
					self.photo.height = self.photo.height * scale_factor 
					self.photo:setMask( self.mask )
				end
			end
		end
		if  userProfile.guestHasPhoto==false then
			local rowHeight=200
	        local headers = {}
	        headers["Content-Type"] = "application/json"
	        headers["Authorization"] = rdb_common:get_admin_authorization_string()

	        local params = {}
	        params.headers = headers
	        params.progress = "download"	
	                	if self.photo then
	                		self.photo:removeSelf( )
	                	--	self.photo = nil
	                	end	        	
			display.loadRemoteImage( "http://"..rdb_common:getconnectionstring().."/admin/users/photo/"..guest_id.."?size="..rowHeight.."x"..rowHeight, "GET",networkListenerLoadSmallPhoto, params,"userAvatarPhoto.jpg", system.DocumentsDirectory, rowHeight, rowHeight)
			_print( "...avatar:downloadActualPhoto" )
		end
	end

	function avatar:sendPhotoToCloud( widthPhoto,heightPhoto )
		_print("avatar:sendPhotoToCloud userProfile.guestHasPhoto",userProfile.guestHasPhoto)
	    local path = system.pathForFile( "userAvatarPhoto.jpg", system.DocumentsDirectory)
--	    local path = system.pathForFile( "logo2335.jpg", system.TemporaryDirectory)
	    local fileHandle = io.open( path, "rb" )
	    _print("after fileHandle")
	    if fileHandle then
	    	_print("2")
	        local data = fileHandle:read( "*a" )
	        _print("3")
	        io.close( fileHandle )
	        local function listenerPhoto(event)
	        	--_printResponse(event.data)
	            if event.data.code == "ok:" then
	            elseif event.data.code == "ok" then
	            --    table.remove(idWithoutPhoto,table.indexOf(idWithoutPhoto,dataGuestsById[guestId].guest_id))                             
	            else
	                native.showAlert( _G.appTitle, "Error while sending photo" , {"OK"} )
	            end
	        end
	        if not data then
	            return
	        end
	        local com_id=4
	        if userProfile.SelectedRestaurantData and userProfile.SelectedRestaurantData.com_id then
	        	com_id = userProfile.SelectedRestaurantData.com_id
	    	end 
	    	_print("4")
	       -- if  table.indexOf(idWithoutPhoto,dataGuestsById[guestId].guest_id) == nil then
	       local guest_id=ldb:getSettValue( "guest_id" )
	       if userProfile.guestHasPhoto==true and data~=nil then
	       		_print("update photo")
	            local sentTable = {
	            table_name = "guests_photos",
	            admin_auth=true,
	            listener = listenerPhoto, 
	            body = {
	                    ['guest_id'] = guest_id,
	                    ['com_id'] = com_id,
	                    ['guestp_filename'] = "ph"..guest_id,
	                    ['guestp_photo'] = mime.b64(data) ,
	                    ['guestp_photo_width'] = widthPhoto,
	                    ['guestp_photo_height'] = heightPhoto,
	                    ['guestp_photo_format'] = "jpg",
	                } 
	            }
	            rdb_common:setDataFromView(sentTable)
	            -- native.showAlert( "SENT FOTO 1", "set", {"OK"} )
	        else
	        	if data~=nil then
	            local sentTable = {
	            table_name = "guests_photos",	            
	            listener = listenerPhoto, 
	            admin_auth=true,
	            row = {
	                    ['guest_id'] = guest_id,
	                    ['com_id'] = com_id,
	                    ['guestp_filename'] = "ph"..guest_id,
	                    ['guestp_photo'] = mime.b64(data) ,
	                    ['guestp_photo_width'] = widthPhoto,
	                    ['guestp_photo_height'] = heightPhoto,
	                    ['guestp_photo_format'] = "jpg",
	            	} 
	        	}
	        	rdb_common:sendTableRow(sentTable)
	        	userProfile.guestHasPhoto=true
	        	end
	        end
    	end    

	end

	avatar:downloadActualPhoto(ldb:getSettValue( "guest_id" ))
	return avatar

end

local function createHonorificMenu( init_value )
	local items_names = { " ", "Miss", "Mrs", "Mr" }
	local hmenu = display.newGroup()
	hmenu.anchorChildren = true

	hmenu.init_value = init_value
	hmenu.curr_value = init_value

	-- header ---------------------------------------------
	
	local header = display.newGroup()

	local sheet_options = {
		width = 67,
		height = 20,
		numFrames = 2,
		sheetContentWidth = 134,
		sheetContentHeight = 20
	}
	local sheet_file = "assets/dropdownMenuHeader.png"

	local header_sheet = graphics.newImageSheet( sheet_file, sheet_options )
	local headerimg1 = display.newImage( header, header_sheet, 1 )
	local headerimg2 = display.newImage( header, header_sheet, 2 )
	headerimg2.isVisible = false

	hmenu.title = display.newText({
		parent = header,
		text = init_value or "",
		font = "HelveticaNeueCyr-Light", 
		fontSize = 13,
		align = "center"
	})
	hmenu.title:setFillColor( 0.2 )
	hmenu.title.x = -5

	-- body -----------------------------------------------

	local body = display.newGroup()
	local body_width = header.width - 4

	local body_bg = display.newImageRect( "assets/dropdownMenuBg.png", 63, 85 )
	body:insert( body_bg )

	local bleft = -body_bg.width*0.5
	local btop = -body_bg.height*0.5

	function hmenu:createMenuItem( item_name, ypos )
		local item = display.newGroup()
		item.anchorChildren = true

		local item_bg = display.newRect( item, 0, 0, body_bg.width-2, 15 )
		item_bg:setFillColor( 228/255, 228/255, 228/255 )

		local ileft = -item_bg.width*0.5

		item.label = display.newText({
			parent = item, 
			text = item_name, 
			font = "HelveticaNeueCyr-Light", 
			fontSize = 14
		})
		item.label:setFillColor( 0 )
		item.label.anchorX = 0
		item.label.x = ileft + 5

		item:addEventListener( "touch", function( event )
			if event.phase == "began" then
				event.target.isFocus = true
				display.getCurrentStage():setFocus( event.target )
			elseif event.phase == "moved" then
				if math.abs( event.y - event.yStart ) >= 10 then
					event.target.isFocus = false
					display.getCurrentStage():setFocus( nil )
					return false
				end
			elseif event.phase == "ended" or event.phase == "cancelled" then
				self.title.text = event.target.label.text
				self.curr_value = event.target.label.text
				event.target.isFocus = false
				display.getCurrentStage():setFocus( nil )
			end
			return true
		end )

		body:insert( item )
		item.y = ypos
	end

	local ypos = btop + 12
	for i=1, #items_names do
		hmenu:createMenuItem( items_names[i], ypos )
		ypos = ypos + body[body.numChildren].height + 4
	end

	-- slpanel --------------------------------------------
	
	local options = { 
		id = "honor_menu",
		width = header.width,
		height = body.height + 25,
		speed = 250,
		inEasing = easing.outCubic,
		outEasing = easing.outCubic,
		isScrollable = false,
		topScrollPad = 5,
		bottomScrollPad = 5,            
		onPress = function( event )
		end,
		onStart = function( panel )
			if panel.completeState == "hidden" then
				hmenu:createSubstrate()
				headerimg2.isVisible = true
				headerimg1.isVisible = false
			elseif panel.completeState == "shown" then
				if hmenu.substrate then
					hmenu.substrate:removeSelf( )
					hmenu.substrate = nil
				end
				headerimg2.isVisible = false
				headerimg1.isVisible = true
			end
		end,
		headerHeight = header.height-5,
		headerColor = { 0.8, 0 }
	}
	slpanel = widget.newSlidingPanel( options )
	slpanel.anchorChildren = true
	hmenu:insert( slpanel )

	slpanel.header:insert( header )
	slpanel.header.y = slpanel.header.y + 2
	slpanel:place( body )
	slpanel:hide(0)

	function hmenu:createSubstrate()
		self.substrate = display.newRect( _mX, _mY, _Xa, display.actualContentHeight )
		self.substrate:setFillColor( 0,0,0, 0 )
		self.substrate.isHitTestable = true
		self:insert( self.substrate )
		self:toFront() 
		self.substrate:addEventListener( "touch", function( event )
			if event.phase == "began" then
				slpanel:hide()
				display.getCurrentStage():setFocus( nil )
			end
			return false
		end )
	end

	function hmenu:getValue()
		return self.curr_value
	end

	function hmenu:setValue( value )
		self.title.text = value
		self.curr_value = value
	end    

	return hmenu
end

local function createFields( params  )
	local _Xa = display.actualContentWidth
	local _Ya = display.actualContentHeight
	local border = 1
	local brcolor= {0,0,0, 0.2}
	local bgcolor= {0,0,0, 0.1}
	local step = 8

	params = params or {}
	params.padLeft = params.padLeft or 5
	params.padRight = params.padRight or 5
	local fh = params.fieldsHeight or 20
	local fw = params.fieldsWidth or math.ceil( _Xa*0.6 )
	local fs = params.fieldsFontSize or 18
	local font = params.fieldsFont or { "HelveticaNeueCyr-Thin", "HelveticaNeueCyr-Light" }
	local row_height = params.rowHeight or 40
	local data = params.data   

	local rs = display.newGroup()
	rs.anchorChildren = true

	rs.listener = params.listener
	rs.data = {}
	rs.datavalid = { is = true, count = {}, mess = {} }
	rs.changed = false

	rs.rows = {}
	rs.rowtop = 0
	rs.rowbottom = row_height
	rs.rowleft = 0
	rs.rowcenter = row_height*0.5

	rs.messbox = display.newGroup()
	rs:insert( rs.messbox )

	rs.messages = {
		notMatch = translations["enter_messages"][_G.Lang][1],
		exist = translations["enter_messages"][_G.Lang][2],
		incorrect = translations["enter_messages"][_G.Lang][3],
		reged = translations["enter_messages"][_G.Lang][4],
		["error"] = translations["enter_messages"][_G.Lang][5],
		empty = translations["enter_messages"][_G.Lang][6],
		disconnection = translations["enter_messages"][_G.Lang][7]		
	}
	rs.messages.type = {
		guest_email = translations["enter_messages_type"][_G.Lang][1],
		guest_firstname = translations["enter_messages_type"][_G.Lang][2],
		guest_surname = translations["enter_messages_type"][_G.Lang][3],
		guest_phone = translations["enter_messages_type"][_G.Lang][4],
	}
	rs.messages.min = {
		guest_email = translations["enter_messages_min"][_G.Lang][1],
		guest_firstname = translations["enter_messages_min"][_G.Lang][2],
		guest_surname = translations["enter_messages_min"][_G.Lang][3],
		guest_phone = translations["enter_messages_min"][_G.Lang][4],
		guest_password = translations["enter_messages_min"][_G.Lang][5],
	}	
	
	rs.honorific = createHonorificMenu( data.guest_honorific )
	rs:insert( rs.honorific )
	rs.honorific.x = 0
	rs.honorific.anchorY = 0
	rs.honorific.y = 0

	rs.name = display.newGroup()
	rs:insert( rs.name )
	rs.name.anchorChildren = true
	rs.name.anchorY = 0
	rs.name.y = rs.honorific.y + 25 + step

	local names_pad = 10
	local names_width = (_Xa - 40 - names_pad) * 0.5

	rs.data["guest_firstname"] = data.guest_firstname
	rs.f_firstname = widget.newInputField({
		id = "guest_firstname",
		x = 0,
		y = 0,
		width = names_width,
		height = fh,
		font = font[1],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = 0,
		text = data.guest_firstname,
		placeholder = translations["name"][_G.Lang],
		inputType = "alpha",
		typeControl = true,
		symbolsMin = 1,
		align = "center",
		
		inputFont = font[2],
		inputOffsetX = -1,
		inputOffsetY = -2,      

		listener = function( event )
			event.targetY = rs.name.y
			rs:userInput( event )
		end 
	})
	rs.f_firstname:setDecor({
		allBorders = false,
		--bgColor = bgcolor,        
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	rs.f_firstname.x = rs.rowleft
	rs.f_firstname.y = rs.rowcenter
	rs.name:insert( rs.f_firstname )

	rs.data["guest_surname"] = data.guest_surname
	rs.f_surname = widget.newInputField({
		id = "guest_surname",
		x = 0,
		y = 0,
		width = names_width,
		height = fh,
		font = font[1],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = 0,
		text = data.guest_surname,
		placeholder = "фамилия",
		inputType = "alpha",
		typeControl = true,
		symbolsMin = 1,
		align = "center",
		
		inputFont = font[2],
		inputOffsetX = -1,
		inputOffsetY = -2,      

		listener = function( event )
			event.targetY = rs.name.y
			rs:userInput( event )
		end 
	})
	rs.f_surname:setDecor({
		allBorders = false,
		--bgColor = bgcolor,        
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	rs.f_surname.x = rs.f_firstname.x + rs.f_firstname.width*0.5 + rs.f_surname.width*0.5 + names_pad
	rs.f_surname.y = rs.rowcenter
	rs.name:insert( rs.f_surname )
	
	local pad = params.max_height - ((fh+step)*4 + rs.height)
	if pad > 40 then pad = 30
	elseif pad < 0 then pad = 5
	end 

	local nexti = 1
	rs.rows[nexti] = display.newGroup()
	rs:insert( rs.rows[nexti] )
	rs.rows[nexti].anchorChildren = true
	rs.rows[nexti].anchorY = 0
	rs.rows[nexti].y = rs.name.y + rs.name.height + pad
	
	rs.rows[nexti].icon = display.newImageRect( "assets/emailIcon.png", 20, 18 )
	rs.rows[nexti]:insert( rs.rows[nexti].icon )
	rs.rows[nexti].icon.anchorX = 0
	rs.rows[nexti].icon.x = rs.rowleft
	rs.rows[nexti].icon.y = rs.rowcenter - 2
 
	rs.data["guest_email"] = data.guest_email
	rs.f_email = widget.newInputField({
		id = "guest_email",
		x = 0,
		y = 0,
		width = fw,
		height = fh,
		font = font[1],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = 0,
		text = data.guest_email,
		placeholder = "Email",
		inputType = "email",
		typeControl = true,
		symbolsMin = 1,     
		
		inputFont = font[2],
		inputOffsetX = -1,
		inputOffsetY = -2,      

		listener = function( event )
			event.targetY = rs.f_email.parent.y
			rs:userInput( event )
		end     
	})
	rs.f_email:setDecor({
		allBorders = false,
		--bgColor = bgcolor,        
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	rs.f_email.x = rs.rowleft + rs.rows[nexti].icon.width + rs.f_email.width*0.5 + 10
	rs.f_email.y = rs.rowcenter
	rs.rows[nexti]:insert( rs.f_email )

	nexti = nexti + 1
	rs.rows[nexti] = display.newGroup()
	rs:insert( rs.rows[nexti] )
	rs.rows[nexti].anchorChildren = true
	rs.rows[nexti].anchorY = 0
	rs.rows[nexti].y = rs.rows[nexti-1].y + rs.rows[nexti-1].height + step

	rs.rows[nexti].icon = display.newImageRect( "assets/phoneIcon.png", 20, 18 )
	rs.rows[nexti]:insert( rs.rows[nexti].icon )
	rs.rows[nexti].icon.anchorX = 0
	rs.rows[nexti].icon.x = rs.rowleft
	rs.rows[nexti].icon.y = rs.rowcenter - 2 

	rs.data["guest_phone"] = data.guest_phone
	rs.f_tel = widget.newInputField({
		id = "guest_phone",
		x = 0,
		y = 0,
		width = fw,
		height = fh,
		font = font[2],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = 0,
		text = data.guest_phone,
		placeholder = translations["phone"][_G.Lang],
		inputType = "number",
		typeControl = true,
		symbolsMin = 1,     
		
		inputFont = font[2],
		inputOffsetX = -1,
		inputOffsetY = -2,      

		listener = function( event )
			event.targetY = rs.f_tel.parent.y
			rs:userInput( event )
		end 
	})
	rs.f_tel:setDecor({
		border = border,
		bColor = bcolor
	})
	rs.f_tel:setDecor({
		allBorders = false,
		--bgColor = bgcolor,        
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	rs.f_tel.x = rs.rowleft + rs.rows[nexti].icon.width + rs.f_tel.width*0.5 + 10
	rs.f_tel.y = rs.rowcenter
	rs.rows[nexti]:insert( rs.f_tel )   


	nexti = nexti + 1
	rs.rows[nexti] = display.newGroup()
	rs:insert( rs.rows[nexti] )
	rs.rows[nexti].anchorChildren = true
	rs.rows[nexti].anchorY = 0
	rs.rows[nexti].y = rs.rows[nexti-1].y + rs.rows[nexti-1].height + step

	rs.rows[nexti].icon = display.newImageRect( "assets/birthIcon.png", 20, 20 )
	rs.rows[nexti]:insert( rs.rows[nexti].icon )
	rs.rows[nexti].icon.anchorX = 0
	rs.rows[nexti].icon.x = rs.rowleft
	rs.rows[nexti].icon.y = rs.rowcenter - 2 
	data.guest_birthday=data.guest_birthday or ""
	rs.data["guest_birthday"] = data.guest_birthday
	rs.f_birthday = display.newText({
		text = data.guest_birthday,     
		x = 0,
		y = 0,
		width = fw-5,
		height = fh,
		font = font[2],
		fontSize = fs
		})
	rs.f_birthday:setFillColor( 0 )
	rs.f_birthday.x = rs.rowleft + rs.rows[nexti].icon.width + rs.f_birthday.width*0.5 + 15
	rs.f_birthday.y = rs.rowcenter
	rs.f_birthday.isActive=false

	function touchListener ( event )
		if event.target.isActive then
	    _print( "Touch phase : " , event.phase )
			native.setKeyboardFocus( nil )
	    if event.phase == "ended" then
	    	local d_t = rs.f_birthday.text
	    	-- _print(d_t)
	    	local year=nil
	    	local month=nil
	    	local day=nil
	    	if d_t~="" then
	    		year = string.sub(d_t,1,4)
	    		month = string.sub(d_t,6,7)
	    		day = string.sub(d_t,9,10)
		    end
		    year = year and tonumber( year ) or 2000
		    month = month and tonumber( month ) or 1
		    day = day and tonumber( day ) or 1
		    _print(year,month,day)
		    uilib.newDateBox({ 
				width = _Xa-60, height = _Ya*0.5,
				title = translations.birthday[_G.Lang],
				initDate = { year = year, month = month, day = day },
				listener = function( event )
					local date = event.date
					rs.f_birthday.text = date.year.."-"..date.month.."-"..date.day
				end
			})
			rs.changed = true
	    end
	    end
	    return true
	end
 

	rs.f_birthday:addEventListener( "touch", touchListener )
	rs.rows[nexti]:insert( rs.f_birthday )  

	nexti = nexti + 1
	rs.rows[nexti] = display.newGroup()
	rs:insert( rs.rows[nexti] )
	rs.rows[nexti].anchorChildren = true
	rs.rows[nexti].anchorY = 0
	rs.rows[nexti].y = rs.rows[nexti-1].y + rs.rows[nexti-1].height + step

	rs.rows[nexti].icon = display.newImageRect( "assets/countryIcon.png", 18, 18 )
	rs.rows[nexti]:insert( rs.rows[nexti].icon )
	rs.rows[nexti].icon.anchorX = 0
	rs.rows[nexti].icon.x = rs.rowleft
	rs.rows[nexti].icon.y = rs.rowcenter - 2
	rs.rows[nexti].icon.alpha=1

	local countr=""
	if CountriesList[_G.userCountry]==nil then countr="Unknown"
	else countr=CountriesList[_G.userCountry][_G.Lang].cntr_name
	end
	rs.f_country = display.newText({
		text = countr,
		x = 0, y = 0,
		width = fw,
            font = font[1],
            fontSize = fs,
		align = "left"
	})
	rs.f_country.fill = { 0.15, 0.52, 0.97 }
	rs.rows[nexti]:insert( rs.f_country, true )
	rs.f_country.x = rs.rowleft + rs.rows[nexti].icon.width + rs.f_country.width*0.5 + 11
	rs.f_country.y = rs.rowcenter
	rs.f_country:addEventListener( "tap", function(e)
		if e.target.isActive then
			if scene.view.cntrbox then
				params.parentGr.cntrbox:select( _G.current_country )
				params.parentGr.cntrbox.isVisible = true
			end
		return true
		end
	end )
	rs.f_country.cntr_id = data.cntr_id

	ui_elm:createCountryBox(params.parentGr, function( e )
				if e and e.country then
					rs.f_country.text = e.country.cntr_name
					rs.f_country.cntr_id =  e.country.cntr_id
					rs.changed = true
				end
			end )

	local secureDotSheet = graphics.newImageSheet( "assets/passwordDot.png", {
		width = 7,
		height = 7,
		numFrames = 1,
		sheetContentWidth = 7,
		sheetContentHeight = 7
	})  

	nexti = nexti + 1
	rs.rows[nexti] = display.newGroup()
	rs:insert( rs.rows[nexti] )
	rs.rows[nexti].anchorChildren = true
	rs.rows[nexti].anchorY = 0
	rs.rows[nexti].y = rs.rows[nexti-1].y + rs.rows[nexti-1].height + step

	rs.rows[nexti].icon = display.newImageRect( "assets/keyIcon.png", 18, 20 )
	rs.rows[nexti]:insert( rs.rows[nexti].icon )
	rs.rows[nexti].icon.anchorX = 0
	rs.rows[nexti].icon.x = rs.rowleft
	rs.rows[nexti].icon.y = rs.rowcenter - 2 

	rs.f_password = widget.newInputField({
		id = "guest_password",
		x = 0,
		y = 0,
		width = fw,
		height = fh,
		font = font[1],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = -0.01,
		placeholder = translations["new_password"][_G.Lang],
		inputType = "password",
		secureDotSheet = secureDotSheet,
		symbolsMin = 5,     
		
		inputFont = font[2],
		inputOffsetX = -0.035,
		inputOffsetY = 0.02,        

		listener = function( event )
			event.targetY = rs.f_password.parent.y
			rs:userInput( event )
		end     
	})
	rs.f_password:setDecor({
		allBorders = false,
		--bgColor = bgcolor,        
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	rs.f_password.x = rs.rowleft + rs.rows[nexti].icon.width + rs.f_password.width*0.5 + 10
	rs.f_password.y = rs.rowcenter
	rs.rows[nexti]:insert( rs.f_password)

	nexti = nexti + 1
	rs.rows[nexti] = display.newGroup()
	rs:insert( rs.rows[nexti] )
	rs.rows[nexti].anchorChildren = true
	rs.rows[nexti].anchorY = 0
	rs.rows[nexti].y = rs.rows[nexti-1].y + rs.rows[nexti-1].height + step

	rs.rows[nexti].icon = display.newImageRect( "assets/keyIcon.png", 18, 20 )
	rs.rows[nexti]:insert( rs.rows[nexti].icon )
	rs.rows[nexti].icon.anchorX = 0
	rs.rows[nexti].icon.x = rs.rowleft
	rs.rows[nexti].icon.y = rs.rowcenter - 2 

	rs.f_reppassw = widget.newInputField({
		id = "guest_password",
		x = 0,
		y = 0,
		width = fw,
		height = fh,
		font = font[1],
		fontSize = fs,
		padLeft = params.padLeft, 
		padRight = params.padRight,
		offsetY = -0.01,
		placeholder = translations["new_password_1_more"][_G.Lang],
		inputType = "password",
		secureDotSheet = secureDotSheet,
		symbolsMin = 5,         
		
		inputFont = font[2],
		inputOffsetX = -0.035,
		inputOffsetY = 0.02,        

		listener = function( event )
			event.targetY = rs.f_reppassw.parent.y
			rs:userInput( event )
		end     
	})
	rs.f_reppassw:setDecor({
		allBorders = false,
		--bgColor = bgcolor,        
		bordersWidth = border,
		bordersColor = brcolor,
		bottomBorder = { 0, 1 },
		-- leftBorder = { 0, 1 },
		-- rightBorder = { 0, 1 }
	})
	rs.f_reppassw.x = rs.rowleft + rs.rows[nexti].icon.width + rs.f_reppassw.width*0.5 + 10
	rs.f_reppassw.y = rs.rowcenter
	rs.rows[nexti]:insert( rs.f_reppassw)

	rs.honorific:toFront()

	function rs:enableVerticalScroll( limit )
		self.defaultpos = self.y
		self.limitpos = self.y - limit 
		function self:touch( event )
			if event.phase == "began" then
				self.prevtouch = event.y
			elseif event.phase == "moved" then
				self.prevtouch = self.prevtouch or 0
				self.lastdist = event.y - self.prevtouch
				if math.abs( self.lastdist ) >= 5 then
					display.getCurrentStage():setFocus( event.target )
				end
	
				if self.y + self.lastdist > self.defaultpos then
					self.y = self.defaultpos
				elseif self.y + self.lastdist < self.limitpos then
					self.y = self.limitpos
				else self.y = self.y + self.lastdist
				end
				self.prevtouch = event.y
				
			elseif event.phase == "ended" or event.phase == "cancelled" then
				self.lastdist = self.lastdist or 0
				if math.abs( self.lastdist ) >= 3 then
					local to = self.limitpos
					if self.lastdist > 0 then to = self.defaultpos end

					self.tween = transition.to( self, {
						time=500, y=to, transition=easing.outExpo, 
					})
				end

				display.getCurrentStage():setFocus( nil )   
			end
		end
		
		self:addEventListener( "touch" )
		self.is_scrollable = true
	end

	function rs:disableVerticalScroll()
		if self.is_scrollable then
			self.y = self.defaultpos 
			self:removeEventListener( "touch" ) 
		end
		self.is_scrollable = false
	end

-------------------------------------------------------------------------

	function rs:userInput( event )
		local ierror = event.inputError
		local id = event.target.id
		local text = event.text

		if event.phase == "will" then
			scrollview:activate( event.targetY )
		elseif event.phase == "ended" then
			self:dataValid( ierror, id, text )
			scrollview:deactivate()
		elseif event.phase == "submitted" then
			native.setKeyboardFocus( nil )
			self:dataValid( ierror, id, text )
			scrollview:deactivate()
		end

		self.changed = true
	end

	function rs:dataValid( ierror, id, text )
		local count = self.datavalid.count
		local mess = self.datavalid.mess
		self.datavalid.is = false

		if ierror == "type" then
			count[id] = 1
			mess[id] = "type"
		elseif ierror == "min" then
			count[id] = 1
			mess[id] = "min"    
		elseif not ierror then 
			count[id] = 0
			self.data[id] = text
			mess[id] = nil
		end

		if id == "guest_email" and not ierror then
			if not self.data.guest_password then
				if self.data.guest_email ~= data.guest_email then
					count["needPassword"] = 1
					mess["needPassword"] = true
				else count["needPassword"] = 0
					mess["needPassword"] = nil
				end
			else
				count["needPassword"] = 0
				mess["needPassword"] = nil
			end   
		end

		if id == "guest_password" then 
			if self.f_password:getText() ~= self.f_reppassw:getText() then
				count["notMatch"] = 1
				mess["notMatch"] = true
			else count["notMatch"] = 0
				mess["notMatch"] = nil
				count["needPassword"] = 0
				mess["needPassword"] = nil
				if not self.data.guest_email then
					self.data.guest_email = data.guest_email
				end
			end
		end

		if id == "guest_firstname" or id == "guest_surname" then 
			self.data.guest_fullname = self.f_firstname:getText().." "..self.f_surname:getText()
		end

		local control = 0
		for k,v in pairs( count ) do control = control + v end
		if control == 0 then 
			self.datavalid.is = true
			self.datavalid.mess = {}
		end
	end

	function rs:sendData( listener )
		local honorific_value = self.honorific:getValue()
		if data.guest_honorific ~= honorific_value then
			self.changed = true
			self.data.guest_honorific = honorific_value
		end

		if self.changed and self.datavalid.is then
			-- _print("FFFFFFFFFFFFFFFFFF",self.f_birthday.text)
			self.data.guest_birthday = self.f_birthday.text
			self.data.guest_id = data.guest_id
			self.data.cntr_id = self.f_country.cntr_id
			local check_email = true
			if not self.data.guest_email or 
			self.data.guest_email == data.guest_email then
				check_email = false
			end
			Spinner:start()
			rdb:changeGuestData( check_email, self.data, function( status )
				Spinner:stop()
				self:handleNetRespond( status, listener )
			end )
		elseif not self.datavalid.is then self:sendMessage()
		else listener( "success", data )
		end
	end

	function rs:removeMessBox()
		self.messbox:removeSelf()
		self.messbox = { numChildren = 0 }
	end

	function rs:sendMessage( tag )
		local ypos = _Ya
		local mess = ""

		if not tag then
			for k,v in pairs( self.datavalid.mess ) do
				if v then 
					if type(v) == "string" then mess = self.messages[v][k]
					else mess = self.messages[k]
					end
					break
				end
			end
		else mess = self.messages[tag]
		end

		if not mess or mess == "" then mess = self.messages["empty"] end

		if self.messbox.numChildren > 0 then 
			timer.cancel( self.messbox.timer_handle )
			self.messbox:removeSelf() 
		end

		self.messbox = display.newGroup()
		self.parent:insert( self.messbox )
		self.messbox.x = _mX
		self.messbox.y = _mY
		local messbox = self.messbox 

		local text = display.newText({
			parent = messbox,
			text = mess,     
			x = 0,
			y = 0,
			width = _Xa*0.7,
			font = "HelveticaNeueCyr-Medium",   
			fontSize = 14
		})
		text:setFillColor( 1, 1, 1 )

		local box = display.newRoundedRect( messbox, 
			0, 0, 
			text.width + 30, text.height + 30, 
			15 
		)
		box:setFillColor( 0,0,0, 0.8 )
		box:toBack()

		self.messbox.timer_handle = timer.performWithDelay( 1500, function()
			self:removeMessBox()
		end )
	end

	function rs:handleNetRespond( status, listener )
		if status == "success" then
			for k,v in pairs( self.data ) do
				data[k] = v
			end
			ldb:saveGuestData( data )
			mySidebar.user_info[1].text = data.guest_fullname
			self.changed = false
			listener( status, data )
			self:restore()
		else -- error, disconnection, denied
			self:sendMessage( status )
		end
	end

	function rs:setEditable( param )
		self.honorific.isVisible = param
		self.name.isVisible = param
		self.f_email:setEditable( param )
		self.f_tel:setEditable( param )
		self.f_birthday.isActive=param
		self.f_country.isActive=param
		self.rows[5].isVisible = param
		self.rows[6].isVisible = param
		
		if param == true then
			local border_params = {         
				allBorders = false,
				bordersWidth = border,
				bordersColor = brcolor,
				bottomBorder = { 0, 1 },
			}
			self.f_email:setDecor( border_params )
			self.f_tel:setDecor( border_params )
			-- self:enableVerticalScroll( self.height*0.5 )
		elseif param == false then
			self.f_email:setDecor({ allBorders = true })
			self.f_tel:setDecor({ allBorders = true })
			-- self:disableVerticalScroll()
		end
	end

	function rs:restore()
		self.honorific:setValue( data.guest_honorific )
		self.f_firstname:setText( data.guest_firstname )
		self.f_surname:setText( data.guest_surname )
		self.f_email:setText( data.guest_email )
		self.f_tel:setText( data.guest_phone )
		self.f_password:clear()
		self.f_reppassw:clear()

		self.changed = false
		self.data = {}
		self.datavalid = { is = true, count = {}, mess = {} }
	end
	
	rs:setEditable( false )

	return rs
end

local function createFullNameLabel( data )
	local fnl = display.newGroup()
	fnl.anchorChildren = true

	local bg = display.newRect( fnl, 0, 0, _Xa, 22 )
	bg.alpha = 0
	
	local params = {
		text = "",     
		x = 0,
		y = 0,
		font = "HelveticaNeueCyr-Thin",   
		fontSize = 18
	}
	local pad = 5

	---------------------------------------------

	function fnl:createLabel( params )
		local label = display.newText( params )
		self.labels:insert( label )
		label.anchorX = 0
		label:setFillColor( 0 )
	end

	function fnl:init( data )
		self.labels = display.newGroup()
		self.labels.anchorChildren = true
		if data.guest_honorific ~= "" then
			params.font = "HelveticaNeueCyr-Thin"
			params.text = data.guest_honorific or "" 
			self:createLabel( params )
		end
		params.font = "HelveticaNeueCyr-Light"
		params.text = data.guest_firstname or ""
		self:createLabel( params )
		params.text = data.guest_surname or ""
		self:createLabel( params )

		local labels = self.labels
		for i=2, labels.numChildren do
			labels[i].x = labels[i-1].x + labels[i-1].width + pad
		end
		self:insert( labels ) 
	end

	-- interface --------------------------------

	function fnl:update( newdata )
		self.labels:removeSelf()
		self.labels = nil
		self:init( newdata )
	end

	-- initialization ---------------------------

	fnl:init( data ) 

	return fnl
end

local function createBalanceLabel( guest_id )
	local bl = display.newGroup()
	bl.anchorChildren = true
	
	local label = display.newText( bl,
		translations["balance"][_G.Lang],
		0,0,
		"HelveticaNeueCyr-Thin", 15 
	)
	label:setFillColor( 0 )  
	local sepr = display.newRect( bl, 0, 0, 1, 13 )
	sepr.alpha = 0.3
	sepr.x = label.x + label.width*0.5 + 12
	sepr:setFillColor( 0 )        

	local balance = "-"
	if userProfile.SelectedRestaurantData then
		local com_id = userProfile.SelectedRestaurantData.com_id
		balance = userProfile.balance[com_id] or balance
	end
	local amount = display.newText( bl,
		balance.._G.Currency,
		0,0,
		"HelveticaNeueCyr-Light", 15
	)
	amount.anchorX = 0
	amount.x = sepr.x + 12
	amount:setFillColor( 0 )
	rdb:getGuestBalance( guest_id, function( state, balance,bonus_types )
		if state == "success" then
			if userProfile.SelectedRestaurantData then
				local com_id = userProfile.SelectedRestaurantData.com_id
				amount.text = balance[com_id] or "-"
				amount.text = amount.text.._G.Currency
			end
			userProfile.balance = balance
			userProfile.bonus_types = bonus_types
		end
	end )
	bl.alpha=0
	return bl
end

local function createEditButton( listener )
	local sheet_options = {
		width = 62,
		height = 62,
		numFrames = 2,
		sheetContentWidth = 124,
		sheetContentHeight = 62
	}
	local sheet_file = "assets/editButton.png"
	local sheet = graphics.newImageSheet( sheet_file, sheet_options )	

	local edit = widget.newButton({
		sheet = sheet,
		defaultFrame = 1,
		overFrame = 2,	    
		onRelease = listener
	})
	
	return edit
end

local function createBalanceButton( listener )
	local sheet_options = {
		width = 62,
		height = 62,
		numFrames = 2,
		sheetContentWidth = 124,
		sheetContentHeight = 62
	}
	local sheet_file = "assets/balanceButton.png"
	local sheet = graphics.newImageSheet( sheet_file, sheet_options )   

	local history = widget.newButton({
		sheet = sheet,
		defaultFrame = 1,
		overFrame = 2,      
		onRelease = listener
	})
	
	return history
end

local function createHistoryButton( listener )
	local sheet_options = {
		width = 62,
		height = 62,
		numFrames = 2,
		sheetContentWidth = 124,
		sheetContentHeight = 62
	}
	local sheet_file = "assets/historyButton.png"
	local sheet = graphics.newImageSheet( sheet_file, sheet_options )   

	local history = widget.newButton({
		sheet = sheet,
		defaultFrame = 1,
		overFrame = 2,      
		onRelease = listener
	})
	
	return history
end

local function createLogoutButton()
	local sheet_options = {
		width = 52,
		height = 52,
		numFrames = 2,
		sheetContentWidth = 104,
		sheetContentHeight = 52
	}
	local sheet_file = "assets/logoutButton.png"
	local sheet = graphics.newImageSheet( sheet_file, sheet_options )	

	local logout = widget.newButton({
		sheet = sheet,
		defaultFrame = 1,
		overFrame = 2,	    
		onRelease = function( event )
			ldb:clearGuestData()
			userProfile.SelectedRestaurantData = nil
			userProfile.activeOrderID = 0
			CreateBaseHeader()
			CreateSideBar()	
--			composer.gotoScene( "actions.main_scene" )
			composer.gotoScene( "actions.rests_list_scene" )
		end
	})
	return logout
end

local function createDecisionButtons( listener )
	local dbuttons = display.newGroup()
	dbuttons.anchorChildren = true

	local confirm_sheet_options = {
		width = 62,
		height = 62,
		numFrames = 2,
		sheetContentWidth = 124,
		sheetContentHeight = 62
	}
	local confirm_sheet_file = "assets/confirmButton.png"
	local confirm_sheet = graphics.newImageSheet( confirm_sheet_file, confirm_sheet_options )	

	local confirm = widget.newButton({
		sheet = confirm_sheet,
		defaultFrame = 1,
		overFrame = 2,	    
		onRelease = function( event )
			event.name = "confirm"
			event.group = dbuttons 
			listener( event )
		end
	})
	dbuttons:insert( confirm, true )

	local cancel_sheet_options = {
		width = 62,
		height = 62,
		numFrames = 2,
		sheetContentWidth = 124,
		sheetContentHeight = 62
	}
	local cancel_sheet_file = "assets/cancelButton.png"
	local cancel_sheet = graphics.newImageSheet( cancel_sheet_file, cancel_sheet_options )	

	local cancel = widget.newButton({
		sheet = cancel_sheet,
		defaultFrame = 1,
		overFrame = 2,	    
		onRelease = function( event )
			event.name = "cancel"
			event.group = dbuttons 
			listener( event )
		end
	})
	dbuttons:insert( cancel, true )
	cancel.anchorX = 0
	cancel.x = confirm.width*0.5 + 50

	return dbuttons
end

local function createBox( name, width, height, onClose )
	local hbox = uilib.newBox( width, height, onClose )
	
	local title = display.newText({
		parent = hbox,
		text = name,
		width = hbox.bg.width*0.8,
		font = "HelveticaNeueCyr-Medium", 
		fontSize = 15,
		align = "center"
	})
	title:setFillColor( 0.1 )
	title.x = hbox.bg.x
	title.anchorY = 0
	title.y = hbox.bg.y - hbox.bg.height*0.5 + 10
			
	local sepr1 = display.newLine( hbox, 
		hbox.bg.x - hbox.bg.width*0.5, title.y + title.height + 10,  
		hbox.bg.x + hbox.bg.width*0.5, title.y + title.height + 10
	)
	sepr1:setStrokeColor( 0,0,0, 0.1 )
	sepr1.strokeWidth = 1.5

	local btn_close = widget.newButton({
		label = translations.CloseBtn[userProfile.lang],
		labelColor = { default={ 1, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
		fontSize = 14,
		font = "HelveticaNeueCyr-Medium",
		--properties for a rounded rectangle button...
		shape="roundedRect",
		width = hbox.bg.width * 0.6,
		height = 37,
		cornerRadius = 5,
		fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
		strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
		strokeWidth = 2,
		onRelease = function( event )
			--onClose()
			hbox:removeSelf()
			hbox = nil
		end     
	})
	hbox:insert( btn_close, true )
	btn_close.x = hbox.bg.x
	btn_close.anchorY = 1
	btn_close.y = hbox.bg.y + hbox.bg.height*0.5 - 15
	hbox:insert( btn_close )

	local sepr2 = display.newLine( hbox, 
		hbox.bg.x - hbox.bg.width*0.5 + 10, 
		btn_close.y - btn_close.height - 15,  
		hbox.bg.x + hbox.bg.width*0.5 - 10, 
		btn_close.y - btn_close.height - 15
	)
	sepr2:setStrokeColor( 0,0,0, 0.1 )
	sepr2.strokeWidth = 1.5  

	return hbox
end

local function createHistoryView( guest_id, data )
	local hview = display.newGroup()
	hview.anchorChildren = true
	local box = {}
	local spinner = {}
		
	local width = _Xa*0.85
	local min_height = 120
	local max_height = _Ya*0.9 - min_height
	local sum_points = 0
	--_print(data)
	local function row( row_data )		
		local row_width = width - 20
		local row_height = 33
	--	_print("############################################")
	--	_printResponse(row_data)
		local row = display.newGroup()
		row.anchorChildren = true

		local bg = display.newRect( row, 0, 0, row_width, row_height )
		bg:setFillColor( 0, 0 )
		local left = -bg.width*0.5
		local right = bg.width*0.5
		local top = -bg.height*0.5
		local bottom = bg.height*0.5

		local rest = display.newText({
			text = row_data.about_name,     
			x = 0,
			y = 0,
			font = "HelveticaNeueCyr-Light",   
			fontSize = 14
		})
		rest:setFillColor( 0 )
		rest.anchorX = 0
		rest.x = left
		rest.anchorY = 0
		rest.y = top
		row:insert( rest )
		_print(row_data.bonus_transaction)
		if row_data.bonus_transaction and row_data.bonus_transaction~="" then
			local date = Date( row_data.bonus_transaction )

			local dm_text = date:fmt( "%d.%m" )
			local dm_label = display.newText({
				text = dm_text,     
				x = 0,
				y = 0,
				font = "HelveticaNeueCyr-Thin",   
				fontSize = 13
			})
			dm_label:setFillColor( 0 )
			dm_label.anchorX = 0
			dm_label.x = left
			dm_label.anchorY = 1
			dm_label.y = bottom
			row:insert( dm_label )

		local year = date:getyear()
		local ylabel = display.newText({
			text = "."..year,     
			x = 0,
			y = 0,
			font = "HelveticaNeueCyr-Thin",   
			fontSize = 8
		})
		ylabel:setFillColor( 0 )
		ylabel.anchorX = 0
		ylabel.x = dm_label.x + dm_label.width - 3
		ylabel.anchorY = 1
		ylabel.y = bottom - 1
		row:insert( ylabel )

		local time = date:fmt( "%H:%M" )
		local tlabel = display.newText({
			text = time,     
			x = 0,
			y = 0,
			font = "HelveticaNeueCyr-Thin",   
			fontSize = 13
		})
		tlabel:setFillColor( 0 )
		tlabel.anchorX = 0
		tlabel.x = ylabel.x + ylabel.width + 5
		tlabel.anchorY = 1
		tlabel.y = bottom
		row:insert( tlabel )
		local txt= translations["bonuses_txt"][_G.Lang]
		local curr_points = row_data.bonus_count_plus
		if not curr_points or row_data.bonus_count_minus > 0 then
			curr_points = "-"..row_data.bonus_count_minus
		elseif  curr_points and curr_points>0 then curr_points = "+"..curr_points
		elseif row_data.bonus_coffee_count ~= nil and row_data.bonus_coffee_count ~= 0 then
			curr_points=" "..row_data.bonus_coffee_count
			txt= translations["cups_txt"][_G.Lang]
		else	
			curr_points=""
		end
		local points = display.newText({
			--text = curr_points.." ["..sum_points.."]",     
			text = curr_points,     
			x = 0,
			y = 0,
			font = "HelveticaNeueCyr-Thin",   
			fontSize = 13
		})
		points:setFillColor( 0 )
		points.anchorX = 0
		points.x = tlabel.x + tlabel.width + 20
		points.anchorY = 1
		points.y = bottom
		row:insert( points )
		local plabel = display.newText({
			text = txt,     
			x = 0,
			y = 0,
			font = "HelveticaNeueCyr-Thin",   
			fontSize = 8
		})
		plabel:setFillColor( 0 )
		plabel.anchorX = 0
		plabel.x = points.x + points.width + 2
		plabel.anchorY = 1
		plabel.y = ylabel.y
		if curr_points=="" then
			plabel.alpha=0
		end
		row:insert( plabel ) 
		end
		local money = display.newText({
			text = row_data.order_sum.._G.Currency,     
			x = 0,
			y = 0,
			font = "HelveticaNeueCyr-Thin",   
			fontSize = 13
		})
		money:setFillColor( 0 )
		money.anchorX = 1
		money.x = right
		money.anchorY = 1
		money.y = bottom
		row:insert( money )          
		
		return row
	end

	local function list()
		local pad = 13
		local ypos = 0
		-- _print("list()")
		-- _printResponse(data)
		for i=1, #data do
			sum_points = sum_points + data[i].bonus_count_plus - data[i].bonus_count_minus
			hview:insert( row( data[i] ) )
			--_printResponse(row( data[i]))
			--_print(data[i] )
			hview[i].anchorY = 0
			hview[i].y = ypos
			ypos = ypos + hview[i].height + pad   
		end

		if hview.height > max_height then
			local content = hview
			hview = widget.newScrollView
			{
				width = width,
				height = max_height,
				bottomPadding = 0,
				horizontalScrollDisabled = true,
				hideBackground = true,
				listener = function( event )
					display.getCurrentStage():setFocus( nil )
				end
			}   
			content.x = hview.width*0.5
			content.anchorY = 0
			content.y = 0
			hview:insert( content )
		end
	end

	local function reload()
		local greload = display.newGroup()
		local mess = display.newText( { 
			width = width - 20,
			parent = greload,
			text = translations.CabinetTxt_History[userProfile.lang][2],
			font = "HelveticaNeueCyr-Light",
			fontSize = 12,
			align = "center"
		} )
		mess:setFillColor( 1,0,0, 0.6 )
		local reloadbutn = widget.newButton
		{
			width = 127*0.5,
			height = 128*0.5,
			defaultFile = "icons/default/reload.png",
			overFile = "icons/over/reload.png",
			onRelease = function( event )
				greload:removeSelf()
				greload = nil
				spinner = Spinner:new()
				box:insert( spinner )
				spinner:start()
				rdb_bonus:getGuestHistory(guest_id,function( event )
					local history = event.data.result
				--	_print("%%%%%")
				--	_printResponse(history)

					if event.result == "completed" then
						scene.history = history
						data = history
						spinner:stop()
						spinner:removeSelf()
						spinner = nil
						box:removeSelf()
						box = nil
					--	_print("############################################1")
						createHistoryView( guest_id, data )
					else 
						spinner:stop()
						spinner:removeSelf()
						spinner = nil
						reload()
					end
				end )
				--rdb:getGuestHistory( guest_id, )
			end
		}
		greload:insert( reloadbutn, true )
		reloadbutn.anchorY = 0
		reloadbutn.y = mess.y + mess.height*0.5 + 15
		hview:insert( greload )
	end

	if data then list()
	else reload()
	end

	box = createBox( translations.CabinetTxt_History[userProfile.lang][1], width, hview.height + 120 )
	box:insert( hview )
	hview.x = box.bg.x
	hview.anchorY = 0
	hview.y = box.bg.y - box.bg.height*0.5 + 45
end

local function createBalanceView( guest_id, data )
	local bview = display.newGroup()
	bview.anchorChildren = true
	local box = {}
	local spinner = {}
		
	local width = _Xa*0.85
	local min_height = 120
	local max_height = _Ya*0.9 - min_height
	local sum_points = 0

	local function row( row_data )
		local row_width = width - 20
		local row_height = 25

		local row = display.newGroup()
		row.anchorChildren = true

		local bg = display.newRect( row, 0, 0, row_width, row_height )
		bg:setFillColor( 0, 0.5 )
		bg.isVisible = false
		local left = -bg.width*0.5
		local right = bg.width*0.5
		local top = -bg.height*0.5
		local bottom = bg.height*0.5
		if row_data.name~=nil then
		local rest = display.newText({
			text = row_data.name,     
			x = 0,
			y = 0,
			font = "HelveticaNeueCyr-Light",   
			fontSize = 14
		})
		rest:setFillColor( 0 )
		rest.anchorX = 0
		rest.x = left
		row:insert( rest )
		local txt= translations["bonuses_txt"][_G.Lang]
		if row_data.bonus_type==1 then
			txt= translations["cups_txt"][_G.Lang]
		end
		local money = display.newText({
			text = row_data.balance.." "..txt,     
			x = 0,
			y = 0,
			font = "HelveticaNeueCyr-Thin",   
			fontSize = 13
		})
		money:setFillColor( 0 )
		money.anchorX = 1
		money.x = right
		row:insert( money )          
		end
		return row
	end

	local function list()
		local row_data = nil
		local pad = 13
		local ypos = 0
		local i = 0
		-- _printResponse(data)
		for com_id, balance in pairs( data ) do
			i = i + 1
			row_data = {}
			for j=1, #RestoransList do
				if RestoransList[j].com_id == com_id then
					row_data.name = RestoransList[j].title
				end
			end
			row_data.balance = balance
			row_data.bonus_type=userProfile.bonus_types[com_id]
			bview:insert( row( row_data ) )
			bview[i].anchorY = 0
			bview[i].y = ypos
			ypos = ypos + bview[i].height + pad   
		end

		if bview.height > max_height then
			local content = bview
			bview = widget.newScrollView
			{
				width = width,
				height = max_height,
				bottomPadding = 0,
				horizontalScrollDisabled = true,
				hideBackground = true,
				listener = function( event )
					display.getCurrentStage():setFocus( nil )
				end
			}   
			content.x = bview.width*0.5
			content.anchorY = 0
			content.y = 0
			bview:insert( content )
		end
	end

	local function reload()
		local greload = display.newGroup()
		local mess = display.newText( { 
			width = width - 20,
			parent = greload,
			text = translations.CabinetTxt_Balance[userProfile.lang][2],
			font = "HelveticaNeueCyr-Light",
			fontSize = 12,
			align = "center"
		} )
		mess:setFillColor( 1,0,0, 0.6 )
		local reloadbutn = widget.newButton
		{
			width = 127*0.5,
			height = 128*0.5,
			defaultFile = "icons/default/reload.png",
			overFile = "icons/over/reload.png",
			onRelease = function( event )
				greload:removeSelf()
				greload = nil
				spinner = Spinner:new()
				box:insert( spinner )
				spinner:start()
				rdb:getGuestBalance( guest_id, function( state, balance,bonus_types )
					_print("rdb:getGuestBalance - done")
					if state == "success" then
						-- _printResponse(balance)
						userProfile.balance = balance
						userProfile.bonus_types=bonus_types
						spinner:stop()
						spinner:removeSelf()
						spinner = nil
						box:removeSelf()
						box = nil
						createBalanceView( guest_id, userProfile.balance )
					else 
						spinner:stop()
						spinner:removeSelf()
						spinner = nil
						reload()
					end
				end )
			end
		}
		greload:insert( reloadbutn, true )
		reloadbutn.anchorY = 0
		reloadbutn.y = mess.y + mess.height*0.5 + 15
		bview:insert( greload )
	end

	if data and next( data ) then list()
	else reload()
	end

	box = createBox( translations.CabinetTxt_Balance[userProfile.lang][1], width, bview.height + 120 )
	box:insert( bview )
	bview.x = box.bg.x
	bview.anchorY = 0
	bview.y = box.bg.y - box.bg.height*0.5 + 45
end

function scene:keyback( event )
	local prevscene = composer.getSceneName( "previous" )
	local options = {
		effect = "fade",
		time = 200,
		params = { }
	}
	if not self.bedit.isVisible then
		if self.decbuttons then
			self.decbuttons:removeSelf()
			self.decbuttons = nil
		end
		self.avatar:restore()
		self.avatar:setState( "locked" )
		self.fields:restore()
		self.fields:setEditable( false )
		self.fnlabel.isVisible = true
		self.ballabel.isVisible = true
		self.bedit.isVisible = true
		self.bbalance.isVisible = true
		self.bhistory.isVisible = true
		self.blogout.isVisible = true
		Keyback:setFocus( nil )
		Keyback:setFocus( self )
	else
		--composer.gotoScene( prevscene, options )
		--CreateSideBar()
		mySidebar:press( "list_main" )
		Keyback:setFocus( nil )
	end
end

function scene:create( event )
	local sceneGroup = self.view
	createHeader()
	Keyback:setFocus( self )

	local _bg = display.newRect( sceneGroup, 0, 0, _Xa, _Ya + display.topStatusBarContentHeight )
	_bg:setFillColor( 228/255, 228/255, 228/255 )
	_bg.x = _mX
	_bg.y = _mY

	scrollview = widget.newScrollView({
		width = _Xa,
		height = _Ya*2,
		verticalScrollDisabled = true,
		horizontalScrollDisabled = true,
		backgroundColor = { 0, 0.5 },
		hideBackground = true,
		hideScrollBar = true,
		listener = function( event )
			if event.phase == "began" then
			end
		end
	})
	scrollview.anchorY = 0
	scrollview.y = _G.general_top
	sceneGroup:insert( scrollview )

	local bg = display.newRect( sceneGroup, 0, 0, _Xa, _Ya + display.topStatusBarContentHeight )
	bg:setFillColor( 228/255, 228/255, 228/255 )
	bg.x = _mX
	scrollview:insert( bg )

	function scrollview:activate( y )
		if self.timerid then timer.cancel( self.timerid ); self.timerid = nil end
		local content = scrollview:getView() 
		if content.y ~= 120 then content.y = -120 end
	end
	
	function scrollview:deactivate()
		self.timerid = timer.performWithDelay( 100, function( event )
			local content = scrollview:getView() 
			content.y = 0
			display.getCurrentStage():setFocus( nil ) 
			self.timerid = nil  
		end )
	end

	local entered = EnterModule.checkEntered()
	
	if entered then
-- 		rdb:getGuestHistory( entered.guest_id, function( state, data )
-- --			_print("$$$$1")
-- 			--_printResponse(data)
-- 			self.bhistory:setEnabled( true )
-- 			if state == "success" then
-- 				self.history = data
-- 			end
-- 		end )
	end

	self.avatar = createUserAvatar()
	self.avatar.x = _mX
	self.avatar.anchorY = 0
	self.avatar.y = 5
	scrollview:insert( self.avatar )

	self.fnlabel = createFullNameLabel( entered )
	self.fnlabel.x = _mX
	self.fnlabel.anchorY = 0
	self.fnlabel.y = self.avatar.y + self.avatar.height + 17
	scrollview:insert( self.fnlabel )

	self.ballabel = createBalanceLabel( entered.guest_id )
	self.ballabel.x = _mX
	self.ballabel.anchorY = 0
	self.ballabel.y = self.fnlabel.y + self.fnlabel.height + 18
	scrollview:insert( self.ballabel )

	self.blogout = createLogoutButton()
	self.blogout.x = _mX
	self.blogout.anchorY = 1
	self.blogout.y = _BOTTOM - 10
	sceneGroup:insert( self.blogout )

	local mhf = -10 + (self.blogout.y - self.blogout.height) - (self.avatar.y + self.avatar.height) 
	self.fields = createFields({parentGr = sceneGroup,max_height = mhf, data = entered } )
	self.fields.x = _mX
	self.fields.anchorY = 0
	self.fields.y = self.fnlabel.y - 47
	self.fields.defy = self.avatar.y + self.avatar.height + 17
	scrollview:insert( self.fields )
	self.fields:toBack()
	bg:toBack()

	self.bbalance = createBalanceButton( function( event )
		createBalanceView( entered.guest_id, userProfile.balance )
	end)
	self.bbalance.x = _mX
	self.bbalance.anchorY = 1
	self.bbalance.y = self.blogout.y - self.blogout.height - 20+10
	sceneGroup:insert( self.bbalance )

	self.bedit = createEditButton( function( event )
		event.target.isVisible = false
		self.bbalance.isVisible = false
		self.blogout.isVisible = false
		self.bhistory.isVisible = false
		self.avatar:setState( "unlocked" )
		self.fnlabel.isVisible = false
		self.ballabel.isVisible = false
		self.fields:setEditable( true )
		
		local decbuttons = createDecisionButtons(
			function( event )
				if event.name == "confirm" then
					self.avatar:save()
					self.fields:sendData( function( state, data )
						if state == "success" then
							event.group:removeSelf()
							event.group = nil
							self.avatar:setState( "locked" )
							self.fields:setEditable( false )
							self.fnlabel:update( data )
							self.fnlabel.isVisible = true
							self.ballabel.isVisible = true
							self.bedit.isVisible = true
							self.bbalance.isVisible = true
							self.bhistory.isVisible = true
							self.blogout.isVisible = true
						else --
							event.group:removeSelf()
							event.group = nil
							self.avatar:setState( "locked" )
							self.fields:setEditable( false )
							self.fnlabel:update( data )
							self.fnlabel.isVisible = true
							self.ballabel.isVisible = true
							self.bedit.isVisible = true
							self.bbalance.isVisible = true
							self.bhistory.isVisible = true
							self.blogout.isVisible = true
							native.showAlert( _G.appTitle, "Error while saving changes", { "OK" } )
						end
					end)
				elseif event.name == "cancel" then
					event.group:removeSelf()
					event.group = nil
					self.avatar:restore()
					self.avatar:setState( "locked" )
					self.fields:restore()
					self.fields:setEditable( false )
					self.fnlabel.isVisible = true
					self.ballabel.isVisible = true
					self.bedit.isVisible = true
					self.bbalance.isVisible = true
					self.bhistory.isVisible = true
					self.blogout.isVisible = true
					return true 
				end
			end
		)
		sceneGroup:insert( decbuttons )
		decbuttons.x = _mX
		decbuttons.anchorY = 1
		decbuttons.y = self.blogout.y
		self.decbuttons = decbuttons
		if sceneGroup.cntrbox then
			sceneGroup.cntrbox:toFront()
		end
	end )
	self.bedit.anchorX = 1
	self.bedit.x = self.bbalance.x - self.bbalance.width*0.5 - 20
	self.bedit.anchorY = 1
	self.bedit.y = self.bbalance.y
	sceneGroup:insert( self.bedit )

	self.bhistory = createHistoryButton( function( event )
		Spinner:start()
		-- rdb:getGuestHistory( entered.guest_id, function( state, data )
		-- 	if state == "success" then
		-- 		self.history = data
		-- 	end
		-- 	--_print("############################################2")
		-- --_printResponse(self.history)
		-- 	Spinner:stop()
		-- 	createHistoryView( entered.guest_id, self.history )

		-- end )	
		rdb_bonus:getGuestHistory( entered.guest_id, function( event )
				--	_print("%%%%%")
					--_printResponse(event)

			if event.result == "completed" then
				self.history = event.data.result
			end
			--_print("############################################2")
		--_printResponse(self.history)
			Spinner:stop()
			createHistoryView( entered.guest_id, self.history )

		end )				
	end)
	self.bhistory.anchorX = 0
	self.bhistory.x = self.bbalance.x + self.bbalance.width*0.5 + 20
	self.bhistory.anchorY = 1
	self.bhistory.y = self.bbalance.y
	sceneGroup:insert( self.bhistory )

	if sceneGroup.cntrbox then
		sceneGroup.cntrbox:toFront()
	end

end

function scene:destroy( event )
	local sceneGroup = self.view
end

scene:addEventListener( "create", scene )
scene:addEventListener( "destroy", scene )

return scene