local SlideView = require "ui.SlideView"
local connection = require "lib.connection"
local widget = require "widget"
local ovlib = require "ui.OrderView"
-- 
local rdb = require "lib.db_remote"
local common_lib = require "lib.common_lib"
local EnterModule = require "ui.EnterModule"
local rdb_bonus = require( "lib.db_remote_bonuses" )
local scene = composer.newScene()

local function goBack()
            local prevscene = composer.getSceneName( "previous" )
            local cases = {
                ["actions.main_scene"] = "main",
                ["actions.rests_list_scene"] = "list_main",
                ["actions.my_cabinet_scene"] = "cabinet",
                ["actions.rest_menu_scene"] = "menu",
                ["actions.my_orders_scene"] = "orders",
                ["actions.actions_scene"] = "actions",
                ["actions.rest_about_scene"] = "about",
            }
            mySidebar:press( cases[prevscene] )
end

local function createHeader()
    local headerItems = {}

    if userProfile.SelectedRestaurantData and userProfile.activeOrderID == 0 then
        local id = ovlib:findLocalActiveOrder( 
            userProfile.SelectedRestaurantData.com_id,
            userProfile.SelectedRestaurantData.pos_id
        )
        userProfile.activeOrderID = id
    end    

    headerItems[1] = {              
        id = "rest_title",
        label = userProfile.SelectedRestaurantData.title,
        labelColor = { 1,1,1 },
        fontSize = 15,
        icon = "back",
        iconWidth = 7,
        iconHeight = 13,
	  iconY = 1,
        labelAnchor = { pos = "right", pad = 3 },
        center = -5,        
        params = {},
        switch = true,
        turnOn = function( event ) 
            goBack()
        end,
    }
    local map_right=20
    if userProfile.activeOrderID ~= 0 then
        headerItems[2] = {              
           id = "ord_sum",
           label = ldb:getOrderSum( userProfile.activeOrderID ).._G.Currency,
           labelColor = { 1,1,1 },
           fontSize = 18,
           icon = "cart",
           iconWidth = 17,
           iconHeight = 15,
           iconY = 1.8,
           labelAnchor = { pos = "left", pad = 3 },
           params = {},
           switch = true,
           right = 20,
           yShift = -1,
           turnOn = function( event ) mySidebar:press("orders") end, 
        }
        map_right=60
    end

    -- headerItems[2] = {
    --     id = "filter",
    --     label = "",
    --     icon = "filter",
    --     iconWidth = 22,
    --     iconHeight = 13,
    --     switch = true,
    --     right = 20,
    --     turnOn = function( event ) mySidebar:open() end,
    --     turnOff = function( event ) mySidebar:close() end
    -- }
    -- headerItems[3] = {
    --     id = "map",
    --     label = "",
    --     icon = "map",
    --     iconWidth = 12,
    --     iconHeight = 15,
    --     switch = true,
    --     right = 60,
    --     turnOn = function( event ) mySidebar:open() end,
    --     turnOff = function( event ) mySidebar:close() end
    -- }
    if userProfile.activeOrderID == 0 
    and userProfile.SelectedRestaurantData.geo_lat~=nil 
    and userProfile.SelectedRestaurantData.geo_long~=nil then
        headerItems[#headerItems+1] = {
            id = "map",
            label = "",
            icon = "map",
            iconWidth = 12,
            iconHeight = 15,
            switch = true,
            right = map_right,
            turnOn = function( event )
                local options = {
                    isModal = true,
                    effect = "fade",
                    time = 400,
                    params = {
                        current_rest_only=true
                    }
                }
                composer.showOverlay( "actions.map_overlay", options )
            end,
            turnOff = function( event ) composer.hideOverlay() end
        }
    end
    CreateHeader( headerItems, {} ) 
end

local function createRestBoard( rest_data )
    local height = 200

    local board = display.newGroup()
    board.anchorChildren = true
    board.x = _mX
    board.anchorY = 0
    board.y = display.screenOriginY

    local container = display.newContainer( board, _Xa, height )

    local pheight = height
    local pwidth = pheight*rest_data.main_photo.aspect
    local scale_factor = _Xa / pwidth
    if scale_factor > 1 then
        pwidth = pwidth * scale_factor
        pheight = pheight * scale_factor
    end
    local photo_file = rest_data.main_photo.name
    local base_dir = rest_data.main_photo.basedir
    local photo = display.newImageRect( container,
        photo_file, base_dir,
        pwidth, pheight
    )
    photo.anchorY = 0
    photo.y = -container.height*0.5

    local cover = display.newRect( board, 0, 0, _Xa, height )
    cover:setFillColor( 0, 0.2 )

    local ref_bottom = container.y + container.height*0.5

    local visor = display.newImageRect( board,
        "assets/visor.png",
        320, 25
    )
    visor.anchorY = 0
    visor.y = ref_bottom    

    local logo_bg = display.newImageRect( board,
        "assets/logoBackground.png",
        110, 110
    )
    logo_bg.y = ref_bottom
    board.logoradius = logo_bg.width*0.5

    local a = rest_data.logo_width
    local b = rest_data.logo_height
    local radius = math.sqrt( math.pow(a,2) + math.pow(b,2) ) / 2
    local scale_factor = (logo_bg.width*0.5-3) / radius
    local logo = display.newImageRect( board,
        rest_data.logo_file, rest_data.logo_dir,
        rest_data.logo_width, rest_data.logo_height
    )
    logo:scale( scale_factor, scale_factor )
    logo.y = logo_bg.y

    if rest_data.com_loyalty_type == 2 or rest_data.com_loyalty_type == 1 then
        local bonus = display.newImageRect( board,
            "assets/bonusIconOpaque.png", 37, 37
        )
        bonus.x = logo_bg.x + logo_bg.width*0.5 - 14
        bonus.y = logo_bg.y - logo_bg.height*0.5 + 6
    end    

    return board
end

local function createTitle( text )
    local pad = 15
    local text_pad = 5
    local height = 20
    local line_height = 0.7
    local line_color = 0.8
    local font = "HelveticaNeueCyr-Light"
    local font_size = 13

    local title = display.newGroup()
    title.anchorChildren = true

    local bg = display.newRect( title, 0, 0, _Xa, height )
    bg:setFillColor( 0.5, 0 )

    local ref_left = -bg.width*0.5 + pad

    local left_line = display.newRect( title, ref_left, 0, 30, line_height )
    left_line:setFillColor( line_color )
    left_line.anchorX = 0

    local title_text = display.newText({
        parent = title,
        text = text,
        font = font,
        fontSize = font_size,
    })
    title_text:setFillColor( 0 )
    title_text.anchorX = 0
    title_text.x = left_line.x + left_line.width + text_pad

    local rlwidth = _Xa - ( left_line.width + title_text.width + pad*2 + text_pad*2 )
    local right_line = display.newRect( title, 0, 0, rlwidth, line_height )
    right_line:setFillColor( line_color )
    right_line.anchorX = 0
    right_line.x = title_text.x + title_text.width + text_pad

    return title

end

local function createPictureViewer( pics_data, init_pic )
    local group = display.newGroup()
    group.anchorChildren = true
    group.x = _mX
    group.y = _mY 

    local bg = display.newRect( group, 0, 0, display.actualContentWidth, display.actualContentHeight )
    bg:setFillColor( 0, 0.8 )
    bg:addEventListener( "touch", function( event ) return true end )
    bg:addEventListener( "tap", function( event ) return true end )

    local w, h = display.actualContentWidth, display.actualContentHeight
    local function loadSlide( slide, slideview )
        local num = slide.index_number
        slide.params = {
            autoscale = false,
            hasBg = false,
        }
        
        local pwidth = 100
        local pheight = 0
        local pname = pics_data[num].name
        local paspect = pics_data[num].aspect
        pheight = pwidth / paspect
        local w_aspect = w / pwidth
        local h_aspect = h / pheight
        paspect = w_aspect
        if w_aspect > h_aspect then paspect = h_aspect end
        display.newImageRect( slide,
            pname, 
            pics_data[num].basedir, 
            pwidth*paspect, pheight*paspect 
        )
    end

    local slideView = widget.newSlideView({ 
        x = 0,
        y = 0,
        height = display.actualContentHeight,
        numSlides = #pics_data,
        initSlide = init_pic,
        leftPadding = 30,
        rightPadding = 30,
        timeMove = 400,

        loadSlide = loadSlide,
        unloadSlide = function() end,
        showSlide = function() end,
        hideSlide = function() end,
        reachedFirst = function() end,
        reachedLast = function() end
    })
    group:insert( slideView )

    local closebut = display.newImageRect( group,
        "icons/closeButton.png", 
        20, 20 
    )
    closebut.anchorX = 1
    closebut.x = bg.width*0.5 - 10
    closebut.anchorY = 0
    closebut.y = -bg.height*0.5 + 30

    local function onOrientationChange( event )
        local currentOrientation = event.type
        print( "Current orientation: " .. currentOrientation, _Xa )
        
        local reversTouch = "right"
        if currentOrientation == "landscapeLeft" then
            reversTouch = "down"
        elseif currentOrientation == "landscapeRight" then
            reversTouch = "up"
        end

        w, h = display.actualContentWidth, display.actualContentHeight
        if system.getInfo( "platformName" ) == "Android" then
            if currentOrientation == "landscapeLeft" or currentOrientation == "landscapeRight" then
                w = display.actualContentHeight 
                h = display.actualContentWidth
            end
        end

        local last_slide = slideView.currindex
        slideView:removeSelf()
        slideView = widget.newSlideView({ 
            x = 0,
            y = 0,
            width = w,
            height = h,
            numSlides = #pics_data,
            initSlide = last_slide,
            leftPadding = 30,
            rightPadding = 30,
            timeMove = 400,
            reversTouch = reversTouch,

            loadSlide = loadSlide,
            unloadSlide = function() end,
            showSlide = function() end,
            hideSlide = function() end,
            reachedFirst = function() end,
            reachedLast = function() end
        })
        group:insert( slideView )

        if currentOrientation == "landscapeLeft" then
            slideView.rotation = -90
            closebut.x = -bg.width*0.5 + 30
            closebut.y = -bg.height*0.5 + 30
        elseif currentOrientation == "landscapeRight" then
            slideView.rotation = 90
            closebut.x = bg.width*0.5 - 10
            closebut.y = bg.height*0.5 - 30           
        else closebut.x = bg.width*0.5 - 10
            closebut.y = -bg.height*0.5 + 30
        end
        closebut:toFront()

    end

    closebut:addEventListener( "touch", function( event ) 
        if event.phase == "began" then
            Keyback:setFocus( group.prev_keyback_focus )            
            Runtime:removeEventListener( "orientation", onOrientationChange )
            group:removeSelf()
            group = nil
        end
        return true 
    end )

    function group:keyback( event )
        timer.performWithDelay( 200, function()
            Keyback:setFocus( group.prev_keyback_focus )
        end )
        Runtime:removeEventListener( "orientation", onOrientationChange )
        self:removeSelf()
        self = nil
    end
    group.prev_keyback_focus = Keyback.focuson
    Keyback:setFocus( group )

    Runtime:addEventListener( "orientation", onOrientationChange )
end

local function createRestPhotoGallery( rest_data, listener )
    local idcom = rest_data.com_id
    local idpos = rest_data.pos_id
    local gallery = display.newGroup()
    gallery.anchorChildren = true
    local photos = {}
    local size = display.pixelWidth.."x"..display.pixelHeight 
    
    local function initPhotoGallery()
	  gallery.photos_data = {}
	  if rest_data.main_photo.name ~= "assets/noPhoto.jpg" then
		  gallery.photos_data[1] = rest_data.main_photo
	end
        local othes_photo = rest_data.othes_photo or {}
        for i=1, #othes_photo do
            gallery.photos_data[#gallery.photos_data+1] = othes_photo[i]
        end                

        if #gallery.photos_data < 2 then
            local mess = display.newText( { 
                width = 200,
                parent = gallery,
                text = translations["no_photo_mes"][_G.Lang],
                font = "HelveticaNeueCyr-Light",
                fontSize = 12,
                align = "center"
            } )
            mess:setFillColor( 1,0,0, 0.6 )
            local reloadbutn = widget.newButton
            {
                width = 127*0.5,
                height = 128*0.5,
                defaultFile = "icons/default/reload.png",
                overFile = "icons/over/reload.png",
                onRelease = function( event )
                    gallery:removeSelf()
                    gallery = nil
                    scene.spinner_gallery:start()
                    createRestPhotoGallery( rest_data, listener )
                end
            }
            gallery:insert( reloadbutn, true )
            reloadbutn.y = 60
            if listener then listener( gallery ) end
            return
        end

        local pwidth = 100
        local pheight = 0
	  for i=1, #gallery.photos_data do
            photos[i] = display.newGroup()
            photos[i].id = i
            pheight = pwidth / gallery.photos_data[i].aspect
            display.newImageRect( photos[i],
                gallery.photos_data[i].name, 
                gallery.photos_data[i].basedir, 
                pwidth, pheight 
            )
            display.newRect( photos[i], 0, 0, pwidth, pheight ):setFillColor( 0, 0.35 )

            if photos[i] then
                photos[i]:addEventListener( "touch", function( event )
                    if event.phase == "began" then
                        event.target.alpha = 0.5
                        event.target.isFocus = true
                    elseif event.phase == "moved" then
                        if math.abs( event.yStart - event.y ) >= 7 or
                        math.abs( event.xStart - event.x ) >= 7 then
                            event.target.isFocus = false
                            event.target.alpha = 1
                        end
                    elseif event.phase == "ended" or event.phase == "cancelled" then    
                        if event.target.isFocus then
                            createPictureViewer( gallery.photos_data, event.target.id )
                            event.target.alpha = 1
                            event.target.isFocus = false
                        end
                    end
                end )
            end
        end

        local pad = 3
        local columns = 3
        if #photos <= 2 then columns = #photos end
        local twidth = (_Xa - pad*(columns-1)) / columns
        local theight = 100
        local gallery_tiles = widget.newTilesGroup({
            tiles = photos,
            --width = _Xa,
            --height = height,
            tileWidth = twidth,
            tileHeight = theight,
            columnTilesNumber = columns,
            verticalPad = pad,
            horizontalPad = pad,
            --topPad = pad,
            --bottomPad = pad,
            --leftPad = pad,
            -- rightPad
            fitSize = true,
            horizontalScrollDisabled = true,
            verticalScrollDisabled = true,
            --bgColor = {228/255, 228/255, 228/255}
        })
        
        if listener then listener( gallery_tiles ) end
    end

    local function getRestPhotos()
        local filter = {
            group = "and",
            conditions = {
                { 
                    left = "com_id",
                    oper = "=",
                    right = idcom
                },             
                { 
                    left = "pos_id",
                    oper = "=",
                    right = idpos
                },
                { 
                    left = "abp_is_main",
                    oper = "<>",
                    right = "true"
                }      
            }
        }

        if not rest_data.othes_photo then
            rdb.sign = idcom
            rdb:getRestPhoto( "restPhotos", size, function( state, response )
                if state == "success" then
                    if response[idcom][idpos].othes then
                        rest_data.othes_photo = response[idcom][idpos].othes
                    end
                end
                initPhotoGallery()
            end, filter )            
        else initPhotoGallery()
        end
    end

    getRestPhotos()
end

local function createButtons()
    local pad = 20
    local but_width = (_Xa-pad*3)*0.5
    local but_height = 37    

    local group = display.newGroup()
    group.anchorChildren = true

    local bg = display.newRect( group, 0, 0, _Xa, but_height+30 )
    bg:setFillColor( 0, 0.7 )

    local rightbut = widget.newButton({
        labelColor = { default={1}, over={0} },
        font = "HelveticaNeueCyr-Light",
        fontSize = 18,
        label = translations["new_order"][_G.Lang],
        onRelease = function( event )
            if EnterModule:checkEntered() then
                ldb:createNewOrder()
                userProfile.activeOrderID = 0
                --userProfile.SelectedRestaurantData = nil
                mySidebar:press( "orders" )
            else
                local options = {
                    isModal = true,
                    effect = "fromRight",
                    time = 300,
                    params = { parent = "actions.rest_about_scene" }
                }
                composer.showOverlay( "actions.enter_overlay", options )
                CreateBaseHeader()                  
            end
        end,
        --properties for a rounded rectangle button...
        shape = "roundedRect",
        width = but_width,
        height = but_height,
        cornerRadius = 5,
        fillColor = { default={ 244/255, 64/255, 66/255 }, over={ 211/255,211/255,211/255 } },
        strokeColor = { default={ 244/255, 64/255, 66/255 }, over={ 0,0,0, 0.13 } },
        strokeWidth = 2
    })
    group:insert( rightbut, true )
    rightbut.anchorX = 1
    rightbut.x = bg.width*0.5 - pad

    local leftbut = widget.newButton({
        labelColor = { default={1}, over={0} },
        font = "HelveticaNeueCyr-Light",
        fontSize = 18,
        label = translations["price_list"][_G.Lang],
        onRelease = function( event )
		composer.setVariable( "came_from", "about" )
		mySidebar:press( "menu" )
        end,
        --properties for a rounded rectangle button...
        shape = "roundedRect",
        width = but_width,
        height = but_height,
        cornerRadius = 5,
        fillColor = { default={ 56/255, 214/255, 128/255 }, over={ 211/255,211/255,211/255 } },
        strokeColor = { default={ 56/255, 214/255, 128/255 }, over={ 0,0,0, 0.13 } },
        strokeWidth = 2
    })
    group:insert( leftbut, true )
    leftbut.anchorX = 0
    leftbut.x = -bg.width*0.5 + pad    

    return group
end


-- ------------------------------------------------------------------------------

function scene:keyback( event )
	print( "<<<<<<< ABOUT" )
    goBack()
    Keyback:setFocus( nil )
end

function scene:create( event )
    local sceneGroup = self.view
    local rest_data = userProfile.SelectedRestaurantData
    local font = "HelveticaNeueCyr-Thin"
    local font_size = 11
    local font_color = 0
    local pad = 15
    local space = 5
    createHeader()
        
    local bg = display.newRect( sceneGroup, 0, 0, _Xa, _Ya )
    bg:setFillColor( 228/255, 228/255, 228/255 )
    bg.x = _mX
    bg.y = _mY + display.topStatusBarContentHeight

    local ref_left = -bg.width*0.5 + pad
    local ref_right = bg.width*0.5 - pad

    local board = createRestBoard( rest_data )
    sceneGroup:insert( board )    

    local content = display.newGroup()
    --content.anchorChildren = true
    --content.anchorY = 0
    content.y = 20
    content.x = _mX - display.screenOriginX
    sceneGroup:insert( content )

    local phone = display.newGroup( )
    local next_pos = 0
    local phspace = 1
    local counter = 0
        local phone_txt = display.newText({
             parent = phone,
             text = rest_data.phone,
             font = "HelveticaNeueCyr-Light",
             fontSize = 12,
         })
         phone_txt:setFillColor( 1,0,0, 0.8 )
         phone_txt.anchorX = 1
         phone_txt.anchorY = 0
         phone_txt.y = next_pos
     content:insert( phone )
    phone.x = ref_right

    local glyph_y=90
    if userProfile.balance[rest_data.com_id]~=nil and userProfile.bonus_types and userProfile.bonus_types[rest_data.com_id]==0 then
        local bonuses_label = display.newText({
            parent = board,
            text = "+ "..userProfile.balance[rest_data.com_id],
            font = "HelveticaNeueCyr-Medium",
            fontSize = 15,
            width = 60,
            height = 15,
            align = "right"
        })
        bonuses_label:setFillColor( 244/255,207/255,1/255  )
        bonuses_label.anchorX = 0
        bonuses_label.x = ref_left-15
        bonuses_label.anchorY = 0.5
        bonuses_label.y = glyph_y-2
    end

    local x_add=10
    local flDelivery=false
    local flOrder=false
    local flPreorder=false
    if rest_data.menu_type==0 then
        flOrder=true
        flPreorder=true
    elseif rest_data.menu_type==1 then
        flOrder=true
        flPreorder=true
        flDelivery=true
    elseif rest_data.menu_type==2 then
        flOrder=true
    elseif rest_data.menu_type==3 then
        flDelivery=true
    elseif rest_data.menu_type==4 then
        flOrder=true
        flDelivery=true
    elseif rest_data.menu_type==6 then
        flPreorder=true
    end     

    if flOrder then
        local fork = display.newImageRect( board,
                "assets/fork.png", 16, 16
            )
        fork.x = ref_right-7
        fork.y = glyph_y
                 
    end
    if flPreorder then
        x_add=x_add+10 
        local clock = display.newImageRect( board,
                "assets/clock.png", 16, 16
            )
        clock.x = ref_right-x_add-7
        clock.y = glyph_y  
          
    end
    if flDelivery then 
        x_add=x_add+18 
        local delivery = display.newImageRect( board,
                "assets/delivery.png", 16, 16
            )
        delivery.x = ref_right-x_add-7
        delivery.y = glyph_y
        
    end
    if rest_data.pos_bonusme_merchant_id~=nil and rest_data.pos_bonusme_merchant_id~="" then
        x_add=x_add+20
        local card = display.newImageRect( board,
                "assets/card.png", 16, 16
            )
        card.x = ref_right-x_add-7
        card.y = glyph_y
    end

    -- for k, v in string.gmatch( rest_data.phone, "(%d+) (%d+)" ) do
    --     counter = counter + 1
    --     display.newText({
    --         parent = phone,
    --         text = "("..k..") "..v,
    --         font = "HelveticaNeueCyr-Light",
    --         fontSize = 12,
    --     })
    --     phone[counter]:setFillColor( 1,0,0, 0.8 )
    --     phone[counter].anchorX = 1
    --     phone[counter].anchorY = 0
    --     phone[counter].y = next_pos
    --     next_pos = next_pos + phone[counter].height + phspace
    -- end


    local rating = display.newGroup( )
    local t=0--translations["no_rating"][_G.Lang]
    if rest_data.rating>0 then
        t=tonumber(rest_data.rating)
    end
    local type_txt = display.newText({
        parent = rating,
        text = rest_data.post_name,
        font = "HelveticaNeueCyr-Light",
        fontSize = 13,
    })
    type_txt:setFillColor( 0,0,0, 1 )
    type_txt.anchorX = 0
    rating.anchorY = 0
    type_txt.y = phone.y+5
    local ratingGroup = common_lib.createRating(t)
    ratingGroup.x=type_txt.x+5
    ratingGroup.anchorX = 0
    ratingGroup.y=type_txt.y+type_txt.height+2
    rating:insert( ratingGroup )
    content:insert( rating )
    rating.x = ref_left    
    -- local rat_txt = display.newText({
    --     parent = rating,
    --     text = translations["rating"][_G.Lang]..": "..t,
    --     font = "HelveticaNeueCyr-Light",
    --     fontSize = 10,
    -- })
    -- rat_txt:setFillColor( 0,0,0, 1 )
    -- rat_txt.anchorX = 0
    -- rat_txt.y = type_txt.y+type_txt.height+2


    -- content:insert( rating )
    -- rating.x = ref_left

    local addr_title = createTitle( translations["address_open"][_G.Lang] )
    content:insert( addr_title )
    addr_title.anchorY = 0
    addr_title.y = board.logoradius - 10

    local addr = display.newText({
        parent = content,
        text = rest_data.addr,
        font = font,
        width = 160,
        height = 40,
        fontSize = font_size,
    })
    addr:setFillColor( font_color )
    addr.anchorX = 0
    addr.x = ref_left
    addr.anchorY = 0
    addr.y = addr_title.y + addr_title.height + space

    local worktime = display.newText({
        parent = content,
        text = rest_data.work_time,
        font = font,
        fontSize = font_size,
    })
    worktime:setFillColor( font_color )
    worktime.anchorX = 1
    worktime.x = ref_right
    worktime.anchorY = 0
    worktime.y = addr_title.y + addr_title.height + space

    local descr_title = createTitle( translations["description"][_G.Lang])
    content:insert( descr_title )
    descr_title.anchorY = 0
    descr_title.y = addr.y + addr.height + space
    local dsc_txt=""
    if rest_data.com_loyalty_type == 2 then
    	dsc_txt="\r\n".."\r\n"..translations["cumulat_bonus"][_G.Lang]
    elseif rest_data.com_loyalty_type == 1 then
    	dsc_txt="\r\n".."\r\n"..translations["cumulat_discount"][_G.Lang]
    end
    local descr = display.newText({
        parent = content,
        text = connection.replace_html_tags( rest_data.descr )..dsc_txt,
        font = font,
        fontSize = font_size,
        width = _Xa - pad*2
    })
    descr:setFillColor( font_color )
    descr.anchorX = 0
    descr.x = ref_left
    descr.anchorY = 0
    descr.y = descr_title.y + descr_title.height + space


    local reviews = createTitle( translations["reviews"][_G.Lang] )
    content:insert( reviews )
    reviews.anchorY = 0
    reviews.y = descr.y + descr.height + space
    local reviews_group = display.newGroup( )
    reviews_group.y = reviews.y + reviews.height + space

        local free_space = display.actualContentHeight - board.height
        local scrollView = widget.newScrollView
        {
            --top = 0,
            --backgroundColor = { 0.3, 0.8, 0.8 },
            --left = 0,
            width = _Xa,
            height = free_space + board.logoradius,
            bottomPadding = 0,
            horizontalScrollDisabled = true,
            hideBackground = true,
            listener = function( event )
                display.getCurrentStage():setFocus( nil )
            end
        }   
        sceneGroup:insert( scrollView, true )
        scrollView.x = _mX
        scrollView.anchorY = 0
        scrollView.y = board.y + board.height - board.logoradius
        scrollView:insert( content )
        board:toFront()


    local function on_pos_reviews(event)
    --print("save_menu_to_localdb")
        local data = event.data
        if event.result ~= "completed" then
            print("ERROR LOADING reviews!!!")
            data=nil
         --   return
        end
        if data==nil or #data.rows==0 then
            local no_reviews = display.newText({
                    parent = reviews_group,
                    text = translations["no_reviews"][_G.Lang],
                    font = "HelveticaNeueCyr-Light",
                    fontSize = 10,
                })
                no_reviews:setFillColor( 0,0,0, 0.6 )
                no_reviews.x=0
                no_reviews.y =  5   
                reviews_group:insert( no_reviews )  
        else
            local r_top=5
            for i,item in pairs(data.rows) do

                if item.ordrat_text and item.ordrat_text~="" then
                    if r_top>5 then
                            local left_line = display.newRect(0, r_top, _mX, 0.7 )
                            left_line:setFillColor( 0.8 )
                           -- left_line.y = r_top
                            reviews_group:insert( left_line )
                            r_top=r_top+15
                    end
                    local review_guest_name = display.newText({
                        parent = reviews_group,
                        text = item.guest_fullname,
                        font = "HelveticaNeueCyr-Light",
                        fontSize = 12,
                        width = _mX
                    })
                    review_guest_name:setFillColor( 0,0,0, 1 )
                    review_guest_name.anchorX=0
                    review_guest_name.x=ref_left
                    review_guest_name.y =  r_top
                    reviews_group:insert( review_guest_name )  

                    local rating1 = common_lib.createRating((item.ordrat_index1+item.ordrat_index2+item.ordrat_index3)*10/3)
                    rating1.x=ref_right-55
                    rating1.anchorX = 1
                    rating1.y=r_top
                    reviews_group:insert( rating1 )                     
                    
                    local review_txt = display.newText({
                        parent = reviews_group,
                        text = item.ordrat_text,
                        font = "HelveticaNeueCyr-Light",
                        fontSize = 10,
                        width = _Xa-80
                    })
                    review_txt:setFillColor( 0,0,0, 0.8 )
                    review_txt.anchorX=0
                    review_txt.x=ref_left+15
                    review_txt.y =  rating1.y+25

                    local review_datetime = display.newText({
                        parent = reviews_group,
                        text = Date( item.ordrat_datetime ):fmt("%d-%m-%Y"),
                        font = "HelveticaNeueCyr-Light",
                        fontSize = 10,
                        width = _mX
                    })
                    review_datetime:setFillColor( 0,0,0, 1 )
                    review_datetime.anchorX=0
                    review_datetime.x=ref_right-60
                    review_datetime.y =  review_txt.y+review_txt.height
                    reviews_group:insert( review_datetime )  

                    r_top  = review_datetime.y+review_datetime.height


                    reviews_group:insert( review_txt )             
                end
            end            
        end
 --       printResponse(data)

        ---rest of section
        local photo_title = createTitle( translations["photo"][_G.Lang] )
        content:insert( photo_title )
        photo_title.anchorY = 0
        photo_title.y = reviews_group.y + reviews_group.height + space+5

        
        scene.spinner_gallery = Spinner:new()
        content:insert( scene.spinner_gallery, true )
        scene.spinner_gallery.y = photo_title.y + photo_title.height + space + 50
        scene.spinner_gallery:start()    


        local buttons = createButtons()
        buttons.x = _mX
        buttons.anchorY = 1
        buttons.y =  display.screenOriginY + display.actualContentHeight
        sceneGroup:insert( buttons )

        local function onGalleryCreated( gallery )
            if not sceneGroup then 
                gallery:removeSelf()
                gallery = nil
                return
            end
            pcall( function( gallery )
                content:insert( gallery )
                gallery.anchorY = 0
                gallery.y = photo_title.y + photo_title.height + space
                scrollView:setScrollHeight( content.height + buttons.height + 15 ) 
                scene.spinner_gallery:stop() 
            end, gallery )
        end 
        local photo_gallery = createRestPhotoGallery( rest_data, onGalleryCreated )        
        ---
    end    
    content:insert( reviews_group )
    rdb_bonus:getPosReviews(rest_data.pos_id,5,on_pos_reviews)




end


-- "scene:show()"
function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase
    Keyback:setFocus( self )

    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).
    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.
    end
end


-- "scene:hide()"
function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
    end
	print("scene:hide")
end


-- "scene:destroy()"
function scene:destroy( event )
    self.view = nil
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene