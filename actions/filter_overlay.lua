local composer = require( "composer" )
-- 
local clib = require "lib.common_lib"
local uilib = require "ui.uilib"
local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------

local function createConsolidatedTable( data, fields )
	local contable = {}

	local function getFieldValues( idata, field )
		local values = {}
		if idata[field] then
			if type( idata[field] ) == "table" then
				for i=1, #idata[field] do 
					if idata[field][i] then values[#values+1] = idata[field][i] end
				end
			else values[#values+1] = idata[field]      
			end
		elseif field == "features" then
			values[#values+1] = false
		else 
			values[#values+1] = "Unknown"
		end
		return values
	end

	local function getNumFieldValues( idata, field )
		local values = getFieldValues( idata, field )
		return #values
	end
	
	local function getNumRows( idata )
		local counter = 1
		local var = 0
		for i=1, #fields do
			var = getNumFieldValues( idata, fields[i] )
			if var > 0 then counter = counter * var end
		end
		return counter
	end

	local function getMaxValuesField( idata )
		local max_field = ""
		local max = 0
		local values = {}
		for i=1, #fields do
			values = getFieldValues( idata, fields[i] )
			if max < #values then max = #values; max_field = fields[i] end
		end
		return max_field
	end

	local function createTableForOneKey( key )
		local onekey_table = {}
		local idata = data[key]

		local nrows = getNumRows( idata )
		local maxfield = getMaxValuesField( idata )
		local maxfield_vals = getFieldValues( idata, maxfield )
		local rowsnonchange = nrows / getNumFieldValues( idata, maxfield )
		local index, j = 1, 0
		local vals = {}
		for i=1, nrows do
			onekey_table[i] = {}
			if i > 1 and (i-1) % rowsnonchange == 0 then
				index = index + 1
				j = 0
			end
			j = j + 1
			nonchangeval = maxfield_vals[index]
			for g=1, #fields do
				if fields[g] == maxfield then
					onekey_table[i][fields[g]] = nonchangeval
				else vals = getFieldValues( idata, fields[g] )
					onekey_table[i][fields[g]] = vals[j] or vals[1]
				end
			end
			onekey_table[i].key = key
		end

		return onekey_table
	end
	
	local oktable = {}
	for i=1, #data do
		oktable = createTableForOneKey( i )
		contable = table._copy( contable, oktable )
	end

	-- table.print( contable )

	return contable
end

local function FModel( data, props, settings )
	local rawdata = CityRestorans
	local newfm = {}
	local terms = {}
	local selection = {}

	newfm.output = {}
	newfm.listener = function() end

	local function reset()
		newfm.output = {}
		local check = {}
		for i=1, #data do
			if not check[data[i].key] then
				newfm.output[#newfm.output+1] = data[i].key
				check[data[i].key] = true
			end
		end
		
		selection = table.copy( data )
	end

	if settings then
		newfm.output = settings[1]
		terms = settings[2]
		selection = settings[3]
	else reset()
	end

	local function applyTerm( prop_name, values )
		local features = nil
		local exclude = {}
		if prop_name == "features" then
			for i=1, #rawdata do
				features = rawdata[i].features
				if features then
					for j=1, #values do
						for g=1, #features do
							if features[g] == values[j] then break end
							if g == #features then exclude[i] = true end
						end
						if exclude[i] then break end
					end
				else
					exclude[i] = true
				end
			end
		end

		for i=1, #selection do
			if selection[i] then
				if prop_name == "features" then
					if exclude[selection[i].key] then selection[i] = false end
				else
					for j=1, #values do
						if selection[i][prop_name] == values[j] then
							break
						elseif j == #values then
							selection[i] = false
						end
					end
				end
			end
		end
	end    
	
	local function getPropValues( prop_name )
		local values = {}
		local numbers = {}

		local fprops = {}
		local clouse = nil
		for i=1, #props do 
			if props[i] ~= prop_name then
				for j=1, #terms do
					clouse = terms[j][ props[i] ]
					if clouse then
						fprops[props[i]] = {}
						for k,v in pairs( clouse ) do
							if v then fprops[props[i]][k] = true end
						end
						if not next( fprops[props[i]] ) then fprops[props[i]] = nil end
					end
				end
			end
		end		

		local suit = true
		local noduplicates = {}
		local value = nil
		for i=1, #rawdata do
			suit = true
			for prop, values in pairs( fprops ) do
				if not rawdata[i][prop] then
					suit = false
					break
				elseif type( rawdata[i][prop] ) == "table" then
					if prop == "cuisines" then
						for j=1, #rawdata[i][prop] do
							if values[ rawdata[i][prop][j] ] then
								break
							elseif j == #rawdata[i][prop] then
								suit = false
							end
						end							
					elseif prop == "features" then
						for k,v in pairs( values ) do
							for j=1, #rawdata[i][prop] do
								if rawdata[i][prop][j] == k then
									break
								elseif j == #rawdata[i][prop] then
									suit = false
								end
							end
							if not suit then break end	
						end
					end
				elseif not values[ rawdata[i][prop] ] then
					suit = false
					break
				end
			end
			if suit then 
				if type( rawdata[i][prop_name] ) == "table" then
					for j=1, #rawdata[i][prop_name] do
						value = rawdata[i][prop_name][j]
						if not noduplicates[value] then
							values[#values+1] = value
							noduplicates[value] = true
						end
						numbers[value] = ( numbers[value] or 0 ) + 1
					end
				elseif rawdata[i][prop_name] then
					value = rawdata[i][prop_name]
					if not noduplicates[value] then
						values[#values+1] = value
						noduplicates[value] = true
					end
					numbers[value] = ( numbers[value] or 0 ) + 1
				end
			end
		end
		
		if prop_name == "post_name" then
			for i=1, #rawdata do
				if not numbers[rawdata[i].post_name] then
					values[#values+1] = rawdata[i].post_name
					numbers[rawdata[i].post_name] = 0
				end
			end
		end

		table.sort( values, function( a, b )
			a = a or b; b = b or a
			local a1, b1
			local num = utf8.len( a ) < utf8.len( b ) and utf8.len( a ) or utf8.len( b )
			for i=1, num do
				a1 = _G.utf8.sub( a, i, i )
				b1 = _G.utf8.sub( b, i, i )
				if a1 ~= b1 then return _G.utf8.byte( a1 ) < _G.utf8.byte( b1 ) end
			end
		end )

		return values, numbers
	end

	local function filtration()
		local output = {}

		reset()

		local prop, values
		for i=1, #terms do
			prop = next( terms[i] )
			
			values = {}
			for k,v in pairs( terms[i][prop] ) do
				if v then values[#values+1] = k end
			end
			
			if #values ~= 0 then applyTerm( prop, values ) end
		end
		
		local noduplicates = {}
		local key = 0
		for i=1, #selection do
			if selection[i] then
				key = selection[i].key
				if not noduplicates[key] then
					output[#output+1] = key
					noduplicates[key] = true
				end
			end
		end

		newfm.output = output
	end
	
	local function checkIsApplied()
		local prop = ""
		for i=1, #terms do
			prop = next( terms[i] )
			for k,v in pairs( terms[i][prop] ) do
				if v then return true end
			end
		end
		return false
	end

	function newfm:select( prop_name, prop_value, dispatch_event )
		if not prop_name then error( "Filter Model select(): no property name " ) end
		if not prop_value then error( "Filter Model select(): no property value " ) end

		local values = getPropValues( prop_name )
		if #values == 0 then return end

		values = {}
		if #terms == 0 then
			terms[1] = { [prop_name] = { [prop_value] = true } }
		else
			for i=1, #terms do
				if terms[i][prop_name] then
					values = terms[i][prop_name]
					values[prop_value] = true
					prop_name = nil
					break
				end
			end
			if prop_name then
				terms[#terms+1] = { [prop_name] = { [prop_value] = true } }
			end
		end

		filtration()
		if dispatch_event ~= "false" then
			self.listener( self, self.output, checkIsApplied() )
		end
	end

	function newfm:deselect( prop_name, prop_value, dispatch_event )
		local values = {}
		if #terms == 0 then return
		else 
			for i=1, #terms do
				if terms[i][prop_name] then
					values = terms[i][prop_name]
					values[prop_value] = false
					prop_name = nil
					break
				end
			end
			if prop_name then return end
		end

		filtration()
		if dispatch_event ~= "false" then
			self.listener( self, self.output, checkIsApplied() )
		end
	end

	function newfm:reset()
		reset()
		terms = {}
		self.listener( self, self.output, checkIsApplied() )
	end

	function newfm:refresh()
		self.listener( self, self.output, checkIsApplied() )
	end    

	function newfm:getPropValues( prop_name )
		return getPropValues( prop_name )
	end

	function newfm:checkIsSelected( prop_name, prop_value )
		local values = {}
		for i=1, #terms do
			if terms[i][prop_name] then
				values = terms[i][prop_name]
				if values[prop_value] then return true end
			end
		end
		return false
	end

	function newfm:checkIsApplied( prop_name, prop_value )
		return checkIsApplied()
	end

	function newfm:save()
		local settings = {}
		settings[1] = self.output
		settings[2] = terms
		settings[3] = selection
		return settings
	end

	return newfm    
end

local function createTitle( text )
	local pad = 15
	local text_pad = 5
	local height = 20
	local line_height = 0.7
	local line_color = 0.8
	local font = "HelveticaNeueCyr-Light"
	local font_size = 13

	local title = display.newGroup()
	title.anchorChildren = true

	local bg = display.newRect( title, 0, 0, _Xa, height )
	bg:setFillColor( 0.5, 0 )

	local ref_left = -bg.width*0.5 + pad

	local left_line = display.newRect( title, ref_left, 0, 30, line_height )
	left_line:setFillColor( line_color )
	left_line.anchorX = 0

	local title_text = display.newText({
		parent = title,
		text = text,
		font = font,
		fontSize = font_size,
	})
	title_text:setFillColor( 0 )
	title_text.anchorX = 0
	title_text.x = left_line.x + left_line.width + text_pad

	local rlwidth = _Xa - ( left_line.width + title_text.width + pad*2 + text_pad*2 )
	local right_line = display.newRect( title, 0, 0, rlwidth, line_height )
	right_line:setFillColor( line_color )
	right_line.anchorX = 0
	right_line.x = title_text.x + title_text.width + text_pad

	return title
end

local function getTextWidth( text, font, font_size )
	local temp_text = display.newText( text, 0,0, font, font_size )
	local width = temp_text.width
	temp_text:removeSelf()
	temp_text = nil
	return width
end

local function createInformerButton( params )
	local infbutton = display.newGroup()
	infbutton.anchorChildren = true

	infbutton.params = {}
	infbutton.params.label = params.label or ""
	infbutton.params.labelColor = params.labelColor or false
	infbutton.params.font = params.font or "HelveticaNeueCyr-Light"
	infbutton.params.fontSize = params.fontSize or 15
	infbutton.params.width = params.width or "auto"
	infbutton.params.height = params.height or 37
	infbutton.params.leftPad = params.leftPad or 2
	infbutton.params.rightPad = params.rightPad or 7
	infbutton.params.switch = params.switch ~= false and true or false
	infbutton.params.initState = params.initState or "off"
	infbutton.params.initInfo = params.initInfo or 0
	infbutton.params.sheet = params.sheet or false
	infbutton.params.onRelease = params.onRelease or function() end
	params = infbutton.params

	local function newInformer( info, def, over )
		local sheet_options = {
			["small"] = {
				width = 22,
				height = 15,
				numFrames = 2,
				sheetContentWidth = 44,
				sheetContentHeight = 15
			},
			["large"] = {
				width = 32,
				height = 15,
				numFrames = 2,
				sheetContentWidth = 64,
				sheetContentHeight = 15                 
			}
		}
		local sheet_files = {
			["small"] = "assets/filterInformerSmall.png",
			["large"] = "assets/filterInformerLarge.png",
		}

		local labels_color = { 
			{ 1 },
			{ 0, 0.9 }
		}

		if infbutton.i then infbutton.i:removeSelf() end         
		infbutton.i = display.newGroup()
		infbutton.i.anchorChildren = true
	
		local font_size = params.fontSize * 0.8
		local label = display.newText({
			parent = infbutton.i,
			text = info,
			font = "HelveticaNeueCyr-Medium",
			fontSize = font_size,
		})
		label:setFillColor( unpack( labels_color[def] ) )
		
		local sheet = "small"
		if label.width > 15 then sheet = "large" end
		local inf_sheet = graphics.newImageSheet( sheet_files[sheet], sheet_options[sheet] )
		local bg = display.newImageRect( 
			infbutton.i, inf_sheet, def, sheet_options[sheet].width, sheet_options[sheet].height
		)
		bg:toBack()

		infbutton:insert( infbutton.i )
	end

	local function newButton( def, over, sheet, offset, font_size )
		local sheet_options = {
			["small"] = {
				width = 82,
				height = 27,
				numFrames = 2,
				sheetContentWidth = 164,
				sheetContentHeight = 27
			},
			["medium"] = {
				width = 102,
				height = 27,
				numFrames = 2,
				sheetContentWidth = 204,
				sheetContentHeight = 27                
			},
			["large"] = {
				width = 122,
				height = 27,
				numFrames = 2,
				sheetContentWidth = 244,
				sheetContentHeight = 27                 
			}
		}
		local sheet_files = {
			["small"] = "assets/filterButtonSmall.png",
			["medium"] = "assets/filterButtonMedium.png",
			["large"] = "assets/filterButtonLarge.png",
		}
		local buttonSheet
		if params.sheet then buttonSheet = params.sheet
		else buttonSheet = graphics.newImageSheet( sheet_files[sheet], sheet_options[sheet] )
		end

		local labels_color = { 
			{ 1,0,0 },
			{ 0, 0.4 }
		}
		labels_color = params.labelColor or labels_color

		if infbutton.b then infbutton.b:removeSelf() end
		infbutton.b = widget.newButton({
			label = params.label,
			labelColor = { default=labels_color[def], over=labels_color[over] },
			fontSize = font_size or params.fontSize,
			font = params.font,
			labelAlign = "left",
			labelXOffset = offset,
			onRelease = function( event )
				local state = over == 1 and "on" or "off"
				if params.switch then
					if state == "off" or infbutton.params.initInfo > 0 then
						infbutton:update( nil, state ) 
					end
				end
				if state == "off" or infbutton.params.initInfo > 0 then
					params.onRelease( { target = infbutton, state = params.initState } )
				end
			end,
			sheet = buttonSheet,
			defaultFrame = def,
			overFrame = over,               
		})
		infbutton:insert( infbutton.b, true )
		infbutton.b:toBack()
	end

	function infbutton:update( info, state )
		info = info or params.initInfo
		if state == nil then state = params.initState end
		
		if info == params.initInfo and state == params.initState then
			return
		end

		local def, over = 2, 1
		if state == "on" then
			def = 1; over = 2
		end
		params.initState = state
		params.initInfo = info
						
		newInformer( info, def, over )
			
		local label_width = getTextWidth( 
			params.label, params.font, params.fontSize
		)            
		local butn_width = params.width
		if params.width == "auto" then
			butn_width = self.i.width + label_width + params.leftPad + params.rightPad + 10
		end
		local sheet = "small"
		if butn_width > 90 and butn_width <= 115 then
			sheet = "medium"
		elseif butn_width > 115 then
			sheet = "large"
		end
		local label_offset = -params.leftPad
		local font_size = params.fontSize
		if butn_width > 120 then font_size = font_size * 0.8 end
 
		newButton( def, over, sheet, label_offset, font_size )
		self.i.anchorX = 1
		self.i.x = infbutton.b.width*0.5 - params.rightPad
	end
	local state = params.initState
	params.initState = ""
	infbutton:update( params.initInfo, state )

	function infbutton:setEnabled( is_enabled )
		if is_enabled then self.b:setEnabled( true )
		elseif not is_enabled then self.b:setEnabled( false )
		end
	end

	return infbutton
end

local function createFooter( fmodel, fview, height )
	local footer = display.newGroup()
	footer.anchorChildren = true

	local pad = 15    

	local bg = display.newRect( footer, 0, 0, _Xa, height )
	bg:setFillColor( 1, 1, 1, 1 )
	local top = -bg.height*0.5
	local bottom = bg.height*0.5
	local left = -bg.width*0.5
	local right = bg.width*0.5

	local sepr = display.newRect( footer, 0, 0, _Xa, 0.7 )
	sepr:setFillColor( 0,0,0, 0.2 )
	sepr.anchorY = 0
	sepr.y = top

	local buttons = display.newGroup()
	buttons.anchorChildren = true

	local sheet_options = {
		{    
			width = 152,
			height = 37,
			numFrames = 2,
			sheetContentWidth = 304,
			sheetContentHeight = 37
		},
		{    
			width = 112,
			height = 37,
			numFrames = 2,
			sheetContentWidth = 224,
			sheetContentHeight = 37
		}
	}
	local sheet_files = {
		"assets/filterShowButton.png",
		"assets/filterResetButton.png",
	}
	
	local leftsheet = graphics.newImageSheet( sheet_files[1], sheet_options[1] )
	footer.leftbutn = createInformerButton({
		label = translations["show"][_G.Lang],
		labelColor = { {0, 0.9}, {0, 0.9} },
		font = "HelveticaNeueCyr-Medium",
		fontSize = 15,
		leftPad = 0,
		rightPad = 10,
		switch = false,
		initState = "on",
		initInfo = 0,
		sheet = leftsheet,
		onRelease = function( event )
			composer.setVariable( "filter_settings", fmodel:save() )
			composer.setVariable( "filter_output", fmodel.output )
			composer.hideOverlay( "fade", 200 )
		end      
	})
	footer.leftbutn.anchorX = 1
	buttons:insert( footer.leftbutn )

	local rightsheet = graphics.newImageSheet( sheet_files[2], sheet_options[2] )
	footer.rightbutn = widget.newButton({
		label = translations["OrderTxt_ReservCommentClean"][_G.Lang],
		labelColor = { default={ 1,0,0 }, over={ 0 } },
		fontSize = 15,
		font = "HelveticaNeueCyr-Medium",
		onRelease = function( event )
			composer.setVariable( "filter_settings", false )
			composer.setVariable( "filter_mode", "default" )
			composer.setVariable( "filter_output", fmodel.output )
			composer.setVariable( "slider_params", false )
			scene.fmodel:reset()
		end,
		sheet = rightsheet,
		defaultFrame = 1,
		overFrame = 2,     
	})
	footer.rightbutn.anchorX = 0
	buttons:insert( footer.rightbutn, true )
	footer.rightbutn.x = footer.leftbutn.x + pad

	if #fmodel.output == 0 then
		footer.leftbutn:setEnabled( false )
		footer.leftbutn:update( 0, "off" )
	else
		footer.leftbutn:setEnabled( true )
		footer.leftbutn:update( #fmodel.output, "on" )            
	end
	if fmodel:checkIsApplied() then 
		footer.rightbutn:setEnabled( true )
		footer.rightbutn.alpha = 1
	else
		footer.rightbutn:setEnabled( false )
		footer.rightbutn.alpha = 0.5
	end
	
	footer:insert( buttons )
	return footer
end

local function createModeSwitcher()
	local modes = display.newGroup()
	modes.anchorChildren = true

	local init_mode = composer.getVariable( "filter_mode" )

	local bg = display.newRect( modes, 0, 0, _Xa, 50 )
	bg:setFillColor( 1, 0)
	
	local left = -bg.width*0.5
	local pad = 20
	local font_size = 13
	local xoffset = 16*0.5 + 5

	local sheet_options = {
		width = 16,
		height = 16,
		numFrames = 6,
		sheetContentWidth = 32,
		sheetContentHeight = 48
	}
	local sheet_file = "assets/filterViewModeButtons.png"
	local sheet = graphics.newImageSheet( sheet_file, sheet_options )

	local init_state = "off"    
	if init_mode == "list" then init_state = "on" end
	local text_width = getTextWidth( translations["list"][_G.Lang], "HelveticaNeueCyr-Light", font_size )
	modes.list_butn = uilib.newToggleButton({
		label = { on = translations["list"][_G.Lang], off = translations["list"][_G.Lang] },
		labelColor = { on = { 247/255,64/255,66/255 }, off = { 194/255,194/255,194/255} },
		font = "HelveticaNeueCyr-Light",
		fontSize = font_size,
		labelXOffset = xoffset + text_width*0.5, 
		sheet = sheet,
		frameOn = 1,
		frameOff = 2,
		initState = init_state,
		listener = function( event )
			if event.phase == "ended" or event.phase == "cancelled" then
				if event.name == "turnOn" then
					composer.setVariable( "filter_mode", "list" )
					ldb:setSettValue( "filter_mode", "list" )
					modes.map_butn:turnOff()
					modes.tiles_butn:turnOff()
				else composer.setVariable( "filter_mode", "default" )
				end
			end    
		end
	})
	modes:insert( modes.list_butn )

	init_state = "off"    
	if init_mode == "map" then init_state = "on" end
	text_width = getTextWidth( translations["on_the_map"][_G.Lang], "HelveticaNeueCyr-Light", font_size )
	modes.map_butn = uilib.newToggleButton({
		label = { on = translations["on_the_map"][_G.Lang], off = translations["on_the_map"][_G.Lang] },
		labelColor = { on = { 247/255,64/255,66/255 }, off = { 194/255,194/255,194/255} },
		font = "HelveticaNeueCyr-Light",
		fontSize = font_size,
		labelXOffset = xoffset + text_width*0.5,        
		sheet = sheet,
		frameOn = 3,
		frameOff = 4,
		initState = init_state,
		listener = function( event )
			if event.phase == "ended" or event.phase == "cancelled" then
				if event.name == "turnOn" then
					composer.setVariable( "filter_mode", "map" )
					ldb:setSettValue( "filter_mode", "list" )
					modes.list_butn:turnOff()
					modes.tiles_butn:turnOff()
				else composer.setVariable( "filter_mode", "default" )
				end
			end 
		end
	})
	modes:insert( modes.map_butn )
	modes.map_butn.anchorX = 1
	modes.map_butn.x = -modes.list_butn.width*0.5 - pad

	init_state = "off"    
	if init_mode == "tiles" then init_state = "on" end
	text_width = getTextWidth( translations["bricks"][_G.Lang], "HelveticaNeueCyr-Light", font_size )
	modes.tiles_butn = uilib.newToggleButton({
		label = { on = translations["bricks"][_G.Lang], off = translations["bricks"][_G.Lang] },
		labelColor = { on = { 247/255,64/255,66/255 }, off = { 194/255,194/255,194/255} },
		font = "HelveticaNeueCyr-Light",
		fontSize = font_size,
		labelXOffset = xoffset + text_width*0.5,        
		sheet = sheet,
		frameOn = 5,
		frameOff = 6,
		initState = init_state,
		listener = function( event )
			if event.phase == "ended" or event.phase == "cancelled" then
				if event.name == "turnOn" then
					composer.setVariable( "filter_mode", "tiles" )
					ldb:setSettValue( "filter_mode", "tiles" )
					modes.map_butn:turnOff()
					modes.list_butn:turnOff()
				else composer.setVariable( "filter_mode", "default" )
				end
			end 
		end
	})
	modes:insert( modes.tiles_butn )
	modes.tiles_butn.anchorX = 0
	modes.tiles_butn.x = modes.list_butn.width*0.5 + pad    

	return modes
end

local function createCuisinesPanel( names_and_states, onRelease )
	local slpanel = {}

	-- header ---------------------------------------------
	
	local header = display.newGroup()

	local sheet_options = {
		width = 292,
		height = 39,
		numFrames = 2,
		sheetContentWidth = 292,
		sheetContentHeight = 78
	}
	local sheet_file = "assets/filterCuisinesButton.png"

	local header_sheet = graphics.newImageSheet( sheet_file, sheet_options )
	local headerimg1 = display.newImage( header, header_sheet, 1 )
	local headerimg2 = display.newImage( header, header_sheet, 2 )

	local title = display.newText({
		parent = header,
		text = translations["select_cuisine"][_G.Lang],--translations.OrderTxt_ReservDate[self.lang][1],
		font = "HelveticaNeueCyr-Light", 
		fontSize = 13,
		align = "center"
	})
	title:setFillColor( 0, 0.4 )

	-- body -----------------------------------------------

	local body = display.newGroup()
	local body_width = header.width-40

	local function createCuisineLine( name, state, width, height, onRelease )
		local cuisline = display.newGroup()
		cuisline.anchorChildren = true
		cuisline.anchorY = 0
		cuisline.state = state and "on" or "off"
		cuisline.name = name

		local bg = display.newRect( cuisline, 0, 0, width, height )
		bg:setFillColor( 0.95, 0 )
		bg.isHitTestable = true

		local right = -bg.width*0.5

		local options = {
			width = 25,
			height = 25,
			numFrames = 2,
			sheetContentWidth = 50,
			sheetContentHeight = 25
		}
		local rbsheet = graphics.newImageSheet( "assets/orders/smallRadio.png", options )
		local radio_on = display.newImageRect( cuisline, rbsheet, 1, 20, 20 )
		local radio_off = display.newImageRect( cuisline, rbsheet, 2, 20, 20 )
		radio_on.anchorX = 0
		radio_off.anchorX = 0
		radio_on.x = right + 15
		radio_off.x = right + 15
		if cuisline.state == "off" then radio_on.isVisible = false end

		local label = display.newText({
			parent = cuisline, 
			text = name, 
			font = "HelveticaNeueCyr-Light", 
			fontSize = 17
		})
		label:setFillColor( 0 )
		label.anchorX = 0
		label.x = radio_on.x + radio_on.width + 10

		function cuisline:turnOn()
			radio_on.isVisible = true
			self.state = "on"                
		end
		function cuisline:turnOff()
			radio_on.isVisible = false
			self.state = "off"                
		end

		cuisline:addEventListener( "touch", function( event )
			if event.phase == "began" then
				event.target.isFocus = true
				display.getCurrentStage():setFocus( event.target )
			elseif event.phase == "moved" then
				if math.abs( event.y - event.yStart ) >= 10 then
					event.target.isFocus = false
					display.getCurrentStage():setFocus( nil )
					return false
				end
			elseif event.phase == "ended" or event.phase == "cancelled" then
				if event.target.state == "off" then
					event.target:turnOn()
				elseif event.target.state == "on" then
					event.target:turnOff()
				end
				local pEvent = { 
					target = event.target,
					state = event.target.state,
					newName = event.target.name
				}
				onRelease( pEvent )
				display.getCurrentStage():setFocus( nil )
			end
			return true
		end )

		return cuisline
	end

	local newcl, sepr
	local ypos = 10
	for k,v in pairs( names_and_states ) do
		if v then
			headerimg2.isVisible = false
			title.text = k
			title:setFillColor( 1,0,0 )
			scene.currCuisName = k
		end          

		newcl = createCuisineLine( 
			k, v, 
			body_width, 45, 
			function( event )
				event.panel = slpanel
				event.currName = scene.currCuisName
				onRelease( event )
				slpanel:hide(0)
			end 
		)
		body:insert( newcl )
		newcl.y = ypos
		ypos = ypos + newcl.height

		sepr = display.newRect( body, 0, 0, body_width, 1 )
		sepr:setFillColor( 0.95 )
		sepr.y = newcl.y + newcl.height        
	end
	if body[body.numChildren] then body[body.numChildren].isVisible = false end -- убираем последнюю линию

	-- slpanel --------------------------------------------
	
	local sp_height = body.height + header.height
	sp_height = sp_height <= 250 and sp_height or 250

	local slpanel_bg = display.newRoundedRect( 0, 0, body_width, sp_height, 5 )
	slpanel_bg:setFillColor( 1 )
	slpanel_bg:setStrokeColor( 0.9 )
	slpanel_bg.strokeWidth = 1
	slpanel_bg:addEventListener( "touch", function( event )
	  return true
	end )
	
	slpanel_bg.isVisible = false    
	body.isVisible = false    
	   
	local options = { 
		-- general
		id = "cuis",
		width = header.width,
		height = sp_height,
		bg = slpanel_bg,
		speed = 250,
		inEasing = easing.outCubic,
		outEasing = easing.outCubic,
		isScrollable = true,
		topScrollPad = 5,
		bottomScrollPad = 5,            
		onPress = function( event )
		end,
		onStart = function( panel )
			if not next( names_and_states ) then return end
			if panel.completeState == "hidden" then
				  slpanel_bg.isVisible = true    
				  body.isVisible = true    
				panel:toFront() 
			elseif panel.completeState == "shown" then
			end
		end,
		headerHeight = header.height-8.5,
		headerColor = { 0.8, 0 }
	}
	slpanel = widget.newSlidingPanel( options )
	slpanel.anchorChildren = true
	slpanel.header:insert( header )
	slpanel.header.y = slpanel.header.y + 2.5
	slpanel.bg.y = slpanel.bg.y + 2.5
	slpanel:place( body, { top = 5 } )

	function slpanel:setIsActivate( is_active, mess )
		if is_active then
			headerimg2.isVisible = false
			title:setFillColor( 1,0,0 )            
		else headerimg2.isVisible = true                     
			title:setFillColor( 0, 0.4 )
		end
		title.text = mess
	end

	slpanel:hide(0)
	return slpanel
end

local function newDoubleSlider( params )
	local newds = display.newGroup()
	newds.anchorChildren = true

	local bg = display.newRect( newds, 0, 0, params.length + params.handleWidth*2, 10 )
	bg:setFillColor( 0.8, 0 )
	local left = -bg.width*0.5 + params.handleWidth
	local right = bg.width*0.5 - params.handleWidth

	local function calculatePosition( value )
		if value <= params.minValue then return left end
		if value >= params.maxValue then return right end
		local dist = params.maxValue - params.minValue
		local value_perc = (value - params.minValue) / dist
		return math.ceil( params.length * value_perc + left )
	end
	local function calculateValue( xpos )
		if xpos <= left then return params.minValue end
		if xpos >= right then return params.maxValue end
		local dist = right - left
		local xpos_perc = (xpos - left) / dist
		local val = (params.maxValue - params.minValue) * xpos_perc + params.minValue
		return math.ceil( val )
	end

	local left_initpos = calculatePosition( params.initValueLeft )
	local right_initpos = calculatePosition( params.initValueRight )
	newds.left_max = calculatePosition( params.limitLeft ) - params.handleWidth*0.5    
	newds.right_max = calculatePosition( params.limitRight ) + params.handleWidth*0.5

	local back_track = display.newRect( newds, 0, 0, params.length, 3 )
	back_track:setFillColor( 0.8 )
	local front_width = right_initpos - left_initpos
	local front_track = display.newRect( newds, 0, 0, front_width, 3 )
	front_track:setFillColor( 1,0,0 )
	front_track.anchorX = 0
	front_track.x = left_initpos

	-- left slider -----------------------------------------------------

	local leftlabel = display.newGroup()
	leftlabel.anchorChildren = true

	leftlabel.label = display.newText({
		parent = leftlabel,
		text = params.initValueLeft.._G.Currency,
		font = "HelveticaNeueCyr-Light",
		fontSize = 11,
	})
	leftlabel.label:setFillColor( 0.6 )
	leftlabel.label.anchorY = 0

	leftlabel.line = display.newRect( leftlabel, 0, 0, 30, 0.5 )
	leftlabel.line:setFillColor( 0.9 )
	leftlabel.line.anchorY = 0
	leftlabel.line.y = leftlabel.label.height

	leftlabel.x = calculatePosition( params.initValueLeft or params.minValue )
	leftlabel.anchorY = 1
	leftlabel.y = -13
	newds:insert( leftlabel )

	newds.leftslider = widget.newButton({
		onEvent = function( event )
			if newds.isActive == false then return end
			if event.phase == "began" then
				event.target.xDelta = 0
			elseif event.phase == "moved" then
				event.target.xDelta = event.target.xDelta or 0
				local delta = event.x - event.xStart
				local newx = event.target.x + ( delta - event.target.xDelta )
				if newx >= left and newx <= newds.left_max then
					event.target.x = newx
					leftlabel.x = newx
					leftlabel.label.text = calculateValue( newx ).._G.Currency
					front_track.width = front_track.width - ( delta - event.target.xDelta )
					front_track.anchorX = 0
					front_track.x = newx
				end
				event.target.xDelta = delta
			elseif event.phase == "ended" or event.phase == "cancelled" then
				newds.right_max = event.target.x + event.target.width + 10
			end
			event.sliderId = "left"
			event.minValue = params.minValue
			event.maxValue = params.maxValue
			event.leftValue = calculateValue( event.target.x )
			event.leftValue = event.leftValue <= params.limitLeft and event.leftValue or params.limitLeft
			event.rightValue = calculateValue( newds.rightslider.x )
			params.listener( event )
		end,
		sheet = params.handleSheet,
		defaultFrame = 1,
		overFrame = 2,               
	})
	newds:insert( newds.leftslider, true )
	newds.leftslider.x = calculatePosition( params.initValueLeft or params.minValue )

	-- right slider -----------------------------------------------------

	local rightlabel = display.newGroup()
	rightlabel.anchorChildren = true

	rightlabel.label = display.newText({
		parent = rightlabel,
		text = params.initValueRight.._G.Currency,
		font = "HelveticaNeueCyr-Light",
		fontSize = 11,
	})
	rightlabel.label:setFillColor( 0.6 )
	rightlabel.label.anchorY = 0

	rightlabel.line = display.newRect( rightlabel, 0, 0, 30, 0.5 )
	rightlabel.line:setFillColor( 0.9 )
	rightlabel.line.anchorY = 0
	rightlabel.line.y = rightlabel.label.height

	rightlabel.x = calculatePosition( params.initValueRight or params.maxValue )
	rightlabel.anchorY = 0
	rightlabel.y = 10
	newds:insert( rightlabel )

	newds.rightslider = widget.newButton({
		onEvent = function( event )
			if newds.isActive == false then return end
			if event.phase == "began" then
				event.target.xDelta = 0
			elseif event.phase == "moved" then
				event.target.xDelta = event.target.xDelta or 0
				local delta = event.x - event.xStart
				local newx = event.target.x + ( delta - event.target.xDelta )
				if newx >= newds.right_max and newx <= right then
					event.target.x = newx
					rightlabel.x = newx
					rightlabel.label.text = calculateValue( newx ).._G.Currency
					front_track.width = front_track.width + ( delta - event.target.xDelta )
					front_track.anchorX = 1
					front_track.x = newx
				end
				event.target.xDelta = delta
			elseif event.phase == "ended" or event.phase == "cancelled" then
				newds.left_max = event.target.x - event.target.width - 10
			end
			event.sliderId = "right"
			event.minValue = params.minValue
			event.maxValue = params.maxValue
			event.rightValue = calculateValue( event.target.x )
			event.rightValue = event.rightValue >= params.limitRight and event.rightValue or params.limitRight
			event.leftValue = calculateValue( newds.leftslider.x )
			params.listener( event )            
		end,
		sheet = params.handleSheet,
		defaultFrame = 1,
		overFrame = 2,               
	})
	newds:insert( newds.rightslider, true )
	newds.rightslider.x = calculatePosition( params.initValueRight or params.maxValue )

	function newds:setIsActivate( slider, is_active )
		if slider == "left" or slider == "all" then
			self.leftslider:setEnabled( is_active )
			if not is_active then
				self.leftslider.cover = display.newRect( self, 0, 0, 
					params.handleWidth, self.leftslider.height
				)
				self.leftslider.cover:setFillColor( 1, 0.5 )
				self.leftslider.cover.anchorX = 0
				self.leftslider.cover.x = left - params.handleWidth*0.5   
			else self.leftslider.cover:removeSelf()
				self.leftslider.cover = nil
			end
		end
		if slider == "right" or slider == "all" then
			self.rightslider:setEnabled( is_active )
			if not is_active then
				self.rightslider.cover = display.newRect( self, 0, 0, 
					params.handleWidth, self.rightslider.height
				)
				self.rightslider.cover:setFillColor( 1, 0.5 )
				self.rightslider.cover.anchorX = 1
				self.rightslider.cover.x = right + params.handleWidth*0.5   
			else self.rightslider.cover:removeSelf()
				self.rightslider.cover = nil
			end    
		end
		if slider == "all" then
			if is_active then front_track.alpha = 1
			else front_track.alpha = 0.4
			end
		end
	end

	return newds
end

local function createFeaturesSection( names_and_states, width, listener )
	local features = display.newGroup()
	features.anchorChildren = true

	function createFeatureSwitch( name, width, state, listener )
		local fswitch = display.newGroup()
		fswitch.anchorChildren = true

		local bg = display.newRect( fswitch, 0, 0, width, 50 )
		bg:setFillColor( 0,1,0, 0 )
		local left = -bg.width*0.5
		local right = bg.width*0.5

		local label = display.newText({
			parent = fswitch, 
			text = name, 
			font = "HelveticaNeueCyr-Light", 
			fontSize = 13
		})
		label:setFillColor( 0 )
		label.anchorX = 0
		label.x = left

		local switch_container = display.newContainer( fswitch, 70, 40 )
		switch_container.anchorChildren = true

		local switch_bg = display.newImageRect( switch_container, 
			"assets/onOffSwitch-bg.png", 60, 33 
		)

		local options = {
			frames = {
				{ x=0, y=0, width=100, height=35 },
				{ x=1, y=36, width=29, height=29 },
				{ x=32, y=36, width=29, height=29 },
				{ x=63, y=36, width=58, height=32 }
			},
			sheetContentWidth = 121,
			sheetContentHeight = 68
		}
		local onOffSwitchSheet = graphics.newImageSheet( "assets/onOffSwitch.png", options )

		local onOffSwitch = widget.newSwitch
		{
			style = "onOff",
			id = name,
			initialSwitchState = state,
			onRelease = function( event )
				local switch = event.target
				listener( event )
			end,
			
			sheet = onOffSwitchSheet,

			onOffBackgroundFrame = 1,
			onOffBackgroundWidth = 100,
			onOffBackgroundHeight = 35,
			onOffMask = "assets/onOffSwitch-mask.png",

			onOffHandleDefaultFrame = 2,
			onOffHandleOverFrame = 3,

			onOffOverlayFrame = 4,
			onOffOverlayWidth = 58,
			onOffOverlayHeight = 32
		}
		switch_container:insert( onOffSwitch, true )
		if not onOffSwitch.isOn then 
			onOffSwitch.x = -13
			switch_bg.x = 2
		else onOffSwitch.x = 16
			switch_bg.x = 2
		end
		switch_container:scale( 0.9, 0.9 )
		switch_container.anchorX = 1
		switch_container.x = right

		return fswitch
	end

	local ypos = 0
	local switch, sepr
	local name, state
	for i=1, #names_and_states do
		name = names_and_states[i].name
		state = names_and_states[i].state
		switch = createFeatureSwitch( name, width, state, listener )
		features:insert( switch )
		switch.anchorX = 0
		switch.anchorY = 0
		switch.y = ypos

		sepr = display.newRect( features, 0, 0, width, 1 )
		sepr:setFillColor( 0.7, 0.1 )
		sepr.anchorX = 0
		sepr.y = ypos + switch.height

		ypos = ypos + switch.height          
	end

	return features
end

local function FView( fmodel, sections_names )
	local newfv = display.newGroup()
	local scroll_content = display.newGroup()
	local content = display.newGroup()
	local hard_content = display.newGroup()
	local sections = {}
	local side_pad = 15
	local sections_pad = 10

	local bg = display.newRect( newfv, 0, 0, _Xa, _Ya - headerHeight )
	bg:setFillColor( 1, 1, 1, 1 )

	local top = -bg.height*0.5
	local bottom = bg.height*0.5 

	local mode_switch = createModeSwitcher()
	mode_switch.anchorY = 0
	mode_switch.y = top
	content:insert( mode_switch )

	local title = {}
	for i=1, #sections_names do
		sections[i] = display.newGroup()
		title = createTitle( sections_names[i] )
		sections[i]:insert( title )
		content:insert( sections[i] )
	end

	-- cuisines ---------------------------------------------------

	local function createCuisPanel( sectnum )
		local cuisines = sections[sectnum]
	
		local sepr_rect = display.newRect( cuisines, 0, 0, _Xa, 37 )
		sepr_rect:setFillColor( 1, 0 )
		sepr_rect.anchorY = 0
		sepr_rect.y = cuisines[1].y + cuisines[1].height
		cuisines:insert( sepr_rect, true )
	
		local cuisnames = fmodel:getPropValues( "cuisines" )
		local names_and_states = {}
		for i=1, #cuisnames do
			names_and_states[cuisnames[i]] = false
			if fmodel:checkIsSelected( "cuisines", cuisnames[i] ) then
				names_and_states[cuisnames[i]] = true
			end
		end
			
		cuisines.panel = createCuisinesPanel( names_and_states, function( event )
			if event.state == "on" then
				if event.currName then
					fmodel:deselect( "cuisines", event.currName )
				end
				fmodel:select( "cuisines", event.newName )
			elseif event.state == "off" then
				fmodel:deselect( "cuisines", event.newName )
			end
		end )
		cuisines.panel.x = 0
		hard_content:insert( cuisines.panel )
		return cuisines
	end


	-- pos type --------------------------------------------------

	local function createPosTypeBtns( sectnum )
		local postypes = sections[sectnum]
		local postnames, postnumbers = fmodel:getPropValues( "post_name" )
		local init_xpos = -postypes[1].width*0.5 + side_pad
		local xstep = 0
		local ypos = postypes[1].y + postypes[1].height
		local gap = 12   
		local params = {
			label = "",
			font = "HelveticaNeueCyr-Light",
			fontSize = 13,
			width = "auto",
			height = 27,
			switch = true,
			initState = "off",
			initInfo = 0,
			onRelease = function( event ) end      
		}
		postypes.btns = display.newGroup()
		postypes:insert( postypes.btns )
		for i=1, #postnames do
			params.label = postnames[i]
			params.initInfo = postnumbers[postnames[i]]
			if fmodel:checkIsSelected( "post_name", postnames[i] ) then 
				params.initState = "on" 
			else params.initState = "off" 
			end
			params.onRelease = function( event )
				if event.state == "on" then
					fmodel:select( "post_name", postnames[i] )
				else fmodel:deselect( "post_name", postnames[i] )
				end
			end
			
			postypes.btns:insert( createInformerButton( params ) )
			if (xstep + postypes.btns[i].width + gap) > (_Xa - side_pad*2) then
				xstep = 0
				ypos = ypos + postypes.btns[i].height + 12
			end
			postypes.btns[i].anchorX = 0
			postypes.btns[i].x = init_xpos + xstep
			postypes.btns[i].anchorY = 0
			postypes.btns[i].y = ypos
			xstep = xstep + postypes.btns[i].width + gap
		end

		return postypes
	end


	-- average bill ---------------------------------------------------------

	--     local bill = sections[3]
	--     local bill_values = fmodel:getPropValues( "average_bill" )
	--     table.sort( bill_values )
		
	--     local left_init, right_init = 0, 0
	--     local llimit, rlimit = 0, 0
	--     if #bill_values > 1 then
	--         for i=1, #bill_values do
	--             if fmodel:checkIsSelected( "average_bill", bill_values[i] ) then
	--                 left_init = left_init > 0 and left_init or bill_values[i]
	--                 right_init =  bill_values[i]
	--             end
	--         end
	--         left_init = left_init > 0 and left_init or bill_values[1]
	--         right_init = right_init > 0 and right_init or bill_values[#bill_values]

	--         local saved_params = composer.getVariable( "slider_params" )
	--         if saved_params then
	--             if bill_values[1] < saved_params[1] then 
	--                 left_init = saved_params[1] end
	--             if bill_values[#bill_values] > saved_params[2] then
	--                 right_init = saved_params[2]
	--             end
	--             rlimit = bill_values[#bill_values]
	--             for i=1, #bill_values do
	--                 if bill_values[i] <= right_init and bill_values[i] > llimit then
	--                     llimit = bill_values[i]
	--                 end
	--                 if bill_values[i] >= left_init and bill_values[i] < rlimit then
	--                     rlimit = bill_values[i]
	--                 end
	--             end
	--             llimit = llimit - 5
	--             rlimit = rlimit + 5
	--         else
	--             llimit = bill_values[#bill_values] - 5
	--             rlimit = bill_values[1] + 5             
	--         end
	--     elseif #bill_values == 1 then
	--         left_init, right_init = bill_values[1] - 1, bill_values[1] + 1
	--         llimit, rlimit = left_init, right_init
	--     end

	--     local sheet_options = {
	--         width = 20,
	--         height = 22,
	--         numFrames = 2,
	--         sheetContentWidth = 40,
	--         sheetContentHeight = 22
	--     }
	--     local sheet_file = "assets/sliderHandle.png"
	--     local handle_sheet = graphics.newImageSheet( sheet_file, sheet_options )

	--     local function sliderListener( event )
	--         local val, flag 
	--         local dispatch = false
	--         if event.phase == "ended" or event.phase == "cancelled" then
	--             local slider_params = composer.getVariable( "slider_params" )
	--             if slider_params then
	--                 if slider_params[1] == event.leftValue and slider_params[2] == event.rightValue then
	--                     return
	--                 end
	--             end

	--             fmodel:deselect( "average_bill", 0, dispatch )            
	--             for i=1, #bill_values do
	--                 val = bill_values[i]
	--                 if i == #bill_values then dispatch = true end
	--                 if val >= event.leftValue and val <= event.rightValue then
	--                     fmodel:select( "average_bill", val, dispatch )
	--                     flag = true
	--                 else fmodel:deselect( "average_bill", val, dispatch )
	--                 end
	--             end
	--             if not flag then fmodel:select( "average_bill", 0 ) end 
	--             composer.setVariable( "slider_params", { 
	--                 event.leftValue, event.rightValue
	--             } )
	--         end
	--     end

	--     local slider = newDoubleSlider({
	--         length = _Xa - side_pad*2 - 40,
	--         minValue = bill_values[1],
	--         maxValue = bill_values[#bill_values],
	--         initValueLeft = left_init,
	--         initValueRight = right_init,
	--         limitLeft = llimit,
	--         limitRight = rlimit,
	--         handleSheet = handle_sheet,
	--         handleWidth = 20,
	--         listener = sliderListener
	--     })
	--     if #bill_values == 1 then slider:setIsActivate( "all", false ) end
	--     if llimit <= bill_values[1] then slider:setIsActivate( "left", false ) end
	--     if rlimit >= bill_values[#bill_values] then slider:setIsActivate( "right", false ) end
	--     bill:insert( slider )
	--     slider.anchorY = 0
	--     slider.y = bill[1].y + bill[1].height

	-- features -------------------------------------------------------

	local function createFeaturesList( sectnum )
		local features = sections[sectnum]
		local features_values = fmodel:getPropValues( "features" )
		local values_and_states = {}
		for i=1, #features_values do
			if fmodel:checkIsSelected( "features", features_values[i] ) then
				values_and_states[i] = {
					name = features_values[i],
					state = true
				}
			else values_and_states[i] = {
					name = features_values[i],
					state = false
				}
			end
		end
		local fswitches = createFeaturesSection( 
			values_and_states, bg.width - side_pad*2,
			function( event )
				local switch = event.target
				if switch.isOn then
					fmodel:select( "features", switch.id )
				else fmodel:deselect( "features", switch.id )
				end
			end
		)    
		features:insert( fswitches )
		fswitches.anchorY = 0
		fswitches.y = features[1].y + features[1].height - 10
		features.list = fswitches

		return features
	end

	-------------------------------------------------------------------

	local cuisines = createCuisPanel( 1 )
	local postypes = createPosTypeBtns( 2 )
	local features = createFeaturesList( 3 )

	for i=1, #sections do
		sections[i].anchorChildren = true
		sections[i].anchorY = 0
		if i == 1 then sections[i].y = mode_switch.y + mode_switch.height
		else sections[i].y = sections[i-1].y + sections[i-1].height + sections_pad    
		end
	end

	--------------------------------------------------------------------

	local footer = createFooter( fmodel, newfv, 70 )
	footer.anchorY = 1
	footer.y = bottom
	newfv:insert( footer )

	--------------------------------------------------------------------
	
	scroll_content:insert( content )
	scroll_content:insert( hard_content )
	scroll_content.anchorChildren = true
	scroll_content.x = _mX - display.screenOriginX
	scroll_content.anchorY = 0
	scroll_content.y = 0

	cuisines.panel.anchorY = 0
	cuisines.panel.y = top + mode_switch.height + cuisines[1].height + sections_pad
	footer:toFront()
	
	local free_space = _Ya - headerHeight - footer.height
	local scrollView = widget.newScrollView
	{
		--top = 0,
		backgroundColor = { 0.3, 0.8, 0.8 },
		--left = 0,
		width = _Xa,
		height = free_space,
		bottomPadding = 0,
		horizontalScrollDisabled = true,
		hideBackground = true,
		listener = function( event )
			if event.phase == "began" and cuisines.panel.completeState == "shown" then
				cuisines.panel:hide()     
			end
		end
	}   
	newfv:insert( scrollView, true )
	newfv.scrollView=scrollView
	scrollView.anchorY = 0
	scrollView.y = top
	scrollView:insert( scroll_content )

	---------------------------------------------------------------------
	
	function newfv:update()
		cuisines.panel:removeSelf()
		cuisines = createCuisPanel( 1 )
		cuisines.panel.anchorY = 0
		cuisines.panel.y = top + mode_switch.height + cuisines[1].height + sections_pad

		postypes.btns:removeSelf()
		postypes = createPosTypeBtns( 2 )

		features.list:removeSelf()
		features = createFeaturesList( 3 )

		for i=1, #sections do
			sections[i].anchorChildren = true
			sections[i].anchorY = 0
			if i == 1 then sections[i].y = mode_switch.y + mode_switch.height
			else sections[i].y = sections[i-1].y + sections[i-1].height + sections_pad    
			end
		end
		scrollView:setScrollHeight( scroll_content.height )
		
		footer:removeSelf()
		footer = createFooter( fmodel, newfv, 70 )
		footer.anchorY = 1
		footer.y = bottom
		newfv:insert( footer )
		footer:toFront()
	end


	---------------------------------------------------------------------
	
	newfv.anchorChildren = true
	newfv.anchorY = 0
	newfv.y = general_top
	newfv.x = _mX

	return newfv
end

-- -------------------------------------------------------------------------------

function scene:keyback( event )
	composer.hideOverlay()
	Keyback:setFocus( nil )
	Keyback:setFocus( self.parent )
end

function scene:create( event )
print( "...FILTER SCENE"  )
	local sceneGroup = self.view
	local props = {
	  --   "average_bill",
		"cuisines",
		"post_name",
		"features",
	}
	local sections_names = translations["section_names"][_G.Lang]

	local contable = createConsolidatedTable( CityRestorans, props )
	self.fmodel = FModel( contable, props, composer.getVariable( "filter_settings" ) )
	self.fmodel.listener = function( filter, output, is_applied )
		if not filter.view then 
			filter.view = FView( filter, sections_names )
			sceneGroup:insert( filter.view )
			filter.view:toBack()
		else 
			filter.view:update() 
		end
		composer.setVariable( "filter_settings", filter:save() )
	end
	self.fmodel:refresh()
end


function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if ( phase == "will" ) then
		Keyback:setFocus( self )
		self.parent = event.parent
		-- Called when the scene is still off screen (but is about to come on screen).
	elseif ( phase == "did" ) then
		-- Called when the scene is now on screen.
		-- Insert code here to make the scene come alive.
		-- Example: start timers, begin animation, play audio, etc.
	end
end


function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if phase == "will" then
		if event.parent then
			event.parent.content:removeSelf()
		end
		local mode = composer.getVariable( "filter_mode" )
		local output = composer.getVariable( "filter_output" )
		if event.parent then event.parent:setViewMode( mode, output ) end
  
	elseif phase == "did" then
		-- Called immediately after scene goes off screen.
	end
end


function scene:destroy( event )
	local sceneGroup = self.view
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene