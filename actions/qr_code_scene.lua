local scene = composer.newScene()
local libtile = require "ui.RestsTiles"

-- ----------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------

function scene:keyback( event )    
            local prevscene = composer.getSceneName( "previous" )
            local cases = {
                ["actions.main_scene"] = "main",
                ["actions.rests_list_scene"] = "list_main",
                ["actions.my_cabinet_scene"] = "cabinet",
                ["actions.rest_menu_scene"] = "menu",
                ["actions.my_orders_scene"] = "orders",
                ["actions.rest_about_scene"] = "about",
            }
            mySidebar:press( cases[prevscene] )
end

function scene:create( event )
    local sceneGroup = self.view
    CreateBaseHeader()
    
    local bg = display.newRect( sceneGroup, 0, 0, _Xa, _Ya )
    bg:setFillColor( 228/255, 228/255, 228/255 )
    bg.x = _mX
    bg.y = _mY + display.topStatusBarContentHeight
    
    local rt_height = _Ya - headerHeight
    local w=math.min(display.contentWidth,display.contentHeight)
    local qr = display.newImageRect( "qr.jpg", system.DocumentsDirectory,w-50, w-50 )

    sceneGroup:insert( qr )
    qr.x = _mX
    qr.anchorY = 0
    qr.y = general_top*1.4+20
    
    local qr_info = display.newText({
        parent = sceneGroup,
        text = translations["qr_info"][_G.Lang],
        font = "HelveticaNeueCyr-Light",
        fontSize = 11,
        width = bg.width*0.8,
        height = 40,
        align = "left"
    })
    qr_info:setFillColor( 0, 0.8 )
    qr_info.anchorX = 0
    qr_info.x = 10
    qr_info.anchorY = 0
    qr_info.y = qr.y + qr.height +20
    sceneGroup:insert( qr_info )

    local qr_info = display.newText({
        parent = sceneGroup,
        text = translations["tel_info"][_G.Lang],
        font = "HelveticaNeueCyr-Light",
        fontSize = 11,
        width = bg.width*0.8,
        height = 40,
        align = "left"
    })
    qr_info:setFillColor( 0, 0.8 )
    qr_info.anchorX = 0
    qr_info.x = 10
    qr_info.anchorY = 0
    qr_info.y = qr.y + qr.height +70
    sceneGroup:insert( qr_info )    
end


-- "scene:show()"
function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase
    Keyback:setFocus( self )

    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).

    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.
    end
end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene