require "ui.SlideView"
local composer = require( "composer" )
local scene = composer.newScene()
local connection = require "lib.db_remote_common"
local widget = require( "widget" )
local translations = require("translations")
-- local ldb = require( "lib.db_local" )
local clib = require( "lib.common_lib" )
local rdb = require( "lib.db_remote_bonuses" )
local ldb_bonus = require( "lib.db_local_bonuses" )
local uilib = require "ui.uilib"

---------------------------------------------------------------------------------
-- All code outside of the listener functions will only be executed ONCE
-- unless "composer.removeScene()" is called.
---------------------------------------------------------------------------------

-- local forward references should go here

---------------------------------------------------------------------------------
local sceneGroup
local menulistView=nil
local listGroupRowHeight = 40
local listItemRowHeight = 110
local OrderRowTapedColor = {249/255, 178/255, 51/255, 0.7}--{ 0.99, 0.99, 0.83 } 
local OrderRowDefaultColor = { 1, 1, 1} 
local listTitleSize = BasicTextFontSize+1
local orderTitleLen=45
local LEFT_PADDING = 15
local screenGroup
--local screen_size=(display.contentWidth/4).."x"..display.contentHeight/4
local small_image_size="100x80"
local big_image_size="300x200"
local createSlideView, ShowMenuList, CreateMenuHeaderOnItem
local MenuArray
local CurrSlideView, TopSlideView, SecondSlideView, ItemsSlideView
local MenuPhotos={}
local ShowMode="MenulList"
local screenW, screenH = display.contentWidth, display.contentHeight
local newSearchView

function chsize(char)
	if not char then
		return 0
	elseif char > 240 then
		return 4
	elseif char > 225 then
		return 3
	elseif char > 192 then
		return 2
	else
		return 1
	end
end

function utf8sub(str, startChar, numChars)
  local startIndex = 1
  while startChar > 1 do
	  local char = string.byte(str, startIndex)
	  startIndex = startIndex + chsize(char)
	  startChar = startChar - 1
  end
 
  local currentIndex = startIndex
 
  while numChars > 0 and currentIndex <= #str do
	local char = string.byte(str, currentIndex)
	currentIndex = currentIndex + chsize(char)
	numChars = numChars -1
  end
  local substr = str:sub(startIndex, currentIndex - 1)
  return #str > #substr and substr.."..." or substr
end

local function addSearchBtn()
	if myHeader then
		    local sheet =  graphics.newImageSheet( "icons/b_search.png",{width=48, height=48, numFrames=2, sheetContentWidth = 96,sheetContentHeight=48})
			local radius = 16
			local font = { "HelveticaNeueCyr-Thin", "HelveticaNeueCyr-Light" } 
			local gbt = display.newGroup()
			-- gbt.x,gbt.y = radius*1.7, general_top-2+(common_tab_height+4)*0.5 --+(common_tab_height+7)*0.5
			gbt.x,gbt.y = myHeader.width, general_top-2+(common_tab_height+4)*0.5 --+(common_tab_height+7)*0.5
			local bS2
			local bS1 =  widget.newButton ({
	        x =  -radius*1.2, --10,
	        y =  0,
	        sheet = sheet,
	        defaultFrame = 1,
			overFrame = 2,
	        onRelease = function(e)
	        	if sceneGroup.fFind == nil then
	        		e.target.isVisible = false
	        		bS2.isVisible = true
	        		local gFind= display.newGroup()
	        		gFind.x,gFind.y = myHeader.width*0.5,gbt.y
					local bg1 = display.newRect(gFind,0,0,myHeader.width,common_tab_height+7)
					bg1:setFillColor(0,0,0,0.3)
					local corY = 4
		        	local vf = newSearchView({parent=gFind, x=0,y=(common_tab_height+7)*0.5,height =_Ya-headerHeight-(common_tab_height+7)+corY})
		        	local f = widget.newInputField({
						id = "find_price",
						x = -radius*1.2,
						y = 0,
						width = myHeader.width-40,
						height = common_tab_height+7-10,
						width_init = myHeader.width-40-12-35,
						font = font[1],
						fontSize = 18,
						padLeft = 12, 
						padRight = 4,
						offsetX = 4,
						text = '',
						inputType = "alpha",
						typeControl = true,
						symbolsMin = 1,
						inputFont = font[2],
						inputOffsetX = -1,
						inputOffsetY = -2,      
						listener = function( e )
							if e.phase == "editing" then
								if e.text:len()> 0 then
									vf:update(ldb_bonus:findByName(e.text))
								else
									vf:update({})
								end
							end 
						end 
					})
					local bg2 = display.newRoundedRect(f.x,0,f.width-4,f.height-4,8)
					gFind:insert(2,bg2)
					bg2:addEventListener("touch",function() return true end)
					bg2:addEventListener("tap",function() return true end)
					bg2:setFillColor(228/255,228/255,228/255)
					local l = display.newRect(gFind,f.x,bg2.height*0.35,bg2.width-12,2)
					l:setFillColor(0.5,0.5,0.5,0.8)
					gFind:insert(f)
					sceneGroup:insert(gFind)					
					sceneGroup.fFind = gFind
					f:touch(e)
					gbt:toFront()
				-- else
				-- 	sceneGroup.fFind:removeSelf()
				-- 	sceneGroup.fFind = nil
				end
	        end
	    	}
		)
		bS1.width = 36
		bS1.height = 36
		gbt:insert(bS1)
		bS2 =  widget.newButton ({
	        x =  -radius*1.2, --10,
	        y =  0,
	        sheet = sheet,
	        defaultFrame = 2,
			overFrame = 1,
	        onRelease = function(e)
	        	if sceneGroup.fFind ~= nil then
	        		bS1.isVisible = true
	        		bS2.isVisible = false
	       	 		sceneGroup.fFind:removeSelf()
					sceneGroup.fFind = nil
				end
	    	end
	    })
	    bS2.isVisible = false
	    bS2.width = 36
		bS2.height = 36
		gbt:insert(bS2)

		sceneGroup:insert(gbt)
		sceneGroup.gS=gbt
		gbt:toFront()
	end
end

local function networkListenerLoadPhoto( event )
	if ( event.isError ) then
		print ( "Network error - download failed" )
	else
		print("load photo list "..ShowMode)
		print(event.response)
		if event.response~=nil and CurrSlideView and ShowMode=="MenulList" then 
			
			local name=string.gsub(event.response.filename, ".jpg", "")
			local slide_num = tonumber( string.match( name, "s(%d+)" ) )
			local row_num = tonumber( string.match( name, "p(%d+)" ) )
			local photo_id = tonumber( string.match( name, "id(%d+)" ) )
			
			local slide=CurrSlideView:getSlide( slide_num )
			if slide~=nil then
				local row=slide.menulistView:getRowAtIndex( row_num )
				if row~=nil and row.params.data.is_group=="false" and row.photo_id == photo_id then
					row:insert( event.target )
					event.target.x=event.target.width*0.5 + LEFT_PADDING - 6
					event.target.y=listItemRowHeight*0.5
					local mask = graphics.newMask( "assets/avatarMask.png" )
					event.target:setMask( mask )
					event.target.maskScaleX = 0.8; event.target.maskScaleY = 0.8
					event.target.alpha = 0
					transition.to( event.target, { alpha = 1.0 } )
					row.nopicture:removeSelf(); row.nopicture = nil
				else
					event.target:removeSelf()
					event.target=nil			
				end
			else
				event.target:removeSelf()
				event.target=nil			
			end
		elseif event.response~=nil  then
			event.target:removeSelf()
			event.target=nil
		end
	end

	--print ( "event.response.fullPath: ", event.response.fullPath )
	--print ( "event.response.filename: ", event.response.filename )
	--print ( "event.response.baseDirectory: ", event.response.baseDirectory )
end

local function networkListenerItemPhoto( event )
	if ( event.isError ) then
		print ( "Network error - download failed" )
	else
	--print("load photo item "..ShowMode)
		if event.response~=nil and ShowMode=="MenulItem" then
			local name=string.gsub(event.response.filename, ".jpg", "")
			local slide_num=string.gsub(name, "big_photo", "")
			slide_num=string.gsub(slide_num, "s", "")
			event.target.anchorX  = 0.5;
			event.target.anchorY  = 0.5;
			event.target.alpha = 0
			
			local slide = CurrSlideView:getSlide(tonumber(slide_num))
			
			local mask = graphics.newMask( "assets/dishMask.png")
			event.target:setMask( mask )
			slide:insert( 2, event.target )
			transition.to( event.target, { alpha = 1.0 } )
		elseif event.response~=nil  then
			event.target:removeSelf()
			event.target=nil			
		end
	end
end

local function switchSlideView( parentID )
	if CurrSlideView == ItemsSlideView then
		ItemsSlideView:removeSelf(); ItemsSlideView = nil
		CurrSlideView = SecondSlideView or TopSlideView
		if SecondSlideView then
			local top_parent_id = ldb_bonus:getParentID( parentID, true ) 
			CreateMenuHeaderOnItem( top_parent_id, true )
		else CreateBaseHeader()
			Keyback:setFocus( scene )
		end
	elseif CurrSlideView == SecondSlideView then
		SecondSlideView:removeSelf(); SecondSlideView = nil
		CurrSlideView = TopSlideView
		CreateBaseHeader()
		Keyback:setFocus( scene )
	end
	CurrSlideView.isVisible = true
	ShowMode = "MenulList"
end

function CreateMenuHeaderOnItem( parentID,cartAnimate )
	local cartAnimate=cartAnimate
	if cartAnimate==nil then
		cartAnimate=false
	end
	local headerItems = {
		{ 
			id = "parentmenu",
			label = ldb_bonus:getItemName( parentID, true ),
			labelColor = { 1,1,1 },
			fontSize = 14,
			icon = "back",
			iconWidth = 7,
			iconHeight = 13,
			iconY = -1.5,
			labelAnchor = { pos = "right", pad = 5 },
			center = -5,            
			switch = true,
			turnOn = function( event )
				switchSlideView( parentID )
			end
		}        
	}
	if userProfile.activeOrderID ~= 0 then
		headerItems[2] = {              
		id = "ord_sum",
		label = ldb:getOrderSum( userProfile.activeOrderID ).._G.Currency,
		labelColor = { 1,1,1 },
		fontSize = 18,
		icon = "cart",
		iconWidth = 17,
		iconHeight = 15,
		iconY = -1,
		labelAnchor = { pos = "left", pad = 3 },
		animate=cartAnimate,
		params = {},
		switch = true,
		right = 20,
		yShift = -1,
			turnOn = function( event ) mySidebar:press("orders") end, 
		}
	end
	print("CreateMenuHeaderOnItem")
	CreateHeader(headerItems)

	Keyback:setFocus({ keyback = function()
		switchSlideView( parentID )
		Keyback:setFocus( nil )
	end })
end

local function add_pressed_ammount(row,add_btn_pressed,ammount_in_order)
	local add_ammount = display.newImageRect( "icons/plus_ammount.png", 16, 16 )
	add_ammount.anchorY = 0.5
	add_ammount.anchorX = 0.5
	add_ammount.y = add_btn_pressed.y-11
	add_ammount.x = add_btn_pressed.x+11				
	row:insert( add_ammount )
	local txt_ammount = display.newText( tostring(ammount_in_order), 0, 0, nil, listTitleSize-4 )
	txt_ammount.anchorY = 0.5
	txt_ammount.anchorX = 0.5
	txt_ammount.y = add_ammount.y
	txt_ammount.x = add_ammount.x+1
	txt_ammount:setFillColor( 50/255, 50/255, 50/255 )
	row:insert( txt_ammount )
	add_btn_pressed.txt_ammount = txt_ammount
end


local function createCountItemBox(params  )
	local modifiers = ldb:getDataPriceModifiers(params.mi_id,"",-1)
	local enabled_modif = ldb:getRowsData("order_items","local_ord_id="..userProfile.activeOrderID.." and ordi_base_gi_id="..params.gi_id)
--	print("Modifiers for ",params.mi_id)
	local y_add=0
	if #modifiers>0 then
		y_add=100
	end
--	printResponse(enabled_modif)

	local startAmmount=params.startAmmount or "1"
		local function onClose()
			-- body
			if cibox then
			local date = os.date( "*t" ) 
			local dt = os.date('%Y-%m-%d %H:%M:%S', os.time(date))
			local oi_row_id=params.row_id
			local quant=tonumber(cibox.label.text)
			if oi_row_id==nil then
				oi_row_id=ldb:addNewRowData( "order_items", {local_ord_id=userProfile.activeOrderID,ordi_create=dt,mi_id=params.mi_id,com_id=userProfile.SelectedRestaurantData.com_id,ordi_count=quant,ordi_price=params.mi_price,ordi_sum=params.mi_price*quant, mi_name = params.mi_name, gi_id = params.gi_id} )
			end			
			for i,item in pairs( modifiers ) do
				flAction=0
				if item.row_id then
					flAction=2
				else	
					flAction=1
				end
---				print(item.row_id,item.enabled)
				if item.enabled and flAction==1 then
					ldb:addNewRowData( "order_items", {local_ord_id=userProfile.activeOrderID,ordi_modifier_owner_row_id=oi_row_id,ordi_base_gi_id=params.gi_id,has_base_modifiers=true,ordi_create=dt,mi_id=params.mi_id,com_id=userProfile.SelectedRestaurantData.com_id,ordi_count=1,ordi_price=item.price,ordi_sum=item.price, mi_name = item.name, gi_id = item.gi_id} )
				elseif item.enabled and flAction==2 then
					--ldb:updateRowData( "order_items", item.row_id, prepdata )
				elseif item.enabled==false and flAction==2 then	
					ldb:delRow( "order_items", item.row_id )
				end
			end
			end
--			print("onClose!")
		end
		local function onEmptySpacePress()
			native.setKeyboardFocus( nil )
		end		
		cibox = uilib.newBox( 300, 160+y_add*2, onClose,onEmptySpacePress,true )
		local function recalcSum( ammount )
			local ammount=ammount or 0
				local target=cibox
				local price = tonumber( target.price_plate.label.text) --:match( "%d+" )
						target.tempsum =  price*ammount
						target.sum_plate:setText( price*ammount.._G.Currency )
						self.itmpanel.label_sum.text = target.tempsum.._G.Currency
				self.slideview:setTabLabel( self.numslide, self.ordview:getOrderSum() )		    	
		end
		local function textListener( event )

			-- if ( event.phase == "editing" ) then
		 --        -- Output resulting text from "defaultField"
		 --        local v=event.target.text
		 --        if v==nil then
		 --        	v=0
		 --        end
		 --        params.listener( tonumber(v) )

		 --    end
		end		
			local rowItemName = display.newText( 
				params.mi_name, 0, 0, 250, 0,
				"HelveticaNeueCyr-Medium", 16
			)
			rowItemName:setFillColor( 0.4 )
			rowItemName.x=_Xa*0.5-130
			rowItemName.y=_mY-(160+y_add*2)/2+20--_Ya*0.5-95-y_add			
--			print("OOOOOOOOOOOOOOOO",rowItemName.y,cibox.y,cibox.height,_mY,160+y_add*2,display.actualContentHeight)
			rowItemName.anchorX  = 0; rowItemName.anchorY  = 0.5;
			cibox:insert( rowItemName ) 
			local sepr1 = display.newLine( 
				math.round(_Xa*0.5)-150, rowItemName.y+13,  
				math.round(_Xa*0.5)+150, rowItemName.y+13
			)
			sepr1:setStrokeColor( 0,0,0, 0.3 )
			sepr1.strokeWidth = 1
			cibox:insert( sepr1 ) 
			cibox.label = native.newTextField( -200, -100, 130, 30)
			cibox.label.anchorX = 0.5
			cibox.label.align = "left";  cibox.label:setTextColor( 1, 0.5, 0 )
			cibox.label.size = 15
			cibox.label.x=_Xa*0.5+40
			cibox.label.y=rowItemName.y+40
			cibox.label.inputType = "decimal"
			cibox.label.text = startAmmount
			cibox.label:addEventListener( "userInput", textListener  )
			cibox:insert( cibox.label )
			if params.gi_weightflag=="true" then
				native.setKeyboardFocus( cibox.label )
			end
			local rowText = display.newText( 
				translations.OrderTxt_Quantity[_G.Lang]..":", 0, 0, 180, 0,
				"HelveticaNeueCyr-Light", 15
			)
			rowText:setFillColor( 0,0,0 )
			rowText.x=_Xa*0.5-130
			rowText.y=rowItemName.y+40
			rowText.anchorX  = 0; rowText.anchorY  = 0.5;
			cibox:insert( rowText ) 

			if y_add>0 then
					local rowText = display.newText( 
						translations.modifiers[_G.Lang]..":", 0, 0, 180, 0,
						"HelveticaNeueCyr-Light", 15
					)
					rowText:setFillColor( 0 )
					rowText.x=_Xa*0.5-130
					rowText.y=rowItemName.y+72			
					rowText.anchorX  = 0; rowText.anchorY  = 0.5;
					cibox:insert( rowText )				
			    local scrollView = widget.newScrollView
			    {
			        --top = 0,
			       -- backgroundColor = { 0.3, 0.8, 0.8 },
			        --left = 0,
			        width = 280,
			        height = 180,
			        topPadding = 20,
			        bottomPadding = 20,
			        horizontalScrollDisabled = true,
			        hideBackground = true,
			        listener = function( event )
			            display.getCurrentStage():setFocus( nil )
			        end
			    }   
				scrollView.anchorX = 0.5
				scrollView.x = _Xa*0.5
				scrollView.anchorY = 1
				scrollView.y = rowItemName.y+270
			    cibox:insert( scrollView )
				local options = {
						width = 31,
						height = 30,
						numFrames = 2,
						sheetContentWidth = 62,
						sheetContentHeight = 30
				}    		
	    		local radioButtonSheet = graphics.newImageSheet( "assets/orders/itemCheckbox.png", options )
	    		local function onSwitchPress( event )
   					local switch = event.target
   					--if switch.isOn==true then
   					modifiers[switch.id].enabled=switch.isOn
   					--end
    				print( "Switch with ID '"..switch.id.."' is on: "..tostring(switch.isOn) )
	    		end
	    		for i,item in pairs( modifiers ) do
	    			if item.price~=nil and item.price>0 then
						local rowText = display.newText( 
							"+"..item.price, 0, 0, 180, 0,
							"HelveticaNeueCyr-Light", 13
						)
						rowText:setFillColor( 0,0,1 )
						rowText.x=_Xa*0.5+10
						rowText.y=(i-1)*35				
						rowText.anchorX  = 0; rowText.anchorY  = 0.5;
						scrollView:insert( rowText ) 
					end
					local rowText = display.newText( 
						utf8sub(item.name, 0,20 ), 0, 0, 150, 0,
						"HelveticaNeueCyr-Light", 15
					)
					rowText:setFillColor( 0 )
					rowText.x=_Xa*0.5-130
					rowText.y=(i-1)*35				
					rowText.anchorX  = 0; rowText.anchorY  = 0.5;
					scrollView:insert( rowText )
					local iState=false
					if enabled_modif~=nil then
						for i1,item1 in pairs( enabled_modif ) do
							if item1.gi_id==item.gi_id then
								iState=true
								item.row_id=item1.id
							end
						end				 		
					end			  			
					local checkbox = widget.newSwitch
					{
							id = i,
							x = 0,
							y = 0,
							style = "checkbox",
							onPress = onSwitchPress,
							sheet = radioButtonSheet,
							initialSwitchState = iState,
							frameOff = 1,
							frameOn = 2
					}
					checkbox.xScale=0.8;checkbox.yScale=0.8
					checkbox.x=_Xa*0.5+70
					checkbox.y=(i-1)*35	;checkbox.anchorY  = 0.5;
					scrollView:insert( checkbox )
				end
			end
	--print("!!!!!!!!!!!!!!!!!!!!!!1",rowItemName.y,160+y_add*2)
			cibox.lbut = widget.newButton({
				label = "OK",
				labelColor = { default={ 0, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
				fontSize = 13.5,
				font = "HelveticaNeueCyr-Medium",
				--properties for a rounded rectangle button...
				shape="roundedRect",
				width = 120,
				height = 37,
				cornerRadius = 5,
				fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
				strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
				strokeWidth = 2,
				onRelease = function( event )
					onClose()
					params.listener(cibox.label.text)
					cibox:removeSelf()
					cibox = nil
				end	    
		})
		cibox.lbut.anchorX = 0
		cibox.lbut.x = _Xa*0.5-120-10
		cibox.lbut.anchorY = 1
		if #modifiers>0 then
			cibox.lbut.y = rowItemName.y+320
		else
			cibox.lbut.y = rowItemName.y+120
		end
		cibox:insert( cibox.lbut )

		cibox.rbut = widget.newButton({
				label = translations.Cancel[_G.Lang],
				labelColor = { default={ 1, 1, 1 }, over={ 0, 0, 0, 1 } },
				fontSize = 13.5,
				font = "HelveticaNeueCyr-Medium",
				--properties for a rounded rectangle button...
				shape="roundedRect",
				width = 120,
				height = 37,
				cornerRadius = 5,
				fillColor = { default={ 244/255, 64/255, 66/255 }, over={ 211/255,211/255,211/255 } },
				strokeColor = { default={ 244/255, 64/255, 66/255 }, over={ 0,0,0, 0.13 } },
				strokeWidth = 2,
				onRelease = function( event )

					cibox:removeSelf()
				cibox = nil
				end	    
		})
		cibox.rbut.anchorX = 0
		cibox.rbut.x = _Xa*0.5+10
		cibox.rbut.anchorY = 1
		cibox.rbut.y = cibox.lbut.y
		cibox:insert( cibox.rbut )		

	end

local function addmenu_btnRelease( event )
		if not ldb:getSettValue( "guest_id" ) then 
			native.showAlert( _G.appTitle, translations["need_login"][_G.Lang], { "OK" } )
			return 
		end

		local num=#_G.Order
		local order_table=_G.Order[num].data
		local ItemArray = event.target.item_row
		local item_id=ItemArray.mi_id
--		print(item_id)
		local parentID = ldb_bonus:getParentID(item_id,false)
		if userProfile.activeOrderID == 0 then
			ldb:createNewOrder()
		else
			
			local active_order_row=ldb:getRowDataByCondition("orders"," id="..userProfile.activeOrderID)
			if active_order_row.com_id==nil then
				print("Cannot find order by activeOrderID "..userProfile.activeOrderID)
				return
			end
			local active_order_com_id=active_order_row.com_id
			if active_order_com_id~=userProfile.SelectedRestaurantData.com_id then
				print("Wrong restaurant in order, you can not add. Last com_id="..tostring(active_order_com_id).." current com_id="..userProfile.SelectedRestaurantData.com_id)
				return
			end
		end	
		local activeorderitems_row=ldb:getRowDataByCondition("order_items"," local_ord_id="..userProfile.activeOrderID.." AND mi_id = "..item_id..";" )
		local fl_new_row=false
		if activeorderitems_row.mi_id==nil then
			fl_new_row=true
		else
			if activeorderitems_row.mi_id~=item_id then
				fl_new_row=true
			end
		end
		local ammount_in_order
			local function onQuantEnter( quant )
				local quant = tonumber( quant )
				local price=ItemArray.mi_price
				local iname = ItemArray.mi_name
				local date = os.date( "*t" ) 
				local dt = os.date('%Y-%m-%d %H:%M:%S', os.time(date))
				if event.target.pressed_button then
					event.target.alpha=0
					event.target.pressed_button.alpha=1				
				else
					ldb:incrOrderItemCount( activeorderitems_row.id, quant-ammount_in_order )
				end

				if not SecondSlideView and not ItemsSlideView then
					CreateBaseHeader({cartAnimate=true})
				else
					CreateMenuHeaderOnItem( parentID,true )
				end				
			end		
		if not fl_new_row then
			print("increment item")
			ammount_in_order=ldb_bonus:getItemAmountInOrder(item_id)
			if ItemArray.gi_weightflag=="true" then				
				createCountItemBox( {listener=onQuantEnter,gi_weightflag=="true",row_id=activeorderitems_row.item_id,gi_id=ItemArray.gi_id,mi_price=ItemArray.mi_price,mi_name=ItemArray.mi_name,gi_id=ItemArray.gi_id,mi_id=item_id,startAmmount=tostring(ammount_in_order)} )
			else			
				
				if ammount_in_order>1 then
					event.target.txt_ammount.text = ammount_in_order+1
				elseif ammount_in_order==1 then
					add_pressed_ammount(event.target.row,event.target,2)
				end			
				ldb:incrOrderItemCount( activeorderitems_row.id, 1 )
				if not SecondSlideView and not ItemsSlideView then
					CreateBaseHeader({cartAnimate=true})
				else
					CreateMenuHeaderOnItem( parentID,true )
				end	
			end		
		else

			print("add item")
			local modifiers = ldb:getDataPriceModifiers(item_id,"",-1)

			if ItemArray.gi_weightflag=="true" or (modifiers~=nil and #modifiers>0) then				
				createCountItemBox( {listener=onQuantEnter,gi_weightflag==ItemArray.gi_weightflag,row_id=activeorderitems_row.item_id,gi_id=ItemArray.gi_id,mi_price=ItemArray.mi_price,mi_name=ItemArray.mi_name,gi_id=ItemArray.gi_id,mi_id=item_id} )
			else
				event.target.alpha=0
				event.target.pressed_button.alpha=1				
				local price=ItemArray.mi_price
				local iname = ItemArray.mi_name
				local date = os.date( "*t" ) 
				local dt = os.date('%Y-%m-%d %H:%M:%S', os.time(date))
--				print("!!!!!!!!!!!!!!!!!!!!! 111")
				local id = ldb:addNewRowData( "order_items", {local_ord_id=userProfile.activeOrderID,ordi_create=dt,mi_id=item_id,com_id=userProfile.SelectedRestaurantData.com_id,ordi_count=1,ordi_price=price,ordi_sum=price, mi_name = iname, gi_id = ItemArray.gi_id} )
				if not SecondSlideView and not ItemsSlideView then
					CreateBaseHeader({cartAnimate=true})
				else
					CreateMenuHeaderOnItem( parentID,true )
				end				
			end
		end				


end		

-- Handle row rendering
local function onMenuRowRender( event )
	local phase = event.phase
	local row = event.row
	local data = row.params.data
	local slide_index = row.params.slide_index
	local height = listItemRowHeight
	local mask = graphics.newMask( "assets/avatarMask.png" )
	
	local name_text_height
	local name_text_anchorY
	
	if data.is_group=='false' then
		name_text_anchorY=0
		name_text_height=row.contentHeight-50
	else
		name_text_anchorY=0.5;
		name_text_height=row.contentHeight-10
		height = listGroupRowHeight
	end	
	local unt=""
	if data.unt_shortname and data.unt_shortname~="" then
		unt=" ("..data.unt_shortname..")"
 	end
	local rowText = display.newText( 
		utf8sub(data.mi_name, 0,orderTitleLen )..unt, 0, 0, 180, 0,
		"HelveticaNeueCyr-Light", listTitleSize
	)
	rowText.anchorX  = 0; rowText.anchorY  = name_text_anchorY; 
	
	if data.is_group=='false' then
		rowText.x = LEFT_PADDING+100 ;
		rowText.y=13
	else
		rowText.x = LEFT_PADDING;
		rowText.y = row.contentHeight / 2+5
	end
	print("ONMENURENDER")
	rowText:setFillColor( 0/255, 0/255, 0/255 )
	row:insert( rowText )
	
	if data.is_group=='true' then
		local rowArrow = display.newImage( row, "icons/default/backBlack.png" )
		rowArrow:scale( -0.3, 0.3 )
		rowArrow.anchorX = 0
		rowArrow.x = _Xa  - LEFT_PADDING
		rowArrow.y = row.contentHeight * 0.5
		rowArrow.alpha = 0.8
	else
		local nopicture=display.newImage( row, "assets/noDishPhoto.jpg" )
		nopicture.width = nopicture.width*0.2; nopicture.height = nopicture.height*0.2
		nopicture.anchorX  = 0.5
		nopicture.anchorY  = 0.5
		nopicture.x = nopicture.width*0.5 + LEFT_PADDING - 6
		nopicture.y = height*0.5
		nopicture:setMask( mask )
		nopicture.maskScaleX = 0.8; nopicture.maskScaleY = 0.8
		row:insert(nopicture)
		row.nopicture = nopicture

		if data.photo_width~=nil then --hasphoto
			local img_filename="id"..data.photo_id.."c"..userProfile.SelectedRestaurantData.com_id.."s"..tostring(slide_index).."p"..tostring(row.index)..".jpg"
			print("check file: "..img_filename)
			if clib:fileExists(img_filename,system.TemporaryDirectory) then
				local item_img = display.newImage(row, img_filename, system.TemporaryDirectory )
				item_img.anchorX=0.5
				item_img.anchorY=0.5
				item_img.x=item_img.width*0.5 + LEFT_PADDING - 6
				item_img.y=height*0.5
				item_img:setMask( mask )
				item_img.maskScaleX = 0.8; item_img.maskScaleY = 0.8
				nopicture:removeSelf(); nopicture = nil
			else
				print("Loading file..")

				row.photo_id = tonumber( data.photo_id )

				local headers = {}
				headers["Content-Type"] = "application/json"
				headers["Authorization"] = connection.get_authorization_string()

				local params = {}
				params.headers = headers
				params.progress = "download"		
				print("http://"..connection.getconnectionstring().."/dictionary/menu/photoSizeMenu/"..data.photo_id.."?size="..small_image_size)
				display.loadRemoteImage( 
					"http://"..connection.getconnectionstring().."/dictionary/menu/photoSizeMenu/"..data.photo_id.."?size="..small_image_size, 
					"GET", networkListenerLoadPhoto, params,
					img_filename, system.TemporaryDirectory, 
					LEFT_PADDING+40, row.contentHeight * 0.5 
				)
			end
		end
		data.mi_description=data.mi_description or ""
		local TextOptions={
			text= clib:replace_html_tags( data.mi_description or ""),
			x = 0,y = 0,
			font="HelveticaNeueCyr-Light", fontSize=listTitleSize-2, 
			width=150, height=60
		}
		local ItemDescription = display.newText(TextOptions )
		ItemDescription.anchorX  = 0; ItemDescription.anchorY  = 0; 
		ItemDescription.x = LEFT_PADDING+100 ;
		ItemDescription.y = 52
		ItemDescription:setFillColor( 0.6 )
		row:insert( ItemDescription )
		
		local ItemWeight = display.newText( data.mi_weight.." "..translations["Item_Gram"][_G.Lang], 0, 0, "HelveticaNeueCyr-Light", listTitleSize-2 )
		ItemWeight.anchorX  = 0; ItemWeight.anchorY  = 1; 
		ItemWeight.x = LEFT_PADDING+100 ;
		ItemWeight.y = height - 13
		ItemWeight:setFillColor( 0.1 )
		row:insert( ItemWeight )
		if data.mi_weight=="0" then
			ItemWeight.alpha=0
		end
	    local ratingGroup = clib.createRating(data.rating*10)
 		ratingGroup.x=ItemWeight.x+70
  	  	ratingGroup.y=ItemWeight.y-8
    	row:insert(ratingGroup)
		
		local add_btn = widget.newButton{
			defaultFile="icons/plus_grey.png",
			width=25, height=25,
			onRelease = addmenu_btnRelease	-- event listener function
		}
		local ammount_in_order=ldb_bonus:getItemAmountInOrder(data.mi_id)
		
		add_btn.anchorX  = 0.5; add_btn.anchorY  = 0.5; 
		add_btn.x = _Xa - add_btn.width*0.5 - LEFT_PADDING		
		add_btn.y = height*0.5
		add_btn.item_row = data
		row:insert( add_btn )		
		
		local add_btn_pressed = widget.newButton{
			defaultFile="icons/plus_red.png",
			width=25, height=25,
			onRelease = addmenu_btnRelease	-- event listener function
		}
		add_btn_pressed.anchorX  = 0.5; add_btn_pressed.anchorY  = 0.5; 
		add_btn_pressed.x = _Xa - add_btn.width*0.5 - LEFT_PADDING
		add_btn_pressed.y = height*0.5
		add_btn_pressed.item_row = data
		row:insert( add_btn_pressed )				
		add_btn.pressed_button = add_btn_pressed
		add_btn.pressed_button.row=row
		
		if ammount_in_order~=nil then
			add_btn.alpha=0
			add_btn_pressed.alpha=1
			if ammount_in_order>1 and data.gi_weightflag~="true" then
				add_pressed_ammount(row,add_btn_pressed,ammount_in_order)
			end
		else
			add_btn.alpha=1
			add_btn_pressed.alpha=0		
		end
		local priceSize=listTitleSize-2
		if userProfile.SelectedRestaurantData~=nil and userProfile.SelectedRestaurantData.menu_type==5 then --íåò ðàáîòû ñ çàêàçàìè
			priceSize=priceSize+4
			add_btn.alpha=0
			add_btn_pressed.alpha=0
		end
		local cur=""
		if userProfile.SelectedRestaurantData.cur_short then
			cur=" "..userProfile.SelectedRestaurantData.cur_short
		end
		local ItemPrice = display.newText( data.mi_price..cur, 0, 0, "HelveticaNeueCyr-Light", priceSize )
		ItemPrice.anchorX  = 1; ItemPrice.anchorY  = 0; 
		ItemPrice.x = _Xa - LEFT_PADDING		
		ItemPrice.y = add_btn.y + add_btn.height*0.5 + 10
		ItemPrice:setFillColor( 0.1 )
		row:insert( ItemPrice )
		if userProfile.SelectedRestaurantData~=nil and userProfile.SelectedRestaurantData.menu_type==5 then --íåò ðàáîòû ñ çàêàçàìè
			ItemPrice.y = ItemPrice.y -25
		end
	end

	local line = display.newRect( row, 
		row.contentWidth*0.5, height, 
		row.contentWidth - LEFT_PADDING*2, 1.2
	)
	line:setFillColor( 0.9  )
end

local function ShowMenuItem( ItemArray, slide )
	local sidepad = 20

	if ItemArray.photo_width~=nil then 	-- has photo
		local headers = {}
		headers["Content-Type"] = "application/json"
		headers["Authorization"] = connection.get_authorization_string()

		local params = {}
		params.headers = headers
		params.progress = "download"		
		display.loadRemoteImage( 
			"http://"..connection.getconnectionstring().."/dictionary/menu/photoSizeMenu/"..ItemArray.photo_id.."?size="..big_image_size, 
			"GET",
		 	networkListenerItemPhoto,
			params,
			"s"..tostring(slide.index_number).."big_photo.jpg", system.TemporaryDirectory,
			0, -slide.actualHeight*0.5 + 95
		)
	end
	
	local nopicture=display.newImage( slide, "assets/noDishPhoto.jpg" )
	nopicture.width = nopicture.width*0.4; nopicture.height = nopicture.height*0.4
	nopicture.anchorX  = 0.5
	nopicture.anchorY  = 0.5
	nopicture.y=-slide.actualHeight*0.5 + 95
	local mask = graphics.newMask( "assets/dishMask.png" )
	nopicture:setMask( mask )
	slide:insert(nopicture)

	local frame = display.newImageRect( slide, "assets/dishFrame.png", 165, 165 )
	frame.y = -slide.actualHeight*0.5 + 95
	local unt=""
	if ItemArray.unt_shortname and ItemArray.unt_shortname~="" then
		unt=" ("..ItemArray.unt_shortname..")"
 	end
	local ItemName = display.newText({
		text=ItemArray.mi_name..unt,
		x = 0, y = 0,
		font="HelveticaNeueCyr-Light", fontSize=listTitleSize+5, 
		width= slide.actualWidth - sidepad*2, --height=0,
		align = "center"
	})
	ItemName:setFillColor( 0.2 )
	ItemName.anchorX = 0.5;ItemName.anchorY = 0;
	ItemName.x = 0
	ItemName.y = frame.y + frame.height*0.5 + 20
	slide:insert(ItemName)

	local ItemWeight = display.newText(
		ItemArray.mi_weight.." "..translations["Item_Gram"][_G.Lang], 0, 0,  
		"HelveticaNeueCyr-Thin", listTitleSize+2
	)
	ItemWeight:setFillColor( 80/255, 80/255, 80/255 )
	ItemWeight.anchorX = 0;ItemWeight.anchorY = 0.5;
	ItemWeight.x = -slide.actualWidth*0.5 + sidepad
	ItemWeight.y = ItemName.y +  ItemName.height + 30	
	slide:insert(ItemWeight)	
	if ItemArray.mi_weight=="0" then
		ItemWeight.alpha=0
	end
    local ratingGroup = clib.createRating(ItemArray.rating*10)
    ratingGroup.x=ItemWeight.x+30
    ratingGroup.y=ItemWeight.y-20
    slide:insert(ratingGroup)

	local ammount_in_order=ldb_bonus:getItemAmountInOrder(ItemArray.mi_id)	
	
	local add_btn = widget.newButton{
		defaultFile="icons/plus_grey.png",
		width=27, height=27,
		onRelease = addmenu_btnRelease	-- event listener function
	}	
	add_btn.anchorX = 0.5; add_btn.anchorY = 0.5;
	add_btn.x = slide.actualWidth*0.5 - add_btn.width*0.5 - sidepad - 5
	add_btn.y = ItemWeight.y -2	
	add_btn.item_row = ItemArray
	slide:insert(add_btn)	
	
	local add_btn_pressed = widget.newButton{
			defaultFile="icons/plus_red.png",
			width=27, height=27,
			onRelease = addmenu_btnRelease	-- event listener function
		}
	add_btn_pressed.anchorX  = 0.5; add_btn_pressed.anchorY  = 0.5; 
	add_btn_pressed.x = slide.actualWidth*0.5 - add_btn_pressed.width*0.5 - sidepad - 5
	add_btn_pressed.y = ItemWeight.y -2	
	add_btn_pressed.item_row = ItemArray
	slide:insert( add_btn_pressed )				
	add_btn.pressed_button = add_btn_pressed
	add_btn.pressed_button.row=slide
		
	if ammount_in_order~=nil then
		add_btn.alpha=0
		add_btn_pressed.alpha=1
		if ammount_in_order>1 then
			add_pressed_ammount(slide,add_btn_pressed,ammount_in_order)
		end
	else
		add_btn.alpha=1
		add_btn_pressed.alpha=0		
	end
	if userProfile.SelectedRestaurantData~=nil and userProfile.SelectedRestaurantData.menu_type==5 then --íåò ðàáîòû ñ çàêàçàìè
		add_btn.alpha=0
	end

	local cur=""
	if userProfile.SelectedRestaurantData.cur_short then
		cur=" "..userProfile.SelectedRestaurantData.cur_short
	end
	local ItemPrice = display.newText( ItemArray.mi_price..cur, 0, 0,  "HelveticaNeueCyr-Light", listTitleSize+2 )
	ItemPrice:setFillColor( 80/255, 80/255, 80/255 )
	ItemPrice.anchorX = 1; ItemPrice.anchorY = 0.5;
	ItemPrice.x = add_btn_pressed.x - add_btn_pressed.width*0.5 - 7
	ItemPrice.y = ItemWeight.y
	slide:insert(ItemPrice)
	
	local line = display.newRect( slide, 
		0, ItemPrice.y + ItemPrice.height*0.5 + 25, 
		slide.actualWidth - sidepad*2, 1.2
	)
	line:setFillColor( 0.9  )

	local descr_text = ItemArray.mi_description or ""
	local TextOptions={
		text=clib:replace_html_tags(descr_text),
		x = 0, y = line.y + 25,
		font="HelveticaNeueCyr-Light", fontSize=listTitleSize+2, 
		width= slide.actualWidth - sidepad*2, height=slide.actualHeight*0.5 - line.y - 20
	}
	local ItemDesc = display.newText( TextOptions )
	ItemDesc:setFillColor( 0.5 )
	ItemDesc.anchorX = 0.5; ItemDesc.anchorY = 0;
	slide:insert(ItemDesc)
end

function ShowMenuList( ParentID, slide )
	print("menulist. ParentID "..ParentID)

	local function onMenuRowTouch( event )
		local phase = event.phase
		local row = event.row
		if row.params==nil then return end
		local slide_index = row.params.slide_index
		local slide = row.params.slide

		if event.phase == "release" then
			local cover = display.newRect( row, row.width*0.5, row.height*0.5, row.width, row.height )
			cover.fill = OrderRowTapedColor
			timer.performWithDelay( 100, function()
				cover:removeSelf(); cover = nil

				local item = event.row.params.data
				if item.is_group=='true' then
					ShowMode = "MenulList"
					print(ShowMode)
					
					-- create new slide view of category
					MenuArray = ldb_bonus:getCurrentMenuitems( row.params.parentID )
					SecondSlideView = createSlideView( MenuArray, row.id )
				else
					ShowMode = "MenulItem"
					print(ShowMode)

					-- create new slide view of items
					MenuArray = ldb_bonus:getCurrentMenuitems( row.params.parentID )
					ItemsSlideView = createSlideView( MenuArray, row.id )
				end
				CreateMenuHeaderOnItem( row.params.parentID )
			end )
		end
		display.getCurrentStage():setFocus( nil )
	end		

	local menuarray=ldb_bonus:getCurrentMenuitems(ParentID)
--	printResponse(menuarray)
	if menuarray == nil then return end
	--printResponse(menuarray)
		
	local ml = widget.newTableView
	{
		top = -slide.actualHeight*0.5,
		left = display.screenOriginX-display.contentWidth*0.5 ,
		width = _Xa,
		height = slide.actualHeight, --display.contentHeight-50,
		backgroundColor = { 1, 1, 1 },
		noLines = true,
		onRowRender = onMenuRowRender,
		onRowTouch = onMenuRowTouch
	}
	slide:insert( ml )

	slide.menulistView=ml

	local listRowHeight
	for k,row in pairs(menuarray) do 
		if row.is_group=='false'then
			listRowHeight=listItemRowHeight
		else
			listRowHeight=listGroupRowHeight
		end
		ml:insertRow
		{
			isCategory = false,
			--rowColor = { default={ 1, 0.2, 1 }, over={ 1, 0.5, 0, 0.2 } },
			-- lineColor = { 0.5, 0.5, 0.5 },
			rowColor = { default=OrderRowDefaultColor, over=OrderRowDefaultColor },
			rowHeight = listRowHeight,
			params = {data=row,parentID=ParentID,slide=slide,slide_index=slide.index_number}
			
		}
	end
	if #menuarray<=4 then
		ml:setIsLocked(true)
	end
end

newSearchView = function(params)
	local p = params or {}
	p.x = p.x or 0
	p.y = p.y or 0
	p.width = p.width or _Xa
	p.height = p.height or _Ya
	local gr = display.newGroup()
	gr.anchorY = 0
	gr.x,gr.y = p.x,p.y
	if p.parent then
		p.parent:insert(gr)
	end
	local bg = display.newRect(gr,0,0,p.width,p.height)
	bg.anchorY = 0
	bg:addEventListener("touch",function() return true end)
	bg:addEventListener("tap",function() return true end)
	-- bg:setFillColor(0.5,0,0,0.5)
	bg:setFillColor(0.5,0,0,0.5)

	local ml = widget.newTableView
	{
		top = 0, -- -bg.height*0.5,
		left = -bg.width*0.5,
		width = bg.width,
		height = bg.height, --display.contentHeight-50,
		backgroundColor = { 1, 1, 1 },
		noLines = true,
		onRowRender = onMenuRowRender,
		-- onRowTouch = onMenuRowTouch
	}

	gr:insert( ml )
	function gr:update(dataRows)
		dataRows = dataRows or {}
		ml:deleteAllRows()
		for k,row in pairs(dataRows) do 
			if row.is_group=='false'then
				ml:insertRow
				{
					isCategory = false,
					rowColor = { default=OrderRowDefaultColor, over=OrderRowDefaultColor },
					rowHeight = listItemRowHeight,
					params = {data=row}
				}
			end
		end
	end
	return gr;
end

local function loadSlide( slide, slideView )
	local num = slide.index_number
	local data = slideView.MenuArray or MenuArray
	slide.params = {
		autoscale = false,
		hasBg = true,
		bgColor = { 1, 1, 1 },
		--bgStroke = { width = 1.5, color = {0.5, 0, 0} }
	}
	
	if data[num].is_group == "true" then
		ShowMenuList( data[num].mi_id, slide ) 	
	else 
		ShowMenuItem( data[num], slide )
	end
end

local function unloadSlide( slide )
	slide:removeSelf()
	slide = nil
end

local function showSlide( slide, slideView )
	if slide then
		slideView:setTabLabel( slide.index_number, nil, {0,0,0,1} )
	end
end

local function hideSlide( slide, slideView )
	local num = slide.index_number
	slideView:setTabLabel( num, nil, {0,0,0,0.3} )
end

local function reachedFirst( slide )
	local line = display.newLine( 
		slide.x+display.screenOriginX, slide.y, 
		slide.x+display.screenOriginX, slide.height )
	line.strokeWidth = 20
	line.alpha = 0
	
	transition.to( line, { time=1500, alpha=0.4 })
	timer.performWithDelay( 200, function( event )
		line:removeSelf()
	end )
end

local function reachedLast( slide )
	local line = display.newLine( 
		slide.width+display.screenOriginX, slide.y, 
		slide.width+display.screenOriginX, slide.height )
		line.strokeWidth = 20
		line.alpha = 0
		transition.to( line, { time=1500, alpha=0.4 })
	timer.performWithDelay( 200, function( event )
		line:removeSelf()
	end )
end

function createSlideView( data, initslide )
	Spinner:stop()
	if data~=nil then
	local tabbar_height = common_tab_height+7
	local sv_height = _Ya - headerHeight - tabbar_height
	local slideView = widget.newSlideView({ 
		y = 0, 
		--width = 200, 
		height = _Ya-headerHeight-tabbar_height,
		numSlides = #data,
		initSlide = initslide or 1,
		leftPadding = 50,
		--topPadding = 180,
		rightPadding = 50,
		timeMove = 400,

		loadSlide = loadSlide,
		unloadSlide = unloadSlide,
		showSlide = showSlide,
		hideSlide = hideSlide,
		reachedFirst = reachedFirst,
		reachedLast = reachedLast
	})
	slideView.anchorChildren = true
	slideView.anchorY = 0
	slideView.y = general_top - 2
	sceneGroup:insert( slideView )

	slideView.MenuArray = data
	local widthTabsView = display.actualContentWidth-36
	local tabs = {}
	local scale_factor = display.pixelWidth / widthTabsView 
	local twidth = scale_factor >= 2.0 and widthTabsView/3 or widthTabsView/2
	twidth = #data <= 2 and widthTabsView/2 or twidth	
	local bgactive = {}
	local bgadef = {}
	local sepr = nil

	-- twidth = twidth	
	for i,row in pairs(data) do 
		bgactive = display.newGroup()
		bgactive.anchorChildren = true
		display.newRect( bgactive, 0, 0, twidth, tabbar_height )
		display.newImageRect( bgactive, "assets/orders/tabPointer.png", twidth, 7 )
		bgactive[2].anchorY = 1
		bgactive[2].y = tabbar_height*0.5

		bgadef = display.newGroup()
		bgadef.anchorChildren = true
		display.newRect( bgadef, 0, 0, twidth, tabbar_height )
		display.newImageRect( bgadef, "assets/orders/tabBottom.png", twidth, 7 )
		bgadef[2].anchorY = 1
		bgadef[2].y = tabbar_height*0.5        
		
		sepr = nil
		if i > 1 then sepr = display.newRect( 0, -2, 1, tabbar_height-7 ) end

		tabs[#tabs+1] = {
			label = display.newText({
				text = utf8sub( row.mi_name, 1, math.ceil( twidth*0.12 ) ),
				width = twidth*0.8, 
				--height = 30,
				font = "HelveticaNeueCyr-Medium",
				fontSize = 14,
				align = "center"
			}),
			labelColor = { 0,0,0, 0.3 },
			bgDefault = bgadef,        
			bgActive = bgactive,
			separator = sepr,
			seprColor = { 0,0,0, 0.1 }		
		}
	end

	 slideView:attachTabBar(
		{
			--y = 0,
			height = tabbar_height,
			width = widthTabsView
			-- bgDefColor = { 1, 1, 1 },
			-- bgActColor = { 0.5, 1, 0.3 }
		},
		tabs
	 )
	slideView:setTabLabel( slideView.currindex, nil, {0,0,0,1} )

	if CurrSlideView then CurrSlideView.isVisible = false end
	CurrSlideView = slideView

	return slideView
	else
		return nil
	end
end

local function save_menu_to_localdb(event)
	print("save_menu_to_localdb")
	local data = event.data
	local topParentID,topParentName
	if event.result ~= "completed" then
		print("ERROR LOADING menu!!!")
		return
	end
	print("t1")
	--print(userProfile.SelectedRestaurantData.com_id)
		ldb.db:exec("BEGIN TRANSACTION")
	ldb:delRows("menu","com_id="..userProfile.SelectedRestaurantData.com_id)
	print("delRows done")
	local groups={}
	local function getTopParentID(parentID)
		--
		if parentID~=nil and groups[parentID]~=nil then
		if groups[parentID].parent_id==0 then
			return parentID
		else
			return getTopParentID(groups[parentID].parent_id)
		end
		end
		--print(parentID)
		return 0
	end
	
	for i,item in pairs(data.result) do
		if item.is_group=="1" then
			groups[item.mg_id]={}
			groups[item.mg_id].parent_id=item.parent_id
			groups[item.mg_id].name=item.name
		end
		
	end
	for i,item in pairs(data.result) do
		if item.parent_id~=nil then
		local is_group='false'
		if item.is_group=="1" then
			is_group='true'
			topParentID=0
			topParentName=''
		else
			topParentID=getTopParentID(item.parent_id)
			if topParentID==0 then
			topParentName=''
			else
			topParentName=groups[topParentID].name
			end
		end
		--print(item.com_id)
		item.rating=item.rating or 0
		if item.mi_disable~="true" then	
			ldb:addNewRowData( "menu", {name_upper= _G.utf8.upper(item.name),pos_id=item.pos_id,unt_shortname=item.unt_shortname,mi_disable=item.mi_disable,gi_weightflag=item.gi_weightflag,sorting=item.sorting,rating=item.rating,mi_id=item.mg_id,gi_id=item.gi_id,parent_id=item.parent_id,com_id=item.com_id,mi_name=item.name,mi_price=item.price or 0,mi_weight=item.mi_weight or 0,is_group=is_group,top_parent_id=topParentID,top_parent_name=topParentName,photo_width=item.photo_width,photo_height=item.photo_height,photo_format=item.photo_format,photo_id=item.photo_id,mi_description=item.description},nil,true ) --clib:replace_html_tags(item.description)
		end
		-- print("addNewRowData done")
		end
	end
	print("t4")
	ldb.db:exec("COMMIT")  
	print("t5")
	-- create main slide view --
	MenuArray = ldb_bonus:getCurrentMenuitems( 0 )
	TopSlideView = createSlideView( MenuArray )
	addSearchBtn()
	-------------------------------
end

function scene:keyback( event )
	print( "<<<<<<< MENU", composer.getVariable( "came_from" ) )
	local came_from = composer.getVariable( "came_from" )

	if came_from == "order"	or came_from == "about" then
		composer.setVariable( "came_from", "" )
		mySidebar:press( came_from )
	else
		mySidebar:press( "list_main" )
	end
	Keyback:setFocus( nil )
end

-- "scene:create()"
function scene:create( event )
   sceneGroup = self.view
   screenGroup = display.newGroup()
   sceneGroup:insert( screenGroup )
   CreateBaseHeader()
   local n=ldb_bonus:getCurrentMenuitems(0)
   Spinner:start() 
 --  if n~=nil and #n>0 then --прайс уже загружен
--	MenuArray = ldb_bonus:getCurrentMenuitems( 0 )
--	TopSlideView = createSlideView( MenuArray )
 --  else	
 	rdb:getModifiers()
   	rdb:getMenu{listener=save_menu_to_localdb}
 --  end
   
   
--   connection.init_menu(menu_loaded,nil,nil,true)

   ----- WHEREFORE? -----
   --clib:DeleteTempFiles()
   ----------------------

   
   print("mem:"..collectgarbage("count")/1000)	
   print("TEXMEM: "..(system.getInfo("textureMemoryUsed")/1048576))	
   
end

-- "scene:show()"
function scene:show( event )
	Keyback:setFocus( self )
   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
	display.setDefault( "background",  1 )

	  -- Called when the scene is still off screen (but is about to come on screen).
   elseif ( phase == "did" ) then
	  -- Called when the scene is now on screen.
	  -- Insert code here to make the scene come alive.
	  -- Example: start timers, begin animation, play audio, etc.
   end
end

-- "scene:hide()"
function scene:hide( event )

   local sceneGroup = self.view
   local phase = event.phase

   if ( phase == "will" ) then
   	CurrSlideView = nil; TopSlideView = nil; SecondSlideView = nil; ItemsSlideView = nil
	  display.setDefault( "background",  228 / 255, 228 / 255, 228 / 255 )
   elseif ( phase == "did" ) then
	  -- Called immediately after scene goes off screen.
   end
end

-- "scene:destroy()"
function scene:destroy( event )

   local sceneGroup = self.view

   -- Called prior to the removal of scene's view ("sceneGroup").
   -- Insert code here to clean up the scene.
   -- Example: remove display objects, save state, etc.
end

---------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

---------------------------------------------------------------------------------

return scene