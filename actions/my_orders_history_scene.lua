-- local SlideView = require "ui.SlideView"
-- local connection = require "lib.connection"
-- local ovlib = require "ui.OrderView"
local rdb_bonus = require "lib.db_remote_bonuses"
local uilib = require "ui.uilib"

-- local rdb = require "lib.db_remote"
-- local uilib = require "ui.uilib"
local scene = composer.newScene()

local yGPos = display.topStatusBarContentHeight
local  nextDateBtn, prevDateBtn, curDateTitle, curDate
local wS = display.safeActualContentWidth
local hS = display.safeActualContentHeight
local hTopRect = 40
local hTopPanel = 40
local OV = nil
-- local selectedOrder = 0
-- local selectedOrderData = {}
-- local isTypeFour = false
local screenGroup 

--------------------------------------------------------------------------------
local function createHeader()
	local headerItems = {}

	headerItems[1] = {              
		id = "history_orders",
		label = translations["history_orders"][_G.Lang],
		labelColor = { 0,0.7 },
		fontSize = 20,
		icon = "backBlack",
		iconWidth = 7*1.2,
		iconHeight = 13*1.2,
		labelAnchor = { pos = "right", pad = 6 },
		center = 0,        
		switch = true,
		turnOn = function( event ) 
			local prevscene = composer.getSceneName("previous")
			mySidebar:press( _G.scenes_ids[prevscene] or "list_main" )
		end,
	}
  
	CreateHeader( headerItems, { color = {0,0} } ) 
end

local function createBox( name, width, height, onClose )
	local hbox = uilib.newBox( width, height, onClose )
	
	local title = display.newText({
		parent = hbox,
		text = name,
		width = hbox.bg.width*0.8,
		font = "HelveticaNeueCyr-Medium", 
		fontSize = 15,
		align = "center"
	})
	title:setFillColor( 0.1 )
	title.x = hbox.bg.x
	title.anchorY = 0
	title.y = hbox.bg.y - hbox.bg.height*0.5 + 10
			
	local sepr1 = display.newLine( hbox, 
		hbox.bg.x - hbox.bg.width*0.5, title.y + title.height + 10,  
		hbox.bg.x + hbox.bg.width*0.5, title.y + title.height + 10
	)
	sepr1:setStrokeColor( 0,0,0, 0.1 )
	sepr1.strokeWidth = 1.5

	local btn_close = widget.newButton({
		label = translations.CloseBtn[userProfile.lang],
		labelColor = { default={ 1, 0, 0, 1 }, over={ 0, 0, 0, 0.5 } },
		fontSize = 14,
		font = "HelveticaNeueCyr-Medium",
		--properties for a rounded rectangle button...
		shape="roundedRect",
		width = hbox.bg.width * 0.6,
		height = 37,
		cornerRadius = 5,
		fillColor = { default={1,1,1}, over={ 211/255,211/255,211/255 } },
		strokeColor = { default={ 0,0,0, 0.13 }, over={ 0,0,0, 0.13 } },
		strokeWidth = 2,
		onRelease = function( event )
			--onClose()
			hbox:removeSelf()
			hbox = nil
		end     
	})
	hbox:insert( btn_close, true )
	btn_close.x = hbox.bg.x
	btn_close.anchorY = 1
	btn_close.y = hbox.bg.y + hbox.bg.height*0.5 - 15
	hbox:insert( btn_close )

	local sepr2 = display.newLine( hbox, 
		hbox.bg.x - hbox.bg.width*0.5 + 10, 
		btn_close.y - btn_close.height - 15,  
		hbox.bg.x + hbox.bg.width*0.5 - 10, 
		btn_close.y - btn_close.height - 15
	)
	sepr2:setStrokeColor( 0,0,0, 0.1 )
	sepr2.strokeWidth = 1.5  

	return hbox
end

local function getDataOrders(dateValue)
	local subQuery = ""
	if dateValue then
		subQuery = " strftime('%d.%m.%Y', orders.ord_create)='".. dateValue .."' AND "
	end

	local dataOrdersById  = {}
	local condition = ""
	local pmtTypeCondition = ""
	-- if  userProfile.lua_hide_black_cash == "true" then
	-- 	pmtTypeCondition = " and payment_types.pmt_type <> 4"
	-- end

	local sql = [[SELECT  orders.id, orders.guest_id, orders.ord_id, orders.ord_comment,orders.ord_type,orders.ord_create,orders.ord_closed,
	 SUM(ordi_sum-orders.ord_discount_sum+orders.ord_extracharge_sum+orders.ord_extra_tax_sum) as total, ti2.count as cf 
	  FROM orders inner 
				join (select SUM(order_items.ordi_sum) as ordi_sum,local_ord_id from order_items group by local_ord_id) as ti on orders.id=ti.local_ord_id
				left join (select count() as count, i.local_ord_id as l_id from order_items i, menu l where i.mi_id = l.mi_id  group by i.local_ord_id) as ti2 on orders.id=ti2.l_id
				WHERE ]].. subQuery ..[[ orders.id=local_ord_id AND (orders.ord_state=3 or orders.ord_reserv_state = 3)   ]]..pmtTypeCondition..[[ GROUP BY local_ord_id order by local_ord_id desc]]

	
	print("ddd",sql)
	for row in ldb.db:nrows(sql) do 
		if row.cf ~= nil and row.cf > 0 then
			row.hasFisc = true
			row.cf = nil
		else
			row.hasFisc = false
		end
		row.ord_has_fiscal_receipt=row.ord_has_fiscal_receipt or "false"
		row.items = {}
		dataOrdersById[row.id] = row
		if condition == "" then
			condition = "order_items.local_ord_id = "..row.id 
		else
			condition = condition.."  or order_items.local_ord_id = "..row.id
		end
	end

	local tempData = {}
	if condition ~= "" then

		local sql = [[SELECT order_items.*, 
			CASE WHEN order_items.ordi_modifier_owner_row_id>0  THEN modifiers.gi_name 
			ELSE  order_items.mi_name END as name  
			FROM order_items   
			left join modifiers on order_items.gi_id=modifiers.gi_id and modifiers.lang_code = ']].. _G.Lang ..[[' 
			WHERE ]]..condition

		print(sql)
		for item in ldb.db:nrows(sql) do
			table.insert( dataOrdersById[item.local_ord_id].items , {item_id=item.item_id,ordi_id= item.ordi_id,has_base_modifiers=item.has_base_modifiers,ordi_base_gi_id=item.ordi_base_gi_id,amount = item.ordi_count, price = item.ordi_price, mg_id = item.mi_id, name = item.name, parent_id = item.parent_id, total = item.ordi_sum, gi_id = item.gi_id, ordi_modifier_owner_row_id = item.ordi_modifier_owner_row_id,pmt_id=dataOrdersById[item.local_ord_id].pmt_id} )
		end

		for ko, vo in pairs(dataOrdersById) do -- отберем модификаторы и роскидаем по строкам 
			local rowsMod = {} -- модификаторы
			for k, v in pairs(vo.items) do -- отбираем модификаторы
				if v.ordi_modifier_owner_row_id and v.ordi_modifier_owner_row_id > 0 then
					table.insert( rowsMod,v)
				end
			end

			while 1 do -- удаление из vo.items модификаторов
				local k_del = 0
				for k, v in pairs(vo.items) do
					if v.ordi_modifier_owner_row_id and v.ordi_modifier_owner_row_id > 0 then
						k_del = k
						break
					end
				end
				if k_del > 0 then
					vo.items[k_del]=nil 
				else
					break
				end
			end

			for k, v in pairs(vo.items) do 			
				v.modifers={}
				local dk = {}
				for km, vm in pairs(rowsMod) do
					if v.item_id == vm.ordi_modifier_owner_row_id or v.ordi_id == vm.ordi_modifier_owner_row_id then
						table.insert(v.modifers,vm)
						table.insert(dk,km)
					end
				end
				for kdel, vdel in pairs(dk) do
					table.remove( rowsMod, vdel )
				end
			end
		end

		for k, v in pairs(dataOrdersById) do			
			table.insert( tempData,v)
		end
		dataOrdersById = nil
		table.sort( tempData, function (a,b) return a.id > b.id end )
	end
	return tempData
end

local function createScrllVeiw(dataOrders)
	local gRoot = display.newGroup()

	screenGroup:insert(gRoot)
	local wView, hView  = wS, hS-hTopRect-hTopPanel

	local sizeFontItem = css.size_font_4-4
	local sizeFontItemMod = sizeFontItem-2
	local colorFontItems = {0.1,0.1,0.1}
	local colorFontItemsMod = {0.3,0.3,0.3}

	local isSelect=0
	local function createViewOrders(idOrd) -- возвращает группу 
		if gRoot.ordersById[idOrd] and gRoot.ordersById[idOrd].items and  #gRoot.ordersById[idOrd].items >0 then
			local g=display.newGroup()
			local px, py, step = 0,0, 28
			-- printResponse(gRoot.ordersById[idOrd].items)
			local bg = display.newRoundedRect( g, px, py, wView-8, step-8, 2 )
			bg.anchorY=0
			py = step*0.5
			local leftIndentRow=8
			local wRow = bg.width-leftIndentRow*2
			px=-wRow*0.5

			local countRow =  #gRoot.ordersById[idOrd].items
			for k, v in pairs(gRoot.ordersById[idOrd].items) do

				local i_name = display.newText({parent=g,text=v.name or "..name..?",x=px,y=py,width=(wRow-leftIndentRow*3)*0.5,font=native.systemFont,fontSize=sizeFontItem} )
				i_name.anchorX=0,i_name:setFillColor(unpack(colorFontItems))

				local strAmount = v.amount.. " x "..v.price
				local i_amount = display.newText({parent=g,text=strAmount,x=i_name.x+i_name.width+leftIndentRow,y=py,width=(wRow-leftIndentRow*3)*0.25,font=native.systemFont,fontSize=sizeFontItem,align="right"} )
				i_amount.anchorX=0,i_amount:setFillColor(unpack(colorFontItems))
				
				local i_total = display.newText({parent=g,text=string.format("%.2f",v.total),x=i_amount.x+i_amount.width+leftIndentRow,y=py,width=(wRow-leftIndentRow*3)*0.25,font=native.systemFont,fontSize=sizeFontItem,align="right"} )
				i_total.anchorX=0,i_total:setFillColor(unpack(colorFontItems))

				local pmy=0
				local cMod = #v.modifers
				if cMod > 0 then
					pmy= i_name.height
					for k, v in pairs(v.modifers) do
						local i_name = display.newText({parent=g,text=v.name,x=px,y=py+pmy,width=(wRow-leftIndentRow*3)*0.5,font=native.systemFont,fontSize=sizeFontItemMod} )
						i_name.anchorX=0,i_name:setFillColor(unpack(colorFontItemsMod))

						local strAmount = v.amount.. " x "..v.price
						local i_amount = display.newText({parent=g,text=strAmount,x=i_name.x+i_name.width+leftIndentRow,y=py+pmy,width=(wRow-leftIndentRow*3)*0.25,font=native.systemFont,fontSize=sizeFontItemMod,align="right"} )
						i_amount.anchorX=0,i_amount:setFillColor(unpack(colorFontItemsMod))
						
						local i_total = display.newText({parent=g,text=string.format("%.2f",v.total),x=i_amount.x+i_amount.width+leftIndentRow,y=py+pmy,width=(wRow-leftIndentRow*3)*0.25,font=native.systemFont,fontSize=sizeFontItemMod,align="right"} )
						i_total.anchorX=0,i_total:setFillColor(unpack(colorFontItemsMod))

						if k < cMod then
							pmy=pmy+i_name.height
						end

					end
				end
				if k < countRow then
					local l = display.newLine(g,px,py+step*0.5+pmy,px+wRow,py+step*0.5+pmy)
					l:setStrokeColor(0.5,0.5,0.5,0.7)
					l.strokeWidth=2
				end
				py=py+step+pmy
			end
			bg.height = py-step*0.5
			local s = ui_elm:shadow(bg.width,bg.height,4)
			s.x,s.y = bg.x, bg.y+bg.height*0.5
			g:insert( 1,s)
			return g
		else
			return nil
		end
	end


	local optionsSW = {width = 48,height = 48,numFrames = 2,sheetContentWidth = 96,sheetContentHeight = 48}
	local onOffSwitchSheet = graphics.newImageSheet( "icons/aw_row.png", optionsSW ) -- для конопки открытия 
	local sheetRow = graphics.newImageSheet( "icons/b_more_row.png",{width=120,height=120,numFrames=2,sheetContentWidth=240,sheetContentHeight=120})

	local lOrders -- список иницилизация ниже 

	local leftIndent = 10
	local colorFont = {0,0,0}
	local fontSize = css.size_font_4-4

	local function drawItem(dataItem) -- отрисовка 
		dataItem = dataItem or {}
        params = params or {}
        local wi = params.wi or wView
        local hi = params.height or 50
        local colorBG= params.colorBG or {1,1,1}
        -- local colorBG= params.colorBG or {0.3,0,0,0.5}
        local g = display.newGroup()
        local bg = display.newRoundedRect( g, 0, 0, wi, hi, 1 )
        bg:setFillColor(unpack(colorBG))
        g.bg=bg

        local s = ui_elm:shadow(wi,hi)
        g:insert(1,s)
        g.id = dataItem.id 
        
        local l_create = display.newText( g, dataItem.ord_create, -wi*0.5+leftIndent,-math.roundIdp(hi*0.25,0),native.systemFont,fontSize)
        l_create.anchorX=0
        l_create:setFillColor(unpack(colorFont))

        local num = dataItem.id
		-- if userProfile.pos_unique_numbers_for_outlet == "true" or SETTINGS['set_id_in_day'] == 'true' or SETTINGS['set_num_for_type_four']=="true" or SETTINGS['min_id_orders'] > 0 or SETTINGS['max_id_orders'] > 0 then
		-- 	num = dataItem.id_in_day or dataItem.id
		-- 	if num == 0 then
		-- 		num= dataItem.id
		-- 	end
		-- elseif SETTINGS['use_local_id_ord_from_ser'] == 'true' and dataItem.l_id_ser and dataItem.l_id_ser > 0 then
		-- 	nO= dataItem.l_id_ser
		-- end 
		if dataItem.ord_id and dataItem.ord_id > 0 then
			num = num.." ("..dataItem.ord_id..")" 
		end 

		local l_num = display.newText( g,"#"..num, -wi*0.5+leftIndent,0,native.systemFont,fontSize)
        l_num.anchorX=0
        l_num:setFillColor(unpack(colorFont))

        local str_fio = dataItem.user_fio or ""
        -- local ft=""
		-- if dataItem.ord_has_fiscal_receipt=="true" then
		-- 	ft=" "..trans['print_fiscal_reciept'][userProfile.lang_code]
		-- end
		-- str_fio = str_fio.."  "..dataItem.pmt_name
        local l_user_fio = display.newText( g,str_fio, -wi*0.5+leftIndent,math.roundIdp(hi*0.25,0),native.systemFont,fontSize)
        l_user_fio.anchorX=0
        l_user_fio:setFillColor(unpack(colorFont)) 

        local l_total = display.newText( g,string.format( "%.2f",math.roundIdp(dataItem.total)), wi*0.5-30,0,native.systemFont,fontSize)
        l_total.anchorX=1
        l_total:setFillColor(unpack(colorFont))

        g.imCheck = display.newImageRect(g, "icons/check.png", 34, 34 ); 
		g.imCheck.anchorX=0; g.imCheck.anchorY=1; g.imCheck.x=wi*0.5-g.imCheck.width; g.imCheck.y= math.roundIdp(-hi*0.5+g.imCheck.height,0) 
		g.imCheck.isVisible = false

		if dataItem.ord_return == "true" then
			g.imCheck.isVisible = true
		end
		
		dataItem.ord_comment = dataItem.ord_comment or ""
		if dataItem.ord_comment ~= "" then
			local comment = display.newText({parent=g,text=translations["r_comment"][_G.Lang]..": "..dataItem.ord_comment,x=-wi*0.5+leftIndent,y=l_user_fio.y+l_user_fio.height*0.5,width=wi-leftIndent*2-120-l_total.width,font=native.systemFont,fontSize=fontSize-2})
			comment.anchorX=0; comment.anchorY=0 
			comment:setFillColor(unpack(colorFont))
			bg.height = bg.height+comment.height
			local th = comment.height*0.5
			l_create.y=l_create.y-th 
			l_num.y = l_num.y-th
			l_user_fio.y = l_user_fio.y-th
			comment.y = comment.y-th
		end

		local im1 = display.newImage( g, onOffSwitchSheet,2,wi*0.5-15,0)
		im1.width, im1.height = 10,10
		g.im1=im1
		local im2 = display.newImage( g, onOffSwitchSheet,1,wi*0.5-15,0)
		im2.width, im2.height = 10,10
		g.im2=im2
		im2.alpha=0


		-- local bMore = widget.newButton({
		-- 	x=-wi*0.5+leftIndent*0.5,
		-- 	y=0,
		-- 	sheet = sheetRow,
		-- 	defaultFrame = 1,
	 --        overFrame = 2,
	 --        onRelease = function()
	 --        	menu(dataItem)
	 --        end	
		-- })
		-- g:insert(bMore)
		bg:addEventListener( "touch", bg )
		bg.isOn = false
		bg.id = dataItem.id
        bg:addEventListener( "tap", function(e)
        	lOrders.f=nil
        	if e.numTaps == 1 then
	        	if not e.target.isOn then -- рисуем вкладку  и вставляем
	    			local o=createViewOrders(e.target.id)
	    			if o then
	        			o.x,o.y = 0,bg.height*0.5+8
	        			lOrders:addSpaceRowByID(e.target.id,o)
	    				e.target.isOn = true
	    				g.im1.alpha = 0
	    				g.im2.alpha = 1
	        		end
	    		else -- удаляем вкладку 
	    			lOrders:deleteSpaceRowByID(e.target.id)
	    			e.target.isOn = false
	    			g.im1.alpha = 1
	    			g.im2.alpha = 0
	    		end
	    	end
        end)

        g.heightSet = bg.height
        return g
	end

	-- создаем список 
 	lOrders = ui_elm:newRowList({top=hTopRect+yGPos+hTopPanel,
 								left=0,width=wView,
 								height=hView,
 								drawRowItem=drawItem,
 								colorBackground=css.colorGreyText4,
 								drawSpase = createViewOrders,
 								})
    gRoot:insert(lOrders)
    gRoot.list = lOrders

    function gRoot:initList(d) -- закгрузка списка 
		lOrders:clear()
		d = d or  getDataOrders(curDateTitle.text)
		gRoot.dataOrders = d
		gRoot.ordersById = {}
		for k, v in pairs(d) do
			gRoot.ordersById[v.id]=v
		end
		for k, v in pairs(d) do
			lOrders:addRow(v)
		end
	end	
	gRoot:initList(dataOrders)
	-- обновление  списка 
	function gRoot:update(d)
		d = d or  {}
		for k, v in pairs(d) do
			local r, row = lOrders:updateLine(v) 
			-- r  булево если истина установим  кнопку в открыто 
			if r and row and row.bg then
				-- row.b_open:setState( { isOn=true} )
				row.bg.isOn=true
				if row.im1 and row.im2 then
					row.im1.alpha = 0
	    			row.im2.alpha = 1
				end
			end
		end
	end
    return gRoot
end

-- local function createHistoryView( guest_id, data )
-- 	local hview = display.newGroup()
-- 	hview.anchorChildren = true
-- 	local box = {}
-- 	local spinner = {}
		
-- 	local width = _Xa*0.85
-- 	local min_height = 120
-- 	local max_height = _Ya*0.9 - min_height
-- 	local sum_points = 0
-- 	--_print(data)
-- 	local function row( row_data )		
-- 		local row_width = width - 20
-- 		local row_height = 33
-- 	--	_print("############################################")
-- 	--	_printResponse(row_data)
-- 		local row = display.newGroup()
-- 		row.anchorChildren = true

-- 		local bg = display.newRect( row, 0, 0, row_width, row_height )
-- 		bg:setFillColor( 0, 0 )
-- 		local left = -bg.width*0.5
-- 		local right = bg.width*0.5
-- 		local top = -bg.height*0.5
-- 		local bottom = bg.height*0.5

-- 		local rest = display.newText({
-- 			text = row_data.about_name,     
-- 			x = 0,
-- 			y = 0,
-- 			font = "HelveticaNeueCyr-Light",   
-- 			fontSize = 14
-- 		})
-- 		rest:setFillColor( 0 )
-- 		rest.anchorX = 0
-- 		rest.x = left
-- 		rest.anchorY = 0
-- 		rest.y = top
-- 		row:insert( rest )
-- 		_print(row_data.bonus_transaction)
-- 		if row_data.bonus_transaction and row_data.bonus_transaction~="" then
-- 			local date = Date( row_data.bonus_transaction )

-- 			local dm_text = date:fmt( "%d.%m" )
-- 			local dm_label = display.newText({
-- 				text = dm_text,     
-- 				x = 0,
-- 				y = 0,
-- 				font = "HelveticaNeueCyr-Thin",   
-- 				fontSize = 13
-- 			})
-- 			dm_label:setFillColor( 0 )
-- 			dm_label.anchorX = 0
-- 			dm_label.x = left
-- 			dm_label.anchorY = 1
-- 			dm_label.y = bottom
-- 			row:insert( dm_label )

-- 		local year = date:getyear()
-- 		local ylabel = display.newText({
-- 			text = "."..year,     
-- 			x = 0,
-- 			y = 0,
-- 			font = "HelveticaNeueCyr-Thin",   
-- 			fontSize = 8
-- 		})
-- 		ylabel:setFillColor( 0 )
-- 		ylabel.anchorX = 0
-- 		ylabel.x = dm_label.x + dm_label.width - 3
-- 		ylabel.anchorY = 1
-- 		ylabel.y = bottom - 1
-- 		row:insert( ylabel )

-- 		local time = date:fmt( "%H:%M" )
-- 		local tlabel = display.newText({
-- 			text = time,     
-- 			x = 0,
-- 			y = 0,
-- 			font = "HelveticaNeueCyr-Thin",   
-- 			fontSize = 13
-- 		})
-- 		tlabel:setFillColor( 0 )
-- 		tlabel.anchorX = 0
-- 		tlabel.x = ylabel.x + ylabel.width + 5
-- 		tlabel.anchorY = 1
-- 		tlabel.y = bottom
-- 		row:insert( tlabel )
-- 		local txt= translations["bonuses_txt"][_G.Lang]
-- 		local curr_points = row_data.bonus_count_plus
-- 		if not curr_points or row_data.bonus_count_minus > 0 then
-- 			curr_points = "-"..row_data.bonus_count_minus
-- 		elseif  curr_points and curr_points>0 then curr_points = "+"..curr_points
-- 		elseif row_data.bonus_coffee_count ~= nil and row_data.bonus_coffee_count ~= 0 then
-- 			curr_points=" "..row_data.bonus_coffee_count
-- 			txt= translations["cups_txt"][_G.Lang]
-- 		else	
-- 			curr_points=""
-- 		end
-- 		local points = display.newText({
-- 			--text = curr_points.." ["..sum_points.."]",     
-- 			text = curr_points,     
-- 			x = 0,
-- 			y = 0,
-- 			font = "HelveticaNeueCyr-Thin",   
-- 			fontSize = 13
-- 		})
-- 		points:setFillColor( 0 )
-- 		points.anchorX = 0
-- 		points.x = tlabel.x + tlabel.width + 20
-- 		points.anchorY = 1
-- 		points.y = bottom
-- 		row:insert( points )
-- 		local plabel = display.newText({
-- 			text = txt,     
-- 			x = 0,
-- 			y = 0,
-- 			font = "HelveticaNeueCyr-Thin",   
-- 			fontSize = 8
-- 		})
-- 		plabel:setFillColor( 0 )
-- 		plabel.anchorX = 0
-- 		plabel.x = points.x + points.width + 2
-- 		plabel.anchorY = 1
-- 		plabel.y = ylabel.y
-- 		if curr_points=="" then
-- 			plabel.alpha=0
-- 		end
-- 		row:insert( plabel ) 
-- 		end
-- 		local money = display.newText({
-- 			text = row_data.order_sum.._G.Currency,     
-- 			x = 0,
-- 			y = 0,
-- 			font = "HelveticaNeueCyr-Thin",   
-- 			fontSize = 13
-- 		})
-- 		money:setFillColor( 0 )
-- 		money.anchorX = 1
-- 		money.x = right
-- 		money.anchorY = 1
-- 		money.y = bottom
-- 		row:insert( money )          
		
-- 		return row
-- 	end

-- 		local function list()
-- 		local pad = 13
-- 		local ypos = 0
-- 		-- _print("list()")
-- 		-- _printResponse(data)
-- 		for i=1, #data do
-- 			sum_points = sum_points + data[i].bonus_count_plus - data[i].bonus_count_minus
-- 			hview:insert( row( data[i] ) )
-- 			--_printResponse(row( data[i]))
-- 			--_print(data[i] )
-- 			hview[i].anchorY = 0
-- 			hview[i].y = ypos
-- 			ypos = ypos + hview[i].height + pad   
-- 		end

-- 		if hview.height > max_height then
-- 			local content = hview
-- 			hview = widget.newScrollView
-- 			{
-- 				width = width,
-- 				height = max_height,
-- 				bottomPadding = 0,
-- 				horizontalScrollDisabled = true,
-- 				hideBackground = true,
-- 				listener = function( event )
-- 					display.getCurrentStage():setFocus( nil )
-- 				end
-- 			}   
-- 			content.x = hview.width*0.5
-- 			content.anchorY = 0
-- 			content.y = 0
-- 			hview:insert( content )
-- 		end
-- 	end

-- 	local function reload()
-- 		local greload = display.newGroup()
-- 		local mess = display.newText( { 
-- 			width = width - 20,
-- 			parent = greload,
-- 			text = translations.CabinetTxt_History[userProfile.lang][2],
-- 			font = "HelveticaNeueCyr-Light",
-- 			fontSize = 12,
-- 			align = "center"
-- 		} )
-- 		mess:setFillColor( 1,0,0, 0.6 )
-- 		local reloadbutn = widget.newButton
-- 		{
-- 			width = 127*0.5,
-- 			height = 128*0.5,
-- 			defaultFile = "icons/default/reload.png",
-- 			overFile = "icons/over/reload.png",
-- 			onRelease = function( event )
-- 				greload:removeSelf()
-- 				greload = nil
-- 				spinner = Spinner:new()
-- 				box:insert( spinner )
-- 				spinner:start()
-- 				rdb_bonus:getGuestHistory(guest_id,function( event )
-- 					local history = event.data.result
-- 				--	_print("%%%%%")
-- 				--	_printResponse(history)

-- 					if event.result == "completed" then
-- 						scene.history = history
-- 						data = history
-- 						spinner:stop()
-- 						spinner:removeSelf()
-- 						spinner = nil
-- 						box:removeSelf()
-- 						box = nil
-- 					--	_print("############################################1")
-- 						createHistoryView( guest_id, data )
-- 					else 
-- 						spinner:stop()
-- 						spinner:removeSelf()
-- 						spinner = nil
-- 						reload()
-- 					end
-- 				end )
-- 				--rdb:getGuestHistory( guest_id, )
-- 			end
-- 		}
-- 		greload:insert( reloadbutn, true )
-- 		reloadbutn.anchorY = 0
-- 		reloadbutn.y = mess.y + mess.height*0.5 + 15
-- 		hview:insert( greload )
-- 	end

-- 	if data then list()
-- 	else reload()
-- 	end

-- 	return hview
-- 	-- box = createBox( translations.CabinetTxt_History[userProfile.lang][1], width, hview.height + 120 )
-- 	-- box:insert( hview )
-- 	-- hview.x = box.bg.x
-- 	-- hview.anchorY = 0
-- 	-- hview.y = box.bg.y - box.bg.height*0.5 + 45
-- end


--------------------------------------------------------------------------------
function scene:create( event )
print( "ORDERS SCENE CREATE" )
	screenGroup = self.view
	createHeader()
	Keyback:setFocus( self )
	-- local _bg = display.newRect( sceneGroup, 0, 0, _Xa, _Ya + display.topStatusBarContentHeight )
	-- _bg:setFillColor( 228/255, 228/255, 228/255 )
	-- _bg.x = _mX
	-- _bg.y = _mY

	-- local entered 
	-- event.params=event.params or {}
	-- entered = event.params.entered

	-- if entered ~= nil then
	-- 	Spinner:start()
	-- 	rdb_bonus:getGuestHistory( entered.guest_id, function( event )
	-- 		if event.result == "completed" then
	-- 			self.history = event.data.result
	-- 		end
	-- 		Spinner:stop()
	-- 		local VOL = createHistoryView( entered.guest_id, self.history )
	-- 		VOL.x = _Xa*0.5
	-- 		VOL.y = _Ya*0.5
	-- 		sceneGroup:insert(VOL)

	-- 	end )
	-- end

	local gTop = display.newGroup()
	gTop.x,gTop.y = _Xa*0.5, yGPos
	screenGroup:insert(gTop)

	local bgDate = display.newRect( 0, 0, _Xa, hTopRect)
	bgDate:setFillColor(unpack(css.bottomTabColor))
	bgDate.anchorY = 0
	gTop:insert( bgDate )

	--- set DatePicker
	curDate = os.date('*t')
	curDateTitle = display.newText( os.date("%d.%m.%Y", os.time(curDate)) , 0, 0, nil, css.orderRowTitleSize-6 ) 
	curDateTitle.x = bgDate.x; curDateTitle.y = bgDate.y + bgDate.height / 2
	curDateTitle:setFillColor(0)
	gTop:insert( curDateTitle )
	scene.curDate = curDateTitle.text


	local handleNextDateEvent = function ( event )
	    if ( "ended" == event.phase ) then
	    	curDate.day = curDate.day + 1
	    	curDateTitle.text = os.date("%d.%m.%Y", os.time(curDate))
	    	OV:initList()
	    end
	end

	local handlePrevDateEvent = function ( event )
	    if ( "ended" == event.phase ) then
	    	curDate.day = curDate.day - 1
	    	curDateTitle.text = os.date("%d.%m.%Y", os.time(curDate))
	    	OV:initList()
	    end
	end

	local optionsSh = {width=72, height=120, numFrames=2, sheetContentWidth=144, sheetContentHeight=120}
	nextDateBtn = widget.newButton {x=bgDate.width*0.25, y=bgDate.y+bgDate.height*0.5,onEvent = handleNextDateEvent,sheet = graphics.newImageSheet( "icons/b_a_r_br.png", optionsSh ),defaultFrame = 1,overFrame = 2}
	gTop:insert( nextDateBtn )
	nextDateBtn:scale(0.5,0.5)
	prevDateBtn = widget.newButton {x=-bgDate.width*0.25, y=bgDate.y+bgDate.height*0.5, onEvent = handlePrevDateEvent,sheet = graphics.newImageSheet( "icons/b_a_l_br.png", optionsSh ),defaultFrame = 1,overFrame = 2 }
	gTop:insert( prevDateBtn )
	prevDateBtn:scale(0.5,0.5)


	if userProfile.lua_deny_last_day_order_view and userProfile.lua_deny_last_day_order_view == "true" then
		nextDateBtn.alpha = 0
		prevDateBtn.alpha = 0
	end

	OV=createScrllVeiw(getDataOrders(curDateTitle.text))
	local xx,yy =  OV.list.scrll:localToContent(0,0)
	gTop.y = yy-OV.list.scrll.height*0.5-hTopRect
	gTop:toFront()
end

function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	Keyback:setFocus( self )

	if phase == "will" then
		-- display.setDefault( "background",  1 )
		-- self.timerid = timer.performWithDelay( _ORDERS_UPDATE * 1000, function( event )
		-- 	--  если юзер вышел из сцены то отменяем фоновое обновление
		-- 	if not self.view then
		-- 		timer.cancel( self.timerid ); self.timerid = nil; return 
		-- 	-- если есть заказы запускаем фоновое обновление
		-- 	elseif self.slideView then 
		-- 		self.ovm:backgroundUpdate()
		-- 	end		
		-- end, -1 )
	end
end

-- function scene:refresh( )
	-- print("scene:refresh")
	-- if self.slideView then self.ovm:backgroundUpdate() end			
-- end

-- "scene:hide()"
function scene:hide( event )
	local sceneGroup = self.view
	local phase = event.phase

	if phase == "will" then
		-- if self.timerid then timer.cancel( self.timerid ); self.timerid = nil end
		-- display.setDefault( "background",  228 / 255, 228 / 255, 228 / 255 )
	elseif phase == "did" then
		-- Called immediately after scene goes off screen.
	end
end


-- "scene:destroy()"
function scene:destroy( event )
	print( "...DESTROY MY ORDERS" )
	local sceneGroup = self.view
	-- self.ovm:deleteAllOrdersViews()
	-- Called prior to the removal of scene's view ("sceneGroup").
	-- Insert code here to clean up the scene.
	-- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene