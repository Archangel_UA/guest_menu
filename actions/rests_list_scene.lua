local composer = require "composer"
-- 
local common_lib = require "lib.common_lib"
local rdb = require "lib.db_remote"
local libtile = require "ui.RestsTiles"
local qrscanner = require('plugin.qrscanner')

local scene = composer.newScene()

local function createHeader( bg_params )
    local headerItems = {
        {              
            id = "city",
            label = userProfile.currentCity.name or translations["select_city"][_G.Lang],
            labelColor = { 1,1,1 },
            fontSize = 15,
            params = {},
            switch = true,
            turnOn = function( event ) 
                createCityBox( function()
                        local rests = {}
                        for i=1, #CityRestorans do 
                            print("createHeader,createCityBox",i)
                            rests[i] = i
                        end
                        composer.setVariable( "filter_output", rests )
                        createHeader( bg_params )
                end )
            end,
        }
    }
    headerItems[2] = {
        id = "filter",
        label = "",
        icon = "filter",
        iconWidth = 22,
        iconHeight = 13,
        switch = true,
        right = 20,
        turnOn = function( event ) 
            local options = {
                isModal = true,
                effect = "fade",
                time = 200,
                params = {}
            }
		if composer.getSceneName( "overlay" ) then
			composer.removeScene( composer.getSceneName( "overlay" ) )
		  end		
		_G.overlayIsActive=true
            composer.showOverlay( "actions.filter_overlay", options )
        end,
        turnOff = function( event ) _G.overlayIsActive=false; composer.hideOverlay( "fade", 200 ) end
    }
    headerItems[3] = {
        id = "map",
        label = "",
        icon = "map",
        iconWidth = 12,
        iconHeight = 15,
        switch = true,
        right = 60,
        turnOn = function( event )
            local options = {
                isModal = true,
                effect = "fade",
                time = 400,
                params = {
                }
            }
		if composer.getSceneName( "overlay" ) then
			composer.removeScene( composer.getSceneName( "overlay" ) )
		end		
            _G.overlayIsActive=true
            composer.showOverlay( "actions.map_overlay", options )
        end,
        turnOff = function( event ) _G.overlayIsActive=false; composer.hideOverlay() end
    }
    headerItems[4] = {
        id = "qr_search",
        label = "",
        icon = "searchqr",
        iconWidth = 13,
        iconHeight = 13,
        switch = true,
        left = 60,
        turnOn = function( event ) 
            local function listener(e)  
                -- e variable contains the value of a scanned QR Code.
                -- if type(e) == 'table' then
                    -- native.showAlert('QR Code Scanner', json.encode(e), {'OK'})
                -- else
                    -- native.showAlert('QR Code Scanner', e, {'OK'})
                -- end
                -- e = e or {}
                -- e.message = "https://77.123.129.42/en_3142"
                -- e.isError = false
                if e and e.isError == false then
                    local founded=false
                    e.message = e.message or 0
                    if type(e.message) == "string" then
                        local arr_mark = {"web","v1","en"}
                        local id_num
                        for k, v in pairs(arr_mark) do
                            id_num =  e.message:match(v.."_%d+")
                            if id_num ~= nil then
                                id_num = id_num:match("%d+",v:len()+1)
                                id_num = tonumber(id_num) 
                                break
                            end
                        end
                        if id_num then
                            e.message = id_num
                        else
                            e.message = tonumber(e.message)
                            e.message = e.message or 0
                        end
                    end
                    for i=1, #CityRestorans do 
                            if CityRestorans[i].pos_id==e.message then
                                founded=true
                                userProfile.SelectedRestaurantData = CityRestorans[i]
                                userProfile.activeOrderID = 0
                                _G.defineUserCity( CityRestorans[i].city,CityRestorans[i].city_id )
                                ldb:setSettValue( "last_rest", CityRestorans[i].pos_id )
                                CreateSideBar()
                                mySidebar:press("about")            
                            end
                    end 
                    if founded==false then
                        native.showAlert(translations["wrong_outlet_qr"][_G.Lang], e.message, {'OK'})
                    end
                elseif e.errorMessage and e.errorCode  then
                    native.showAlert(translations["wrong_outlet_qr"][_G.Lang], "code error: "..e.errorCode.."\n"..e.message, {'OK'})
                end
            end
            qrscanner.show({listener=listener,topbar  = {text = translations["scan_outlet_qr"][_G.Lang]}})
        end,
        turnOff = function( event )  end
    }
    CreateHeader( headerItems, bg_params ) 
end

local function createRestBoard( rest_data )
    local rest_of_day_id = tonumber( ldb:getSettValue( "rest_of_the_day" ) )
    local is_rest_of_day = false

   --  if not rest_data then
   --      for i=1, #CityRestorans do
   -- --         print(CityRestorans[i].pos_id)
   --          if CityRestorans[i].pos_id == rest_of_day_id then
   --              rest_data = CityRestorans[i]
   --              is_rest_of_day = true
   --              break
   --          end
   --          if i == #CityRestorans then rest_data = CityRestorans[1] end
   --      end
   --  elseif rest_data.com_id == rest_of_day_id then
   --      is_rest_of_day = true
   --  end

    local board = display.newGroup()
    board.anchorChildren = true
    board.x = _mX
    board.anchorY = 0
    if rest_data~=nil then
    local bg_file = rest_data.main_photo.name
    local base_dir = rest_data.main_photo.basedir
    local bg_height = 240
    print(bg_file,base_dir)
    local bg = display.newImageRect( board,
        bg_file, base_dir,
        bg_height*rest_data.main_photo.aspect, bg_height
    )

    local cover = display.newRect( board, 0, 0, _Xa, bg_height )
    cover:setFillColor( 0, 0.45 )

    local ref_left = -_Xa*0.5
    local ref_right = _Xa*0.5
    local ref_top = bg.y - bg.height*0.5
    local ref_centerY = bg.y

    local red_inscript 
    if is_rest_of_day then
        red_inscript = display.newGroup()
        red_inscript.anchorChildren = true
        red_inscript.anchorX = 0
        red_inscript.x = ref_left
        red_inscript.y = ref_centerY - 15
        board:insert( red_inscript )
        red_inscript.bg = display.newRect( red_inscript, 0, 0, 115, 28 )
        red_inscript.bg:setFillColor( 247/255, 64/255, 66/255 )
        red_inscript.label = display.newText({
            parent = red_inscript,
            text = translations["rest_of_the_day"][_G.Lang],
            x = 0, y = 0,
            font = "HelveticaNeueCyr-Light",
            fontSize = 12
        })
        red_inscript.label:setFillColor( 1,1,1,1 )
    end

    local time_icon = display.newImageRect( board, 
        "icons/clock.png", 
        20, 20 
    )
    time_icon.anchorX = 0
    time_icon.x = 7 + (red_inscript and red_inscript.x + red_inscript.width or ref_left + 10 )
    time_icon.y = ref_centerY - 15

    local time_label = display.newText({
        parent = board,
        text = rest_data.work_time,
        font = "HelveticaNeueCyr-Light",
        fontSize = 14
    })
    time_label.anchorX = 0
    time_label.x = time_icon.x + time_icon.width + 5
    time_label.y = ref_centerY - 15

    local rest_address = display.newText({
        parent = board,
        text = rest_data.addr,
        font = "HelveticaNeueCyr-Light",
        fontSize = 14,
        width = _Xa - 30
    })
    rest_address.anchorX = 0
    rest_address.x = ref_left + 17
    rest_address.anchorY = 0
    rest_address.y = time_icon.y + time_icon.height*0.5 + 10

    if rest_data.com_loyalty_type == 2 or rest_data.com_loyalty_type == 1 then
        local bonus = display.newImageRect( board,
            "assets/bonusIcon.png", 28, 28
        )
        bonus.anchorX = 1; bonus.anchorY = 1
        bonus.x = ref_right - 17
        bonus.y = rest_address.y + rest_address.height
    end      

    local sepr_line = display.newLine( board, 
        ref_left + 17, rest_address.y + rest_address.height + 13, 
        ref_right - 17, rest_address.y + rest_address.height + 13
    )
    sepr_line:setStrokeColor( 1, 0.3 )
    sepr_line.strokeWidth = 1

    local rest_title = display.newText({
        parent = board,
        text = rest_data.title or "No data",
        font = "HelveticaNeueCyr-Medium",
        fontSize = 23,
        width = _Xa - 30
    })
    rest_title.anchorX = 0
    rest_title.x = ref_left + 17
    local free_space = bg.contentBounds.yMax - sepr_line.contentBounds.yMax
    rest_title.y = sepr_line.y + free_space*0.5 - 5

    local touch_pad = display.newRect( board, 0, 0, _Xa, bg.height - myHeader.height )
    touch_pad.isVisible = false
    touch_pad.isHitTestable = true
    touch_pad.anchorY = 0
    touch_pad.y = ref_top + myHeader.height
    touch_pad:addEventListener( "touch", function( event )
       if event.phase == "began" then
            event.target.isFocus = true
            display.getCurrentStage():setFocus( event.target )
            cover:setFillColor( 1, 0.2 )
        elseif event.phase == "ended" or event.phase == "cancelled" then
            if event.target.isFocus then
                cover:setFillColor( 0, 0.45 )
                userProfile.SelectedRestaurantData = rest_data
                userProfile.activeOrderID = 0
                CreateSideBar()
                event.target.isFocus = false
                display.getCurrentStage():setFocus( nil )
                mySidebar:press("about")
            end
        end
    end )
    end
    return board
end

local function createListRestsTiles( height, rests )
    local rest_of_day_id = 0
    local pad = 7
    local numcolums = 2 
    local tile_width = (_Xa - (pad*3)) / numcolums
    local tile_height = 120
    local tiles = {}
    
    local index = 0
    for i=1, #rests do
        index = rests[i]
        tiles[i] = ( libtile:createRestTile( tile_width, tile_height, CityRestorans[index] ) )
    end
    -- for i=4, 6 do
    --  tiles[i] = ( libtile:createRestTile( tile_width, tile_height, CityRestorans[i-3] ) )
    -- end
    
    local tiles_group = widget.newTilesGroup({
        tiles = tiles,
        width = _Xa,
        height = height,
        columnTilesNumber = numcolums,
        verticalPad = pad,
        horizontalPad = pad,
        topPad = pad,
        bottomPad = pad,
        leftPad = pad,
        -- rightPad
        horizontalScrollDisabled = true,
        bgColor = {228/255, 228/255, 228/255}
    })
    
    scene.tiles = tiles
    return tiles_group
end

local function createListViewMember( rest_data )
    local member = display.newGroup()
    member.anchorChildren = true
    member.pos_id = rest_data.pos_id

    local bg = display.newRoundedRect( member, 0, 0, _Xa-20, 55, 5 )
    bg:setFillColor( 1 )

    local left = -bg.width*0.5
    local right = bg.width*0.5
    local top = -bg.height*0.5
    local bottom = bg.height*0.5
    local side_pad = 5
    local pad = 5
    local logo_width = bg.width * 0.25
    local logo_height = bg.height * 0.8

    local logo = display.newImageRect( member,
        rest_data.logo_file, rest_data.logo_dir,
        rest_data.logo_width, rest_data.logo_height
    )
    local width_factor = logo_width / logo.width
    local height_factor = logo_height / logo.height
    if width_factor < 1 and width_factor < height_factor then 
        logo:scale( width_factor, width_factor )
    elseif height_factor < 1 and width_factor > height_factor then  
        logo:scale( height_factor, height_factor )
    end
    logo.x = left + side_pad + logo_width*0.5

    local name = display.newText({
        parent = member,
        text = rest_data.title or "No data",
        font = "HelveticaNeueCyr-Medium",
        fontSize = 14,
        width = bg.width*0.6,
        height = 16,
        align = "left"
    })
    name:setFillColor( 0.1 )
    name.anchorX = 0
    name.x = left + side_pad + logo_width + 5
    name.anchorY = 0
    name.y = top + 6

    local addr = display.newText({
        parent = member,
        text = rest_data.addr or "No data",
        font = "HelveticaNeueCyr-Light",
        fontSize = 11,
        width = bg.width*0.6,
        height = 13,
        align = "left"
    })
    addr:setFillColor( 0, 0.8 )
    addr.anchorX = 0
    addr.x = left + side_pad + logo_width + 5
    addr.anchorY = 0
    addr.y = name.y + name.height 

    local time = display.newText({
        parent = member,
        text = rest_data.work_time or "No data",
        font = "HelveticaNeueCyr-Thin",
        fontSize = 10,
        width = bg.width*0.6,
        height = 12,
        align = "left"
    })
    time:setFillColor( 0 )
    time.anchorX = 0
    time.x = left + side_pad + logo_width + 5
    time.anchorY = 1
    time.y = bottom - 6

    if userProfile.balance[rest_data.com_id]~=nil and userProfile.bonus_types and userProfile.bonus_types[rest_data.com_id]==0 then
        local bonuses_label = display.newText({
            parent = member,
            text = "+ "..userProfile.balance[rest_data.com_id],
            font = "HelveticaNeueCyr-Medium",
            fontSize = 14,
            width = 50,
            height = 15,
            align = "right"
        })
        bonuses_label:setFillColor( 244/255,207/255,1/255  )
        bonuses_label.anchorX = 1
        bonuses_label.x = 145
        bonuses_label.anchorY = 0
        bonuses_label.y = addr.y-2
    end

    local ratingGroup = common_lib.createRating(rest_data.rating)
    ratingGroup.x=90
    ratingGroup.y=name.y+5
    member:insert(ratingGroup)

    member:addEventListener( "tap", function( event )
	event.target.alpha = 0.5
	timer.performWithDelay( 100, function()
		event.target.alpha = 1
		userProfile.SelectedRestaurantData = rest_data
		userProfile.activeOrderID = 0
		_G.defineUserCity( rest_data.city,rest_data.city_id )
		ldb:setSettValue( "last_rest", rest_data.pos_id )		
		CreateSideBar()
		mySidebar:press("about")
	end )
    end)

	function member:updateLogo()
	logo:removeSelf()
	logo = display.newImageRect( member,
		rest_data.logo_file, rest_data.logo_dir,
		rest_data.logo_width, rest_data.logo_height
	  )
	  local width_factor = logo_width / logo.width
	  local height_factor = logo_height / logo.height
	  if width_factor < 1 and width_factor < height_factor then 
		logo:scale( width_factor, width_factor )
	  elseif height_factor < 1 and width_factor > height_factor then  
		logo:scale( height_factor, height_factor )
	  end
	  logo.x = left + side_pad + logo_width*0.5
	end

    return member
end

---------------------------------------------------------------------------------------

-- function scene:keyback( event )
-- print( "........................." )
--     mySidebar:press( "main" )
--     Keyback:setFocus( nil )
-- end

function scene:setViewMode( mode, rests )
    if self.content then self.content:removeSelf() end
    self.content = display.newGroup()
    self.content.anchorChildren = true
   -- print("$$$$$$$$$$$$$$$")
   -- printResponse(rests)
    if mode == "default111" then
        createHeader( {} )

        self.restboard = createRestBoard()
        self.content:insert( self.restboard )

        local rt_height = _Ya - self.restboard.height + display.topStatusBarContentHeight
        self.reststiles = createListRestsTiles( rt_height, rests )
        self.content:insert( self.reststiles )
        self.reststiles.x = _mX
        self.reststiles.anchorY = 0
        self.reststiles.y = self.restboard.y + self.restboard.height

        self.view:insert( self.content )
        self.content.x = _mX
        self.content.anchorY = 0
        self.content.y = display.screenOriginY        
    
    elseif mode == "tiles" or mode == "default" then
        createHeader()

        if self.restboard then 
            self.restboard:removeSelf()
            self.restboard = nil
        end
        if self.reststiles then
            self.reststiles:removeSelf()
            self.reststiles = nil
        end

        local rt_height = _Ya + display.topStatusBarContentHeight-50
        self.reststiles = createListRestsTiles( rt_height, rests )
        self.content:insert( self.reststiles )
        
        self.view:insert( self.content )
        self.content.x = _mX
        self.content.anchorY = 0
        self.content.y = general_top

    elseif mode == "list" then
        createHeader()

        if self.restboard then 
            self.restboard:removeSelf()
            self.restboard = nil
        end
        if self.reststiles then
            self.reststiles:removeSelf()
            self.reststiles = nil
        end

        local dist = 5
        local ypos = 0
        local members = display.newGroup()
        local member, sepr, index
        for i=1, #rests do
            index = rests[i]
            -- print("createListViewMember",index,CityRestorans[index])
            if CityRestorans[index]~=nil then
                member = createListViewMember( CityRestorans[index] )
                members:insert( member )
                member.anchorY = 0
                member.y = ypos

                sepr = display.newRect( members, 0, 0, member.width, 1 )
                sepr:setFillColor( 0, 0.1 )
                sepr.y = ypos + member.height + dist

                ypos = ypos + member.height + sepr.height + dist*2
            end
        end
        members.anchorChildren = true
        members.x = _mX - display.screenOriginX   
        members.anchorY = 0
	  members.y = 10
	  self.content.members = members

        local free_space = _Ya - headerHeight
        local scrollView = widget.newScrollView
        {
            --top = 0,
            backgroundColor = { 0.3, 0.8, 0.8 },
            --left = 0,
            width = _Xa,
            height = free_space,
            bottomPadding = 0,
            horizontalScrollDisabled = true,
            hideBackground = true,
            listener = function( event )
                display.getCurrentStage():setFocus( nil )
            end
        }
        scrollView:insert( members )   
        self.content:insert( scrollView, true )

        self.view:insert( self.content )
        self.content.x = _mX
        self.content.anchorY = 0
        self.content.y = general_top
    
    elseif mode == "map" then
        createHeader()

        if self.restboard then 
            self.restboard:removeSelf()
            self.restboard = nil
        end
        if self.reststiles then
            self.reststiles:removeSelf()
            self.reststiles = nil
        end

        local rt_height = _Ya + display.topStatusBarContentHeight-50
        self.reststiles = createListRestsTiles( rt_height, rests )
        self.content:insert( self.reststiles )
        
        self.view:insert( self.content )
        self.content.x = _mX
        self.content.anchorY = 0
	  self.content.y = general_top 
	  
	  if _G.overlayIsActive==true then composer.hideOverlay() end
	  _G.overlayIsActive=true
        composer.showOverlay( "actions.map_overlay", {
            effect = "fade",
            time = 400,
            params = {}         
	  })
        myHeader.buttons[3]:on()
    end

    -- метод для оновления логотипов
    function self.content:updateLogo( pos_id )
	local members = self.members
	if mode == "tiles" or mode == "default" then members = scene.tiles end	
	if not members then return end
	local num = members.numChildren or #members
	    for i=1, num do
		    if members[i].pos_id == pos_id then
			members[i]:updateLogo()
		    end
	    end
    end

end

function scene:keyback( event )
	print( ".......LIST" )    
    local function onComplete( event2 )
        if event2.action == "clicked" then
            if event2.index == 2 then
                native.requestExit()
            end
        end
    end 
    native.showAlert( 
        "Exit", translations["AppPausedText"][_G.Lang], 
        { "Cancel", "OK" }, 
        onComplete
    )
end

function scene:create( event )
    print("scene:create rests_list_scene")
    local sceneGroup = self.view

    local bg = display.newRect( sceneGroup, 0, 0, _Xa, _Ya )
    bg:setFillColor( 228/255, 228/255, 228/255 )
    bg.x = _mX
    bg.y = _mY + display.topStatusBarContentHeight

    local mode = composer.getVariable( "filter_mode" )
    if not mode or mode == "map" then
        mode = "default"
        composer.setVariable( "filter_mode", mode )
    end
    rdb:getGuestBalance( ldb:getSettValue( "guest_id" ),function() end )
    local rests = composer.getVariable( "filter_output" )
    if not rests then
        rests = {}
        for i=1, #CityRestorans do 
            rests[i] = i
        end
        composer.setVariable( "filter_output", rests )
    end
    self:setViewMode( mode, rests )

    if _G.hasLocation and  _G.myLocation.latitude == 0 and native.canShowPopup( "requestAppPermission" ) then
        native.showPopup( "requestAppPermission",
            { appPermission={"android.permission.ACCESS_FINE_LOCATION","android.permission.ACCESS_COARSE_LOCATION"},
                    listener = function (e)
                        if e.grantedAppPermissions and #e.grantedAppPermissions> 0 then
                            local rests = {}
                            for i=1, #CityRestorans do 
                                print("createHeader,createCityBox",i)
                                rests[i] = i
                            end
                            composer.setVariable( "filter_output", rests )
                            createHeader( bg_params )
                        end 
                    end,
            } )
    end
     
end

function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase
    
    if phase == "will" then
        Keyback:setFocus( self )
    elseif phase == "did"then

	function self.onUpdatedLogo( event )
		self.content:updateLogo( event.pos_id )
	end
	_G.DataEventDispatcher:addEventListener( "updatedLogo", self.onUpdatedLogo )
	
	composer.removeHidden()
    end
end


function scene:hide( event )
    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        Keyback:setFocus( nil )
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
	  -- Called immediately after scene goes off screen.
	_G.DataEventDispatcher:removeEventListener( "updatedLogo", self.onUpdatedLogo )
	  
    end
end


function scene:destroy( event )
    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end



-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene