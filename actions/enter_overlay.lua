local EnterModule = require "ui.EnterModule"
local rdb = require "lib.db_remote"
local uilib = require "ui.uilib"
local SMSMasterClass = require "lib.sms"

local scene = composer.newScene()
local cntrbox

local function createMessageBox( mess )
	local messbox = display.newGroup()
	local text = display.newText({
		parent = messbox,
		text = mess,     
		x = 0,
		y = 0,
		width = _Xa*0.7,
		font = "HelveticaNeueCyr-Medium",   
		fontSize = 14
	})
	text:setFillColor( 1, 1, 1 )

	local box = display.newRoundedRect( messbox, 
		0, 0, 
		text.width + 30, text.height + 30, 
		15 
	)
	box:setFillColor( 0,0,0, 0.8 )
	box:toBack()

	messbox.timer_handle = timer.performWithDelay( 1600, function()
		messbox:removeSelf()
	end )

	return messbox
end

function scene:keyback( event )
	if composer.getSceneName( "current" ) ~= "actions.rest_about_scene" then
		mySidebar:press( "list_main" )
	else
		composer.hideOverlay()
	end
    Keyback:setFocus( nil )
end

-----------------------------------------------------------------------

function scene:create( event )
	local sceneGroup = self.view
	local parent = event.params.parent
	local SMSMaster, vpbox, code
	Keyback:setFocus( self )

	local function listener( em_event )
		if em_event.phase == "requestRegistration" then
			if not SMSMaster.lowBalance then
				vpbox:show(  string.gsub( em_event.data.tel, "%+", "" ) )
			else
				self.em:request( "registration" )
			end
		elseif em_event.phase == "logged" or em_event.phase == "regged" then
			composer.gotoScene( parent, { effect = "fromRight", time = 200 } )
			local cases = {
				["actions.my_cabinet_scene"] = "cabinet",
				["actions.my_orders_scene"] = "orders"
			}
			CreateSideBar( cases[parent] )
			rdb:getGuestBalance( em_event.data.guest_id, function( state, balance )
				if state == "success" then
					userProfile.balance = balance
				end
			end )				
		elseif em_event.phase == "back" then
			composer.hideOverlay( "fade", 200 )
		end
		if em_event.name == "countryRequest" then 
			cntrbox:select( _G.current_country )
			cntrbox.isVisible = true 
		end
	end	
	-- countries =  ldb:getRowsData("countries","lang_code = '".._G.Lang.."'")

	local params = {
		--width = 0,
		--height = 0,
		--y = headerHeight,
		tabBarHeight = common_tab_height,
		tabBarFont = "HelveticaNeueCyr-Medium",
		tabBarFontSize = 15,
		
		fieldsWidth = math.ceil( _Xa*0.6 ),
		fieldsHeight = 20, --0.15,
		fieldsFont = { "HelveticaNeueCyr-Thin", "HelveticaNeueCyr-Light" },
		fieldsFontSize = 16, --0.12,
		fieldsTop = 30,
		padLeft = 5,
		padRight = 5,

		rowHeight = 20,		
		
		listener = listener
	}
	
	self.em = EnterModule:new( params )
	self.em.anchorChildren = true
	self.em.x = _mX
	self.em.anchorY = 0
	self.em.y = general_top-- headerHeight
	sceneGroup:insert( self.em )

	if system.getInfo( "platformName" ) == "Win" or system.getInfo( "platformName" ) == "Mac OS X"  then
		local fillin = widget.newButton({
		    label = "Fill in",
		    labelColor = { default={ 0, 0, 0 }, over={ 0, 0, 0, 0.5 } },
		    onPress = function( event )
				self.em.login.data.email = "necky@bigmir.net" --"abc@abc.com"
				self.em.login.data.password = "12345"
				-- self.em.login.data.email = "test@test.com" --"abc@abc.com"
				-- self.em.login.data.password = "12345"
				-- self.em.login.data.email = "1@test.com"  
				-- self.em.login.data.password = "Qwerty" 
				self.em.login.datavalid.is = true
				self.em.login:sendNetRequest()
		    end,
		    font = native.systemFont,
		    fontSize = 20
		})
		fillin.x = display.contentCenterX
		fillin.y = _Ya - 50
		sceneGroup:insert( fillin )
		--fillin.isVisible = false		
	end

	cntrbox = ui_elm:createCountryBox(scene.view, function( event )
		if event then self.em:setCountry( event.country ) end
	end )
	cntrbox.isVisible = false

	for i=1, #_G.countries do
		if _G.countries[i].cntr_name == _G.current_country then
			self.em:setCountry( _G.countries[i] )
			break
		end
	end
	--- подтверждение номера телефона

	SMSMaster = SMSMasterClass({ 
		country = _G.userCountry,
		listener = function( event )
			table.print( event )
			if event.isError then
				if event.name == "sending" then  -- провайдер не смог отправить сообщение
					vpbox:update( translations.sms_error_provider[userProfile.lang] )
				else										-- сетевая ошибка
					vpbox:update( translations.sms_error_network[userProfile.lang] )
				end
			end
		end		 
	})

	vpbox = uilib.VerificationPhoneBoxClass({ 
		delay = 15,			-- перерыв между отправками сообщений
		fieldSize = 4,		-- длинна кода в сообщении
		listener = function( event )
			if event.name == "sendSms" then
				code = math.random( 1000, 9999 )
				if system.getInfo( "environment" ) == "simulator" then print( "SMS CODE ", code  ) end
				SMSMaster:send({ phone = event.phone, message = code })
			elseif event.name == "inputCode" then
				if event.code == code then 
					vpbox:hide() 
					self.em:request( "registration" )
				else 
					local messbox = createMessageBox( translations.sms_wrong_code[userProfile.lang] )
					messbox.x = _mX; messbox.y = _mY - 100
					sceneGroup:insert( messbox )
				end
			end
		end
	})
	vpbox.x = _cX; vpbox.y = _cY
	sceneGroup:insert( vpbox )
end

function scene:hide( event )
	if event.phase == "did" then
		self.em:remove()
	end
end

scene:addEventListener( "create", scene )
scene:addEventListener( "hide", scene )

return scene
