local scene = composer.newScene()
local libtile = require "ui.RestsTiles"
-- 
local rest_id=""
local blockTap=false
-- -----------------------------------------------------------------------------------------------------------------
local function createNews( top )
    print("createNews")

    local hview = widget.newScrollView
    {
                width = _Xa-10,
                height = top-5,        
                scrollHeight = top-5,  
                topPadding = 10,    
                bottomPadding = 10,
                horizontalScrollDisabled = true,
                hideBackground = true,
                listener = function( event )
                    display.getCurrentStage():setFocus( nil )
                end
    }   
    hview.x = _mX
    hview.anchorY = 0
    hview.anchorX = 0.5
    hview.y = top
    local function onShadowTap( )
        blockTap=false
    end  

    local function onNewsTap( event )
        if blockTap==false then
            local newsFullGroup=display.newGroup( )
                    
            local rect = display.newRect( 0,display.screenOriginY, display.contentWidth-2*display.screenOriginX, display.contentHeight-2*display.screenOriginY )
            rect.anchorX=0;rect.anchorY=0
            rect:setFillColor( 0, 0, 0,0.7 ) 
            newsFullGroup:insert( rect)       
            local bg = display.newRoundedRect( newsFullGroup, _mX, 80, _Xa-15, _mY,4 )
                    local paint = { 0,0, 0, 0.3 }
                    bg.stroke = paint
                    bg.strokeWidth = 1
                    bg.anchorY=0
                    bg.anchorX=0.5
                    bg:setFillColor( 1 )
                     newsFullGroup:insert( bg )  
            local news_txt = display.newText({ 
                        text = event.target.data.pos_name, 
                        font = "HelveticaNeueCyr-Light", 
                        fontSize = 14
                    })
                    --setFillColor( 1,0,0, 0.8 )
            news_txt:setFillColor( 1,0,0, 0.8  )
            news_txt.anchorX=0
            news_txt.anchorY=0.5
            news_txt.x=20
            news_txt.y=bg.y+15
            newsFullGroup:insert( news_txt ) 

                    local dt = Date( event.target.data.posn_datetime )
                    dt = dt and dt:fmt( "%d.%m.%Y %H:%M" ) or nil
                    local news_txt = display.newText({ 
                        text = dt, 
                        font = "HelveticaNeueCyr-Light", 
                        fontSize = 12
                    })
                    news_txt:setFillColor( 0,0,0, 0.8 )
                    news_txt.anchorX=1
                    news_txt.anchorY=0.5
                    news_txt.x=_Xa- 25
                    news_txt.y=bg.y+15
            newsFullGroup:insert( news_txt )
            local news_txt = display.newText({ 
                        text = event.target.data.posn_text, 
                        font = "HelveticaNeueCyr-Light",
                        width = _Xa-25,
                        
                        fontSize = 14
                    })
                    --setFillColor( 1,0,0, 0.8 )
            news_txt:setFillColor( 0 )
            news_txt.anchorX=0
            news_txt.anchorY=0.5
            news_txt.x=20
            news_txt.y=bg.y+45
            newsFullGroup:insert( news_txt ) 
            local leftbut = widget.newButton({
                labelColor = { default={1}, over={0} },
                font = "HelveticaNeueCyr-Light",
                fontSize = 16,
                label = translations["open_outlet"][_G.Lang],
                onRelease = function( event )
                    newsFullGroup:removeSelf( )
                    newsFullGroup = nil
                    for i=1, #CityRestorans do 
                        if CityRestorans[i].pos_id==event.target.data.pos_id then
                            userProfile.SelectedRestaurantData = CityRestorans[i]
                            userProfile.activeOrderID = 0
                            _G.defineUserCity( CityRestorans[i].city,CityRestorans[i].city_id )
                            ldb:setSettValue( "last_rest", CityRestorans[i].pos_id )
                            CreateSideBar()
                            mySidebar:press("about")            
                        end
                    end                    

                end,
                --properties for a rounded rectangle button...
                shape = "roundedRect",
                width = 120,
                height = 30,
                cornerRadius = 5,
                fillColor = { default={ 56/255, 214/255, 128/255 }, over={ 211/255,211/255,211/255 } },
                strokeColor = { default={ 56/255, 214/255, 128/255 }, over={ 0,0,0, 0.13 } },
                strokeWidth = 2
            })
            leftbut.data=event.target.data
            leftbut.anchorX = 0.5
            leftbut.x = _mX   
            leftbut.y = bg.y+bg.height-30
            newsFullGroup:insert( leftbut)
            blockTap=true
            rect:addEventListener( "tap", function() return true end )
            rect:addEventListener( "touch", function(event)
            if event.phase == "ended" then
                timer.performWithDelay(500,onShadowTap,1)            
                newsFullGroup:removeSelf( )
                newsFullGroup = nil
            end
            return true 
        end)
        end
    end    
    local no_news=true
    if rest_id~="" then
        local pos_news = ldb:getNews(rest_id,1)

        if pos_news then
                y_pos=0
                for i=1, #pos_news do
                    no_news=false
                    local bg = display.newRoundedRect( hview, 0, y_pos, _Xa-15, 50,4 )
                    local paint = { 0,0, 0, 0.3 }
                    bg.data=pos_news[i]
                    bg:addEventListener( "tap", onNewsTap )
                    bg.stroke = paint
                    bg.strokeWidth = 1
                    bg.anchorY=0
                    bg.anchorX=0
                    bg:setFillColor( 1 )
                     hview:insert( bg )  
                    local news_txt = display.newText({ 
                        text = pos_news[i].pos_name, 
                        font = "HelveticaNeueCyr-Light", 
                        fontSize = 14
                    })
                    --setFillColor( 1,0,0, 0.8 )
                    news_txt:setFillColor( 1,0,0, 0.8  )
                    news_txt.anchorX=0
                    news_txt.anchorY=0.5
                    news_txt.x=10
                    news_txt.y=13+y_pos
                    hview:insert( news_txt )                           
                    local dt = Date( pos_news[i].posn_datetime )
                    dt = dt and dt:fmt( "%d.%m.%Y %H:%M" ) or nil
                    local news_txt = display.newText({ 
                        text = dt, 
                        font = "HelveticaNeueCyr-Light", 
                        fontSize = 12
                    })
                    news_txt:setFillColor( 0,0,0, 0.8 )
                    news_txt.anchorX=1
                    news_txt.anchorY=0.5
                    news_txt.x=_Xa- 20
                    news_txt.y=13+y_pos
                    hview:insert( news_txt )                           

                    local news_txt = display.newText({ 
                        text = pos_news[i].posn_title, 
                        font = "HelveticaNeueCyr-Light", 
                        fontSize = 13,
                        width=_Xa-18,
                        height=20
                    })
                    news_txt:setFillColor( 0 )
                    news_txt.anchorX=0
                    news_txt.anchorY=0.5
                    news_txt.x=10
                    news_txt.y=38+y_pos
                    hview:insert( news_txt )                           
                    y_pos=y_pos+60
                end
        end    
    end
            if no_news==true then
            print("NO NEWS")
                    local news_txt = display.newText({ 
                        text = translations["no_news"][_G.Lang], 
                        font = "HelveticaNeueCyr-Light", 
                        fontSize = 14
                    })
                    --setFillColor( 1,0,0, 0.8 )
                    news_txt:setFillColor( 0.5  )
                    news_txt.anchorX=0.5
                    news_txt.anchorY=0.5
                    news_txt.x=_mX
                    news_txt.y=_mY/2-50 
                    hview:insert( news_txt )                   
            end    
    return hview
end

local function createMainRestsTiles( height )
    print("createMainRestsTiles")
    local group = display.newGroup()
    group.anchorChildren = true
    
    local pad = 7
    local numrows = 2 
    local tile_width = (_Xa - (pad*3)) * 0.5
    local tile_height = (height - pad*3) * 0.5
    local tiles = {}

    local right_arrow = display.newImageRect( group, 
        "icons/scrollArrow.png", 60*0.5, 94*0.5
    )
    right_arrow.alpha = 0.6
    right_arrow.rotation = 180
    local left_arrow = display.newImageRect( group, 
        "icons/scrollArrow.png", 60*0.5, 94*0.5 
    )
    left_arrow.alpha = 0.6
    left_arrow.isVisible = false

    for i=1, #CityRestorans do 
  --      print(CityRestorans[i].logo_width,i)
        tiles[i] = libtile:createRestTile( tile_width, tile_height, CityRestorans[i] ) 
        if rest_id~="" then
            rest_id=rest_id..","
        end
        rest_id=rest_id..CityRestorans[i].pos_id
    end
 --   print(tiles[1])
    local function onTilesScroll( event )
        if event.limitReached then
            if event.direction == "right" then
                left_arrow.isVisible = false
            elseif event.direction == "left" then
                right_arrow.isVisible = false
            end
        elseif event.direction then 
            left_arrow.isVisible = true
            right_arrow.isVisible = true
        end
    end
    
    local tiles_group = widget.newTilesGroup({
        tiles = tiles,
        width = _Xa,
        height = height,
        rowTilesNumber = numrows,
        --columnTilesNumber = 2,
        verticalPad = pad,
        horizontalPad = pad,
        topPad = pad,
        bottomPad = pad,
        leftPad = pad,
        rightPad = pad,
        --horizontalScrollDisabled = true,
        verticalScrollDisabled = true,
        bgColor = {228/255, 228/255, 228/255},
        scrollListener = onTilesScroll 
    })
    group:insert( tiles_group )
    
    right_arrow:toFront()
    right_arrow.anchorX = 0
    right_arrow.x = _Xa*0.5
    right_arrow:addEventListener( "touch", function( event )
        if event.phase == "began" and tiles_group then
            tiles_group:scrollToNext( "left", { 
                time = 1000,
                onComplete = function( event )
                    if event.limitReached == "right" then
                       right_arrow.isVisible = false
                    else left_arrow.isVisible = true 
                    end
                end 
            }) 
        end
        return true
    end )
    left_arrow:toFront()
    left_arrow.anchorX = 0
    left_arrow.x = -_Xa*0.5
    left_arrow:addEventListener( "touch", function( event )
        if event.phase == "began" and tiles_group then
            tiles_group:scrollToNext( "right", { 
                time = 1000,
                onComplete = function( event )
                    if event.limitReached == "left" then
                       left_arrow.isVisible = false
                    else right_arrow.isVisible = true 
                    end
                end 
            }) 
        end
        return true
    end )
    
    return group
end

-- -------------------------------------------------------------------------------

function scene:keyback( event )    
    local function onComplete( event2 )
        if event2.action == "clicked" then
            if event2.index == 2 then
                native.requestExit()
            end
        end
    end 
    native.showAlert( 
        "Exit", translations["AppPausedText"][_G.Lang], 
        { "Cancel", "OK" }, 
        onComplete
    )
end

function scene:create( event )
    local sceneGroup = self.view
    CreateBaseHeader()
    
    local bg = display.newRect( sceneGroup, 0, 0, _Xa, _Ya )
    bg:setFillColor( 228/255, 228/255, 228/255 )
    bg.x = _mX
    bg.y = _mY + display.topStatusBarContentHeight
    rest_id=""
    local rt_height = _Ya - headerHeight
    local rests_tiles = createMainRestsTiles( rt_height*0.5 )
    sceneGroup:insert( rests_tiles )
    local news = createNews( general_top+rests_tiles.height )
    sceneGroup:insert( news )
    rests_tiles.x = _mX
    rests_tiles.anchorY = 0
    rests_tiles.y = general_top
end


-- "scene:show()"
function scene:show( event )
    local sceneGroup = self.view
    local phase = event.phase
    Keyback:setFocus( self )

    if ( phase == "will" ) then
        -- Called when the scene is still off screen (but is about to come on screen).

    elseif ( phase == "did" ) then
        -- Called when the scene is now on screen.
        -- Insert code here to make the scene come alive.
        -- Example: start timers, begin animation, play audio, etc.
    end
end


-- "scene:hide()"
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        Keyback:setFocus( nil )
        -- Called when the scene is on screen (but is about to go off screen).
        -- Insert code here to "pause" the scene.
        -- Example: stop timers, stop animation, stop audio, etc.
    elseif ( phase == "did" ) then
        -- Called immediately after scene goes off screen.
    end
end


-- "scene:destroy()"
function scene:destroy( event )

    local sceneGroup = self.view

    -- Called prior to the removal of scene's view ("sceneGroup").
    -- Insert code here to clean up the scene.
    -- Example: remove display objects, save state, etc.
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene