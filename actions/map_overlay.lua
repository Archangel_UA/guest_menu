local composer = require( "composer" )
-- 
local scene = composer.newScene()

-- -----------------------------------------------------------------------------------------------------------------

local function createRestBoard( rest_data, onTouchHide )
	local rest_of_day_id = tonumber( ldb:getSettValue( "rest_of_the_day" ) )
	local is_rest_of_day = false

	if not rest_data then
		for i=1, #CityRestorans do
			if CityRestorans[i].com_id == rest_of_day_id then
				rest_data = CityRestorans[i]
				is_rest_of_day = true
			end
		end
	elseif rest_data.com_id == rest_of_day_id then
		is_rest_of_day = true
	end

	local board = display.newGroup()
	board.anchorChildren = true
	board.x = _mX
	board.anchorY = 1
	board.y = display.screenOriginY

	local bg_file = rest_data.main_photo.name
	local base_dir = rest_data.main_photo.basedir
	local bg_height = 240
	if bg_file == "assets/noPhoto.jpg" then
		base_dir = system.ResourceDirectory
	end
 --   print("!!!!!!!!!!!!!!!!!!!!!!!")
   -- print(bg_file)
	local bg = display.newImageRect( board,
		bg_file, base_dir,
		bg_height*rest_data.main_photo.aspect, bg_height
	)

	local cover = display.newRect( board, 0, 0, _Xa, bg_height )
	cover:setFillColor( 0, 0.45 )

	local ref_left = -_Xa*0.5
	local ref_right = _Xa*0.5
	local ref_top = bg.y - bg.height*0.5
	local ref_centerY = bg.y

	local red_inscript 
	if is_rest_of_day then
		red_inscript = display.newGroup()
		red_inscript.anchorChildren = true
		red_inscript.anchorX = 0
		red_inscript.x = ref_left
		red_inscript.y = ref_centerY - 15
		board:insert( red_inscript )
		red_inscript.bg = display.newRect( red_inscript, 0, 0, 115, 28 )
		red_inscript.bg:setFillColor( 247/255, 64/255, 66/255 )
		red_inscript.label = display.newText({
			parent = red_inscript,
			text = translations["rest_of_the_day"][_G.Lang],
			x = 0, y = 0,
			font = "HelveticaNeueCyr-Light",
			fontSize = 12
		})
		red_inscript.label:setFillColor( 1,1,1,1 )
	end

	local time_icon = display.newImageRect( board, 
		"icons/clock.png", 
		20, 20 
	)
	time_icon.anchorX = 0
	time_icon.x = 7 + (red_inscript and red_inscript.x + red_inscript.width or ref_left+10)
	time_icon.y = ref_centerY - 15

	local time_label = display.newText({
		parent = board,
		text = rest_data.work_time,
		font = "HelveticaNeueCyr-Light",
		fontSize = 14
	})
	time_label.anchorX = 0
	time_label.x = time_icon.x + time_icon.width + 5
	time_label.y = ref_centerY - 15

	local rest_address = display.newText({
		parent = board,
		text = rest_data.addr,
		font = "HelveticaNeueCyr-Light",
		fontSize = 14,
		width = _Xa - 30
	})
	rest_address.anchorX = 0
	rest_address.x = ref_left + 17
	rest_address.anchorY = 0
	rest_address.y = time_icon.y + time_icon.height*0.5 + 10

	local sepr_line = display.newLine( board, 
		ref_left + 17, rest_address.y + rest_address.height + 13, 
		ref_right - 17, rest_address.y + rest_address.height + 13
	)
	sepr_line:setStrokeColor( 1, 0.3 )
	sepr_line.strokeWidth = 1

	local rest_title = display.newText({
		parent = board,
		text = rest_data.title or "No data",
		font = "HelveticaNeueCyr-Medium",
		fontSize = 23,
		width = _Xa - 30
	})
	rest_title.anchorX = 0
	rest_title.x = ref_left + 17
	local free_space = bg.contentBounds.yMax - sepr_line.contentBounds.yMax
	rest_title.y = sepr_line.y + free_space*0.5 - 5

	local touch_pad = display.newRect( board, 0, 0, _Xa, bg.height - myHeader.height )
	touch_pad.isVisible = false
	touch_pad.isHitTestable = true
	touch_pad.anchorY = 0
	touch_pad.y = ref_top + myHeader.height
	touch_pad:addEventListener( "touch", function( event )
		if event.phase == "began" then
			event.target.isFocus = true
			display.getCurrentStage():setFocus( event.target )
			cover:setFillColor( 1, 0.2 )
		elseif event.phase == "moved" and event.target.isFocus then
			--if evevnt.y 
		elseif event.phase == "ended" or event.phase == "cancelled" then
			if event.target.isFocus then
				cover:setFillColor( 0, 0.45 )
				event.target.isFocus = false
				display.getCurrentStage():setFocus( nil )
				board:hide( 700 )
				onTouchHide( board )
				if event.yStart - event.y < 5 then
					userProfile.SelectedRestaurantData = rest_data
					userProfile.activeOrderID = 0
					CreateSideBar()
					mySidebar:press("about")
				end
			end
		end
		return true
	end )

	function board:show()
		self.is = true
		if self.tranc then transition.cancel( self.tranc ) end
		self.tranc = transition.to( self, {
			time=600, y=self.height, delta=true, transition = easing.outExpo --, onComplete=listener1 
		})
	end

	function board:hide( time )
		if not self.is then return end
		self.is = false
		if self.tranc then transition.cancel( self.tranc ) end
		self.tranc = transition.to( self, {
			time = time, y = -self.height, delta = true, transition = easing.outExpo, 
			onComplete = function()
				self:removeSelf()
				self = nil
			end 
		})
	end

	return board
end


local function addMapMarkers( data )
	if not scene.map then return end
	if scene.removed then scene.map:removeSelf(); scene.map = nil; return end

	local function onTouchHide( board )
		scene.board = nil
		local factor = (_Ya - headerHeight) / scene.map.curr_height 
		scene.map:scale( 1, factor )
	end

	for i=1, #data do
		print("Add marker ",data[i].geo_lat,data[i].geo_long)
		  if data[i].geo_lat~=nil and data[i].geo_lat~=0 and data[i].geo_long~=nil and data[i].geo_long~=0 then
		scene.map:addMarker(  data[i].geo_lat, data[i].geo_long, { 
			title =  data[i].title,
			subtitle =  data[i].addr,
			imageFile = { filename = data[i].logo_file_name, baseDir = system.TemporaryDirectory },
			listener = function( event )
				local rdata = data[i]
				local resize_map = true

				if scene.board then
				   scene.board:hide( 2000 )
				   scene.board = nil
				   resize_map = false
				end
				
				scene.board = createRestBoard( rdata, onTouchHide )
				scene.view:insert( scene.board )
				scene.board:show()                
				
				if resize_map then
					local free_space = display.actualContentHeight - scene.board.height
					local factor = free_space / scene.map.height
					scene.map.curr_height = scene.map.height * factor
					scene.map:scale( 1, factor )
				end

				scene.map:setCenter( rdata.geo_lat, rdata.geo_long, true )
			end
		})
		end
	end
	--current user location
	local attempts=0
	  local function locationHandler( event )
		if not scene.map then return end

		  local currentLocation = scene.map:getUserLocation()

		  if ( currentLocation.errorCode or ( currentLocation.latitude == 0 and currentLocation.longitude == 0 ) ) then
--	        locationText.text = currentLocation.errorMessage

			  attempts = attempts + 1

			  if ( attempts > 10 ) then
				  print( "No GPS Signal Can't sync with GPS.",currentLocation.errorCode )
			  else
				  timer.performWithDelay( 1000, locationHandler )
			  end
		  else
--	        locationText.text = "Current location: " .. currentLocation.latitude .. "," .. currentLocation.longitude
			  scene.map:setCenter( currentLocation.latitude, currentLocation.longitude )
			  scene.map:addMarker( currentLocation.latitude, currentLocation.longitude, { 
			title =  translations["your_location"][_G.Lang],
			imageFile = { filename = "icons/location_icon.png" }} )
		  end
	  end

	  locationHandler()    
end


-- -------------------------------------------------------------------------------

function scene:keyback( event )
	if self.map then display.remove( self.map ); self.map = nil end
	composer.hideOverlay()
	Keyback:setFocus( nil )
	Keyback:setFocus( self.parent )
end

function scene:create( event )
print( "...MAP", self, self.view  )

	local sceneGroup = self.view
	local rest_data = CityRestorans
	local curr_city = userProfile.currentCity
	local current_rest_only=false
	if event.params and event.params.current_rest_only then
		print("current_rest_only",userProfile.SelectedRestaurantData.title)
		current_rest_only=true
		rest_data = {}
		rest_data[1] = userProfile.SelectedRestaurantData
	else
		local filter_output = composer.getVariable( "filter_output" )
		if filter_output then
			rest_data = {}
			local index = 0
			for i=1, #filter_output do
				index = filter_output[i]
				rest_data[i] = CityRestorans[index]
			end
		end
	end

	local bg = display.newRect( sceneGroup, _mX, 0, _Xa, _Ya - headerHeight )
	bg.y = myHeader.y + bg.height*0.5 + myHeader.height - display.topStatusBarContentHeight*0.5
	bg:setFillColor( 0.5, 0.95 )
	bg:addEventListener( "touch", function() return true end)
	print(curr_city,curr_city.latitude)
	
	if system.getInfo( "environment" ) == "device" then
		self.map = native.newMapView( 
			_mX, bg.y,
			_Xa, bg.height
		)
	end
	if self.map and (curr_city or current_rest_only) then
		self.map.x = _mX
		self.map.y = bg.y
		sceneGroup:insert( self.map )
		if current_rest_only then
			self.map:setRegion( userProfile.SelectedRestaurantData.geo_lat, userProfile.SelectedRestaurantData.geo_long, 0.1, 0.1, false )
		else
			if curr_city.latitude~=nil then
				self.map:setRegion( curr_city.latitude, curr_city.longitude, 0.1, 0.1, false )
			else
				for i=1, #rest_data do
					if rest_data[i].geo_lat~=nil and rest_data[i].geo_long~=nil then
						self.map:setRegion( rest_data[i].geo_lat, rest_data[i].geo_long, 0.1, 0.1, false )
						break
					end
				end
			end
		end
		timer.performWithDelay( 1000, function()
			if not scene.map then return end
			if scene.removed then scene.map:removeSelf(); scene.map = nil
			else addMapMarkers( rest_data )
			end
		end )

		-- local function mapLocationListener(event)
		--     print("map tapped latitude: ", event.latitude)
		--     print("map tapped longitude: ", event.longitude)
		-- end
		-- self.map:addEventListener("mapLocation", mapLocationListener)
	end

end


function scene:show( event )
	local sceneGroup = self.view
	local phase = event.phase
	
	if ( phase == "will" ) then
		Keyback:setFocus( self )
		self.parent = event.parent
		-- Called when the scene is still off screen (but is about to come on screen).
	elseif ( phase == "did" ) then

		-- Called when the scene is now on screen.
		-- Insert code here to make the scene come alive.
		-- Example: start timers, begin animation, play audio, etc.
	end
end


function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		if self.map then display.remove( self.map ); self.map = nil end
		-- Called when the scene is on screen (but is about to go off screen).
		-- Insert code here to "pause" the scene.
		-- Example: stop timers, stop animation, stop audio, etc.
	elseif ( phase == "did" ) then
		-- Called immediately after scene goes off screen.
	end
end


function scene:destroy( event )
	local sceneGroup = self.view
	if self.map then display.remove( self.map ); self.map = nil end
	-- способ устранить глюк блокировки тача после перехода из фильтра, 
	-- потом в карту, а из карты в список заведений
	local unknown = nil
	for i=1, composer.stage.numChildren do
		unknown = composer.stage[i]
		if unknown and not unknown.isVisible and unknown.isHitTestable then
			composer.stage[i]:removeSelf(); unknown = nil
		end
	end
	self.removed = true
end


-- -------------------------------------------------------------------------------

-- Listener setup
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

-- -------------------------------------------------------------------------------

return scene