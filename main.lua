
_G.appVersion='1.1.8'
_G.utf8 = require "plugin.utf8"

std = require "lib.std"
require "lib"
require "utf8data"
require "utf8"
require("mime")
composer = require( "composer" )
widget = require "widget"
Date = require "lib.date"
json = require "json"	
require "ui.date_view"
require "css"
require( "ui.ui_elements" )
Spinner = require("ui.common").Spinner
Keyback = require("ui.common").KeyBackHandler
translations = require("translations")
sidebar = require( "ui.sidebar" )
header = require( "ui.header" )
_G.current_country = nil
_G.countries ={}
local rdb = require "lib.db_remote"
local rdb_common = require "lib.db_remote_common"
local rdb_bonuses = require "lib.db_remote_bonuses"
local EnterModule = require "ui.EnterModule"
local clib = require "lib.common_lib"
ldb = require "lib.db_local"
local ovlib = require "ui.OrderView"
local uilib = require "ui.uilib"
local loadsave = require "lib.loadsave"
_G.appTitle="BonusMe"
_X = display.contentWidth
_Y = display.contentHeight - display.topStatusBarContentHeight
_Xa = display.actualContentWidth
_Ya = display.actualContentHeight - display.topStatusBarContentHeight
_mX = display.contentWidth*0.5
_mY = display.contentHeight*0.5
_BOTTOM = display.contentHeight - display.screenOriginY
_Xo = display.screenOriginX
_Yo = display.screenOriginY

mW = display.actualContentWidth
mH = display.actualContentHeight-display.topStatusBarContentHeight
cX = mW/2
cY = mH/2+display.topStatusBarContentHeight

math.roundIdp = function(num, idp) -- округлення по розряду idp
	idp = idp or 2
	local mult = 10^(idp or 0)
	return math.round(num*mult)/mult
end

-- print( "_X: ", _X, "_Y: ", _Y )
-- print( "_Xa: ", _Xa, "_Ya: ", _Ya )
-- print( "pixelWidth: ", display.pixelWidth , "pixelHeight: ", display.pixelHeight )
-- print( "screenOriginX ", display.screenOriginX, "screenOriginY ", display.screenOriginY )
-- print( "using image suffix ", display.imageSuffix )

-- 1. INIT GENERAL PARAMS
-----------------------------------------------------
composer.recycleOnSceneChange = true
display.setStatusBar( display.TranslucentStatusBar )
display.setDefault( "background",  228 / 255, 228 / 255, 228 / 255 )

common_tab_height=40
headerHeight = 40
BasicTextFontSize = 14
general_top = display.screenOriginY + headerHeight + display.topStatusBarContentHeight

_ORDERS_UPDATE  = 25 		-- частота обновления состояния ордеров, сек.

_G.Lang ="ru"
_G.host="web.smarttouchpos.eu"
_G.Currency=""
_G.Order={}
_G.Order[1]={id=1,state=0,data={}}
_G.demoModeWithoutCountriesFilter=false --false
if system.getInfo( "platform" ) == "win32" or system.getInfo( "environment" ) == "simulator" then
	_G.demoModeWithoutCountriesFilter=true
end
_G.internetConnectionIsActive=true
RestoransList = {}
CitiesList = {}
CountriesList = {["RU"]={["ru"]="Россия",["en"]="Russia"},["UA"]={["ru"]="Украина",["en"]="Ukraine"},["BY"]={["ru"]="Белоруссия",["en"]="Belarus"},["CA"]={["ru"]="Канада",["en"]="Canada"},["US"]={["ru"]="США",["en"]="USA"},["PH"]={["ru"]="Филиппины",["en"]="Philippines"}}
CityRestorans = {}
userProfile = {
	lang = _G.Lang,
	SelectedRestaurantData = nil,
	activeOrderID = 0,
	AppType= 'BONUS',
	currentCity = {},
	balance = {}, 
	guestHasPhoto=false,
	price_lang = _G.Lang,
	lat=0,
	long=0,
	player_id='',
	country_code = ""
}

_G.DataEventDispatcher = system.newEventDispatcher() -- диспатчер событий обновления данных в приложении

local stage = display.getCurrentStage()
mainGroup = display.newGroup()
local loadingGroup = display.newGroup()
mySidebar = nil
myHeader = nil
_debugStr={}

_G.scenes_ids = {		-- используются для кнопок назад в хеадаре
	["actions.main_scene"] = "main",
	["actions.actions_scene"] = "actions",
	["actions.rests_list_scene"] = "list_main",
	["actions.my_cabinet_scene"] = "cabinet",
	["actions.rest_menu_scene"] = "menu",
	["actions.my_orders_scene"] = "orders",
	["actions.my_orders_history_scene"] = "orders_history",
	["actions.rest_about_scene"] = "about",	
	["actions.settings_scene"] = "settings",	
}

function _print(s1)
	_debugStr[#_debugStr+1]=tostring(s1)
	--_debugStr=_debugStr..tostring(s1).."\n"
	print(s1)
end

_print = print


function log( ... )
	local mess = "\n---------------------------------------------------"
	mess = mess.."\n@log: "
	for i=1, #arg do mess = mess..tostring( arg[1] ) end
	mess = mess.."\n---------------------------------------------------"
	return mess
end

function debug_print(s1,s2)
--	native.showAlert( tostring(s1), tostring(s2), { "OK" } )
end
--Notification bloc ONESIGNAL---
-- This function gets called when the user opens a notification or one is received when the app is open and active.
-- Change the code below to fit your app's needs.
function DidReceiveRemoteNotification(message, additionalData, isActive)
	printResponse(additionalData)
    if (additionalData) then
        if (additionalData.bonus) then
            native.showAlert( translations["bonuses_push"][_G.Lang], message, { "OK" } )
            -- Take user to your app store
        elseif (additionalData.sevencup) then 
        	native.showAlert( translations["bonuses_push"][_G.Lang], message, { "OK" } )
--        elseif (additionalData.actionSelected) then -- Interactive notification button pressed
--            native.showAlert("Button Pressed!", "ButtonID:" .. additionalData.actionSelected, { "OK"} )
        elseif (additionalData.message==3) then 
        	native.showAlert( message, string.gsub(translations["order_done"][_G.Lang],"!p!",additionalData.ord_id), { "OK" } )
        elseif (additionalData.message==1) then 
        	native.showAlert( message, string.gsub(translations["order_confirmed"][_G.Lang],"!p!",additionalData.ord_id), { "OK" } )        	
        elseif (additionalData.message==4) then 
        	native.showAlert( message, string.gsub(translations["order_confirmed"][_G.Lang],"!p!",additionalData.ord_id), { "OK" } )        	
        end
        if (additionalData.message==1 or additionalData.message==3 or additionalData.message==4) then
        	if composer.getSceneName("current")=="actions.my_orders_scene" then
        		composer.getScene(composer.getSceneName("current")):refresh()
        	end
        end
    else
        native.showAlert("BonusMe", message, { "OK" } )
    end
end

local OneSignal = require("plugin.OneSignal")
-- Uncomment SetLogLevel to debug issues.
-- OneSignal.SetLogLevel(4, 4)
OneSignal.Init("bf1d16b3-060d-4e77-8f91-5a328e7d1f7c", "159030499453", DidReceiveRemoteNotification)

function IdsAvailable(userID, pushToken)
    print("PLAYER_ID:" .. userID)
    userProfile.player_id=tostring(userID)
    rdb_bonuses:updatePlayerID()
    if (pushToken) then -- nil if user did not accept push notifications on iOS
        print("PUSH_TOKEN:" .. pushToken)
    end
end

--IdsAvailable('12123',false)
OneSignal.IdsAvailableCallback(IdsAvailable)
--------------------------------------
function errorHandler( event )
	print("errorHandler")		
	if string.find(event.errorMessage,"setFillColor")==nil then
		rdb_common:sendEmail{listener=nil,error_flag=1,email="bugs@smarttouchpos.eu",email_text="BonusMe   \n scene:"..composer.getSceneName("current").."\n message:"..event.errorMessage.."  \n stackTrace:"..event.stackTrace.." \n server "}
	end
	return true
end 

Runtime:addEventListener( "unhandledError", errorHandler )
-- 2. DEFINE INITIAL UI METHODS
-----------------------------------------------------------------

function showTable(obj,n) -- функция рекрусивная
    if n == nil then
        n = 0
        print("****** showObj ******")
    end
    local prf = ""
    for i=1 , n  do
        prf = prf..'\t'
    end
    if (obj ~= nil) then
        if (type(obj) == 'table') then
            for k, v in pairs(obj) do
                if( type(v) == 'table') then
                    print(prf..k,obj[k])
                    showTable(obj[k],n+1)
                else
                    print(prf..k,obj[k])
                end
            end
        else
            print(prf,obj)
        end
    end
end

function createCityBox( onClose )
	onClose = onClose or function() end

	local w, h = _Xa*0.8, _Ya
	local citybox = uilib.newBox( w, 330, onClose )    
		
	citybox.name = display.newText({
		parent = citybox,
		text = translations["select_city"][_G.Lang],--translations.OrderTxt_ReservDate[self.lang][1],
		width = citybox.bg.width*0.8,
		font = "HelveticaNeueCyr-Medium", 
		fontSize = 15,
		align = "center"
	})
	citybox.name:setFillColor( 1,0,0 )
	citybox.name.x = citybox.bg.x
	citybox.name.anchorY = 0
	citybox.name.y = citybox.bg.y - citybox.bg.height*0.5 + 10
		
	citybox.separ1 = display.newLine( citybox, 
		citybox.bg.x - citybox.bg.width*0.5, citybox.name.y + citybox.name.height + 10,  
		citybox.bg.x + citybox.bg.width*0.5, citybox.name.y + citybox.name.height + 10
	)
	citybox.separ1:setStrokeColor( 0,0,0, 0.1 )
	citybox.separ1.strokeWidth = 1.5
	local hview
	local function createCityLine( name, state, width, height, onRelease )
		local line = display.newGroup()
		line.anchorChildren = true
		line.anchorY = 0
		line.state = state and "on" or "off"
		line.name = name

		local bg = display.newRect( line, 0, 0, width, height )
		bg:setFillColor( 0.95, 0 )
		bg.isHitTestable = true

		local right = -bg.width*0.5

		local options = {
			width = 25,
			height = 25,
			numFrames = 2,
			sheetContentWidth = 50,
			sheetContentHeight = 25
		}
		local rbsheet = graphics.newImageSheet( "assets/orders/smallRadio.png", options )
		local radio_on = display.newImageRect( line, rbsheet, 1, 20, 20 )
		local radio_off = display.newImageRect( line, rbsheet, 2, 20, 20 )
		radio_on.anchorX = 0
		radio_off.anchorX = 0
		radio_on.x = right + 15
		radio_off.x = right + 15
		if line.state == "off" then radio_on.isVisible = false end

		local label = display.newText({
			parent = line, 
			text = name, 
			font = "HelveticaNeueCyr-Light", 
			fontSize = 17
		})
		label:setFillColor( 0 )
		label.anchorX = 0
		label.x = radio_on.x + radio_on.width + 10

		function line:turnOn()
			radio_on.isVisible = true
			self.state = "on"                
		end
		function line:turnOff()
			radio_on.isVisible = false
			self.state = "off"                
		end

		line:addEventListener( "touch", function( event )
			display.getCurrentStage():setFocus( nil )
			if event.phase == "began" then
				event.target.isFocus = true
				display.getCurrentStage():setFocus( event.target )
			elseif  event.phase == "moved" then				
				if event.y and math.abs( event.y - event.yStart ) >= 10 then
					event.target.isFocus = false
					display.getCurrentStage():setFocus( nil )
					return false
				end
			elseif event.target.isFocus and (event.phase == "ended" or event.phase == "cancelled") then
				if event.target.state == "off" then
					event.target:turnOn()
				elseif event.target.state == "on" then
					event.target:turnOff()
				end
				local pEvent = { 
					target = event.target,
					state = event.target.state,
					newName = event.target.name
				}
				onRelease( pEvent )
				display.getCurrentStage():setFocus( nil )
			end
			return false
		end )

		return line
	end

	local newline, sepr, state
	local ypos = 0--citybox.separ1.y + 5
	hview = widget.newScrollView
			{
				width = citybox.bg.width,
				height = citybox.bg.height-60,
				bottomPadding = 0,
				--backgroundColor = { 0.8, 0.8, 0.8 },
				horizontalScrollDisabled = true,
				hideBackground = true,
				listener = function( event )
					display.getCurrentStage():setFocus( nil )
				end
			}  

	hview.x=citybox.bg.x
	hview.y=citybox.bg.y+10
	citybox:insert( hview )		 
	local array_to_sort={}
	for name, geo in pairs( CitiesList ) do
		array_to_sort[#array_to_sort+1]=name
	end	
	table.sort(array_to_sort, function(a,b) return a<b end)
	for geo,name in pairs( array_to_sort ) do
		if userProfile.currentCity and name == userProfile.currentCity.name then
			state = true
		else state = false
		end 

		newline = createCityLine( 
			name, state, 
			w, 40, 
			function( event )
				-- print("&^^^&&&&&&&&&&&&&&&&&")
				_G.defineUserCity( name )
				citybox:removeSelf()
				citybox = nil
				display.getCurrentStage():setFocus( nil )
				onClose()
				composer.removeScene(composer.getSceneName( "current" ))
				composer.gotoScene( "actions.rests_list_scene" )
				composer.setVariable( "filter_settings", false )
			end 
		)
		hview:insert( newline )
		newline.x = citybox.bg.x 
		newline.y = ypos
		ypos = ypos + newline.height

		sepr = display.newRect( hview, 0, 0, w, 1 )
		sepr:setFillColor( 0.95 )
		sepr.x = citybox.bg.x-33
		sepr.y = newline.y + newline.height   
		hview:insert( sepr )     
	end
end

function CreateSideBar( on_button )
	on_button = on_button or "main"
	local stage = display.getCurrentStage()
	local currscene = composer.getSceneName( "current" )

	if mySidebar~=nil then
		mySidebar.curtain:removeSelf()
		mySidebar.curtain = nil
		mySidebar:removeSelf()
		mySidebar=nil
	end

	local goto_options = {
		effect = "fade",
		time = 200,
		params = { }
	}

	local sidebarItems = {
		-- { 
		-- 	id = "main",
		-- 	label = translations["Sidebar_main"][_G.Lang],
		-- 	labelColorOn = {1,0.3},
		-- 	labelColorOff = {1},
		-- 	top = 80,
		-- 	left = 33,            
		-- 	switch = true,
		-- 	onTurnOn = function( event )
		-- 		composer.gotoScene( "actions.main_scene", goto_options )                
		-- 	end,
		-- 	onTurnOff = function( event )
		-- 		-- body
		-- 	end
		-- },
		{ 
			id = "list_main",
			label = translations["Sidebar_list_main"][_G.Lang],
			labelColorOn = {1,0.3},
			labelColorOff = {1},
			top = 80,
			left = 33, 
			switch = true,
			onTurnOn = function( event )
				if currscene~=nil then composer.removeScene(currscene) end
				composer.gotoScene( "actions.rests_list_scene", goto_options )                
			end,
			onTurnOff = function( event )
				-- body
			end
		},
		{ 
			id = "actions",
			label = translations["Sidebar_actions"][_G.Lang],
			labelColorOn = {1,0.3},
			labelColorOff = {1},
			switch = true,
			onTurnOn = function( event )
				print(currscene)
				if currscene~=nil then composer.removeScene(currscene) end
				composer.gotoScene( "actions.actions_scene", goto_options )                
			end,
			onTurnOff = function( event )
				-- body
			end
		},
		{ 
			id = "cabinet",
			label = translations["Sidebar_cabinet"][_G.Lang],
			labelColorOn = {1,0.3},
			labelColorOff = {1},
			switch = true,
			onTurnOn = function( event )
--				print("!!!")
				if EnterModule:checkEntered() then
					print(currscene)
					if currscene~=nil then composer.removeScene(currscene) end
					composer.gotoScene( "actions.my_cabinet_scene", goto_options )                
				else
					local options = {
						isModal = true,
						effect = "fromRight",
						time = 300,
						params = { parent = "actions.my_cabinet_scene" }
					}
					composer.showOverlay( "actions.enter_overlay", options )
					CreateBaseHeader() 
				end            
			end,
			onTurnOff = function( event )
				-- body
			end
		}
	}

	local entered = EnterModule.checkEntered()
	if entered then 
		if userProfile.SelectedRestaurantData~=nil and userProfile.SelectedRestaurantData.menu_type~=5 then
			sidebarItems[#sidebarItems+1]={
				id = "orders",
				label = translations["Sidebar_orders"][_G.Lang],
				labelColorOn = {1,0.3},
				labelColorOff = {1},
				switch = true,
				onTurnOn = function( event )
					if currscene~=nil then composer.removeScene(currscene) end
					composer.gotoScene( "actions.my_orders_scene", goto_options )                
				end,
				onTurnOff = function( event )
					-- body
				end
			}
			sidebarItems[#sidebarItems+1]={
				id = "orders_history",
				label = translations["history_orders"][_G.Lang],
				labelColorOn = {1,0.3},
				labelColorOff = {1},
				switch = true,
				onTurnOn = function( event )
					local options = {}
					for k, v in pairs(goto_options) do
						options[k]=v
					end
					options.params = options.params or {}
					options.params.entered = {}
					for k, v in pairs(entered) do
						options.params.entered[k]=v						
					end
					if currscene~=nil then composer.removeScene(currscene) end
					composer.gotoScene( "actions.my_orders_history_scene", options )
				end,
				onTurnOff = function( event )
					-- body
				end
			}
		end
		sidebarItems[#sidebarItems+1]={
			id = "qr_code",
			label = translations["qr_code"][_G.Lang],
			labelColorOn = {1,0.3},
			labelColorOff = {1},
			switch = true,
			onTurnOn = function( event )
				if currscene~=nil then composer.removeScene(currscene) end
				composer.gotoScene( "actions.qr_code_scene", goto_options )                
			end,
			onTurnOff = function( event )
				-- body
			end
		}
	end
	if userProfile.SelectedRestaurantData~=nil or currscene == "actions.my_orders_scene" then
		sidebarItems[#sidebarItems+1]={
			id = "menu",
			label = translations["Sidebar_menu"][_G.Lang],
			labelColorOn = {1,0.3},
			labelColorOff = {1},
			switch = true,
			onTurnOn = function( event )
				if currscene~=nil then composer.removeScene(currscene) end
				composer.gotoScene( "actions.rest_menu_scene", goto_options )                
			end,
			onTurnOff = function( event )
				-- body
			end
		}
		sidebarItems[#sidebarItems+1]={
			id = "about",
			label = translations["Sidebar_about"][_G.Lang],
			labelColorOn = {1,0.3},
			labelColorOff = {1},
			switch = true,
			onTurnOn = function( event )
				if currscene~=nil then composer.removeScene(currscene) end
				composer.gotoScene( "actions.rest_about_scene", goto_options )                
			end,
			onTurnOff = function( event )
				-- body
			end
		}
	end

	sidebarItems[#sidebarItems+1]={
		id = "settings",
		label = translations["Sidebar_settings"][_G.Lang],
		labelColorOn = {1,0.3},
		labelColorOff = {1},
		left = 33, 
		switch = true,
		onTurnOn = function( event )
			if currscene~=nil then composer.removeScene(currscene) end
			composer.gotoScene( "actions.settings_scene", goto_options )                
		end,
		onTurnOff = function( event )
			-- body
		end
	}		

	for i=1, #sidebarItems do
		if sidebarItems[i].id == on_button then
			sidebarItems[i].state = "on"
		end
	end

	local initpos = nil

	mySidebar = sidebar.newSidebar({
		displayGroup = mainGroup,
		width = 250,
		items = sidebarItems,
		itemWidth = 50,
		modal = true, -- set to false if multiple buttons can be selected
		spacing = 0,
		bgColor={ 0.8,0.7,0.9, 0 },
		font = "HelveticaNeueCyr-Medium",
		fontSize = 19,
		top = general_top,
		side = "left",
		seprNumber = #sidebarItems,
		seprWidth = 190,
		seprAlign = "left",
		seprPad = 21.5,
		timeOpen = 700,
		transOpen = easing.outExpo,
		delayOpen = 200,
		timeClose = 230,
		transClose = nil,
		delayClose = 0,
		onOpen = function( event ) 
			if event.phase == "will" then
				mySidebar.curtain:toFront()
				event.target:toFront()

				if not initpos then
					initpos = {
						bg_y = mySidebar.curtain.y,
						bg_x = mySidebar.curtain.x,
						mg_y = mainGroup.y
					}
				end

				transition.cancel( mySidebar.curtain )
				mySidebar.curtain.y = initpos.bg_y 
				mySidebar.curtain.x = initpos.bg_x
				mainGroup.y = initpos.mg_y
				local function onObjectTouch( event )
    				return true
				end

				mainGroup.bg=display.newRect( display.contentWidth / 2+display.screenOriginX, display.contentHeight / 2+display.screenOriginY, display.contentWidth-4*display.screenOriginX, display.contentHeight-4*display.screenOriginY )
				mainGroup.bg:setFillColor( 0.7,0.7,0.7 ); mainGroup.bg.alpha = 0.5
				mainGroup:insert( mainGroup.bg ) 
				mainGroup.bg:addEventListener( "touch", onObjectTouch )

				transition.to( mySidebar.curtain, { time=200, delta=true, y=92 })
				transition.to( mainGroup , { 
					time = 100, delay = 100,
					y = mainGroup.y + 83 - display.topStatusBarContentHeight,
				})
				_G.dist = event.target.options.width + 1
				_G.curtainX=mySidebar.curtain.x
				transition.to( mySidebar.curtain, { time=700, delay=200, x=mySidebar.curtain.x+dist, 
					transition=easing.outExpo 
				})

				if mySidebar.user_info[3] and userProfile.SelectedRestaurantData then
					local pos_id = userProfile.SelectedRestaurantData.com_id
					local balance = userProfile.balance[pos_id]
					mySidebar.user_info[3].text = balance and balance.._G.Currency or "-"
				end
			end
		end,
		onClose = function( event )
			if event.phase == "will" then
				transition.cancel( mySidebar.curtain )
				transition.to( mySidebar.curtain, { time=1680, delay=230, delta=true, y=-100, 
					transition=easing.outExpo 
				})
				if mainGroup.bg then
					mainGroup.bg:removeSelf( )
					mainGroup.bg=nil
				end
				transition.to( mainGroup ,{
					time=200, delay = 230,
					y = mainGroup.y - 83 + display.topStatusBarContentHeight,
				})
				_G.dist=_G.dist or event.target.options.width + 1
				_G.curtainX = _G.curtainX or mySidebar.curtain.x-_G.dist
		--		print("TRANSITION",_G.curtainX,_G.dist)
				--local dist = event.target.options.width + 1
				transition.to( mySidebar.curtain, { time=230, x=_G.curtainX } )
  
			elseif event.phase == "did" then
				for i=1, #myHeader.buttons do
					if myHeader.buttons[i].id == "sidebar" then
						myHeader.buttons[i]:off()
					end
				end
			end
		end,
	})
	stage:insert(mySidebar)

	mySidebar.curtain = display.newImageRect( --stage, 
		"assets/bgSidebar.png", 1300*0.5, 1500*0.5 
	)
	mySidebar.curtain.x = display.screenOriginX + mySidebar.curtain.width*0.5*0.12
	mySidebar.curtain.anchorY = 0
	mySidebar.curtain.y = display.screenOriginY - 100
	stage:insert( mySidebar.curtain )
	mySidebar.curtain:toFront()

	mySidebar.user_info = display.newGroup()
	local user_name = display.newText( mySidebar.user_info,
		translations["need_login"][_G.Lang],
		0,0,
		"HelveticaNeueCyr-Light", 16 
	)
	local entered = EnterModule.checkEntered()
	if entered then
--		print("WWWWWWWWWW")
		user_name.text = entered.guest_fullname
		display.newRect( mySidebar.user_info, 0, 0, 0.5, 15 )
		mySidebar.user_info[2].alpha = 0.5
		mySidebar.user_info[2].x = user_name.x + user_name.width*0.5 + 12        
		display.newText( mySidebar.user_info,
			" - ".._G.Currency,
			0,0,
			"HelveticaNeueCyr-Medium", 20
		)
		if mySidebar.user_info.numChildren then
			mySidebar.user_info[3].anchorX = 0
			mySidebar.user_info[3].x = mySidebar.user_info[2].x + 12
		end
	end
	mySidebar.user_info:addEventListener( "touch", function( event )
		if event.phase == "began" then
			mySidebar:close()
			mySidebar:press( "cabinet" )
		end
	end )
	
	mySidebar:attach({
		content = mySidebar.user_info,
		top = 34,
		left = 21
	})
end

function CreateHeader( headerItems, bg_options )
	-- print('create header')  
	local stage = display.getCurrentStage()

	if myHeader~=nil then
		myHeader:removeSelf()
		myHeader=nil
	end

	local sidebar_icon = "sidebar"
	local currscene = composer.getSceneName( "current" )
	if currscene == "actions.my_cabinet_scene" or currscene == "actions.my_orders_history_scene" then
		sidebar_icon = "sidebarBlack"
	end

	headerItems[#headerItems+1] = {
		id = "sidebar",
		--label = "",
		icon = sidebar_icon,
		iconWidth = 22,
		iconHeight = 12,
		switch = true,
		left = 20,
		turnOn = function( event ) mySidebar:open() end,
		turnOff = function( event ) mySidebar:close() end
	}

	local rest_of_the_day = ldb:getSettValue( "rest_of_the_day" )
--	print(userProfile.SelectedRestaurantData.com_id)
	if not bg_options and userProfile.SelectedRestaurantData~=nil and userProfile.SelectedRestaurantData.main_photo then
--		print('1')
		bg_options = {}
		bg_options.imgfile = userProfile.SelectedRestaurantData.main_photo.name
		bg_options.basedir = userProfile.SelectedRestaurantData.main_photo.basedir
		bg_options.width = _Xa
		bg_options.height = _Xa/userProfile.SelectedRestaurantData.main_photo.aspect 
	elseif not bg_options and rest_of_the_day then
--	print('2')
		local found=false
		for i=1, #RestoransList do
			if RestoransList[i].pos_id == tonumber( rest_of_the_day ) then
				bg_options = {}
				bg_options.imgfile = RestoransList[i].main_photo.name
				bg_options.basedir = RestoransList[i].main_photo.basedir
				bg_options.width = _Xa
				bg_options.height = _Xa/RestoransList[i].main_photo.aspect         
				found=true
				break;
			end
		end
		if not found then
			bg_options = {}
			bg_options.imgfile = "assets/noPhoto.jpg"
			bg_options.width = _Xa
			bg_options.height = _Xa/1.42 
		end
	elseif not bg_options then
--	print('3')
		bg_options = {}
		bg_options.imgfile = "assets/noPhoto.jpg"
		bg_options.width = _Xa
		bg_options.height = _Xa/1.42 
	end 
	--clib.printTable(bg_options)
	-- init HEADER
	myHeader = header.newHeader{
		items = headerItems,
		height = 40 + display.topStatusBarContentHeight,
		modal = true, -- set to false if multiple buttons can be selected
		spacing = display.topStatusBarContentHeight*0.5,
		bgFile = bg_options.imgfile,
		baseDir = bg_options.basedir,
		bgWidth = bg_options.width,
		bgHeight = bg_options.height,
		bgColor = bg_options.color or { 0,0.5 },
	}

	----
	--stage:insert( myHeader )
	mainGroup:insert( myHeader ) 
end

function CreateBaseHeader(params)

	local headerItems = {}
	local cartAnimate=false
	if params then
		cartAnimate = params.cartAnimate
	end
--	print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
--	print("CreateBaseHeader")
	if userProfile.SelectedRestaurantData and userProfile.activeOrderID == 0 then
		local id = ovlib:findLocalActiveOrder( 
			userProfile.SelectedRestaurantData.com_id,
			userProfile.SelectedRestaurantData.pos_id
		)
		userProfile.activeOrderID = id
	end

	if userProfile.SelectedRestaurantData~=nil and composer.getSceneName("current")~="actions.rests_list_scene" then 
		headerItems[1] = {              
			id = "rest_title",
			label = utf8sub(userProfile.SelectedRestaurantData.title,1,18),
			labelColor = { 1,1,1 },
			fontSize = 15,
			params = {},
			switch = true,
			turnOn = function( event ) mySidebar:press("about") end,
		}
	else
		headerItems[1] = {              
			id = "city",
			label = userProfile.currentCity.name or translations["select_city"][_G.Lang],
			labelColor = { 1,1,1 },
			fontSize = 15,
			params = {},
			switch = true,
			turnOn = function( event ) 
				createCityBox( function()
					CreateBaseHeader()
				end )
			end,
		} 
	end    
	if userProfile.activeOrderID ~= 0 then
		headerItems[2] = {              
		   id = "ord_sum",
		   label = ldb:getOrderSum( userProfile.activeOrderID ),
		   labelColor = { 1,1,1 },
		   labelY = 0,
		   fontSize = 18,
		   icon = "cart",
		   iconWidth = 17,
		   iconHeight = 15,
		   animate=cartAnimate,
		   iconY = -1,
		   labelAnchor = { pos = "left", pad = 3 },
		   params = {},
		   switch = true,
		   right = 20,
		   yShift = -1,
		   turnOn = function( event ) mySidebar:press("orders") end, 
		}
	end

	CreateHeader(headerItems)
end

local function initUI()
	-- init SIDEBAR
	CreateSideBar()

	mainGroup:insert( composer.stage )
	stage:insert( mainGroup )
	stage:insert( mySidebar )
end



-- 3. DEFINE INITIAL DATA LOAD METHODS
--------------------------------------------------
local function loadUserData()
	local entered = EnterModule:checkEntered()

	if entered then
		rdb:getRowData( "guests", entered.guest_id, function( state, data )
			if state == "success" then
				if entered.guest_email ~= data.guest_email or 
				entered.guest_password ~= data.guest_password then
				--	ldb:clearGuestData()
				else
				--	ldb:saveGuestData( data )
				end
			else ldb:clearGuestData()
			end
		end )

		rdb:getGuestBalance( entered.guest_id, function( state, balance )
			if state == "success" then
				userProfile.balance = balance
			end
		end )		
	end
end

local function loadPosNews()
	local max_posn_id=ldb:getMaxField("pos_news","posn_id")
			local filter=""
				filter= {
				  group= "and",
				  conditions= {
					  {
						left= "posn_id",
						oper= ">",
						right= max_posn_id
					  }	  
				  }
				}
--				print("max_posn_id",max_posn_id)
			rdb:getData( "pos_news", function( response, rows )
				if response == "success" then
	--				print("pos_news  success")
					for i=1, #rows do
	--					print(rows[i])
						ldb:addNewRowData( "pos_news", {com_id=rows[i].com_id,posn_type=rows[i].posn_type,posn_action_date_end=rows[i].posn_action_date_end,pos_id=rows[i].pos_id,posn_id=rows[i].posn_id,posn_title=rows[i].posn_title,posn_text=rows[i].posn_text,posn_datetime=rows[i].posn_datetime} )
					end
				else load_data_error = state
				end    
			end, filter )
end

local myLocation = {}
myLocation.latitude = 0
myLocation.longitude = 0
_G.myLocation = myLocation

local  function calculateTheDistance (latA, lonA, latB, lonB) 
    local EARTH_RADIUS=6372795
    local M_PI = math.pi
    -- Расстояние между двумя точками
    -- latA, lonA - широта, долгота 1-й точки,
    -- latB, lonB - широта, долгота 2-й точки
    -- Написано по мотивам http://gis-lab.info/qa/great-circles.html
    -- Михаил Кобзарев <mikhail@kobzarev.com>

    --  перевести координаты в радианы
    local lat1 = latA * M_PI / 180;
    local lat2 = latB * M_PI / 180;
    local long1 = lonA * M_PI / 180;
    local long2 = lonB * M_PI / 180;

    -- косинусы и синусы широт и разницы долгот
    local cl1 = math.cos(lat1);
    local cl2 = math.cos(lat2);
    local sl1 = math.sin(lat1);
    local sl2 = math.sin(lat2);
    local delta = long2 - long1;
    local cdelta = math.cos(delta);
    local sdelta = math.sin(delta);
     
    -- вычисления длины большого круга
    local y = math.sqrt(math.pow(cl2 * sdelta, 2) + math.pow(cl1 * sl2 - sl1 * cl2 * cdelta, 2));
    local x = sl1 * sl2 + cl1 * cl2 * cdelta;
     
    local ad = math.atan2(y, x);
    local dist = ad * EARTH_RADIUS;
    dist = math.round(dist)*0.001
    return dist;
end

function _G.defineUserCity( currcity_name,currcity_id )
	print("_G.defineUserCity",currcity_name)
	currcity_name = currcity_name or ldb:getSettValue( "guest_city" )
	local currcity_id=currcity_id or 0
	if currcity_id~=0 then
		OneSignal.SendTag("city", currcity_id)
	end
	if currcity_name then
		
		--print(currcity_name)
		for k, v in pairs( CitiesList ) do
			if currcity_name == k then
				userProfile.currentCity = {
					name = k,
					longitude = v.longitude,
					latitude = v.latitude
				}
				--print(v.longitude)
				ldb:setSettValue( "guest_city", currcity_name )
				break
			end
		end
		CityRestorans = {}
		for i=1, #RestoransList do
			if RestoransList[i].city == currcity_name then
				if myLocation.longitude ~= 0 and RestoransList[i].geo_lat and RestoransList[i].geo_long then
					RestoransList[i].distance =  calculateTheDistance(myLocation.latitude,myLocation.longitude,RestoransList[i].geo_lat,RestoransList[i].geo_long)
				end  
				table.insert(CityRestorans,RestoransList[i])
			end
		end
			table.sort(CityRestorans,function(a,b)
				if a ~= nil and b ~= nil then
					if a.distance and b.distance then
						return a.distance < b.distance
					elseif a.distance == nil and b.distance then
						return  false
					elseif a.distance and b.distance == nil then
						return true
					else
						return false
					end
				else
					if a == nil and b then
						return false
					elseif a and b == nil then
						return true
					elseif a == nil and b == nil then
						return false
					end
				end
			end)
	end
end

local function defineLastRest()
	local last_rest = ldb:getSettValue( "last_rest" )
	if last_rest then
		last_rest = tonumber( last_rest )
		for i=1, #RestoransList do
			if RestoransList[i].pos_id == last_rest then
				userProfile.SelectedRestaurantData = RestoransList[i]
				break
			end
		end
	end
end

local function onDataLoad( data, onComplete )
	local load_data_error = nil
	local xScale,yScale
	-- print("2")
	-- printResponse(data)
	local pos_filter = ""
	for i,item in pairs( data ) do
		local dataItem = {}
		-- print("%%%%%%%%%%%%%%%%%",item)
		if item.lang_code == userProfile.lang then       
			print( item.about_name, 'com_id: '..item.com_id, 'pos_id: '..item.pos_id )
			print( "menu_type: ", item.pos_guest_menu_type, "lolyalty: ", item.com_loyalty_type )

			-- формируем таблицу данных заведения

			dataItem = {
				com_id = item.com_id,
				pos_id = item.pos_id,
				name = item.com_name,
				logo_file = "assets/noLogo.jpg", --logo_file_name,
				logo_dir = system.ResourceDirectory, --system.TemporaryDirectory,
				logo_width = 225, --item.about_logo_width,
				logo_height = 225, --item.about_logo_height,
				city_id = item.city_id,
				city = item.city_name,
				pos_bonusme_merchant_id = item.pos_bonusme_merchant_id,
				use_bonusme_proccessing = item.use_bonusme_proccessing,
				rating = item.rating or 0,
				geo_lat = tonumber(item.about_latitude),
				geo_long = tonumber(item.about_longitude),
				title = item.about_name or translations["no_name"][_G.Lang],
				descr = item.about_description or translations["no_description"][_G.Lang],
				addr = item.about_address or translations["no_address"][_G.Lang],
				work_time = item.about_working_hours or "00:00 - 24:00",
				phone = item.about_phone or " ",
				post_name = item.post_name or " ",
				pos_type = item.pos_type,
				average_bill = tonumber(item.about_average_bill) or 0,
				menu_type = item.pos_guest_menu_type,
				usr_lang_code = item.usr_lang_code,
				srv_url = item.srv_url,
				cur_short = item.cur_short or "",
				com_loyalty_type = item.com_loyalty_type,
				main_photo = {
					name = "assets/noPhoto.jpg",
					aspect = 1.42,
					basedir = system.ResourceDirectory
				},
				meta = item
			}
			if pos_filter == "" then pos_filter = item.pos_id
			else pos_filter = pos_filter..","..item.pos_id
			end
			table.insert(RestoransList, dataItem)
			
			-- формирум список доступных городов

			CitiesList[ item.city_name ] = {
				latitude = tonumber(item.about_latitude),
				longitude = tonumber(item.about_longitude) 
			}

			-- проверяем наличие логотипов заведений

			local logo_file_name = 'logos/logo'..item.pos_id ..'.jpg'
			if clib:fileExists( logo_file_name, system.TemporaryDirectory ) and item.about_logo_width then
				dataItem.logo_file = logo_file_name
				dataItem.logo_dir = system.TemporaryDirectory
				dataItem.logo_width = item.about_logo_width
				dataItem.logo_height = item.about_logo_height			
			end

			-- обновляем логотипы заведений

			rdb:downloadRestLogo( item.com_id,item.srv_url,item.pos_id, logo_file_name, function( state, logo_data )
				if state ~= "success" then return end
				for i=1, #RestoransList do
					if RestoransList[i].pos_id == logo_data.pos_id then
						RestoransList[i].logo_file = logo_data.file
						RestoransList[i].logo_dir = logo_data.basedir
						if RestoransList[i].meta.about_logo_width then
							RestoransList[i].logo_width = RestoransList[i].meta.about_logo_width
						end
						if RestoransList[i].meta.about_logo_height then
							RestoransList[i].logo_height = RestoransList[i].meta.about_logo_height
						end
						_G.DataEventDispatcher:dispatchEvent({ name = "updatedLogo", pos_id = logo_data.pos_id })
						break
					end
				end
			end )

		end
	end
	CityRestorans = RestoransList

	-------------
	rdb:getData( "vw_cuisines", function( response, rows )
		if response == "success" then
			for i=1, #rows do
				for j=1, #RestoransList do
					if rows[i].pos_id == RestoransList[j].pos_id and
					rows[i].lang_code == userProfile.lang then
						RestoransList[j].cuisines = RestoransList[j].cuisines or {}
						table.insert( RestoransList[j].cuisines, rows[i].csn_name )
					end
				end
			end            
		else load_data_error = state
		end
	end)

	-------------
	rdb:getData( "vw_feautures", function( response, rows )
		if response == "success" then
			for i=1, #rows do
				for j=1, #RestoransList do
					if rows[i].pos_id == RestoransList[j].pos_id and
					rows[i].lang_code == userProfile.lang then
						RestoransList[j].features = RestoransList[j].features or {}
						table.insert( RestoransList[j].features, rows[i].ftr_name )
					end
				end
			end
		else load_data_error = state
		end    
	end) -------------
	
	rdb:getData( "vw_bonusme_all_payments", function( response, rows )
		if response == "success" then
			for i=1, #rows do
				for j=1, #RestoransList do
					if   (rows[i].pos_id == -1 and RestoransList[j].com_id == rows[i].com_id ) or  rows[i].pos_id == RestoransList[j].pos_id then
						RestoransList[j].typePays = RestoransList[j].typePays or {}
						table.insert( RestoransList[j].typePays, rows[i] )
					end
				end
			end
		else load_data_error = state
		end    
	end) 

	-- качаем главные фотки ресторанов

	local size = math.round(display.pixelWidth*0.7).."x"..math.round(display.pixelHeight*0.7)
	local filter = {
		group = "and",
		conditions = {
			{ 
				left = "abp_is_main",
				oper = "=",
				right = "true"
			},
			{ 
				left = "pos_id",  --
				oper = "in",
				right = "("..pos_filter..")"
			},
			{ 
				left = "user_guest",  --
				oper = "=",
				right = "true"
			} 			
		}
	}

	rdb:getRestPhoto( "restPhotos", size, function( state, response )
		if state ~= "success" then return end
		for i=1, #RestoransList do
			for k_comid, v_pos in pairs( response ) do
				for k_posid, v_photos in pairs( v_pos ) do
					if RestoransList[i].com_id == k_comid 
					and RestoransList[i].pos_id == k_posid then
						RestoransList[i].main_photo = v_photos.main
					end
				end
			end
		end

		-- определение заведени дня - НЕИСПОЛЬЗУЕТСЯ
		-- rdb.sign = 0
		-- rdb:getRowData( "global_settings", 1, function( state, data )
		-- 	if state == "success" then
		-- 		ldb:setSettValue( "rest_of_the_day", data.rest_of_the_day )
		-- 	end
		-- end )

	end, filter, true )

	local mode = ldb:getSettValue( "filter_mode" )
	if mode and mode ~= "" then composer.setVariable( "filter_mode", mode ) end	
print( "<<<<<<<<<<", mode )
	-------------------------------------
	onComplete( load_data_error )
	-------------------------------------	
end

local function loadData( onComplete )
	Spinner:start( {1,0} )

	local default_lang="en"
	local host="web.smarttouchpos.eu"
	--local host="77.120.98.140:3002"
	if system.getInfo( "environment" ) == "simulator" then
		default_lang="ru"
	else
		local app_language=string.lower(system.getPreference( "ui", "language" ))
		local app_country=string.lower(system.getPreference( "locale", "country" ))
		--[[ native.showAlert( string.lower(system.getPreference( "ui", "language" )), string.lower(system.getPreference( "locale", "country" )) , { "OK" } )]]
		if (string.find( app_language, "ru" ) ~=nil) or (string.find( app_language, "ua" ) ~=nil) or (app_language=="ru") or (app_language=="ua") or (app_language=="russian") or (app_language=="ukrainian") then
			default_lang="ru"
		elseif (app_language=="tr" or app_country=="tr") then
			default_lang="tr"
		elseif (app_language=="es" or app_country=="es" or app_country=="pe") then
			default_lang="es"
		end
		
		if (app_country=="ru") or (app_country=="ua") then
			default_lang="ru"
		end
		if default_lang~="ru" and default_lang~="es" then
			if (app_country=="gb") or (app_country=="de") or (app_country=="in") or (app_country=="id") or (app_country=="ca") or (app_country=="au") or (app_country=="ae") or (app_country=="pt")  or (app_country=="fr") or (app_country=="tr") or (app_country=="us") or (app_country=="eg") or (app_country=="ar") or (app_country=="cz") or (app_country=="se") or (app_country=="br") or (app_country=="nl") or (app_country=="ir") or (app_country=="vn") or (app_country=="mx") or (app_country=="nz") or (app_country=="at") or (app_country=="be") or (app_country=="ro") or (app_country=="jp") or (app_country=="kr") or (app_country=="it") or (app_country=="ng") or (app_country=="hk") or (app_country=="za") or (app_country=="cm") or (app_country=="ph") or (app_country=="gr") or (app_country=="cn") then
				host="en.smarttouchpos.eu"
			end
		end
	end
	_G.Lang = ldb:getSettValue( "guest_lang") or default_lang
	_G.host = host
	_G.countries = ldb:getRowsData("countries","lang_code = '".._G.Lang.."' and cntr_code is not null order by cntr_name")
--	print(_G.host,_G.Lang)
	-- _G.Lang = "es"
--	_G.host = "en.smarttouchpos.eu"
	--deleting temp files
	--clib:DeleteTempFiles()
	userProfile.lang = _G.Lang
	rdb.sign = 0
	ldb:open()
	print( "////////////////////", _G.Lang )
	--------------------------------
	print(" <<<<<<<< load User's settings >>>>>>>>")
	loadUserData()
	print(" <<<<<<<< load news >>>>>>>>")
	loadPosNews()
	if clib:fileExists("userAvatarPhoto.jpg",system.DocumentsDirectory) then
		userProfile.guestHasPhoto=true
	end
	local function networkListener( state, data )
		-- print("1")
		if state == "success" then
			-- print("6")
			onDataLoad( data, function( load_error )
				if load_error then
					print(" -- load_error--")
					uilib.createReloadScreen( load_error, function()
						Spinner:start()
						rdb.sign = 0
						_G.demoModeWithoutCountriesFilter=true
						rdb:getData( "vw_bonus_allcompanies", networkListener,nil,true )
					end )
					return                    
				end
				print("4")
				_G.defineUserCity()        -- define city location
				defineLastRest()
				onComplete()
				Spinner:stop()
			end)
		else
			if _G.demoModeWithoutCountriesFilter==false then
				_G.demoModeWithoutCountriesFilter=true
				local filter=""
				rdb:getData( "vw_bonus_allcompanies", networkListener, filter )				
			else
			Spinner:stop()
			
			uilib.createReloadScreen( state, function()
				Spinner:start()
				rdb.sign = 0
				local filter=""
				if _G.demoModeWithoutCountriesFilter==false then
					filter={
					  group= "and",
					  conditions= {
						  {
							left= "cntr_code",
							oper= "=",
							right= "'".._G.userCountry.."'"
						  },
						  {
							left= "com_id",
							oper= "<>",
							right= "102"
						  },
						  {
							left= "com_id",
							oper= "<>",
							right= "4"
						  },
						  {
							left= "com_id",
							oper= "<>",
							right= "30"
						  }							  							  						  
						 --  {
							-- left= "cntr_code",
							-- oper= "is",
							-- right= "null"
						 --  }		 
					  }
					}
				end
				rdb:getData( "vw_bonus_allcompanies", networkListener, filter,true )				
			end )
			end
		end
	end 
	rdb.sign = 0
	
	local function country_listener(res)
		if res.result=='completed' then
			_G.userCountry=res.data.country
			if _G.userCountry=="UA" and _G.host == "en.smarttouchpos.eu" then
				_G.userCountry=system.getPreference( "locale", "country" )
			end
			ldb:getGuestCodeCountry()
		-- _G.userCountry = "PE";
		-- _G.userCountry = "UA"
		--print( "COUNTRY_LANG", _G.userCountry, _G.Lang )
			local filter=""
			if _G.demoModeWithoutCountriesFilter==false then
				filter= {
				  group= "and",
				  conditions= {
					  {
						left= "cntr_code",
						oper= "=",
						right= "'".._G.userCountry.."'"
					  },
						  {
							left= "com_id",
							oper= "<>",
							right= "102"
						  },
						  {
							left= "com_id",
							oper= "<>",
							right= "4"
						  },
						  {
							left= "com_id",
							oper= "<>",
							right= "30"
						  }							  
				  }
				}
			end
			rdb:getData( "vw_bonus_allcompanies", networkListener, filter,true )
		--     native.showAlert(_G.appTitle, _G.userCountry .."  ".._G.host, { "OK" })			
			-- print(_G.userCountry)
			OneSignal.SendTags({["country"] = _G.userCountry,["lang"] = _G.Lang})
		end
	end
	-- rdb_common:getCountry{listener=country_listener}


	local function writeCountriesList(d) -- инициализация таблицы CountriesList 
		CountriesList = {}
		for k, v in pairs(d) do
			if v.cntr_code and v.cntr_code:len() > 0 then
				if CountriesList[v.cntr_code]  then
					table.insert(CountriesList[v.cntr_code],v)
				else
					CountriesList[v.cntr_code] = {}
					table.insert(CountriesList[v.cntr_code],v)
				end
			end
		end

		for k, v in pairs(CountriesList) do

			local t = {}
			for ki, vi in pairs(v) do
				t[vi.lang_code] = vi
				t[vi.lang_code].lang_code = nil
			end
			CountriesList[k] = t
		end		
	end

	local hasCountrs = false
	local dc =  ldb:getAllData("countries")
	if dc and #dc > 0 then
		hasCountrs = true
		writeCountriesList(dc)
	end

	if hasCountrs then
		rdb_common:getCountry{listener=country_listener}
	else
		rdb_common:getListCountry{listener=function(res)
			if res.result=='completed' then
				if res.data and res.data.rows and #res.data.rows then
					ldb.db:exec("BEGIN TRANSACTION")
					ldb:delRows("countries")
					for i,item in pairs(res.data.rows ) do				
						ldb:addNewRowData( "countries", item)
					end 
					ldb.db:exec("COMMIT")
					-- запролняем телефоные коды 
					local cntrPhCode = json.decodeFile( system.pathForFile( "lib/countries.json" ) ).countries
					local d = ldb:getRowsData("countries", "lang_code = 'en'")
					for k, v in pairs(d) do
						for kp, vp in pairs(cntrPhCode) do
							if v.cntr_name == vp.name then
								if  vp.code then
									ldb:updateRowDataWithCondition("countries","cntr_id = "..v.cntr_id, {cntr_ph_code = vp.code} )
								end
								break
							end
						end
					end

					local dc =  ldb:getAllData("countries")
					if dc and #dc > 0 then
						hasCountrs = true
						writeCountriesList(dc)
					end
				end	
			end
			rdb_common:getCountry{listener=country_listener}	
		end}
	end

	--local rdb_bonus = require( "lib.db_remote_bonuses" )
	rdb_bonuses:downloadQRCode("qr.jpg")
end

local function loadingScreen( enable )
	if enable then
	--	print("%$%$%%$%$%$%",display.screenOriginX,display.screenOriginY)
		local xS=(display.actualContentWidth)/599
		local yS=(display.actualContentHeight)/835
		local s=math.max( xS, yS )+0.001
		 local loading_img = display.newImage( loadingGroup, "assets/fon1.jpg" )
		 loading_img.anchorX=0.5
		 loading_img.anchorY=0.5
		-- local loading_img = display.newImage( loadingGroup, "Icon-xhdpi.png" )
		-- loading_img.anchorX=0.5
		-- loading_img.anchorY=0.5
		loading_img.width=599*s
		loading_img.height=835*s
		loading_img.x=_mX
		loading_img.y=_mY
		local loading_txt = display.newText({
				parent = loadingGroup, 
				text = "loading ...", 
				font = "HelveticaNeueCyr-Light", 
				fontSize = 14
		})
		loading_txt:setFillColor( 0 )
		loading_txt.anchorX=0.5
		loading_txt.anchorY=0.5
		loading_txt.x=_mX
		loading_txt.y=loading_img.y+loading_img.height*0.7
	else
		loadingGroup:removeSelf( )
	end
end
-- 4. START APP
---------------------
if rdb_common:checkInternetConnection()==false then
	local function onComplete( event2 )
        if event2.action == "clicked" then
            native.requestExit()
        end
    end 
    native.showAlert( 
        _G.appTitle, translations["enter_messages"][_G.Lang][7], 
        { "OK" }, 
        onComplete
    )
end

local function makePaytmPayment()
				local MID="eMAPPS30731868098218"
				local ORDER_ID="s23234sdfsdf"..math.random(10)..math.random(10)..math.random(10)
				local CUST_ID="345345345"
				local TXN_AMOUNT=10
				local MOBILE_NO=7777777777
				local EMAIL="necky@bigmmir.net"
				local function networkListener( event )
					if ( event.isError ) then
						print( "Network error!" )
						Spinner:stop()
					elseif event.phase == "ended" then
						print(":",event.response,":")
						local t = json.decode(event.response)
						printResponse(t)
						--t.res=string.gsub(t.res,"^[^<>]+$","")
						--print("99999`",t.res,"`9999")
						--
						webView = native.newWebView( _mX, _mY+((headerHeight + display.topStatusBarContentHeight)/2)-55, _Xa-2, -110+display.actualContentHeight-(headerHeight + display.topStatusBarContentHeight) )							
						--webView:request( "http://www.coronalabs.com/" )
						local html='<html>'
						html=html..'<body onload="document.frm1.submit()">'
						html=html..'<form action="https://pguat.paytm.com/oltp-web/processTransaction" name="frm1">'
						html=html..'      <input type="hidden" name="REQUEST_TYPE" value="DEFAULT" />'
						html=html..'      <input type="hidden" name="CHANNEL_ID" value="WAP" />'
						html=html..'      <input type="hidden" name="INDUSTRY_TYPE_ID" value="Retail" />'
						html=html..'      <input type="hidden" name="WEBSITE" value="WEB_STAGING" />'
						html=html..'      <input type="hidden" name="CHECKSUMHASH" value="'..t.res..'" />'
						html=html..'      <input type="hidden" name="MID" value="'..MID..'" />'
						html=html..'      <input type="hidden" name="ORDER_ID" value="'..ORDER_ID..'" />'
						html=html..'      <input type="hidden" name="CUST_ID" value="'..CUST_ID..'" />'
						html=html..'      <input type="hidden" name="TXN_AMOUNT" value="'..TXN_AMOUNT..'" />'
						html=html..'      <input type="hidden" name="MOBILE_NO" value="'..MOBILE_NO..'" />'
						html=html..'      <input type="hidden" name="EMAIL" value="'..EMAIL..'" />'
						html=html..'   </form>'
						html=html..'</body>'
						html=html..'</html>'
						loadsave.saveFile(html, "paytm.html",system.TemporaryDirectory)
						webView:request(  "paytm.html", system.TemporaryDirectory )

					end
				end

							local params = {}
							local headers = {}
							headers["Content-Type"] = "application/x-www-form-urlencoded"
							headers["Accept-Language"] = "en-US"
							headers["Authorization"] = rdb_common:get_admin_authorization_string()
							params.headers = headers
							params.progress = "upload"
							params.body = "REQUEST_TYPE=DEFAULT&WEBSITE=WEB_STAGING&INDUSTRY_TYPE_ID=Retail&CHANNEL_ID=WAP&MID="..MID.."&EMAIL="..EMAIL.."&MOBILE_NO="..MOBILE_NO.."&TXN_AMOUNT="..TXN_AMOUNT.."&CUST_ID="..CUST_ID.."&ORDER_ID="..ORDER_ID
						    -- params.response = {
						    --     filename = "auth_res.json",
						    --     baseDirectory = system.TemporaryDirectory
		    				-- }
							network.request("http://web.smarttouchpos.eu:3005/webapi/bonusme/paytm_generate_checksum","POST",networkListener ,params )


end

local function makePaynearPayment()
	local orderData = {orderid="A00020",
	guest_id="3233",
	pos_id="247",
	merchant_data="md",
	order_desc="Testorder",
	merchantid=1007870,
	sum="20.00",
	guest_name="Harigopal",
	guest_address="Hyderabad",
	guest_city="Hyderabad",
	guest_state="Hyderabad",
	guest_postal_code="500082",
	guest_email="ppopl@gmail.com",
	guest_phone="9853766496",
	rectoken=0,
	bonuses_used=0}

				local function networkListener( result,data )
					if  result == "success" then
						webView = native.newWebView( _mX, _mY+((headerHeight + display.topStatusBarContentHeight)/2)-55, _Xa-2, -110+display.actualContentHeight-(headerHeight + display.topStatusBarContentHeight) )							
						print(data.redirectURL)
						webView:request(  data.redirectURL)

					end
				end	
	rdb:makePayment( orderData,networkListener,"paynear" )
end

local function makeCoingatePayment()
	local orderData = {orderid="A00020",sum=150,currency="USD",description="Кофе эспрессо"}

				local function networkListener( result,data )

					printResponse(data)
					if  result == "success" then
						webView = native.newWebView( _mX, _mY+((headerHeight + display.topStatusBarContentHeight)/2)-55, _Xa-2, -110+display.actualContentHeight-(headerHeight + display.topStatusBarContentHeight) )							
						print(data.redirectURL)
						webView:request(  data.payment_url)

					end
				end	
	rdb:makePayment( orderData,networkListener,"coinpayments" )
end


local function makeCryptonatorPayment()
	local orderData = {orderid="A00020",sum=150,currency="uah",description="Кофе эспрессо"}

				local function networkListener( result,data )

					printResponse(data)
					if  result == "success" then
						webView = native.newWebView( _mX, _mY+((headerHeight + display.topStatusBarContentHeight)/2)-55, _Xa-2, -110+display.actualContentHeight-(headerHeight + display.topStatusBarContentHeight) )							
						print(data.redirectURL)
						webView:request(  data.redirectURL)

					end
				end	
	rdb:makePayment( orderData,networkListener,"cryptonator" )
end

local function makeCacceptPayment()
	local orderData = {orderid="A00020",sum=150,currency="uah",description="Кофе эспрессо"}

				local function networkListener( result,data )

					printResponse(data)
					if  result == "success" then
						webView = native.newWebView( _mX, _mY+((headerHeight + display.topStatusBarContentHeight)/2)-55, _Xa-2, -110+display.actualContentHeight-(headerHeight + display.topStatusBarContentHeight) )							
						print(data.redirectURL)
						loadsave.saveFile(data.redirectURL, "paytm.html",system.TemporaryDirectory)
						webView:request(  "paytm.html", system.TemporaryDirectory )

						

					end
				end	
	rdb:makePayment( orderData,networkListener,"caccept" )
end


loadingScreen(true)


loadData( function()
	loadingScreen(false)
	initUI()
	--------------------------------
	--composer.gotoScene( "actions.main_scene" )
	composer.gotoScene( "actions.rests_list_scene" )
	--------------------------------
	--makePaytmPayment()
	--makeCacceptPayment()
				-- 			local params = {}
				-- 			local headers = {}
				-- 			headers["Content-Type"] = "application/json"
				-- 			headers["Accept-Language"] = "en-US"
				-- 			headers["Authorization"] = rdb_common:get_admin_authorization_string()
				-- 			params.headers = headers
				-- 			--params.progress = "upload"
				-- 		--	params.body = "REQUEST_TYPE=DEFAULT&WEBSITE=WEB_STAGING&INDUSTRY_TYPE_ID=Retail&CHANNEL_ID=WAP&MID="..MID.."&EMAIL="..EMAIL.."&MOBILE_NO="..MOBILE_NO.."&TXN_AMOUNT="..TXN_AMOUNT.."&CUST_ID="..CUST_ID.."&ORDER_ID="..ORDER_ID
				-- 		    -- params.response = {
				-- 		    --     filename = "auth_res.json",
				-- 		    --     baseDirectory = system.TemporaryDirectory
		  --   				-- }
		  --   								local function networkListener( event )
				-- 	if ( event.isError ) then
				-- 		print( "Network error!" )
				-- 		Spinner:stop()
				-- 	elseif event.phase == "ended" then
				-- 		print(":--",event.response,"--:")


				-- 	end
				-- end
				-- 			network.request("http://web.smarttouchpos.eu/webapi/chain/import_chain_goods_receipts","POST",networkListener ,params )

end)


if system.getInfo( "platformName" ) == "Win" then -- нужно удалить не совсем ясно для чего
	local function onKeyEvent(event)
	 
	    local phase = event.phase
	    local keyName = event.keyName
	   
	    -- Listening for B as well so can test Android Back with B key
	    if ("back" == keyName and phase == "down") or ("b" == keyName and phase == "down" and system.getInfo("environment") == "simulator")  then
	        print("Back")
	 
	    end
	 
	end 
	Runtime:addEventListener( "key", onKeyEvent )
end

_G.hasLocation = system.hasEventSource("location")
if _G.hasLocation then
	Runtime:addEventListener( "location", 
	    function (e)
	        if ( e.errorCode ) then
	            native.showAlert( "GPS Location Error", e.errorMessage, {"OK"} )
	            print( "Location error: " .. tostring( e.errorMessage ) )
	        else
	            myLocation.longitude = e.longitude
	            myLocation.latitude = e.latitude
	            if system.getInfo( "environment" ) == "simulator" then
					myLocation.longitude = 30.488053
	            	myLocation.latitude = 50.485339
	            end
	        end
	    end 
	)
else
	native.showAlert( "GPS", "no access to geodata", {"OK"} )
end

--------------------------------------------------------------------------------------------------------------
