local loadsave = require "lib.loadsave"

local mime = require "mime"
local json = require "json"
-- 
local crypto = require "crypto"
local rdb_common = require( "lib.db_remote_common" )
--local rdb_common1 = require( "lib.db_remote" )

local rdb_bonus = {}

function rdb_bonus:getGuestBalance(GuestID,listener)
  print("getGuestBalance")
	local filter= {
      group= "and",
      conditions= {
          {
              left= "guest_id",
              oper= "=",
              right= GuestID
          }	  
      }
	}
	rdb_common:getDataFromView{listener=listener,table_name="vw_bonus_balance_by_guest",filter=filter,admin_auth=true}
end

function rdb_bonus:getPosReviews(pos_id,limit,listener)
  local filter= {
      group= "and",
      conditions= {
          {
              left= "pos_id",
              oper= "=",
              right= pos_id
          }   
      }
  }
  rdb_common:getDataFromView{listener=listener,table_name="vw_reviews",orderField="ordrat_datetime desc",pageSize=limit,filter=filter,admin_auth=true}
end

function rdb_bonus:getMenu(params)
  print("rdb_bonus:getMenu")
	local filter= {
      group= "and",
      conditions= {  
          {
              left= "lang_code",
              oper= "=",
              right="'"..userProfile.SelectedRestaurantData.usr_lang_code.."'"--_G.Lang
          },          
           {
               left= "deleted",
               oper= "=",
               right="false"
           },   
          {
              left= "pos_id",
              oper= "=",
              right=userProfile.SelectedRestaurantData.pos_id
          },                  
      }
	}
	--rdb_common:getDataFromView{listener=params.listener,table_name="vw_bonus_pricelist",filter=filter,admin_auth=true}
  rdb_common:getDataFromView{listener=params.listener,filter={pos_id=userProfile.SelectedRestaurantData.pos_id,verion=0},func_call=3,table_name="pos_price_list_emenu"}
    --rdb_common1:getData("vw_pos_pricelist",params.listener)
end

function rdb_bonus:getModifiers()
  local tt = rdb_common:newGetPackTable()
  
  local filter= {group= "and",conditions= {{left= "lang_code",oper= "=",right= "'".._G.Lang.."'"},}}  
  tt:addTable('vw_modifiers',filter,
    function(data)
      print("Done loading vw_modifiers")    
      ldb.db:exec("BEGIN TRANSACTION")
      ldb:delRows("modifiers")
      for i,item in pairs(data) do
        item.gi_price = item.gi_price or 0.00
        item.prnc_id = item.prnc_id or 0
        item.gi_common_modifier = item.gi_common_modifier or false
        item.gi_description = item.gi_description or ""
        if item.gi_id~=nil then         
          ldb:addNewRowData( "modifiers", {
            gi_id=item.gi_id,
            com_id=item.com_id,
            version=item.version,
            gi_price=item.gi_price,
            gg_id=item.gg_id,
            gi_weightflag=tostring(item.gi_weightflag),
            prnc_id=item.prnc_id,
            gi_modifier=tostring(item.gi_modifier),
            gi_common_modifier=tostring(item.gi_common_modifier),
            gi_name=item.gi_name,--utf8replace(tostring(item.gi_name),utf8_lc_uc),   
            gi_description=item.gi_description,
            lang_code=item.lang_code} )
        end
      end
      ldb.db:exec("COMMIT")
    end
  );
  
  tt:addTable('menu_modifiers_links',nil,
    function(data)
      print("Done loading modifiersLinks")
      ldb.db:exec("BEGIN TRANSACTION")
      ldb:delRows("modifiersLinks")
      for i,item in pairs(data) do
        if item.gi_id~=nil then         
          ldb:addNewRowData( "modifiersLinks", {
            mml_id = item.mml_id,
              gi_id = item.gi_id,
              mi_id = item.mi_id,
            mml_price = item.mml_price,
              mml_obligatory = item.mml_obligatory,
              com_id =  item.com_id
              })
        end
      end
      ldb.db:exec("COMMIT")
    end
  );

  local filter= {group= "and",conditions= {{left= "lang_code",oper= "=",right= "'".._G.Lang.."'"},}}  
  tt:addTable('vw_modifiers_groups',filter,
    function(data)
      print("Done loading vw_modifiers_groups")
      ldb.db:exec("BEGIN TRANSACTION")
      ldb:delRows("modifiers_groups")
      for i,item in pairs(data) do
        if item.gg_id~=nil then         
          ldb:addNewRowData( "modifiers_groups", {
            gg_id = item.gg_id,
              com_id = item.com_id,
              gg_parent_id = item.gg_parent_id,
            lang_code = item.lang_code,
              gg_name = item.gg_name,
              gg_description =  item.gg_description,
              })
        end
      end
      ldb.db:exec("COMMIT")
    end
  );


  local function onFinish()
    print("Finish test !!!")
  end
  -- tt:setFunFinish(checkEndSync)
  tt:setFunFinish(function(p) end)
  tt:showInit()
  tt:getDataCloud({row_index=0})
end 

function rdb_bonus:getGuestHistory(GuestID,listener)
	-- local filter= {
 --      group= "and",
 --      conditions= {
 --          {
 --              left= "guest_id",
 --              oper= "=",
 --              right= GuestID
 --          },	  
 --          {
 --              left= "lang_code",
 --              oper= "=",
 --              right= "'".._G.Lang.."'"
 --          },		  
 --      }
	-- }
  print("getGuestHistory",GuestID)
	rdb_common:getDataFromView{listener=listener,func_call=3,table_name="bonus_history_by_guest",filter={guest_id=GuestID,lang_code=_G.Lang},admin_auth=true}
end

function rdb_bonus:downloadLogo(filename,com_id)
    local screen_size_quarter=(display.contentWidth*0.25).."x"..(display.contentHeight*0.25)
	local headers = {}
	headers["Content-Type"] = "application/json"
    print(com_id)
	headers["Authorization"] = rdb_common:get_authorization_string(com_id)

	local params = {}
	params.headers = headers
	params.progress = "download"	
	params.response = {
	filename = filename,
	baseDirectory = system.DocumentsDirectory
    }	
                        
    local function networkListenerPhotoGet( event )
            if ( event.isError ) then
                    print( "Network error!")
            elseif ( event.phase == "ended" ) then
            end
    end                        
                  
	network.request( "http://"..rdb_common:getconnectionstring().."/common/about/logo_pos?pos_id=247&size="..screen_size_quarter, "GET", 	networkListenerPhotoGet,  params )
end

function rdb_bonus:updatePlayerID(entered)
    local entered = entered
    if entered==nil then
        local EnterModule = require "ui.EnterModule"
        entered = EnterModule:checkEntered()
    end
      --update player_id
    if entered then  
      local sentTable = {
      table_name = "guests",
      admin_auth=true,
      listener = function() end, 
      body = {
                        ['guest_id'] = entered.guest_id,
                        ['player_id'] = userProfile.player_id,
                    } 
      }
      print("*********BEFORE PLAYER ID********************")
      print(sentTable.body.guest_id)
      print(sentTable.body.player_id)
      rdb_common:setDataFromView(sentTable)    
    end
end

function rdb_bonus:downloadQRCode(filename)
	local EnterModule = require "ui.EnterModule"
	local entered = EnterModule:checkEntered()

	if entered then

		local headers = {}
		headers["Content-Type"] = "application/json"

		local params = {}
		params.headers = headers
		params.progress = "download"	
		params.response = {
		filename = filename,
		baseDirectory = system.DocumentsDirectory
		}	
							
		local function networkListenerPhotoGet( event )
				if ( event.isError ) then
						print( "Network error!")
				elseif ( event.phase == "ended" ) then
				end
		end                        
		 --                      
		network.request( "http://api.qrserver.com/v1/create-qr-code/?data=id"..entered.guest_id..';', "GET", 	networkListenerPhotoGet,  params )
 --   userProfile.player_id="f3dcfa41-8aa3-4538-8c65-7d80ba74a704"
    self:updatePlayerID(entered)
	end
end

return rdb_bonus