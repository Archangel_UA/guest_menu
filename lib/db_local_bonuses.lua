local loadsave = require "lib.loadsave"

local mime = require "mime"
local json = require "json"
local crypto = require "crypto"
-- local ldb = require( "lib.db_local" )

local ldb_bonus = {}

function ldb_bonus:getCurrentMenuitems(parentID)
    print("pos_id="..userProfile.SelectedRestaurantData.pos_id.." and parent_id="..parentID)
	return ldb:getRowsData("menu","mi_disable<>'true' and pos_id="..userProfile.SelectedRestaurantData.pos_id.." and parent_id="..parentID.." order by sorting")
end

function ldb_bonus:getItemName(itemID,is_group)
    local row
    --print("com_id="..userProfile.SelectedRestaurantData.com_id.." and is_group='"..tostring(is_group).."' and mi_id="..itemID)
    if itemID then
        row=ldb:getRowsData("menu","com_id="..userProfile.SelectedRestaurantData.com_id.." and is_group='"..tostring(is_group).."' and mi_id="..itemID)[1]
	    return row.mi_name
    else
        return ""
    end
end

function ldb_bonus:getParentID(itemID,is_group)
    local row
    row=ldb:getRowsData("menu","com_id="..userProfile.SelectedRestaurantData.com_id.." and is_group='"..tostring(is_group).."' and mi_id="..itemID)
    if row~=nil then
       return row.parent_id
    else
	   return 0
    end
end

function ldb_bonus:getItemAmountInOrder(itemID)
    local row

    row=ldb:getRowsData("order_items","local_ord_id="..userProfile.activeOrderID.."  and mi_id="..itemID)
    if row==nil then
        return nil
    else
        return row[1].ordi_count
    end
end


function ldb_bonus:findByName(str)
    local contd =  "mi_disable<>'true' and is_group == 'false' and pos_id="..userProfile.SelectedRestaurantData.pos_id.." and name_upper like '%".._G.utf8.upper(str).."%' order by name_upper"
    local row=ldb:getRowsData("menu", contd)
    if row==nil then
        return {}
    else
        return row
    end
end

return ldb_bonus