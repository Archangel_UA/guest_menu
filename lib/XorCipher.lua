
local _ = {}

 function _.convert( chars, dist, inv )
 return string.char( ( string.byte( chars ) - 32 + ( inv and -dist or dist ) ) % 95 + 32 )
end

--[[ function _.crypt(str,inv)
 local k = {29, 58, 93, 28, 27};
 local enc= "";
 for i=1,#str do
 if(#str-k[5] >= i or not inv)then
 for inc=0,3 do
 if(i%4 == inc)then
 enc = enc .. _.convert(string.sub(str,i,i),k[inc+1],inv);
 break;
 end
 end
 end
 end
 if(not inv)then
 for i=1,k[5] do
 enc = enc .. string.char(math.random(32,126));
 end
 end
 return enc;
end]]

 function _.crypt(str,inv)
 
 local enc= "";
 
 for i=#str-3,1,-1 do
  enc=enc..string.sub(str,i,i)
 end
 for i=#str,#str-2,-1 do
  enc=enc..string.sub(str,i,i)
 end
 return enc;
end

return _
