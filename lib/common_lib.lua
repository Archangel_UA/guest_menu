local lfs = require "lfs"

local clib = {}

function clib:DeleteTempFiles()
	

	local doc_path = system.pathForFile( "", system.TemporaryDirectory )

	for file in lfs.dir(doc_path) do
        print( "Found file: " .. file )

		-- file is the current file or directory name
		if file~="." and file~=".." then
            print( "Deleted file: " .. file )
			local results, reason = os.remove( system.pathForFile( file, system.TemporaryDirectory  ) )
		end
		
	end
end

function clib:fileExists(fileName, base)
  assert(fileName, "fileName is missing")
  local base = base or system.ResourceDirectory
  local filePath = system.pathForFile( fileName, base )
  local exists = false
 
  if (filePath) then -- file may exist. won't know until you open it
    local fileHandle = io.open( filePath, "r" )
    if (fileHandle) then -- nil if no file found
      exists = true
      io.close(fileHandle)
    end
  end
 
  return(exists)
end

function clib:replace_html_tags(str)
	str=string.gsub(str, "<p>", "" )
	str=string.gsub(str, "</p>", "" )
	str=string.gsub(str, "&nbsp;", "" )
	return str
end

function clib:crop(photo, newFileName,endWidth,endHeight)
	
    local tempGroup = display.newGroup()
    
    photo.x = display.contentCenterX
    photo.y = display.contentCenterY
    tempGroup:insert(photo)
    -- Find the bigger scale out of widht or height so it will fill in the crop
    local scale = math.max(endWidth / photo.width, endHeight / photo.height)
	--photo:scale(scale, scale)
    tempGroup.height=endHeight* display.contentScaleY
	tempGroup.width=endWidth* display.contentScaleX
    
    
    --display.save(photo, newFileName,system.DocumentsDirectory) 
    -- This object will be used as screen capture boundaries object
    -- local cropArea = display.newRect(display.contentCenterX, display.contentCenterY, endWidth, endHeight)
    -- cropArea.x     = display.contentCenterX
    -- cropArea.y     = display.contentCenterY
    -- cropArea.alpha = 0.0
    -- tempGroup:insert(cropArea)
    
    -- Now capture the crop area which the user image will be underneith

    local myCaptureImage = display.captureBounds(photo.contentBounds)
	--local myCaptureImage = display.capture()
    display.save(myCaptureImage, newFileName,system.DocumentsDirectory) 
    --myCaptureImage:removeSelf() -- Remove captured image
    --myCaptureImage = nil
   
    tempGroup:removeSelf()
    tempGroup = nil
end

function clib:copyTable(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[self:copyTable(orig_key)] = self:copyTable(orig_value)
        end
        setmetatable(copy, self:copyTable(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end

function clib.printTable( res )
     print( "PRINT TABLE...", res )
     if type(res) ~= "table" then return end
     for k,v in pairs(res) do
         print(k,v)
         if type(v) == "table" then
             for k2,v2 in pairs(v) do
                 print(" ", k2,v2)
                 if type(v2) == "table" then
                     for k3,v3 in pairs(v2) do
                         print("     ", k3,v3)
                     end
                 end                             
             end
         end
     end
end

function clib.getTextSize( params )
    local text = display.newText( params )
    local w = text.width
    local h = text.height
    text:removeSelf(); text = nil
    return w, h
end

function clib.newTextBox( box_width, box_height, text_params )
print( "... ", text_params.text )
    if not text_params.text then text_params.text = "" end
    text_params.width = box_width
    local s = text_params.text
    local w, h = clib.getTextSize( text_params )
    if h > box_height then
        if not text_params.croped then
            local factor = box_height / h
            local j = math.round( #s * factor )
            text_params.text = string.sub( s, 1, j ).."..."
            text_params.croped = true
            return clib.newTextBox( box_width, box_height, text_params )             
        else
            local j = string.find( s, "%.%.%.$" )
            if j then s = string.sub( s, 1, j-1 ) end
            j = string.find( s, "%s+%S+%s*$" )
            if not j then 
                text_params.text = string.match( s, "^%S+" ).."..."
                return display.newText( text_params )
            else
                text_params.text = string.sub( s, 1, j-1 ).."..."
                return clib.newTextBox( box_width, box_height, text_params )
            end 
        end
    else
        return display.newText( text_params )
    end
end


function clib.createRating(rating)
   -- rating=4.5
-- print("rating ",rating)
    local rating=rating/10
   
    local ratingGroup = display.newGroup()
    local star_width=10
    for i=0,4 do
        if rating>0 then
          local star_name="star"
          if rating<=(i) then
              star_name="star0"
          elseif rating>=(i+0.9) then
              star_name="star"
          elseif rating>=(i+0.6) then
              star_name="star75"
          elseif rating>=(i+0.3) then
              star_name="star50"
          else
              star_name="star25"
          end
          local star = display.newImageRect( ratingGroup,
              "icons/"..star_name..".png", star_width, star_width
          )
          star.x = (star_width+2)*(i)
          star.y = 0
        end
    end
    return ratingGroup
end

return clib


