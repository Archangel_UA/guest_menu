module(..., package.seeall)
local json = require "json"
local mime=require("mime")
local loadsave = require("lib.loadsave")
--local storyboard = require( "storyboard" )

local menuarray={}
--1 - "item\group", 2 - id, 3 - table with data, 4 - has photo, 5 - parent id
-- 3 - data:
--.photos=array of {mip_id,mip_photo_width,mip_photo_height,mip_photo_format}	
--.mainphoto={mip_id,mip_photo_width,mip_photo_height,mip_photo_format}


--��������� ������� �������
--ID,state,data,datesent,closed  --{��, ������ (��������� - �.�. ������� ��� ������),������� ������, ����� ��������,������ - ����� � �����(���� ������ ������ �������)}

--������� ������� ������ - ������ �������� - �� �����, ������ - ���,������ �������� - ����, ��������� - ���, �����- ���� �� ��������
--Data: id,ammount,price,weight,printed

--local server = {}
local onDataLoad = nil

local recommendedarray={}
local itemsneedtoget=0
local total_itemsneedtoget=0
local about_info={}
local about_photos={}

local progr_gray_line,perc_value_txt
local screen_size=display.contentWidth.."x"..display.contentHeight
local screen_size_quarter=(display.contentWidth*0.25).."x"..(display.contentHeight*0.25)
local about_loaded=false
local recommended_loaded=false
local orders_loaded=false
local DoNotShowProgLine=false

function load( filename )

    -- set default base dir if none specified

    local base = system.DocumentsDirectory
    -- create a file path for corona i/o
    local path = system.pathForFile( filename, base )
    -- will hold contents of file
    local contents
    -- io.open opens a file at path. returns nil if no file found
    local file = io.open( path, "r" )
    if file then
        -- read all contents of file into a string
        contents = file:read( "*a" )
        io.close( file )    -- close the file after using it
        --return decoded json string
        return json.decode( contents )
    else
        --or return nil if file didn't ex
        return nil
    end
end

function fileExists(fileName, base)
  assert(fileName, "fileName is missing")
  local base = base or system.ResourceDirectory
  local filePath = system.pathForFile( fileName, base )
  local exists = false
 
  if (filePath) then -- file may exist. won't know until you open it
    local fileHandle = io.open( filePath, "r" )
    if (fileHandle) then -- nil if no file found
      exists = true
      io.close(fileHandle)
    end
  end
 
  return(exists)
end


function get_authorization_string(com_id)
	
	local log_str
	if userProfile.ElectronicMenu==true then
		log_str="demorest:2014766"
	else
		if com_id==nil then
			log_str="bns"..userProfile.SelectedRestaurantData.com_id..":bns_psw"..userProfile.SelectedRestaurantData.com_id
		else
			log_str="bns"..com_id..":bns_psw"..com_id
		end
	end
	
	return "Basic " ..mime.b64 ( log_str )
end

function get_admin_authorization_string()
	--return "Basic " ..mime.b64 ( "demorest:2014766" )
	return "Basic " ..mime.b64 ( "suser:dwt35s42s4" )
end

function getconnectionstring()
	--if (_G.ServerHost=="") then
		--return "test:test@77.120.98.139:3000"
--	return "77.120.98.139:3000"
	return _G.host
	--else
	--	return _G.ServerLogin.._G.ServerPassword.."@".._G.ServerHost
	--end
end

local function loading_status()
	
	
	if itemsneedtoget==1 and about_loaded and recommended_loaded and orders_loaded then
		itemsneedtoget=0
		print("final")
	end
	if itemsneedtoget>0 or (about_loaded and recommended_loaded and orders_loaded) then
		if DoNotShowProgLine~=true then
			progr_gray_line.xScale=itemsneedtoget/total_itemsneedtoget
		end
		local val=(1-itemsneedtoget/total_itemsneedtoget)*100
		if val<0 then 
			val=0
		end
		
		if DoNotShowProgLine~=true then
			perc_value_txt.text=string.format("%0.0f",val)
			if perc_value_txt.text=="-1" then
				perc_value_txt.text="1"
			end
		end
		print(itemsneedtoget.." of "..total_itemsneedtoget)
		if itemsneedtoget==0 then
			if DoNotShowProgLine~=true then
				progr_gray_line.visible=false
			end
			onDataLoad()
			_G.ScreenEnable=true
		end	
	end
end

local function networkListenerPhotoGet( event )
	--print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        if ( event.isError ) then
                print( "Network error!")
				about_loaded=true
        elseif ( event.phase == "ended" ) then
				if event.response.filename==nil then
					about_loaded=true
				else
                print( "Download complete, total bytes transferred: " .. event.response.filename .." ".. event.bytesTransferred )
				if event.response.filename=="about_photos.json" then
					local path = system.pathForFile("about_photos.json",system.DocumentsDirectory)
					local file = io.open( path, "r" )
					local data = file:read( "*a" )	
					io.close( file )
						local headers = {}
						headers["Content-Type"] = "application/json"
						headers["Authorization"] = get_authorization_string()

						local params = {}
						params.headers = headers
						params.progress = "download"				
					local t = json.decode(data)				
					for i = 1, #t.rows do
						if t.rows[i]~=nil then
							--print(t.rows[i].abp_photo_format)
							about_photos[i]={}
							about_photos[i].about_photo_filename="ab"..t.rows[i].abp_id.."."..t.rows[i].abp_photo_format
							about_photos[i].about_photo_width=t.rows[i].abp_photo_width
							about_photos[i].about_photo_height=t.rows[i].abp_photo_height
							
							params.response = {
							filename = "ab"..t.rows[i].abp_id.."."..t.rows[i].abp_photo_format,
							baseDirectory = system.TemporaryDirectory
							}	
							network.request( "http://"..getconnectionstring().."/common/about/photo/"..t.rows[i].abp_id.."?size="..screen_size, "GET", networkListenerPhotoGet,  params )							
						end
					end				
				end
				end
        end
end

function downloadLogo(filename,com_id)
						local headers = {}
						headers["Content-Type"] = "application/json"
						headers["Authorization"] = get_authorization_string(com_id)

						local params = {}
						params.headers = headers
						params.progress = "download"	
						params.response = {
						filename = filename,--t.rows[1].about_logo_format,
						baseDirectory = system.DocumentsDirectory-- system.TemporaryDirectory
						}	
						network.request( "http://"..getconnectionstring().."/common/about/logo_pos?pos_id=247&size="..screen_size_quarter, "GET", 	networkListenerPhotoGet,  params )
						--print("http://"..getconnectionstring().."/common/about/logo?size=100x100")
end

local function networkListenerAbout( event )
        if ( event.isError ) then
                print( "Network error!")
				about_loaded=true
        elseif ( event.phase == "began" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download starting, size unknown" )
                else
                        print( "Download starting, estimated size: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "progress" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download progress: " .. event.bytesTransferred )
                else
                        print( "Download progress: " .. event.bytesTransferred .. " of estimated: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "ended" ) and (event.response.filename~=nil) then
                print( "Download complete, total bytes transferred: " .. event.response.filename .." ".. event.bytesTransferred )
				
					local path = system.pathForFile("about_res.json",system.DocumentsDirectory)
					local file = io.open( path, "r" )
					local data = file:read( "*a" )	
					io.close( file )
				
					local t = json.decode(data)				
					for i = 1, #t.rows do
						if t.rows[i]~=nil then
							about_info[t.rows[i].lang_code]={}
							about_info[t.rows[i].lang_code].about_name=t.rows[i].about_name
							about_info[t.rows[i].lang_code].about_description=t.rows[i].about_description
							about_info[t.rows[i].lang_code].about_logo_file="logo.jpg"--..t.rows[i].about_logo_format--t.rows[i].about_logo_file
							about_info[t.rows[i].lang_code].about_logo_format=t.rows[i].about_logo_format
							print(about_info[t.rows[i].lang_code].about_logo_file)
						end
					end
					
					
					--print(about_info["en"].about_description)
					--print (table.concat(about_info, ", "))  
					
					-------------�������� ���� ��������
					--print("about "..#t.rows)
					if #t.rows>0 then
--[[						local headers = {}
						headers["Content-Type"] = "application/json"
						headers["Authorization"] = get_authorization_string()

						local params = {}
						params.headers = headers
						params.progress = "download"	
						params.response = {
						filename = "logo.jpg",--t.rows[1].about_logo_format,
						baseDirectory = system.TemporaryDirectory
						}	
						network.request( "http://"..getconnectionstring().."/common/about/logo?size=100x100", "GET", 	networkListenerPhotoGet,  params )
						print("http://"..getconnectionstring().."/common/about/logo?size=100x100")]]
						downloadLogo("logo.jpg")
					end
					t=nil
					--itemsneedtoget=itemsneedtoget-1
					--print(itemsneedtoget)
					about_loaded=true
					loading_status()
				--end
        end
end

local function networkListenerItem( event ) --�������� ����� � ������� ����� � ��������� ����
        if ( event.isError ) then
                print( "Network error!")
				itemsneedtoget=itemsneedtoget-1
        elseif ( event.phase == "began" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download starting, size unknown" )
                else
                        print( "Download starting, estimated size: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "progress" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download progress: " .. event.bytesTransferred )
                else
                        print( "Download progress: " .. event.bytesTransferred .. " of estimated: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "ended" ) then
				local fn=event.response.filename
                print( "Download complete, total bytes transferred: " .. fn .." ".. event.bytesTransferred )
				local hasphoto="0"
				itemsneedtoget=itemsneedtoget-1
				local t = load(event.response.filename)
				local v_dec
				local fn_add
				local photos={}
				local mainphoto={}
				local k=0
				
				--print(screen_size)
				local headers = {}
				headers["Content-Type"] = "application/json"
				headers["Authorization"] = get_authorization_string()
				local params = {}
				params.headers = headers
				params.progress = "download"
								
				if t.menu_item_photos~=nil and fn:sub(1,1)~="g" then
					for i = 1, #t.menu_item_photos do
						if t.menu_item_photos[i]~=nil then
							if t.menu_item_photos[i].mip_is_main==false then
								fn_add="_"..t.menu_item_photos[i].mip_id
								k=k+1
								photos[k]={t.menu_item_photos[i].mip_id,t.menu_item_photos[i].mip_photo_width,t.menu_item_photos[i].mip_photo_height,t.menu_item_photos[i].mip_photo_format}
								
							else	
								fn_add=""
								mainphoto={t.menu_item_photos[i].mip_id,t.menu_item_photos[i].mip_photo_width,t.menu_item_photos[i].mip_photo_height,t.menu_item_photos[i].mip_photo_format}

								
								--small photo
								params.response = {
									filename = string.gsub( event.response.filename, ".json", fn_add.."_sm."..t.menu_item_photos[i].mip_photo_format),
									baseDirectory = system.TemporaryDirectory
								}								
								network.request( "http://"..getconnectionstring().."/dictionary/menu/photoSizeMenu/"..t.menu_item_photos[i].mip_id.."?size="..screen_size_quarter, "GET", 				networkListenerPhotoGet,  params )																
							end
							
								params.response = {
									filename = string.gsub( event.response.filename, ".json", fn_add.."."..t.menu_item_photos[i].mip_photo_format),
									baseDirectory = system.TemporaryDirectory
								}
								--full photo
								network.request( "http://"..getconnectionstring().."/dictionary/menu/photoSizeMenu/"..t.menu_item_photos[i].mip_id.."?size="..screen_size, "GET", 				networkListenerPhotoGet,  params )															
--[[						local path = system.pathForFile(string.gsub( event.response.filename, ".json", fn_add..".jpg"), system.TemporaryDirectory )
						local out = assert(io.open(path, "wb"))
						--v_dec=dec(t.menu_item_photos[1].mip_photo) 
						v_dec=mime.unb64(t.menu_item_photos[i].mip_photo) 
						out:write(v_dec)
						v_dec=nil
						assert(out:close())
						out=nil	]]
						--path=nil	
						hasphoto="1"						
						end
					end

					t.menu_item_photos=nil
				elseif t.mg_photo~=nil and fn:sub(1,1)=="g" then
					params.response = {
					filename = string.gsub( event.response.filename, ".json", "."..t.mg_photo_format),
					baseDirectory = system.TemporaryDirectory
								}
					--full photo
					network.request( "http://"..getconnectionstring().."/dictionary/menu/photoSizeGroup/"..t.mg_id.."?size="..screen_size, "GET", 				networkListenerPhotoGet,  params )				
					--small photo
					params.response = {
					filename = string.gsub( event.response.filename, ".json", "_sm."..t.mg_photo_format),
					baseDirectory = system.TemporaryDirectory
								}
					
					network.request( "http://"..getconnectionstring().."/dictionary/menu/photoSizeGroup/"..t.mg_id.."?size="..screen_size_quarter, "GET", 				networkListenerPhotoGet,  params )				
					
					mainphoto={1,t.mg_photo_width,t.mg_photo_height,t.mg_photo_format}					
					hasphoto="1"
					t.mg_photo=nil
				end
				
				if  fn:sub(1,1)=="g" then --group
					for i = 1, #menuarray do
						if t.mg_id==menuarray[i][2] and menuarray[i][1]=="group" then --��� ������ ���� ����, ��������
							menuarray[i][3]=t
							menuarray[i][3].mainphoto=mainphoto
							menuarray[i][4]=hasphoto
							menuarray[i][5]=t.mg_parent_id
						end
					end
				else  --item
					for i = 1, #menuarray do
						if t.mi_id==menuarray[i][2] and menuarray[i][1]=="item" then --��� ������ ���� ����, ��������
							menuarray[i][3]=t
							menuarray[i][3].photos=photos
							menuarray[i][3].mainphoto=mainphoto
							menuarray[i][4]=hasphoto
							menuarray[i][5]=t.mg_id
						end
					end
				end				
				photos=nil
				mainphoto=nil
				--print("rest "..itemsneedtoget)
				
				loading_status()

				t=nil
				--t = load(event.response.filename)
--[[				for i in pairs( t.menu_groups_res ) do
					print( t.menu_groups_res[i].lang_code )
				end]]
        end
end


local function networkListenerOrderDetSend( event )
        if ( event.isError ) then
                print( "Network error!")
        elseif ( event.phase == "began" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download starting, size unknown" )
                else
                        print( "Download starting, estimated size: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "progress" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download progress: " .. event.bytesTransferred )
                else
                        print( "Download progress: " .. event.bytesTransferred .. " of estimated: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "ended" ) then
                print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
        end
end

local function networkListenerOrderUpdate( event )
        if ( event.isError ) then
                print( "Network error!")
        elseif ( event.phase == "began" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download starting, size unknown" )
                else
                        print( "Download starting, estimated size: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "progress" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download progress: " .. event.bytesTransferred )
                else
                        print( "Download progress: " .. event.bytesTransferred .. " of estimated: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "ended" ) then
                print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
        end
end

local function networkListenerOrderSend( event )
        if ( event.isError ) then
                print( "Network error!")
        elseif ( event.phase == "began" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download starting, size unknown" )
                else
                        print( "Download starting, estimated size: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "progress" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download progress: " .. event.bytesTransferred )
                else
                        print( "Download progress: " .. event.bytesTransferred .. " of estimated: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "ended" ) then
				local params,jsonString
                print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
				local path = system.pathForFile("order_added.json",system.DocumentsDirectory)
				local file = io.open( path, "r" )
				local img = file:read( "*a" )	
				io.close( file )
				
				local t = json.decode(img) 
				local t_id=t.id
				if t_id~=nil then
					_G.Order[#_G.Order-1].id=t_id
					
					--POSTING ORDER DETAILS
					local cur_order_table=_G.Order[#_G.Order-1].data
					local num=#cur_order_table
					
					local date =os.date( "*t" ) 
					local dt = date.year.."-"..date.month.."-"..date.day .." "..date.hour..":"..date.min..":"..date.sec
					
					for i = 1 , num do
						--print(cur_order_table[i][3])
						 t = { 
							["com_id"] = 1,
							["mi_id"]=cur_order_table[i].id,
							["ordi_create"]=dt,
							["ord_id"]=t_id,
							["ordi_is_reject"]=false,
							["ordi_is_completed"]=false,
							["ordi_price"]=tonumber(cur_order_table[i].price),
							["ordi_sum"]=tonumber(cur_order_table[i].ammount)*tonumber(cur_order_table[i].price),
							["ordi_count"]=cur_order_table[i].ammount
						}	
						jsonString = json.encode (t)
						--print(cur_order_table[1])
						path = system.pathForFile( "order_det"..i..".json", system.DocumentsDirectory )
						--print(path)
						file = io.open( path, "w" )
						--print(file)
						file:write( jsonString )
						io.close( file )
					
						headers = {}

						headers["Content-Type"] = "application/json"
						headers["Authorization"] = get_authorization_string()
						 params = {}
						params.headers = headers

						params.body = {
							filename = "order_det"..i..".json",
							baseDirectory = system.DocumentsDirectory        
						}
						params.progress = "download"

						params.response = {
							filename = "order_item_added"..i..".json",
							baseDirectory = system.DocumentsDirectory
						}
						network.request( "http://"..getconnectionstring().."/webapi/object/order_items", "POST", networkListenerOrderDetSend,  params )
					end
					file = nil
					-------------------------------------------------------------
					
					local s=storyboard.getCurrentSceneName()
					local s1=storyboard.getScene(s )
					s1:redraw_orderid()
				end
        end
end

local function networkListener( event )
        if ( event.isError ) then
				--local alert = native.showAlert( "Network error", "Network error", { "Yes"  } )
                print( "Network error!")
				itemsneedtoget=itemsneedtoget-1
        elseif ( event.phase == "began" ) then
				--local alert = native.showAlert( "Download starting", "Download starting", { "Yes"  } )
                if event.bytesEstimated <= 0 then
                        print( "Download starting, size unknown" )
                else
                        print( "Download starting, estimated size: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "progress" ) then
				--local alert = native.showAlert( "Download progress", "Download progress", { "Yes"  } )
                if event.bytesEstimated <= 0 then
                        print( "Download progress: " .. event.bytesTransferred )
                else
                        print( "Download progress: " .. event.bytesTransferred .. " of estimated: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "ended" ) then
				--local alert = native.showAlert( "Download complete", "Download complete, total bytes transferred: " .. event.bytesTransferred, { "Yes"  } )
                print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
				local path = system.pathForFile("corona_q.json",system.DocumentsDirectory)
				local file = io.open( path, "r" )
				local img = file:read( "*a" )	
				io.close( file )
				file=nil
				local t = json.decode(img)      --view_menugroups view_menugroups
				--print(t.recordsCount) 				
				local headers = {}		
				--local alert = native.showAlert( "done", img, { "Yes"  } )
				headers["Content-Type"] = "application/json"
				headers["Authorization"] = get_authorization_string()
				local continue
				local gf
				local params = {}
				params.headers = headers
				params.progress = "download"
				itemsneedtoget=#t.rows	
				itemsneedtoget=itemsneedtoget+1 --all other views to load
				total_itemsneedtoget=itemsneedtoget
				for i = 1, #t.rows do
					--print("need to download "..t.rows[i].mg_id)
					if t.rows[i].group=="1" then
						menuarray[i]={"group",t.rows[i].mg_id,{}}
						--�������� ������ ���� ����� ���� ��� ���� � ������ ���������, �� ������ �� ����
						gf=load("group"..t.rows[i].mg_id..".json")
						continue=0
						if gf~=nil then
							--print("group"..t.rows[i].mg_id..".json"..tostring(t.rows[i].version).."--"..gf.version)
							if gf.version==tostring(t.rows[i].version) then
								continue=1
								print("skip group")
								itemsneedtoget=itemsneedtoget-1
								if gf.mg_photo==nil then
									menuarray[i][4]="0"
								else
									menuarray[i][4]="1"
								end								
								gf.mg_photo=nil
								mainphoto={1,gf.mg_photo_width,gf.mg_photo_height,gf.mg_photo_format}
--[[								gf.menu_item_photos={}
								gf.recommended={}
								gf.order_items={}]]
								menuarray[i][3]=gf
								menuarray[i][3].mainphoto=mainphoto
								--table.remove( gf, pos )
								--menuarray[i][3].menu_item_photos=nil
								menuarray[i][5]=gf.mg_parent_id
							end
						end
						gf=nil
						if continue==0 then
							params.response = {
								filename = "group"..t.rows[i].mg_id..".json",
								baseDirectory = system.DocumentsDirectory
							}				
							network.request( "http://"..getconnectionstring().."/webapi/complex/menu_groups/"..t.rows[i].mg_id.."?list=menu_groups_res", "GET",networkListenerItem,  params )
						end
						
					else
						menuarray[i]={"item",t.rows[i].mg_id,{}}
						gf=load("item"..t.rows[i].mg_id..".json")
						continue=0
						if gf~=nil then
							if gf.version==tostring(t.rows[i].version) then
								continue=1
								print("skip item")
								itemsneedtoget=itemsneedtoget-1
								if gf.menu_item_photos==nil or #gf.menu_item_photos==0 then
									menuarray[i][4]="0"
								else
									menuarray[i][4]="1"
								end
								local photos={}
								local mainphoto={}
								local k=0
								if gf.menu_item_photos~=nil then
									for i = 1, #gf.menu_item_photos do
										if gf.menu_item_photos[i].mip_is_main==false then
											k=k+1
											photos[k]={gf.menu_item_photos[i].mip_id,gf.menu_item_photos[i].mip_photo_width,gf.menu_item_photos[i].mip_photo_height,gf.menu_item_photos[i].mip_photo_format}										
										else
											mainphoto={gf.menu_item_photos[i].mip_id,gf.menu_item_photos[i].mip_photo_width,gf.menu_item_photos[i].mip_photo_height,gf.menu_item_photos[i].mip_photo_format}																				
										end
										gf.menu_item_photos[i].mip_photo=nil
									end
								end
								--print(menuarray[i][4])
								--menuarray[i][3]=gf
								menuarray[i][3]=gf
								menuarray[i][3].photos=photos
								menuarray[i][3].mainphoto=mainphoto
								--print(menuarray[i][4])
								menuarray[i][5]=gf.mg_id
								photos=nil
								mainphoto=nil
							end
						end	
						gf=nil
						if continue==0 then					
							params.response = {
								filename = "item"..t.rows[i].mg_id..".json",
								baseDirectory = system.DocumentsDirectory
							}									
							network.request( "http://"..getconnectionstring().."/webapi/complex/menu_items/"..t.rows[i].mg_id, "GET",		networkListenerItem,  params )
							
						end
					end
					
					if continue==1 then
						loading_status()
					end
				end
        end
end

local function networkListenerOrdersItems( event )
        if ( event.isError ) then
                print( "Network error!")
        elseif ( event.phase == "began" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download starting, size unknown" )
                else
                        print( "Download starting, estimated size: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "progress" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download progress: " .. event.bytesTransferred )
                else
                        print( "Download progress: " .. event.bytesTransferred .. " of estimated: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "ended" ) then
				if event.response.filename~=nil then
                print( "Download orders complete, "..event.response.filename.." total bytes transferred: " .. event.bytesTransferred )
				local path = system.pathForFile(event.response.filename,system.DocumentsDirectory)
				local file = io.open( path, "r" )
				local img = file:read( "*a" )	
				io.close( file )
				local t = json.decode(img)      --view_menugroups view_menugroups
				local order_id=t.rows[1].ord_id
				local x=1
				while _G.Order[x].id ~= order_id do
					x=x+1
				end
				
				for i = 1, #t.rows do
					_G.Order[x].data[i]={id=t.rows[i].mi_id,ammount=t.rows[i].ordi_count,price=t.rows[i].ordi_price,weight=get_item_field(t.rows[i].mi_id,"item","Weight"),printed=0}
				end
				end
        end
end

local function networkListenerOrders( event )
        if ( event.isError ) then
                print( "Network error!")
        elseif ( event.phase == "began" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download starting, size unknown" )
                else
                        print( "Download starting, estimated size: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "progress" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download progress: " .. event.bytesTransferred )
                else
                        print( "Download progress: " .. event.bytesTransferred .. " of estimated: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "ended" ) then
                print( "Download orders complete, total bytes transferred: " .. event.bytesTransferred )
				local path = system.pathForFile("ord_res.json",system.DocumentsDirectory)
				local file = io.open( path, "r" )
				local img = file:read( "*a" )	
				io.close( file )
				local t = json.decode(img)      --view_menugroups view_menugroups
				local headers = {}		
				headers["Content-Type"] = "application/json"
				headers["Authorization"] = get_authorization_string()
				local params = {}
				params.headers = headers
				params.progress = "download"
				local myQuery
				for i = 1, #t.rows do
					local ord_n=#_G.Order
					local id=_G.Order[ord_n].id
					_G.Order[ord_n].id=t.rows[i].ord_id
					_G.Order[ord_n].state=t.rows[i].ord_state
					_G.Order[ord_n].datesent=makeTimeStamp(t.rows[i].ord_create)
					_G.Order[ord_n].data={}
					
					_G.Order[ord_n+1]={id=id+1,state=0,data={}} --������ ����� �����
					
					myQuery={pageNum=1,pageSize=100000,   
					filter= {
						group= "and",
						conditions= {
						{
							left= "ord_id",
							oper= "=",
							right= t.rows[i].ord_id
						}		  
						}
					}}
					loadsave.saveTable(myQuery, "order_det"..i..".json",system.DocumentsDirectory)
					params = {}
					params.headers = headers

					params.body = {
						filename = "order_det"..i..".json",
						baseDirectory = system.DocumentsDirectory        
					}
					params.progress = "download"

					params.response = {
					filename = "ord_det_res"..i..".json",
					baseDirectory = system.DocumentsDirectory
					}
	
					network.request( "http://"..getconnectionstring().."/webapi/object/query/order_items", "POST", networkListenerOrdersItems,  params )					
				end
				orders_loaded=true
				loading_status()
        end
end

local function networkListenerRecommended( event )
        if ( event.isError ) then
                print( "Network error!")
				recommended_loaded=true
        elseif ( event.phase == "began" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download starting, size unknown" )
                else
                        print( "Download starting, estimated size: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "progress" ) then
                if event.bytesEstimated <= 0 then
                        print( "Download progress: " .. event.bytesTransferred )
                else
                        print( "Download progress: " .. event.bytesTransferred .. " of estimated: " .. event.bytesEstimated )
                end
        elseif ( event.phase == "ended" ) then
                print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
				local path = system.pathForFile("corona_rec.json",system.DocumentsDirectory)
				local file = io.open( path, "r" )
				local img = file:read( "*a" )	
				io.close( file )
				local t = json.decode(img)      --view_menugroups view_menugroups
				local headers = {}		
				headers["Content-Type"] = "application/json"
				headers["Authorization"] = get_authorization_string()
				local params = {}
				params.headers = headers
				params.progress = "download"

				for i = 1, #t.rows do
					recommendedarray[i]=t.rows[i].mi_id
				end
				recommended_loaded=true
				loading_status()
        end
end

local function testNetworkConnection()
	local socket = require("socket")
            
	local test = socket.tcp()
	test:settimeout(1000)                   -- Set timeout to 1 second
            
	local testResult = test:connect("www.google.com", 80)        -- Note that the test does not work if we put http:// in front
           
	test:close()
	test = nil
	if not(testResult == nil) then
		print("Internet access is available")
		return true
	else
		print("Internet access is not available")
		return false
	end
end

function get_last_order_sum()
	local sum=0
	for k,row in pairs(_G.Order[#_G.Order].data) do	
		sum = sum+row.ammount*row.price
	end
	
	return sum
end

function get_current_menuitems(ParrentID,onlyitems,onlywithphoto)
	local current_menuitems={}
	if onlyitems==nil then
		onlyitems=false
	end	
	if onlywithphoto==nil then
		onlywithphoto=false
	end		
	for i = 1, #menuarray do
		--print(menuarray[i][1]..menuarray[i][2].." parent "..menuarray[i][5])
		if menuarray[i][5]==ParrentID and (onlyitems==false or menuarray[i][1]=="item") then
			if onlywithphoto==false or (menuarray[i][4]== "1") then
			--print("add item "..menuarray[i][2])
				current_menuitems[#current_menuitems+1]=menuarray[i]
			end
		end
	end
	return current_menuitems
end

function get_parentID(ItemID,item_group)
	local ParentID=0
	for i = 1, #menuarray do
		--print("i "..menuarray[i][2])
		if menuarray[i][2]==ItemID and item_group==menuarray[i][1] then
			ParentID=menuarray[i][5]
			--print("p "..ParentID)
			break
		end
	end
	if ParentID==0 or ParentID==nil then
		print("Error. ItemID "..ItemID)
		return 0
	else
		--print("parent id "..ParentID)
		return ParentID
	end
end

function get_parentID_TopFolder(ItemID,item_group)
	local ParentID=ItemID
	local prevParentID
	while ParentID ~= 0 do
		prevParentID=ParentID
		ParentID=get_parentID(ParentID,item_group)
	end
	
	return prevParentID
end

function get_menuitems_from_current_folder(ItemID,item_group,onlyitems,onlywithphoto)
	local ParentID=get_parentID(ItemID,item_group)
	if ParentID==0 or ParentID==nil then
		print("Error. ItemID "..ItemID)
		return {}
	else
		--print("parent id "..ParentID)
		return get_current_menuitems(ParentID,onlyitems,onlywithphoto)
	end
end

function get_all_items(full,onlywithphoto)
	local current_menuitems={}
	if full==nil then
		full=false
	end
	--print(onlywithphoto)
	if onlywithphoto==nil then
--		print("menuarray[i][4]")
		onlywithphoto=false
	end	
	for i = 1, #menuarray do
		if (menuarray[i][1]== "item") or (full==true) then
			if onlywithphoto==false or (menuarray[i][4]== "1") then
		--		print(menuarray[i][1])
				current_menuitems[#current_menuitems+1]=menuarray[i]
			end
		end
	end
	return current_menuitems
end

function get_item_index(itemID,item_group)
	local x=1
	local num=#menuarray
	--print(item_group)
	while (x<=num) and ((menuarray[x][2] ~= itemID) or (menuarray[x][1] ~= item_group)) do
	--	print(itemID.."   "..menuarray[x][2])
		x=x+1
	end
	if (x==#menuarray) and (menuarray[x][2] ~= itemID) then
		return 0
	end
	return x
end

function replace_html_tags(str)
	str=string.gsub(str, "<p>", "" )
	str=string.gsub(str, "</p>", "" )
	str=string.gsub(str, "&nbsp;", "" )
	return str
end

function get_item_name(itemID,item_group,returndescription)
	--print(item_group)
	local x=get_item_index(itemID,item_group)
	if x==0 then
		return "Error. no name"
	end	
	if menuarray[x][1] == "group" then
		if menuarray[x][3].menu_groups_res~=nil then
			for i = 1, #menuarray[x][3].menu_groups_res do
				print("lang"..menuarray[x][3].menu_groups_res[i].lang_code)
				if menuarray[x][3].menu_groups_res[i].lang_code==_G.Lang then
					if returndescription==false then
						if menuarray[x][3].menu_groups_res[i].mg_name~=nil then
							return menuarray[x][3].menu_groups_res[i].mg_name
						else
							return ""
						end
					else
						if menuarray[x][3].menu_groups_res[i].mg_description~=nil then
							return replace_html_tags(menuarray[x][3].menu_groups_res[i].mg_description)
						else
							return ""
						end
					end
				end
			end
			
		end
	else
		if menuarray[x][3].menu_items_res~=nil then
			for i = 1, #menuarray[x][3].menu_items_res do
				--print("lang"..item[3].menu_groups_res[i].lang_code)
				if menuarray[x][3].menu_items_res[i].lang_code==_G.Lang then
					if returndescription==false then
						if menuarray[x][3].menu_items_res[i].mi_name~=nil then
							return menuarray[x][3].menu_items_res[i].mi_name
						else
							return ""
						end
					else
						if menuarray[x][3].menu_items_res[i].mi_description~=nil then
							return replace_html_tags(menuarray[x][3].menu_items_res[i].mi_description)
						else
							return ""
						end
					end
				end
			end
		end			
	end	
	return ""
end

function get_item_field(itemID,item_group,field) --�������� �������� ��������� ��� ��������
	local x=get_item_index(itemID,item_group)
	--print(item_group)
	if x==0 then
		return "Error. no name"
	end	
	if menuarray[x][1] == "group" then
		return 0;
	else
		if field=="Price" then
			if menuarray[x][3].mi_price~=nil then
				return menuarray[x][3].mi_price;
			else
				return 0;
			end
		elseif field=="Weight" then
			if menuarray[x][3].mi_weight~=nil then
				return menuarray[x][3].mi_weight;
			else
				return 0;
			end
		end
	end
end

function get_about_info(field) --�������� �������� ��������� ��� about
	print(field)
	return about_info[_G.Lang][field];
end

function get_recommended_array()
	return recommendedarray
end

function get_about_photos()
	return about_photos
end

function init_menu(f_menushow_func,progr_gray,perc_txt,vDoNotShowProgLine)
	if testNetworkConnection()==false then
		print("No internet connection")
		return
	end
	_G.ScreenEnable=false
	DoNotShowProgLine=vDoNotShowProgLine
	onDataLoad=f_menushow_func
	progr_gray_line = progr_gray

	perc_value_txt = perc_txt
	
	itemsneedtoget=0
	menuarray={}
	
	
	local headers = {}

	headers["Content-Type"] = "application/json"
	headers["Authorization"] = get_authorization_string()
	
	local myQuery={pageNum=1,pageSize=100000}
	loadsave.saveTable(myQuery, "q.json",system.DocumentsDirectory)
	
	--local jsonString = json.encode( myQuery )
	local path = system.pathForFile("q.json")
	--local alert = native.showAlert( "done1", path, { "Yes"  } )
	--local alert = native.showAlert( "done2", getconnectionstring(), { "Yes"  } )
	
	local params = {}
	params.headers = headers

	params.body = {
        filename = "q.json",
		--user={username="test", password="test"},
        baseDirectory = system.DocumentsDirectory        
        }
-- This tells network.request() that we want the 'began' and 'progress' events...
	params.progress = "download"

-- This tells network.request() that we want the output to go to a file...
	params.response = {
        filename = "corona_q.json",
        baseDirectory = system.DocumentsDirectory
        }
		--system.DocumentsDirectory
	--local alert = native.showAlert( "before request", "before request", { "Yes"  } )
	network.request( "http://"..getconnectionstring().."/webapi/object/query/vw_menugroups", "POST", networkListener,  params )	
	
	
	-------------��������� �������������
	
	myQuery={pageNum=1,pageSize=100000}
	loadsave.saveTable(myQuery, "rec_q.json",system.DocumentsDirectory)
	
	params = {}
	params.headers = headers

	params.body = {
        filename = "rec_q.json",
        baseDirectory = system.DocumentsDirectory        
        }
	params.progress = "download"

-- This tells network.request() that we want the output to go to a file...
	params.response = {
        filename = "corona_rec.json",
        baseDirectory = system.DocumentsDirectory
        }
		--system.DocumentsDirectory
	
	network.request( "http://"..getconnectionstring().."/webapi/object/query/recommended", "POST", networkListenerRecommended,  params )	

	-------------��������� �������� ���� - ������ ���� ���������, ������, �.�.
	myQuery={pageNum=1,pageSize=100000}
	loadsave.saveTable(myQuery, "menutypes_q.json",system.DocumentsDirectory)
	
	params = {}
	params.headers = headers

	params.body = {
        filename = "menutypes_q.json",
        baseDirectory = system.DocumentsDirectory        
        }
	params.progress = "download"

-- This tells network.request() that we want the output to go to a file...
	params.response = {
        filename = "menutypes_res.json",
        baseDirectory = system.DocumentsDirectory
        }
		--system.DocumentsDirectory
	
	network.request( "http://"..getconnectionstring().."/webapi/object/query/list_menu", "POST", networkListenerOrderDetSend,  params )	
	
	-------------��������� �������� ������
	local deviceID = system.getInfo( "deviceID" )
	myQuery={pageNum=1,pageSize=100000,   
	filter= {
      group= "and",
      conditions= {
 
		  {
              left= "deviceid",
              oper= "=",
              right= "'"..deviceID.."'"
          },		  
		  {
              left= "ord_state",
              oper= "<>",
              right= "3"
          }		  
      }
	}}
	loadsave.saveTable(myQuery, "ord_q.json",system.DocumentsDirectory)
	
	params = {}
	params.headers = headers

	params.body = {
        filename = "ord_q.json",
        baseDirectory = system.DocumentsDirectory        
        }
	params.progress = "download"

	params.response = {
        filename = "ord_res.json",
        baseDirectory = system.DocumentsDirectory
        }
	
	network.request( "http://"..getconnectionstring().."/webapi/object/query/orders", "POST", networkListenerOrders,  params )		
	
	
		-------------��������� ������� � ��������
	myQuery={pageNum=1,pageSize=100000}
	loadsave.saveTable(myQuery, "about_body.json",system.DocumentsDirectory)
	local headers = {}
	headers["Content-Type"] = "application/json"
	headers["Authorization"] = get_authorization_string()
	
	params = {}
	params.headers = headers

	params.body = {
        filename = "about_body.json",
        baseDirectory = system.DocumentsDirectory        
        }
	params.progress = "download"

-- This tells network.request() that we want the output to go to a file...
	params.response = {
        filename = "about_res.json",
        baseDirectory = system.DocumentsDirectory
        }
		--system.DocumentsDirectory
	
	network.request( "http://"..getconnectionstring().."/webapi/object/query/vw_about_info", "POST", networkListenerAbout,  params )	
	
		-------------��������� ������� ����� � ��������
	loadsave.saveTable(myQuery, "about_photos_body.json",system.DocumentsDirectory)
	params = {}
	params.headers = headers

	params.body = {
        filename = "about_photos_body.json",
        baseDirectory = system.DocumentsDirectory        
        }
	params.progress = "download"		
	params.response = {
        filename = "about_photos.json",
        baseDirectory = system.DocumentsDirectory
        }
		--system.DocumentsDirectory
	
	network.request( "http://"..getconnectionstring().."/webapi/object/query/vw_about_photos", "POST", networkListenerPhotoGet,  params )	
	

	
end

function makeTimeStamp(dateString)
	local pattern = "(%d+)%-(%d+)%-(%d+)%a(%d+)%:(%d+)%:([%d%.]+)([Z%p])(%d*)%:?(%d*)";
	local year, month, day, hour, minute, seconds, tzoffset, offsethour, offsetmin = dateString:match(pattern);
	print(year)
	local timestamp = os.time({year=year, month=month, day=day, hour=hour, min=minute, sec=seconds});
	local offset = 0;
 
	if (tzoffset) then
		if ((tzoffset == "+") or (tzoffset == "-")) then  -- we have a timezone!
			offset = offsethour * 60 + offsetmin;
			
			if (tzoffset == "-") then
			  offset = offset * -1;
			end
			
			timestamp = timestamp + offset;
		end
	end
	print("ok")
	return timestamp;
	
end

function send_order()
	if testNetworkConnection()==false then
		print("No internet connection")
		return
	end
	local date = os.date( "*t" ) 
	_G.Order[#_G.Order].state=1 --������ ������� ��� ����� ���������
	_G.Order[#_G.Order].datesent=os.time()
	local id=_G.Order[#_G.Order].id
	_G.Order[#_G.Order+1]={id=id+1,state=0,data={}} --������ ����� �����
	
-- Lua script:
	local deviceID = system.getInfo( "deviceID" )

	
	local dt = date.year.."-"..date.month.."-"..date.day .." "..date.hour..":"..date.min..":"..date.sec--YYYY-MM-DD HH:MM:SS
	--print(dt)
	local t = { 
		["com_id"] = 1,
		["t_id"]=1,--tonumber(_G.TableNumber),
		["ord_create"]=dt,
		["ord_delivery_type"]=0,
		["ord_state"]=1,
		["deviceid"]=deviceID,
		["ord_is_reject"]=false,
		
	}	
	local jsonString = json.encode (t)
    local path = system.pathForFile( "order.json", system.DocumentsDirectory )
    local file = io.open( path, "w" )
    file:write( jsonString )
	io.close( file )
    file = nil
	

	local headers = {}

	headers["Content-Type"] = "application/json"
	headers["Authorization"] = get_authorization_string()
	local params = {}
	params.headers = headers

	params.body = {
        filename = "order.json",
        baseDirectory = system.DocumentsDirectory        
        }
-- This tells network.request() that we want the 'began' and 'progress' events...
	params.progress = "download"

-- This tells network.request() that we want the output to go to a file...
	params.response = {
        filename = "order_added.json",
        baseDirectory = system.DocumentsDirectory
        }
		--system.DocumentsDirectory
	network.request( "http://"..getconnectionstring().."/webapi/object/orders", "POST", networkListenerOrderSend,  params )

end

function update_order(orderindex,action_type)
	if testNetworkConnection()==false then
		print("No internet connection")
		return
	end
	_G.Order[orderindex].state=action_type --������ ������� ��������� ������
	local t = {}
	if action_type==3 then --order is comleted and closed
		local date = os.date( "*t" ) 
		local dt = date.year.."-"..date.month.."-"..date.day .." "..date.hour..":"..date.min..":"..date.sec--YYYY-MM-DD HH:MM:SS		
		t = { 
			orders= {ord_id=_G.Order[orderindex].id,ord_state=action_type,ord_is_completed=true,ord_closed=dt}
		}		
	else
		t = { 
			orders= {ord_id=_G.Order[orderindex].id,ord_state=action_type}
		}	
	end
	local jsonString = json.encode (t)
    local path = system.pathForFile( "order.json", system.DocumentsDirectory )
    local file = io.open( path, "w" )
    file:write( jsonString )
	io.close( file )
    file = nil
	

	local headers = {}

	headers["Content-Type"] = "application/json"
	headers["Authorization"] = get_authorization_string()
	local params = {}
	params.headers = headers

	params.body = {
        filename = "order.json",
        baseDirectory = system.DocumentsDirectory        
        }
-- This tells network.request() that we want the 'began' and 'progress' events...
	params.progress = "download"

-- This tells network.request() that we want the output to go to a file...
	params.response = {
        filename = "order_added.json",
        baseDirectory = system.DocumentsDirectory
        }
		--system.DocumentsDirectory
	network.request( "http://"..getconnectionstring().."/webapi/object/orders", "PUT", networkListenerOrderUpdate,  params )

end


function send_order_rating(order_index,Interior,Cuisine,Service)
	if testNetworkConnection()==false then
		print("No internet connection")
		return
	end
	_G.Order[order_index].state=1 


	local date = os.date( "*t" ) 
	local dt = date.year.."-"..date.month.."-"..date.day .." "..date.hour..":"..date.min..":"..date.sec--YYYY-MM-DD HH:MM:SS
	--print(dt)
	local t = { 
		["com_id"] = 1,
		["ord_id"]=_G.Order[order_index].id,--tonumber(_G.TableNumber),
		["ordrat_datetime"]=dt,
		["ordrat_index1"]=Interior,
		["ordrat_index2"]=Cuisine,
		["ordrat_index3"]=Service
	}	
	local jsonString = json.encode (t)
    local path = system.pathForFile( "order_rat.json", system.DocumentsDirectory )
    local file = io.open( path, "w" )
    file:write( jsonString )
	io.close( file )
    file = nil
	

	local headers = {}

	headers["Content-Type"] = "application/json"
	headers["Authorization"] = get_authorization_string()
	local params = {}
	params.headers = headers

	params.body = {
        filename = "order_rat.json",
        baseDirectory = system.DocumentsDirectory        
        }
-- This tells network.request() that we want the 'began' and 'progress' events...
	params.progress = "download"

-- This tells network.request() that we want the output to go to a file...
	params.response = {
        filename = "order_rat_added.json",
        baseDirectory = system.DocumentsDirectory
        }
		--system.DocumentsDirectory
	network.request( "http://"..getconnectionstring().."/webapi/object/orders_rating", "POST", networkListenerOrderUpdate,  params )

end

local function networkListenerDefault(event)
    print ("loadData...:>>>>>" )
    if ( event.isError ) then
            --local alert = native.showAlert( "Network error", "Network error", { "Yes"  } )
            print( "Network error!")
            onDataLoad({result = "error"})
    elseif ( event.phase == "began" ) then
            --local alert = native.showAlert( "Download starting", "Download starting", { "Yes"  } )
            if event.bytesEstimated <= 0 then
                print( "Download starting, size unknown" )
            else
                print( "Download starting, estimated size: " .. event.bytesEstimated )
            end
    elseif ( event.phase == "progress" ) then
            --local alert = native.showAlert( "Download progress", "Download progress", { "Yes"  } )
            if event.bytesEstimated <= 0 then
                print( "Download progress: " .. event.bytesTransferred )
            else
                print( "Download progress: " .. event.bytesTransferred .. " of estimated: " .. event.bytesEstimated )
            end
    elseif ( event.phase == "ended" ) then
            print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
            print(event.response.filename, event.response)
			if event.response.filename~=nil then 
				local path = system.pathForFile(event.response.filename,system.DocumentsDirectory ) 
				local file = io.open( path, "r" )
				local output = file:read( "*a" )    
				io.close( file )
				file=nil
				local t = json.decode(output)
				onDataLoad({result = "completed",data=t, rawData = output})
			end
    end

end

function getDataFromView(callback,ViewName)
	if testNetworkConnection()==false then
		print("No internet connection")
		return
	end

	local url = "http://"..getconnectionstring().."/webapi/object/query/vw_bonus_allcompanies"
    local headers = {}
    onDataLoad = callback
    headers["Content-Type"] = "application/json"
    headers["Authorization"] = get_admin_authorization_string()
    
    local myQuery={pageNum=1,pageSize=100000}
    loadsave.saveTable(myQuery, ViewName..".json",system.DocumentsDirectory)
    
    local params = {}
    params.headers = headers

    params.body = {
        filename = ViewName..".json",
        baseDirectory = system.DocumentsDirectory        
    }
    params.progress = "download"

    params.response = {
        filename = ViewName.."_res.json",
        baseDirectory = system.DocumentsDirectory
    }
    print ("send request...")
    network.request(url,"POST", networkListenerDefault, params)
end

--return server