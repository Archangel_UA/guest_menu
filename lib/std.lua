--MY LIBRARY OF STANDARD METHODS

----------------------------------------------------------------------------------

local physics = require "physics"

_X = display.contentWidth
_Y = display.contentHeight
_mX = display.contentWidth*0.5    -- middle _X
_mY = display.contentHeight*0.5   -- middle _Y

_Xa = display.actualContentWidth
_Ya = display.actualContentHeight
_mXa = display.actualContentWidth*0.5
_mYa = display.actualContentHeight*0.5

local _print = print
-- print = function( ... )
-- 	local info = debug.getinfo( 2, "nSl" )
-- 	local fname = info.name and " ("..info.name..")" or ""
-- 	local source = string.match( info.source, "/[%w%_%-%.%s]+/[%w%_%-%s]+/[%w%_%-%.%s]+$" )
-- 	source = source or info.source
-- 	_print( "@.."..source.." ["..info.currentline.."]"..fname..": ", unpack( arg ) )
-- end

local std = {
	cX = 0,
	cY = 0,

	dsX = 0,
	dsY = 0,

	ds = function() end,

	sortNoNil = function( table, quanonilval ) end,
	-- sort table (array) to delete nil among the data, for example:
	--[[		  before
			tab[1] = "a"                              tab[1] = "a"     
			tab[2] = nil 							  tab[2] = "b"
			tab[3] = nil   std.sortNoNil( tab, 2 )    tab[3] = "c"
			tab[4] = "b" 							  tab[4] = nil
			tab[5] = "c"                              tab[5] = nil
	  ]]		
	-- use only for sort static array (not changing other parts of the code during the sorting process)		

	randdelay = function( frequency ) end,
	
	dragPhysicObject = function( bodyobj ) end,

	printTap = function() end,

	getPolygonRectan = function() end,

	bodyScreenBounds = function( type, ... ) end,

	checkOutOfScreen = function ( object ) end,

	alignTop = function( displayobject ) end,
	alignBottom = function( displayobject ) end,
	alignLeft = function( displayobject ) end,
	alignRight = function( displayobject ) end,
	alignCenter = function( displayobject ) end,

	dragObject = function( event ) end,

	grid = function( xstep, ystep ) end,

	printProps = function( displayobject ) end,

	addDebugTouch = function( displayobject ) end,

	crypt = function( str, k, inv ) end,
	-- !!! NOT SUPPORT RUSSIAN LANGUAGE !!!
	-- local enc1 = {29, 58, 93, 28, 27};
	-- local str = "This is an encrypted string.";
	-- local crypted = crypt(str,enc1)
	-- print("Encryption: " .. crypted);
	-- print("Decryption: " .. crypt(crypted,enc1,true));
		-- returns: 
		-- Encryption: /f&1Zg0=<l<#Ia/7Kr""Zq10Dl$KT)8b>%*JbV>Z^Ik!p=B@'7cHMY<
		-- Decryption: This is an encrypted string.

	realpos = function( display_object )end,

	printtable = function( t --[[, pad]] )end,	 
	tprint = function( t --[[, pad]] )end,

	gettextsize = function( text, font, fontsize, width, height ) end,
}

function std.getOS()	-- ios, android, simulator
	local os = ""
	if system.getInfo( "environment" ) == "device" then
		os = system.getInfo( "platformName" ) == "Android" and "android" or "ios"
	else
		os = "simulator" 			
	end
	return os
end

function std.checkIsTablet( limit )
	limit = limit or 6
	local model = system.getInfo( "model" )
	local model_cases = {
		["iPad"] = true,
		["Galaxy Tab"] = true,
		["BNTV250"] = true,
		["iPhone"] = false,
	}
	if model_cases[model] ~= nil then return model_cases[model] end
	local iwidth = system.getInfo( "androidDisplayWidthInInches" )
	local iheight = system.getInfo( "androidDisplayHeightInInches" )
	if not iwidth or not iheight then return false end
	local diagonal = math.sqrt( iwidth^2 + iheight^2 )
	if diagonal > limit then return true 
	else return false
	end
end

function std:memUsage( delay, print_to_console )
	delay = delay or 3000

	local displ = display.newGroup()

	local label = display.newText( displ, "MEMORY USAGE:", 0, 0, native.systemFont, 8 )

	local ram_label = display.newText( displ, " RAM = ", 0, 0, native.systemFont, 8 )
	ram_label.x = label.x - label.width*0.5 + ram_label.width*0.5
	ram_label.y =label.y + label.height

	local texture_label = display.newText( displ, " TEXTURE = ", 0, 0, native.systemFont, 8 )
	texture_label.x = label.x - label.width*0.5 + texture_label.width*0.5
	texture_label.y =ram_label.y + ram_label.height

	displ.x = displ.width*0.5

	timer.performWithDelay( delay, function( event )
		collectgarbage("collect")
		local mem_str = string.format( "%.3f KB", collectgarbage( "count" ) )
		local texture_str = string.format( "%.3f MB", (system.getInfo("textureMemoryUsed")/1048576) )
		local texture_lim = " (LIMIT = "..system.getInfo( "maxTextureSize" )..")"
		
		ram_label.text = "  RAM = "..mem_str
		ram_label.x = label.x - label.width*0.5 + ram_label.width*0.5
		texture_label.text = "  TEXTURE = "..texture_str..texture_lim
		texture_label.x = label.x - label.width*0.5 + texture_label.width*0.5
		
		if print_to_console then
			print( "MEMORY ="..mem_str .. " | TEXTURE = "..texture_str..texture_lim )
		end
		displ:toFront()
	end, 10000 )
end

function std:gettextsize( text, font, fontsize, width, height )
	local cotext = display.newText({
		text = text,
		font = font,
		fontSize = fontsize,
		width = width,
		height = height
	})
	local w = cotext.width
	local h = cotext.height
	cotext:removeSelf()
	cotext = nil
	return w, h
end

function std:printtable( t, pad, limit )
	if not t then 
		return print( "STD WARNING: in func 'printtable' expected table, get "..tostring( t ) ) 
	end

	pad = pad or "  "
	limit = limit or 3
	if limit <= 0 then return end
	
	local function printpair( key, value, pad )
		if type( value ) == "table" then
			_print( pad.."["..key.."] = "..tostring( value ).." :" )
			self:printtable( value, pad.."  ", limit-1 )
		elseif type( value ) == "string" then
			_print( pad.."["..key.."] = \""..value.."\";" )
		else _print( pad.."["..key.."] = "..tostring( value )..";" )
		end
	end

	for i=1, #t do printpair( i, t[i], pad ) end
	for k,v in pairs( t ) do
		if type( k ) == "number" then
			if k > #t then printpair( k, v, pad ) end
		else printpair( k, v, pad )	 
		end
	end
end

function std:tprint( t, pad )
	self:printtable( t, pad )
end

function std:realpos( dispobj )
	local bounds = dispobj.contentBounds
	local realx = bounds.xMin + dispobj.width*dispobj.anchorX
	local realy = bounds.yMin + dispobj.height*dispobj.anchorY
	return realx, realy
end

function std:crypt( str, k, inv )
	local function convert( chars, dist, inv )
	    return string.char( ( string.byte( chars ) - 32 + ( inv and -dist or dist ) ) % 95 + 32 )
	end    

    local enc = ""
    for i=1,#str do
        if(#str-k[5] >= i or not inv)then
            for inc=0,3 do
                if(i%4 == inc)then
                    enc = enc .. convert(string.sub(str,i,i),k[inc+1],inv)
                    break
                end
            end
        end
    end
    if(not inv)then
        for i=1,k[5] do
            enc = enc .. string.char(math.random(32,126))
        end
    end
    return enc	
end

function std:addDebugTouch( dispobj )
	local function listener( event )
		self:touch( event )
		if event.phase == "ended" then
			self:printProps( event.target )
		end
	end

	if not dispobj.numChildren then
		dispobj:addEventListener( "touch", listener )
	elseif dispobj.numChildren == 0 then
		error("std:addDebugTouch: argument must be DisplayObject or DisplayGroup with objecys inside")
	else
		for i = 1, dispobj.numChildren do
			dispobj[i]:addEventListener( "touch", listener )
		end
	end
end

function std:printProps( dispobj )
	print( "Display object ", dispobj, " :" )
	print( "   position x:", dispobj.x )
	print( "   position y:", dispobj.y )
	print( "   width:", dispobj.width )
	print( "   height:", dispobj.height )
	print( "   rotation:", dispobj.x )
	print( "   anchorX:", dispobj.anchorX )
	print( "   anchorY:", dispobj.anchorY )
	print( "   is physics body:", dispobj.isBodyActive )
	print( "   parent:", dispobj.parent )
	print( "-------------------------------------------" )
end

function std:grid( xstep, ystep, alpha, params )
	local X, Y = _X, _Y
	params = params or {}
	if params.actual then X, Y = _Xa, _Ya end
	if params.info then
		local info = display.newText({
			x = 3, y = 3,
			width = _Xa*0.4,
			text = "",
			font = native.systemFont, 
			fontSize = 10,
			align = "left"
		})
		info:setFillColor( 1,1,1, 0.7 )
		info.anchorX = 0
		info.anchorY = 0
		info.text = "content: ".._X.." x ".._Y.."\nactual: ".._Xa.." x ".._Ya.."\npixel: "..display.pixelWidth.." x "..display.pixelHeight					
		
		local box = display.newRect( 0, 0, info.width+3, info.height+5 )
		box.anchorX = 0
		box.anchorY = 0
		box:setFillColor( 0,0,0, 0.7 )

		info:toFront()	
	end

	local lane, upnote, downnote
	alpha = alpha or 0.5
	local vertlines = math.floor( X/xstep )
	for i = 1, vertlines do
		lane = display.newLine( xstep*i, 0, xstep*i, Y )
		lane:setStrokeColor( 0.4, alpha )
		upnote = display.newText( xstep*i, xstep*i, 5, native.systemFont, 10 )
		upnote:setFillColor( 0.5, alpha )
		downnote = display.newText( xstep*i, xstep*i, Y-5, native.systemFont, 10 )
		downnote:setFillColor( 0.5, alpha )
	end

	local horlines = math.floor( Y/ystep )
	for i = 1, horlines do
		lane = display.newLine( 0, ystep*i, X, ystep*i )
		lane:setStrokeColor( 0.4, alpha )
		upnote = display.newText( ystep*i, 10, ystep*i, native.systemFont, 10 )
		upnote:setFillColor( 0.5, alpha )
		downnote = display.newText( ystep*i, X-5, ystep*i, native.systemFont, 10 )
		downnote:setFillColor( 0.5, alpha )
	end
end

function std:touch( event )
	local t = event.target

	local phase = event.phase
	if "began" == phase then
		-- Make target the top-most object
		local parent = t.parent
		parent:insert( t )
		display.getCurrentStage():setFocus( t )

		-- Spurious events can be sent to the target, e.g. the user presses 
		-- elsewhere on the screen and then moves the finger over the target.
		-- To prevent this, we add this flag. Only when it's true will "move"
		-- events be sent to the target.
		t.isFocus = true

		-- Store initial position
		t.x0 = event.x - t.x
		t.y0 = event.y - t.y
	elseif t.isFocus then
		if "moved" == phase then
			-- Make object move (we subtract t.x0,t.y0 so that moves are
			-- relative to initial grab point, rather than object "snapping").
			t.x = event.x - t.x0
			t.y = event.y - t.y0
		elseif "ended" == phase or "cancelled" == phase then
			display.getCurrentStage():setFocus( nil )
			t.isFocus = false
		end
	end

	-- Important to return true. This tells the system that the event
	-- should not be propagated to listeners of any objects underneath.
	return true
end

function std:alignTop( dispobj, secside )
	if secside == "left" then
		dispobj.anchorY = 0
	    dispobj.anchorX = 0
	    dispobj.x = 0
	    dispobj.y = 0
	elseif secside == "right" then
		dispobj.anchorY = 0
	    dispobj.anchorX = 1
	    dispobj.x = _X
	    dispobj.y = 0
	else
		dispobj.anchorY = 0
	    dispobj.anchorX = 0.5
	    dispobj.x = _mX
	    dispobj.y = 0
	end
	
	dispobj.anchorY = 0.5
	dispobj.anchorX = 0.5	
end

function std:alignBottom( dispobj, secside )
	if secside == "left" then
		dispobj.anchorY = 1
	    dispobj.anchorX = 0
	    dispobj.x = 0
	    dispobj.y = _Y
	elseif secside == "right" then
		dispobj.anchorY = 1
	    dispobj.anchorX = 1
	    dispobj.x = _X
	    dispobj.y = _Y
	else
		dispobj.anchorY = 1
	    dispobj.anchorX = 0.5
	    dispobj.x = _mX
	    dispobj.y = _Y
	end
	
	-- dispobj.anchorY = 0.5
	-- dispobj.anchorX = 0.5
end

function std:ds()
	local platformName = system.getInfo( "platformName" )
	local x, y

	if platformName == "Win" or platformName == "Mac OS X" then
		x = display.actualContentWidth
		y = display.actualContentHeight
	elseif platformName == "Android" then
		x = system.getInfo( "androidDisplayXDpi" )
		y = system.getInfo( "androidDisplayYDpi" )
	else
		x = display.actualContentWidth
		y = display.actualContentHeight
	end

	return x, y
end
std.dsX, std.dsY = std:ds()
std.cX, std.cY = std.dsX*0.5, std.dsY*0.5

function std:sortNoNil( table, quanonilval )
	local stop = quanonilval
	local vals = 0
	local nilgebeg = 0
	local i = 0
			
	if not table or not quanonilval or quanonilval == 0 then
		return ( print "std.sortNoNil() -- Error: wrong argument!" )
	end
	while i < stop do 
		i = i + 1                                   
		if vals ~= quanonilval then
			if not table[ i ] then 
				stop = stop + 1                      
				if nilgebeg == 0 then nilgebeg = i 
				end
			else vals = vals + 1                   
				if nilgebeg ~= 0 then 
					table[ nilgebeg ] = table[ i ]
					table[ i ] = nil
					nilgebeg = nilgebeg + 1
				end 
			end
		else break
		end
	end
end

function std:randdelay( frequency )
	if not inter then inter = 0 end
	inter = inter + 1

	local rand = math.random()

	if os.time() ~= lasttime then 
    	lasttime = os.time()
    	inter = 1
    elseif inter > frequency then
    	rand = nil 
    end
    
    return rand
end

function std:dragPhysicObject( event )
	local body = event.target
	local phase = event.phase
	local stage = display.getCurrentStage()

	if "began" == phase then
		stage:setFocus( body, event.id )
		body.isFocus = true

		body.tempJoint = physics.newJoint( "touch", body, event.x, event.y )
	elseif body.isFocus then
		if "moved" == phase then
			
			-- Update the joint to track the touch
			body.tempJoint:setTarget( event.x, event.y )

		elseif "ended" == phase or "cancelled" == phase then
			stage:setFocus( body, nil )
			body.isFocus = false
				
			-- Remove the joint when the touch ends			
			body.tempJoint:removeSelf()
				
		end
	end
	-- Stop further propagation of touch event
	return true
end

function std:printTap(  )
	
	function listnPrintTap( event )
		print()
		print( "--- Debug message:" )
		print( " tap was in place: x =", event.x, "y =", event.y )
		print( "---" ) 
	end
	Runtime:addEventListener( "tap", listnPrintTap )	
	
end

function std:getPolygonRectan( body )
	local vertex = {
		x1, y1,
		x2, y2,
		x3, y3,
		x4, y4
	}
	local W = body.contentWidth
	local H = body.contentHeight
	
	x1, x4 = 0, 0
	x2, x3 = W-10, W-10
	y1, y2 = -H/2+2, -H/2+2
	y4, y3 = H/2-2, H/2-2

	return { x1,y1, x2,y2, x3,y3, x4,y4 }
end

function std:bodyScreenBounds( type, ... )
	--physics.start()
		
	local bounds = display.newGroup()
	local left = display.newRect( bounds, 0, _mY, -1, _Y )
	left.name = "screen_bounds_left"
	left.isVisible = false
	local right = display.newRect( bounds, _X, _mY, 1, _Y )
	right.name = "screen_bounds_right"
	right.isVisible = false
	local top = display.newRect( bounds, _mX, 0, _X, -1 )
	top.name = "screen_bounds_top"
	top.isVisible = false
	local bottom = display.newRect( bounds, _mX, _Y, _X, 1 )
	bottom.name = "screen_bounds_bottom"
	bottom.isVisible = false
	--group:insert( bounds )


	if type == "sensor" then
		physics.addBody( left, "static", { isSensor = true, filter = edge_filter } )
		physics.addBody( right, "static", { isSensor = true, filter = edge_filter } )
		physics.addBody( top, "static", { isSensor = true, filter = edge_filter } )
		physics.addBody( bottom, "static", { isSensor = true, filter = edge_filter } )
	elseif type == "static" then
		physics.addBody( left, "static", arg[1] )
		physics.addBody( right, "static", arg[1] )
		physics.addBody( top, "static", arg[1] )
		physics.addBody( bottom, "static", arg[1] )
	else error( "ERROR: Bad arguments in function 'std:bodyScreenBounds'. Should be 'sensor' or 'static' " ) 
	end

	return bounds
end

function std:checkOutOffScreen( object )
	local outofscreen = false
	local left = object.width * ( object.anchorX - 1 )
	local right = _Xa + object.width * object.anchorX
	local top = object.height * ( object.anchorY - 1 )
	local bottom = _Ya + object.height * object.anchorY
	
	if not object then  
    	error( "ERROR: Bad arguments in function 'std:interScreen'. Not should be a 'nil'." ) 
	end

    if object.x < left then outofscreen = true end
    if object.x > right then outofscreen = true end
    if object.y < top then outofscreen = true end
    if object.y > bottom then outofscreen = true end

    return outofscreen	
end

_LABXLIB_PRINT_PATH_SIZE = 1

_default_print = print

local _print = print
local function print( ... )
	if _LABXLIB_ENABLE_DEFAULT_PRINT then return _print( unpack( arg ) ) end
	local time = system.getTimer()
	local info = debug.getinfo( 2, "nSl" )
	local fname = info.name and " ("..info.name..")" or ""
	local source = nil
	local path = {}
	local count = 0
	for s in string.gmatch( info.source, "[^/]+" ) do
		path[#path+1] = s
	end
	local limit = #path-_LABXLIB_PRINT_PATH_SIZE + 1
	limit = limit <= 0 and 1 or limit 
	for i=#path, limit, -1 do 
		source = "/"..path[i]..( source or "" ) 
	end

	source = source or info.source
	local mess = ""
	for i=1, #(arg or {}) do 
		mess = mess..tostring( arg[i] )
		if i < #arg then mess = mess.."  " end
	end
	_print( time.."@.."..source.." ["..info.currentline.."]"..fname..": ", mess )
end	
if Runtime._G then Runtime._G.print = print
else _G.print = print
end

local transrefs = {}
local _cancelTrans = transition.cancel
transition.cancel = function( arg )
	if arg == nil then 
		print( "WARNING: transition canceled globally")
		print( debug.traceback() )
	end
	if transrefs[arg] then _cancelTrans( transrefs[arg].id );  transrefs[arg] = nil
	else _cancelTrans( arg )
	end
end

table.print = function( t, deep, pad, _selfcall )
	if not t or type( t ) ~= "table" then 
		return _print( "WARNING: table.print expected table, get "..tostring( t ) ) 
	end

	pad = pad or "  "
	deep = deep or 5
	if deep <= 0 then return end
	
	local function printpair( key, value, pad )
		key = tostring( key )
		if type( value ) == "table" then
			_print( pad.."["..key.."] = "..tostring( value )..":" )
			table.print( value, deep-1, pad.."  ", true )
		elseif type( value ) == "string" then
			_print( pad.."["..key.."] = \""..value.."\";" )
		else _print( pad.."["..key.."] = "..tostring( value )..";" )
		end
	end

	if not _selfcall then
		local time = system.getTimer()
		local info = debug.getinfo( 2, "nSl" )
		local fname = info.name and " ("..info.name..")" or ""
		local source = string.match( info.source, "/[%w%_%-%.%s]+/[%w%_%-%s]+/[%w%_%-%.%s]+$" )
		source = source or info.source
		local mess = "table.print"
		_print( time.."@.."..source.." ["..info.currentline.."]"..fname..": "..mess )
	end

	for i=1, #t do printpair( i, t[i], pad ) end
	for k,v in pairs( t ) do
		if type( k ) == "number" then
			if k > #t then printpair( k, v, pad ) end
		else printpair( k, v, pad )	 
		end
	end
end

table._copy = table.copy
table.copy = function( orig, deep )
	deep = deep or 100
	if type( orig ) ~= "table" then error( "table.copy: bad argument #1, table expected, got "..type( orig ), 2 ) end
	if type( deep or 0 ) ~= "number" then error( "table.copy: the deep should be number", 2 ) end
	
	local levels = 0
	local function copy( t )
		local nt = {}
		
		if levels + 1 > deep then return
		elseif levels + 1 > 100 then
			print( "WARNING (table.copy): too deep copying" )
			return
		else
			levels = levels + 1
		end
		
		for k,v in pairs( t ) do
			if type( v ) == "table" then nt[k] = copy( v )
			else nt[k] = v
			end
		end

		levels = levels - 1
		return nt
	end
	
	return copy( orig )
end

-- для безопасного создания и удаления карт: активная карт только одна
local active_map = nil
local _native_newMapView = native.newMapView
native.newMapView = function( ... )
	if active_map and active_map.removeSelf then
		active_map.isVisible = false 
		active_map:removeSelf() 
	end
	active_map = _native_newMapView( ... )
	return active_map
end

return std