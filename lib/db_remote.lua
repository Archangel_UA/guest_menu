local loadsave = require "lib.loadsave"
local mime = require "mime"
local json = require "json"
local crypto = require "crypto"
local XorCipher = require "lib.XorCipher"
local rdb_common = require( "lib.db_remote_common" )
local clib = require "lib.common_lib"

local rdb = {
	attempts = 3,
	sign = nil,
	print_response = false
}

local function testNetworkConnection()
--	_print("testNetworkConnection")
	local socket = require("socket")
            
	local test = socket.tcp()
	test:settimeout(1)                   -- Set timeout to 1 second
            
	local testResult = test:connect("www.google.com", 80)        -- Note that the test does not work if we put http:// in front
           
	test:close()
	test = nil
	if not(testResult == nil) then
		-- _print("Internet access is available")
		_G.internetConnectionIsActive=true
		return true
	else
		_print("Internet access is not available")
		_G.internetConnectionIsActive=false
		return false
	end
end

local function get_authorization_string( sign )
	sign = sign or rdb.sign
	if sign then
		if sign == 0 then
			return "Basic "..mime.b64 ("suser:dwt35s42s4")
		elseif sign == 3 then
			return "Basic "..mime.b64 ("test:test")
		elseif sign == 0.1 then
			return "Basic "..mime.b64 ("demorest:2014766")
		else return "Basic "..mime.b64 ("bns"..sign..":bns_psw"..sign)
		end
	else _print( "  !!!  NO AUTHORIZATION SIGN  !!!" )
		return "Basic "..mime.b64 ("test:test")
	end
--	_print(sign,"bns"..sign..":bns_psw"..sign)
end

local function removeFiles( uniq )
	os.remove( system.pathForFile( "r"..uniq..".json", system.DocumentsDirectory  ) )	
	os.remove( system.pathForFile( "q"..uniq..".json", system.DocumentsDirectory  ) )	
end

local function _printResponse( res )
	if not rdb.print_response then return end

	_print( "---------- NETWORK RESPONSE --> ",res," ----------" )
	if type(res) ~= "table" then return end
	for k,v in pairs(res) do
		_print(k,v)
		if type(v) == "table" then
			for k2,v2 in pairs(v) do
				_print(" ", k2,v2)
				if type(v2) == "table" then
					for k3,v3 in pairs(v2) do
						_print("     ", k3,v3)
					end
				end								
			end
		end
	end
	_print( "---------------------------------------------------" )
end

local function defaultParams( uniq )
	local headers = {}
	headers["Content-Type"] = "application/json"
	headers["Authorization"] = get_authorization_string()
	headers["Forward"] = get_authorization_string()
	
	local params = {}
	params.headers = headers

	params.body = {
        filename = "q"..uniq..".json",
        baseDirectory = system.DocumentsDirectory        
    }
	params.progress = "download"

	params.response = {
        filename = "r"..uniq..".json",
        baseDirectory = system.DocumentsDirectory
    }

    return params
end

function rdb:getconnectionstring()
	if userProfile and userProfile.SelectedRestaurantData and userProfile.SelectedRestaurantData.srv_url then
		return userProfile.SelectedRestaurantData.srv_url 
	else
		return _G.host 
	end
--	return "web.smarttouch.com.ua:3005"
end

function rdb:getData( table, listener, filter,allservers )
	_print( "...rdb:getData from ", table )
--	_printResponse(filter)
	local allservers=allservers
	if allservers==nil then
		allservers=false
	end
	local uniq = math.random( 1000, 9999 )

	if testNetworkConnection() == false then
		if listener then listener( "disconnection" ) end
		return
	end

	local function networkListener( event )
	    if event.isError  then
			_print( "rdb:getData...error ",event.response )
			if listener then listener( "error" ) end
			--removeFiles( uniq )	
		elseif event.phase == "ended" then
			-- _print( "rdb:getData...no error " )
			local res = loadsave.loadTable( "r_"..table..".json", system.DocumentsDirectory )
			--_printResponse( res )			
			if listener then
				if allservers==true and res then
					local res_rows={}
					for i,item in pairs( res ) do
--						_print(item.server.srv_url)
						if item.body.rows then
							for i1,item1 in pairs( item.body.rows ) do
								res_rows[#res_rows+1]=item1
								res_rows[#res_rows].srv_url=item.server.srv_url
							end	
						end	
					end	
					listener( "success", res_rows )
				else
					if res and res.recordsCount and res.recordsCount >= 1 then 
						listener( "success", res.rows )
					else listener( "denied" )
					end
				end	
			end
			--removeFiles( uniq )
	    end
	end

	local t = {
		pageNum = 1,
		pageSize = 100000
	}
	if filter then
		t = {
			pageNum = 1,
			pageSize = 100000,
			filter = filter
		}
	end	
--	_printResponse(t)
	loadsave.saveTable(t, "q_"..table..".json" )
	local str_url="http://"..self:getconnectionstring()
	if allservers then
		str_url="http://admin.smarttouchpos.eu:3030"
	end
	local params = defaultParams( "_"..table )
--	_print(str_url.."/webapi/object/query/"..table)
	if table == "vw_bonus_allcompanies" then
		-- print("!!!!!!!!!!!!!!!!!!!!! url",str_url.."/webapi/object/query/"..table)
		-- printResponse(params)
		-- print("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII")
		 -- native.showAlert(_G.appTitle,str_url.."/webapi/object/query/"..table .."\n"..json.encode(t),{ "OK" })
	end
	network.request( str_url.."/webapi/object/query/"..table, "POST", networkListener,  params )	
end

function rdb:addNewRowData( table, data, listener )
_print( "rdb:addNewRowData..." )

	local uniq = math.random( 1000, 9999 )

	if testNetworkConnection() == false then
		if listener then listener( "disconnection" ) end
		return
	end

	local function networkListener( event )
	    if event.isError  then
			_print( "rdb:addNewRowData...error " )
			if listener then listener( "error" ) end
			removeFiles( uniq )
		elseif event.phase == "ended" then
			local res = loadsave.loadTable( "r"..uniq..".json",system.DocumentsDirectory )
			_printResponse( res )
			if listener then
				if res and res.code == "ok" then listener( "success", res.id )
				else listener( "denied" )
				end
			end
			removeFiles( uniq )
	    end
	end

	loadsave.saveTable( data, "q"..uniq..".json")

	local params = defaultParams( uniq )
	network.request( "http://"..self:getconnectionstring().."/webapi/object/"..table, "POST", networkListener,  params )	
end

function rdb:getRowData( table, row_id, listener )
_print( "...rdb:getRowData from ", table, row_id )
	
	local uniq = math.random( 1000, 9999 )
	
	if testNetworkConnection() == false then
		if listener then listener( "disconnection" ) end
		return
	end

	local function networkListener( event )
	    if event.isError  then
			_print( "rdb:getRowData...error " )
			for k,v in pairs(event) do
				_print("........  ", k, v)
			end
			local res = loadsave.loadTable( "r"..uniq..".json",system.DocumentsDirectory )
			--_printResponse( res )
			if listener then listener( "error" ) end
			removeFiles( uniq )	
		elseif event.phase == "ended" then
			_print( "rdb:getRowData...no error " )
			local res = loadsave.loadTable( "r"..uniq..".json",system.DocumentsDirectory )
			_printResponse( res )
			if listener then
				if res then listener( "success", res )
				else listener( "denied" )
				end
			end
			removeFiles( uniq )
	    end
	end
	
	local params = defaultParams( uniq )
	params.body = nil
	network.request( "http://"..self:getconnectionstring().."/webapi/object/"..table.."/"..row_id, "GET", networkListener,  params )	
end

function rdb:updateRowData( table, data, listener )
_print( "rdb:updateRowData...", data.com_id )
	
	local uniq = math.random( 1000, 9999 )
	
	if testNetworkConnection() == false then
		if listener then listener( "disconnection" ) end
		return
	end

	local function networkListener( event )
	    if event.isError  then
			if listener then listener( "error" ) end
--			removeFiles( uniq )	
		elseif event.phase == "ended" then
			local res = loadsave.loadTable( "r"..uniq..".json",system.DocumentsDirectory )
			_printResponse( res )
			if listener then
				if res and res.code == "ok:" or res.code == "ok" then listener( "success" )
				else listener( "denied" )
				end
			end
--			removeFiles( uniq )
	    end
	end

	loadsave.saveTable( data, "q"..uniq..".json" )
	_print("q"..uniq..".json")
	local params = defaultParams( uniq )
	network.request( "http://"..self:getconnectionstring().."/webapi/object/"..table, "PUT", networkListener,  params )	
end

function rdb:deleteRow( table, id, listener )
_print( "rdb:deleteRow...", id )
	
	local uniq = math.random( 1000, 9999 )
	
	if testNetworkConnection() == false then
		if listener then listener( "disconnection" ) end
		return
	end

	local function networkListener( event )
	    if event.isError  then
			if listener then listener( "error" ) end
			removeFiles( uniq )	
		elseif event.phase == "ended" then
			local res = loadsave.loadTable( "r"..uniq..".json",system.DocumentsDirectory )
	--		_printResponse( res )
			if listener then
				if res.code == "ok:" or res.code == "ok" then listener( "success" )
				else listener( "denied" )
				end
			end
			removeFiles( uniq )
	    end
	end

	loadsave.saveTable( data, "q"..uniq..".json" )

	local params = defaultParams( uniq )
	network.request( "http://"..self:getconnectionstring().."/webapi/object/"..table..id, "DELETE", networkListener,  params )	
end

function rdb:restorePassword( data, listener )
	_print( "restorePassword sended" )

	self.sign = 0
	if testNetworkConnection() == false then
		if listener then listener( "disconnection" ) end
		return
	end

	local function networkListener( event )
		_print(event.phase,event.isError)
	    if event.isError  then
			if listener then listener( "error" ) end	
		elseif event.phase == "ended" then
			local res = loadsave.loadTable( "guest_pass_rest_res.json",system.DocumentsDirectory )
			if listener then
				if res.recordsCount >= 1 then 
	--				_print(res.rows[1].guest_email)
					local res_pass=XorCipher.crypt(res.rows[1].guest_password,true)
					rdb_common:sendUserData{listener=nil,body="lang_code="..userProfile.lang.."&email="..res.rows[1].guest_email.."&email_text="..translations["restore_password_txt"][_G.Lang]..res_pass.."&email_subject="..translations["restore_password_subj"][_G.Lang],api_proc="send_email_to_client"}

					listener( "gotpassword", XorCipher.crypt(res.rows[1].guest_password,true) )
				else listener( "nosuchemail" )
				end
			end
			_printResponse( res )
	    end
	end

	--local hash = XorCipher.crypt(res.rows[1].guest_password)--string.upper(crypto.digest( 
		--crypto.md5, string.upper(data.email).."PASSWORD"..data.password 
	--))

	local headers = {}

	headers["Content-Type"] = "application/json"
	headers["Authorization"] = get_authorization_string()
	
	local t = {
		pageNum = 1,
		pageSize = 100000,
		filter = {
			group = "or",
			conditions = {
				{ 
					left = "guest_email",
					oper = "=",
					right = "'"..data.email.."'"
				},
				{ 
					left = "guest_phone",
					oper = "=",
					right = "'"..data.email.."'"
				},				
			}
		}
	}
	loadsave.saveTable(t, "guest_pass_rest.json")
	
	local params = {}
	params.headers = headers

	params.body = {
        filename = "guest_pass_rest.json",
        baseDirectory = system.DocumentsDirectory        
    }
	params.progress = "download"

	params.response = {
        filename = "guest_pass_rest_res.json",
        baseDirectory = system.DocumentsDirectory
    }
 --   _print("http://"..self:getconnectionstring().."/webapi/object/query/guests")
	network.request( "http://"..self:getconnectionstring().."/webapi/object/query/guests", "POST", networkListener,  params )	
end

function rdb:requestToLogin( data, listener )
	_print( "requestToLogin sended" )

	self.sign = 0
	if testNetworkConnection() == false then
		if listener then listener( "disconnection" ) end
		return
	end

	local function networkListener( event )
	    if event.isError  then
			if listener then listener( "error" ) end	
		elseif event.phase == "ended" then
			local res = loadsave.loadTable( "response.json",system.DocumentsDirectory )
			if listener then
				if res.recordsCount == 1 then listener( "accepted", res.rows[1] )
				else listener( "denied" )
				end
			end
			_printResponse( res )
	    end
	end

	local hash = XorCipher.crypt(data.password)--string.upper(crypto.digest( 
		--crypto.md5, string.upper(data.email).."PASSWORD"..data.password 
	--))

	local headers = {}

	headers["Content-Type"] = "application/json"
	headers["Authorization"] = get_authorization_string()
	
	local t = {
		pageNum = 1,
		pageSize = 100000,
		filter = {
			group = "and",
			conditions = {
				{ 
					left = "guest_email",
					oper = "=",
					right = "'"..data.email.."'"
				},
				{ 
					left = "guest_password",
					oper = "=",
					right = "'"..hash.."'"
				},
			}
		}
	}
	loadsave.saveTable(t, "q.json")
	
	local params = {}
	params.headers = headers

	params.body = {
        filename = "q.json",
        baseDirectory = system.DocumentsDirectory        
    }
	params.progress = "download"

	params.response = {
        filename = "response.json",
        baseDirectory = system.DocumentsDirectory
    }
	network.request( "http://"..self:getconnectionstring().."/webapi/object/query/guests", "POST", networkListener,  params )	
end

local function checkUniqueGuest( email,phone, listener )
	local uniq = math.random( 1000, 9999 )
	rdb.sign = 0
	_print("checkUniqueGuest ",email)
	if testNetworkConnection() == false then
		if listener then listener( "disconnection" ) end
		return
	end

	local function networkListener( event )
		if event.isError  then
			if listener then listener( "error" ) end
			removeFiles( uniq )	
		elseif event.phase == "ended" then
			local res = loadsave.loadTable( "r"..uniq..".json", system.DocumentsDirectory )			
			if listener then
				if res.recordsCount == 0 then
					_print("checkUniqueGuest success")
					debug_print("5","checkUniqueGuest success")
					listener( "success" )
				elseif res.recordsCount >= 1 then
					_print("checkUniqueGuest exist",res.rows[1].guest_id)
					debug_print("5","checkUniqueGuest exist")
					--_print(res.rows[1].guest_id)
					listener( "exist",res.rows )
				else
					_print("checkUniqueGuest denied")
					debug_print("5","checkUniqueGuest denied")
				 	listener( "denied" )
				end
			end
			removeFiles( uniq )
		end
	end

	local headers = {}
	headers["Content-Type"] = "application/json"
	headers["Authorization"] = get_authorization_string()
	
	local t = {
		pageNum = 1,
		pageSize = 100000,
		filter = {
			group = "or",
			conditions = {
				{ 
					left = "guest_email",
					oper = "=",
					right = "'"..email.."'"
				},
				
			}
		}
	}
	if phone and phone~="" then
		t.filter.conditions[#t.filter.conditions+1]={ 
					left = "guest_phone",
					oper = " like ",
					right = "'%"..phone.."'"
				}
	end
	loadsave.saveTable( t, "q"..uniq..".json" )
	
	local params = defaultParams( uniq )
	print("http://"..rdb_common:getconnectionstring().."/webapi/object/query/guests")
	network.request( "http://"..rdb_common:getconnectionstring().."/webapi/object/query/guests", "POST", networkListener, params )
end

local function sendNewGuestData( data, listener )
	_print( "rdb:sendNewGuestData..." )
	
	if testNetworkConnection()==false then
		if listener then listener( "disconnection" ) end
		return
	end
	local hash = XorCipher.crypt(data.password)--string.upper(crypto.digest( 
	
	local t = {
		["guest_email"] = data.email,
		["guest_firstname"] = data.name,
		["guest_surname"] = data.surname,
		["guest_fullname"] = data.fullname,
		["guest_phone"] = data.tel,
		["guest_password"] = data.guest_password,
		["cntr_id"] = data.cntr_id
	}
	local function networkListener( event )
	    if event.isError  then
			if listener then listener( "error" ) end	
		elseif event.phase == "ended" then
			local res = loadsave.loadTable( "response.json",system.DocumentsDirectory )
			_printResponse( res )			
			if listener then
				if res.code == "ok" then 
					if data.fb_login==true then
						t.guest_id=res.id
						listener( "accepted", t )
					else
						listener( "accepted", res.id )
					end
				else listener( "denied" )
				end
			end
	    end
	end


		--crypto.md5, string.upper(data.email).."PASSWORD"..data.password 
	--))

	local headers = {}

	headers["Content-Type"] = "application/json"
	headers["Authorization"] = get_authorization_string()
	

	loadsave.saveTable(t, "q.json")
	
	local params = {}
	params.headers = headers

	params.body = {
        filename = "q.json",
        baseDirectory = system.DocumentsDirectory        
    }
	params.progress = "download"

	params.response = {
        filename = "response.json",
        baseDirectory = system.DocumentsDirectory
    }
	network.request( "http://"..rdb_common:getconnectionstring().."/webapi/object/guests", "POST", networkListener,  params )	
end

function rdb:requestToRegistry( data, listener )
	local function networkListener( state,res_data )
		_print("checkUniqueGuest networkListener",state)
		debug_print("checkUniqueGuest networkListener",state)
		if state == "success" then 
			sendNewGuestData( data, listener )
		elseif state == "exist" and data.fb_login==true then 
			--_printResponse(res_data)
			--_print(res_data[1])
			listener( "accepted", res_data[1] )
		else listener( state )
		end
	end
	print(data.email,data.tel)
	checkUniqueGuest( data.email,data.tel, networkListener )
end

function rdb:getGuestBalance( guest_id, listener )
	_print("rdb:getGuestBalance")
	local function networkListener( state, data )
		local balance = {}
		local bonus_types = {}
	--	_print("RRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRR",state)
        if  state == "denied" then
	 		if userProfile.SelectedRestaurantData and userProfile.SelectedRestaurantData.com_loyalty_type == 2 then
	--			userProfile.balance[userProfile.SelectedRestaurantData.com_id]=0
				balance[userProfile.SelectedRestaurantData.com_id] = 0
				bonus_types[userProfile.SelectedRestaurantData.com_id] = 0
				_print("ZERO BALANCE")
			end	        
			listener( state, balance,bonus_types )	
        elseif state == "success" then

-- data = {
-- 	[1] = { pos_id = 1, balance = 177.5, fullname = "Andrew Marvell" },
-- 	[2] = { pos_id = 13, balance = 25.8, fullname = "Andrew Marvell" },
-- 	[3] = { pos_id = 83, balance = 0.11, fullname = "Andrew Marvell" }
-- }

		--local balance = {}
	
		for i=1, #data do
			if data[i].com_id then
				balance[data[i].com_id] = data[i].balance
				bonus_types[data[i].com_id] = 0
--				_print("coffee_balance",data[i].coffee_balance)
				if data[i].coffee_balance~=nil and data[i].coffee_balance>0 then
					balance[data[i].com_id] = data[i].coffee_balance
					bonus_types[data[i].com_id] = 1
				end
			end
		end
		userProfile.balance=balance
		userProfile.bonus_types=bonus_types
--[[_print( "######### getGuestBalance", data[1].fullname )
for k,v in pairs(balance) do
	_print("pos_id: ", k, "balanc: ", v)
end]]

            listener( state, balance,bonus_types )
        else listener( state, balance,bonus_types )
        end
	end

	local filter= {
      group= "and",
      conditions= {
          {
	        left= "guest_id",
	        oper= "=",
	        right= guest_id
          }	  
      }
	}
	self.sign = 0
	--self:getData( "vw_bonus_balance_by_guest", networkListener, filter )
	_print("guest_id",guest_id)
	self:getData( "vw_bonus_balance_by_guest_and_pos", networkListener, filter )
end

function rdb:getGuestHistory( guest_id, listener )
	local function networkListener( state, data )
        if state == "success" then
            listener( state, data )
        else listener( state )
        end
	end

	local filter= {
      group= "and",
      conditions= {
          {
	        left= "guest_id",
	        oper= "=",
	        right= guest_id
          },
          {
            left= "lang_code",
            oper= "=",
            right= "'".._G.Lang.."'"
          }	  
      }
	}
	self.sign = 0
	--if userProfile.SelectedRestaurantData~=nil and userProfile.SelectedRestaurantData.com_loyalty_type<=1 then
	--	self:getData( "vw_orders_history_by_guest", networkListener, filter )  --������� ������
	--else
		self:getData( "vw_bonus_history_by_guest", networkListener, filter )	-- ������
	--end
end

function rdb:changeGuestData( check_uniq, data, listener )
	if data.guest_password then
		data.guest_password = XorCipher.crypt(data.guest_password)--string.upper(crypto.digest( 
			--crypto.md5, string.upper(data.guest_email).."PASSWORD"..data.guest_password 
		--))
	end

	local function networkListener( state )
		if state == "success" then 
			self:updateRowData( "guests", data, listener )
		else listener( state )
		end
	end

	if data.guest_email and check_uniq then 
		checkUniqueGuest( data.guest_email,"", networkListener )
	else
		self:updateRowData( "guests", data, listener )
	end
end

local function downloadAboutImg( com_id, srv_url,img_id, filename, basedir, size, listener,pos_id )
--	_print(srv_url)
	if _G.internetConnectionIsActive == false then
		if listener then listener( "disconnection" ) end
		return
	end	

	local function networkListener( event )
	    if event.isError then
			_print("########################################################",com_id,filename)
			_printResponse( event )			
			if listener then listener( event ) end			
		elseif event.phase == "ended" then
			_printResponse( event )			
			if listener then listener( event ) end			
		end
	end

	local headers = {}
	headers["Content-Type"] = "application/json"
	headers["Authorization"] = get_authorization_string( com_id )
	-- _print(com_id,get_authorization_string( com_id ))
	local params = {}
	params.headers = headers
	params.timeout = 5
	params.progress = "download"
	params.response = {
		filename = filename,
		baseDirectory = basedir
	}
	local srv_url_local
	if srv_url then
		srv_url_local = srv_url
	else
		srv_url_local = rdb:getconnectionstring()
	end
	if img_id ~= "logo" then img_id = "photo/"..img_id end	
	if img_id ~= "logo" then
--		_print( "http://"..srv_url_local.."/common/about/"..img_id.."?size="..size)
		network.request( "http://"..srv_url_local.."/common/about/"..img_id.."?size="..size, "GET", networkListener, params )							
	else
--		_print( "http://"..srv_url_local.."/common/about/logo_pos?pos_id="..pos_id.."&size="..size,filename)
		network.request( "http://"..srv_url_local.."/common/about/logo_pos?pos_id="..pos_id.."&size="..size, "GET", networkListener, params )							
	end
end

local function downloadRestPhoto( photos_data, srv_url, local_dir, size, listener )
	_print("downloadRestPhoto")
	local basedir = system.TemporaryDirectory
	local counter = 0
	local qual = #photos_data
	
	local check = {
		--[name] = { exist = false, basedir, aspect, mark }
	}
	
	local files = {}
	-- structure clarification:
	--	files[comid] = {
	--  	[pos_id] = {	
	--			main = { name = "", basedir = {}, aspect = 0 },
	--			othes = {
	--				[1] = { name = "", basedir = {}, aspect = 0 },
	--				[2] = { name = "", basedir = {}, aspect = 0 },
	--			...
	--			}
	--      }
	--	}

	-- создаем корневую папку для хранения фотографий
	lfs.chdir( system.pathForFile( "", basedir ) )
	if not lfs.chdir( lfs.currentdir().."/"..local_dir ) then lfs.mkdir( local_dir ) end

	local function addPhoto(filename)
			counter = counter + 1
			if filename then
				if check[filename] then
					check[filename].exist = true
					local file = check[filename]
					local com_id = file.com_id
					local pos_id = file.pos_id
					files[com_id] = files[com_id] or {}
					files[com_id][pos_id] = files[com_id][pos_id] or {}
					files[com_id][pos_id].othes = files[com_id][pos_id].othes or {}
					local othes = files[com_id][pos_id].othes
					
					if file.mark == "main" then
						files[com_id][pos_id].main = {
							name = file.name,
							basedir = file.basedir,
							aspect = file.aspect
						}
					else 
						othes[#othes+1] = {
							name = file.name,
							basedir = file.basedir,
							aspect = file.aspect							
						}
					end
				end
			end
			if counter == qual and listener then
				if next( files ) then
					listener( "success", files )
				else listener( "denied" )
				end
			end		
	end

	local function networkListener( event )
	    if event.isError or event == "disconnection" then
			counter = counter + 1
			if counter == qual and listener then
				local state = event.isError and "error" or event
				listener( state, files )
			end			
		
		elseif event.phase == "ended" then
			local res = event.response
			addPhoto(res.filename)
	    end
	end	

	local path, filename, mark
	local com_id, pos_id, abp_id, aspect = 0, 0, 0, 0
	for i = 1, #photos_data do
		-- print("abp_id",photos_data[i].abp_id,"com_id",photos_data[i].com_id,"pos_id",photos_data[i].pos_id)
		com_id = photos_data[i].com_id or 0
		pos_id = photos_data[i].pos_id or 0
		abp_id = photos_data[i].abp_id
		aspect = photos_data[i].abp_photo_width / photos_data[i].abp_photo_height
		-- path = local_dir.."\\"..com_id.."\\"
		path = local_dir.."/"..com_id.."/"
		filename = "p-"..com_id.."-"..pos_id.."-"..abp_id.."."..photos_data[i].abp_photo_format
		mark = nil
		if photos_data[i].abp_is_main then 
			mark = "main"
			filename = "m"..filename
		end
		filename = path..filename
		
		check[filename] = {}
		check[filename].name = filename
		check[filename].com_id = com_id
		check[filename].pos_id = pos_id
		check[filename].basedir = basedir
		check[filename].aspect = aspect
		check[filename].mark = mark

		lfs.chdir( system.pathForFile( "", basedir ).."/"..local_dir )
		if not lfs.chdir( lfs.currentdir().."/"..com_id ) then lfs.mkdir( com_id ) end
	
		if clib:fileExists(filename,basedir)==false then
			-- print("NEW PHOTO",filename)
			local srv_url_local = photos_data[i].srv_url
			if srv_url ~= nil then srv_url_local = srv_url end
			downloadAboutImg( com_id, srv_url_local,abp_id, filename, basedir, size, networkListener  )
		else
			-- print("OLD PHOTO",filename)
			addPhoto(filename)		
		end
	end
	if #photos_data==0 then
		listener( "denied" )
	end
end

function rdb:getRestPhoto( dir, size, listener, filter, allservers )
	local allservers=allservers
	if allservers==nil then
		allservers=false
	end
	self.sign = 0
	_print("rdb:getRestPhoto",allservers)
	self:getData( "vw_about_photos", function( state, data )
		_print("rdb:getRestPhoto - vw_about_photos ",state)
		-- _print(data)
		-- _printResponse(data)
		if state == "success" then downloadRestPhoto( data, nil, dir, size, listener )
		else listener( state )
		end
	end, filter, allservers )
end

function rdb:getDishData( com_id, mi_id, lang, listener )
	self.sign = com_id

	local function networkListener( state, data )
		if listener then
			if state == "success" then listener( state, data[1] )
			else listener( state )	
			end
		end 
	end

	local filter = {     
		group = "and",
	    conditions = {
	        {
	            left = "mg_id",
	            oper = "=",
	            right = mi_id
	        }, 	      
	        {
	            left = "lang_code",
	            oper = "=",
	            right = "'"..lang.."'"
	        },          
	        -- {
	        --     left = "user_guest",
	        --     oper = "=",
	        --     right ="true"
	        -- },
	        -- {
	        --     left = "group",
	        --     oper = "=",
	        --     right = "0"
	        -- }, 	            
	    }
	}
	rdb:getData( "vw_bonus_pricelist", networkListener, filter )
end

function rdb:getDishPhoto( com_id, photo_id, size, listener )
	if testNetworkConnection() == false then
		if listener then listener( "disconnection" ) end
		return
	end
	if photo_id==nil then
		if listener then listener( "disconnection" ) end
		return		
	end
	local function networkListener( event )
	    if event.isError then
	        if listener then listener( "error" ) end
	    elseif event.target then
	    	if listener then listener( "success", {
		    		image = event.target,
		    		fullpath = event.response.fullPath,
		    		filename = event.response.filename
		    	})
	    	end
		end
	end	

	local name = "mip_"..tostring(photo_id)..".jpg"
	local dir = system.TemporaryDirectory

	local headers = {}
	headers["Content-Type"] = "application/json"
	headers["Authorization"] = get_authorization_string( com_id )

	local params = {}
	params.headers = headers
	params.progress = "download"			
	display.loadRemoteImage( "http://"..self:getconnectionstring().."/dictionary/menu/photoSizeMenu/"..photo_id.."?size="..size, "GET", networkListener, params, name, dir, 0, 0 )
--	_print("http://"..self:getconnectionstring().."/dictionary/menu/photoSizeMenu/"..photo_id.."?size="..size)
end

function rdb:sendRestRate( rate, listener )
	self.sign = rate.com_id
	local data = {   
	    ["com_id"] = rate.com_id,
	    ["ord_id"] = rate.ord_id,
	    ["pos_id"] = rate.pos_id,	    
	    ["guest_id"] = rate.guest_id,	    
	    ["ordrat_datetime"]= "'"..rate.datetime.."'",
	    ["ordrat_index1"]= rate.interior,
	    ["ordrat_index2"]= rate.cuisine,
	    ["ordrat_index3"]= rate.service,
	    ["ordrat_text"]= rate.ordrat_text	    
	}
--	_print( "sendRestRate",rate.pos_id, rate.guest_id)
	rdb:addNewRowData( "orders_rating", data, function( state, row_id )
	    listener( state, row_id )
	end )
end

function rdb:sendDishRate( rate, listener )
	self.sign = rate.com_id
	local data = {   
	    ["com_id"] = rate.com_id,
	    ["ord_id"] = rate.ord_id,
	    ["orditrat_datetime"] = "'"..rate.datetime.."'",
	    ["mi_id"] = rate.mi_id,	    
	    ["ortitrat_index1"] = rate.rate
	}
	rdb:addNewRowData( "order_items_rating", data, function( state, row_id )
	    listener( state, row_id )
	end )
end

function rdb:downloadRestLogo( com_id,srv_url, pos_id, filename, listener )
	listener = listener or function() end
	local basedir = system.TemporaryDirectory
	local size = "100x100"--(display.contentWidth*0.25).."x"..(display.contentHeight*0.25)

	-- создаем корневую папку для хранения логотипов
	lfs.chdir( system.pathForFile( "", basedir ) )
	if not lfs.chdir( lfs.currentdir().."/logos" ) then lfs.mkdir( "logos" ) end

	local function networkListener( event )
	    if event.isError then listener( "error" )	
	    elseif event == "disconnection" then
			listener( "disconnection" )			
		elseif event.phase == "ended" then
			local res = event.response
			if res.filename and event.bytesTransferred > 0 then 
				listener( "success", { pos_id = pos_id, basedir = basedir, file = filename } )
			else listener( "denied" )
			end
		end
	end
	
	downloadAboutImg( com_id, srv_url,"logo", filename, basedir, size, networkListener, pos_id  )
end


function rdb:makePayment(  orderData,listener,command )
	_print( "...makePayment " )
	local command=command or "pay_card"
--	_printResponse(filter)
	local uniq = math.random( 1000, 9999 )
	local table="paym"
	if testNetworkConnection() == false then
		if listener then listener( "disconnection" ) end
		return
	end

	local function networkListener( event )
	    if event.isError  then
			_print( "rdb:getData...error " )
			if listener then listener( "error" ) end
			--removeFiles( uniq )	
		elseif event.phase == "ended" then
_print( "rdb:getData...no error " )
			local res = loadsave.loadTable( "r_paym.json", system.DocumentsDirectory )
--			_print("1")
--			_printResponse( res )
--			_print("2")
			if listener then
				if res then 
					listener( "success", res )
				else listener( "denied" )
				end
			end
			--removeFiles( uniq )
	    end
	end

	local t = orderData
	
	loadsave.saveTable(t, "q_paym.json" )
	
	local params = defaultParams( "_"..table )
	params.body = {
        filename = "q_paym.json",
        baseDirectory = system.DocumentsDirectory        
    }	
	network.request( "http://"..self:getconnectionstring().."/webapi/bonusme/"..command, "POST", networkListener,  params )	
	--network.request( "http://web.smarttouchpos.eu:3005/webapi/bonusme/"..command, "POST", networkListener,  params )	
--	network.request( "http://"..self:getconnectionstring().."/webapi/callback/fondy", "POST", networkListener,  params )	
end

--- for debugging app ---

function rdb:getOrderState( row_id, listener )
	rdb:getRowData( "orders", row_id, function( response, data )
		if data then listener( data.ord_state ) end
	end )
end

function rdb:setOrderState( ord_id, state, listener )
	rdb:updateRowData( "orders", { ord_id = ord_id, ord_state = state }, listener )
end

function rdb:setReservState( ord_id, state, listener )
	rdb:updateRowData( "orders", { ord_id = ord_id, ord_reserv_state = state }, listener )
end

return rdb