local loadsave = require "lib.loadsave"
local mime = require "mime"
local json = require "json"
local crypto = require "crypto"
local socket = require("socket")
local rdb = {}

function rdb:checkInternetConnection()
	local sec=sec or 1
	local test = socket.tcp()
	local status
	if platformName == "Android" then
		test:settimeout(1,b)
		test:settimeout(1,t)
		--test:settimeout(1)
	else
		test:settimeout(sec)
	end
	
	local testResult 
	--native.showAlert( "SmartTouch", "start check" , { "OK" } )
	--if SETTINGS['defaultLang']=='ru' then
	testResult= test:connect("8.8.8.8", 53)
	--else
	--	testResult = test:connect('en.smarttouchpos.eu', 80)
	--end
	if not(testResult == nil) then
	    status = true
	--	native.showAlert( "SmartTouch", "true" , { "OK" } )
	else
		print("8.8.8.8 false")
	    status = false
	--	native.showAlert( "SmartTouch", "false" , { "OK" } )
		if _G.country=="US" or _G.country=="ID" or _G.country=="CA" then
			testResult = test:connect('78.47.148.53', 80)
			if not(testResult == nil) then
				status = true
			else
				status = false
			end
		else
			testResult = test:connect('web.smarttouchpos.eu', 80)
			if not(testResult == nil) then
				status = true
			else
				status = false
			end			
		end
	end
	            
	test:close()
	test = nil
	_G.internetConnectionIsActive=status
	return status

end

function rdb:testNetworkConnection()
	return _G.internetConnectionIsActive
end

function rdb:get_authorization_string(com_id)
	local log_str
	if userProfile.AppType=='Emenu' then
		if userProfile.ServerLogin~=nil then
			log_str=userProfile.ServerLogin..":"..userProfile.ServerPassword
		else
			log_str="demorest:2014766"
		end
	elseif userProfile.AppType=='BONUS' then
		if com_id==nil then
			log_str="bns"..userProfile.SelectedRestaurantData.com_id..":bns_psw"..userProfile.SelectedRestaurantData.com_id
		else
			log_str="bns"..com_id..":bns_psw"..com_id
		end
	else
		_G.password=_G.password or ""
		return "Basic " ..mime.b64 (_G.login..":".._G.password) -- ( userProfile.user_login..":"..userProfile.user_password )
	end
--	log_str = "demorest:2014766"
--	_print(userProfile.AppType,userProfile.SelectedRestaurantData.com_id)
	_print("!!!!!!!!!!"..log_str)
	return "Basic " ..mime.b64 ( log_str )
end

function rdb:get_admin_authorization_string()
	return "Basic " ..mime.b64 ( "suser:dwt35s42s4" )
end

function rdb:getconnectionstring()
	if userProfile and userProfile.SelectedRestaurantData and userProfile.SelectedRestaurantData.srv_url then
		return userProfile.SelectedRestaurantData.srv_url 
	else
		return _G.host 
	end	

--	return _G.host
--	return "web.smarttouch.com.ua:3005"
end

 function printResponse( res )
	print( "RESPONSE...", res )
	if type(res) ~= "table" then return end
	for k,v in pairs(res) do
		print(k,v)
		if type(v) == "table" then
			for k2,v2 in pairs(v) do
				print(" ", k2,v2)
				if type(v2) == "table" then
					for k3,v3 in pairs(v2) do
						print("     ", k3,v3)
						if type(v3) == table then
							for k4, v4 in pairs(v3) do
								print("      ",k4,k4)
							end
						end
					end
				end								
			end
		end
	end
end

function rdb:sendTableRow( params )
	_print("rdb:sendTableRow")
	local row = params.row or {}
	local table_name = params.table_name or nil
	--local fields = params.fields or {}
	local listener = params.listener or nil
	local row_index=params.row_index or 0
	local admin_auth=params.admin_auth or false
	local t = row


	local jsonString = json.encode (t)
	
	local fname=table_name..tostring(row_index).."_i.json"
    local path = system.pathForFile( fname, system.TemporaryDirectory )
    local file = io.open( path, "w" )
    file:write( jsonString )
	io.close( file )
    file = nil
	

	local headers = {}
	
	headers["Content-Type"] = "application/json"
	if admin_auth then
		headers["Authorization"] = self:get_admin_authorization_string()
	else
		headers["Authorization"] = self:get_authorization_string()
	end
	local params = {}
	params.headers = headers

	params.body = {
        filename = fname,
        baseDirectory = system.TemporaryDirectory        
        }
	--This tells network.request() that we want the 'began' and 'progress' events...
	params.progress = "download"

	--This tells network.request() that we want the output to go to a file...
	params.response = {
        filename = table_name..tostring(row_index).."_i_ad.json",
        baseDirectory = system.TemporaryDirectory
    }
	local function networkListenerSendTableRow(event)
		_print ("loadData...:>>>>>" )
		if ( event.isError ) then
				_print( "Network error!")
				if listener~=nil then
					listener({result = "error"})
				end
		elseif ( event.phase == "began" ) then
				if event.bytesEstimated <= 0 then
					_print( "Download starting, size unknown" )
				else
					_print( "Download starting, estimated size: " .. event.bytesEstimated )
				end
		elseif ( event.phase == "progress" ) then
				if event.bytesEstimated <= 0 then
					_print( "Download progress: " .. event.bytesTransferred )
				else
					_print( "Download progress: " .. event.bytesTransferred .. " of estimated: " .. event.bytesEstimated )
				end
		elseif ( event.phase == "ended" ) then
				_print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
				--_print(event.response.filename)
				--if event.response.filename~=nil then 
					local path = system.pathForFile(table_name..tostring(row_index).."_i_ad.json",system.TemporaryDirectory ) 
					local file = io.open( path, "r" )
					local output = file:read( "*a" )    
					io.close( file )
					file=nil
					local t = json.decode(output)
					printResponse(t)
					if listener~=nil then
						listener({result = "completed",data=t, rawData = output})
					end
				--end 
		end

	end	

	network.request( "http://"..self:getconnectionstring().."/webapi/object/"..table_name, "POST", networkListenerSendTableRow,  params )
	print("http://"..self:getconnectionstring().."/webapi/object/"..table_name)
end

function rdb:uploadTableWithData( params )
	local data = params.data or {}
	for i,item in ipairs(data) do
		params.row=item
		params.row_index=i
		self:sendTableRow(params)
	end
end



function rdb:getDataFromView(params) 
	local listener=params.listener
	local table_name=params.table_name
	local filter=params.filter or nil
	local func_call=params.func_call or 0
	local listener_params=params.listener_params or nil
	local admin_auth=params.admin_auth or false
	local row_index=params.row_index or ""
	if params.try_number==nil then
		params.try_number=1
	else
		params.try_number=params.try_number+1
	end
	if self:testNetworkConnection()==false then
		_print("No internet connection")
		listener({result = "error"})
		return
	end
	local url
	local myQuery
	if func_call==1 then
		url = "http://"..self:getconnectionstring().."/webapi/func_call/"..table_name--/"..filter  --�������� ������
		if filter~=nil then
			url = url .. "/"..filter
		end
		myQuery={pageNum=1,pageSize=100000}
	elseif func_call==3 then
		url = "http://"..self:getconnectionstring().."/webapi/func_call/"..table_name--/"..filter  --�������� ������
		myQuery=filter

	elseif func_call==2 then
		url = "http://"..self:getconnectionstring().."/webapi/object/query_array"
		myQuery=params.query_params
	else
		url = "http://"..self:getconnectionstring().."/webapi/object/query/"..table_name
		myQuery={pageNum=1,pageSize=100000}
	end
    local headers = {}

    headers["Content-Type"] = "application/json"
	if admin_auth then
		headers["Authorization"] = self:get_admin_authorization_string()
	else
		headers["Authorization"] = self:get_authorization_string()
	end
    
	if filter~=nil and func_call~=3 then
		myQuery.filter= filter
	end
    loadsave.saveTable(myQuery, table_name..row_index..".json",system.TemporaryDirectory)
   -- print(filter,table_name..row_index..".json")
    local http_params = {}
    http_params.headers = headers

    http_params.body = {
        filename = table_name..row_index..".json",
        baseDirectory = system.TemporaryDirectory        
    }
--    http_params.progress = "download"

    http_params.response = {
        filename = table_name.."_res"..tostring(row_index)..".json",
        baseDirectory = system.TemporaryDirectory
    }
	http_params.timeout = 6
    _print ("send request..."..table_name)
	local function networkListenerGetDataFromView(event)
		_print ("loadData...:>>>>>"..table_name )
		if ( event.isError ) then
	--			_print('status: ',event.status)
	--			_print('response: ',event.response)
				--local alert = native.showAlert( "Network error", "Network error", { "Yes"  } )
				-- _print( "Network error!")
				-- if table_name == "bonus_history_by_guest" then
				-- if table_name == "get_pay_url" then
				-- 	printResponse(event)
				-- 	print("PPPPPPPPPPPPPPPPPPPP")
				-- 	printResponse(http_params)
				-- 	print("PPPPPPPPPPPPPPPPPPPP")
				-- 	print(url)
				-- 	print("PPPPPPPPPPPPPPPPPPPP")
				-- end

				if params.try_number==1 then
					_print("trying to get second time "..table_name)
					self:getDataFromView(params)
				else
					listener({result = "error",response=event.status})
				end
		-- elseif ( event.phase == "began" ) then
				-- --local alert = native.showAlert( "Download starting", "Download starting", { "Yes"  } )
				-- if event.bytesEstimated <= 0 then
					-- _print( "Download starting, size unknown" )
				-- else
					-- _print( "Download starting, estimated size: " .. event.bytesEstimated )
				-- end
		-- elseif ( event.phase == "progress" ) then
				-- --local alert = native.showAlert( "Download progress", "Download progress", { "Yes"  } )
				-- if event.bytesEstimated <= 0 then
					-- _print( "Download progress: " .. event.bytesTransferred )
				-- else
					-- _print( "Download progress: " .. event.bytesTransferred .. " of estimated: " .. event.bytesEstimated )
				-- end
		elseif ( event.phase == "ended" ) then
				_print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
				--_print(event.response.filename)
				if event.response.filename~=nil then 
					local path = system.pathForFile(event.response.filename,system.TemporaryDirectory ) 
					local file = io.open( path, "r" )
					local output = file:read( "*a" )    
					io.close( file )
					file=nil
					local t = json.decode(output)
					if listener~=nil then
						listener({result = "completed",params=listener_params,data=t})
					end
				else
					_print("ERROR IN getting "..table_name)
					_print(event.response)
					-- if table_name == "get_pay_url" then
					-- 	printResponse(event)
					-- 	print("PPPPPPPPPPPPPPPPPPPP")
					-- 	printResponse(http_params)
					-- 	print("PPPPPPPPPPPPPPPPPPPP")
					-- 	print(url)
					-- 	print("PPPPPPPPPPPPPPPPPPPP")
					-- end
					if params.try_number==1 then
						_print("trying to get second time "..table_name)
						self:getDataFromView(params)
					else
						listener({result = "error"})
					end
				end
		end

	end	
	-- if table_name == "get_pay_url" then
	-- 	print("QQQQQQqqqqqqqqqqqqqqqqq")
	-- 	print(url)
	-- 	printResponse(http_params)
	-- 	print("QQQQQQqqqqqqqqqqqqqqqqq")
	-- end
	if func_call==1 then
		network.request(url,"GET", networkListenerGetDataFromView, http_params)
	else
		network.request(url,"POST", networkListenerGetDataFromView, http_params)
	end
end

function rdb:setDataFromView(params, metod) -- add G 
	local listener=params.listener
	local table_name=params.table_name 
	--local filter=params.filter or nil
	local listener_params=params.listener_params or nil
	local data=params.body or nil
	local row_id=params.row_id or ""
	local admin_auth=params.admin_auth or false
	if self:testNetworkConnection()==false then
		_print("No internet connection")
		listener({result = "error",params=listener_params})
		return
	end
	if params.try_number==nil then
		params.try_number=1
	else
		params.try_number=params.try_number+1
	end
	local url = "http://"..self:getconnectionstring().."/webapi/object/"..table_name
    local headers = {}

    headers["Content-Type"] = "application/json"
	if admin_auth then
		headers["Authorization"] = self:get_admin_authorization_string()
	else
		headers["Authorization"] = self:get_authorization_string()
	end
	printResponse(data)
    loadsave.saveTable(data, table_name..row_id..".json",system.TemporaryDirectory)
    
    local http_params = {}
    http_params.headers = headers

    http_params.body = {
        filename = table_name..row_id..".json",
        baseDirectory = system.TemporaryDirectory        
    }
--    http_params.progress = "download"

    http_params.response = {
        filename = table_name..row_id.."_res.json",
        baseDirectory = system.TemporaryDirectory
    }
	http_params.timeout = 4
    _print ("send request..."..table_name)
	local function networkListenerGetDataFromView(event)
		_print ("loadData...:>>>>>" )
		if ( event.isError ) then
				--local alert = native.showAlert( "Network error", "Network error", { "Yes"  } )
				_print( "Network error!")
				if params.try_number==1 then
					self:setDataFromView(params, metod)
				else
					listener({result = "error",params=listener_params})
				end
				
		-- elseif ( event.phase == "began" ) then
				-- --local alert = native.showAlert( "Download starting", "Download starting", { "Yes"  } )
				-- if event.bytesEstimated <= 0 then
					-- _print( "Download starting, size unknown" )
				-- else
					-- _print( "Download starting, estimated size: " .. event.bytesEstimated )
				-- end
		-- elseif ( event.phase == "progress" ) then
				-- --local alert = native.showAlert( "Download progress", "Download progress", { "Yes"  } )
				-- if event.bytesEstimated <= 0 then
					-- _print( "Download progress: " .. event.bytesTransferred )
				-- else
					-- _print( "Download progress: " .. event.bytesTransferred .. " of estimated: " .. event.bytesEstimated )
				-- end
		elseif ( event.phase == "ended" ) then
				_print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
				_print(event.response.filename)
				if event.response.filename~=nil then 
					local path = system.pathForFile(event.response.filename,system.TemporaryDirectory ) 
					local file = io.open( path, "r" )
					local output = file:read( "*a" )    
					io.close( file )
					file=nil
					local t = json.decode(output)
					listener({result = "completed",data=t, rawData = output,params=listener_params, dataInput = data})
				else
					_print("error on transfer, sending again!")
					if params.try_number==1 then
						self:setDataFromView(params, metod)
					end
				end
		end

	end	
	
    network.request(url,"PUT", networkListenerGetDataFromView, http_params)

end -- add G

function rdb:getQRCode(params) 
	local listener=params.listener
	local filter=params.filter or nil
	local func_call=params.func_call or 0
	local listener_params=params.listener_params or nil
	local admin_auth=params.admin_auth or false
	if params.try_number==nil then
		params.try_number=1
	else
		params.try_number=params.try_number+1
	end
	if self:testNetworkConnection()==false then
		_print("No internet connection")
		return
	end
	local url

		url = "http://"..self:getconnectionstring().."/webapi/func_call/qr_code"--/"..filter  --óñêîðÿåì ðàáîòó
		if filter~=nil then
			url = url .. "/"..filter
		end
    local headers = {}

    headers["Content-Type"] = "application/json"
	if admin_auth then
		headers["Authorization"] = self:get_admin_authorization_string()
	else
		headers["Authorization"] = self:get_authorization_string()
	end
    local myQuery={pageNum=1,pageSize=100000}
    loadsave.saveTable(myQuery, "qr.json",system.TemporaryDirectory)
    
    local http_params = {}
    http_params.headers = headers

    http_params.body = {
        filename = table_name..row_index..".json",
        baseDirectory = system.TemporaryDirectory        
    }
--    http_params.progress = "download"

    http_params.response = {
        filename = "qr.jpg",
        baseDirectory = system.TemporaryDirectory
    }
	--http_params.timeout = 4
    _print ("send request...")
	local function networkListenerGetDataFromView(event)
		_print ("loadData...:>>>>>"..table_name )
		if ( event.isError ) then
				--local alert = native.showAlert( "Network error", "Network error", { "Yes"  } )
				_print( "Network error!")
				if params.try_number==1 then
					_print("trying to get second time "..table_name)
					self:getDataFromView(params)
				else
					listener({result = "error"})
				end
		elseif ( event.phase == "ended" ) then
				_print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
				--_print(event.response.filename)
				if event.response.filename~=nil then 
					local path = system.pathForFile(event.response.filename,system.TemporaryDirectory ) 
					local file = io.open( path, "r" )
					local output = file:read( "*a" )    
					io.close( file )
					file=nil
					local t = json.decode(output)
					if listener~=nil then
						listener({result = "completed",params=listener_params,data=t, rawData = output})
					end
				else
					_print("ERROR IN getting "..table_name)
					_print(event.response)
					if params.try_number==1 then
						_print("trying to get second time "..table_name)
						self:getDataFromView(params)
					else
						listener({result = "error"})
					end
				end
		end

	end	
		network.request(url,"GET", networkListenerGetDataFromView, http_params)

end

function rdb:sendUserData(params) 
	local listener=params.listener
	
	local api_proc=params.api_proc or ''
	local body=params.body or ''
	local listener_params=params.listener_params or nil

	if self:testNetworkConnection()==false then
		_print("No internet connection")
		return
	end
	local url


	url = "http://"..self:getconnectionstring().."/webapi/"..api_proc
	--url = "http://web.smarttouch.com.ua/webapi/"..api_proc

	--_print(url)
--	native.showAlert( "SmartTouch",url, { "OK" })
    local headers = {}

	headers["Content-Type"] = "application/x-www-form-urlencoded"
	headers["Accept-Language"] = "en-US"
    
    
    local http_params = {}
    http_params.headers = headers
	http_params.body = body--
    http_params.progress = "upload"
	http_params.timeout=8
    _print ("send user data...")
	local function networkListenerGetDataFromView(event)
		
		if ( event.isError ) then
				--local alert = native.showAlert( "Network error", "Network error", { "Yes"  } )
				_print( "Network error!")
		elseif ( event.phase == "ended" ) then
				_print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
				--_print(event.response.filename)
		end

	end	
    --native.showAlert( "SmartTouch",'get', { "OK" })
	network.request(url,"POST", networkListenerGetDataFromView, http_params)
end

function rdb:getCountry(params) 
	local listener=params.listener
	
	
	local body=params.body or ''

	if self:testNetworkConnection()==false then
		_print("No internet connection")
		return
	end
	local url


	url = "http://"..self:getconnectionstring().."/get_country"
	--url = "http://web.smarttouch.com.ua/webapi/"..api_proc

	--_print(url)
--	native.showAlert( "SmartTouch",url, { "OK" })
    local headers = {}

    headers["Content-Type"] = "application/json"
		headers["Authorization"] = self:get_admin_authorization_string()
    local myQuery={pageNum=1,pageSize=100000}
    loadsave.saveTable(myQuery, "country.json",system.TemporaryDirectory)
    
    local http_params = {}
    http_params.headers = headers

    http_params.body = {
        filename = "country.json",
        baseDirectory = system.TemporaryDirectory        
    }
--    http_params.progress = "download"

    http_params.response = {
        filename = "country_res.json",
        baseDirectory = system.TemporaryDirectory
    }
	--http_params.timeout = 4
    _print ("send request...")
	local function networkListenerGetDataFromView(event)
		_print ("loadData...:>>>>>countries")
		if ( event.isError ) then
				--local alert = native.showAlert( "Network error", "Network error", { "Yes"  } )
				_print( "Network error!")
					listener({result = "error"})

		elseif ( event.phase == "ended" ) then
				_print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
				--_print(event.response.filename)
				if event.response.filename~=nil then 
					local path = system.pathForFile(event.response.filename,system.TemporaryDirectory ) 
					local file = io.open( path, "r" )
					local output = file:read( "*a" )    
					io.close( file )
					file=nil
					local t = json.decode(output)
					if listener~=nil then
						listener({result = "completed",data=t})
					end
				else
					_print("ERROR IN getting country")
					--_print(event.response)
					listener({result = "error"})

				end
		end

	end
	network.request(url,"GET", networkListenerGetDataFromView, http_params)
end


function rdb:getListCountry(params) 
	local listener=params.listener
	
	local body=params.body or ''
	if self:testNetworkConnection()==false then
		_print("No internet connection")
		return
	end
	local url = "http://"..self:getconnectionstring().."/webapi/object/query_new/countries"
    local headers = {}

    headers["Content-Type"] = "application/json"
		headers["Authorization"] = self:get_admin_authorization_string()
    local myQuery={pageNum=1,pageSize=100000}
    -- myQuery.filter = {group= "and",conditions= {{left= "cntr_code",oper= "is not",right= 'null'},}}
    loadsave.saveTable(myQuery, "countries.json",system.TemporaryDirectory)
    
    local http_params = {}
    http_params.headers = headers


    http_params.body = {
        filename = "countries.json",
        baseDirectory = system.TemporaryDirectory        
    }
    http_params.response = {
        filename = "countries_res.json",
        baseDirectory = system.TemporaryDirectory
    }
	
	local function networkListenerGetDataFromView(event)
		_print ("loadData...:>>>>>countries")
		if ( event.isError ) then
				_print( "Network error!")
				listener({result = "error"})
		elseif ( event.phase == "ended" ) then
				_print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
				if event.response.filename~=nil then 
					local path = system.pathForFile(event.response.filename,system.TemporaryDirectory ) 
					local file = io.open( path, "r" )
					local output = file:read( "*a" )    
					io.close( file )
					file=nil
					local t = json.decode(output)
					if listener~=nil then
						listener({result = "completed",data=t})
					end
				else
					_print("ERROR IN getting countries")
					listener({result = "error"})
				end
		end
	end	
	network.request(url,"POST", networkListenerGetDataFromView, http_params)
end


function rdb:getUserAddress(params) 
	local listener=params.listener
	local long=params.long or 0
	local lat=params.lat or 0
	local listener_params=params.listener_params or nil

	if self:testNetworkConnection()==false then
		_print("No internet connection")
		return
	end

    local headers = {}

    headers["Content-Type"] = "application/json"

    
    local http_params = {}
    http_params.headers = headers

    http_params.response = {
        filename = "loc.json",
        baseDirectory = system.TemporaryDirectory
    }
	--http_params.timeout = 4
    _print ("send request...")
	local function networkListenerGetDataFromView(event)
		if ( event.isError ) then
				_print( "Network error!")
				listener({result = "error"})
		elseif ( event.phase == "ended" ) then
				_print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
				--_print(event.response.filename)
				if event.response.filename~=nil then 
					local path = system.pathForFile(event.response.filename,system.TemporaryDirectory ) 
					local file = io.open( path, "r" )
					local output = file:read( "*a" )    
					io.close( file )
					file=nil
					local t = json.decode(output)
					if listener~=nil then
						listener({result = "completed",params={long=long,lat=lat},data=t, rawData = output})
					end
				else
					_print("ERROR IN getting "..table_name)
					_print(event.response)

						listener({result = "error"})

				end
		end

	end	
	network.request("http://maps.googleapis.com/maps/api/geocode/json?latlng="..lat..","..long,"GET", networkListenerGetDataFromView, http_params)
end

function rdb:newGetPackTable(params)
	
	local function classForGetArr()
		cl = {}
		cl.tablesGetOptions = {} -- название таблиц, фильтры  
		cl.functionsByOnGet = {} -- фнукции по ключам  ключ - имя таблицы
		cl.onFinish =  nil  -- конечная функция после всех functionsByOnEnd
		cl.__index = cl            -- cl будет использоваться как мета-таблица
		return cl
	end

	local function object( cl, obj )
		obj = obj or {}            -- возможно уже есть заполненные поля
		setmetatable(obj, cl)
		return obj
	end

	local arrTable = classForGetArr()

	function arrTable:addTable(table_name, filter, onGet) -- добавление таблицы (тазвание, фильтр, функция псле загрузки )
	 	_print("add table:",  table_name)
	 	onGet = onGet or function() _print( "Function \"onGEt\"  is not specified for "..table_name.."!!!") end
	 	if filter then 
	 		table.insert(self.tablesGetOptions, {table_name = table_name, filter=filter})
	 	else
	 		table.insert(self.tablesGetOptions, {table_name = table_name})
	 	end
	 	self.functionsByOnGet[table_name] = onGet
	end

	function arrTable:showInit() -- посмотреть текущее 
		local isEndFun  = true
		if self.onFinish == nil then
			isEndFun = false
		end
		_print( "count:"..#self.tablesGetOptions.."; isFinisFunction:"..tostring(isEndFun) )
	 	for k, v in pairs(self.tablesGetOptions) do
	 		_print(k,v.table_name) 		
	 	end
	end

	function arrTable:setFunFinish(fun) -- задать конечную функцию 
		if fun and type(fun) == "function" then
			self.onFinish = fun
		end
	end

	function arrTable:getDataCloud(loc_params) -- получить данные из облака 
		local row_index=1
		if loc_params~=nil then
			row_index=loc_params.row_index or 1
		end
		if #self.tablesGetOptions > 0 then
			local function onGetData(event)
				local resByName = {}
				if event.result == "completed" then
				 	for k, v in pairs(event.data) do
				 		if v.table_name  then
				 			resByName[v.table_name] = {}
				 			for kr, vr in pairs(v.rows) do
				 			 	table.insert(resByName[v.table_name], vr)
				 			end 
				 		end
				 	end
				 	for k, v in pairs(self.tablesGetOptions) do
						if resByName[v.table_name] then
							self.functionsByOnGet[v.table_name](resByName[v.table_name])
						else
							self.functionsByOnGet[v.table_name]({})
						end
					end
					if self.onFinish then
						self.onFinish(resByName)
					end
				else
					local str = ""
--[[					for k, v in pairs(self.tablesGetOptions) do
						str = str..v.table_name..", "
					end
					_print("ERROR LOADING "..str.."!!!")]]
					
					if  self.onFinish then
						self.onFinish(false)
					end
				end
			end
			rdb:getDataFromView{listener= onGetData,table_name= "tablepack",func_call=2,query_params=self.tablesGetOptions,row_index=row_index}
		else
			_print( "no tables for download!!!" )
			if  self.onFinish then
				self.onFinish(false)
			end
		end
	end

	function arrTable:clear() -- очистка параметров  
		self.tablesGetOptions = {}   
		self.functionsByOnGet = {} 
		self.onFinish = nil 
	end

	function arrTable:new(params)
		return object(self, params )
	end
	
	return arrTable:new(params)
end 

function rdb:sendEmail(params) 
	local listener=params.listener
	local email_text=params.email_text or ''
	local email_target=params.email_target or ''
	local email=params.email or ''
	local error_flag=params.error_flag or 0

	local listener_params=params.listener_params or nil

	if self:testNetworkConnection()==false then
		_print("No internet connection")
		return
	end
	local url

	if error_flag==1 then
		url = "http://"..self:getconnectionstring().."/webapi/emailbugs"
	else
		url = "http://"..self:getconnectionstring().."/webapi/emailtosupport"
	end
	--_print(url)
	--native.showAlert( "SmartTouch",url, { "OK" })
    local headers = {}

	headers["Content-Type"] = "application/x-www-form-urlencoded"
	headers["Accept-Language"] = "en-US"
    
    
    local http_params = {}
    http_params.headers = headers
	http_params.body = "email="..email.."&email_text="..email_text
	if email_target~="" then
		http_params.body = http_params.body.."&email_target="..email_target
	end
    http_params.progress = "upload"
	http_params.timeout=0
    _print ("send email...")
	local function networkListenerGetDataFromView(event)
		
		if ( event.isError ) then
				--local alert = native.showAlert( "Network error", "Network error", { "Yes"  } )
				_print( "Network error!")
		elseif ( event.phase == "ended" ) then
				_print( "Download complete, total bytes transferred: " .. event.bytesTransferred )
				--_print(event.response.filename)
		end

	end	
    --native.showAlert( "SmartTouch",'get', { "OK" })
	network.request(url,"POST", networkListenerGetDataFromView, http_params)
end

return rdb