--- module for sending sms --------------------------------------------------------------------------------------------

-- local m = require ""

--------------------------------------------------------------------------------------------------------------

local M = {}

local providers = {
	["DEFAULT"] = "Smsapi",
	["UA"] = nil,
	["BY"] = nil
}

--------------------------------------------------------------------------------------------------------------

M.SMSMasterClass = function( params )
	local o = {}            -- new object (public members)
		o.lowBalance = true
	local c = {             -- private members
		country = params.country or "DEFAULT",
		listener = params.listener or function() end,

		provider = nil
	}

	local function init()
		c.country = providers[c.country] and c.country or "DEFAULT"
		local ProviderClass = M[providers[c.country].."ProviderClass"]
		c.provider = ProviderClass({ listener = c.onProviderResponse })
		-- c.provider = M.BulknessProviderClass({ listener = c.onProviderResponse })

		c.provider:check( "balance" )

		return o
	end

	--- Public Methods ---

	o.send = function( self, props )
		if self ~= o then return end
		local phone = props.phone; if not phone then return end
		local message = tostring( props.message ); if not message then return end
		c.testNetworkConnection( function()
			c.provider:send( phone, message )
			-- c.provider:check( "status", "a8ef3d94-173a-4ac9-9a42-9f4cabdb942f" )
			-- c.provider:check( "balance" )
		end)
	end

	--- Private Methods ---

	c.onProviderResponse = function( event )
		if event.name == "sending" then		-- ответ на отправку сообщения
			-- event = { messageID = "1234", phone = 1234 }
			if not event.isError then  			     
			else
			end
		elseif event.name == "status" then 		-- ответ на запрос статуса сообщения
			-- event = { delivered = true/false, date = "02-07-2018 12:40:45T+0000", messageID = "1234" }
			if not event.isError then				
			else
			end		
		elseif event.name == "balance" then 	    -- ответ на запрос состояние баланса
			-- event = { value = 0 }
			if not event.isError then
				o.lowBalance = tonumber( event.value ) <= 0					
			else
			end		
		elseif event.isError then					-- сетевая ошибка
		end
		c.listener( event )
	end

	c.testNetworkConnection = function( listener )
		network.request( "http://worldclockapi.com/api/json/est/now", "GET", function( event )
			if event.isError then c.onProviderResponse( event )
			else listener()
			end
		end )
	end

	return init()
end

M.ProviderClass = function( params )
	local o = {}            -- new object (public members)
	local c = {             -- private members
		listener = params.listener or function() end,
	}

	local function init()

		setmetatable( c, { __index = params } )
		return o, c
	end

	--- Public Methods ---

	o.send = function( self, phone, message )
		if self ~= o then return end
		if not phone or not message then return end
		c.request( "send", { phone = phone, message = message } )
	end

	o.check = function( self, ... )
		local what = arg[1]
		if what == "balance" then
			c.request( "balance" )
		elseif what == "status" then
			local mess_id = arg[2]
			c.request( "status", { mess_id = mess_id } )
		end
	end

	--- Private Methods ---

	c.onRequest = function( event ) 	-- virtual
	end

	c.request = function( method, data )  -- virtual
	end

	c.tostring = function( body )
		local str = nil
		for k,v in pairs( body ) do
			if not str then str = k.."="..v
			else str = str.."&"..k.."="..v 
			end
		end
		return str
	end

	return init()
end

--- PROVIDERS -----------------------------------------------------------------------------------------------------------

M.BulknessProviderClass = function( params )
	local o, c  = M.ProviderClass({        -- inherits public and private members
		-- self private members
		listener = params.listener or function() end,

		host = "https://api.bulkness.com/",		
		user_name = "eugene593c",
		api_key = "EbsFZrVn4J5sUZyd",
		sender = "BonusMe",  -- имя отправителя в сообщениях 
		priority = 0,			-- приоритет собщений 0 (lowest) to 3 (highest)
		system_type = "registration",  	-- группировка сообщений,
	}) 

	local function init()
		return o
	end

	--- Public Methods ---

		-- наследуются из ProviderClass

	--- Private Methods ---

	c.onRequest = function( event )
		local response = json.decode( event.response or "" )
		table.print( response )
		if event.isError then  		             -- ошибка сетевого запроса
			c.listener({ isError = true })
		elseif response.reply and response.reply[1] then 	-- ответ на запрос отправки сообщения
			response = response.reply[1]
			c.listener({ 
				name = "sending", isError = response.status ~= "OK",
				messageID = response.message_id, phone = response.number 
			})
		elseif response.balance then 	   -- ответ на запрос состояния баланса
			c.listener({ 
				name = "balance", isError = not tonumber( response.balance ),
				value = response.balance
			})			
		else 									-- ответ на запрос состояния сообщения
			local message_id, status = next( response )
			status = status or {} 
			c.listener({ 
				name = "status", isError = not message_id,
				messageID = message_id, 
				delivered = status.status == "delivrd",
				date = status.date
			})				
		end
	end	

	c.request = function( method, data )
		local url = c.host.."message/"..method.."/"
		local params = {
			headers = {
				["Content-Type"] = "application/json"
			}
		}
		if method == "send" then
			params.body = c.tostring({
				username = c.user_name,
				api_key = c.api_key,
				from = c.sender,
				to = data.phone,
				message = data.message
			})
		elseif method == "status" then
			params.body = c.tostring({
				username = c.user_name,
				api_key = c.api_key,
				requests = data.mess_id,
				timezone = "UTC",
				timeformat = "%25d-%25m-%25Y%20%25H:%25M:%25ST%25z"
			})
		elseif method == "balance" then
			url = c.host..method.."/"
			params.body = c.tostring({
				username = c.user_name,
				api_key = c.api_key
			})	
		end
		network.request( url, "POST", c.onRequest, params )		
	end	

	return init()
end

M.SmsapiProviderClass = function( params )
	local o, c  = M.ProviderClass({        -- inherits public and private members
		-- self private members
		listener = params.listener or function() end,

		host = "https://api.smsapi.com/",		
		access_token = "3AtSusSbx3SeVWeKnVkd8n3rS2W0CXnUEkU2iHGv",
		-- test = 1,  				-- для тестирования без отправки сообщений
		sender = "Test",  	-- имя отправителя в сообщениях, нужно добавлять в админке
		fast = 1,				-- быстрая отправка, но дороже на 50%
		flash = 1, 				-- сообщение сразу откроется на устройстве пользователя
		format = "json",		-- формат ответа сервера
	}) 

	local function init()
		return o
	end

	--- Public Methods ---

		-- наследуются из ProviderClass

	--- Private Methods ---

	c.onRequest = function( event )
		local response = json.decode( event.response or "" )
		table.print( response )
		
		if event.isError then  		             -- ошибка сетевого запроса
			c.listener({ isError = true })
		elseif response.error then 		-- ответ при отправке сообщения
			c.listener({ name = "sending", isError = true })
		elseif response.list and response.list[1] then 	-- ответ на запрос отправки сообщения
			response = response.list[1]
			local is_error = false
			if response.status ~= "QUEUE" and response.status ~= "DELIVERED" 
			and response.status ~= "SENT" then is_error = true
			end
			c.listener({ 
				name = "sending", isError = is_error,
				messageID = response.id, phone = response.number
			})
		elseif response.points then 	   -- ответ на запрос состояния баланса
			c.listener({ 
				name = "balance", isError = not tonumber( response.points ),
				value = response.points 
			})			
		end
	end	

	c.request = function( method, data )
		local body = nil
		local url = c.host
		local params = {
			headers = {
				["Content-Type"] = "application/json",
				["Authorization"] = "Bearer "..c.access_token,
			}
		}
		if method == "send" then
			body = c.tostring({
				from = c.sender,
				to = data.phone,
				message = data.message,
				format = c.format,
				test = c.test,
				fast = c.fast,
				flush = c.flush
			})
			url = url.."sms.do?"..body
		elseif method == "status" then
			-- у этого провайдера нет метода проверки статуса сообщения по запросу
		elseif method == "balance" then
			body = c.tostring({
				credits = 1,
				format = c.format
			})
			url = url.."user.do?"..body
		end
		network.request( url, "GET", c.onRequest, params )		
	end	

	return init()
end


--------------------------------------------------------------------------------------------------------------

return M.SMSMasterClass