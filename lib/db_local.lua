--[[------------------LOCAL DATABASE----------------------------------------------------------------  

DATABASE
	dataDesk.db

TABLES 
	settings
	orders
	order_items
	menu


----------- INTERFACE ------------------------

- General methods -

open()
close()
checkExistTable( name )
getFieldsNames( table )                    -- returns array with names of exist tables
checkExistColumn( table, col_name )
checkExistRow( table, row_id )
getRowsCount( table )
getLastId( table )
addNewRowData( table, data [, num_rows ] )               -- returns new row id
getFieldValue( table, row_id, field )
getColumnsValues( table, col_names )       -- returns: table = { { col_name1 = val, col_name2 = val, ... }, { col_name1 = val, col_name2 = val, ... } ... }
getRowData( table, row_id )
getRowsData( table, condition, [fields] )
getAllData( table )
delRow( table, row_id )
updateRowData( table, row_id, data )
setFieldValue( table, row_id, field, value )


- Proprietary methods -

for table "settings"
	setSettValue( setting, value )
	getSettValue( setting )
	saveGuestData( data )
	getGuestData()
	clearGuestData()

]]---------------------------------------------------------------------------------------

require "sqlite3"

local ldb = {
	db_name = "dataDesk.db",
	db = nil,
	newColumns = {
			orders={"ord_guest_comment","ord_latitude","ord_longitude","ord_is_completed"},
			order_items={"ordi_modifier_owner_row_id","ordi_base_gi_id","has_base_modifiers"},			
			menu={"name_upper","sorting","mi_disable","gi_weightflag","unt_shortname"},
			pos_news={"posn_type","posn_action_date_end"}
		},			
	columns = {
		settings = 
			[[ id       INTEGER PRIMARY KEY autoincrement, ]]..
			[[ name     TEXT, ]]..
			[[ value    TEXT ]]
		,
		orders =
			[[ id           INTEGER PRIMARY KEY autoincrement, ]]..  -- id заказ в локальной базе
			[[ ord_id       INTEGER DEFAULT 0, ]].. 				    -- id заказ в облаке
			[[ user_id 		INTEGER, ]]..                            -- id официанта
			[[ guest_id 		INTEGER, ]]..                        -- id гостя
			[[ com_id           INTEGER, ]]..       -- id ресторана (юр. лица)
			[[ pos_id           INTEGER, ]]..       -- id точки продаж ресторана
			[[ user_name        TEXT, ]]..          -- имя и фамилия официанта
			[[ ord_type         INTEGER, ]]..       -- 0 - заказ в ресторане, 1 - доставка , 2 - бронь столика, 3 - бронь с заказом еды на вынос
			[[ ord_create       TEXT, ]]..       -- дата создания ордера
			[[ ord_closed       TEXT, ]]..       -- дата закрытия ордера
			[[ ord_state           INTEGER	 DEFAULT 0, ]]..	 -- идентификатор состояния заказа: 
			-- 0-opened,1-sent by user (closed for editing for user), 2- bill request, 3 - payed and closed, 4 - canceled by user,
			--Новое 5 - встречки (начали готовить), 6 - предчек(=bill request), 7 - удален рестораном
			[[ ord_reject_comment     TEXT, ]]..              -- (300) причина отказа рестораном
			[[ ord_prev_reject_comment     TEXT, ]]..         -- предыдущее сообщение ресторана
			[[ ord_reserv_number 	  INTEGER, ]]..           -- кол-во гостей или кол-во приборов
			[[ ord_reserv_datetime 	  TEXT, ]]..              -- дата и время брони
			[[ ord_comment 	  TEXT, ]]..       	     		-- (300) адрес доставки
			[[ ord_reserv_comment 	  TEXT, ]]..       	     -- (300) комментарий к брони
			[[ ord_reserv_state       INTEGER DEFAULT 0, ]]..   -- состояние брони: 0-opened, 1-sent by user (closed for editing for user), 2- confirmed by request, 3 - closed, 4 - canceled by user, 5-canceled by restaurant			
			[[ order_version          INTEGER DEFAULT 0, ]]..
			[[ ord_fee_percent        INTEGER DEFAULT 0,  ]]..     -- процент чаевых
			[[ ord_sum        NUMERIC DEFAULT 0,  ]]..     -- сумма заказа
			[[ ord_discount_sum numeric(10,2), ]].. 
			[[ ord_bonus_sum numeric(10,2), ]].. 
			[[ ord_discount_percent numeric(10,2), ]].. 
			[[ ord_extracharge_sum numeric(10,2), ]].. 
			[[ ord_extra_tax_sum numeric(10,2) default 0.00, ]].. 
		 	[[ ord_latitude 		REAL default 0, ]]..   
		 	[[ ord_longitude        REAL default 0, ]].. 
		 	[[ ord_is_completed     TEXT, ]]..
		 	[[ ord_guest_comment     TEXT, ]]..			
			[[ ord_guest_phone_number        INTEGER DEFAULT 0  ]]     -- номер телефона гостя для доставки и брони
		,
		order_items =
			[[ id               INTEGER PRIMARY KEY autoincrement, ]].. 	 -- id позиции в локальной базе
			[[ local_ord_id     INTEGER, ]]..      -- id заказа в локальной базе                      
			[[ ord_id           INTEGER, ]]..      -- id заказа в облаке
			[[ mi_id            INTEGER, ]]..      -- id блюда в меню
			[[ com_id           INTEGER, ]]..      -- id ресторана
			[[ pos_id           INTEGER, ]]..       -- id точки продаж ресторана
			[[ ordi_id          INTEGER default 0, ]]..    -- id позиции в облаке		
			[[ ordi_create      TEXT, ]]..         -- дата и время добавления позиции в заказ
			[[ ordi_count       INTEGER default 0, ]]..      -- кол-во блюда
			[[ ordi_price       REAL default 0, ]]..                 -- цена блюда
			[[ ordi_sum         REAL default 0, ]]..                 -- цена блюда умноженное на его кол-во
			[[ gi_id 			INTEGER, ]]..              -- id блюда в облаке
			[[ mi_name          TEXT, ]]..                   -- название блюда
			[[ ordi_modifier_owner_row_id INTEGER default 0,]]..-- 09.08
			[[ ordi_base_gi_id INTEGER DEFAULT 0,]]..
			[[ has_base_modifiers INTEGER DEFAULT 0,]]..									
			[[ order_item_version     INTEGER ]]
		,
		menu =
			[[ id           INTEGER PRIMARY KEY autoincrement, ]].. -- id строки а таблице
			[[ com_id       INTEGER, ]]..   -- id ресторана
			[[ pos_id       INTEGER, ]]..   -- id точки продаж ресторана
			[[ mi_id  		INTEGER, ]]..   -- id элемента меню\группы меню - справочник иерархический, содержатся как элементы, так и группы
			[[ gi_id  		INTEGER, ]]..   -- id товара, связанного с элементом меню. данное значение  нужно писать в заказ
			[[ parent_id 	INTEGER, ]]..   -- id родителя для данной позиции. для верхнего уровня=0
			[[ is_group     TEXT DEFAULT 'false', ]]..   -- если да - группа, если нет - элемент меню
			[[ mi_name     	TEXT, ]]..	       -- название
			[[ mi_description     	TEXT, ]]..	       -- описание
			[[ mi_price 	REAL, ]]..	       -- цена
			[[ mi_weight   	    TEXT, ]]..	   -- вес
			[[ gi_weightflag   	    TEXT, ]]..	   -- признак весового блюда			
			[[ photo_width  	 	    TEXT, ]]..	   -- длинна фото
			[[ photo_height   	    TEXT, ]]..	   -- высота фото
			[[ photo_format   	    TEXT, ]]..	   -- формат фото
			[[ photo_id   		    INTEGER, ]]..	   -- id фото
			[[ sorting   		    INTEGER, ]]..	   -- sorting order			
			[[ mi_disable   		    TEXT, ]]..	   -- mark that item is disabled
			[[ version   		 	   REAL default 0, ]]..	   -- версия записи
			[[ rating   		 	   REAL default 0, ]]..	   -- рейтинг позиции
			[[ top_parent_id    INTEGER, ]]..   -- id родителя верхней группы меню
			[[ top_parent_name  TEXT, ]]..        -- имя родителя верхней группы меню
			[[ unt_shortname  TEXT, ]]..        -- ед изм
			[[ name_upper  TEXT ]]        -- для поиска 
		,
		modifiers =   -- модификаторы к блюдам
			[[ id INTEGER PRIMARY KEY autoincrement, ]].. 
			[[ gi_id INTEGER, ]]..
			[[ com_id INTEGER, ]]..
			[[ version, ]]..
    		[[ gi_price NUMERIC (18,2), ]]..
    		[[ gg_id INTEGER, ]] ..
    		[[ gi_weightflag, ]]..
    		[[ prnc_id INTEGER, ]]..
    		[[ gi_modifier, ]]..
    		[[ gi_common_modifier, ]]..
    		[[ gi_name TEXT, ]]..
    		[[ gi_description TEXT, ]].. 
    		[[ lang_code TEXT ]]
    	,		
    	modifiersLinks =
			[[ id INTEGER PRIMARY KEY autoincrement, ]].. 
    		[[ mml_id INTEGER, ]]..
  			[[ gi_id INTEGER, ]]..
  			[[ mi_id INTEGER, ]]..
			[[ mml_price NUMERIC (18,2), ]]..
  			[[ mml_obligatory, ]]..
  			[[ com_id INTEGER ]]  
    	,
    	modifiers_groups =
			[[ id INTEGER PRIMARY KEY autoincrement, ]]..     	
    		[[gg_id INTEGER, ]]..
		    [[com_id INTEGER, ]]..
		    [[gg_parent_id INTEGER, ]]..
		    [[lang_code TEXT, ]]..
		    [[gg_name TEXT, ]]..
		    [[gg_description TEXT]]
    	,   
    	outlets =
			[[ id INTEGER PRIMARY KEY autoincrement, ]]..     	
    		[[com_id INTEGER, ]]..
		    [[pos_id INTEGER, ]]..
		    [[name TEXT, ]]..
		    [[logo_file TEXT, ]]..
		    [[logo_width TEXT, ]]..
		    [[logo_height TEXT, ]]..
		    [[city_id TEXT, ]]..
		    [[city TEXT, ]]..
		    [[pos_bonusme_merchant_id TEXT, ]]..
		    [[use_bonusme_proccessing TEXT, ]]..
		    [[rating INTEGER, ]]..
		    [[geo_lat NUMERIC (12,8), ]]..
		    [[geo_long NUMERIC (12,8), ]]..
		    [[title TEXT, ]]..
		    [[descr TEXT, ]]..
		    [[addr TEXT, ]]..
		    [[work_time TEXT, ]]..	
		    [[phone TEXT, ]]..
		    [[post_name TEXT, ]]..
		    [[pos_type INTEGER, ]]..
		    [[average_bill NUMERIC (12,6), ]]..			    	    		    		    		    
		    [[menu_type TEXT, ]]..	
		    [[usr_lang_code TEXT, ]]..
		    [[srv_url TEXT, ]]..	    
		    [[cur_short TEXT, ]]..
		    [[com_loyalty_type INTEGER]]
    	,       	 	
		addresses = 
			[[ id INTEGER PRIMARY KEY autoincrement, ]].. -- id строки а таблице
			[[ address 	TEXT ]]   -- сохранненый адрес доставки
		,
		pos_news = --таблица новостей заведений
			[[ id          	INTEGER PRIMARY KEY autoincrement, ]].. -- id строки а таблице
			[[ com_id       INTEGER, ]]..   -- id ресторана
			[[ pos_id       INTEGER, ]]..   -- id точки продаж ресторана
			[[ posn_id  		INTEGER, ]]..   -- id новости в облаке
			[[ posn_title  		TEXT, ]]..   -- заголовок новости
			[[ posn_text 	TEXT, ]]..   -- текст новости
			[[ posn_type 	INTEGER, ]]..   -- 
			[[ posn_action_date_end 	TEXT, ]]..   -- 
			[[ posn_datetime 	TEXT ]],   -- датавремя новости

		countries = 
			[[ id INTEGER PRIMARY KEY autoincrement, ]]..
			[[ cntr_id integer NOT NULL,]]..
  			[[ cntr_name TEXT,]]..
  			[[ lang_code TEXT NOT NULL,]]..
  			[[ cntr_code TEXT,]]..
  			[[ cntr_ph_code TEXT]]

		-- ,
		-- order_extra_data =
		-- 	[[ id           INTEGER PRIMARY KEY autoincrement, ]]..  -- id заказ в локальной базе
		-- 	[[ ord_id       INTEGER, ]].. 				    -- id заказ в облаке
		-- 	[[ pos_id 		INTEGER, ]]..                  
		-- 	[[ ord_latitude 		REAL default 0, ]]..   
		-- 	[[ ord_longitude        REAL default 0, ]]..   
		-- 	[[ com_id           INTEGER, ]]..    
		-- 	[[ ord_phone        TEXT, ]]..       
		-- 	[[ version         REAL default 0 ]]
	},
	setts_names = {                    -- настройки, которые хранятся в settings
		"guest_id",  
	    "guest_fullname",
	    "guest_firstname",
	    "guest_surname",
	    "guest_email",
	    "guest_phone",
	    "guest_password",
	    "guest_city",
	    "guest_birthday",
	    "guest_honorific",
	    "rest_of_the_day",
	    "filter_mode",
	    "orders_deleted_version",
	    "last_rest",
	    "cntr_id"
	},
	db_schema = {}
}

function ldb:open()
	local dbPath = system.pathForFile( self.db_name, system.DocumentsDirectory )
	if sqlite3.temp_directory ~= nil then
		local baseDir = system.pathForFile("",system.TemporaryDirectory)
		sqlite3.temp_directory( baseDir )
	end	
	self.db = sqlite3.open( dbPath )
	self.db:exec("PRAGMA synchronous=NORMAL")
	self.db:exec("PRAGMA journal_mode = WAL")
	
	for k,v in pairs( self.columns ) do
		self.db:exec([[ CREATE TABLE IF NOT EXISTS ]]..k..[[ ( ]]..v..[[ ); ]])
		if self:getLastId( k ) == 0 then
			self.db:exec([[ INSERT INTO ]]..k..[[ DEFAULT VALUES ]])
		end
	end

	for k,v in pairs( self.newColumns ) do
		for i,c in pairs( v ) do
			if self:checkExistColumn(k,c,true)==false then
				print("ALTER TABLE " ..k.." ADD COLUMN "..c )
				self.db:exec([[ ALTER TABLE ]] ..k..[[ ADD COLUMN ]]..c..[[ ; ]])
			end
		end
	end	
	self:getDbSchema()	
end

function ldb:close()
	self.db:close()
end

function ldb:getDbSchema()
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local names = {}

	for n,v in db:urows([[ SELECT * FROM sqlite_master  WHERE type = 'table' ]])do
		local sql = "SELECT * FROM "..v.." LIMIT 1;"
		local stmt = db:prepare( sql )
		local names = stmt:get_names()
		self.db_schema[v] = names
	end
--	printResponse(self.db_schema)
end

function ldb:checkExistTable( name,check_in_db )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local names = {}

	if name then
		if check_in_db==true then
			for n,v in db:urows([[ SELECT * FROM sqlite_master WHERE type = 'table' AND name = ']]..name..[[' ]]) do
		 		return true
			end
			return false
		else
			if self.db_schema[name] ~= nil then
				return true
			else
				return false
			end
		end
	else
		for n,v in db:urows([[ SELECT * FROM sqlite_master ]])do
			names[#names+1] = v
		end
	end

	if #names == 0 then return nil 
	else return names
	end
end

function ldb:getFieldsNames( table, check_in_db )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db

	if not table then return nil end

	if not self:checkExistTable( table,check_in_db ) then 
		return nil
	end

	local sql = "SELECT * FROM "..table.." LIMIT 1;"
	local stmt = db:prepare( sql )
	local names = stmt:get_names()

	return names
end

function ldb:checkExistColumn( table, col_name, check_in_db )
	local names
	if check_in_db==true then
		names = self:getFieldsNames( table,check_in_db )
	else
		names = self.db_schema[table]
	end
	--print(table)
	for i=1, #names do
		if names[i] == col_name then return true end
	end

	return false
end

function ldb:checkExistRow( table, row_id )
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	
	local values = self:getColumnsValues( table, { "id" } )

	for i=1, #values do
		if values[i].id == row_id then return true end
	end

	return false
end

function ldb:getRowsCount( table )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local count = 0

	for row in db:nrows([[ SELECT * FROM orders ]]) do
		count = count + 1
	end

	return count
end

function ldb:getLastId( tbl )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local id = 0

	if not self:checkExistTable( tbl ) then return nil end
	
	for row in db:nrows([[ SELECT id FROM ]]..tbl..[[ ORDER BY id DESC LIMIT 1]] ) do
		id = row.id
	end

	return id
end

-- if num_rows specified, data should be:
-- 	data = {
-- 		["field1"] = { value1, values2, value3, ... },
-- 		["field2"] = { value1, values2, value3, ... },
-- 		["field3"] = { value1, values2, value3, ... },
-- 		...
-- 	}
function ldb:addNewRowData( table, data, num_rows,fast_mode )
	-- if not self.db or not self.db:isopen() then
	-- 	self:open()
	-- end
	local fast_mode = fast_mode
	if fast_mode==nil then
		fast_mode=false
	end	
	local db = self.db
	local columns = self.columns[table]

	db:exec([[ CREATE TABLE IF NOT EXISTS ]]..table..[[ ( ]]..columns..[[ ); ]])

	local stmt = [[ INSERT INTO ]]..table
	local first = true
	local part = [[( ]]
	local colnames, values = [[ ]], [[]]

	if not num_rows then		
		for n,v in pairs( data ) do
--			print(v)
			if not self:checkExistColumn( table, n ) then
				error( "SQLight: wrong field name" )
			end
			colnames = colnames..part..n			
			if type( v ) == "string" then values = values..part..[[']]..v..[[']]
			elseif type( v ) == "boolean" then values = values..part..[[']]..tostring(v)..[[']] 
			else values = values..part..v
			end
			if first then 
				first = false
				part = [[, ]]
			end
		end
		colnames = colnames..[[ ) ]]
		values = values..[[ ) ]]
	else
		local values_str = {}
		for n,v in pairs( data ) do
			if not self:checkExistColumn( table, n ) then
				error( "SQLight: wrong field name" )
			end
			colnames = colnames..part..n

			for i=1, num_rows do
				values_str[i] = values_str[i] or ""
				if type( v[i] ) == "string" then 
					values_str[i] = values_str[i]..part..[[']]..v[i]..[[']]
				else values_str[i] = values_str[i]..part..v[i]
				end
			end
			if first then 
				first = false
				part = [[, ]]
			end
		end
		colnames = colnames..[[ ) ]]
		local close_part = [[ ), ]]		
		for i=1, num_rows do
			if i == num_rows then close_part = [[ ) ]] end 
			values_str[i] = values_str[i]..close_part
			values = values..values_str[i]
		end
	end

	stmt = stmt..colnames..[[ VALUES ]]..values..[[; ]]
--	print(stmt)
	db:exec( stmt )
	if fast_mode==false then
		return self:getLastId( table )
	end
end

function ldb:getFieldValue( table, row_id, field )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local value = nil

	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( table, row_id ) then
		error( "SQLight: row with id '"..row_id.."' doesn't exist in the table '"..table.."'" )
	end
	if not self:checkExistColumn( table, field ) then
		error( "SQLight: wrong field name" )
	end
	
	local sql = [[ SELECT ]]..field..[[ FROM ]]..table..[[ WHERE id = ]]..row_id
	for row in db:nrows( sql ) do
		value = row[field]
	end

	return value
end

function ldb:getColumnsValues( table, column_names )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local data = {}

	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	for i=1, #column_names do
		if not self:checkExistColumn( table, column_names[i] ) then
			error( "SQLight: wrong field name" )
		end
	end
	
	for row in db:nrows([[ SELECT * FROM ]]..table ) do
		data[#data+1] = {}
		local d = data[#data]
		local name = ""
		for i=1, #column_names do
			name = column_names[i]
			d[name] = row[name]
		end
	end

	return data
end

function ldb:getRowData( table, row_id )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local data = nil

	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( table, row_id ) then
		error( "SQLight: row with id '"..row_id.."' doesn't exist in the table '"..table.."'" )
	end
	
	local sql = [[ SELECT * FROM ]]..table..[[ WHERE id = ]]..row_id
	for row in db:nrows( sql ) do
		data = {}
		for n,v in pairs( row ) do
			data[n] = v
		end
	end

	return data
end

function ldb:getRowsData( table, condition, fields )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local data = {}
	
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end

	local sql = [[ SELECT * FROM ]]..table..[[ WHERE ]]..condition
	if fields then sql = [[ SELECT ]]..fields..[[ FROM ]]..table..[[ WHERE ]]..condition end
	for row in db:nrows( sql ) do
		data[#data+1] = {}
		for n,v in pairs( row ) do
			data[#data][n] = v
		end
	end

	if #data == 0 then return nil end
	return data
end

function ldb:getAllData( table )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local data = {}

	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	
	for row in db:nrows( [[ SELECT * FROM ]]..table ) do
		print(row)
		data[#data+1] = row
	end

	return data
end

function ldb:delRow( table, row_id )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db

	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( table, row_id ) then
		error( "SQLight: row with id '"..row_id.."' doesn't exist in the table '"..table.."'" )
	end
	
	db:exec( [[ DELETE FROM ]]..table..[[ WHERE id = ]]..row_id )

	return data
end

function ldb:delRows( table, condition )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db

	if self:checkExistTable( table ) then 
		if condition==nil then
			db:exec( [[ DELETE FROM ]]..table)
		else
			db:exec( [[ DELETE FROM ]]..table..[[ WHERE ]]..condition )
		end
	end
end

function ldb:incrementRowData( table, row_id, data )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local columns = self.columns[table]

	if not table then return end
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( table, row_id ) then
		error( "SQLight: row with id '"..row_id.."' doesn't exist in the table '"..table.."'" )
	end

	for n,v in pairs( data ) do
		if not self:checkExistColumn( table, n ) then
			error( "SQLight: wrong field name" )
		end
		db:exec([[ UPDATE ]]..table..[[ SET ]]..n..[[ = ]]..n..
				[[+]]..v..[[ WHERE id=]]..row_id..[[; ]])
	end
end

function ldb:updateRowData( table, row_id, data )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local columns = self.columns[table]
--	print(table,row_id)
	if not table then return end
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( table, row_id ) then
		error( "SQLight: row with id '"..row_id.."' doesn't exist in the table '"..table.."'" )
	end
	
	local stmt = [[ UPDATE ]]..table..[[ SET ]]
	local first = true
	local part = [[]]
	for n,v in pairs( data ) do
		if not self:checkExistColumn( table, n ) then
			error( "SQLight: wrong field name" )
		end
		if type( v ) == "string" or  type( v ) == "boolean" then 
			if type( v ) == "boolean" then
				v= tostring(v)
			end
			if v:sub( 1,1 )=="'" then stmt = stmt..part..n..[[ = ]]
			else
				stmt = stmt..part..n..[[ = ']]
			end
			stmt = stmt..v
			if v:sub( -1,-1 )~="'" then stmt = stmt..[[']] end
		--	stmt = stmt..part..n..[[ = ']]..v..[[']]
		else stmt = stmt..part..n..[[=]]..v
		end
		if first then 
			first = false
			part = [[, ]]
		end
	end
	stmt = stmt..[[ WHERE id=]]..row_id..[[; ]]
	print(stmt)
	db:exec( stmt )
end

function ldb:updateRowDataWithCondition( table, condition, data )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local columns = self.columns[table]

	if not table then return end
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	
	local stmt = [[ UPDATE ]]..table..[[ SET ]]
	local first = true
	local part = [[]]
	for n,v in pairs( data ) do
		if not self:checkExistColumn( table, n ) then
			error( "SQLight: wrong field name" )
		end
		if type( v ) == "string" then stmt = stmt..part..n..[[ = ']]..v..[[']]
		else stmt = stmt..part..n..[[=]]..v
		end
		if first then 
			first = false
			part = [[, ]]
		end
	end
	stmt = stmt..[[ WHERE ]]..condition..[[; ]]
	
	db:exec( stmt )
end


function ldb:setFieldValue( table, row_id, field, value )
	self:updateRowData( table, row_id, { [field] = value } )
end

-------------------------------------------------------------------------------------------

local function checkSettingName( sett_name )
	local names = ldb.setts_names
	for i=1, #names do
		if names[i] == sett_name then return true end
	end

	return false
end


function ldb:setSettValue( setting, value )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local columns = self.columns.settings

--	if not checkSettingName( setting ) then
--		error( "SQLight: wrong setting name" )
--	end

	db:exec( [[CREATE TABLE IF NOT EXISTS settings (]]..columns..[[);]] )
	
	if not self:getRowsData( "settings", "name='"..setting.."'" ) then
		db:exec( [[INSERT INTO settings VALUES (NULL, ']]..setting..
			     [[',']]..value..[[');]] )		
	else
		db:exec( [[ UPDATE settings SET value = ']]..value..
				 [[' WHERE name=']]..setting..[['; ]])
	end
end

function ldb:getSettValue( setting )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local value = nil

	if not self:checkExistTable( "settings") then return nil end

--	if not checkSettingName( setting ) then
--		error( "SQLight: wrong setting name" )
--	end

	local sql = [[ SELECT value FROM settings WHERE name = ']]..setting..[[']]
	for row in db:nrows( sql ) do
		value = row.value
	end	

	return value
end

function ldb:clearGuestData()
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db

	if not self:checkExistTable( "settings" ) then return end

	db:exec( [[DELETE FROM settings WHERE name='guest_id';]] )
	db:exec( [[DELETE FROM settings WHERE name='guest_fullname';]] )
	db:exec( [[DELETE FROM settings WHERE name='guest_firstname';]] )
	db:exec( [[DELETE FROM settings WHERE name='guest_surname';]] )
	db:exec( [[DELETE FROM settings WHERE name='guest_phone';]] )
	db:exec( [[DELETE FROM settings WHERE name='guest_birthday';]] )
	db:exec( [[DELETE FROM settings WHERE name='guest_email';]] )
	db:exec( [[DELETE FROM settings WHERE name='guest_password';]] )
	db:exec( [[DELETE FROM settings WHERE name='guest_honorific';]] )
	db:exec( [[DELETE FROM settings WHERE name='cntr_id';]] )
end

function ldb:saveGuestData( data )
	self:clearGuestData()

	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local columns = self.columns.settings

	db:exec( [[CREATE TABLE IF NOT EXISTS settings (]]..columns..[[);]] )
	for k,v in pairs( data ) do
		if checkSettingName( k ) then
			db:exec( [[INSERT INTO settings VALUES (NULL, ']]..k..[[',']]..v..[[');]] )
		end
	end
	self:getGuestCodeCountry()
end

function ldb:getGuestData()
	print("ldb:getGuestData()")
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local data = {}
	local row = {}
	local names = { "guest_id", "guest_email", "guest_fullname", "guest_birthday", "guest_password",
		"guest_firstname", "guest_surname", "guest_phone", "guest_honorific", "cntr_id"
	}

	if not self:checkExistTable( "settings" ) then return nil end

	for i=1, #names do
		-- print([[ SELECT * FROM settings WHERE name = ']]..names[i]..[[' ]])
		for row in db:nrows([[ SELECT * FROM settings WHERE name = ']]..names[i]..[[' ]]) do
--			data[names[i]] = row.value
			if names[i]=="guest_birthday" then
				data[names[i]] = string.sub(row.value,1,10)
			else
				data[names[i]] = row.value
			end
			-- print("gor user")
		end
	end

	for k,v in pairs( data ) do return data end
	return nil
end

function ldb:getGuestCodeCountry()
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local  dg = self:getGuestData()
	local codeCnr = nil
	if dg and dg.cntr_id and dg.cntr_id:len() > 0 then
		for row in self.db:nrows("select * from countries where cntr_id = " ..dg.cntr_id.." and cntr_code is not null") do
			if row.cntr_code:len() > 0 then
				codeCnr = row.cntr_code
				break
			end
		end
	end
	if codeCnr ~= nil then
		_G.userCountry = codeCnr
		_G.current_country = CountriesList[_G.userCountry][_G.Lang].cntr_name
	end
	return codeCnr 
end

function ldb:createNewOrder( ord_type )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local date = os.date( "*t" ) 
	local dt = os.date('%Y-%m-%d %H:%M:%S', os.time(date))
	userProfile.activeOrderID = self:addNewRowData( "orders", {
		guest_id = ldb:getSettValue( "guest_id" ),
		com_id = userProfile.SelectedRestaurantData.com_id,
		pos_id = userProfile.SelectedRestaurantData.pos_id,
		ord_type = ord_type or 0,
		ord_create = dt
	})
	return userProfile.activeOrderID
end

function ldb:getRowDataByCondition( table, condition )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	
	if not self:checkExistTable( table ) then 
	 error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	 
	local sql = [[ SELECT * FROM ]]..table..[[ WHERE ]]..condition
	data = {}
	for row in db:nrows( sql ) do  
	 for n,v in pairs( row ) do
	  print(v)
	  data[n] = v
	 end
	end

	return data
end

function ldb:incrOrderItemCount( row_id, count )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db

	local table = "order_items"
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( table, row_id ) then
		error( "SQLight: row with id '"..row_id.."' doesn't exist in the table '"..table.."'" )
	end

	local incr = [[ ordi_count + ]]..count
	db:exec([[ UPDATE ]]..table..[[ SET ordi_count = ]]..incr..
		[[, ordi_sum = ordi_price * (]]..incr..[[) WHERE id=]]..row_id..[[; ]]
	)

end

function ldb:getOrderSum( local_ord_id )
	print("getOrderSum")
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db

	local table = "order_items"
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( "orders", local_ord_id ) then
		error( "SQLight: row with id '"..local_ord_id.."' doesn't exist in the table 'orders'" )
	end

	local sql = [[ SELECT (oi.ordi_sum-coalesce(ord_discount_sum,0)) AS total from orders o inner join (select sum(ordi_sum) 
		AS ordi_sum,local_ord_id FROM order_items 
		where local_ord_id =]]..local_ord_id..[[ group by local_ord_id) 
		oi on oi.local_ord_id=o.id where o.id =]]..local_ord_id..[[ and o.ord_state<>3 and o.ord_reserv_state<>3 GROUP BY local_ord_id]]

	for row in db:nrows( sql ) do 
--		print(row.total) 
		return row.total
	end

	return 0
end

function ldb:setOrderSum( local_ord_id )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db

	local table = "order_items"

	local sql = [[update orders set ord_sum= (SELECT sum(ordi_sum) AS total FROM order_items where local_ord_id = ]]..local_ord_id..[[)
		 where id= ]]..local_ord_id
	db:exec(sql)
	return 0
end

function ldb:saveAddress( address )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local index = self:addNewRowData( "addresses", {
		["address"] = address
	})
	return index
end

function ldb:getAllAddresses()
	local rawdata = self:getAllData( "addresses" )
	local data = {}
	for i=1, #rawdata do
		for k,v in pairs( rawdata[i] ) do
			if k == "address" then data[#data+1] = v end 
		end
	end
	return data
end

function ldb:getNews(pos_id_array,posn_type)
	local sql = [[ SELECT * from pos_news where pos_id in (]]..pos_id_array..[[)  order by posn_id desc limit 20]] --and posn_type=]]..posn_type..[[
	-- if pos_id_array==0 then
	-- 	sql = [[ SELECT * from pos_news where pos_id in (]]..pos_id_array..[[) order by posn_id desc limit 10]]
	-- end
	local data = {}
	local db = self.db
	for row in db:nrows( sql ) do 
		data[#data+1] = row
		for j=1, #RestoransList do
			if row.pos_id == RestoransList[j].pos_id then
				data[#data].pos_name=RestoransList[j].title
			end
		end

	end
	return data
end

function ldb:getMaxField(table, field, condition)
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistColumn( table, field ) then
		error( "SQLight: wrong field name" )
	end	
	local value
	local sql
	local db = self.db
	if condition then
		sql = [[ SELECT MAX(]]..field..[[) as value FROM ]]..table..[[ where ]]..condition
	else
		sql = [[ SELECT MAX(]]..field..[[) as value FROM ]]..table
	end
	for row in db:nrows( sql ) do
		value = row.value
	end
	if value == nil then
		value = 0
	end
	return value
end

function ldb:getMinField(table, field)
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistColumn( table, field ) then
		error( "SQLight: wrong field name" )
	end	
	local db = self.db
	local value
	local sql = [[ SELECT MIN(]]..field..[[) as value FROM ]]..table
	for row in db:nrows( sql ) do
		value = row.value
	end
	if value == nil then
		value = 0
	end
	return value
end

function ldb:getDataPriceModifiers(mg_id,search_query,gg_id)
	local data = {}
	local isCommon = false
	local db = self.db	
	gg_id = gg_id or 0
	local filter_by_group = ""
	local gCommon = {}
	search_query = search_query or ""

	if search_query == "" then
		if gg_id ~= -1 then
			if gg_id == 0 then
				local sql =  [[select gg_id as gi_id,gg_parent_id, gg_name as name, 0 as price, 'true' as gi_common_modifier, 1 as modifier, 0 as gi_weightflag, 1 as is_group from modifiers_groups]]
				for row in db:nrows( sql ) do
					table.insert(data, row)
				end
			else
				local sql =  [[select gg_id as gi_id,gg_parent_id, gg_name as name, 0 as price, 'true' as gi_common_modifier, 1 as modifier, 0 as gi_weightflag, 1 as is_group from modifiers_groups where gg_id=]]..gg_id
				for row in db:nrows( sql ) do
					table.insert(data, row)
				end
				if gg_id > 0 then
					filter_by_group = "and gg_id ="..gg_id
				end
			end
		end
	end

	local sql_common = [[select gg_id as parent_id, gi_price as price, gi_name as name, gi_id, gi_common_modifier, 1 as modifier, gi_weightflag from modifiers where lang_code = ']].. _G.Lang..[[' and gi_common_modifier = 'true' ]]..search_query..filter_by_group..[[  union ]]
	local sql =	[[select gg_id as parent_id, modifiersLinks.mml_price as price, gi_name as name, modifiers.gi_id, "false" as gi_common_modifier, 1 as modifier, modifiers.gi_weightflag  from modifiersLinks , modifiers where modifiersLinks.mi_id =']]..mg_id..[[' and modifiersLinks.gi_id = modifiers.gi_id and lang_code =']].._G.Lang.. [[' ]]..search_query..[[ order by gi_common_modifier desc]]


	if #data >1 then
	else
		if #data == 1 then
			gg_id = data[1].gi_id
		end
		sql = sql_common..sql
	end


	for row in db:nrows( sql ) do
		print(row.name)
		row.price = row.price or 0
		row.name = string.utf8sub(row.name, 0, 32)
		table.insert( data, row )
		if (not isCommon) and row.gi_common_modifier == "true" then
			isCommon = true
		end
	end
	return data, isCommon, gg_id
end

return ldb


