--[[------------------LOCAL DATABASE----------------------------------------------------------------  

DATABASE
	dataDesk.db

TABLES 
	settings
	orders
	order_items
	menu


----------- INTERFACE ------------------------

- General methods -

open()
close()
checkExistTable( name )
getFieldsNames( table )                    -- returns array with names of exist tables
checkExistColumn( table, col_name )
checkExistRow( table, row_id )
getRowsCount( table )
getLastId( table )
addNewRowData( table, data [, num_rows ] )               -- returns new row id
getFieldValue( table, row_id, field )
getColumnsValues( table, col_names )       -- returns: table = { { col_name1 = val, col_name2 = val, ... }, { col_name1 = val, col_name2 = val, ... } ... }
getRowData( table, row_id )
getRowsData( table, condition, [fields] )
getAllData( table )
delRow( table, row_id )
updateRowData( table, row_id, data )
setFieldValue( table, row_id, field, value )


- Proprietary methods -

for table "settings"
	setSettValue( setting, value )
	getSettValue( setting )
	saveGuestData( data )
	getGuestData()
	clearGuestData()

]]---------------------------------------------------------------------------------------

require "sqlite3"

local ldb = {
	db_name = "dataDesk.db",
	db = nil,	
	columns = {
		settings = 
			[[ id       INTEGER PRIMARY KEY autoincrement, ]]..
			[[ name     TEXT, ]]..
			[[ value    TEXT ]]
		,
		orders =
			[[ id           INTEGER PRIMARY KEY autoincrement, ]]..  -- id заказ в локальной базе
			[[ ord_id       INTEGER DEFAULT 0, ]].. 				    -- id заказ в облаке
			[[ user_id 		INTEGER, ]]..                            -- id официанта
			[[ guest_id 		INTEGER, ]]..                        -- id гостя
			[[ com_id           INTEGER, ]]..       -- id ресторана
			[[ user_name        TEXT, ]]..          -- имя и фамилия официанта
			[[ ord_type         INTEGER, ]]..       -- 0 - заказ в ресторане, 1 - доставка , 2 - бронь столика, 3 - бронь с заказом еды на вынос
			[[ ord_create       TEXT, ]]..       -- дата создания ордера
			[[ ord_closed       TEXT, ]]..       -- дата закрытия ордера
			[[ ord_state           INTEGER	DEFAULT 0, ]]..	 -- идентификатор состояния заказа: 0-opened,1-sent by user (closed for editing for user), 2- bill request, 3 - payed and closed, 4 - canceled by user, 5-canceled by restaurant 
			[[ ord_reject_comment     TEXT, ]]..              -- (300) причина отказа рестораном
			[[ ord_reserv_number 	  INTEGER, ]]..           -- кол-во гостей или кол-во приборов
			[[ ord_reserv_datetime 	  TEXT, ]]..              -- дата и время брони
			[[ ord_reserv_comment 	  TEXT, ]]..       	     -- (300) комментарий к брони
			[[ ord_reserv_state       INTEGER DEFAULT 0, ]]..   -- состояние брони: 0-opened, 1-sent by user (closed for editing for user), 2- confirmed by request, 3 - closed, 4 - canceled by user, 5-canceled by restaurant			
			[[ order_version          INTEGER DEFAULT 0, ]]..
			[[ ord_fee_percent               INTEGER DEFAULT 0  ]]     -- процент чаевых
		,
		order_items =
			[[ id               INTEGER PRIMARY KEY autoincrement, ]].. 	 -- id позиции в локальной базе
			[[ local_ord_id     INTEGER, ]]..      -- id заказа в локальной базе                      
			[[ ord_id           INTEGER, ]]..      -- id заказа в облаке
			[[ mi_id            INTEGER, ]]..      -- id блюда в меню
			[[ com_id           INTEGER, ]]..      -- id ресторана
			[[ ordi_id          INTEGER default 0, ]]..    -- id позиции в облаке		
			[[ ordi_create      TEXT, ]]..         -- дата и время добавления позиции в заказ
			[[ ordi_count       INTEGER default 0, ]]..      -- кол-во блюда
			[[ ordi_price       REAL default 0, ]]..                 -- цена блюда
			[[ ordi_sum         REAL default 0, ]]..                 -- цена блюда умноженное на его кол-во
			[[ gi_id 			INTEGER, ]]..              -- id блюда в облаке
			[[ mi_name          TEXT, ]]..                   -- название блюда
			[[ order_item_version     INTEGER default 0 ]]
		,
		menu =
			[[ id           INTEGER PRIMARY KEY autoincrement, ]].. -- id строки а таблице
			[[ com_id       INTEGER, ]]..   -- id ресторана
			[[ mi_id  		INTEGER, ]]..   -- id элемента меню\группы меню - справочник иерархический, содержатся как элементы, так и группы
			[[ parent_id 	INTEGER, ]]..   -- id родителя для данной позиции. для верхнего уровня=0
			[[ is_group     TEXT DEFAULT 'false', ]]..   -- если да - группа, если нет - элемент меню
			[[ mi_name     	TEXT, ]]..	       -- название
			[[ mi_price 	REAL, ]]..	       -- цена
			[[ mi_weight   	    TEXT, ]]..	   -- вес
			[[ top_parent_id    INTEGER, ]]..   -- id родителя верхней группы меню
			[[ top_parent_name  TEXT ]]        -- имя родителя верхней группы меню
	},
	setts_names = {                    -- настройки, которые хранятся в settings
		"guest_id",  
	    "guest_fullname",
	    "guest_email",
	    "guest_password"
	}
}

function ldb:open()
	local dbPath = system.pathForFile( self.db_name, system.DocumentsDirectory )
	self.db = sqlite3.open( dbPath )
	
	for k,v in pairs( self.columns ) do
		self.db:exec([[ CREATE TABLE IF NOT EXISTS ]]..k..[[ ( ]]..v..[[ ); ]])
		if self:getLastId( k ) == 0 then
			self.db:exec([[ INSERT INTO ]]..k..[[ DEFAULT VALUES ]])
		end
	end
end

function ldb:close()
	self.db:close()
end

function ldb:checkExistTable( name )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local names = {}

	if name then
		for n,v in db:urows([[ SELECT * FROM sqlite_master WHERE type = 'table' AND name = ']]..name..[[' ]]) do
			return true
		end
		return false
	else
		for n,v in db:urows([[ SELECT * FROM sqlite_master ]])do
			names[#names+1] = v
		end
	end

	if #names == 0 then return nil 
	else return names
	end
end

function ldb:getFieldsNames( table )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db

	if not table then return nil end

	if not self:checkExistTable( table ) then 
		return nil
	end

	local sql = "SELECT * FROM "..table.." LIMIT 1;"
	local stmt = db:prepare( sql )
	local names = stmt:get_names()

	return names
end

function ldb:checkExistColumn( table, col_name )
	local names = self:getFieldsNames( table )

	for i=1, #names do
		if names[i] == col_name then return true end
	end

	return false
end

function ldb:checkExistRow( table, row_id )
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	
	local values = self:getColumnsValues( table, { "id" } )

	for i=1, #values do
		if values[i].id == row_id then return true end
	end

	return false
end

function ldb:getRowsCount( table )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local count = 0

	for row in db:nrows([[ SELECT * FROM orders ]]) do
		count = count + 1
	end

	return count
end

function ldb:getLastId( tbl )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local id = 0

	if not self:checkExistTable( tbl ) then return nil end
	
	for row in db:nrows([[ SELECT id FROM ]]..tbl..[[ ORDER BY id DESC LIMIT 1]] ) do
		id = row.id
	end

	return id
end

-- if num_rows, data should be:
-- 	data = {
-- 		["field1"] = { value1, values2, value3, ... },
-- 		["field2"] = { value1, values2, value3, ... },
-- 		["field3"] = { value1, values2, value3, ... },
-- 		...
-- 	}
function ldb:addNewRowData( table, data, num_rows )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local columns = self.columns[table]

	db:exec([[ CREATE TABLE IF NOT EXISTS ]]..table..[[ ( ]]..columns..[[ ); ]])

	local stmt = [[ INSERT INTO ]]..table
	local first = true
	local part = [[( ]]
	local colnames, values = [[]], [[]]

	if not num_rows or num_rows == 1 then		
		for n,v in pairs( data ) do
			if not self:checkExistColumn( table, n ) then
				error( "SQLight: wrong field name" )
			end
			colnames = colnames..part..n			
			if type( v ) == "string" then values = values..part..[[']]..v..[[']]
			else values = values..part..v
			end
			if first then 
				first = false
				part = [[, ]]
			end
		end
		colnames = colnames..[[ ) ]]
		values = values..[[ ) ]]
	else
		local values_str = {}
		for n,v in pairs( data ) do
			if not self:checkExistColumn( table, n ) then
				error( "SQLight: wrong field name" )
			end
			colnames = colnames..part..n

			for i=1, num_rows do
				values_str[i] = values_str[i] or ""
				if type( v[i] ) == "string" then 
					values_str[i] = values_str[i]..part..[[']]..v[i]..[[']]
				else values_str[i] = values_str[i]..part..v[i]
				end
			end
			if first then 
				first = false
				part = [[, ]]
			end
		end
		colnames = colnames..[[ ) ]]
		local close_part = [[ ), ]]		
		for i=1, num_rows do
			if i == num_rows then close_part = [[ ) ]] end 
			values_str[i] = values_str[i]..close_part
			values = values..values_str[i]
		end
	end

	stmt = stmt..colnames..[[ VALUES ]]..values..[[; ]]
	db:exec( stmt )

	return self:getLastId( table )
end

function ldb:getFieldValue( table, row_id, field )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local value = nil

	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( table, row_id ) then
		error( "SQLight: row with id '"..row_id.."' doesn't exist in the table '"..table.."'" )
	end
	if not self:checkExistColumn( table, field ) then
		error( "SQLight: wrong field name" )
	end
	
	local sql = [[ SELECT ]]..field..[[ FROM ]]..table..[[ WHERE id = ]]..row_id
	for row in db:nrows( sql ) do
		value = row[field]
	end

	return value
end

function ldb:getColumnsValues( table, column_names )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local data = {}

	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	for i=1, #column_names do
		if not self:checkExistColumn( table, column_names[i] ) then
			error( "SQLight: wrong field name" )
		end
	end
	
	for row in db:nrows([[ SELECT * FROM ]]..table ) do
		data[#data+1] = {}
		local d = data[#data]
		local name = ""
		for i=1, #column_names do
			name = column_names[i]
			d[name] = row[name]
		end
	end

	return data
end

function ldb:getRowData( table, row_id )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local data = nil

	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( table, row_id ) then
		error( "SQLight: row with id '"..row_id.."' doesn't exist in the table '"..table.."'" )
	end
	
	local sql = [[ SELECT * FROM ]]..table..[[ WHERE id = ]]..row_id
	for row in db:nrows( sql ) do
		data = {}
		for n,v in pairs( row ) do
			data[n] = v
		end
	end

	return data
end

function ldb:getRowsData( table, condition, fields )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local data = {}

	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end

	local sql = [[ SELECT * FROM ]]..table..[[ WHERE ]]..condition
	if fields then sql = [[ SELECT ]]..fields..[[ FROM ]]..table..[[ WHERE ]]..condition end
	for row in db:nrows( sql ) do
		data[#data+1] = {}
		for n,v in pairs( row ) do
			data[#data][n] = v
		end
	end

	if #data == 0 then return nil end
	return data
end

function ldb:getAllData( table )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local data = {}

	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	
	for row in db:nrows( [[ SELECT * FROM ]]..table ) do
		data[#data+1] = row
	end

	return data
end

function ldb:delRow( table, row_id )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db

	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( table, row_id ) then
		error( "SQLight: row with id '"..row_id.."' doesn't exist in the table '"..table.."'" )
	end
	
	db:exec( [[ DELETE FROM ]]..table..[[ WHERE id = ]]..row_id )

	return data
end

function ldb:delRows( table, condition )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db

	if self:checkExistTable( table ) then 
		db:exec( [[ DELETE FROM ]]..table..[[ WHERE ]]..condition )
	end
end

function ldb:incrementRowData( table, row_id, data )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local columns = self.columns[table]

	if not table then return end
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( table, row_id ) then
		error( "SQLight: row with id '"..row_id.."' doesn't exist in the table '"..table.."'" )
	end

	for n,v in pairs( data ) do
		if not self:checkExistColumn( table, n ) then
			error( "SQLight: wrong field name" )
		end
		db:exec([[ UPDATE ]]..table..[[ SET ]]..n..[[ = ]]..n..
				[[+]]..v..[[ WHERE id=]]..row_id..[[; ]])
	end
end

function ldb:updateRowData( table, row_id, data )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local columns = self.columns[table]

	if not table then return end
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( table, row_id ) then
		error( "SQLight: row with id '"..row_id.."' doesn't exist in the table '"..table.."'" )
	end
	
	local stmt = [[ UPDATE ]]..table..[[ SET ]]
	local first = true
	local part = [[]]
	for n,v in pairs( data ) do
		if not self:checkExistColumn( table, n ) then
			error( "SQLight: wrong field name" )
		end
		if type( v ) == "string" then stmt = stmt..part..n..[[ = ']]..v..[[']]
		else stmt = stmt..part..n..[[=]]..v
		end
		if first then 
			first = false
			part = [[, ]]
		end
	end
	stmt = stmt..[[ WHERE id=]]..row_id..[[; ]]
	
	db:exec( stmt )
end

function ldb:setFieldValue( table, row_id, field, value )
	self:updateRowData( table, row_id, { [field] = value } )
end

-------------------------------------------------------------------------------------------

local function checkSettingName( sett_name )
	local names = ldb.setts_names
	for i=1, #names do
		if names[i] == sett_name then return true end
	end

	return false
end


function ldb:setSettValue( setting, value )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local columns = self.columns.settings

	if not checkSettingName( setting ) then
		error( "SQLight: wrong setting name" )
	end

	db:exec( [[CREATE TABLE IF NOT EXISTS settings (]]..columns..[[);]] )
	db:exec( [[ UPDATE settings SET value = ']]..value..
			 [[' WHERE name=']]..setting..[['; ]])	

end

function ldb:getSettValue( setting )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local value = nil

	if not self:checkExistTable( "settings") then return nil end

	if not checkSettingName( setting ) then
		error( "SQLight: wrong setting name" )
	end

	local sql = [[ SELECT value FROM settings WHERE name = ']]..setting..[[']]
	for row in db:nrows( sql ) do
		value = row.value
	end	

	return value
end

function ldb:clearGuestData()
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db

	if not self:checkExistTable( "settings" ) then return end

	db:exec( [[DELETE FROM settings WHERE name='guest_id';]] )
	db:exec( [[DELETE FROM settings WHERE name='guest_fullname';]] )
	db:exec( [[DELETE FROM settings WHERE name='guest_email';]] )
	db:exec( [[DELETE FROM settings WHERE name='guest_password';]] )

end

function ldb:saveGuestData( data )
	self:clearGuestData()

	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local columns = self.columns.settings

	db:exec( [[CREATE TABLE IF NOT EXISTS settings (]]..columns..[[);]] )
	db:exec( [[INSERT INTO settings VALUES (NULL, 'guest_id',']]..data.guest_id..[[');]] )
	db:exec( [[INSERT INTO settings VALUES (NULL, 'guest_fullname',']]..data.guest_fullname..[[');]] )
	db:exec( [[INSERT INTO settings VALUES (NULL, 'guest_email',']]..data.guest_email..[[');]] )
	db:exec( [[INSERT INTO settings VALUES (NULL, 'guest_password',']]..data.guest_password..[[');]] )
	
end

function ldb:getGuestData()
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local data = {}
	local row = {}
	local names = { "guest_id", "guest_email", "guest_fullname", "guest_password","cntr_id" }

	if not self:checkExistTable( "settings" ) then return nil end

	for i=1, #names do
		for row in db:nrows([[ SELECT * FROM settings WHERE name = ']]..names[i]..[[' ]]) do
			data[names[i]] = row.value
		end
	end

	for k,v in pairs( data ) do return data end
	return nil
end

function ldb:createNewOrder()
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	local date = os.date( "*t" ) 
	local dt = os.date('%Y-%m-%d %H:%M:%S', os.time(date))
	userProfile.activeOrderID = self:addNewRowData( "orders", {
		guest_id = ldb:getSettValue( "guest_id" ),
		com_id = userProfile.SelectedRestaurantData.com_id,
		ord_type = 0,
		ord_create = dt
	})
	return userProfile.activeOrderID
end

function ldb:getRowDataByCondition( table, condition )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db
	
	if not self:checkExistTable( table ) then 
	 error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	 
	local sql = [[ SELECT * FROM ]]..table..[[ WHERE ]]..condition
	data = {}
	for row in db:nrows( sql ) do  
	 for n,v in pairs( row ) do
	  print(v)
	  data[n] = v
	 end
	end

	return data
end

function ldb:incrOrderItemCount( row_id, count )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db

	local table = "order_items"
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( table, row_id ) then
		error( "SQLight: row with id '"..row_id.."' doesn't exist in the table '"..table.."'" )
	end

	local incr = [[ ordi_count + ]]..count
	db:exec([[ UPDATE ]]..table..[[ SET ordi_count = ]]..incr..
		[[, ordi_sum = ordi_price * (]]..incr..[[) WHERE id=]]..row_id..[[; ]]
	)

end

function ldb:getOrderSum( local_ord_id )
	if not self.db or not self.db:isopen() then
		self:open()
	end
	local db = self.db

	local table = "order_items"
	if not self:checkExistTable( table ) then 
		error( "SQLight: table with name '"..table.."' doesn't exist" )
	end
	if not self:checkExistRow( "orders", local_ord_id ) then
		error( "SQLight: row with id '"..local_ord_id.."' doesn't exist in the table 'orders'" )
	end

	local sql = [[ SELECT sum(ordi_sum) AS total FROM order_items 
		GROUP BY local_ord_id HAVING local_ord_id = ]]..local_ord_id
	for row in db:nrows( sql ) do  
		return row.total
	end

	return 0
end

return ldb


